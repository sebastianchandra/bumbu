<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("menu_model");
        $this->load->model('master/users_model');
        $this->load->model('master/master_model');
        $this->load->library('form_validation');
    }


	function index(){
        // test($this->session->userdata,1);
		if (!isset($this->current_user['loginuser'])){
			$username 		= $this->security->xss_clean($this->db->escape_str($this->input->post('username')));
			$password		= $this->security->xss_clean($this->db->escape_str($this->input->post('password')));

			$this->form_validation->set_rules('username','Username');
            $this->form_validation->set_rules('password','Password');

			if ($username == '' ){
                $this->load->view('login');
            }else{
                $company_data   = $this->master_model->company();
            	$usr_result = $this->users_model->get_user($username,$password);
                $row = $this->users_model->detail_user($username,$password);

                if ($usr_result > 0){
                    
                  	$session_data = array('user_id'        => $row->user_id,
                                            'nip'          => $row->nip,
                                            'name'         => $row->name,
                                            'user_group'   => $row->id_user_group,
                                            'user_level'   => $row->id_user_level,
                                            // 'user_level_new'=>$row->user_level,
                                            // 'user_level_name'=>$row->user_level_name,
                                            'company_id'   => $company_data->company_id,
                                            'company_name' => $company_data->company_name,
                                            'loginuser'    => TRUE,
                                            'menu'         => $this->menu_model->get_akses_menu($row->user_id),
                                            'submenu'      => $this->menu_model->get_akses_submenu($row->user_id)
                                        );
                    // test($session_data,1);
                    $this->session->sess_expiration_on_close = 'true';
                    $this->session->set_userdata('session', $session_data);
                    $this->users_model->aktifitas_user('logged in');

                    redirect('welcome');

                }else{
                	$this->session->set_flashdata('msg','<div class="alert alert-danger text-center"><font size="2">Username Atau Password Anda salah</font></div>');
                    redirect($_SERVER['HTTP_REFERER']);
                    redirect('login');
                }
            } 
		}else{
            redirect('welcome');
		}
	}

	function logout() {
        // if ($this->current_user['loginuser'] == 1){
        //     $this->users_model->aktifitas_user('logged out');
        // }
        $this->session->unset_userdata('session');

        $this->load->view('login');
    }
}
