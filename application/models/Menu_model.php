<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    function get_akses_menu($id){
        $sql    = "SELECT a.menu_id,a.app_code,a.menu_group,a.icon,a.url,count(a.menu_id) jmlh FROM mst_menu a, 
                    mst_menu_permissions b WHERE b.user_id=".$id." AND a.menu_id=b.menu_id AND a.active=1 
                    GROUP BY a.menu_group";
        $query  = $this->db->query($sql)->result();
        return $query;
    }

    function get_akses_submenu($id){
        $sql    = "SELECT a.menu_id,a.app_code,a.menu_name,a.menu_group,a.url,a.icon,a.no FROM mst_menu a, 
                    mst_menu_permissions b WHERE b.user_id=".$id." AND a.menu_id=b.menu_id AND a.active=1
                    ORDER BY a.menu_group,a.no";
        $query  = $this->db->query($sql)->result();
        return $query;
    }

    function get_menu(){
        $sql    = "SELECT menu_id,app_code,menu_name,menu_group FROM mst_menu WHERE active='1' ORDER BY menu_group";
        $query  = $this->db->query($sql)->result();
        return $query;
    }

    function permission_menu($id,$url){
        $sql    = "SELECT * FROM mst_menu a, mst_menu_permissions b WHERE a.menu_id=b.menu_id AND b.user_id='".$id."' AND a.url='".$url."'";
        // test($sql,1);
        $query  = $this->db->query($sql)->num_rows();
        return $query;
    }
}	