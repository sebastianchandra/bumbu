<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_order_model extends CI_Model
{
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_po_01';
    private $poId;
    private $poNo;
    private $prId;
    private $prNo;
    private $poDate;
    private $requester;
    private $projectId;
    private $projectName;
    private $companyId;
    private $companyName;
    private $supplierId;
    private $supplierName;
    private $deliveryAddress;
    private $poInfo;
    private $currencyId;
    private $poStatus;
    private $paymentStatus;
    private $transportFee;
    private $ppnPercent;
    private $ppnValue;
    private $pph;
    private $pphValue;
    private $pbbkb;
    private $pbbkbValue;
    private $discPercent;
    private $discVal;
    private $total;
    private $reamaining;
    private $top;
    private $noPrint;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->poId = 0;
        $this->poNo = '';
        $this->prId = 0;
        $this->prNo = '';
        $this->poDate = "0000-00-00";
        $this->requester = '';
        $this->projectId = 0;
        $this->projectName = '';
        $this->companyId = 0;
        $this->companyName = '';
        $this->supplierId = 0;
        $this->supplierName = '';
        $this->deliveryAddress = '';
        $this->poInfo = '';
        $this->currencyId = 0;
        $this->poStatus = '';
        $this->paymentStatus = '';
        $this->transportFee = 0;
        $this->ppnPercent = 0;
        $this->ppnValue = 0;
        $this->pph = 0;
        $this->pphValue = 0;
        $this->pbbkb = 0;
        $this->pbbkbValue = 0;
        $this->discPercent = 0;
        $this->discVal = 0;
        $this->total = 0;
        $this->reamaining = 0;
        $this->top = '';
        $this->noPrint = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
      
    }

    public function setPoId($aPoId)
    {
        $this->poId = $this->db->escape_str($aPoId);
    }
    public function getPoId()
    {
        return $this->poId;
    }
    public function setPoNo($aPoNo)
    {
        $this->poNo = $this->db->escape_str($aPoNo);
    }
    public function getPoNo()
    {
        return $this->poNo;
    }
    public function setPrId($aPrId)
    {
        $this->prId = $this->db->escape_str($aPrId);
    }
    public function getPrId()
    {
        return $this->prId;
    }
    public function setPrNo($aPrNo)
    {
        $this->prNo = $this->db->escape_str($aPrNo);
    }
    public function getPrNo()
    {
        return $this->prNo;
    }
    public function setPoDate($aPoDate)
    {
        $this->poDate = $this->db->escape_str($aPoDate);
    }
    public function getPoDate()
    {
        return $this->poDate;
    }
    public function setRequester($aRequester)
    {
        $this->requester = $this->db->escape_str($aRequester);
    }
    public function getRequester()
    {
        return $this->requester;
    }
    public function setProjectId($aProjectId)
    {
        $this->projectId = $this->db->escape_str($aProjectId);
    }
    public function getProjectId()
    {
        return $this->projectId;
    }
    public function setProjectName($aProjectName)
    {
        $this->projectName = $this->db->escape_str($aProjectName);
    }
    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setCompanyId($aCompanyId)
    {
        $this->companyId = $this->db->escape_str($aCompanyId);
    }
    public function getCompanyId()
    {
        return $this->companyId;
    }
    public function setCompanyName($aCompanyName)
    {
        $this->companyName = $this->db->escape_str($aCompanyName);
    }
    public function getCompanyName()
    {
        return $this->companyName;
    }
    public function setSupplierId($aSupplierId)
    {
        $this->supplierId = $this->db->escape_str($aSupplierId);
    }
    public function getSupplierId()
    {
        return $this->supplierId;
    }
    public function setSupplierName($aSupplierName)
    {
        $this->supplierName = $this->db->escape_str($aSupplierName);
    }
    public function getSupplierName()
    {
        return $this->supplierName;
    }
    public function setDeliveryAddress($aDeliveryAddress)
    {
        $this->deliveryAddress = $this->db->escape_str($aDeliveryAddress);
    }
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }
    public function setPoInfo($aPoInfo)
    {
        $this->poInfo = $this->db->escape_str($aPoInfo);
    }
    public function getPoInfo()
    {
        return $this->poInfo;
    }
    public function setCurrencyId($aCurrencyId)
    {
        $this->currencyId = $this->db->escape_str($aCurrencyId);
    }
    public function getCurrencyId()
    {
        return $this->currencyId;
    }
    public function setPoStatus($aPoStatus)
    {
        $this->poStatus = $this->db->escape_str($aPoStatus);
    }
    public function getPoStatus()
    {
        return $this->poStatus;
    }
    public function setPaymentStatus($aPaymentStatus)
    {
        $this->paymentStatus = $this->db->escape_str($aPaymentStatus);
    }
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }
    public function setTransportFee($aTransportFee)
    {
        $this->transportFee = $this->db->escape_str($aTransportFee);
    }
    public function getTransportFee()
    {
        return $this->transportFee;
    }
    public function setPpnPercent($aPpnPercent)
    {
        $this->ppnPercent = $this->db->escape_str($aPpnPercent);
    }
    public function getPpnPercent()
    {
        return $this->ppnPercent;
    }
    public function setPpnValue($aPpnValue)
    {
        $this->ppnValue = $this->db->escape_str($aPpnValue);
    }
    public function getPpnValue()
    {
        return $this->ppnValue;
    }
    public function setPph($aPph)
    {
        $this->pph = $this->db->escape_str($aPph);
    }
    public function getPph()
    {
        return $this->pph;
    }
    public function setPphValue($aPphValue)
    {
        $this->pphValue = $this->db->escape_str($aPphValue);
    }
    public function getPphValue()
    {
        return $this->pphValue;
    }
    public function setPbbkb($aPbbkb)
    {
        $this->pbbkb = $this->db->escape_str($aPbbkb);
    }
    public function getPbbkb()
    {
        return $this->pbbkb;
    }
    public function setPbbkbValue($aPbbkbValue)
    {
        $this->pbbkbValue = $this->db->escape_str($aPbbkbValue);
    }
    public function getPbbkbValue()
    {
        return $this->pbbkbValue;
    }
    public function setDiscPercent($aDiscPercent)
    {
        $this->discPercent = $this->db->escape_str($aDiscPercent);
    }
    public function getDiscPercent()
    {
        return $this->discPercent;
    }
    public function setDiscVal($aDiscVal)
    {
        $this->discVal = $this->db->escape_str($aDiscVal);
    }
    public function getDiscVal()
    {
        return $this->discVal;
    }
    public function setTotal($aTotal)
    {
        $this->total = $this->db->escape_str($aTotal);
    }
    public function getTotal()
    {
        return $this->total;
    }
    public function setReamaining($aReamaining)
    {
        $this->reamaining = $this->db->escape_str($aReamaining);
    }
    public function getReamaining()
    {
        return $this->reamaining;
    }
    public function setTop($aTop)
    {
        $this->top = $this->db->escape_str($aTop);
    }
    public function getTop()
    {
        return $this->top;
    }
    public function setNoPrint($aNoPrint)
    {
        $this->noPrint = $this->db->escape_str($aNoPrint);
    }
    public function getNoPrint()
    {
        return $this->noPrint;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

    function get_po()
    {
        $sql ='SELECT a.po_id,a.po_no,a.pr_id,a.pr_no,a.po_date,a.requester,a.company_name,a.supplier_id,a.supplier_name,a.delivery_address,a.po_info,a.po_status,a.transport_fee,a.ppn_percent,a.ppn_value,a.total,b.project_name FROM trn_po_01 a LEFT JOIN db_bumbu_master.mst_project b on a.project_id=b.project_id ORDER BY a.po_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_pr_header(){
        $sql  ="SELECT MAX(a.pr_id)pr_id,MAX(a.pr_info)pr_info FROM db_bumbu_transaction.trn_pr_01 a 
                WHERE a.pr_id IN (";
        foreach ($this->session->userdata('new_po')['items'] as $key => $value) {
            $sql .= "$value[det_id],";            
        }
        $sql .= "'') ";

        $query = $this->dbpurch->query($sql);
        return $query->row();
    }

    function get_pr_detail(){
        $sql  ="SELECT a.pr_id,a.items_id,a.items_name,SUM(a.qty)qty,b.price FROM db_bumbu_transaction.trn_pr_02 a 
                LEFT JOIN db_bumbu_master.mst_items b ON a.items_id=b.items_id
                WHERE a.pr_id IN (";
        foreach ($this->session->userdata('new_po')['items'] as $key => $value) {
            $sql .= "$value[det_id],";            
        }
        $sql .= "'') GROUP BY a.items_id";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_no_po_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(po_no,10,4))+1,4,'0'),'0001') nomor_dok, po_no FROM `trn_po_01` WHERE SUBSTRING(po_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    function get_id_po(){
        $query = $this->dbpurch->query("SELECT IFNULL(MAX(po_id),0)+1 po_id FROM `db_bumbu_transaction`.`trn_po_01` ")->row();
        return $query;
    }

    public function insert()
    {
        if($this->poId =='' || $this->poId == NULL ){
            $this->poId = 0;
        }
        if($this->poNo =='' || $this->poNo == NULL ){
            $this->poNo = '';
        }
        if($this->prId =='' || $this->prId == NULL ){
            $this->prId = 0;
        }
        if($this->prNo =='' || $this->prNo == NULL ){
            $this->prNo = '';
        }
        if($this->poDate =='' || $this->poDate == NULL ){
            $this->poDate = "0000-00-00";
        }
        if($this->requester =='' || $this->requester == NULL ){
            $this->requester = '';
        }
        if($this->projectId =='' || $this->projectId == NULL ){
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL ){
            $this->projectName = '';
        }
        if($this->companyId =='' || $this->companyId == NULL ){
            $this->companyId = 0;
        }
        if($this->companyName =='' || $this->companyName == NULL ){
            $this->companyName = '';
        }
        if($this->supplierId =='' || $this->supplierId == NULL ){
            $this->supplierId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL ){
            $this->supplierName = '';
        }
        if($this->deliveryAddress =='' || $this->deliveryAddress == NULL ){
            $this->deliveryAddress = '';
        }
        if($this->poInfo =='' || $this->poInfo == NULL ){
            $this->poInfo = '';
        }
        if($this->currencyId =='' || $this->currencyId == NULL ){
            $this->currencyId = 0;
        }
        if($this->poStatus =='' || $this->poStatus == NULL ){
            $this->poStatus = '';
        }
        if($this->paymentStatus =='' || $this->paymentStatus == NULL ){
            $this->paymentStatus = '';
        }
        if($this->transportFee =='' || $this->transportFee == NULL ){
            $this->transportFee = 0;
        }
        if($this->ppnPercent =='' || $this->ppnPercent == NULL ){
            $this->ppnPercent = 0;
        }
        if($this->ppnValue =='' || $this->ppnValue == NULL ){
            $this->ppnValue = 0;
        }
        if($this->pph =='' || $this->pph == NULL ){
            $this->pph = 0;
        }
        if($this->pphValue =='' || $this->pphValue == NULL ){
            $this->pphValue = 0;
        }
        if($this->pbbkb =='' || $this->pbbkb == NULL ){
            $this->pbbkb = 0;
        }
        if($this->pbbkbValue =='' || $this->pbbkbValue == NULL ){
            $this->pbbkbValue = 0;
        }
        if($this->discPercent =='' || $this->discPercent == NULL ){
            $this->discPercent = 0;
        }
        if($this->discVal =='' || $this->discVal == NULL ){
            $this->discVal = 0;
        }
        if($this->total =='' || $this->total == NULL ){
            $this->total = 0;
        }
        if($this->reamaining =='' || $this->reamaining == NULL ){
            $this->reamaining = 0;
        }
        if($this->top =='' || $this->top == NULL ){
            $this->top = '';
        }
        if($this->noPrint =='' || $this->noPrint == NULL ){
            $this->noPrint = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL ){
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL ){
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL ){
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL ){
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'po_id,'; 
        $stQuery .=   'po_no,'; 
        $stQuery .=   'pr_id,'; 
        $stQuery .=   'pr_no,'; 
        $stQuery .=   'po_date,'; 
        // $stQuery .=   'requester,'; 
        $stQuery .=   'project_id,'; 
        $stQuery .=   'project_name,'; 
        $stQuery .=   'company_id,'; 
        $stQuery .=   'company_name,'; 
        $stQuery .=   'supplier_id,'; 
        $stQuery .=   'supplier_name,'; 
        $stQuery .=   'delivery_address,'; 
        $stQuery .=   'po_info,'; 
        // $stQuery .=   'currency_id,'; 
        $stQuery .=   'po_status,'; 
        $stQuery .=   'payment_status,'; 
        // $stQuery .=   'transport_fee,'; 
        // $stQuery .=   'ppn_percent,'; 
        // $stQuery .=   'ppn_value,'; 
        // $stQuery .=   'pph,'; 
        // $stQuery .=   'pph_value,'; 
        // $stQuery .=   'pbbkb,'; 
        // $stQuery .=   'pbbkb_value,'; 
        // $stQuery .=   'disc_percent,'; 
        $stQuery .=   'disc_val,'; 
        $stQuery .=   'total,'; 
        $stQuery .=   'reamaining,'; 
        // $stQuery .=   'top,'; 
        // $stQuery .=   'no_print,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->poId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->poNo).'",'; 
        $stQuery .=   $this->db->escape_str($this->prId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->prNo).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->poDate).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->requester).'",'; 
        $stQuery .=   $this->db->escape_str($this->projectId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->projectName).'",'; 
        $stQuery .=   $this->db->escape_str($this->companyId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->companyName).'",'; 
        $stQuery .=   $this->db->escape_str($this->supplierId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->supplierName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->deliveryAddress).'",';
        $stQuery .=   '"'.$this->db->escape_str($this->poInfo).'",'; 
        // $stQuery .=   $this->db->escape_str($this->currencyId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->poStatus).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->paymentStatus).'",'; 
        // $stQuery .=   $this->db->escape_str($this->transportFee).','; 
        // $stQuery .=   $this->db->escape_str($this->ppnPercent).','; 
        // $stQuery .=   $this->db->escape_str($this->ppnValue).','; 
        // $stQuery .=   $this->db->escape_str($this->pph).','; 
        // $stQuery .=   $this->db->escape_str($this->pphValue).','; 
        // $stQuery .=   $this->db->escape_str($this->pbbkb).','; 
        // $stQuery .=   $this->db->escape_str($this->pbbkbValue).','; 
        // $stQuery .=   $this->db->escape_str($this->discPercent).','; 
        $stQuery .=   $this->db->escape_str($this->discVal).','; 
        $stQuery .=   $this->db->escape_str($this->total).','; 
        $stQuery .=   $this->db->escape_str($this->reamaining).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->top).'",'; 
        // $stQuery .=   $this->db->escape_str($this->noPrint).','; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 

        return $this->poNo;
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('po_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    function act_delete_js($po_id){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_po_01` SET `po_status` = 'Reject' WHERE `po_id` = '".$po_id."'");
    }

    function act_force_js($po_id){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_po_01` SET `po_status` = 'Force Close' WHERE `po_id` = '".$po_id."'");
    }

    function act_reject_js($po_id){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_po_01` SET `po_status` = 'Reject' WHERE `po_id` = '".$po_id."'");
    }

    function update_status_incoming($no_po,$status){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_po_01` SET `po_status` = '".$status."' WHERE `po_no` = '".$no_po."'");
    }

    function getPoPayment($supplier_id){
        $sql    = "SELECT a.po_id,a.po_no,a.po_date,a.total,a.supplier_name,sum(b.total) grand_total FROM trn_po_01 a 
                    LEFT JOIN trn_po_payment_02 b on b.po_id=a.po_id AND is_active=1
                    WHERE a.payment_status IN ('Process','Outstanding')
                    AND a.po_status NOT IN ('Reject','Force Close') AND supplier_id='".$supplier_id."' GROUP BY a.po_id ORDER BY a.po_id";
        return $this->dbpurch->query($sql)->result_array();
    }

    function update_payment($po_id,$status){
        $sql    = "UPDATE db_bumbu_transaction.trn_po_01 
                    SET
                    payment_status = '".$status."',
                    pic_edit = '".$this->current_user['user_id']."', 
                    edit_time = '".dbnow()."'
                    WHERE
                    po_id = '".$po_id."'";
        $this->dbpurch->query($sql);
    }

    function getByDoc($noDoc){
        return $this->dbpurch->query('SELECT po_info as remarks FROM trn_po_01 WHERE po_no="'.$noDoc.'"');
    }

    function print_header($id){
        return $this->dbpurch->query("SELECT a.po_no,a.po_date,a.project_id,b.project_name,a.company_id,c.company_name,a.supplier_id,d.supplier_name,a.delivery_address,a.po_info,a.total FROM trn_po_01 a
LEFT JOIN db_bumbu_master.mst_project b ON a.project_id=b.project_id
LEFT JOIN db_bumbu_master.mst_company c ON a.company_id=c.company_id
LEFT JOIN db_bumbu_master.mst_supplier d ON a.supplier_id=d.supplier_id WHERE a.po_id='".$id."'")->row();
    }

} 
// 1. Penambahan Discount untuk Header PO 
// 2. Penambahan Print PO
?>