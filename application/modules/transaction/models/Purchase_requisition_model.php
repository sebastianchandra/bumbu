<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_requisition_model extends CI_Model
{
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_pr_01';
    private $prId;
    private $poNo;
    private $prNo;
    private $prDate;
    private $requester;
    private $projectId;
    private $projectName;
    private $companyId;
    private $companyName;
    private $supplierId;
    private $supplierName;
    private $fileQuotation;
    private $deliveryAddress;
    private $prInfo;
    private $currencyId;
    private $prStatus;
    private $transportFee;
    private $ppnPercent;
    private $ppnValue;
    private $top;
    private $pph;
    private $pphValue;
    private $pbbkb;
    private $pbbkbValue;
    private $total;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;
    private $approveLevel;

    public function __construct()
    {
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->prId = 0;
        $this->poNo = '';
        $this->prNo = '';
        $this->prDate = "0000-00-00";
        $this->requester = '';
        $this->projectId = 0;
        $this->projectName = '';
        $this->companyId = 0;
        $this->companyName = '';
        $this->supplierId = 0;
        $this->supplierName = '';
        $this->fileQuotation = 0;
        $this->deliveryAddress = '';
        $this->prInfo = '';
        $this->currencyId = 0;
        $this->prStatus = '';
        $this->transportFee = 0;
        $this->ppnPercent = 0;
        $this->ppnValue = 0;
        $this->top = '';
        $this->pph = 0;
        $this->pphValue = 0;
        $this->pbbkb = 0;
        $this->pbbkbValue = 0;
        $this->total = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
        $this->approveLevel = 0;
    }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setPrId($aPrId)
     {
        $this->prId = $this->db->escape_str($aPrId);
     }
     public function getPrId()
     {
        return $this->prId;
     }
     public function setPoNo($aPoNo)
     {
        $this->poNo = $this->db->escape_str($aPoNo);
     }
     public function getPoNo()
     {
        return $this->poNo;
     }
     public function setPrNo($aPrNo)
     {
        $this->prNo = $this->db->escape_str($aPrNo);
     }
     public function getPrNo()
     {
        return $this->prNo;
     }
     public function setPrDate($aPrDate)
     {
        $this->prDate = $this->db->escape_str($aPrDate);
     }
     public function getPrDate()
     {
        return $this->prDate;
     }
     public function setRequester($aRequester)
     {
        $this->requester = $this->db->escape_str($aRequester);
     }
     public function getRequester()
     {
        return $this->requester;
     }
     public function setProjectId($aProjectId)
     {
        $this->projectId = $this->db->escape_str($aProjectId);
     }
     public function getProjectId()
     {
        return $this->projectId;
     }
     public function setProjectName($aProjectName)
     {
        $this->projectName = $this->db->escape_str($aProjectName);
     }
     public function getProjectName()
     {
        return $this->projectName;
     }
     public function setCompanyId($aCompanyId)
     {
        $this->companyId = $this->db->escape_str($aCompanyId);
     }
     public function getCompanyId()
     {
        return $this->companyId;
     }
     public function setCompanyName($aCompanyName)
     {
        $this->companyName = $this->db->escape_str($aCompanyName);
     }
     public function getCompanyName()
     {
        return $this->companyName;
     }
     public function setSupplierId($aSupplierId)
     {
        $this->supplierId = $this->db->escape_str($aSupplierId);
     }
     public function getSupplierId()
     {
        return $this->supplierId;
     }
     public function setSupplierName($aSupplierName)
     {
        $this->supplierName = $this->db->escape_str($aSupplierName);
     }
     public function getSupplierName()
     {
        return $this->supplierName;
     }
     public function setFileQuotation($aFileQuotation)
     {
        $this->fileQuotation = $this->db->escape_str($aFileQuotation);
     }
     public function getFileQuotation()
     {
        return $this->fileQuotation;
     }
     public function setDeliveryAddress($aDeliveryAddress)
     {
        $this->deliveryAddress = $this->db->escape_str($aDeliveryAddress);
     }
     public function getDeliveryAddress()
     {
        return $this->deliveryAddress;
     }
     public function setPrInfo($aPrInfo)
     {
        $this->prInfo = $this->db->escape_str($aPrInfo);
     }
     public function getPrInfo()
     {
        return $this->prInfo;
     }
     public function setCurrencyId($aCurrencyId)
     {
        $this->currencyId = $this->db->escape_str($aCurrencyId);
     }
     public function getCurrencyId()
     {
        return $this->currencyId;
     }
     public function setPrStatus($aPrStatus)
     {
        $this->prStatus = $this->db->escape_str($aPrStatus);
     }
     public function getPrStatus()
     {
        return $this->prStatus;
     }
     public function setTransportFee($aTransportFee)
     {
        $this->transportFee = $this->db->escape_str($aTransportFee);
     }
     public function getTransportFee()
     {
        return $this->transportFee;
     }
     public function setPpnPercent($aPpnPercent)
     {
        $this->ppnPercent = $this->db->escape_str($aPpnPercent);
     }
     public function getPpnPercent()
     {
        return $this->ppnPercent;
     }
     public function setPpnValue($aPpnValue)
     {
        $this->ppnValue = $this->db->escape_str($aPpnValue);
     }
     public function getPpnValue()
     {
        return $this->ppnValue;
     }
     public function setTop($aTop)
     {
        $this->top = $this->db->escape_str($aTop);
     }
     public function getTop()
     {
        return $this->top;
     }
     public function setPph($aPph)
     {
        $this->pph = $this->db->escape_str($aPph);
     }
     public function getPph()
     {
        return $this->pph;
     }
     public function setPphValue($aPphValue)
     {
        $this->pphValue = $this->db->escape_str($aPphValue);
     }
     public function getPphValue()
     {
        return $this->pphValue;
     }
     public function setPbbkb($aPbbkb)
     {
        $this->pbbkb = $this->db->escape_str($aPbbkb);
     }
     public function getPbbkb()
     {
        return $this->pbbkb;
     }
     public function setPbbkbValue($aPbbkbValue)
     {
        $this->pbbkbValue = $this->db->escape_str($aPbbkbValue);
     }
     public function getPbbkbValue()
     {
        return $this->pbbkbValue;
     }
     public function setTotal($aTotal)
     {
        $this->total = $this->db->escape_str($aTotal);
     }
     public function getTotal()
     {
        return $this->total;
     }
     public function setPicInput($aPicInput)
     {
        $this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
        return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
        $this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
        return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
        $this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
        return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
        $this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
        return $this->editTime;
     }
     public function setApproveLevel($aApproveLevel)
     {
        $this->approveLevel = $this->db->escape_str($aApproveLevel);
     }
     public function getApproveLevel()
     {
        return $this->approveLevel;
     }

    function get_pr(){
        $sql    = "SELECT pr_id,po_no,pr_no,pr_date,requester,project_id,project_name,company_id,company_name,supplier_id,supplier_name,file_quotation,
                    delivery_address,pr_info,currency_id,pr_status,transport_fee,ppn_percent,ppn_value,top,pph,pph_value,pbbkb,pbbkb_value,total FROM
                    db_bumbu_transaction.trn_pr_01 order by pr_id DESC";
        return $this->dbpurch->query($sql)->result();
    }


    function get_nomor_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(pr_no,10,4))+1,4,'0'),'0001') nomor_dok, pr_no FROM `trn_pr_01` WHERE SUBSTRING(pr_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    function getDataCount()
    {
        $stQuery  = 'SELECT IFNULL(MAX(pr_id),0)+1 pr_id FROM trn_pr_01 '; 
        $query = $this->dbpurch->query($stQuery);
        return $query->row();
    }

    public function insert()
    {
        if($this->prId =='' || $this->prId == NULL )
        {
            $this->prId = 0;
        }
        if($this->poNo =='' || $this->poNo == NULL )
        {
            $this->poNo = '';
        }
        if($this->prNo =='' || $this->prNo == NULL )
        {
            $this->prNo = '';
        }
        if($this->prDate =='' || $this->prDate == NULL )
        {
            $this->prDate = "0000-00-00";
        }
        if($this->requester =='' || $this->requester == NULL )
        {
            $this->requester = '';
        }
        if($this->projectId =='' || $this->projectId == NULL )
        {
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL )
        {
            $this->projectName = '';
        }
        if($this->companyId =='' || $this->companyId == NULL )
        {
            $this->companyId = 0;
        }
        if($this->companyName =='' || $this->companyName == NULL )
        {
            $this->companyName = '';
        }
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->fileQuotation =='' || $this->fileQuotation == NULL )
        {
            $this->fileQuotation = 0;
        }
        if($this->deliveryAddress =='' || $this->deliveryAddress == NULL )
        {
            $this->deliveryAddress = '';
        }
        if($this->prInfo =='' || $this->prInfo == NULL )
        {
            $this->prInfo = '';
        }
        if($this->currencyId =='' || $this->currencyId == NULL )
        {
            $this->currencyId = 0;
        }
        if($this->prStatus =='' || $this->prStatus == NULL )
        {
            $this->prStatus = '';
        }
        if($this->transportFee =='' || $this->transportFee == NULL )
        {
            $this->transportFee = 0;
        }
        if($this->ppnPercent =='' || $this->ppnPercent == NULL )
        {
            $this->ppnPercent = 0;
        }
        if($this->ppnValue =='' || $this->ppnValue == NULL )
        {
            $this->ppnValue = 0;
        }
        if($this->top =='' || $this->top == NULL )
        {
            $this->top = '';
        }
        if($this->pph =='' || $this->pph == NULL )
        {
            $this->pph = 0;
        }
        if($this->pphValue =='' || $this->pphValue == NULL )
        {
            $this->pphValue = 0;
        }
        if($this->pbbkb =='' || $this->pbbkb == NULL )
        {
            $this->pbbkb = 0;
        }
        if($this->pbbkbValue =='' || $this->pbbkbValue == NULL )
        {
            $this->pbbkbValue = 0;
        }
        if($this->total =='' || $this->total == NULL )
        {
            $this->total = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        if($this->approveLevel =='' || $this->approveLevel == NULL )
        {
            $this->approveLevel = 0;
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'pr_id,'; 
        $stQuery .=   'pr_no,'; 
        $stQuery .=   'pr_date,'; 
        $stQuery .=   'requester,'; 
        $stQuery .=   'company_id,'; 
        $stQuery .=   'company_name,'; 
        $stQuery .=   'pr_info,'; 
        $stQuery .=   'pr_status,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->prId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->prNo).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->prDate).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->requester).'",'; 
        $stQuery .=   $this->db->escape_str($this->companyId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->companyName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->prInfo).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->prStatus).'",'; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
        return $this->prNo;
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('pr_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    public function update($id)
    {
        if($this->prId =='' || $this->prId == NULL )
        {
            $this->prId = 0;
        }
        if($this->poNo =='' || $this->poNo == NULL )
        {
            $this->poNo = '';
        }
        if($this->prNo =='' || $this->prNo == NULL )
        {
            $this->prNo = '';
        }
        if($this->prDate =='' || $this->prDate == NULL )
        {
            $this->prDate = "0000-00-00";
        }
        if($this->requester =='' || $this->requester == NULL )
        {
            $this->requester = '';
        }
        if($this->projectId =='' || $this->projectId == NULL )
        {
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL )
        {
            $this->projectName = '';
        }
        if($this->companyId =='' || $this->companyId == NULL )
        {
            $this->companyId = 0;
        }
        if($this->companyName =='' || $this->companyName == NULL )
        {
            $this->companyName = '';
        }
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->fileQuotation =='' || $this->fileQuotation == NULL )
        {
            $this->fileQuotation = 0;
        }
        if($this->deliveryAddress =='' || $this->deliveryAddress == NULL )
        {
            $this->deliveryAddress = '';
        }
        if($this->prInfo =='' || $this->prInfo == NULL )
        {
            $this->prInfo = '';
        }
        if($this->currencyId =='' || $this->currencyId == NULL )
        {
            $this->currencyId = 0;
        }
        if($this->prStatus =='' || $this->prStatus == NULL )
        {
            $this->prStatus = '';
        }
        if($this->transportFee =='' || $this->transportFee == NULL )
        {
            $this->transportFee = 0;
        }
        if($this->ppnPercent =='' || $this->ppnPercent == NULL )
        {
            $this->ppnPercent = 0;
        }
        if($this->ppnValue =='' || $this->ppnValue == NULL )
        {
            $this->ppnValue = 0;
        }
        if($this->top =='' || $this->top == NULL )
        {
            $this->top = '';
        }
        if($this->pph =='' || $this->pph == NULL )
        {
            $this->pph = 0;
        }
        if($this->pphValue =='' || $this->pphValue == NULL )
        {
            $this->pphValue = 0;
        }
        if($this->pbbkb =='' || $this->pbbkb == NULL )
        {
            $this->pbbkb = 0;
        }
        if($this->pbbkbValue =='' || $this->pbbkbValue == NULL )
        {
            $this->pbbkbValue = 0;
        }
        if($this->total =='' || $this->total == NULL )
        {
            $this->total = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        if($this->approveLevel =='' || $this->approveLevel == NULL )
        {
            $this->approveLevel = 0;
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'pr_date ="'.$this->db->escape_str($this->prDate).'",'; 
        $stQuery .=   'requester ="'.$this->db->escape_str($this->requester).'",'; 
        $stQuery .=   'company_id ='.$this->db->escape_str($this->companyId).','; 
        $stQuery .=   'company_name ="'.$this->db->escape_str($this->companyName).'",'; 
        $stQuery .=   'pr_info ="'.$this->db->escape_str($this->prInfo).'",';         
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'pr_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
    }

    function act_delete_js(){
        $sql = "UPDATE trn_pr_01 SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE pr_id = '".$this->input->post('pr_id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

    function act_reject_js(){
        $sql = "UPDATE trn_pr_01 SET pr_status='Reject', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE pr_id = '".$this->input->post('pr_id')."'";
        //test($sql,1);
        $query = $this->dbpurch->query($sql);
        return $query;
    }

    function getByPrProcess(){
        $sql    = "SELECT pr_id,po_no,pr_no,pr_date,requester,project_id,project_name,company_id,company_name,supplier_id,supplier_name,file_quotation,
                    delivery_address,pr_info,currency_id,pr_status,transport_fee,ppn_percent,ppn_value,top,pph,pph_value,pbbkb,pbbkb_value,total FROM
                    db_bumbu_transaction.trn_pr_01 WHERE pr_status='Process' ORDER BY pr_id DESC";
        return $this->dbpurch->query($sql)->result_array();
    }

    function updateNoPo($noPo,$prId){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_pr_01` SET `po_no` = '".$noPo."', `pr_status` = 'Close' WHERE `pr_id` = '".$prId."'");
    }

    function act_update_po($po_no){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_pr_01` SET `po_no` = '', `pr_status` = 'Process' WHERE `po_no` = '".$po_no."'");
    }

    function print_header($id){
        return $this->dbpurch->query("SELECT a.pr_no,a.company_id,b.company_name,a.pr_date,a.requester,a.pr_info FROM trn_pr_01 a LEFT JOIN db_bumbu_master.mst_company b ON a.company_id=b.company_id WHERE a.pr_id='".$id."'")->row();

    }

}   

// Penambahan PR Print

?>