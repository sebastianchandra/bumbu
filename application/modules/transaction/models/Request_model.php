<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_model extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        
    }

    function get_pr()
    {
        $sql ='SELECT pr_id, pr_no, pr_date, project_name, company_name, supplier_name, requester, pr_status FROM trn_pr_01 ORDER BY pr_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_pr_approve_view()
    {
        $sql ="SELECT a.pr_id, a.pr_no, a.pr_date, a.project_name, a.company_name, a.supplier_name, a.requester, a.pr_status, 
                (SELECT COUNT(b.pr_no) jumlah FROM trn_pr_approval b WHERE b.pr_id=a.pr_id AND b.user_id='".$this->current_user['user_id']."') jml_approve
                FROM trn_pr_01 a
                ORDER BY a.pr_id DESC";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    // function user_group_max($id){
    //     $sql    = "SELECT id_user_group FROM mst_user_group_permissions WHERE user_id='".$id."'";
    //     $query = $this->db->query($sql);
    //     return $query->num_rows();
    // }

    function get_pr_approve()
    {
        // $max = $this->user_group_max($this->current_user['user_id']);
        // $no  = 0;   
        $sql ="SELECT a.request_id,a.request_no,a.request_date,a.company_name,a.dept,b.name name_dept,a.requester,a.remark,COUNT(c.pr_id) total,a.status_approve,a.project_id is_project, d.project_name
                FROM trn_request_01 a 
                LEFT JOIN db_master.mst_user_group b ON a.dept=b.id_user_group 
                LEFT JOIN trn_pr_01 c ON a.request_id=c.request_id
                LEFT JOIN db_master.mst_project d ON d.project_id=a.project_id
                GROUP BY a.request_id ORDER BY a.request_id DESC";
        // test($sql,1);
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function act_form(){
        $new_req   = $this->session->userdata('new_req');
        
        $periode        = date('Y').date('m');
        $data_id        = $this->get_id();
        $rq_id          = $data_id->rq_id;
        $data_no        = $this->get_nomor_dok($periode);
        $nomor_dok      = $data_no->nomor_dok;
        $rq_no          = 'RQ'.$this->current_user['company_code'].$periode.$nomor_dok;

        //test($new_req,1);

        $subtotal=0;
        $total_detail=0;
        $ppn_value=0;
        $total=0;

        $is_oke     = 0;
        $is_max     = 0;
        $notif      = array();
        $error      = array();

        // test($new_req['budget'],1);
        if(isset($new_req['items'])){
            // test($this->input->post('is_project'),1);
            if($this->current_user['company_id']=='1' AND $this->input->post('is_project')!='1'){
                $budget         = $new_req['budget'];
                $jm_budget      = count($budget);

                // test($budget,0);
                foreach ($budget as $key => $value) {
                    // test($value['total_qty'].' - '.$value['qty_remaining'],0);
                    if($value['total_qty'] <= $value['qty_remaining']){
                        $is_oke     = $is_oke+1;
                        $notif[]    = array("mbp_name"      => $value['mbp_name'],
                                            "total_qty"     => $value['total_qty'],
                                            "qty_remaining" => $value['qty_remaining']);
                    }else{
                        $is_max     = $is_max+1;
                        $error[]    = array("mbp_name"      => $value['mbp_name'],
                                            "total_qty"     => $value['total_qty'],
                                            "qty_remaining" => $value['qty_remaining']);
                        // test($error,1);
                    }
                }
                // test($is_oke.' - '.$is_max,0);
                // test($error,1);

                if($is_oke==$jm_budget){
                    $items          = $new_req['items'];
                    foreach ($items as $key => $value) {
                        $item_id        = $this->security->xss_clean($this->db->escape_str($value['item_id']));
                        $item_name      = $this->security->xss_clean($this->db->escape_str($value['item_name']));
                        $item_info      = $this->security->xss_clean($this->db->escape_str($value['item_info']));
                        $item_qty       = str_replace(',','',$this->security->xss_clean($this->db->escape_str($value['item_qty'])));

                        $sql_detail     = "INSERT INTO trn_request_02 (request_id,items_id,qty,remarks,is_active,pic_input,input_time)VALUES 
                                            ('".$rq_id."','".$item_id."','".$item_qty."','".$item_info."','1','".$this->current_user['user_id']."','".dbnow()."')";

                        $query          = $this->dbpurch->query($sql_detail);
                    }
                }else{
                    return array("keterangan" => "Max Pr", "items" => $error);
                }
            }else{
                $items          = $new_req['items'];
                foreach ($items as $key => $value) {
                    $item_id        = $this->security->xss_clean($this->db->escape_str($value['item_id']));
                    $item_name      = $this->security->xss_clean($this->db->escape_str($value['item_name']));
                    $item_info      = $this->security->xss_clean($this->db->escape_str($value['item_info']));
                    $item_qty       = str_replace(',','',$this->security->xss_clean($this->db->escape_str($value['item_qty'])));

                    $sql_detail     = "INSERT INTO trn_request_02 (request_id,items_id,qty,remarks,is_active,pic_input,input_time)VALUES 
                                        ('".$rq_id."','".$item_id."','".$item_qty."','".$item_info."','1','".$this->current_user['user_id']."','".dbnow()."')";

                    $query          = $this->dbpurch->query($sql_detail);
                }
            }
        }

        $date           = $this->security->xss_clean($this->db->escape_str($this->input->post('date')));
        $requester      = $this->security->xss_clean($this->db->escape_str($this->input->post('requester')));
        $company_name   = $this->security->xss_clean($this->db->escape_str($this->current_user['company_name']));
        $company_id     = $this->security->xss_clean($this->db->escape_str($this->current_user['company_id']));
        $info           = $this->security->xss_clean($this->db->escape_str($this->input->post('info')));
        $dept           = $this->security->xss_clean($this->db->escape_str($this->input->post('dept')));

        $is_project     = $this->security->xss_clean($this->db->escape_str($this->input->post('is_project')));

        if($this->input->post('ref_number')!=''){
            $ref_number     = "'".$this->security->xss_clean($this->db->escape_str($this->input->post('ref_number')))."'";
        }else{
            $ref_number     = 'NULL';
        }

        if($this->input->post('date_req')!=''){
            $date_req       = "'".$this->security->xss_clean($this->db->escape_str($this->input->post('date_req')))."'";
        }else{
            $date_req       = 'NULL';
        }

        $email      = $this->get_email($dept,$company_id);

        $sql        = "INSERT INTO trn_request_01 (request_id,request_no,request_date,company_id,company_name,dept,requester,remark,pic_input,input_time,project_id
                        ,reference_no,required_date)VALUES 
                        ('".$rq_id."','".$rq_no."','".$date."','".$company_id."','".$company_name."','".$dept."','".$requester."','".$info."','".$this->current_user['user_id']."','".dbnow()."','".$is_project."',".$ref_number.",".$date_req.")";
        
        $query = $this->dbpurch->query($sql);

        $sql_email  = "INSERT INTO trn_approval_email (doc_id, doc_date,doc_no, doc_type, is_send, email_to)VALUES
                            ('".$rq_id."','".$date."','".$rq_no."','1','0','".$email->email."')";
        // test($sql_email,1);
        $query_email = $this->dbpurch->query($sql_email);
        
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return array("nomor_rq" => $rq_no, "items" => $notif);
        }
    }

    function get_email($dept,$company_id){

        $query = $this->db->query("SELECT a.user_id,a.id_user_group,b.name,b.nip,b.email FROM mst_user_group_permissions a JOIN mst_user b ON a.user_id=b.user_id
            JOIN mst_user_company c ON c.user_id=a.user_id AND c.company_id='".$company_id."' WHERE a.id_user_group='".$dept."' AND b.id_user_level='5' AND b.is_active='1'")->row();
                
        return $query;
    }

    function detail_pr($id){
        $query = $this->dbpurch->query("SELECT a.request_id,a.request_no,a.request_date,a.dept,b.name name_dept,a.requester,a.remark,a.company_id,c.company_name,a.project_id is_project,a.reference_no,a.required_date
                FROM trn_request_01 a 
                LEFT JOIN db_master.mst_company c ON c.company_id=a.company_id
                LEFT JOIN db_master.mst_user_group b ON a.dept=b.id_user_group 
                where a.request_id='".$id."'")->row();
                
        return $query;
    }

    function detail_items_pr($id){
        $query = $this->dbpurch->query("SELECT a.request_02_id,a.request_id,a.items_id,b.items_name,a.qty,a.remarks 
                    FROM trn_request_02 a, db_master.mst_items b WHERE a.items_id=b.items_id AND a.request_id='".$id."' and a.is_active='1'")->result();
                
        return $query;
    }

    function total_items_pr($id){
        $query = $this->dbpurch->query("SELECT a.request_id,a.items_id,b.items_name,a.qty,a.remarks 
                    FROM trn_request_02 a, db_master.mst_items b WHERE a.items_id=b.items_id AND a.request_id='".$id."' and a.is_active='1'")->num_rows();
                
        return $query;
    }

    function get_id(){
        $query = $this->dbpurch->query("SELECT IFNULL(MAX(request_id)+1,1) rq_id FROM trn_request_01")->row();
        return $query;
    }

    function get_nomor_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(request_no,10,4))+1,4,'0'),'0001') nomor_dok, request_no FROM trn_request_01 
                                        WHERE SUBSTRING(request_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    function get_rq_no($id){
        $query = $this->dbpurch->query("SELECT `request_no` FROM `trn_request_01` WHERE request_id='".$id."'")->row();
        return $query;
    }

    function cek($request_id,$item_id,$det_id){
        $query = $this->dbpurch->query("SELECT request_02_id,request_id,items_id,qty,remarks FROM trn_request_02 WHERE request_id='".$request_id."' AND items_id='".$item_id."' and request_02_id='".$det_id."'")->num_rows();
                
        return $query;
    }

    function act_edit(){
        $new_req = $this->session->userdata('new_req');

        // $delete = "DELETE FROM trn_request_02 WHERE request_id = '".$this->input->post('request_id')."'";
        // $query = $this->dbpurch->query($delete);

        if(isset($new_req['items'])){
            $items          = $new_req['items'];
            foreach ($items as $key => $value) {
                $det_id         = $this->security->xss_clean($this->db->escape_str($value['det_id']));
                $request_id     = $this->security->xss_clean($this->db->escape_str($this->input->post('request_id')));
                $item_id        = $this->security->xss_clean($this->db->escape_str($value['item_id']));
                $item_name      = $this->security->xss_clean($this->db->escape_str($value['item_name']));
                $item_info      = $this->security->xss_clean($this->db->escape_str($value['item_info']));
                $item_qty       = str_replace(',','',$this->security->xss_clean($this->db->escape_str($value['item_qty'])));

                $cek    = $this->cek($request_id,$item_id,$det_id);
                if($cek==1){
                    $sql_detail     = "UPDATE trn_request_02 SET 
                                            items_id = '".$item_id."', 
                                            qty = '".$item_qty."',
                                            remarks = '".$item_info."',
                                            pic_edit = '".$this->current_user['user_id']."',
                                            edit_time = '".dbnow()."'
                                            WHERE items_id = '".$item_id."' AND request_id = '".$request_id."' AND request_02_id = '".$det_id."'";
                }else{
                    $sql_detail     = "INSERT INTO trn_request_02 (request_id,items_id,qty,remarks,pic_input,input_time)VALUES 
                                    ('".$request_id."','".$item_id."','".$item_qty."','".$item_info."','".$this->current_user['user_id']."','".dbnow()."')";
                }
                //test($sql_detail,0);
                $query          = $this->dbpurch->query($sql_detail);
            }
        }
        //test('oke',1);

        $data_no        = $this->get_rq_no($this->security->xss_clean($this->db->escape_str($this->input->post('request_id'))));
        $nomor_dok      = substr($data_no->request_no,3,10);

        $rq_no          = 'RQ'.$this->current_user['company_code'].$nomor_dok;

        // if($this->security->xss_clean($this->db->escape_str($this->input->post('is_project')))==1){
        //     $is_project     = $this->security->xss_clean($this->db->escape_str($this->input->post('is_project')));
        // }else{
        //     $is_project     = 0;
        // }

        $is_project     = $this->security->xss_clean($this->db->escape_str($this->input->post('is_project')));

        if($this->input->post('ref_number')!=''){
            $ref_number     = "'".$this->security->xss_clean($this->db->escape_str($this->input->post('ref_number')))."'";
        }else{
            $ref_number     = 'NULL';
        }

        if($this->input->post('date_req')!=''){
            $date_req       = "'".$this->security->xss_clean($this->db->escape_str($this->input->post('date_req')))."'";
        }else{
            $date_req       = 'NULL';
        }

        $sql    = "UPDATE trn_request_01
                    SET request_no = '".$rq_no."',
                      request_date = '".$this->security->xss_clean($this->db->escape_str($this->input->post('date')))."',
                      dept = '".$this->security->xss_clean($this->db->escape_str($this->input->post('dept')))."',
                      requester = '".$this->security->xss_clean($this->db->escape_str($this->input->post('requester')))."',
                      remark = '".$this->security->xss_clean($this->db->escape_str($this->input->post('info')))."',
                      pic_edit = '".$this->current_user['user_id']."',
                      edit_time = '".dbnow()."',
                      project_id= '".$is_project."', 
                      reference_no = ".$ref_number." , 
                      required_date = ".$date_req."
                    WHERE request_id = '".$this->security->xss_clean($this->db->escape_str($this->input->post('request_id')))."'";

        $query = $this->dbpurch->query($sql);
        
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $rq_no; 
        }
    }

    function act_remove_detail($id){
        $sql    = "UPDATE trn_request_02 SET is_active = '0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' 
                    WHERE request_02_id = '".$id."'";
        $query = $this->dbpurch->query($sql);
    }

    function get_no_po_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(po_no,10,4))+1,4,'0'),'0001') nomor_dok, po_no FROM `trn_po_01` WHERE SUBSTRING(po_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    function get_id_po_dok(){
        $query = $this->dbpurch->query("SELECT IFNULL(MAX(po_id)+1,1) po_id FROM trn_po_01")->row();
        return $query;
    }

    function act_process(){

        $id_pr      = $this->security->xss_clean($this->db->escape_str($this->input->post('id_pr')));
        $no_pr      = $this->security->xss_clean($this->db->escape_str($this->input->post('no_pr')));

        $header     = $this->detail_pr($id_pr);
        $detail     = $this->detail_items_pr($id_pr);

        $periode    = date('Y').date('m');
        $data_no    = $this->get_no_po_dok($periode);
        $data_id    = $this->get_id_po_dok();
        
        ($header->company_name=='PT. LM System Indonesia')? $company = 'L' : $company = 'S'; 
        
        $po_no      = 'PO'.$company.$periode.$data_no->nomor_dok;
        $pr_id      = $header->pr_id;
        $pr_no      = $header->pr_no;
        $requester  = $header->requester;
        $project_id = $header->project_id;
        $project_name=$header->project_name;
        $company_name=$header->company_name;
        $supplier_id= $header->supplier_id;
        $supplier_name=$header->supplier_name;
        $delivery_address=$header->delivery_address;
        $po_info    = $header->pr_info;
        $transport_fee=$header->transport_fee;
        $ppn_percent= $header->ppn_percent;
        $ppn_value  = $header->ppn_value;
        $total      = $header->total;
        $currency_id= $header->currency_id;

        $sql    = "INSERT INTO trn_po_01 (po_id,po_no,pr_id,pr_no,po_date,requester,project_id,project_name,company_name,supplier_id,supplier_name,
                    delivery_address,po_info,currency_id,po_status,transport_fee,ppn_percent,ppn_value,total,pic_input,input_time)VALUES 
                    ('".$data_id->po_id."','".$po_no."','".$pr_id."','".$pr_no."','".dbnow()."','".$requester."','".$project_id."','".$project_name."',
                    '".$company_name."','".$supplier_id."','".$supplier_name."','".$delivery_address."','".$po_info."','".$currency_id."','New',
                    '".$transport_fee."','".$ppn_percent."','".$ppn_value."','".$total."','".$this->current_user['user_id']."','".dbnow()."')";

        $query = $this->dbpurch->query($sql);

        foreach ($detail as $key => $value) {
            $subtotal   = $value->qty*$value->price*$value->exchange_rate;
            $sql_detail     = "INSERT INTO trn_po_02(po_id,items_id,items_name,qty,price,exchange_rate,subtotal,remarks,pic_input,input_time)
                                VALUES 
                                ('".$data_id->po_id."','".$value->items_id."','".$value->items_name."','".$value->qty."','".$value->price."'
                                ,'".$value->exchange_rate."','".$subtotal."','".$value->remarks."','".$this->current_user['user_id']."'
                                ,'".dbnow()."')";

            $query          = $this->dbpurch->query($sql_detail);
        }

        $sql_update    = "UPDATE trn_pr_01 SET pr_status = 'Process' ,pic_edit = '".$this->current_user['user_id']."',edit_time = '".dbnow()."' WHERE pr_id = '".$id_pr."'";

        $query_update = $this->dbpurch->query($sql_update);

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $po_no; 
        }
    }

    function detail_approve($id){
        $query = $this->dbpurch->query("SELECT a.request_approval_id, a.request_id, a.request_no, a.user_id, a.date, a.note, a.is_approve, b.name, c.user_level_name
                                        FROM trn_request_approval a LEFT JOIN db_master.mst_user b ON a.user_id=b.user_id, db_master.mst_user_level c
                                        WHERE a.request_id='".$id."' AND c.id_user_level=b.id_user_level")->result();
    
        return $query;
    }

    function get_all_request($start=0,$length=10,$search='') {
        $sql = "SELECT a.request_id, a.request_no, a.request_date,a.requester,c.name dept,a.project_id,d.project_name,d.project_location FROM trn_request_01 a 
                LEFT JOIN db_master.mst_user_group c ON c.id_user_group=a.dept
                LEFT JOIN db_master.mst_project d ON d.project_id=a.project_id
                WHERE a.status_request='2' AND a.status_approve='1' AND (a.request_no LIKE '%".$search."%')
                order by a.request_no asc
                LIMIT ".$start.", ".$length."";

        $item = $this->dbpurch->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_count(){
        $sql ="SELECT COUNT(a.request_id) total FROM trn_request_01 a 
                LEFT JOIN db_master.mst_user_group c ON c.id_user_group=a.dept
                WHERE a.status_request='2' AND a.status_approve='1'
                order by a.request_no asc";
        $item = $this->dbpurch->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function get_count_display($start,$length,$search = false) {
        $str = '';
        if ($search) {
            $str = " AND (a.request_no LIKE '%".$search."%') ";
        }

        $sql = " SELECT COUNT(a.request_id) total FROM trn_request_01 a 
                LEFT JOIN db_master.mst_user_group c ON c.id_user_group=a.dept
                WHERE a.status_request='2' AND a.status_approve='1'  ".$str." ";
        $item = $this->dbpurch->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function get_request_approve()
    {
        $max = $this->user_group_max($this->current_user['user_id']);
        $no  = 0;
        $sql ="SELECT a.request_id,a.request_no,a.request_date,a.company_id,a.company_name,a.dept,b.name name_dept,a.requester,a.remark,COUNT(c.request_approval_id) total 
                FROM trn_request_01 a 
                LEFT JOIN db_master.mst_user_group b ON a.dept=b.id_user_group 
                LEFT JOIN trn_request_approval c ON c.request_id=a.request_id
                WHERE a.status_approve=0 AND a.dept IN (";
                foreach ($this->current_user['user_permission_group'] as $key => $value) {
                    $no  = $no+1;
                    $sql.= $value->id_user_group;
                    if($no!=$max){
                        $sql.= ',';
                    }
                }
        $sql.=") GROUP BY a.request_id ORDER BY a.request_id DESC";   
        // test($sql,1);     
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

     function act_request(){
        $id_pr      = $this->security->xss_clean($this->db->escape_str($this->input->post('id_pr')));
        $no_pr      = $this->security->xss_clean($this->db->escape_str($this->input->post('no_pr')));
        $date_pr    = $this->security->xss_clean($this->db->escape_str($this->input->post('date_pr')));
        $keterangan = $this->security->xss_clean($this->db->escape_str($this->input->post('keterangan')));
        $is_approve = $this->security->xss_clean($this->db->escape_str($this->input->post('is_approve')));

        if($is_approve==0){
            $app_request = '2';
        }elseif($is_approve==1){
            $app_request = '1';
        }
        // test($this->input->post('id_company'),1);
        if($is_approve==1){
            $querymail = $this->db->query("SELECT b.email FROM mst_user_group_permissions a JOIN mst_user b ON a.user_id=b.user_id
            JOIN mst_user_company c ON c.user_id=a.user_id AND c.company_id='".$this->input->post('id_company')."' WHERE a.id_user_group='5' AND b.is_active='1'")->result();

            foreach ($querymail as $key => $value) {
                if($value->email!=''){
                    $sql_email  = "INSERT INTO trn_approval_email (doc_id, doc_date,doc_no, doc_type, is_send, email_to)VALUES
                            ('".$id_pr."','".$date_pr."','".$no_pr."','1','0','".$value->email."')";
                    // test($sql_email,1);
                    $query_email = $this->dbpurch->query($sql_email);
                }
               
            }
        }

        $sql    = "INSERT INTO trn_request_approval (request_id,request_no,user_id,`date`,note,is_approve)VALUES('".$id_pr."','".$no_pr."','".$this->current_user['user_id']."','".dbnow()."','".$keterangan."','".$is_approve."')";
        // test($sql,0);
        $query = $this->dbpurch->query($sql);

        $sql1   = "UPDATE trn_request_01 SET status_approve = '".$app_request."' , pic_edit = '".$this->current_user['user_id']."' , edit_time = '".dbnow()."' WHERE request_id = '".$id_pr."'";
        // test($sql1,1);
        $query1 = $this->dbpurch->query($sql1);
        
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function history_request($id)
    {
        $sql="SELECT b.company_name,a.company_id,a.request_id,a.request_no,a.request_date,a.status_approve,
                CASE a.status_approve 
                WHEN '0' THEN 'NEW'
                WHEN '1' THEN 'Approved'
                WHEN '2' THEN 'Not Approved'
                END AS 'req_status',c.pr_id,c.pr_no,c.pr_date,c.project_id,c.pr_status,d.project_name,c.supplier_id,e.supplier_name,f.po_id,f.po_no,f.po_date,f.po_status,
                (SELECT COUNT(pr_approval_id) jml FROM trn_pr_approval WHERE pr_id=c.pr_id AND is_approve='0') pr_notapprove,
                (SELECT COUNT(pr_approval_id) jml FROM trn_pr_approval WHERE pr_id=c.pr_id AND is_approve='1') pr_approve
                FROM trn_request_01 a 
                LEFT JOIN db_master.mst_company b ON a.company_id=b.company_id
                LEFT JOIN trn_pr_01 c ON a.request_id=c.request_id
                LEFT JOIN db_master.mst_project d ON c.project_id=d.project_id
                LEFT JOIN db_master.mst_supplier e ON c.supplier_id=e.supplier_id
                LEFT JOIN trn_po_01 f ON f.pr_id=c.pr_id
                WHERE a.request_id='".$id."' ";
        
        $query = $this->dbpurch->query($sql);
        return $query->result();

    }

}   