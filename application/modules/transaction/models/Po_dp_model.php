<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Po_dp_model extends CI_Model
{
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_po_dp';
    private $pdId;
    private $supplierId;
    private $supplierName;
    private $pdNo;
    private $pdDate;
    private $poId;
    private $poNo;
    private $amount;
    private $remarks;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->pdId = 0;
        $this->supplierId = 0;
        $this->supplierName = '';
        $this->pdNo = '';
        $this->pdDate = "0000-00-00";
        $this->poId = 0;
        $this->poNo = '';
        $this->amount = 0;
        $this->remarks = '';
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
    }

    public function setPdId($aPdId)
    {
        $this->pdId = $this->db->escape_str($aPdId);
    }
    public function getPdId()
    {
        return $this->pdId;
    }
    public function setSupplierId($aSupplierId)
    {
        $this->supplierId = $this->db->escape_str($aSupplierId);
    }
    public function getSupplierId()
    {
        return $this->supplierId;
    }
    public function setSupplierName($aSupplierName)
    {
        $this->supplierName = $this->db->escape_str($aSupplierName);
    }
    public function getSupplierName()
    {
        return $this->supplierName;
    }
    public function setPdNo($aPdNo)
    {
        $this->pdNo = $this->db->escape_str($aPdNo);
    }
    public function getPdNo()
    {
        return $this->pdNo;
    }
    public function setPdDate($aPdDate)
    {
        $this->pdDate = $this->db->escape_str($aPdDate);
    }
    public function getPdDate()
    {
        return $this->pdDate;
    }
    public function setPoId($aPoId)
    {
        $this->poId = $this->db->escape_str($aPoId);
    }
    public function getPoId()
    {
        return $this->poId;
    }
    public function setPoNo($aPoNo)
    {
        $this->poNo = $this->db->escape_str($aPoNo);
    }
    public function getPoNo()
    {
        return $this->poNo;
    }
    public function setAmount($aAmount)
    {
        $this->amount = $this->db->escape_str($aAmount);
    }
    public function getAmount()
    {
        return $this->amount;
    }
    public function setRemarks($aRemarks)
    {
        $this->remarks = $this->db->escape_str($aRemarks);
    }
    public function getRemarks()
    {
        return $this->remarks;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

	function get_po_dp()
    {
        $sql ='SELECT pd_id,pd_no,supplier_name,pd_date,po_no,amount,remarks FROM trn_po_dp where is_active="1" ORDER BY pd_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_po_dp_free($id)
    {
        $sql ='SELECT pd_id,pd_no,supplier_name,pd_date,po_no,amount,remarks FROM trn_po_dp where is_active="1" AND po_id IS NULL and supplier_id="'.$id.'" ORDER BY pd_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    public function getDataCount()
    {
        $stQuery  = 'SELECT IFNULL(MAX(pd_id),0) pd_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
        $query = $this->dbpurch->query($stQuery);
        return $query->row();
    }

    function get_nomor_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(pd_no,10,4))+1,4,'0'),'0001') nomor_dok, pd_no FROM `trn_po_dp` WHERE SUBSTRING(pd_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    public function insert()
     {
        if($this->pdId =='' || $this->pdId == NULL )
        {
            $this->pdId = 0;
        }
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->pdNo =='' || $this->pdNo == NULL )
        {
            $this->pdNo = '';
        }
        if($this->pdDate =='' || $this->pdDate == NULL )
        {
            $this->pdDate = "0000-00-00";
        }
        if($this->poId =='' || $this->poId == NULL )
        {
            $this->poId = 0;
        }
        if($this->poNo =='' || $this->poNo == NULL )
        {
            $this->poNo = '';
        }
        if($this->amount =='' || $this->amount == NULL )
        {
            $this->amount = 0;
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'pd_id,'; 
        $stQuery .=   'supplier_id,'; 
        $stQuery .=   'supplier_name,'; 
        $stQuery .=   'pd_no,'; 
        $stQuery .=   'pd_date,'; 
        // $stQuery .=   'po_id,'; 
        // $stQuery .=   'po_no,'; 
        $stQuery .=   'amount,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->pdId).','; 
        $stQuery .=   $this->db->escape_str($this->supplierId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->supplierName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->pdNo).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->pdDate).'",'; 
        // $stQuery .=   $this->db->escape_str($this->poId).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->poNo).'",'; 
        $stQuery .=   $this->db->escape_str($this->amount).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        // test($stQuery,1);
        $this->db->query($stQuery); 
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('pd_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    public function update($id)
     {
        if($this->pdId =='' || $this->pdId == NULL )
        {
            $this->pdId = 0;
        }
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->pdNo =='' || $this->pdNo == NULL )
        {
            $this->pdNo = '';
        }
        if($this->pdDate =='' || $this->pdDate == NULL )
        {
            $this->pdDate = "0000-00-00";
        }
        if($this->poId =='' || $this->poId == NULL )
        {
            $this->poId = 0;
        }
        if($this->poNo =='' || $this->poNo == NULL )
        {
            $this->poNo = '';
        }
        if($this->amount =='' || $this->amount == NULL )
        {
            $this->amount = 0;
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        // $stQuery .=   'pd_id ='.$this->db->escape_str($this->pdId).','; 
        $stQuery .=   'supplier_id ='.$this->db->escape_str($this->supplierId).','; 
        $stQuery .=   'supplier_name ="'.$this->db->escape_str($this->supplierName).'",'; 
        // $stQuery .=   'pd_no ="'.$this->db->escape_str($this->pdNo).'",'; 
        $stQuery .=   'pd_date ="'.$this->db->escape_str($this->pdDate).'",'; 
        // $stQuery .=   'po_id ='.$this->db->escape_str($this->poId).','; 
        // $stQuery .=   'po_no ="'.$this->db->escape_str($this->poNo).'",'; 
        $stQuery .=   'amount ='.$this->db->escape_str($this->amount).','; 
        $stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
        // $stQuery .=   'pic_input ='.$this->db->escape_str($this->picInput).','; 
        // $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'pd_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
     }

    function act_delete_js(){
        $sql = "UPDATE trn_po_dp SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE pd_id = '".$this->input->post('pd_id')."'";
        //test($sql,1);
        $query = $this->dbpurch->query($sql);
        return $query;
    }

    function updatePoDp($noPo,$id_po,$id){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_po_dp` SET `po_no` = '".$noPo."',`po_id` = '".$id_po."'   
                WHERE `pd_id` = '".$id."'");
    }

    function getDetailPoDp($id){
        return $this->dbpurch->query("SELECT * FROM `db_bumbu_transaction`.`trn_po_dp` WHERE pd_id='".$id."'")->row();
    }

    function getTotalAmount($id){
        return $this->dbpurch->query("SELECT ifNULL(SUM(amount),0) tamount FROM `db_bumbu_transaction`.`trn_po_dp` WHERE po_id='".$id."'")->row();
    }

    function act_update_dp($id){
        $this->dbpurch->query("UPDATE trn_po_dp SET po_id = NULL, po_no = NULL WHERE po_id='".$id."'");
    }



}	