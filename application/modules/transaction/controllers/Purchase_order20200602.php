<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Transaction', 'active_submenu' => 'transaction/purchase_order')); 
        $this->isMenu();
        $this->load->model('transaction/purchase_order_model');
        $this->load->model('transaction/purchase_order_model02');
        $this->load->model('transaction/purchase_requisition_model');
        $this->load->model('master/items_model');
    }


    function index(){ 
        $data['data_po']  = $this->purchase_order_model->get_po();
        $this->template->load('body', 'transaction/purchase_order/po_view',$data);
    }

    function view_pr(){
        $header         = $this->purchase_requisition_model->getByPrProcess();

        $myData = array();
        foreach ($header as $key => $row) {
            $myData[] = array(
                $row['pr_no'],       
                '<input type="checkbox" value="'.$row['pr_id'].'">'
                // '<label><input type="checkbox" id="coba_id" name="options[]" value="'.$row['pr_id'].'"></label>'
            );            
        }  

        return jsout(array('detail'=> json_encode($myData)));
    }

    function pr_selected(){
        $this->session->unset_userdata('new_po');
        $new_po = $this->session->userdata('new_po');

        foreach($this->input->post('values') as $key=>$val){
            // test($val,1);
            $new_po['items'][$key] = array(
                'det_id'        => $val
            );
        }
        $this->session->set_userdata('new_po', $new_po);
    }

    function po_input(){
        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');
        $this->load->model('master/project_model');

        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        $data['data_project']   = $this->project_model->get_project();
        $data['data_pr']        = $this->purchase_order_model->get_pr_detail();
        $data['header_pr']      = $this->purchase_order_model->get_pr_header();
        $this->template->load('body', 'transaction/purchase_order/po_form',$data);

    }

    function form_act(){
        $this->load->model('master/supplier_model');
        $this->load->model('master/project_model');

        $periode            = substr($this->input->post('po_date'),0,4).substr($this->input->post('po_date'),5,2);
        $no_po              = 'POS'.$periode.$this->purchase_order_model->get_no_po_dok($periode)->nomor_dok;
        $id_po              = $this->purchase_order_model->get_id_po()->po_id;
        $company_id         = $this->input->post('company_id');
        $company_name       = $this->input->post('company_name');
        $po_date            = $this->input->post('po_date');
        $project_id         = $this->input->post('project_id');
        $supplier_id        = $this->input->post('supplier_id');

        $get_project        = $this->project_model->getById($project_id);
        $get_supplier       = $this->supplier_model->getById($supplier_id);

        $total              = 0;
        $subtotal           = 0;
        $tpotongan          = 0;

        foreach ($this->session->userdata('new_po')['items'] as $key => $value) {
            $pr_id          = $value['det_id'];
            $this->purchase_requisition_model->updateNoPo($no_po,$pr_id);
        }        

        foreach ($this->input->post('items_id') as $key => $value) {
            $potongan       = str_replace(',','',$this->input->post('potongan'))[$key];
            $tpotongan      = $tpotongan+$potongan;
            $subtotal       = ($this->input->post('qty')[$key] * str_replace(',','',$this->input->post('price'))[$key])-$potongan;
            $total          = $total + $subtotal;

            // $this->purchase_order_model02->setPo02Id($this->security->xss_clean($_POST['po02Id']));
            $this->purchase_order_model02->setPoId($this->security->xss_clean($id_po));
            $this->purchase_order_model02->setItemsId($this->security->xss_clean($value));
            $this->purchase_order_model02->setItemsName($this->security->xss_clean($this->input->post('items_name')[$key]));
            $this->purchase_order_model02->setQty($this->security->xss_clean($this->input->post('qty')[$key]));
            $this->purchase_order_model02->setPrice($this->security->xss_clean(str_replace(',','',$this->input->post('price'))[$key]));
            // $this->purchase_order_model02->setDiscPercent($this->security->xss_clean($_POST['discPercent']));
            $this->purchase_order_model02->setDiscVal($this->security->xss_clean($potongan));
            // $this->purchase_order_model02->setExchangeRate($this->security->xss_clean($_POST['exchangeRate']));
            $this->purchase_order_model02->setSubtotal($this->security->xss_clean($subtotal));
            // $this->purchase_order_model02->setRemarks($this->security->xss_clean($_POST['remarks']));
            $this->purchase_order_model02->setIsActive($this->security->xss_clean(1));
            $this->purchase_order_model02->setPicInput($this->security->xss_clean($this->current_user['user_id']));
            $this->purchase_order_model02->setInputTime($this->security->xss_clean(dbnow()));
            // $this->purchase_order_model02->setPicEdit($this->security->xss_clean($_POST['picEdit']));
            // $this->purchase_order_model02->setEditTime($this->security->xss_clean($_POST['editTime']));
            $this->purchase_order_model02->insert();

            $this->items_model->update_price($value,str_replace(',','',$this->input->post('price'))[$key]);

        }

        $this->purchase_order_model->setPoId($this->security->xss_clean($id_po));
        $this->purchase_order_model->setPoNo($this->security->xss_clean($no_po));
        // $this->purchase_order_model->setPrId($this->security->xss_clean($_POST['prId']));
        // $this->purchase_order_model->setPrNo($this->security->xss_clean($_POST['prNo']));
        $this->purchase_order_model->setPoDate($this->security->xss_clean($_POST['po_date']));
        // $this->purchase_order_model->setRequester($this->security->xss_clean($_POST['requester']));
        $this->purchase_order_model->setProjectId($this->security->xss_clean($_POST['project_id']));
        $this->purchase_order_model->setProjectName($this->security->xss_clean($get_project->project_name));
        $this->purchase_order_model->setCompanyId($this->security->xss_clean($_POST['company_id']));
        $this->purchase_order_model->setCompanyName($this->security->xss_clean($_POST['company_name']));
        $this->purchase_order_model->setSupplierId($this->security->xss_clean($_POST['supplier_id']));
        $this->purchase_order_model->setSupplierName($this->security->xss_clean($get_supplier->supplier_name));
        $this->purchase_order_model->setDeliveryAddress($this->security->xss_clean($_POST['delivery_address']));
        $this->purchase_order_model->setPoInfo($this->security->xss_clean($_POST['remarks']));
        // $this->purchase_order_model->setCurrencyId($this->security->xss_clean($_POST['currencyId']));
        $this->purchase_order_model->setPoStatus($this->security->xss_clean('Process'));
        $this->purchase_order_model->setPaymentStatus($this->security->xss_clean('Process'));
        // $this->purchase_order_model->setTransportFee($this->security->xss_clean($_POST['transportFee']));
        // $this->purchase_order_model->setPpnPercent($this->security->xss_clean($_POST['ppnPercent']));
        // $this->purchase_order_model->setPpnValue($this->security->xss_clean($_POST['ppnValue']));
        // $this->purchase_order_model->setPph($this->security->xss_clean($_POST['pph']));
        // $this->purchase_order_model->setPphValue($this->security->xss_clean($_POST['pphValue']));
        // $this->purchase_order_model->setPbbkb($this->security->xss_clean($_POST['pbbkb']));
        // $this->purchase_order_model->setPbbkbValue($this->security->xss_clean($_POST['pbbkbValue']));
        // $this->purchase_order_model->setDiscPercent($this->security->xss_clean($_POST['discPercent']));
        $this->purchase_order_model->setDiscVal($this->security->xss_clean($tpotongan));
        $this->purchase_order_model->setTotal($this->security->xss_clean($total));
        // $this->purchase_order_model->setReamaining($this->security->xss_clean($_POST['reamaining']));
        // $this->purchase_order_model->setTop($this->security->xss_clean($_POST['top']));
        // $this->purchase_order_model->setNoPrint($this->security->xss_clean($_POST['noPrint']));
        $this->purchase_order_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->purchase_order_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->purchase_order_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->purchase_order_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        $save   = $this->purchase_order_model->insert();

        // $save   = $this->purchase_requisition_model->act_form();

        // $this->session->unset_userdata('new_pr');
        // $this->session->set_flashdata('alert','Data is updated');
        $this->session->set_flashdata('alert','Data Berhasil Disimpan dengan Nomor '.$no_po);
        redirect('transaction/purchase_order');
        // jsout(array('success' => true, 'status' => $save ));
    }

    function view_popup($id){

        $header         = $this->purchase_order_model->getById($id);
        $detail         = $this->purchase_order_model02->getDetailPo($id)->result_array();
        $myData         = array();
        $subtotal       = 0;
        $total          = 0;
        $tpotongan      = 0;
        $ttotal_barang  = 0;
        foreach ($detail as $key => $row) {
            $tpotongan      = $tpotongan+$row['disc_val'];
            $subtotal       = ($row['price']*$row['qty']);
            $total_barang   = $subtotal-$row['disc_val'];
            $ttotal_barang  = $total_barang+$ttotal_barang;
            $total          = $subtotal+$total;
            $myData[] = array(
                $row['items_name'],     
                number_format($row['qty'],2),
                number_format($row['price'],2),
                number_format($subtotal,2),
                number_format($row['disc_val'],2),
                number_format($total_barang,2),
            );     
        }   
            $myData[] = array(
                '',
                '',
                '<strong>Total</strong>',
                '<strong>'.number_format($total,2).'</strong>',
                '<strong>'.number_format($tpotongan,2).'</strong>',
                '<strong>'.number_format($ttotal_barang,2).'</strong>'
            );      

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function delete_js(){
        $po_id      = $this->input->post('po_id');
        $po_no      = $this->input->post('po_no');

        $update = $this->purchase_requisition_model->act_update_po($po_no);
        $delete = $this->purchase_order_model->act_delete_js($po_id);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function force_js(){
        $po_id      = $this->input->post('po_id');
        $po_no      = $this->input->post('po_no');

        $delete = $this->purchase_order_model->act_force_js($po_id);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function reject_js(){
        $po_id      = $this->input->post('po_id');
        $po_no      = $this->input->post('po_no');

        $delete = $this->purchase_order_model->act_reject_js($po_id);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function view_print($id){
        $data['header']         = $this->purchase_order_model->print_header($id);
        $data['detail']         = $this->purchase_order_model02->print_detail($id);

        $this->template->load('body', 'transaction/purchase_order/po_print',$data);
    }

}

//1. Penambahan Discount untuk Header PO dan Detail PO
//2. Penambahan Print
?>