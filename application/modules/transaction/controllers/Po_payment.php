<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_payment extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Transaction', 'active_submenu' => 'transaction/po_payment')); 
        $this->isMenu();
        $this->load->model('transaction/po_payment_model');
        $this->load->model('transaction/po_payment_model02');
        $this->load->model('transaction/purchase_order_model');
        $this->load->model('master/supplier_model');
    }


    function index(){ 
        $this->session->unset_userdata('new_pa');
        $data['data_supplier']  = $this->supplier_model->get_supplier_payment();
        $data['data_pa']        = $this->po_payment_model->get_pa();
        $this->template->load('body', 'transaction/payment/pa_view',$data);
    }

    function view_po($supplier_id){
        $header         = $this->purchase_order_model->getPoPayment($supplier_id);

        $myData = array();
        $sisa   = 0;
        foreach ($header as $key => $row) {
            $sisa       = $row['total']-$row['grand_total'];
            $myData[] = array(
                $row['po_no'],       
                $row['supplier_name'],       
                money($row['total']),       
                money($sisa),       
                '<input type="checkbox" value="'.$row['po_id'].'">'
                // '<label><input type="checkbox" id="coba_id" name="options[]" value="'.$row['pr_id'].'"></label>'
            );            
        }  

        return jsout(array('detail'=> json_encode($myData)));
    }

    function pa_selected(){
        $this->session->unset_userdata('new_pa');
        $new_pa = $this->session->userdata('new_pa');

        foreach($this->input->post('values') as $key=>$val){
            // test($val,1);
            $new_pa['items'][$key] = array(
                'det_id'        => $val
            );
        }
        $this->session->set_userdata('new_pa', $new_pa);
    }

    function pa_input(){
        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');

        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_po']          = $this->po_payment_model->get_po_detail();
        // $data['data_supplier']  = $this->supplier_model->get_supplier();
        // $data['data_items']     = $this->items_model->get_items();
        $this->template->load('body', 'transaction/payment/pa_form',$data);

    }

    function form_act(){
        $this->db->trans_begin();
        // test(substr($this->input->post('pp_date'),5,2),1);

        $periode            = substr($this->input->post('pp_date'),0,4).substr($this->input->post('pp_date'),5,2);
        $no_pay             = 'PMS'.$periode.$this->po_payment_model->get_no_pay_dok($periode)->nomor_dok;
        $id_pay             = $this->po_payment_model->get_id_pay()->pp_id;
        $company_id         = $this->input->post('company_id');
        $company_name       = $this->input->post('company_name');
        $pp_date            = $this->input->post('pp_date');
        $supplier_id        = $this->input->post('supplier_id');
        $supplier_name      = $this->input->post('supplier_name');
        $payment_type       = $this->input->post('payment_type');

        $total              = 0;
        $subtotal           = 0;
        $tloop              = $this->input->post('tloop'); 
        foreach ($this->input->post('po_id') as $key => $value) {
            $po_id              = $value;
            $po_no              = $this->input->post('po_no')[$key];
            $amount             = str_replace(',','',$this->input->post('total'))[$key];
            $jml_hutang         = $this->input->post('jml_hutang')[$key];
            $total              = $total + $amount;

            $this->po_payment_model02->setPpId($this->security->xss_clean($id_pay));
            $this->po_payment_model02->setPoId($this->security->xss_clean($po_id));
            $this->po_payment_model02->setPoNo($this->security->xss_clean($po_no));
            $this->po_payment_model02->setAmount($this->security->xss_clean($amount));
            $this->po_payment_model02->setDiscPercent($this->security->xss_clean(0));
            $this->po_payment_model02->setDiscValue($this->security->xss_clean(0));
            $this->po_payment_model02->setTotal($this->security->xss_clean($amount));
            $this->po_payment_model02->setIsActive(1);
            $this->po_payment_model02->insert();

            if($jml_hutang == $amount){
                $status         = "Closed";
            }else{
                $status         = "Outstanding";
            }

            $this->purchase_order_model->update_payment($value,$status);

        }

        $this->po_payment_model->setPpId($this->security->xss_clean($id_pay));
        $this->po_payment_model->setPpNo($this->security->xss_clean($no_pay));
        $this->po_payment_model->setPpDate($this->security->xss_clean($pp_date));
        $this->po_payment_model->setCompanyId($this->security->xss_clean($company_id));
        $this->po_payment_model->setCompanyName($this->security->xss_clean($company_name));
        $this->po_payment_model->setSupplierId($this->security->xss_clean($supplier_id));
        $this->po_payment_model->setSupplierName($this->security->xss_clean($supplier_name));
        $this->po_payment_model->setPaymentType($this->security->xss_clean($payment_type));
        $this->po_payment_model->setGrandTotal($this->security->xss_clean($total));
        $this->po_payment_model->setIsActive($this->security->xss_clean(1));
        $this->po_payment_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->po_payment_model->setInputTime($this->security->xss_clean(dbnow()));
        $this->po_payment_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->po_payment_model->setEditTime($this->security->xss_clean(dbnow()));
        $this->po_payment_model->insert();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('alert','Data Gagal Disimpan ');
            redirect('transaction/po_payment');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('alert','Data Berhasil Disimpan Dengan Nomor '.$no_pay);
            redirect('transaction/po_payment');
        }
        
        // jsout(array('success' => true, 'status' => $save ));
    }

    function view_popup($id){

        $header         = $this->po_payment_model->detail_header($id);
        $detail         = $this->po_payment_model02->detail_pembayaran($id)->result_array();
        $myData         = array();
        $total          = 0;
        foreach ($detail as $key => $row) {
            $total      = $row['total']+$total;
            $myData[] = array(
                $row['po_no'],     
                number_format($row['total'],1)
            );     
        }  
            $myData[] = array(
                'Total',
                number_format($total,2)
            );       

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function reject_js(){
        $pp_id      = $this->input->post('pp_id');
        $pp_no      = $this->input->post('pp_no');

        $u_aktif_payment    = $this->po_payment_model->update_status($pp_id);
        $detail             = $this->po_payment_model02->detail_pembayaran($pp_id)->result();

        foreach ($detail as $key => $value) {
            $this->purchase_order_model->update_payment($value->po_id,'Process');
            $this->po_payment_model02->update_active($value->pp_02_id);
        }

        jsout(array('success' => true, 'status' => $u_aktif_payment ));
    }

    function view_print($id){
        $data['header']         = $this->po_payment_model->print_header($id);
        $data['detail']         = $this->po_payment_model02->print_detail($id);
        // $data['detail']         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $this->template->load('body_print', 'transaction/payment/pa_print', $data);
    }

    function pemutihan(){
        $data['data_pemutihan']         = $this->po_payment_model02->pemutihan();
        $this->template->load('body', 'transaction/payment/pa_pemutihan', $data);
    }

    function pemutihan_act(){
        $this->db->trans_begin();

        $data               = $this->po_payment_model02->pemutihan();

        $periode            = date('Y').date('m');
        $no_pay             = 'PMS'.$periode.$this->po_payment_model->get_no_pay_dok($periode)->nomor_dok;
        $id_pay             = $this->po_payment_model->get_id_pay()->pp_id;
        $company_id         = $this->current_user['company_id'];
        $company_name       = $this->current_user['company_name'];
        $pp_date            = date('Y').'-'.date('m').'-'.date('d');
        $supplier_id        = '';
        $supplier_name      = '';
        $payment_type       = 'Pemutihan';

        $total              = 0;
        foreach ($data as $key => $value) {

            $this->po_payment_model02->setPpId($this->security->xss_clean($id_pay));
            $this->po_payment_model02->setPoId($this->security->xss_clean($value->po_id));
            $this->po_payment_model02->setPoNo($this->security->xss_clean($value->po_no));
            $this->po_payment_model02->setAmount($this->security->xss_clean($value->sisa));
            $this->po_payment_model02->setDiscPercent($this->security->xss_clean(0));
            $this->po_payment_model02->setDiscValue($this->security->xss_clean(0));
            $this->po_payment_model02->setTotal($this->security->xss_clean($value->sisa));
            $this->po_payment_model02->setIsActive(1);
            $this->po_payment_model02->insert();

            $total              = $total + $value->sisa;

            $this->purchase_order_model->update_payment($value->po_id,'Closed');

        }

        $this->po_payment_model->setPpId($this->security->xss_clean($id_pay));
        $this->po_payment_model->setPpNo($this->security->xss_clean($no_pay));
        $this->po_payment_model->setPpDate($this->security->xss_clean($pp_date));
        $this->po_payment_model->setCompanyId($this->security->xss_clean($company_id));
        $this->po_payment_model->setCompanyName($this->security->xss_clean($company_name));
        $this->po_payment_model->setSupplierId($this->security->xss_clean($supplier_id));
        $this->po_payment_model->setSupplierName($this->security->xss_clean($supplier_name));
        $this->po_payment_model->setPaymentType($this->security->xss_clean($payment_type));
        $this->po_payment_model->setGrandTotal($this->security->xss_clean($total));
        $this->po_payment_model->setIsActive($this->security->xss_clean(1));
        $this->po_payment_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->po_payment_model->setInputTime($this->security->xss_clean(dbnow()));
        $this->po_payment_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->po_payment_model->setEditTime($this->security->xss_clean(dbnow()));
        $this->po_payment_model->insert();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('alert','Data Gagal Disimpan ');
            redirect('transaction/po_payment');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('alert','Data Pemutihan Berhasil Disimpan Dengan Nomor '.$no_pay);
            redirect('transaction/po_payment');
        }
    }

}
//1. Penambahan Function Untuk Popup.
//2. Penambahan Reject Untuk Pembayaran PO
// 3. Penambahan Print Payment
?>