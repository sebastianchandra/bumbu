<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('transaction/request_model');
    }


    function index(){  
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Transaction', 'active_submenu' => 'transaction/request'));
        $this->isMenu();
        $data['data_pr']  = $this->request_model->get_pr_approve();
        $this->template->load('body', 'transaction/request/request_view',$data);
    }

    function form(){
        $this->session->unset_userdata('new_req');

        $this->load->model('master/items_model');
        $this->load->model('master/users_group_model');
        $this->load->model('master/project_model');

        $new_req = $this->session->userdata('new_req');

        if(!$new_req){
            $new_req = array(
                'items' => array()
            );
        }
        
        $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_items']     = $this->items_model->get_items();
        $data['user_group']     = $this->users_group_model->get_user_group();
        $data['data_project']   = $this->project_model->project_request($this->current_user['company_id']);
        $data['new_req']        = $new_req;
        $this->template->load('body', 'transaction/request/request_form', $data);
    }

    function form_lm(){
        $this->session->unset_userdata('new_req');

        $this->load->model('master/items_model');
        $this->load->model('master/users_group_model');
        $this->load->model('master/project_model');

        $new_req = $this->session->userdata('new_req');

        if(!$new_req){
            $new_req = array(
                'items' => array(),
                'budget' => array()
            );
        }
        
        $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_items']     = $this->items_model->get_items();
        $data['user_group']     = $this->users_group_model->get_user_group();
        $data['data_project']   = $this->project_model->project_budget();
        $data['new_req']        = $new_req;
        $this->template->load('body', 'transaction/request/request_form_lm', $data);
    }

    function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_req    = $this->session->userdata('new_req');
        $items      = $new_req['items'];
        $budget     = $new_req['budget']; 

        $new_req['items'][] = array(
            'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'item_info'     => $this->input->post('item_info')
        );

        $exist = false;
        // test($budget,1);
        if(isset($budget)){
            foreach($budget as $key=>$val){
                if($val['mbp_id'] == $this->input->post('mbp_id')){
                    $new_req['budget'][$key] = array(
                        'mbp_id'        => $this->input->post('mbp_id'),
                        'det_id'        => $this->input->post('det_id'),
                        'item_id'       => $this->input->post('item_id'),
                        'item_name'     => $this->input->post('item_name'),
                        'item_qty'      => $this->input->post('item_qty'),
                        'item_info'     => $this->input->post('item_info'),
                        'total_qty'     => $this->input->post('item_qty')+$val['total_qty'],
                        'qty_remaining' => $this->input->post('qty_remaining'),
                        'mbp_name'      => $this->input->post('mbp_name')
                    );
                    $exist = true;
                    break;
                }
            }
        }

        if(!$exist){
            $new_req['budget'][] = array(
                'mbp_id'        => $this->input->post('mbp_id'),
                'det_id'        => $this->input->post('det_id'),
                'item_id'       => $this->input->post('item_id'),
                'item_name'     => $this->input->post('item_name'),
                'item_qty'      => $this->input->post('item_qty'),
                'item_info'     => $this->input->post('item_info'),
                'total_qty'     => $this->input->post('item_qty'),
                'qty_remaining' => $this->input->post('qty_remaining'),
                'mbp_name'      => $this->input->post('mbp_name')
            );
        }
        
        $this->session->set_userdata('new_req', $new_req);        
    }

    function form_act(){
        $save   = $this->request_model->act_form();

        // $this->session->unset_userdata('new_req');
        jsout(array('success' => true, 'status' => $save ));
    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_req = $this->session->userdata('new_req');

        $items = $new_req['items'];
        // test($items,1);
        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['det_id'] == $index_id){
                $mbp_id     = $new_req['items'][$key]['mbp_id'];
                $item_qty   = str_replace(',','',$this->security->xss_clean($this->db->escape_str($new_req['items'][$key]['item_qty'])));
                // test($mbp_id.' '.$item_qty,1);
                unset($new_req['items'][$key]);
                $new_req['items'] = array_values($new_req['items']);
                break;
            }
        }

        if($mbp_id!=''){

            $budget = $new_req['budget']; 
            // test($budget,1);
            foreach($budget as $key=>$val){

                if($val['mbp_id'] == $mbp_id){

                    $new_req['budget'][$key] = array(
                        'mbp_id'        => $new_req['budget'][$key]['mbp_id'],
                        'det_id'        => $new_req['budget'][$key]['det_id'],
                        'item_id'       => $new_req['budget'][$key]['item_id'],
                        'item_name'     => $new_req['budget'][$key]['item_name'],
                        'item_qty'      => $new_req['budget'][$key]['item_qty'],
                        'item_info'     => $new_req['budget'][$key]['item_info'],
                        'total_qty'     => $new_req['budget'][$key]['total_qty']-$item_qty,
                        'qty_remaining' => $new_req['budget'][$key]['qty_remaining'],
                        'mbp_name'      => $new_req['budget'][$key]['mbp_name']
                    );

                    break;
                }

            }
        }

        $this->session->set_userdata('new_req', $new_req);
        jsout(array('success'=>1)); 
    }

    function remove_item_edit(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_req = $this->session->userdata('new_req');

        $items = $new_req['items'];

        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['det_id'] == $index_id){
                unset($new_req['items'][$key]);
                $new_req['items'] = array_values($new_req['items']);
                $this->request_model->act_remove_detail($index_id);
                break;
            }
        }

        $this->session->set_userdata('new_req', $new_req);
        jsout(array('success'=>1)); 
    }

    // function edit_item(){
    //     //test($_GET['index_id'],0);
    //     if(!isset($_GET['index_id'])) return;
    //     $index_id = $this->input->get('index_id');
    //     $new_req = $this->session->userdata('new_req');

    //     $items = $new_req['items'];

    //     foreach($items as $key=>$val){
    //         //test($val['item_id'],0);
    //         if($val['item_id'] == $index_id){
    //             unset($new_req['items'][$key]);
    //             $new_req['items'] = array_values($new_req['items']);
    //             break;
    //         }
    //     }

    //     $this->session->set_userdata('new_req', $new_req);
    //     jsout(array('success'=>1, 'data' => $new_req['items'][$key])); 
    // }

    function delete($id){
        $delete = $this->supplier_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('master/items',$data);
        }
    }

    function delete_js(){
        $delete = $this->supplier_model->act_delete_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $this->session->unset_userdata('new_req');
        
        $this->load->model('master/items_model');
        $this->load->model('master/users_group_model');
        $this->load->model('master/project_model');

        $new_req = $this->session->userdata('new_req');

        $header     = $this->request_model->detail_pr($id);
        $detail     = $this->request_model->detail_items_pr($id);
        $tdetail    = $this->request_model->total_items_pr($id);
        
        $new_req['items'] = array(); 
        if($tdetail!=0){
            foreach($detail as $key=>$val){
                $new_req['items'][$key] = array(
                    'det_id'        => $val->request_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'item_qty'      => $val->qty,
                    'item_info'     => $val->remarks
                );
            }
        }
        

        $this->session->set_userdata('new_req', $new_req);
        $data['new_req'] = $new_req;
        $data['header'] = $header;
        $data['data_items']     = $this->items_model->get_items();
        $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['user_group']     = $this->users_group_model->get_user_group();
        $data['data_project']   = $this->project_model->project_request($this->current_user['company_id']);
        $this->template->load('body', 'transaction/request/request_edit', $data);
    }

    function edit_lm($id){
        $this->session->unset_userdata('new_req');
        
        $this->load->model('master/items_model');
        $this->load->model('master/users_group_model');
        $this->load->model('master/project_model');

        $new_req = $this->session->userdata('new_req');

        $header     = $this->request_model->detail_pr($id);
        $detail     = $this->request_model->detail_items_pr($id);
        $tdetail    = $this->request_model->total_items_pr($id);
        
        $new_req['items'] = array(); 
        if($tdetail!=0){
            foreach($detail as $key=>$val){
                $new_req['items'][$key] = array(
                    'det_id'        => $val->request_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'item_qty'      => $val->qty,
                    'item_info'     => $val->remarks
                );
            }
        }
        

        $this->session->set_userdata('new_req', $new_req);
        $data['new_req'] = $new_req;
        $data['header'] = $header;
        $data['data_items']     = $this->items_model->get_items();
        $data['data_project']   = $this->project_model->project_budget();
        $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['user_group']     = $this->users_group_model->get_user_group();
        $this->template->load('body', 'transaction/request/request_edit_lm', $data);
    }

    function edit_act(){
        $update   = $this->request_model->act_edit();

        $this->session->unset_userdata('new_req');
        jsout(array('success' => true, 'status' => $update ));
    }

    function reset(){
        $this->session->unset_userdata('new_req');
        redirect('transaction/request');
    }

    function view_popup($id){
        $data['id']     = $id;
        $data['header'] = $this->request_model->detail_pr($id);
        $data['detail'] = $this->request_model->detail_items_pr($id);
        $data['history']= $this->request_model->history_request($id);
        $this->load->view('transaction/request/request_detail_popup',$data);
    }

    // function form_items($id){
    //     $this->load->model('master/master_model');
    //     $this->load->model('master/supplier_model');

    //     $data['data_items_kind']     = $this->master_model->get_items_kind();
    //     $data['data_items_unit']     = $this->master_model->get_items_unit();
    //     $data['data_supplier']       = $this->supplier_model->detail_supplier($id);
    //     $this->load->view('transaction/purchase_requisition/items_form',$data);
    // }

    function cetak($id){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Transaction', 'active_submenu' => 'transaction/request'));
        $data['header'] = $this->request_model->detail_pr($id);
        $data['detail'] = $this->request_model->detail_items_pr($id);
        $this->template->load('body', 'transaction/request/request_print',$data);
        // $this->load->view('transaction/request/request_print',$data);
    }

    function view_approve(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Approve', 'active_submenu' => 'transaction/request/view_approve'));  
        $this->isMenu();
        $data['data_pr']  = $this->request_model->get_request_approve();
        $this->template->load('body', 'transaction/request/request_approve',$data);
    }

    function process_request(){
        $save   = $this->request_model->act_request();
        jsout(array('success' => true, 'no_po' => $save ));
    }

}
?>