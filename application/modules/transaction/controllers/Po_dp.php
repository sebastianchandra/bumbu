<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_dp extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Transaction', 'active_submenu' => 'transaction/po_dp'));  
        $this->isMenu();
        $this->load->model('transaction/po_dp_model');
    }


	function index(){
        $data['data_po_dp']     = $this->po_dp_model->get_po_dp();
        $this->template->load('body', 'transaction/po_dp/dp_view',$data);
	}

    function form(){
        $this->load->model('master/supplier_model');

        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $this->template->load('body', 'transaction/po_dp/dp_form',$data);
    }

    function form_act(){
        $id         = $this->po_dp_model->getDataCount()->pd_id+1;

        $periode            = date('Y').date('m');
        $nomor_dok          = $this->po_dp_model->get_nomor_dok($periode)->nomor_dok;
        $po_dp_code         = 'PDS'.$periode.$nomor_dok;

        $this->po_dp_model->setPdId($this->security->xss_clean($id));
        $this->po_dp_model->setSupplierId($this->security->xss_clean($_POST['supplier_id']));
        $this->po_dp_model->setSupplierName($this->security->xss_clean($_POST['supplier_name']));
        $this->po_dp_model->setPdNo($this->security->xss_clean($po_dp_code));
        $this->po_dp_model->setPdDate($this->security->xss_clean($_POST['pd_date']));
        // $this->po_dp_model->setPoId($this->security->xss_clean($_POST['poId']));
        // $this->po_dp_model->setPoNo($this->security->xss_clean($_POST['poNo']));
        $this->po_dp_model->setAmount($this->security->xss_clean($_POST['amount']));
        $this->po_dp_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->po_dp_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->po_dp_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->po_dp_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->po_dp_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        $save   = $this->po_dp_model->insert();

        jsout(array('success' => true, 'status' => $save ));
    }

    function delete($id){
        $delete = $this->po_dp_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('transaction/po_dp',$data);
        }
    }

    function delete_js(){
        $delete = $this->po_dp_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $this->load->model('master/supplier_model');

        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['po_dp']          = $this->po_dp_model->getById($id);
        $this->template->load('body', 'transaction/po_dp/dp_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['pd_id']);
        // $this->po_dp_model->getObjectById($id);
        $this->po_dp_model->setSupplierId($this->security->xss_clean($_POST['supplier_id']));
        $this->po_dp_model->setSupplierName($this->security->xss_clean($_POST['supplier_name']));
        $this->po_dp_model->setPdDate($this->security->xss_clean($_POST['pd_date']));
        $this->po_dp_model->setAmount($this->security->xss_clean($_POST['amount']));
        $this->po_dp_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->po_dp_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->po_dp_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->po_dp_model->update($id);
        jsout(array('success' => true, 'status' => $update ));
    }

    function detail_dp_supplier(){
        $sup_id     = $this->input->post('id');
        $result     = $this->po_dp_model->get_po_dp_free($sup_id);
        echo json_encode($result);
    }

    // function form_act_in_pr(){
    //     $save   = $this->po_dp_model->act_form_in_pr();
    //     jsout(array('success' => true, 'status' => $save ));
    // }

}
?>