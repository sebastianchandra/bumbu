<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_requisition extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('transaction/purchase_requisition_model');
        $this->load->model('transaction/purchase_requisition_model02');        
    }

    function index(){  
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Transaction', 'active_submenu' => 'transaction/purchase_requisition'));
        $this->isMenu();

        $data['data_pr']        = $this->purchase_requisition_model->get_pr();
        $this->template->load('body', 'transaction/purchase_requisition/pr_view',$data);
    }

    function form(){
        $this->session->unset_userdata('new_pr');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');


        $new_pr = $this->session->userdata('new_pr');

        if(!$new_pr){
            $new_pr = array(
                'items' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        $data['new_pr']         = $new_pr;
        // test($data,1);
        $this->template->load('body', 'transaction/purchase_requisition/pr_form', $data);
    }

    function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_pr    = $this->session->userdata('new_pr');
        $items      = $new_pr['items'];

        $new_pr['items'][] = array(
            // 'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'item_info'     => $this->input->post('item_info')
        );

        $exist = false;
        // test($new_pr,0);
        $this->session->set_userdata('new_pr', $new_pr);         
    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_pr = $this->session->userdata('new_pr');

        $items = $new_pr['items'];
        // test($items,1);
        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                $mbp_id     = $new_pr['items'][$key]['mbp_id'];
                $item_qty   = str_replace(',','',$this->security->xss_clean($this->db->escape_str($new_pr['items'][$key]['item_qty'])));
                // test($mbp_id.' '.$item_qty,1);
                unset($new_pr['items'][$key]);
                $new_pr['items'] = array_values($new_pr['items']);
                break;
            }
        }
        // test($new_pr,0);
        $this->session->set_userdata('new_pr', $new_pr);
        jsout(array('success'=>1)); 
    }

    function form_act(){
        $this->db->trans_begin();

        $id             = $this->purchase_requisition_model->getDataCount()->pr_id;

        $periode        = substr($this->input->post('pr_date'),0,4).substr($this->input->post('pr_date'),5,2);
        $data_no        = $this->purchase_requisition_model->get_nomor_dok($periode);
        $nomor_dok      = $data_no->nomor_dok;
        $pr_no          = 'PRS'.$periode.$nomor_dok;

        $new_pr         = $this->session->userdata('new_pr');
        foreach ($new_pr['items'] as $key => $value) {
            $this->purchase_requisition_model02->setPrId($this->security->xss_clean($id));
            $this->purchase_requisition_model02->setItemsId($this->security->xss_clean($value['item_id']));
            $this->purchase_requisition_model02->setItemsName($this->security->xss_clean($value['item_name']));
            $this->purchase_requisition_model02->setQty($this->security->xss_clean($value['item_qty']));
            $this->purchase_requisition_model02->setIsActive($this->security->xss_clean('1'));
            $this->purchase_requisition_model02->setPicInput($this->security->xss_clean($this->current_user['user_id']));
            $this->purchase_requisition_model02->setInputTime($this->security->xss_clean(dbnow()));
            $this->purchase_requisition_model02->insert();
        }

        $this->purchase_requisition_model->setPrId($this->security->xss_clean($id));
        $this->purchase_requisition_model->setPrNo($this->security->xss_clean($pr_no));
        $this->purchase_requisition_model->setPrDate($this->security->xss_clean($_POST['pr_date']));
        $this->purchase_requisition_model->setRequester($this->security->xss_clean($_POST['requester']));
        $this->purchase_requisition_model->setCompanyId($this->security->xss_clean($_POST['company_id']));
        $this->purchase_requisition_model->setCompanyName($this->security->xss_clean($_POST['company_name']));
        $this->purchase_requisition_model->setPrInfo($this->security->xss_clean($_POST['remarks']));
        $this->purchase_requisition_model->setPrStatus($this->security->xss_clean('Process'));
        $this->purchase_requisition_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->purchase_requisition_model->setInputTime($this->security->xss_clean(dbnow()));
        $this->purchase_requisition_model->setApproveLevel($this->security->xss_clean('0'));

        $save   = $this->purchase_requisition_model->insert();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            jsout(array('success' => false));
        }else{
            $this->db->trans_commit();
            jsout(array('success' => true, 'status' => $save));
        }

        
        
    }

    function edit($id){
        $this->session->unset_userdata('new_pr');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');

        $detail     = $this->purchase_requisition_model02->getByIdPr($id)->result();
        $tdetail    = $this->purchase_requisition_model02->getByIdPr($id)->num_rows();

        $new_pr = $this->session->userdata('new_pr');

        if($tdetail==0){
            $new_pr = array(
                'items' => array()
            );
        }else{
            foreach($detail as $key=>$val){
                $new_pr['items'][$key] = array(
                    'det_id'        => $val->pr_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'item_qty'      => $val->qty,
                    'item_info'     => $val->remarks
                );
            }
        }

        $this->session->set_userdata('new_pr', $new_pr);
        if($tdetail!=0){
            $data['id_detail']      = $val->pr_02_id;
        }else{  
            $data['id_detail']      = 0;
        }
        
        $data['new_pr']         = $new_pr;
        $data['header']         = $this->purchase_requisition_model->getById($id);
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        $this->template->load('body', 'transaction/purchase_requisition/pr_edit', $data);
    }

    function edit_act(){

        $id = $this->security->xss_clean($_POST['pr_id']);

        $this->purchase_requisition_model02->delete($id);
        $new_pr         = $this->session->userdata('new_pr');
        foreach ($new_pr['items'] as $key => $value) {
            $this->purchase_requisition_model02->setPrId($this->security->xss_clean($id));
            $this->purchase_requisition_model02->setItemsId($this->security->xss_clean($value['item_id']));
            $this->purchase_requisition_model02->setItemsName($this->security->xss_clean($value['item_name']));
            $this->purchase_requisition_model02->setQty($this->security->xss_clean($value['item_qty']));
            $this->purchase_requisition_model02->setIsActive($this->security->xss_clean('1'));
            $this->purchase_requisition_model02->setPicInput($this->security->xss_clean($this->current_user['user_id']));
            $this->purchase_requisition_model02->setInputTime($this->security->xss_clean(dbnow()));
            $this->purchase_requisition_model02->insert();
        }

        $this->purchase_requisition_model->setPrDate($this->security->xss_clean($_POST['pr_date']));
        $this->purchase_requisition_model->setRequester($this->security->xss_clean($_POST['requester']));
        $this->purchase_requisition_model->setCompanyId($this->security->xss_clean($_POST['company_id']));
        $this->purchase_requisition_model->setCompanyName($this->security->xss_clean($_POST['company_name']));
        $this->purchase_requisition_model->setPrInfo($this->security->xss_clean($_POST['remarks']));
        $this->purchase_requisition_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->purchase_requisition_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->purchase_requisition_model->update($id);

        $this->session->unset_userdata('new_pr');
        jsout(array('success' => true, 'status' => $update ));
    }

    function delete_js(){
        $delete = $this->purchase_requisition_model->act_delete_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    function reject_js(){
        $delete = $this->purchase_requisition_model->act_reject_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    function view_popup($id){

        $header         = $this->purchase_requisition_model->getById($id);
        $detail         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $myData = array();
        $no     = 0;
        foreach ($detail as $key => $row) {
            $no     = $no+1;
            $myData[] = array(
                $no,
                $row['items_code'],
                $row['items_name'],     
                number_format($row['qty'],2)        
            );            
        }  

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function view_print($id){
        $data['header']         = $this->purchase_requisition_model->print_header($id);
        $data['detail']         = $this->purchase_requisition_model02->print_detail($id);
        // $data['detail']         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $this->template->load('body_print', 'transaction/purchase_requisition/pr_print', $data);
    }





}

// Penambahan PR Print
?>