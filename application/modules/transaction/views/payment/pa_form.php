<?php 
//test($data_po[0]->supplier_id,1);
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">Purchase Order Payment</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Purchase Order Payment</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal" method="post" action="<?php echo base_url().'transaction/po_payment/form_act'; ?>">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control"value="<?php echo $data_company['company_name']; ?>" disabled>
                <input type="hidden" class="form-control" name="company_name" value="<?php echo $data_company['company_name']; ?>">
                <input type="hidden" class="form-control" name="company_id" value="<?php echo $data_company['company_id']; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal </p>
              </div>
              <div class="col-md-3">
                <!-- <input type="text" class="form-control"  value="<?php echo substr(dbnow(),0,10); ?>" disabled> -->
                <input type="date" class="form-control" id="pp_date" name="pp_date">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Supplier</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control"  value="<?php echo $data_po[0]->supplier_name; ?>" disabled>
                <input type="hidden" name="supplier_name" value="<?php echo $data_po[0]->supplier_name; ?>">
                <input type="hidden" name="supplier_id" value="<?php echo $data_po[0]->supplier_id; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tipe Pembayaran</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id="payment_type" name="payment_type">
                  <option value="">Select</option>
                  <option value="Cash">Cash</option>
                  <option value="Transfer">Transfer</option>
                  <option value="Giro">Giro</option>
                  <option value="Cheque">Cek</option>                  
                </select>
              </div>
            </div>
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div> -->
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th width="15%">Nomor PO</th>
                        <th width="12%">Tanggal PO</th>
                        <th>Nama Supplier</th>
                        <th width="17%">Nilai PO</th>
                        <th width="17%">Sisa PO</th>
                        <th width="17%">Total Pembayaran</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      // test($data_po,0);
                      $sisa     = 0;
                      $tsisa    = 0;
                      foreach ($data_po as $key => $value) {
                      $sisa     = $value->total - $value->tbayar;
                      $tsisa    = $sisa+$tsisa;
                      ?>
                      <tr>
                        <td></td>                          
                        <td>
                          <input type="hidden" id="po_id<?php echo $key; ?>" name="po_id[]" value="<?php echo $value->po_id ?>">
                          <input type="hidden" id="po_no<?php echo $key; ?>" name="po_no[]" value="<?php echo $value->po_no ?>">
                          <input type="hidden" id="jml_hutang<?php echo $key; ?>" name="jml_hutang[]" value="<?php echo $sisa ?>">                          
                          <?php echo $value->po_no; ?>
                        </td>
                        <td><?php echo $value->po_date; ?></td>
                        <td><?php echo $value->supplier_name; ?></td>
                        <td><?php echo money($value->total); ?></td>
                        <td><?php echo money($sisa); ?></td>
                        <td><input  onchange="return tambah(this)" class="form-control price" type="text" name="total[]" id="total<?php echo $key; ?>" value="<?php echo number_format($sisa,2); ?>" ></td>
                      </tr>
                      <?php 
                      }
                      $key    = $key+1;
                      ?>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="right"><strong>TOTAL :</strong></td>
                        <td><input disabled="" class="form-control price" type="text" name="total_pembayaran" id="total_pembayaran" value="<?php echo number_format($tsisa,2); ?>"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input type="hidden" id="tdetail" name="tdetail" value="<?php echo $key; ?>">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm" type="submit" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('transaction/po_payment'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

function tambah(e){
  debugger
  var i;
  var tdetail     = $('#tdetail').val();
  var tpayment    = 0;

  for (i = 0; i < tdetail; i++) {
    var total      = parseFloat($('#total'+i).val().replace(/\,/g, ''));
    tpayment            = total+tpayment;

  }

  $('#total_pembayaran').val(tpayment);
}

$('#save').click(function(){
  
  if(!$('#pp_date').val()){
    toastr.error("<strong>Tanggal Pembayaran</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#pp_date').focus();
    return false;
  }
  
  if(!$('#payment_type').val()){
    toastr.error("<strong>Tipe Pembayaran</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
    $('#payment_type').select2('open');
    return false;
  }

  
  var i;
  var tdetail     = $('#tdetail').val();

  for (i = 0; i < tdetail; i++) {
    var jml_hutang      = parseFloat($('#jml_hutang'+i).val().replace(/\,/g, ''));
    var total           = parseFloat($('#total'+i).val().replace(/\,/g, ''));
    var po_no           = $('#po_no'+i).val();

    if(total > jml_hutang){
      toastr.error("Jumlah Pembayaran Melebihi Jumlah Hutang <strong>"+po_no+"</strong> ", 'Alert', {"positionClass": "toast-top-center"});
      $("#total"+i).focus();
      return false;
    }

  }



  

  // if(!$('#supplier_id').val()){
  //   toastr.error("<strong>Supplier Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#supplier_id').select2('open');
  //   return false;
  // }
});

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#payment_type").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4,5],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ]
    });

    // $('#save').click(pr.save);

  }

};

pr.init();

</script>

<!--  
1. Tanggal Pembayaran Enable (Bisa di Isi.)
-->