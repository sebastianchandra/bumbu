<?php 
// test(,1);
if($this->session->flashdata('alert')!=''){
// if(isset($this->session->flashdata('alert'))){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">Purchase Pemutihan</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View View Pemutihan</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-primary" href="<?php echo base_url('transaction/po_payment/pemutihan_act'); ?>">Proses</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>No. PO</th>
                    <th>Payment Status</th>
                    <th>Total PO</th>
                    <th>Total Pembaaran</th>
                    <th>Sisa</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_pemutihan as $key => $value) {
                  ?>
                  <tr>
                      <td><strong><?php echo $value->po_no; ?></a></strong></td>
                      <td><?php echo $value->payment_status; ?></td>
                      <td align="right"><?php echo money($value->total); ?></td>
                      <td align="right"><?php echo money($value->tpayment); ?></td>
                      <td align="right"><?php echo money($value->sisa); ?></td>
                      
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myPr" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Nomor Purchase Order</h4>
      </div>
      <div class="modal-body">
        <div class="row-form">
          <div class="col-md-12">
            <div class="row-form">
              <div class="col-md-2">
                <p>Nama Supplier</p>
              </div>
              <div class="col-md-7">
                <select class="form-control" id="supplier_id" name="supplier_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_supplier as $key => $value) {
                    echo "<option value='".$value->supplier_id."' data-supplier_name='".$value->supplier_name."'>".$value->supplier_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="table-responsive">              
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th align="center">Nomor PO</th>
                    <th align="center">Nama Supplier</th>
                    <th align="center">Nilai PO</th>
                    <th align="center">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Next</button>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Pembayaran Pesanan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Nomor</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_date"></div>
          </div>          
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Nama Supplier</strong></p>
          </div>
          <div class="col-md-4">
            <div id="supplier_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tipe </strong></p>
          </div>
          <div class="col-md-4">
            <div id="payment_type"></div>
          </div>
        </div>
       
        <div class="row-form">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nomor Pesanan Pembelian</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
// $("#supplier_id").select2();

function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }
  if(values.length>=1){
    $.ajax({
      url: baseUrl+'transaction/po_payment/pa_selected',
      type : "POST",  
      data: {
        values      : values
      },
      success : function(resp){        
        setTimeout(function () {
          window.location.href = baseUrl+'transaction/po_payment/pa_input'; 
        }, 100);
      }
    });
  }else{
    toastr.warning("Pilih Nomor Purchase Order.", 'Alert', {"positionClass": "toast-top-center"});
    return false;
  }
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});


function show_pr(e){  
  // $.get({
  //   url: baseUrl + 'transaction/po_payment/view_po/',
  //   success: function(resp){

  //     popup_pr.clear().draw();
  //     var dataSrc = JSON.parse(resp.detail);                 
  //     popup_pr.rows.add(dataSrc).draw(false);
  //   }
  // });

  $('#myPr').modal('show'); 
}


$('#supplier_id').change(function(e){
  var supplier_id   = $('#supplier_id').val();
  $.get({
    url: baseUrl + 'transaction/po_payment/view_po/'+supplier_id,
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });
})
// function show_po(e){
//   alert('tes')
// }

function show_detail(e){
  var idPa     = $(e).data('id');
  var noPa     = $(e).data('pp');

  $.get({
    url: baseUrl + 'transaction/po_payment/view_popup/'+idPa,
    success: function(resp){
      $('#pp_no').text(resp.header.pp_no);
      $('#pp_date').text(tanggal_indonesia(resp.header.pp_date));
      $('#supplier_name').text(resp.header.supplier_name);
      $('#payment_type').text(resp.header.payment_type);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pp_id  = $(this).data('pp_id');
    var pp_no  = $(this).data('pp_no');

    toastr.warning(
      'Apakah anda Ingin Membatalkan Nomor Pembayaran '+pp_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pp_no="'+pp_no+'" data-pp_id="'+pp_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var pp_id  = $(e).data('pp_id');
  var pp_no  = $(e).data('pp_no');

  $.ajax({
    data: {
      pp_id  : pp_id,
      pp_no  : pp_no
    },
    type : "POST",
    url: baseUrl+'transaction/po_payment/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("Data tidak berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/po_payment'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!--  
1. Penambahan Reject Untuk pembayaran PO
2. Penambahan Popup untuk detail dan header pembayaran.
-->