<?php defined('BASEPATH') || exit('No direct script access allowed');
//https://tutorialweb.net/cara-mudah-export-data-dari-mysql-ke-excel-dengan-php/
    function export_to_excel($header,$detail,$data_currency)
    {       
        // test($header,0);
        // test($detail,1);
        header("Content-type: application/vnd-ms-excel");
        // Mendefinisikan nama file ekspor "hasil-export.xls"
        header("Content-Disposition: attachment; filename=".$header->po_no.".xls");

        foreach ($data_currency as $key => $value) { 
          if($header->currency_id==$value['id']){ 
            $currency = $value['nama']; 
          }  
        }

        echo  '<table border=1>';
        echo    '<tr>';
        echo      '<td>No. PO</td>';  
        echo      '<td>'.$header->po_no.'</td>'; 
        echo      '<td>No. PR</td>'; 
        echo      '<td>'.$header->pr_no.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Date</td>';  
        echo      '<td align="left">'.$header->po_date.'</td>'; 
        echo      '<td>Company</td>'; 
        echo      '<td>'.$header->company_name.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Requester</td>';  
        echo      '<td>'.$header->requester.'</td>'; 
        echo      '<td>Project Name</td>'; 
        echo      '<td>'.$header->project_name.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Supplier</td>';  
        echo      '<td>'.$header->supplier_name.'</td>'; 
        echo      '<td>Delivery Address</td>'; 
        echo      '<td>'.$header->delivery_address.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Info</td>';  
        echo      '<td>'.$header->po_info.'</td>'; 
        echo      '<td>Currency</td>'; 
        echo      '<td>'.$currency.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Transport Fee</td>';  
        echo      '<td>'.number_format($header->transport_fee,0,',','.').'</td>'; 
        echo      '<td>PPN</td>'; 
        echo      '<td>'.number_format($header->ppn_value,0,',','.').'</td>'; 
        echo    '</tr>';

        echo  '<table>';
        echo  '</br>';
        echo  '<table border=1>';
        echo    '<tr>';
        echo      '<td>No. PO</td>';  
        echo      '<td>'.$header->po_no.'</td>'; 
        echo      '<td>No. PR</td>'; 
        echo      '<td>'.$header->pr_no.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Date</td>';  
        echo      '<td align="left">'.$header->po_date.'</td>'; 
        echo      '<td>Company</td>'; 
        echo      '<td>'.$header->company_name.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Requester</td>';  
        echo      '<td>'.$header->requester.'</td>'; 
        echo      '<td>Project Name</td>'; 
        echo      '<td>'.$header->project_name.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Supplier</td>';  
        echo      '<td>'.$header->supplier_name.'</td>'; 
        echo      '<td>Delivery Address</td>'; 
        echo      '<td>'.$header->delivery_address.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Info</td>';  
        echo      '<td>'.$header->po_info.'</td>'; 
        echo      '<td>Currency</td>'; 
        echo      '<td>'.$currency.'</td>'; 
        echo    '</tr>';

        echo    '<tr>';
        echo      '<td>Transport Fee</td>';  
        echo      '<td>'.number_format($header->transport_fee,0,',','.').'</td>'; 
        echo      '<td>PPN</td>'; 
        echo      '<td>'.number_format($header->ppn_value,0,',','.').'</td>'; 
        echo    '</tr>';

        echo  '<table>';
        
     }   
export_to_excel($header,$detail,$data_currency);
?>