<?php 
// test($count,0);
?>
<style>
  @media print{
    @page {
      size: portrait;

      }
    }
  table{
    border-collapse: collapse;
    font-family: arial;
  }
  .borderluar {
    border: 2px solid black;
    padding: 0px;
  }
  .borderdalem {
    border: 1px solid #000000b0;
    padding: 0px;
  }
  .borderdalemcenter {
    border: 1px solid #000000b0;
    padding: 0px;
    text-align: center;
  }
  .borderdalemangka {
    border: 1px solid #000000b0;
    padding: 0px 5px;
    text-align: right;
  }
  .borderdalemangka_detail {
    padding: 0px 5px;
  }
  .bordertengah {
    border: 1px solid #000000b0;
    padding: 5px 5px 5px 5px; 
  }
  p.two {
    border-style: solid;
    border-width: 1px;
  }
  .header{
    padding: 0px 30px; 
  }
  .header_alamat {
    border: 2px solid black;
    padding: 0px 30px; 
    text-align: center;
  }
  .table_detail {
    border-collapse: collapse;
    border: 1px solid black;
  }
</style>

<div class="app-title">
	<div>
		<h1>View Purchase Order</h1>
		<p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Transaction</li>
      <li class="breadcrumb-item">Purchase Order</li>
      <li class="breadcrumb-item active">Print Purchase Order</li>
    </ul></p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <table style="width:100%">
        <tr>
          <td><table style='width:100%'> 
            <tr>
              <td align="left" colspan="2" width='50%' class="header" valign="bottom">
                <strong style="font-size: 20px;">
                  <?php 
                  if($header->company_id=='2'){
                    echo "PT SANGATI SOERYA SEJAHTERA";
                  }elseif($header->company_id=='1'){
                    echo 'L&M System Indonesia';
                  }elseif($header->company_id=='3'){
                    echo 'PT Senengnya Pelesiran Avontourier';
                  }
                  ?>
                </strong>
              </td>
              <td align="right" colspan="2" width='50%' class="header">
                <?php 
                if($header->company_id=='2'){
                  echo '<img alt="" src="'.base_url().'assets/images/sangati_logo.png">';
                }elseif($header->company_id=='1'){
                  echo '<img width="110" height="90" alt="" src="'.base_url().'assets/images/lm_logo1.png">';
                }elseif($header->company_id=='3'){
                  echo '<img style="width: 25%;" alt="" src="'.base_url().'assets/images/logo_avon.png">';
                }
                ?>
              </td>
            </tr>
            <tr>
              <td align="left" colspan="4" width='50%' class="header_alamat"><strong>Jl. Ir. H. Juanda III No. 8, Jakarta 10120, Indonesia<br/>Telp: (62-21)3458568 (Hunting) Fax: (62-21)3454044</strong></td>
            </tr>
            <tr>
              <td colspan='4' align="center"><strong style="font-size: 20px;">PURCHASE ORDER / WORK ORDER</strong></td>
            </tr>
            <tr>
              <td colspan='3' valign="top"><strong>TO : <br/></strong><?php echo $header->supplier_name; ?></td>
              <td colspan='1' valign="top">
                <table width="100%">
                  <tr>
                    <td width="30%"><strong>PO. Number</strong></td>
                    <td>: <?php echo $header->po_no; ?></td>
                  </tr>
                  <tr>
                    <td><strong>PO. Date</strong></td>
                    <td>: <?php echo tgl_singkat($header->po_date); ?></td>
                  </tr>
                  <tr>
                    <td><strong>PR. Number</strong></td>
                    <td>: <?php echo $header->pr_no; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Requester</strong></td>
                    <td>: <?php echo $header->requester; ?></td>
                  </tr>
                  <tr>
                    <td valign="top"><strong>Delivery Address</strong></td>
                    <td>: <?php echo $header->delivery_address; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Print Number</strong></td>
                    <td>: <?php echo $no_print; ?></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan='4' class='borderdalem'>
                <table width='100%' class="table_detail">
                  <tbody>
                    <tr>
                      <td class='borderdalemcenter' width="5%">No.</td>
                      <td class='borderdalemcenter'>Items Name</td>
                      <td class='borderdalemcenter'>Remarks</td>
                      <td class='borderdalemcenter' width="10%">Qty</td>
                      <td class='borderdalemcenter' width="15%">Price</td>
                      <td class='borderdalemcenter' width="17%">Subtotal</td>
                      
                    </tr>
                  </tbody>
                  <thread>
                    <?php
                    $no     = 0;
                    $max    = 20;
                    $subtotal=0;
                    foreach ($detail as $key => $value) {
                      $no = $no+1;
                      ?>
                      <tr>
                        <td class='borderdalemangka_detail' align="right"><?php echo $no; ?>.</td>
                        <td class='borderdalemangka_detail'><?php echo $value->items_name; ?></td>
                        <td class='borderdalemangka_detail'><?php echo $value->remarks; ?></td>
                        <td class='borderdalemangka_detail' align="right"><?php echo money($value->qty); ?></td>
                        <td class='borderdalemangka_detail' align="right">Rp. <?php echo money($value->price); ?></td>
                        <td class='borderdalemangka_detail' align="right">Rp. <?php echo money($value->subtotal); ?></td>
                      </tr>
                      <?php 
                      $subtotal = $subtotal+$value->subtotal;
                    }
                    for($a=$no+1;$a<=$max;$a++){
                      ?>
                      <tr>
                        <td align="right" height="23px" colspan="6"></td>
                      </tr>
                      <?php
                    }
                    ?>
                  </thread>
                </table>
                <?php 
                $baris = 2;
                if($header->pph==1){
                  $baris = $baris+1;
                }
                if($header->pbbkb==1){
                  $baris = $baris+1;
                }
                ?>
                <table width='100%' class="table_detail">
                  <thread>
                    <tr>
                      <td align="center"></td>
                      <td align="center"></td>
                      <td align="center"></td>
                      <td align="right">Subtotal :</td>
                      <td class='borderdalemangka' align="right" width="17%">Rp. <?php echo money($subtotal); ?></td>
                    </tr>
                    <tr>
                      <td align="center" rowspan="<?php echo $baris; ?>"></td>
                      <td align="center" rowspan="<?php echo $baris; ?>"></td>
                      <td align="center"></td>
                      <td align="right">PPN :</td>
                      <td class='borderdalemangka' align="right">Rp. <?php echo money($header->ppn_value); ?></td>
                    </tr>
                    <tr>
                      <td align="right"></td>
                      <td align="right">Transport Fee :</td>
                      <td class='borderdalemangka' align="right">Rp. <?php echo money($header->transport_fee); ?></td>
                    </tr>
                    <?php 
                    if($header->pph==1){
                    ?>
                    <tr>
                      <td align="right"></td>
                      <td align="right">PPH</td>
                      <td class='borderdalemangka' align="right">Rp. <?php echo money($header->pph_value); ?></td>
                    </tr>
                    <?php 
                    }
                    if($header->pbbkb==1){
                    ?>
                    <tr>
                      <td align="right"></td>
                      <td align="right">Pbbkb</td>
                      <td class='borderdalemangka' align="right">Rp. <?php echo money($header->pbbkb_value); ?></td>
                    </tr>
                    <?php 
                    }
                    ?>
                    <tr>
                      <td align="center"></td>
                      <td align="center"></td>
                      <td align="center"></td>
                      <td align="right">Total :</td>
                      <td class='borderdalemangka' align="right">Rp. <?php echo money($header->total); ?></td>
                    </tr>
                  </thread>
                </table>
                <table width='100%' class="table_detail">
                  <thread>
                    <tr>
                      <td colspan="4" align="center">Approve By</td>
                      <td colspan="1" align="left"> </td>
                      <td colspan="1" align="right">Print Date : <?php echo dbnow(); ?></td>
                    </tr>
                  </thread>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</div>
</div>

</head>
<script>window.print(); setTimeout(function(){window.close();},500);</script>