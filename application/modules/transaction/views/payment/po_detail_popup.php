<?php 
//test($header,1);
?>
<section class="content-body" style="padding-top:0;">
    <div class="tile-body">
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">No. PO</label>
                    <div class="col-md-7">: <?php echo $header->po_no; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">No. PR</label>
                    <div class="col-md-7">: <?php echo $header->pr_no; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Date</label>
                    <div class="col-md-7">: <?php echo tanggal($header->po_date); ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Company</label>
                    <div class="col-md-7">: <?php echo $header->company_name; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Requester</label>
                    <div class="col-md-7">: <?php echo $header->requester; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Project Name</label>
                    <div class="col-md-7">: <?php echo $header->project_name; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Supplier</label>
                    <div class="col-md-7">: <?php echo $header->supplier_name; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Delivery Address</label>
                    <div class="col-md-7">: <?php echo $header->delivery_address; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Info</label>
                    <div class="col-md-7">: <?php echo $header->po_info; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Currency</label>
                    <div class="col-md-7">: <?php foreach ($data_currency as $key => $value) { if($header->currency_id==$value['id']){ echo $value['nama']; }  }  ?></div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Transport Fee</label>
                    <div class="col-md-7">: Rp. <?php echo money($header->transport_fee); ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">PPN</label>
                    <div class="col-md-7">: Rp. <?php echo money($header->ppn_value); ?></div>
                </div>
            </div>
        </div> -->
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="center">
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                    <th width="13%">Exchange Rate</th>
                    <th>Info</th>
                    <th width="13%">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $total = 0;
                $subtotal = 0;
                foreach ($detail as $key => $value) {
                ?>
                <tr>
                    <td class="popup"><?php echo $value->items_name; ?></td>
                    <td align="right" class="popup"><?php echo money($value->qty); ?></td>
                    <td align="right" class="popup"><?php echo money($value->price); ?></td>
                    <td align="right" class="popup"><?php echo money($value->exchange_rate); ?></td>
                    <td class="popup"><?php echo $value->remarks; ?></td>
                    <td align="right" class="popup"><?php echo money($value->subtotal); ?></td>
                </tr>
                <?php 
                $subtotal = $subtotal+$value->subtotal;
                }
                $total = $subtotal+$header->transport_fee+$header->ppn_value+$header->pph_value+$header->pbbkb_value;
                ?>
            </tbody>
                <tr>
                    <td class="popup" colspan="5" align="right">Subtotal :</td>
                    <td class="popup" align="right"><?php echo money($subtotal); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">PPN : (Subtotal * <?php echo $header->ppn_percent; ?>%)</td>
                    <td class="popup" align="right"><?php echo money($header->ppn_value); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Transport Fee :</td>
                    <td class="popup" align="right"><?php echo money($header->transport_fee); ?></td>
                </tr>
                <?php 
                if($header->pph==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">PPH</td>
                    <td class="popup" align="right"><?php echo money($header->pph_value); ?></td>
                </tr>
                <?php 
                }
                if($header->pbbkb==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Pbbkb</td>
                    <td class="popup" align="right"><?php echo money($header->pbbkb_value); ?></td>
                </tr>
                <?php 
                }
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Total :</td>
                    <td class="popup" align="right"><?php echo money($total); ?></td>
                </tr>
        </table>
    </div>
</section>