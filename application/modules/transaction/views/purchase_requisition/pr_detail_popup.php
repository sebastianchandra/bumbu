<?php 
// test($header,0);
?>
<section class="content-body" style="padding-top:0;">
    <div class="tile-body">
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Company</label>
                    <div class="col-md-7">: <?php echo $header->company_name; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Request Number</label>
                    <div class="col-md-7">: <?php echo $header->request_no; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Requester</label>
                    <div class="col-md-7">: <?php echo $header->requester; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Date</label>
                    <div class="col-md-7">: <?php echo tanggal($header->pr_date); ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Supplier</label>
                    <div class="col-md-7">: <?php echo $header->supplier_name; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Project Name</label>
                    <div class="col-md-7">: <?php echo $header->project_name; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Info</label>
                    <div class="col-md-7">: <?php echo $header->pr_info; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Delivery Address</label>
                    <div class="col-md-7">: <?php echo $header->delivery_address; ?></div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Transport Fee</label>
                    <div class="col-md-7">: Rp. <?php echo money($header->transport_fee); ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">PPN</label>
                    <div class="col-md-7">: Rp. <?php echo money($header->ppn_value); ?></div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">File Quotation</label>
                    <div class="col-md-7">: <?php echo ($header->file_quotation==0)? 'No File' : '<a target="_blank" href="'.base_url().'file_upload/quotation/'.$header->pr_no.'.pdf">'.$header->pr_no.'</a>';?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Currency</label>
                    <div class="col-md-7">: <?php foreach ($data_currency as $key => $value) { if($header->currency_id==$value['id']){ echo $value['nama']; }  }  ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="Left">
                    <td colspan="6" style="padding: 0.3rem !important;"><strong>Name : <?php echo $header->supplier_name; ?></strong></td>
                </tr>
                <tr align="center">
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                    <th width="13%">Exchange Rate</th>
                    <th>Info</th>
                    <th width="13%">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $total = 0;
                $subtotal = 0;
                foreach ($detail as $key => $value) {
                ?>
                <tr>
                    <td class="popup"><?php echo $value->items_name; ?></td>
                    <td align="right" class="popup"><?php echo money($value->qty); ?></td>
                    <td align="right" class="popup"><?php echo money($value->price); ?></td>
                    <td align="right" class="popup"><?php echo money($value->exchange_rate); ?></td>
                    <td class="popup"><?php echo $value->remarks; ?></td>
                    <td align="right" class="popup"><?php echo money($value->subtotal); ?></td>
                </tr>
                <?php 
                $subtotal = $subtotal+$value->subtotal;
                }
                $transport_fee1     = $header->transport_fee;
                $ppn_percent1        = ($header->ppn_percent*$subtotal)/100;

                $total = $subtotal+$transport_fee1+$ppn_percent1+$header->pph_value+$header->pbbkb_value;
                ?>
            </tbody>
                <tr>
                    <td class="popup" colspan="5" align="right">Subtotal :</td>
                    <td class="popup" align="right"><?php echo money($subtotal); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">PPN : (Subtotal * <?php echo $header->ppn_percent; ?>%)</td>
                    <td class="popup" align="right"><?php echo money($ppn_percent1); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Transport Fee :</td>
                    <td class="popup" align="right"><?php echo money($header->transport_fee); ?></td>
                </tr>
                <?php 
                if($header->pph==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">PPH :</td>
                    <td class="popup" align="right"><?php echo money($header->pph_value); ?></td>
                </tr>
                <?php 
                }
                if($header->pbbkb==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Pbbkb :</td>
                    <td class="popup" align="right"><?php echo money($header->pbbkb_value); ?></td>
                </tr>
                <?php 
                }
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Total :</td>
                    <td class="popup" align="right"><strong><?php echo money($total); ?></strong></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Term of Payment</td>
                    <td class="popup" align="right"><?php echo $header->top; ?> Days</td>
                </tr>
        </table>
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="Left">
                    <td colspan="6" style="padding: 0.3rem !important;"><strong>Name : <?php echo $header->supplier_name2; ?></strong></td>
                </tr>
                <tr align="center">
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                    <th width="13%">Exchange Rate</th>
                    <th>Info</th>
                    <th width="13%">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $total = 0;
                $subtotal = 0;
                // test($detail2,0);
                foreach ($detail2 as $key => $value) {
                    $subtotal = $value->qty*$value->items_price*$value->exchange_rate;
                ?>
                <tr>
                    <td class="popup"><?php echo $value->items_name; ?></td>
                    <td align="right" class="popup"><?php echo money($value->qty); ?></td>
                    <td align="right" class="popup"><?php echo money($value->items_price); ?></td>
                    <td align="right" class="popup"><?php echo money($value->exchange_rate); ?></td>
                    <td class="popup"><?php echo $value->remarks; ?></td>
                    <td align="right" class="popup"><?php echo money($subtotal); ?></td>
                </tr>
                <?php 
                    $total = $subtotal+$total;
                }
                if($header->pph=='1'){
                    $total_pph = round((0.3*$total)/100);
                }else{
                    $total_pph = 0;
                }

                if($header->pbbkb=='1'){
                    $total_pbbkb = round((((17.17*$total)/100)*5)/100);
                }else{
                    $total_pbbkb = 0;
                }

                $transport_fee2     = $header->transport_fee2;
                $ppn_percent2       = ($header->ppn_percent2*$total)/100;
                                    
                $total2             = $total+$total_pph+$total_pbbkb+$transport_fee2+$ppn_percent2;

                ?>
            </tbody>
                <tr>
                    <td class="popup" colspan="5" align="right">Subtotal :</td>
                    <td class="popup" align="right"><?php echo money($total); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">PPN : (Subtotal * <?php echo $header->ppn_percent2; ?>%)</td>
                    <td class="popup" align="right"><?php echo money($ppn_percent2); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Transport Fee :</td>
                    <td class="popup" align="right"><?php echo money($header->transport_fee2); ?></td>
                </tr>
                <?php 
                if($header->pph==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">PPH :</td>
                    <td class="popup" align="right"><?php echo money($total_pph); ?></td>
                </tr>
                <?php 
                }
                if($header->pbbkb==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Pbbkb :</td>
                    <td class="popup" align="right"><?php echo money($total_pbbkb); ?></td>
                </tr>
                <?php 
                }
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Total :</td>
                    <td class="popup" align="right"><strong><?php echo money($total2); ?></strong></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Term of Payment</td>
                    <td class="popup" align="right"><?php echo $header->top2; ?> Days</td>
                </tr>
        </table>
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="Left">
                    <td colspan="6" style="padding: 0.3rem !important;"><strong>Name : <?php echo $header->supplier_name3; ?></strong></td>
                </tr>
                <tr align="center">
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                    <th width="13%">Exchange Rate</th>
                    <th>Info</th>
                    <th width="13%">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $total = 0;
                $subtotal = 0;
                foreach ($detail3 as $key => $value) {
                    $subtotal = $value->qty*$value->items_price*$value->exchange_rate;
                ?>
                <tr>
                    <td class="popup"><?php echo $value->items_name; ?></td>
                    <td align="right" class="popup"><?php echo money($value->qty); ?></td>
                    <td align="right" class="popup"><?php echo money($value->items_price); ?></td>
                    <td align="right" class="popup"><?php echo money($value->exchange_rate); ?></td>
                    <td class="popup"><?php echo $value->remarks; ?></td>
                    <td align="right" class="popup"><?php echo money($subtotal); ?></td>
                </tr>
                <?php 
                    $total = $subtotal+$total;
                }
                if($header->pph=='1'){
                    $total_pph = round((0.3*$total)/100);
                }else{
                    $total_pph = 0;
                }

                if($header->pbbkb=='1'){
                    $total_pbbkb = round((((17.17*$total)/100)*5)/100);
                }else{
                    $total_pbbkb = 0;
                }
                
                $transport_fee3     = $header->transport_fee3;
                $ppn_percent3       = ($header->ppn_percent3*$total)/100;
                                    
                $total3             = $total+$total_pph+$total_pbbkb+$transport_fee3+$ppn_percent3;
                ?>
            </tbody>
                <tr>
                    <td class="popup" colspan="5" align="right">Subtotal :</td>
                    <td class="popup" align="right"><?php echo money($total); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">PPN : (Subtotal * <?php echo $header->ppn_percent3; ?>%)</td>
                    <td class="popup" align="right"><?php echo money($ppn_percent3); ?></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Transport Fee :</td>
                    <td class="popup" align="right"><?php echo money($transport_fee3); ?></td>
                </tr>
                <?php 
                if($header->pph==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">PPH :</td>
                    <td class="popup" align="right"><?php echo money($total_pph); ?></td>
                </tr>
                <?php 
                }
                if($header->pbbkb==1){
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Pbbkb :</td>
                    <td class="popup" align="right"><?php echo money($total_pbbkb); ?></td>
                </tr>
                <?php 
                }
                ?>
                <tr>
                    <td class="popup" colspan="5" align="right">Total :</td>
                    <td class="popup" align="right"><strong><?php echo money($total3); ?></strong></td>
                </tr>
                <tr>
                    <td class="popup" colspan="5" align="right">Term of Payment</td>
                    <td class="popup" align="right"><?php echo $header->top3; ?> Days</td>
                </tr>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="center">
                    <th width="25%">Approve By</th>
                    <th width="20%">Approve Date</th>
                    <th width="12%">Status</th>
                    <th width="15%">Preferred Supplier</th>
                    <th >Note</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach ($approve_request as $key => $value) {
                ?>
                <tr>
                    <td class="popup"><?php echo $value->name.' ('.$value->user_level_name.')'; ?></td>
                    <td class="popup"><?php echo tanggal($value->date); ?></td>
                    <td class="popup"><?php echo ($value->is_approve==1)? 'Approved' : '<font color="red"><strong>Not Approved</strong></font>'; ?></td>
                    <td class="popup"><?php //echo $value->note; ?></td>
                    <td class="popup"><?php echo $value->note; ?></td>
                </tr>
                <?php 
                }
                ?>
                <?php 
                foreach ($approve as $key => $value) {
                ?>
                <tr>
                    <td class="popup"><?php echo $value->approval; ?></td>
                    <td class="popup"><?php echo tanggal($value->date).' '.substr($value->date, 11); ?></td>
                    <td class="popup"><?php echo ($value->is_approve==1)? 'Approved' : '<font color="red"><strong>Not Approved</strong></font>'; ?></td>
                    <td class="popup"><?php echo $value->supplier_name; ?></td>
                    <td class="popup"><?php echo $value->note; ?></td>
                </tr>
                <?php 
                }
                ?>
            </tbody>
        </table>
    </div>
</section>