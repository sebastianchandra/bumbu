<?php 
// test($header,0);
?>
<div class="app-title">
  <div>
    <h1>Approve Purchase Requisition</h1>
    <p><ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Transaction</li>
        <li class="breadcrumb-item">Purchase Requisition</li>
        <li class="breadcrumb-item active">Approve</li>
  </ul></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form class="form-horizontal">
          <div class="form-approve row">
            <label class="control-label col-md-2">Nomor</label>
            <div class="col-md-3">
              : <?php echo $header->pr_no; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Company</label>
            <div class="col-md-3">
              : <?php echo $header->company_name; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Date</label>
            <div class="col-md-8">
              : <?php echo tanggal($header->pr_date); ?>
              <input type='hidden' id="pr_id" value="<?php echo $header->pr_id; ?>">
              <input type="hidden" name="items"  id="sup_items" value='<?php echo json_encode($new_pr["items"]); ?>' required />
              <input type="hidden" name="items2" id="sup_items2" value='<?php echo json_encode($new_pr2["items"]); ?>' required />
              <input type="hidden" name="items3" id="sup_items3" value='<?php echo json_encode($new_pr3["items"]); ?>' required />
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Requester</label>
            <div class="col-md-3">
              : <?php echo $header->requester; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Project Name</label>
            <div class="col-md-5">
              : <?php echo $header->project_name; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Supplier</label>
            <div class="col-md-3">
              : <?php echo $data_supplier->supplier_name; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Delivery Address</label>
            <div class="col-md-5">
              : <?php echo $header->delivery_address; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Info</label>
            <div class="col-md-8">
              : <?php echo $header->pr_info; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Currency</label>
            <div class="col-md-8">
              : <?php foreach ($data_currency as $key => $value) { if($header->currency_id==$value['id']){ echo $value['nama']; }  }  ?>
            </div>
          </div>
          <!-- <div class="form-approve row">
            <label class="control-label col-md-2">Transport Fee</label>
            <div class="col-md-8">
              : Rp. <?php echo number_format($header->transport_fee,'2','.',','); ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">PPN</label>
            <div class="col-md-8">
              : <?php echo number_format($header->ppn_percent,'2','.',','); ?>%
            </div>
          </div> -->
          <div class="form-approve row">
            <label class="control-label col-md-2">File Quotation</label>
            <div class="col-md-8">
              : <?php echo ($header->file_quotation==0)? 'No File' : '<a target="_blank" href="'.base_url().'file_upload/quotation/'.$header->pr_no.'.pdf">'.$header->pr_no.'</a>';?>
            </div>
          </div>
        </form>
      </div>
      <!-- <div class="tile-footer"> -->
        <div class="row">
          <div class="col-md-4">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail">
                <thead>
                  <tr>
                    <td colspan="3" align="left"><input id="supplier_pilihan" type="radio" name="supplier_pilihan" value="<?php echo $header->supplier_id; ?>">&nbsp;&nbsp;&nbsp;<?php echo $header->supplier_name; ?></td>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $subtotal   = 0;
                  $ppn1       = 0;
                  $tsubtotal  = 0;
                  foreach ($detail as $key => $value) {
                  $subtotal = $value->qty*$value->price;
                  ?>
                  <tr>
                    <td><?php echo $value->items_name; ?></td>
                    <td align="right"><?php echo money($value->qty); ?></td>
                    <td align="right"><?php echo money($value->price); ?></td>
                  </tr>
                  <?php 
                  $tsubtotal    = $subtotal+$tsubtotal;
                  }

                  if($header->ppn_percent!=0){
                    $ppn1        = ($tsubtotal*$header->ppn_percent)/100;
                  }

                  if($header->pph=='1'){
                    $total_pph = round((0.3*$tsubtotal)/100);
                  }else{
                    $total_pph = 0;
                  }

                  if($header->pbbkb=='1'){
                    $total_pbbkb = round((((17.17*$tsubtotal)/100)*5)/100);
                  }else{
                    $total_pbbkb = 0;
                  }
                  $total  = $tsubtotal+$ppn1+$total_pph+$total_pbbkb+$header->transport_fee;
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2" align="right">Subtotal</td>
                    <td align="right"><?php echo money($tsubtotal); ?></td>
                  </tr>
                  <?php
                  if($header->ppn_percent!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">PPN ( Subtotal * <?php echo money($header->ppn_percent); ?>% )</td>
                    <td align="right"><?php echo money($ppn1); ?></td>
                  </tr>
                  <?php 
                  }
                  if($header->transport_fee!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">Transport Fee</td>
                    <td align="right"><?php echo money($header->transport_fee); ?></td>
                  </tr>
                  <?php
                  }
                  if($header->pph!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">PPH</td>
                    <td align="right"><?php echo money($total_pph); ?></td>
                  </tr>
                  <?php 
                  }
                  if($header->pph!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">Pbbkb </td>
                    <td align="right"><?php echo money($total_pbbkb); ?></td>
                  </tr>
                  <?php 
                  }
                  ?>
                  <tr>
                    <td colspan="2" align="right"><strong>TOTAL</strong></td>
                    <td align="right"><strong><?php echo money($total); ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="right">Term of Payment</td>
                    <td align="right"><?php echo $header->top; ?> Days</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <div class="col-md-4">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail2">
                <thead>
                  <tr>
                    <td colspan="3" align="left"><input id="supplier_pilihan" type="radio" name="supplier_pilihan" value="<?php echo $header->supplier_id2; ?>">&nbsp;&nbsp;&nbsp;<?php echo $header->supplier_name2; ?></td>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $subtotal2   = 0;
                  $ppn2       = 0;
                  $tsubtotal2  = 0;
                  foreach ($detail2 as $key => $value) {
                  $subtotal2 = $value->qty*$value->items_price;
                  ?>
                  <tr>
                    <td><?php echo $value->items_name; ?></td>
                    <td align="right"><?php echo money($value->qty); ?></td>
                    <td align="right"><?php echo money($value->items_price); ?></td>
                  </tr>
                  <?php 
                  $tsubtotal2    = $subtotal2+$tsubtotal2;
                  }

                  if($header->ppn_percent2!=0){
                    $ppn2        = ($tsubtotal2*$header->ppn_percent2)/100;
                  }

                  if($header->pph=='1'){
                    $total_pph = round((0.3*$tsubtotal2)/100);
                  }else{
                    $total_pph = 0;
                  }

                  if($header->pbbkb=='1'){
                    $total_pbbkb = round((((17.17*$tsubtotal2)/100)*5)/100);
                  }else{
                    $total_pbbkb = 0;
                  }
                  $total  = $tsubtotal2+$ppn2+$total_pph+$total_pbbkb+$header->transport_fee2;
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2" align="right">Subtotal</td>
                    <td align="right"><?php echo money($tsubtotal2); ?></td>
                  </tr>
                  <?php
                  if($header->ppn_percent2!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">PPN ( Subtotal * <?php echo money($header->ppn_percent2); ?>% )</td>
                    <td align="right"><?php echo money($ppn2); ?></td>
                  </tr>
                  <?php 
                  }
                  if($header->transport_fee2!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">Transport Fee</td>
                    <td align="right"><?php echo money($header->transport_fee2); ?></td>
                  </tr>
                  <?php
                  }
                  if($header->pph!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">PPH</td>
                    <td align="right"><?php echo money($total_pph); ?></td>
                  </tr>
                  <?php 
                  }
                  if($header->pph!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">Pbbkb </td>
                    <td align="right"><?php echo money($total_pbbkb); ?></td>
                  </tr>
                  <?php 
                  }
                  ?>
                  <tr>
                    <td colspan="2" align="right"><strong>TOTAL</strong></td>
                    <td align="right"><strong><?php echo money($total); ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="right">Term of Payment</td>
                    <td align="right"><?php echo $header->top2; ?> Days</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <div class="col-md-4">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail3">
                <thead>
                  <tr>
                    <td colspan="3" align="left"><input id="supplier_pilihan" type="radio" name="supplier_pilihan" value="<?php echo $header->supplier_id3; ?>">&nbsp;&nbsp;&nbsp;<?php echo $header->supplier_name3; ?></td>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th width="13%">Price</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $subtotal3   = 0;
                  $ppn3       = 0;
                  $tsubtotal3  = 0;
                  foreach ($detail3 as $key => $value) {
                  $subtotal3 = $value->qty*$value->items_price;
                  ?>
                  <tr>
                    <td><?php echo $value->items_name; ?></td>
                    <td align="right"><?php echo money($value->qty); ?></td>
                    <td align="right"><?php echo money($value->items_price); ?></td>
                  </tr>
                  <?php 
                  $tsubtotal3    = $subtotal3+$tsubtotal3;
                  }

                  if($header->ppn_percent2!=0){
                    $ppn3        = ($tsubtotal3*$header->ppn_percent3)/100;
                  }

                  if($header->pph=='1'){
                    $total_pph = round((0.3*$tsubtotal3)/100);
                  }else{
                    $total_pph = 0;
                  }

                  if($header->pbbkb=='1'){
                    $total_pbbkb = round((((17.17*$tsubtotal3)/100)*5)/100);
                  }else{
                    $total_pbbkb = 0;
                  }
                  $total  = $tsubtotal3+$ppn3+$total_pph+$total_pbbkb+$header->transport_fee3;
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2" align="right">Subtotal</td>
                    <td align="right"><?php echo money($tsubtotal3); ?></td>
                  </tr>
                  <?php
                  if($header->ppn_percent2!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">PPN ( Subtotal * <?php echo money($header->ppn_percent2); ?>% )</td>
                    <td align="right"><?php echo money($ppn3); ?></td>
                  </tr>
                  <?php 
                  }
                  if($header->transport_fee3!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">Transport Fee</td>
                    <td align="right"><?php echo money($header->transport_fee3); ?></td>
                  </tr>
                  <?php
                  }
                  if($header->pph!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">PPH</td>
                    <td align="right"><?php echo money($total_pph); ?></td>
                  </tr>
                  <?php 
                  }
                  if($header->pph!=0){
                  ?>
                  <tr>
                    <td colspan="2" align="right">Pbbkb </td>
                    <td align="right"><?php echo money($total_pbbkb); ?></td>
                  </tr>
                  <?php 
                  }
                  ?>
                  <tr>
                    <td colspan="2" align="right"><strong>TOTAL</strong></td>
                    <td align="right"><strong><?php echo money($total); ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="2" align="right">Term of Payment</td>
                    <td align="right"><?php echo $header->top3; ?> Days</td>
                  </tr>
                </tfoot>                
              </table>
            </div>
          </div>
        </div>
      <!-- </div> -->

      <div class="tile-footer">
        <div class="row">
          <div class="col-md-8 col-md-offset-3">
            <button class="btn btn-primary" type="button" id="approve" data-company_id="<?php echo $header->company_id; ?>" data-date="<?php echo $header->pr_date; ?>" data-id="<?php echo $header->pr_id; ?>" data-no="<?php echo $header->pr_no; ?>"><i class="fa fa-check"></i> Approve</button>
            <button class="btn btn-danger" type="button" id="unapprove" data-no="<?php echo $header->pr_no; ?>" data-id="<?php echo $header->pr_id; ?>"><i class="fa fa-times"></i> Not Approve</button>
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>transaction/purchase_requisition/reset_approve_it"><i class="fa fa-reply"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php //test($new_pr,0); ?>

<script type="text/javascript">
pr = {
  data: {},
  processed: false,
  items: [],
  init: function(){
    $('#approve').click(function(){
      dataID  = $(this).data('id');
      dataNO  = $(this).data('no');
      dataDATE= $(this).data('date');
      dataCOMPANY = $(this).data('company_id');
      dataAPP = 1;
      //alert('test dong '+data_id+' '+data_app);
      pr.action_pr();
    });

    $('#unapprove').click(function(){
      dataID  = $(this).data('id');
      dataNO  = $(this).data('no');
      dataDATE= '';
      dataCOMPANY = '';
      dataAPP = 0;
      //alert('test dong '+data_id+' '+data_app);
      pr.action_pr();
    });

    this.grids = $('#detail').DataTable({
        "ordering": false,
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        
    });

    this.grids2 = $('#detail2').DataTable({
        "ordering": false,
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        "language": {
            "emptyTable": "Tidak Ada Data"
        },
        columns: [
          { data: 'item_name'}, 
          { data: 'item_qty'}, 
          { data: 'item_price'}, 
          // { data: 'item_exchange'}, 
          // { data: 'item_info'}
        ],
    });

    this.grids3 = $('#detail3').DataTable({
        "ordering": false,
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        "language": {
            "emptyTable": "Tidak Ada Data"
        },
        columns: [
          { data: 'item_name'}, 
          { data: 'item_qty'}, 
          { data: 'item_price'}, 
          // { data: 'item_exchange'}, 
          // { data: 'item_info'}
        ],
    });

    this._set_items($('#sup_items').val());
    this._set_items2($('#sup_items2').val());
    this._set_items3($('#sup_items3').val());

  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    // data.act = '<button item-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> already in the list",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }
  },

  _addtogrid2: function(data){
    // debugger
    let grids2 = this.grids2;
    let exist = pr.grids2.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    // data.act = '<button item-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids2.row.add(data).draw();
    }else{ 
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> already in the list",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }
  },

  _addtogrid3: function(data){
    // debugger
    let grids3 = this.grids3;
    let exist = pr.grids3.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    // data.act = '<button item-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids3.row.add(data).draw();
    }else{ 
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> already in the list",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_price: i.item_price,
        item_exchange: i.item_exchange,
        item_info : i.item_info
      };
      pr._addtogrid(data);
    });
    this.no_ajax = false;

  },

  _set_items2: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_price: i.item_price,
        item_exchange: i.item_exchange,
        item_info : i.item_info
      };
      pr._addtogrid2(data);
    });
    this.no_ajax = false;

  },

  _set_items3: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_price: i.item_price,
        item_exchange: i.item_exchange,
        item_info : i.item_info
      };
      pr._addtogrid3(data);
    });
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_price').val('');
    $('#item_info').val('');
  },

  action_pr: function(e){

    if(dataAPP==1){
      var message_info = 'Data di Approve';
      var type_box = 'info';
      var type_dialog =  BootstrapDialog.TYPE_SUCCESS;
      var btn_simpan = 'btn-primary';
      var note = '';
      var message_input = 'Do you want to approve ?';
      var button_simpan = 'Approve'

      if(!$('#supplier_pilihan:checked').val()){
        $.notify({
          title: "Erorr : ",
          message: "<strong> Select Supplier First </strong>",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        return false;
      }

      var suppPilihan = $('#supplier_pilihan:checked').val();

    }else{
      var message_info = 'Data Not Approve';
      var type_box = 'danger';
      var type_dialog = BootstrapDialog.TYPE_DANGER;
      var btn_simpan = 'btn-danger'
      var suppPilihan = 0;
      var message_input = $('<textarea id="note" class="form-control" placeholder="Note ... "></textarea>');
      var button_simpan = 'Not Approve'
    }

    BootstrapDialog.show({
      title: 'NOTE '+dataNO,
      type : type_dialog,
      onshown: function(dialog) {
        $('#note').focus();
      },
      message: message_input,
      closable: false,
      buttons: [
        {
          label: 'Batal', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: button_simpan, cssClass: btn_simpan, id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            
            var supplier_pilihan = suppPilihan;
            var keterangan = $('#note').val();
            var data_id    = dataID;
            var data_no    = dataNO;
            var data_app   = dataAPP;   
            var data_date  = dataDATE;      
            var data_company=dataCOMPANY;

            if(data_app==0){
              if(!$('#note').val()){
                $.notify({
                  title: "Erorr : ",
                  message: "<strong>Note</strong> Can't Be Empty",
                  icon: 'fa fa-times' 
                },{
                  type: "danger",
                  delay: 1000
                });
                return false;
              }
            }


            $.ajax({
                data: {
                    supplier_pilihan : supplier_pilihan,
                    keterangan : keterangan,
                    data_id    : data_id,
                    data_no    : data_no,
                    data_app   : data_app,
                    data_date  : data_date,
                    data_company:data_company
                },

                type : "POST",
                url: baseUrl+'transaction/purchase_requisition/form_approve_act',
                success : function(resp){

                  if(resp.kd_trans == 'ERROR INSERT' || resp.kd_trans == false) {
                    alert("Store tujuan Can't Be Empty");
                    return false;

                  } else {
                    $.notify({
                      icon: "glyphicon glyphicon-save",
                      message: message_info
                    },{
                      type: type_box,
                      z_index : 1100,
                      onClosed: function(){ location.reload();}
                    });

                    setTimeout(function () {
                      window.location.href = baseUrl+'transaction/purchase_requisition/view_approve_it'; //will redirect to google.
                    }, 2000);
                  }
                }
            });

          }
        }
      ],
    });

  }
};

pr.init();
</script>