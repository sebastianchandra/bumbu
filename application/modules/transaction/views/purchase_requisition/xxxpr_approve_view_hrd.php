<?php if (isset($_SESSION['alert'])): ?>
  <script type="text/javascript">
    Command: toastr["info"]("<?php echo $_SESSION['alert']; ?>", "Alert",{
      "closeButton": true,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "5000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "5000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }).css("width","750px");
  </script>
<?php endif; ?>


<div class="app-title">
	<div>
		<h1>Approve Purchase Approve HRD</h1>
		<p><ul class="app-breadcrumb breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Transaction</li>
        <li class="breadcrumb-item active">Purchase Approve HRD</li>
	     </ul>
    </p>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="table-responsive">
	            <table class="table table-hover table-bordered" id="prTable">
	                <thead>
						<tr>
							<th>No</th>
              <th>Date</th>
							<th>Project Name</th>
							<th>Company Name</th>
							<th>Supplier Name</th>
              <th>requester</th>
              <th>Department</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
	                </thead>
	                <tbody>
	                	<?php 
	                	$no = 0;
                		foreach ($data_pr as $key => $value) {
                			$no = $no+1;
	                	?>
						<tr>
							<td><strong><a href="#" id='detail' onclick="return show_detail(this)" 
              data-id="<?php echo $value->pr_id; ?>" 
              data-pr="<?php echo $value->pr_no; ?>"><?php echo $value->pr_no; ?></a></strong></td>
              <td><?php echo tgl_singkat($value->pr_date); ?></td>
							<td><?php echo $value->project_name; ?></td>
							<td><?php echo $value->company_name; ?></td>
							<td><?php echo $value->supplier_name; ?></td>
							<td><?php echo $value->requester; ?></td>
              <td><?php echo $value->nama_dept; ?></td>
              <td><?php echo $value->pr_status; ?></td>
							<td align="center">
              <?php 
              if($value->pr_status=='Process' AND $value->jml_approve>=1){
                echo '<button class="btn btn-info btn-sm" type="button" disabled=""><i class="fa fa-check-square-o"></i> Status </button>';
              }else{
              ?>
								<a class="btn btn-info btn-sm" href="<?php echo base_url()."transaction/purchase_requisition/form_approve_hrd/$value->pr_id"; ?>"><i class="fa fa-check-square-o"></i> Status </a>
              <?php 
              }
              ?></td>
						</tr>
						<?php 
						}
						?>
	                </tbody>
	            </table>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="void_dialog">
  <form id="form_void_dialog">
    <div class="form-group">
      <label for="void-type">Jenis Pembatalan</label>
      <select id="void-type" name="void-type" class="form-control">
        <option value="">Pilih Status</option>
      </select>
    </div>
  </form>
</script>

<script type="text/javascript">
$('#prTable').DataTable({
  	"paging": true, 
  	"bLengthChange": false, // disable show entries dan page
  	"bFilter": true,
  	"bInfo": true, // disable Showing 0 to 0 of 0 entries
  	"bAutoWidth": false,
  	"language": {
      	"emptyTable": "Tidak Ada Data"
    },
  	"aaSorting": [],
  	responsive: true
});

function show_detail(e){
  let noId      = $(e).data('id');
  let noPr      = $(e).data('pr');

  $.get({
    url: baseUrl + 'transaction/purchase_requisition/view_popup/'+noId,
    success: function(resp){
      BootstrapDialog.show({
        title: 'Nomor Purchase Requisition # <strong> '+noPr+' </strong>', 
        nl2br: false, 
        message: resp,
        closable: true,
        size: 'size-full',
        buttons:[
          {
            label: 'Tutup',
            action: function(dia){ dia.close(); }
          }
        ]
      });
    },
    complete: function(){
      $('body').css('cursor','default');
    }
  });
  return false;
  
};

$('#prTable').on('click','.element', function (e) {
	var supplier_id 			= $(this).data('supplier_id');
	var name 	= $(this).data('name');

	BootstrapDialog.show({
      title: 'Delete Items ',
      type : BootstrapDialog.TYPE_DANGER,
      message: 'Apakah anda ingin menghapus data '+name+' ?',
      closable: false,
      buttons: [
        {
          label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: '<i class="fa fa-close"></i> Delete', cssClass: 'btn-danger', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            $.ajax({
                data: {
                    supplier_id : supplier_id
                },
                type : "POST",
                url: baseUrl+'master/supplier/delete_js',
                success : function(resp){

                  if(resp.status == 'ERROR INSERT' || resp.status == false) {
                    alert('Data Tidak berhasil di Hapus');
                    return false;

                  } else {
                    $.notify({
                          icon: "glyphicon glyphicon-save",
                          message: 'Data berhasil dihapus'
                        },{
                          type: 'success',
                          onClosed: function(){ location.reload();}
                        });

                    setTimeout(function () {
                      window.location.href = baseUrl+'master/supplier'; //will redirect to google.
                    }, 2000);
                  }
                }
            });

          }
        }
      ],
    });
});

function confirmDelete() {
	return confirm("Anda yakin untuk Menghapus data ini ?");
}
</script>