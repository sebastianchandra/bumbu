<div class="form-group row">
  <label class="control-label col-md-2">Supplier Name</label>
  <div class="col-md-8">
    <input class="form-control col-md-8" type="text" placeholder="Name Items" id="supplier_name" value="<?php echo $data_supplier->supplier_name; ?>" disabled>
    <input class="form-control col-md-8" type="hidden" placeholder="Name Items" id="supplier_id" value="<?php echo $data_supplier->supplier_id; ?>">
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Group Code</label>
  <div class="col-md-2">
    <select class="form-control" id='items_kind'>
      <option value=""> - </option>
      <?php 
      foreach ($data_items_kind as $key => $value) {
        echo '<option data-kind="'.$value->items_kind.'" value="'.$value->items_kind_name.'">'.$value->items_kind_name.'</option>';
      }
      ?>
    </select>
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Items Name</label>
  <div class="col-md-8">
    <input class="form-control col-md-8" type="text" placeholder="Name Items" id="name">
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Unit</label>
  <div class="col-md-2">
    <select class="form-control" id='unit'>
      <option value=""> - </option>
      <?php 
      foreach ($data_items_unit as $key => $value) {
        echo '<option value="'.$value->items_unit_id.'">'.$value->items_unit_name.'</option>';
      }
      ?>
    </select>
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Group Items</label>
  <div class="col-md-2">
    <select class="form-control" id='items_group'>
      <option value=""> - </option>
      <option value="consumable"> Consumable </option>
      <option value="inventory"> Inventory </option>
    </select>
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Category Items</label>
  <div class="col-md-8">
    <input class="form-control col-md-5" type="text" placeholder="Category Items" id="category">
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Info</label>
  <div class="col-md-8">
    <input class="form-control col-md-8" type="text" placeholder="Info Items" id="info_items">
  </div>
</div>
<div class="form-group row">
  <label class="control-label col-md-2">Price</label>
  <div class="col-md-8">
    <input class="form-control col-md-3" type="text" placeholder="Price" id="price_items">
  </div>
</div>