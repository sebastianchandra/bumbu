<?php 
// test($header,0);

if($header->preferred_supplier==$header->supplier_id){
  $transport    = $header->transport_fee;
  $ppn_percent  = $header->ppn_percent;
  $top          = $header->top;
}elseif($header->preferred_supplier==$header->supplier_id2){
  $transport    = $header->transport_fee2;
  $ppn_percent  = $header->ppn_percent2;
  $top          = $header->top2;
}elseif($header->preferred_supplier==$header->supplier_id3){
  $transport    = $header->transport_fee3;
  $ppn_percent  = $header->ppn_percent3;
  $top          = $header->top3;
}
// test($transport.' '.$ppn_percent,0);
?>
<div class="app-title">
  <div>
    <h1>Process Purchase Requisition Approve</h1>
    <p><ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Transaction</li>
        <li class="breadcrumb-item">Purchase Requisition Approve</li>
        <li class="breadcrumb-item active">Process</li>
  </ul></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="control-label col-md-2">Company</label>
            <div class="col-md-4">
              <?php echo $header->company_name; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Date</label>
            <div class="col-md-8">
              <?php echo tanggal($header->pr_date); ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Purchase Requisition Number</label>
            <div class="col-md-4">
              <?php echo $header->pr_no; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Request Number</label>
            <div class="col-md-4">
              <?php echo $header->request_no; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Requester</label>
            <div class="col-md-4">
              <?php echo $header->requester; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Supplier</label>
            <div class="col-md-4">
              <?php echo $header->sup_choice; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Quotation</label>
            <div class="col-md-4">
              <?php echo ($header->file_quotation==0)? 'No File' : '<a target="_blank" href="'.base_url().'file_upload/quotation/'.$header->pr_no.'.pdf">'.$header->pr_no.'</a>'; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Project Name</label>
            <div class="col-md-4">
              <?php echo $header->project_name; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Delivery Address</label>
            <div class="col-md-5">
              <?php echo $header->delivery_address; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Info</label>
            <div class="col-md-8">
              <?php echo $header->pr_info; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Currency</label>
            <div class="col-md-2">
              <?php 
              foreach ($data_currency as $key => $value) {
                if($value['id']==$header->currency_id){
                  echo $value['nama'];
                }
              }
              ?>
            </div>
          </div>
          <?php 
          if($header->pph==1){
          ?>
          <div class="form-group row">
            <label class="control-label col-md-2">PPH</label>
            <div class="col-md-8">
              <div class="animated-checkbox">
                <?php echo ($header->pph==1)? '<i class="fa fa-check"></i>' : '' ?>
              </div>
            </div>
          </div>
          <?php 
          }
          if($header->pbbkb==1){
          ?>
          <div class="form-group row">
            <label class="control-label col-md-2">PBBKB</label>
            <div class="col-md-8">
              <div class="animated-checkbox">
                <?php echo ($header->pbbkb==1)? '<i class="fa fa-check"></i>' : '' ?>
              </div>
            </div>
          </div>
          <?php 
          } 
          ?>
          <div class="form-group row">
            <label class="control-label col-md-2">Transport Fee</label>
            <div class="col-md-8">
              <input class="form-control col-md-4" type="text" id="fee" placeholder="Transport Fee" disabled="" value="<?php echo $transport; ?>">
              <input class="form-control col-md-4" type="hidden" id="pr_id" placeholder="Transport Fee" value="<?php echo $header->pr_id ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">PPN</label>
            <div class="col-md-8">
              <input class="form-control col-md-4" type="text" placeholder="Percent" id="ppn" disabled="" value="<?php echo $ppn_percent; ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Term of Payment</label>
            <div class="col-md-8">
              <input class="form-control col-md-4" type="text" placeholder="Percent" id="top" disabled="" value="<?php echo $top; ?>">
            </div>
          </div>
        </form>
      </div>
      <div class="tile-footer">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="detail">
              <thead>
                <tr>
                  <th width="1%">ID</th>
                  <th>Name</th>
                  <th width="8%">Qty</th>
                  <th width="13%">Price</th>
                  <th width="13%">Exchange Rate</th>
                  <th>Info</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($detail as $key => $value) {
                ?>
                <tr>
                  <td><?php echo $value->pr_02_id; ?></td>
                  <td><?php echo $value->items_name; ?></td>
                  <td align="right"><?php echo money($value->qty); ?></td>
                  <td align="right"><?php echo money($value->items_price); ?></td>
                  <td align="right"><?php echo money($value->exchange_rate); ?></td>
                  <td><?php echo $value->remarks; ?></td>
                </tr>
                <?php 
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tile-footer">
        <div class="row">
          <div class="col-md-8 col-md-offset-3">
            <button class="btn btn-primary" type="button" id="save"><i class="fa fa-floppy-o"></i> Save</button>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>transaction/purchase_requisition/reset"><i class="fa fa-reply"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php //test($new_pr,0); ?>

<script type="text/javascript">

pr = {
  data: {},
  processed: false,
  items: [],
  init: function(){

    $("#fee").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'});
    $("#ppn").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 1, 'digitsOptional': false, 'placeholder': '0','suffix': " %"});

    this.grids = $('#detail').DataTable({
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        "language": {
            "emptyTable": "Tidak Ada Data"
        },
        "columnDefs": [
          { // disable sorting on column process buttons
            "targets": [1,2,3,4,5],
            "orderable": false,
          },
          { 
            "targets": [0],
            "visible": false,
            "searchable": false
          }
        ],
    });

    // this._set_items($('#sup_items').val());
    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  save: function(e){
    e.preventDefault();
    
    $.ajax({
      url: baseUrl+'transaction/purchase_requisition/process_po',
      type : "POST",  
      data: {
        fee       : $('#fee').val(),
        ppn       : $('#ppn').val(),
        pr_id     : $('#pr_id').val(),
        top       : $('#top').val()
      },
      success : function(resp){
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          $.notify({
            message: 'Data Gagal disimpan'
          },{
            type: 'danger'
          });
          return false;

        } else {

          $.notify({
            message: 'Data successfully saved with document number '+resp.no_po
          },{
            type: 'info'
          });

          setTimeout(function () {
            window.location.href = baseUrl+'transaction/purchase_requisition/'; 
          }, 2000);
        }
      }
    });

  }
};

pr.init();

</script>