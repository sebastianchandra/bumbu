<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">Permintaan Pembelian</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Permintaan Pembelian</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>
                <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_pr["items"]); ?>' required />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="pr_date" value="<?php echo substr(dbnow(),0,10); ?>" >
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Requester</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="requester">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Barang</p>
              </div>
              <div class="col-md-7">
                <select class="form-control" id="item_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_items as $key => $value) {
                    echo "<option value='".$value->items_id."' data-name='".$value->items_name."'>".$value->items_name."</option>";
                  }
                  ?>
                </select>
                <input type="hidden" class="form-control " id="id" name="id" value="0"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Quantity</p>
              </div>
              <div class="col-md-2">
                <input type="text" class="form-control price" id="item_qty">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p><!-- Info --></p>
              </div>
              <div class="col-md-5">
                <input type="hidden" class="form-control" id="item_info">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-5">
                <button type="button" class="btn btn-primary btn-sm" id="add-items">Tambah Barang</button>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th>Nama Barang</th>
                        <th width="8%">Qty</th>
                        <th>Info</th>
                        <th width="5%">Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('transaction/purchase_requisition'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$("#requester").focus();

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#supplier_id").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'item_id'},
        { data: 'item_name'}, 
        { data: 'item_qty', className: "text-right"}, 
        { data: 'item_info',"visible": false}, 
        { data: 'act', className: "text-center" }
      ],
    });

    this._set_items($('#sup_items').val());
    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  add_items: function(e){
    e.preventDefault();

    if($('#item_id').val()==''){
      toastr.error("<strong>Nama Barang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#item_id').select2('open');
      return false;
    }

    if(!$('#item_qty').val()){
      toastr.error("<strong>Qty</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#item_qty").focus();
      return false;
    } 

    let item_id   = $('#item_id').val();
    let item_name = $('#item_id option:selected').attr('data-name');
    let item_qty  = $('#item_qty').val();
    let item_info = $('#item_info').val();
    let id        = parseInt($('#id').val());
    var id_det    = id + 1;


      if(item_id){
      data = {
        det_id : id_det,
        item_id : item_id,
        item_name : item_name,
        item_qty : item_qty,
        item_info : item_info
      };

      pr._addtogrid(data);
      pr._clearitem();
      pr._focus();

    }
  },

  _focus: function(){
    $('#item_id').select2('open');
  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    data.act = '<button det-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw(); 
    }else{ 
      toastr.error("<strong>Barang</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'transaction/purchase_requisition/add_item',
      data: {
        det_id    : data.det_id,
        item_id   : data.item_id,
        item_name : data.item_name,
        item_qty  : data.item_qty.replace(/\,/g, ''),
        item_info : data.item_info
      }
    });
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        det_id    : i.det_id,
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_info : i.item_info
      };
      pr._addtogrid(data);
      pr._clearitem();
    });
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_info').val('');
  },

  _removefromgrid: function(el){
    let id = $(el).attr('det-id');
    pr.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'transaction/purchase_requisition/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },

  save: function(e){
    e.preventDefault();

    if($('#detail').DataTable().data().count()<1){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Detail Barang</strong> Tidak Boleh Kosong",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }

    if(!$('#requester').val()){
      toastr.error("<strong>Requester</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#requester").focus();
      return false;
    }

    // if(!$('#project_id').val()){
    //   toastr.error("<strong>Project Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    //   $('#project_id').select2('open');
    //   return false;
    // }

    // if(!$('#supplier_id').val()){
    //   toastr.error("<strong>Supplier Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    //   $('#supplier_id').select2('open');
    //   return false;
    // }
    
    $('#save').prop("disabled",true);
    
    $.ajax({
      url: baseUrl+'transaction/purchase_requisition/form_act',
      type : "POST",  
      data: {
        company_name      : $('#company_id').val(),
        company_id        : $('#company_id').attr('data-company_id'),
        pr_date           : $('#pr_date').val(),
        requester         : $('#requester').val(),
        project_id        : $('#project_id').val(),
        project_name      : $('#project_id option:selected').attr('data-project_name'),
        supplier_id       : $('#supplier_id').val(),
        supplier_name     : $('#supplier_id option:selected').attr('data-supplier_name'),
        delivery_address  : $('#delivery_address').val(),
        remarks           : $('#remarks').val()

      },
      success : function(resp){
        // debugger
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else {
          messages = 'Data Berhasil di Simpan dengan Nomor Dokumen '+resp.status;
          messages += "<hr>";

          toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
          
          setTimeout(function () {
            window.location.href = baseUrl+'transaction/purchase_requisition/'; 
          }, 2000);
        }
      }
    });

  }

};

pr.init();

</script>