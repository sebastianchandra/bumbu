<div class="app-title">
	<div>
		<h1>View Request</h1>
		<p><ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Transaction</li>
        <li class="breadcrumb-item">Request</li>
        <li class="breadcrumb-item active">View</li>
	</ul></p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item">
      <?php 
      if($this->current_user['company_id']=='1'){
        echo '<a class="btn btn-primary" href="'.base_url().'transaction/request/form_lm"><i class="fa fa-plus"></i> Input</a>';
      }else{
        echo '<a class="btn btn-primary" href="'.base_url().'transaction/request/form"><i class="fa fa-plus"></i> Input</a>';
      }
      ?>
      
      
    </li>
	</ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="table-responsive">
	            <table class="table table-hover table-bordered" id="prTable">
                <thead>
      						<tr>
      							<th>No</th>
                    <th>Date</th>
      							<th>Company Name</th>
      							<th>Department</th>
                    <th>Requester</th>
                    <th>Status</th>
      							<th>Actionn</th>
      						</tr>
                </thead>
                <tbody>
                	<?php 
                	$no = 0;
                  // test($data_pr,0);
              		foreach ($data_pr as $key => $value) {
              			$no = $no+1;
                	?>
      						<tr>
      							<td><strong><a href="#" id='detail' onclick="return show_detail(this)" 
                    data-id="<?php echo $value->request_id; ?>" 
                    data-rq="<?php echo $value->request_no; ?>"><?php echo $value->request_no; ?></a> <?php echo ($value->project_name!='Project Head Office')? '( '.$value->project_name.' )' : ''; ?></strong> 
                    </td>
                    <td><?php echo tgl_singkat($value->request_date); ?></td>
      							<td><?php echo $value->company_name; ?></td>
                    <td><?php echo $value->name_dept; ?></td>
      							<td><?php echo $value->requester; ?></td>
                    <td><?php if($value->status_approve==0){ echo 'New';}elseif($value->status_approve==1){ echo 'Approved';}elseif($value->status_approve==2){ echo 'Not Approved';} ?></td>
      							<td align="center">
                      <?php 
                      if($value->status_approve==0){
                        if($this->current_user['company_id']=='1'){
                          ?>
                          <a data-placement="left" title="Edit Request" class="btn btn-info btn-sm" href="<?php echo base_url()."transaction/request/edit_lm/$value->request_id"; ?>"><i class="fa fa-files-o"></i></a>
                      <?php
                        }else{
                          ?>
                          <a data-placement="left" title="Edit Request" class="btn btn-info btn-sm" href="<?php echo base_url()."transaction/request/edit/$value->request_id"; ?>"><i class="fa fa-files-o"></i></a>
                          <?php
                        }
                      ?>
      
                      
                      <?php 
                      }
                      if($value->status_approve==1){
                      ?>
                      <a target="_blank" data-placement="right" title="Print Request" class="btn btn-success btn-sm" href="<?php echo base_url()."transaction/request/cetak/$value->request_id"; ?>"><i class="fa fa-print"></i></a>
                      <?php 
                      }
                      ?>
                      </td>
      						</tr>
      						<?php 
      						}
      						?>
                </tbody>
	            </table>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="void_dialog">
  <form id="form_void_dialog">
    <div class="form-group">
      <label for="void-type">Jenis Pembatalan</label>
      <select id="void-type" name="void-type" class="form-control">
        <option value="">Pilih Status</option>
      </select>
    </div>
  </form>
</script>

<script type="text/javascript">
$('#prTable').DataTable({
  	"paging": true, 
  	"bLengthChange": false, // disable show entries dan page
  	"bFilter": true,
  	"bInfo": true, // disable Showing 0 to 0 of 0 entries
  	"bAutoWidth": false,
  	"language": {
      	"emptyTable": "Tidak Ada Data"
    },
  	"aaSorting": [],
  	'dom': 'Bfrtip',
        buttons: [
            'print'
        ]
});

$('#prTable').on('click','#process', function (e) {
  var idPr     = $(this).data('id');
  var noPr     = $(this).data('pr');

  BootstrapDialog.show({
      title: 'Process Purchase Requisition ',
      type : BootstrapDialog.TYPE_INFO,
      message: 'Do you want process purchase requisition number <strong>'+noPr+'</storng> ?',
      closable: false,
      buttons: [
        {
          label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: '<i class="fa fa-check"></i> Process', cssClass: 'btn-info', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            $.ajax({
                data: {
                    id_pr : idPr,
                    no_pr : noPr
                },
                type : "POST",
                url: baseUrl+'transaction/purchase_requisition/process_po',
                success : function(resp){

                  if(resp.no_po == 'ERROR INSERT' || resp.no_po == false) {
                    alert('Data Tidak berhasil di Proses');
                    return false;

                  } else {
                    $.notify({
                          icon: "glyphicon glyphicon-save",
                          message: 'Data successfully saved with document number : '+resp.no_po
                        },{
                          type: 'success',
                          onClosed: function(){ location.reload();}
                        });

                    setTimeout(function () {
                      window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
                    }, 2000);
                  }
                }
            });

          }
        }
      ],
    });
});

function show_detail(e){
  let noId      = $(e).data('id');
  let noRq      = $(e).data('rq');

  $.get({
    url: baseUrl + 'transaction/request/view_popup/'+noId,
    success: function(resp){
      BootstrapDialog.show({
        title: 'Nomor Request # <strong> '+noRq+' </strong>', 
        nl2br: false, 
        message: resp,
        closable: true,
        size: 'size-full',
        buttons:[
          {
            label: 'Tutup',
            action: function(dia){ dia.close(); }
          }
        ]
      });
    },
    complete: function(){
      $('body').css('cursor','default');
    }
  });
  return false;
  
};
</script>