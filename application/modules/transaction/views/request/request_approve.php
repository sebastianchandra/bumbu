<div class="app-title">
	<div>
		<h1>Approve Request</h1>
		<p><ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Transaction</li>
        <li class="breadcrumb-item">Request</li>
        <li class="breadcrumb-item active">Approve</li>
	</ul></p>
	</div>
	<!-- <ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><a class="btn btn-primary" href="<?php echo base_url(); ?>transaction/request/form"><i class="fa fa-plus"></i> Input</a></li>
	</ul> -->
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="table-responsive">
	            <table class="table table-hover table-bordered" id="prTable">
                <thead>
      						<tr>
      							<th width="10%">No</th>
                    <th width="8%">Date</th>
      							<th>Company Name</th>
      							<th width="8%">Department</th>
                    <th>Requester</th>
                    <th>Info</th>
      							<th>Actionn</th>
      						</tr>
                </thead>
                <tbody>
                	<?php 
                	$no = 0;
              		foreach ($data_pr as $key => $value) {
              			$no = $no+1;
                	?>
      						<tr>
      							<td><strong><a href="#" id='detail' onclick="return show_detail(this)" 
                    data-id="<?php echo $value->request_id; ?>" 
                    data-rq="<?php echo $value->request_no; ?>"><?php echo $value->request_no; ?></a></strong></td>
                    <td><?php echo tgl_singkat($value->request_date); ?></td>
      							<td><?php echo $value->company_name; ?></td>
                    <td><?php echo $value->name_dept; ?></td>
      							<td><?php echo $value->requester; ?></td>
                    <td><?php echo $value->remark; ?></td>
      							<td align="center">
                      <button type="button" id='approve' class="element btn btn-info btn-sm" data-toggle="modal" data-request_no="<?php echo $value->request_no; ?>" data-request_id="<?php echo $value->request_id; ?>" data-company_id="<?php echo $value->company_id; ?>" data-request_date="<?php echo $value->request_date; ?>" <?php //echo ($value->is_accept==1)? 'disabled' : ''; ?>>Status</button>
                    </td>
      						</tr>
      						<?php 
      						}
      						?>
                </tbody>
	            </table>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="void_dialog">
  <form id="form_void_dialog">
    <div class="form-group">
      <label for="void-type">Jenis Pembatalan</label>
      <select id="void-type" name="void-type" class="form-control">
        <option value="">Pilih Status</option>
      </select>
    </div>
  </form>
</script>

<script type="text/javascript">
$('#prTable').DataTable({
  	"paging": true, 
  	"bLengthChange": false, // disable show entries dan page
  	"bFilter": true,
  	"bInfo": true, // disable Showing 0 to 0 of 0 entries
  	"bAutoWidth": false,
  	"language": {
      	"emptyTable": "Tidak Ada Data"
    },
  	"aaSorting": [],
  	'dom': 'Bfrtip',
        buttons: [
            'print'
        ]
});

$('#prTable').on('click','#approve', function (e) {
  var idPr     = $(this).data('request_id');
  var noPr     = $(this).data('request_no');
  var idCompany= $(this).data('company_id');
  var datePr   = $(this).data('request_date');

  BootstrapDialog.show({
      title: 'Process Request ',
      type : BootstrapDialog.TYPE_INFO,
      //message: 'Do you want process Request number <strong>'+noPr+'</storng> ?',
      onshown: function(dialog) {
        $('#note').focus();
      },
      message: $('<textarea id="note" class="form-control" placeholder="Note ... "></textarea>'),
      closable: false,
      buttons: [
        {
          label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: '<i class="fa fa-close"></i> Not Approve', cssClass: 'btn-danger', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();

            if(!$('#note').val()){
              $.notify({
                title: "Erorr : ",
                message: "<strong>Note</strong> Can't Be Empty",
                icon: 'fa fa-times' 
              },{
                type: "danger",
                delay: 1000
              });
              return false;
            }

            var keterangan = $('#note').val();

            $.ajax({
              data: {
                keterangan: keterangan,
                id_pr     : idPr,
                no_pr     : noPr,
                date_pr   : datePr,
                id_company: idCompany,
                is_approve: 0
              },

              type : "POST",
              url: baseUrl+'transaction/request/process_request',
              success : function(resp){

                if(resp.no_po == 'ERROR INSERT' || resp.no_po == false) {
                  alert('Data Tidak berhasil di Proses');
                  return false;

                } else {
                  $.notify({
                    icon: "glyphicon glyphicon-save",
                    message: 'Data Not Approved '
                  },{
                    type: 'danger',
                    onClosed: function(){ location.reload();}
                  });

                  setTimeout(function () {
                    window.location.href = baseUrl+'transaction/request/view_approve'; //will redirect to google.
                  }, 2000);
                }
              }
            });

          }
        },
        {
          label: '<i class="fa fa-check"></i> Approve', cssClass: 'btn-info', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();

            if(!$('#note').val()){
              $.notify({
                title: "Erorr : ",
                message: "<strong>Note</strong> Can't Be Empty",
                icon: 'fa fa-times' 
              },{
                type: "danger",
                delay: 1000
              });
              return false;
            }

            var keterangan = $('#note').val();

            $.ajax({
              data: {
                keterangan: keterangan,
                id_pr     : idPr,
                no_pr     : noPr,
                date_pr   : datePr,
                id_company: idCompany,
                is_approve: 1
              },

              type : "POST",
              url: baseUrl+'transaction/request/process_request',
              success : function(resp){

                if(resp.no_po == 'ERROR INSERT' || resp.no_po == false) {
                  alert('Data Tidak berhasil di Proses');
                  return false;

                } else {
                  $.notify({
                    icon: "glyphicon glyphicon-save",
                    message: 'Data successfully Approve '
                  },{
                    type: 'success',
                    onClosed: function(){ location.reload();}
                  });

                  setTimeout(function () {
                    window.location.href = baseUrl+'transaction/request/view_approve'; //will redirect to google.
                  }, 2000);
                }
              }
            });

          }
        }
      ],
    });
});

function show_detail(e){
  // debugger
  let noId      = $(e).data('id');
  let noRq      = $(e).data('rq');

  $.get({
    url: baseUrl + 'transaction/request/view_popup/'+noId,
    success: function(resp){
      BootstrapDialog.show({
        title: 'Nomor Request # <strong> '+noRq+' </strong>', 
        nl2br: false, 
        message: resp,
        closable: true,
        size: 'size-full',
        buttons:[
          {
            label: 'Tutup',
            action: function(dia){ dia.close(); }
          }
        ]
      });
    },
    complete: function(){
      $('body').css('cursor','default');
    }
  });
  return false;
  
};
</script>