<section class="content-body" style="padding-top:0;">
    <div class="tile-body">
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">No. Request</label>
                    <div class="col-md-7">: <?php echo $header->request_no; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Date</label>
                    <div class="col-md-7">: <?php echo tanggal($header->request_date); ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Company</label>
                    <div class="col-md-7">: <?php echo $header->company_name; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Department</label>
                    <div class="col-md-7">: <?php echo $header->name_dept; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Requester</label>
                    <div class="col-md-7">: <?php echo $header->requester; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Info</label>
                    <div class="col-md-7">: <?php echo $header->remark; ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="center">
                    <th>Name</th>
                    <th width="8%">Qty</th>
                    <th>Info</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach ($detail as $key => $value) {
                ?>
                <tr>
                    <td class="popup"><?php echo $value->items_name; ?></td>
                    <td align="right" class="popup"><?php echo money($value->qty); ?></td>
                    <td class="popup"><?php echo $value->remarks; ?></td>
                </tr>
                <?php 
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="detail">
        <thead>
            <tr style="font-size: 11px;">
                <th><strong>Company Name</strong></th>
                <th><strong>Project</strong></th>
                <th><strong>PR No</strong></th>
                <th><strong>PR Date</strong></th>
                <th><strong>PR Status</strong></th>
                <th><strong>Supplier</strong></th>
                <th><strong>PO No</strong></th>
                <th><strong>PO Date</strong></th>
                <th><strong>PO Status</strong></th>
            </tr>
        </thead>
        <?php 
            foreach ($history as $key => $detail) {
            ?>
            <tr style="font-size: 10px;">
                <td><?php echo $detail->company_name; ?></td>
                <td><?php echo $detail->project_name; ?></td>
                <td><?php echo $detail->pr_no; ?></td>
                <td><?php echo ($detail->pr_date!='') ? tgl_singkat($detail->pr_date) : ''; ?></td>
                <td><?php if($detail->pr_notapprove>0){ echo 'Not Approved'; } elseif($detail->pr_approve>0){ echo 'Approved'; } else { echo $detail->pr_status; } ?></td>
                <td><?php echo $detail->supplier_name; ?></td>
                <td><?php echo $detail->po_no; ?></td>
                <td><?php echo ($detail->po_date!='') ? tgl_singkat($detail->po_date) : ''; ?></td>
                <td><?php echo $detail->po_status; ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>

</section>