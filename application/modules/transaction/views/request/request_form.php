<?php 
// test($this->current_user['user_permission_group'],0);
?>
<div class="app-title">
  <div>
    <h1>Input Request</h1>
    <p><ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Transaction</li>
        <li class="breadcrumb-item">Request</li>
        <li class="breadcrumb-item active">Input</li>
  </ul></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="control-label col-md-2">Company</label>
            <div class="col-md-5">
              <!-- <select class="form-control" id='company'>
                <option value=""> - </option>
                <?php 
                foreach ($data_company as $key => $value) {
                  echo '<option data-name="'.$value['name'].'" value="'.$value['code'].'">'.$value['name'].'</option>';
                }
                ?>
              </select> -->
              <?php echo $this->current_user['company_name']; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Department</label>
            <div class="col-md-5">
              <select class="form-control" id='id_user_group'>
                <?php 
                foreach ($this->current_user['user_permission_group'] as $key => $value) {
                  echo '<option value="'.$value->id_user_group.'" selected>'.$value->name.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">From Project</label>
            <div class="col-md-4">
              <select class="form-control" id='is_project'>
                <option value=""> - </option>
                <?php 
                foreach ($data_project as $key => $value) {
                  echo '<option data-location="'.$value->project_location.'" data-name="'.$value->project_name.'" value="'.$value->project_id.'">'.$value->project_name.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Date</label>
            <div class="col-md-8">
              <input class="form-control col-md-4" type="date" placeholder="Date" id="date" value="<?php echo substr(dbnow(),0,10); ?>" disabled>
              <!-- <input class="form-control col-md-4" type="date" placeholder="Date" id="date" value="<?php echo isset($new_req['name']) ? $new_req['name']:''; ?>"> -->
              <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_req["items"]); ?>' required />
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Requester</label>
            <div class="col-md-4">
              <input class="form-control" type="text" placeholder="Requester" id="requester" value="<?php echo isset($new_req['siup']) ? $new_req['siup']:''; ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Info Request</label>
            <div class="col-md-8">
              <input class="form-control col-md-8" type="text" placeholder="Info Purchase Requisition. Ex : Urgent" id="info" value="<?php echo isset($new_req['info']) ? $new_req['info']:''; ?>">
            </div>
          </div>
        </form>
      </div>
      <div class="tile-footer">
        <div class="form-group row">
          <label class="control-label col-md-2">Items Name</label>
          <div class="col-md-5">
            <select class="form-control" id='item_id'>
              <option value=""> - </option>
              <?php 
              foreach ($data_items as $key => $value) {
                echo '<option data-name="'.$value->items_nama.'" value="'.$value->items_id.'">'.$value->items_nama.'</option>';
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Qty</label>
          <div class="col-md-8">
            <input class="form-control col-md-4" type="text" placeholder="Qty" id="item_qty">
            <input type="hidden" class="form-control " id="id" name="id" value="0"/>
          </div>
        </div>
        <!-- <div class="form-group row">
          <label class="control-label col-md-2">Info Items</label>
          <div class="col-md-8"> -->
            <input class="form-control col-md-8" type="hidden" placeholder="Description of goods to be purchased. Ex : Khusus Merk ......" id="item_info">
          <!-- </div>
        </div> -->
        <div class="form-group row">
          <label class="control-label col-md-2"></label>
          <div class="col-md-2">
            <button class="btn btn-warning" type="button" id="add-items"><i class="fa fa-plus-square"></i> Add Items</button>
          </div>
          <!-- <label class="control-label col-md-6">Items Name</label>
          <div class="col-md-6">
            <input class="form-control col-md-5" type="text" placeholder="Name Supplier" id="name">
          </div> -->
        </div>
      </div>
      <div class="tile-footer">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="detail">
              <thead>
                <tr>
                  <th width="1%">ID</th>
                  <th>Name</th>
                  <th width="8%">Qty</th>
                  <th>Info</th>
                  <th width="5%">Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
      <div class="tile-footer">
        <div class="row">
          <div class="col-md-8 col-md-offset-3">
            <button class="btn btn-primary" type="button" id="save"><i class="fa fa-floppy-o"></i> Save</button>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>transaction/request/reset"><i class="fa fa-reply"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php //test($new_req,0); ?>

<script type="text/javascript">
pr = {
  data: {},
  processed: false,
  items: [],
  init: function(){
    $('#date_pr').datepicker({
      format: "dd/mm/yyyy",
      autoclose: true,
      todayHighlight: true
    });

    $("#item_qty").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 1, 'digitsOptional': false, 'placeholder': '0'});

    $("#item_id").select2().on('select2:select',function(e){});
    $("#is_project").select2().on('select2:select',function(e){});
    $("#id_user_group").select2().on('select2:select',function(e){});

    $("#company").select2().on('select2:select',function(e){});

    this.grids = $('#detail').DataTable({
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        "language": {
            "emptyTable": "Tidak Ada Data"
        },
        "columnDefs": [
          { // disable sorting on column process buttons
            "targets": [1,2,3,4],
            "orderable": false,
          },
          { 
            "targets": [0],
            "visible": false,
            "searchable": false
          }
        ],
        columns: [
          { data: 'item_id'},
          { data: 'item_name'}, 
          { data: 'item_qty', className: "text-right"}, 
          { data: 'item_info',"visible": false}, 
          { data: 'act', className: "text-center" }
        ],
    });

    this._set_items($('#sup_items').val());
    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  add_items: function(e){
    e.preventDefault();

    if($('#item_id').val()==0){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $('#item_id').select2('open');
      return false;
    }

    if(!$('#item_qty').val()){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Qty</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $("#item_qty").focus();
      return false;
    }    

    let item_id   = $('#item_id').val();
    let item_name = $('#item_id option:selected').attr('data-name');
    let item_qty  = $('#item_qty').val();
    let item_info = $('#item_info').val();
    let id = parseInt($('#id').val());
    var id_det = id + 1;


      if(item_id){
      data = {
        det_id : id_det,
        item_id : item_id,
        item_name : item_name,
        item_qty : item_qty,
        item_info : item_info
      };

      pr._addtogrid(data);
      pr._clearitem();
      pr._focusadd();

    }
  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.det_id).index();
    //
    $('#id').val(data.det_id);

    data.act = '<button det-id="'+data.det_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.det_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> already in the list",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'transaction/request/add_item',
      data: {
        det_id    : data.det_id,
        item_id   : data.item_id,
        item_name : data.item_name,
        item_qty  : data.item_qty,
        item_info : data.item_info
      }
    });
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        det_id    : i.det_id,
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_info : i.item_info
      };
      pr._addtogrid(data);
      pr._clearitem();
      pr._focusadd();
    });
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_info').val('');
  },

  _focusadd: function(){
    $('#item_id').focus();
  },

  _removefromgrid: function(el){
    let id = $(el).attr('det-id');
    pr.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'transaction/request/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },

  save: function(e){
    e.preventDefault();

    // if(!$('#company').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "<strong>Date</strong> Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#company').select2('open');
    //   return false;
    // }

    if($('#detail').DataTable().data().count()<1){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Detail</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }
    
    if(!$('#id_user_group').val()){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Department</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $('#id_user_group').select2('open');
      return false;
    }

    // if(!$('#date').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "<strong>Date</strong> Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $("#date").focus();
    //   return false;
    // }

    if(!$('#requester').val()){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Requester</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $("#requester").focus();
      return false;
    }
    
    $('#save').prop("disabled",true);
    
    $.ajax({
      url: baseUrl+'transaction/request/form_act',
      type : "POST",  
      data: {
        // company_code: $('#company').val(),
        // company_name: $('#company option:selected').attr('data-name'),
        date      : $('#date').val(),
        requester : $('#requester').val(),
        info      : $('#info').val(),
        dept      : $('#id_user_group').val(),
        is_project:  $('#is_project').val(),
        ref_number: '',
        date_req  : ''
      },
      success : function(resp){
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          $.notify({
            message: 'Data Gagal disimpan'
          },{
            type: 'danger'
          });
          return false;

        } else {
          messages = 'Data successfully saved with document number '+resp.status.nomor_rq;
          messages += "<hr>";

          $.notify({
            message: messages
          },{
            type: 'info'
          });

          setTimeout(function () {
            window.location.href = baseUrl+'transaction/request/'; 
          }, 2000);
        }
      }
    });

  }
};

pr.init();
</script>