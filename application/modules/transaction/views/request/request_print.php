<?php 
//test($header,0);
?>
<style>
  @media print{@page {size: landscape}}
  table{
    border-collapse: collapse;
  }
  .borderluar {
    border: 2px solid black;
    padding: 0px;
  }
  .borderdalem {
    border: 1px solid #000000b0;
    padding: 0px;
  }
  .borderdalemcenter {
    border: 1px solid #000000b0;
    padding: 0px;
    text-align: center;
  }
  .bordertengah {
    border: 1px solid #000000b0;
    padding: 5px 5px 5px 5px; 
  }
  p.two {
    border-style: solid;
    border-width: 1px;
  }
</style>

<div class="app-title">
	<div>
		<h1>View Request</h1>
		<p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Transaction</li>
      <li class="breadcrumb-item active">Print Request</li>
    </ul></p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <table style="width:100%">
        <tr>
          <td><table style='width:100%' class='borderluar'> 
            <tr>
              <td rowspan='2' width='25%'>
              <img alt="" src="<?php echo base_url() ?>assets/images/<?php echo $this->current_user['logo']; ?>" width="120" height="55"></td>
              <td rowspan='2' width='25%'></td>
              <td width='25%' class='borderluar'>Date : <?php echo tgl_singkat(dbnow()); ?></td>
              <td width='25%' class='borderluar'>Revision : 02</td>
            </tr>
            <tr>
              <td colspan='2'>Title : Purchase Requisition</td>
            </tr>
            <tr>
              <td class='borderluar' colspan='4'>
                <table width='100%'>
                  <tr>
                    <td width='1%'></td>
                    <td width='49%'>Department : <?php echo $header->name_dept; ?></td>
                    <td width='20%'>No Purchase Requesition</td>
                    <td width='30%'>: <?php echo $header->request_no; ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="checkbox"> Project related material</td>
                    <td>Date of Req</td>
                    <td>: <?php echo ($header->required_date!='')? tgl_singkat($header->required_date) : ''; ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="checkbox"> New Investment</td>
                    <td>Reference Number</td>
                    <td>: <?php echo $header->reference_no; ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="checkbox"> Expansion</td>
                    <td>Delivery Date</td>
                    <td>: </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><input type="checkbox"> Consumable</td>
                    <td>Deliver To</td>
                    <td>: </td>
                  </tr>
                  <tr>
                    <td width='1%'></td>
                    <td><input type="checkbox"> Replacement <input type="checkbox"> Miscellaneous <input type="checkbox"> Car Rental</td>
                    <td>Cost Center</td>
                    <td>: </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan='4' class='borderdalem'>
                <table width='100%'>
                  <tbody>
                    <tr>
                      <td class='borderdalemcenter' width="5%">No.</td>
                      <td class='borderdalemcenter'>Description</td>
                      <td class='borderdalemcenter' width="5%">Qty</td>
                      <td class='borderdalemcenter' width="10%">Currency</td>
                      <td class='borderdalemcenter' width="12%">Est. Unit Price</td>
                      <td class='borderdalemcenter' width="12%">Total Est. Price</td>
                      <td class='borderdalemcenter'>Remarks</td>
                    </tr>
                  </tbody>
                  <thread>
                    <?php
                    $no = 0;
                    foreach ($detail as $key => $value) {
                      $no = $no+1;
                      ?>
                      <tr>
                        <td class='borderdalem'><?php echo $no; ?></td>
                        <td class='borderdalem'><?php echo $value->items_name; ?></td>
                        <td class='borderdalem' align="right"><?php echo $value->qty; ?></td>
                        <td class='borderdalem'></td>
                        <td class='borderdalem'></td>
                        <td class='borderdalem'></td>
                        <td class='borderdalem'><?php echo $value->remarks; ?></td>
                      </tr>
                      <?php 
                    }
                    ?>
                  </thread>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan='2' class='bordertengah' height='50px'>
                <table width='100%'>
                  <tr>
                    <td colspan='12' width='70%' align='center'>Asset No.</td>
                    <td colspan='3' align='center' width='30%'>Sub No.</td>
                  </tr>
                  <tr>
                    <td height='20px' align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                  </tr>
                  <tr>
                    <td colspan='15' align='center'></td>
                  </tr>
                  <tr>
                    <td height='20px' align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                    <td align='center' class='borderdalem'></td>
                  </tr>
                  <tr>
                    <td colspan='11' width='70%' align='center'></td>
                    <td colspan='2' align='center'>Yes</td>
                    <td colspan='2' align='center'>No</td>
                  </tr>
                  <tr>
                    <td colspan='11' align='left'>This Requisition covered by budget</td>
                    <td colspan='2' align='center' class='borderdalem'></td>
                    <td colspan='2' align='center' class='borderdalem'></td>
                  </tr>
                  <tr>
                    <td colspan='11' align='left'>IT Related equipment</td>
                    <td colspan='2' align='center' width='15%' class='borderdalem'></td>
                    <td colspan='2' align='center' width='15%' class='borderdalem'></td>
                  </tr>
                </table>
              </td>
              <td colspan='2' class='borderdalem'>
                <table width='100%'>
                  <tr>
                    <td colspan='15' class='borderdalem' height='63px' valign='top'>Investment Reason :</td>
                  </tr>
                  <tr>
                    <td colspan='15' width='70%' align='center' height='20px'></td>
                  </tr>
                  <tr>
                    <td colspan='15' align='left'>If No, BoD approval required</td>
                  </tr>
                  <tr>
                    <td colspan='15' align='left'>If Yes, Asset Controller approval Required</td>
                  </tr>
                </table>
              </td>
            </tr> 
            <tr>
              <td class='borderluar' colspan='4'>
                <table width='100%'>
                  <tr>
                    <td width='20%' align='center' class='borderdalem'>Requester</td>
                    <td width='20%' align='center' class='borderdalem'>Head Of Department</td>
                    <td width='20%' align='center' class='borderdalem'>Finance / HR</td>
                    <td width='20%' align='center' class='borderdalem'>BOD</td>
                    <td width='20%' align='center' class='borderdalem'>SCM</td>
                  </tr>
                  <tr>
                    <td align='left' class='borderdalem'><br><br>Name : <?php echo $header->requester; ?><br>Date : <?php echo tgl_singkat(dbnow()); ?></td>
                    <td align='left' class='borderdalem'><br><br>Name : <br>Date :</td>
                    <td align='left' class='borderdalem'><br><br>Name : <br>Date :</td>
                    <td align='left' class='borderdalem'><br><br>Name : <br>Date :</td>
                    <td align='left' class='borderdalem'><br><br>Name : <br>Date :</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class='borderluar' colspan='4'>All forms have to be typed, else will not be accepted</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </div>
</div>
</div>

</head>
<script>window.print(); setTimeout(function(){window.close();},500);</script>