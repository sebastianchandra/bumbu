<?php
// test($header,1);
?>
<style>
/* table, td, th {
    border: 1px solid black;
} */

table.header {
    width: 100%;
    border-collapse: collapse;
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
}
table td{
    padding-left: 5px;
}
table.body {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid black;
}
table.body, td.body {
  border: 1px solid black;
}
</style>
<?php 
// test($detail,0);
?>
<table class="header" id="vtable">
  <thead>
    <tr>
        <td colspan="2"><strong>Souwmpies Corp</strong></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3" align="center"><strong>PESANAN PEMBELIAN (PO)</strong></td>
        <td></td>
    </tr>
    <tr>
        <td width='20%'><br></td>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%'></td>
    </tr>
    <tr>
        <td valign="top"><strong>Nomor Pesanan</strong></td>
        <td valign="top">: <?php echo $header->po_no; ?></td>
        <td align="right" valign="top"><strong>Supplier</strong> : </td>
        <td colspan="2" valign="top"><?php echo $header->supplier_name; ?></td>
    </tr>
    <!-- <tr>
        <td valign="top"><strong>Referensi</strong></td>
        <td valign="top">: <?php echo $header->invoice_no; ?></td>
        <td align="right" valign="top"><strong>Customer</strong> : </td>
        <td colspan="2" valign="top"><?php echo $header->fc_name; ?></td>
    </tr> -->
    <tr>
        <td valign="top"><strong>Tanggal</strong></td>
        <td valign="top">: <?php echo tgl_singkat($header->po_date); ?></td>
        <td align="right"><strong>Remarks</strong> : </td>
        <td colspan="2" valign="top"><?php echo $header->po_info; ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td valign="top" align="right"><strong>Alamat</strong> : </td>
        <td colspan="2" valign="top"><?= $header->delivery_address; ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><br/></td>
    </tr>
</table>
<table class="body" id="vtable">
  <thead>
    <tr>
        <td class="body" align="center" width="5%"><strong>No. </strong></td>
        <td class="body" align="center" width="19%"><strong>Kode Barang</strong></td>
        <td class="body" align="center"><strong>Nama Barang</strong></td>
        <td class="body" align="center"><strong>Kuantiti</strong></td>
        <td class="body" align="center"><strong>Harga</strong></td>
        <td class="body" align="center"><strong>Potongan</strong></td>
        <td class="body" align="center"><strong>Subtotal</strong></td>
    </tr>
  </thead>
    <?php 
    $no     = 0;
    $total  = 0;
    foreach ($detail as $key => $value) {
    $no     = $no+1;
    ?>
    <tr>
      <td><?php echo $no; ?>.</td>
      <td><?php echo $value->items_code; ?></td>
      <td><?php echo $value->items_name; ?></td>
      <td align="right"><?php echo number_format($value->qty,2); ?></td>
      <td align="right"><?php echo number_format($value->price,2); ?></td>
      <td align="right"><?php echo number_format($value->disc_val,2); ?></td>
      <td align="right"><?php echo number_format($value->subtotal,2); ?></td>
    </tr>
    <?php 
    }
    $total    = $header->total;
    if($dp!='0.00'){
    ?>
    <tr>
      <td align="right" colspan="6">DP</td>
      <td align="right"><?php echo number_format($dp,2); ?></td>
    </tr>
    <?php 
    $total    = $total - $dp;
    }
    ?>
    <tr>
      <td class="body"  align="right" colspan="6">Total : </td>
      <td class="body"  align="right"><?php echo number_format($total,2); ?></td>
    </tr>
</table>

<table width="100%">
  <tr>
      <td colspan="4"></td>
      <td width="30%" align="center" colspan="3"><br>Hormat Kami</td>
    </tr>
    <tr>
      <td colspan="4"><br><br><br><br><br></td>
      <td colspan="3" align="center">(Souwmpie Corporation)</td>
    </tr>
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>