<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">Pesanan Pembelian</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Pesanan Pembelian</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<form class="form-horizontal" method="post" action="<?php echo base_url().'transaction/purchase_order/form_act'; ?>">    
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control"value="<?php echo $data_company['company_name']; ?>" disabled>
                <input type="hidden" class="form-control" name="company_name" value="<?php echo $data_company['company_name']; ?>">
                <input type="hidden" class="form-control" name="company_id" value="<?php echo $data_company['company_id']; ?>">
                <!-- <input type="hidden" name="pr_id" id="pr_id" value='<?php echo json_encode($this->session->userdata('new_po')['items']); ?>' required /> -->
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="po_date" name="po_date" value="<?php echo substr(dbnow(),0,10); ?>">
                <!-- <input type="hidden" class="form-control" id="po_date" name="po_date" value="<?php echo substr(dbnow(),0,10); ?>" > -->
              </div>
            </div>
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Requester</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="requester">
              </div>
            </div> -->
            
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Supplier</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="supplier_id" name="supplier_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_supplier as $key => $value) {
                    echo "<option value='".$value->supplier_id."' data-supplier_name='".$value->supplier_name."'>".$value->supplier_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Alamat Kirim</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="delivery_address" name="delivery_address">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $header_pr->pr_info; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>PO DP</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="po_dp" name="po_dp[]" multiple="multiple">
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th>Nama Barang</th>
                        <th width="8%">Qty</th>
                        <th>Info</th>
                        <th width="20%">Harga</th>
                        <th width="15%">Total Potongan</th>
                        <th width="20%">Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $sub_total    = 0;
                      $total        = 0;
                      foreach ($data_pr as $key => $value) {
                      $sub_total    = $value->qty*$value->price;
                      $total        = $sub_total+$total;
                      ?>
                      <tr>
                        <td></td>                          
                        <td>
                          <input type="hidden" id="items_id<?php echo $key; ?>" name="items_id[]" value="<?php echo $value->items_id ?>">
                          <input type="hidden" id="items_id<?php echo $key; ?>" name="items_name[]" value="<?php echo $value->items_name ?>">
                          <input type="hidden" id="items_id<?php echo $key; ?>" name="qty[]" value="<?php echo $value->qty ?>">
                          <?php echo $value->items_name; ?>
                        </td>
                        <td><?php echo $value->qty; ?></td>
                        <td><?php echo $value->items_id; ?></td>
                        <td><input class="form-control price" onchange="return calc(this)" data-id_row="<?php echo $key; ?>" data-qty="<?php echo $value->qty ?>" type="text" name="price[]" id="price<?php echo $key; ?>" value="<?php echo number_format($value->price,2); ?>"></td>
                        <td><input class="form-control price" onchange="return calc_pot(this)" type="text" name="potongan[]" id="potongan<?php echo $key; ?>" data-id_row="<?php echo $key; ?>" value="0.00"></td>
                        <td><input class="form-control subtotal" disabled="" id="sub_total<?php echo $key; ?>" type="text" value="<?php echo number_format($sub_total,2); ?>"></td>
                      </tr>
                      <?php 
                      }
                      ?>
                      <tr>
                        <td></td>                          
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Potongan DP</td>
                        <td><input class="form-control" disabled="" type="text" id='pot_dp'></td>
                      </tr>
                      <tr>
                        <td><input type="hidden" id="tloop" name="tloop" value="<?php echo $key ?>"></td>                          
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td><input class="form-control"  disabled="" type="text" id='total' value="<?php echo number_format($total,2); ?>"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm" type="submit" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('transaction/purchase_order'); ?>">Batal</a>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">

$('#save').click(function(){

  if(!$('#supplier_id').val()){
    toastr.error("<strong>Nama Supplier</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#supplier_id').select2('open');
    return false;
  }
});

$("#supplier_id").select2().on('select2:select',function(e){
  var id = $('#supplier_id').val();
    $.ajax({
      url : baseUrl+'transaction/po_dp/detail_dp_supplier',
      method : "POST",
      data : {id: id},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="0"  data-amount="0"> - </option>';
        for(i=0; i<data.length; i++){
          // html += '<option value="'+data[i].pd_id+'" data-amount="'+data[i].amount+'" >'+data[i].pd_no+'</option>';
          html += '<option value="'+data[i].pd_id+'" data-amount="'+data[i].amount+'" >'+data[i].pd_no+'</option>';
        }
        $('#po_dp').html(html);
      }
    })
});

var total_dp = 0;
$("#po_dp").select2().on('select2:select',function(e){
  var amount  = e.params.data.element.dataset.amount;
  var total   = parseInt($("#total").val().replace(/\,/g, ''));
  total_dp    = parseFloat(amount)+parseFloat(total_dp);
  var sisa    = total - amount;
  $("#total").val(numberWithCommas(sisa));
  $("#pot_dp").val(numberWithCommas(total_dp));
});
$("#po_dp").select2().on('select2:unselect',function(e){
  var amount  = e.params.data.element.dataset.amount;
  var total   = parseInt($("#total").val().replace(/\,/g, ''));
  total_dp    = parseFloat(total_dp)-parseFloat(amount);
  var sisa    = parseFloat(total) + parseFloat(amount);
  $("#total").val(numberWithCommas(sisa));
  $("#pot_dp").val(numberWithCommas(total_dp));
});


function calc(e){
  var idRow     = $(e).data('id_row');
  var price     = $('#price'+idRow).val().replace(/\,/g, '');
  var qty       = $('#price'+idRow).attr('data-qty').replace(/\,/g, '');
  var potongan  = $('#potongan'+idRow).val().replace(/\,/g, '');
  var subtotal  = (parseFloat(price)*parseFloat(qty))-parseFloat(potongan);
  $("#sub_total"+idRow).val(numberWithCommas(subtotal));

  total();
}

function calc_pot(e){
  var idRow     = $(e).data('id_row');
  var price     = $('#price'+idRow).val().replace(/\,/g, '');
  var qty       = $('#price'+idRow).attr('data-qty').replace(/\,/g, '');
  var potongan  = $('#potongan'+idRow).val().replace(/\,/g, '');
  var subtotal  = (parseFloat(price)*parseFloat(qty))-parseFloat(potongan);
  $("#sub_total"+idRow).val(numberWithCommas(subtotal));

  total();
}

function total(){
  var total     = 0;
  // if()
  var amount    = $('#pot_dp').val().replace(/\,/g, '');
  for (i = 0; i < $('.subtotal').length; i++) {
    var subtotal_i  = parseInt($('#sub_total'+i).val().replace(/\,/g, ''));
    var total       = parseInt(subtotal_i)+parseInt(total);
  }
  total     = total - amount;
  $("#total").val(numberWithCommas(total));
}

$("input[name='menu_id']:checked").each(function(){
  lang.push(this.value);
});

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#supplier_id").select2();
    // $("#po_dp").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4,5,6],
          "orderable": false,
        },
        { 
          "targets": [0,3],
          "visible": false,
          "searchable": false
        }
      ]
    });

    // $('#save').click(pr.save);

  }

};

pr.init();

</script>

<!--  
1. Penambahan Discount pada detail barang purchase order
-->