<?php 
// test(,1);
if($this->session->flashdata('alert')!=''){
// if(isset($this->session->flashdata('alert'))){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">Pesanan Pembelian</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Pesanan Pembelian</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_pr(this)">Input</button>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="10%">Nomor</th>
                    <!-- <th>Purchase Requisition</th> -->
                    <th>Tanggal</th>
                    <!-- <th>Nama Project</th> -->
                    <th>Nama Company</th>
                    <th>Nama Supplier</th>
                    <!-- <th>Requester</th> -->
                    <th>Status</th>
                    <th>Total</th>
                    <th width="22%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_po as $key => $value) {
                    // test($value,1);
                  ?>
                  <tr>
                      <td>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                      data-id="<?php echo $value->po_id; ?>" 
                      data-po="<?php echo $value->po_no; ?>"><?php echo $value->po_no; ?></a></strong></td>
                      <!-- <td><?php echo tgl_singkat($value->pr_date); ?></td> -->
                      <td><?php echo tgl_singkat($value->po_date); ?></td>
                      <!-- <td><?php echo $value->project_name; ?></td> -->
                      <td><?php echo $value->company_name; ?></td>
                      <td><?php echo $value->supplier_name; ?></td>
                      <!-- <td><?php echo $value->pr_status; ?></td> -->
                      <td><?php echo $value->po_status; ?></td>
                      <td align="right"><?php echo money($value->total); ?></td>
                      <td align="center"><!-- <a href="<?php echo base_url('transaction/purchase_order/edit/'.$value->pr_id); ?>" class="btn btn-danger btn-xs">Edit</a> -->
                      <?php 
                      if($value->po_status=='Process' || $value->po_status=='Outstanding'){
                      ?>
                        <button id="delete" data-po_no='<?php echo $value->po_no; ?>' data-po_id='<?php echo $value->po_id; ?>' class="btn btn-danger btn-xs" type="submit">Tutup Paksa</button>
                        <button id="reject" data-po_no='<?php echo $value->po_no; ?>' data-po_id='<?php echo $value->po_id; ?>' class="btn btn-warning btn-xs" type="submit">Batal</button>
                      <?php
                      }else{
                      ?>
                        <button class="btn btn-danger btn-xs" disabled="">Tutup Paksa</button>
                        <button class="btn btn-warning btn-xs" disabled="">Batal</button>
                      <?php 
                      }
                      ?>
                        <a target="_blank" href="<?php echo base_url('transaction/purchase_order/view_print/'.$value->po_id); ?>" class="btn btn-info btn-xs">Print</a>
                      </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myPr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Nomor Permintaan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th>PR No</th>
                    <th width="15%">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Pesanan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>No Pesanan Pembelian</strong></p>
          </div>
          <div class="col-md-4">
            <div id="no_po"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="po_date"></div>
          </div>          
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Nama Project</strong></p>
          </div>
          <div class="col-md-4">
            <div id="project_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_id"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Nama Supplier</strong></p>
          </div>
          <div class="col-md-4">
            <div id="supplier_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-10">
            <div id="remarks"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th width="8%">No. </th>
                    <th width="10%">Kode Barang </th>
                    <th>Nama Barang</th>
                    <th width="15%">Qty</th>
                    <th width="20%">Harga</th>
                    <th width="25%">Subtotal</th>
                    <th width="15%">Total Potongan</th>
                    <th width="25%">Total</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }

  if(values.length>=1){
    $.ajax({
      url: baseUrl+'transaction/purchase_order/pr_selected',
      type : "POST",  
      data: {
        values      : values
      },
      success : function(resp){        
        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_order/po_input'; 
        }, 100);
      }
    });
  }else{
    toastr.warning("Select PR Number.", 'Alert', {"positionClass": "toast-top-center"});
    return false;
  }
  // alert(values);
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 3,4,5,6,7 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_pr(e){  
  $.get({
    url: baseUrl + 'transaction/purchase_order/view_pr/',
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });

  $('#myPr').modal('show'); 
}

function show_detail(e){
  var idPo     = $(e).data('id');
  var noPo     = $(e).data('po');

  $.get({
    url: baseUrl + 'transaction/purchase_order/view_popup/'+idPo,
    success: function(resp){
      $('#company_id').text(resp.header.company_name);
      $('#no_po').text(resp.header.po_no);
      $('#po_date').text(tanggal_indonesia(resp.header.po_date));
      $('#requester').text(resp.header.requester);
      $('#remarks').text(resp.header.po_info);
      $('#status').text(resp.header.po_status);
      $('#supplier_name').text(resp.header.supplier_name);
      $('#project_name').text(resp.header.project_name);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var po_id  = $(this).data('po_id');
    var po_no  = $(this).data('po_no');

    toastr.warning(
      'Apakah Anda Ingin Tutup Paksa '+po_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-po_no="'+po_no+'" data-po_id="'+po_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });

  $('#vtable').on('click','#reject', function(e){
    var po_id  = $(this).data('po_id');
    var po_no  = $(this).data('po_no');

    toastr.warning(
      'Apakah Anda Ingin Membatalkan '+po_no+'?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return rejectRow(this)" data-po_no="'+po_no+'" data-po_id="'+po_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });
});

function deleteRow(e){
  var po_id  = $(e).data('po_id');
  var po_no  = $(e).data('po_no');

  $.ajax({
    data: {
      po_id  : po_id,
      po_no  : po_no
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_order/force_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Tutup Paksa.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_order'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function rejectRow(e){
  var po_id  = $(e).data('po_id');
  var po_no  = $(e).data('po_no');

  $.ajax({
    data: {
      po_id  : po_id,
      po_no  : po_no
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_order/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_order'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

















