<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">PO Down Payment</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View PO Down Payment</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("transaction/po_dp/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="13%">Nomor PO DP</th>
                    <th>Tanggal PO DP</th>
                    <th>Nama Supplier Name</th>
                    <th>Jumlah DP</th>
                    <th>Nomor PO</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_po_dp as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->pd_no; ?></td>
                    <td><?php echo tgl_singkat($value->pd_date); ?></td>
                    <td><?php echo $value->supplier_name; ?></td>
                    <td><?php echo money($value->amount,2); ?></td>
                    <td><?php echo $value->po_no; ?></td>
                    <td class="center">
                      <?php 
                      if($value->po_no!=''){
                      ?>
                      <a href="<?php echo base_url('transaction/po_dp/edit/'.$value->pd_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-pd_id='<?php echo $value->pd_id; ?>' data-pd_no='<?php echo $value->pd_no; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                      <?php
                      }else{
                      ?>
                      <button class="btn btn-danger btn-xs" type="submit" disabled="">Edit</button>
                      <button id="delete" class="btn btn-warning btn-xs" type="submit" disabled="">Delete</button>
                      <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pd_id  = $(this).data('pd_id');
    var pd_no  = $(this).data('pd_no');

    toastr.warning(
      'Do you want to delete '+pd_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pd_no="'+pd_no+'" data-pd_id="'+pd_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var pd_id  = $(e).data('pd_id');
  var pd_no  = $(e).data('pd_no');

  $.ajax({
    data: {
      pd_id  : pd_id
    },
    type : "POST",
    url: baseUrl+'transaction/po_dp/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/po_dp'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
