<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">PO Down Payment</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input PO Down Payment</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Tanggal</p>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="pd_date" name="pd_date"  value="<?php echo substr(dbnow(),0,10); ?>">
                                <!-- <input type="hidden" class="form-control" id="pd_date" name="pd_date" value="<?php echo substr(dbnow(),0,10); ?>" > -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama Supplier</p>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="supplier_id">
                                    <option value="">Select</option>
                                    <?php 
                                    foreach ($data_supplier as $key => $value) {
                                        echo "<option value='".$value->supplier_id."' data-supplier_name='".$value->supplier_name."'>".$value->supplier_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Jumlah DP</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control price" id="amount" placeholder="Amount">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Remarks</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="remarks" placeholder="Remarks">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('transaction/po_dp'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#items_name').focus();
$("#supplier_id").select2();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#supplier_id').val()){
            toastr.error("<b>Nama Supplier</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#supplier_id').select2('open');
            return false;
        }
        if(!$('#amount').val()){
            toastr.error("<b>JumlaH dp</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#amount').select2('open');
            return false;
        }

        $.ajax({
            url  : baseUrl+'transaction/po_dp/form_act',
            type : "POST",  
            data : {
                pd_date             : $('#pd_date').val(),
                supplier_id         : $('#supplier_id').val(),
                supplier_name       : $('#supplier_id option:selected').attr('data-supplier_name'),
                amount              : $('#amount').val().replace(/\,/g, ''),
                remarks             : $('#remarks').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'transaction/po_dp/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>