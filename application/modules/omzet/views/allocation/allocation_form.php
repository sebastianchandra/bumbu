<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Form Alokasi Omset</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Alokasi Omset</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('omzet/Sales/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead style="text-align: center;">
                  <tr>
                    <th width="3%" rowspan="4">No</th>
                    <!-- <th rowspan="3">Project Name</th> -->
                    <th rowspan="3">Project Value</th>
                    <th>Owner</th>
                    <th>Nahkoda</th>
                    <th colspan="6 ">Operasional</th>
                  </tr>
                  <tr>
                    <th>A * 20%</th>
                    <th>A * 5%</th>
                    <th>A * 75%</th>
                    <th colspan="2">Biaya</th>
                    <th>D - (E+F)</th>
                    <th>G * 50%</th>
                    <th>G * 50%</th>
                  </tr>
                  <tr>
                    <th>Value</th>
                    <th>Value</th>
                    <th>Value</th>
                    <th>Pembelian</th>
                    <th>Pinjaman</th>
                    <th>Total</th>
                    <th>Owner</th>
                    <th>ABK</th>
                  </tr>
                  <tr>
                    <th>A</th>
                    <th>B</th>
                    <th>C</th>
                    <th>D</th>
                    <th>E</th>
                    <th>F</th>
                    <th>G</th>
                    <th>H</th>
                    <th>I</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no             = 0;
                  $allow_owner1   = 0.20;
                  $allow_nahkoda  = 0.05;
                  $allow_opr      = 0.75;
                  $allow_owner2   = 0.50;
                  $allow_abk      = 0.50;
                  foreach ($data_sales as $key => $value) {
                    $no           = $no+1;
                    $owner1       = $value->total*$allow_owner1;
                    $nahkoda      = $value->total*$allow_nahkoda;
                    $opr          = $value->total*$allow_opr;

                    $biaya_opr    = $this->db->query("SELECT a.is_no,a.project_id,a.project_name,a.is_status,SUM(b.total) total
                                                    FROM db_bumbu_transaction.trn_incoming_stock_01 a
                                                    LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                                                    WHERE a.is_status<>'Reject' AND a.project_id='".$value->project_id."' ")->row()->total;
                    $loan_crew    = $this->db->query("SELECT a.project_id,a.project_name,SUM(a.amount) amount,a.cl_status FROM 
                                                    db_loan_shp.trn_project_loan a
                                                    WHERE a.is_active='1' AND a.cl_status<>'Paid' 
                                                    AND a.project_id='".$value->project_id."'")->row()->amount;
                    $total_opr    = $opr - ($biaya_opr+$loan_crew);
                    $owner2       = $total_opr*$allow_owner2;
                    $abk          = $total_opr*$allow_abk;
                  ?>
                  <tr>
                    <td rowspan="2"><?php echo $no; ?></td>
                    <td colspan="9"><?php echo $value->project_name; ?></td>
                  </tr>
                  <tr>
                    <!-- <td><?php echo $no; ?></td> -->
                    <!-- <td><?php echo $value->project_name; ?></td> -->
                    <td align="right"><?php echo number_format($value->total,2); ?></td>
                    <td align="right"><?php echo number_format($owner1,2); ?></td>
                    <td align="right"><?php echo number_format($nahkoda,2); ?></td>
                    <td align="right"><?php echo number_format($opr,2); ?>)</td>
                    <td align="right" style="color: red">(<?php echo number_format($biaya_opr,2); ?>)</td>
                    <td align="right" style="color: red">(<?php echo number_format($loan_crew,2); ?>)</td>
                    <td align="right" ><?php echo number_format($total_opr,2); ?></td>
                    <td align="right" ><?php echo number_format($owner2,2); ?></td>
                    <td align="right" ><?php echo number_format($abk,2); ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
              
            </div>
            <div class="row-form">
              <div class="col-md-8"></div>
              <div class="col-md-4"><br/>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('omzet/allocation/process_allocation'); ?>">Proses</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Penjualan</h4>
      </div>
      <div class="modal-body">
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>No Dok</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_id"></div>
          </div>
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_date"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="fc_name"></div>
          </div>
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-4">
            <div id="remarks"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="status"></div>
          </div>
        </div>
        <div class="row-form">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th width="15%">Qty</th>
                    <th width="15%">Harga</th>
                    <th width="15%">Sub Total</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_detail(e){
  var salesId     = $(e).data('id');
  var salesNo     = $(e).data('sales');
  
  var myUrl = baseUrl + 'omzet/Sales/view_popup/'+salesId; 
  // alert(myUrl);
  $.get({
    url: myUrl,
    success: function(resp){
      $('#company_id').text(resp.header.company_name);
      $('#sales_no').text(resp.header.sales_no);
      $('#sales_date').text(resp.header.sales_date);
      $('#fc_name').text(resp.header.fc_name);
      $('#remarks').text(resp.header.remarks);
      $('#status').text(resp.header.doc_status);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}
$(document).ready(function(){
  

  $('#vtable').DataTable({
    "paging":   false,
    "ordering": false,
    "info":     false,
    "filter":   false,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pr_id  = $(this).data('pr_id');

    toastr.warning(
      'Apakah Anda ingin menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pr_id="'+pr_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });

  $('#vtable').on('click','#reject', function(e){

    var sales_id  = $(this).data('sales_id');
    var sales_no  = $(this).data('sales_no');

    toastr.warning(
      'Apakah anda ingin menolak '+sales_no+'?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return rejectRow(this)" data-sales_id="'+sales_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });
});

function rejectRow(e){
  var sales_id  = $(e).data('sales_id');

  $.ajax({
    data: {
      sales_id  : sales_id
    },
    type : "POST",
    url: baseUrl+'omzet/Sales/reject_js',
    success : function(resp){
      
      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Tolak');
        return false;

      } else {
        toastr.success("Data Berhasil Di Tolak.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'omzet/Sales'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function deleteRow(e){
  var pr_id  = $(e).data('pr_id');

  $.ajax({
    data: {
      pr_id  : pr_id
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_requisition/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>




