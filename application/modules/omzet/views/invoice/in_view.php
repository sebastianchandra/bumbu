<?php if (isset($_SESSION['alert'])): ?>
  <script type="text/javascript">
    Command: toastr["info"]("<?php echo $_SESSION['alert']; ?>", "Alert",{
      "closeButton": true,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "5000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "5000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }).css("width","750px");
  </script>
<?php endif; ?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Invoice</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4>View Invoice</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_do(this)">Input</button> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="10%">No</th>
                    <th width="10%">No Ref</th>
                    <th width="10%">Tanggal</th>
                    <!-- <th>Project Name</th> -->
                    <th>Nama Customer</th>
                    <!-- <th>Supplier Name</th> -->
                    <!-- <th>Warehouse</th> -->
                    <th>Status</th>
                    <th>Total</th>
                    <th width="15%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  // test($data_sales,1);
                  foreach ($data_sales as $key => $value) {
                  ?>
                  <tr>
                    <td>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                    data-id="<?php echo $value->invoice_id; ?>" 
                    data-sales="<?php echo $value->invoice_no; ?>"><?php echo $value->invoice_no; ?></a></strong></td>
                    <td><?php echo $value->doc_ref; ?></td>
                    <td><?php echo tgl_singkat($value->invoice_date); ?></td>
                    <!-- <td><?php #echo $value->project_name; ?></td> -->
                    <td><?php echo $value->fc_name; ?></td>
                    <!-- <td><?php echo $value->supplier_name; ?></td> -->
                    <!-- <td><?php echo $value->warehouse_name; ?></td> -->
                    <td><?php echo $value->doc_status; ?> <?= ($value->is_cancel==1)? '(Reject)' : '';?></td> 
                    <td align="right"><?php echo number_format($value->total_cust_price,2); ?> </td> 
                    <td align="center">
                      <?php 
                      if($value->doc_status =='Close' OR $value->is_cancel=='1'){
                      ?>
                      <button class="btn btn-info btn-xs" disabled="">Print</button>
                      <button class="btn btn-warning btn-xs" disabled="">Reject</button>
                      <!-- <button class="btn btn-warning btn-xs" disabled="">Force Close</button> -->
                      <?php
                      }else{
                      echo '<a target="_blank" href="'.base_url('omzet/invoice/view_print/'.$value->invoice_id).'" class="btn btn-info btn-xs">Print</a>';
                      ?>
                      <!-- <button id="delete" data-sales_id='<?php echo $value->sales_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button> -->
                      <button id="reject" data-invoice_no='<?php echo $value->invoice_no; ?>' data-invoice_id='<?php echo $value->invoice_id; ?>'
                      data-invoice_no='<?php echo $value->invoice_no; ?>' data-doc_ref='<?php echo $value->doc_ref; ?>' class="btn btn-warning btn-xs" type="submit">Reject</button>
                      <!-- <button id="force " data-sales_id='<?php echo $value->sales_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button> -->
                      <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Penjualan</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>No Dok</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_id"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_date"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="fc_name"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-4">
            <div id="remarks"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th width="15%">Qty</th>
                    <th width="15%">Harga</th>
                    <th width="15%">Potongan</th>
                    <th width="15%">Sub Total</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myPr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Nomor Permintaan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th>SO No</th>
                    <th>SO Status</th>
                    <th width="15%">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<script>
var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1,2,3,4 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_do(e){  
  $.get({
    url: baseUrl + 'omzet/delivery_order/view_so/',
    success: function(resp){
      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });

  $('#myPr').modal('show'); 
}

function show_detail(e){
  var salesId     = $(e).data('id');
  var salesNo     = $(e).data('sales');
  
  var myUrl = baseUrl + 'omzet/invoice/view_popup/'+salesId; 
  // alert(myUrl);
  $.get({
    url: myUrl,
    success: function(resp){
      $('#company_id').text(resp.header.company_name);
      $('#sales_no').text(resp.header.invoice_no);
      $('#sales_date').text(tanggal_indonesia(resp.header.invoice_date));
      $('#fc_name').text(resp.header.fc_name);
      $('#remarks').text(resp.header.remarks);
      $('#status').text(resp.header.doc_status);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}
$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pr_id  = $(this).data('pr_id');

    toastr.warning(
      'Apakah Anda ingin menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pr_id="'+pr_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });

  $('#vtable').on('click','#reject', function(e){

    var invoice_id  = $(this).data('invoice_id');
    var invoice_no  = $(this).data('invoice_no');
    var doc_ref  = $(this).data('doc_ref');

    toastr.warning(
      'Apakah anda ingin menolak '+invoice_no+'?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return rejectRow(this)" data-doc_ref="'+doc_ref+'" data-invoice_id="'+invoice_id+'" data-invoice_no="'+invoice_no+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });
});

function rejectRow(e){
  var invoice_id  = $(e).data('invoice_id');
  var invoice_no  = $(e).data('invoice_no');
  var doc_ref     = $(e).data('doc_ref');

  $.ajax({
    data: {
      invoice_id  : invoice_id,
      invoice_no  : invoice_no,
      doc_ref : doc_ref
    },
    type : "POST",
    url: baseUrl+'omzet/invoice/reject_js',
    success : function(resp){
      
      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Tolak');
        return false;

      } else {
        toastr.success("Data Berhasil Di Tolak.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'omzet/invoice'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function deleteRow(e){
  var pr_id  = $(e).data('pr_id');

  $.ajax({
    data: {
      pr_id  : pr_id
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_requisition/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>




