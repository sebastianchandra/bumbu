<?php
// if($status=='excell'){
//   $file = "current_stock.xls";
//   header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//   header('Content-Disposition: attachment; filename='.$file);
// }
?>
<style>
/* table, td, th {
    border: 1px solid black;
} */

table.header {
    width: 100%;
    border-collapse: collapse;
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
}
table td{
    padding-left: 5px;
}
table.body {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid black;
}
table.body, td.body {
  border: 1px solid black;
}
</style>
<?php 
// test($detail,0);
?>
<table class="header" id="vtable">
  <thead>
    <tr>
        <td colspan="2"><strong>Souwmpies Corp</strong></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3" align="center"><strong>INVOICE</strong></td>
        <td></td>
    </tr>
    <tr>
        <td width='20%'><br></td>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%'></td>
    </tr>
    <tr>
        <td valign="top"><strong>Nomor Invoice</strong></td>
        <td valign="top">: <?php echo $header->invoice_no; ?></td>
        <td align="right" valign="top"><strong>Customer</strong> : </td>
        <td colspan="2" valign="top"><?php echo $header->fc_name; ?></td>
    </tr>
    <tr>
        <td valign="top"><strong>Tanggal</strong></td>
        <td valign="top">: <?php echo tgl_singkat($header->invoice_date); ?></td>
        <td align="right"><strong>Remarks</strong> : </td>
        <td colspan="2" valign="top"><?php echo $header->remarks; ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td valign="top" align="right"><!-- Alamat --></td>
        <td colspan="2" valign="top"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><br/></td>
    </tr>
</table>
<table class="body" id="vtable">
  <thead>
    <tr>
        <td class="body" align="center" width="5%"><strong>No. </strong></td>
        <td class="body" align="center" width="19%"><strong>Kode Barang</strong></td>
        <td class="body" align="center"><strong>Nama Barang</strong></td>
        <td class="body" align="center"><strong>Kuantiti</strong></td>
        <td class="body" align="center"><strong>Harga</strong></td>
        <td class="body" align="center"><strong>Potongan</strong></td>
        <td class="body" align="center"><strong>Subtotal</strong></td>
    </tr>
  </thead>
    <?php 
      $no               = 0;
      $total            = 0;
      $tpot_detail      = 0;
      $pot_detail       = 0;
      $subtotal         = 0;
      foreach ($detail as $key => $value) {
      $no     = $no+1;

      if($value->disc_value>=1){
        $tpot_detail  = $value->disc_value+$tpot_detail;
        $pot_detail   = $value->disc_value;
      }else{
        $tpot_detail  = ((($value->qty*$value->cust_price)*$value->disc_percent)/100)+$tpot_detail;
        $pot_detail   = ((($value->qty*$value->cust_price)*$value->disc_percent)/100);
      }

      $subtotal       = $value->subtotal+$subtotal;
    ?>
    <tr>
      <td valign="top"><?php echo $no; ?>.</td>
      <td valign="top"><?php echo $value->items_code; ?></td>
      <td valign="top"><?php echo $value->items_name; ?></td>
      <td align="right" valign="top"><?php echo number_format($value->qty,2); ?></td>
      <td align="right" valign="top"><?php echo number_format($value->cust_price,2); ?></td>
      <td align="right" valign="top"><?php echo number_format($pot_detail,2); ?></td>
      <td align="right" valign="top"><?php echo number_format($value->subtotal,2); ?></td>
    </tr>
    <?php 
    }
    // test($header,1);
    if($header->disc_value>=1){
      $pot_header       = $header->disc_value;
    }else{
      $pot_header       = ((($subtotal-$tpot_detail)*$header->disc_percent)/100);
    }
    
    $tpotongan          = $pot_header+$tpot_detail;
    $total_bayar        = $subtotal-$tpotongan;
    ?>
    <tr>
      <td class="body" colspan="6" align="right">Subtotal</td>
      <td class="body" align="right"><?php echo number_format($subtotal,2); ?></td>
    </tr>
    <tr>
      <td class="body" colspan="6" align="right"> Total Potongan (Potongan Header dan Detail) </td>
      <td class="body" align="right"><?php echo number_format($tpotongan,2); ?></td>
    </tr>
    <tr>
      <td class="body" colspan="6" align="right"> Jumlah yang harus dibayarkan </td>
      <td class="body" align="right"><?php echo number_format($total_bayar,2); ?></td>
    </tr>
</table>

<table width="100%">
  <tr>
      <td colspan="4"></td>
      <td width="30%" align="center" colspan="3"><br>Hormat Kami</td>
    </tr>
    <tr>
      <td colspan="4"><br><br><br><br><br></td>
      <td colspan="3" align="center">(Souwmpie Corporation)</td>
    </tr>
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>