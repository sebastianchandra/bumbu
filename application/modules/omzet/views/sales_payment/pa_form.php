<?php 
//test($data_so[0]->supplier_id,1);
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Sales Payment</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Sales Payment</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_pr(this)">Input</button> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- <form class="form-horizontal" method="post" action="<?php echo base_url().'omzet/sales_payment/form_act'; ?>"> -->
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal" method="post" action="<?php echo base_url().'omzet/sales_payment/form_act'; ?>">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control"value="<?php echo $data_company['company_name']; ?>" disabled>
                <input type="hidden" class="form-control" name="company_name" value="<?php echo $data_company['company_name']; ?>">
                <input type="hidden" class="form-control" name="company_id" value="<?php echo $data_company['company_id']; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal </p>
              </div>
              <div class="col-md-3">
                <!-- <input type="text" class="form-control"  value="<?php echo substr(dbnow(),0,10); ?>" disabled> -->
                <input type="date" class="form-control" id="pp_date" name="pp_date" value="<?php echo substr(dbnow(),0,10); ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Customer</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control"  value="<?php echo $data_so[0]->fc_name; ?>" disabled>
                <input type="hidden" name="fc_name" value="<?php echo $data_so[0]->fc_name; ?>">
                <input type="hidden" name="fc_id" value="<?php echo $data_so[0]->fc_id; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tipe Pembayaran</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id="payment_type" name="payment_type">
                  <option value="">Select</option>
                  <option value="1">Cash</option>
                  <option value="2">Transfer</option>
                  <option value="3">Giro</option>
                  <option value="4">Cek</option>                  
                </select>
              </div>  
            </div>
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>DP Sales Order</p>
              </div>
              <div class="col-md-5">
                <?php //test($data_dp,1); ?>
                <select class="form-control" id="so_dp" name="so_dp[]" multiple="multiple">
                  <?php
                  foreach ($data_dp as $key => $value) {
                  ?>
                    <option value="<?= $value->sd_id ?>" data-nilai="<?= $value->amount ?>"><?= $value->sd_no ?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div> -->
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div> -->
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th width="15%">Nomor Sales</th>
                        <th width="10%">Tanggal Sales</th>
                        <th>Nama Customer</th>
                        <th width="17%">Nilai </th>
                        <th width="17%">Total Invoice</th>
                        <th width="17%">Total Terima</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      // test($data_so,0);
                      $sisa     = 0;
                      $tsisa    = 0;
                      foreach ($data_so as $key => $value) {
                        // test($value,1);
                        $total    = 0;
                        $sisa     = ($value->total_owner_price+$value->total_cust_price+$value->total_gov_price) - $value->tbayar;
                        $tsisa    = $tsisa+$sisa;
                        
                      if($value->total_owner_price!='0.00'){

                      }else if($value->total_cust_price!='0.00'){

                      }else if($value->total_gov_price!='0.00'){

                      }
                      ?>
                      <tr>
                        <td></td>                          
                        <td>
                          <input type="hidden" id="invoice_id<?php echo $key; ?>" name="invoice_id[]" value="<?php echo $value->invoice_id ?>">
                          <input type="hidden" id="invoice_no<?php echo $key; ?>" name="invoice_no[]" value="<?php echo $value->invoice_no ?>">
                          <input type="hidden" id="jml_hutang<?php echo $key; ?>" name="jml_hutang[]" value="<?php echo $sisa ?>">                          
                          <?php echo $value->invoice_no; ?>
                        </td>
                        <td><?php echo $value->invoice_date; ?></td>
                        <td><?php echo $value->fc_name; ?></td>
                        <td><?php echo money($value->total_cust_price); ?></td>
                        <td><input disabled="" class="form-control price" type="text" name="totali[]" id="totali<?php echo $key; ?>" value="<?php echo number_format($sisa,2); ?>"></td>
                        <td><input  onchange="return tambah(this)" class="form-control price" type="text" name="total[]" id="total<?php echo $key; ?>" value="<?php echo number_format($sisa,2); ?>" ></td>
                      </tr>
                      <?php 
                      }
                      $key    = $key+1;
                      ?>
                    </tbody>
                    <tfoot>
                      <!-- <tr>
                        <td colspan="5" align="right">Total Potongan DP Sales Order</td>
                        <td><input class="form-control" disabled="" type="text" id='pot_dp'></td>
                      </tr> -->
                      <tr>
                        <td colspan="5" align="right">Total </td>
                        <td><input class="form-control price" type="text" disabled="" id='total' max="<?php echo $tsisa; ?>" value="<?php echo number_format($tsisa,2); ?>"></td>
                        <td>
                          <input disabled="" class="form-control price" type="text" name="total_pembayaran" id="total_pembayaran" value="<?php echo number_format($tsisa,2); ?>">
                          <input class="form-control price" type="hidden" name="total_pembayaran_hid" id="total_pembayaran_hid" value="<?php echo number_format($tsisa,2); ?>">
                        </td>
                      </tr>
                      <!-- <tr>
                        <td colspan="5" align="right">Total Bayar</td>
                        <td><input class="form-control price" type="text" id='total_bayar' name="total_bayar" max="<?php echo $sisa; ?>" value="<?php echo number_format($sisa,2); ?>"></td>
                      </tr> -->
                    </tfoot>
                  </table>
                  <input type="hidden" id="tdetail" name="tdetail" value="<?php echo $key; ?>">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm" type="submit" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('omzet/sales_payment'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- </form> -->

<script type="text/javascript">

function tambah(e){
  // debugger
  var i;
  var tdetail     = $('#tdetail').val();
  var tpayment    = 0;

  for (i = 0; i < tdetail; i++) {
    var total      = parseFloat($('#total'+i).val().replace(/\,/g, ''));
    tpayment            = total+tpayment;

  }

  $('#total_pembayaran').val(tpayment);
  $('#total_pembayaran_hid').val(tpayment);
}

var total_dp = 0;
$("#so_dp").select2().on('select2:select',function(e){
  var amount  = e.params.data.element.dataset.nilai;
  var total   = parseInt($("#total").val().replace(/\,/g, ''));
  total_dp    = parseFloat(amount)+parseFloat(total_dp);
  var sisa    = total - amount;
  $("#total").val(numberWithCommas(sisa));
  $("#total").prop("max", sisa);
  $("#pot_dp").val(numberWithCommas(total_dp));
  $("#total_bayar").val(numberWithCommas(sisa));
});

$('#save').click(function(){
  
  if(!$('#pp_date').val()){
    toastr.error("<strong>Tanggal Pembayaran</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#pp_date').focus();
    return false;
  }
  
  if($('#payment_type').val()==''){
    toastr.error("<strong>Tipe Pembayaran</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
    $('#payment_type').select2('open');
    return false;
  }

  if(parseFloat($('#total_bayar').attr('max')) < parseFloat($("#total_bayar").val().replace(/\,/g, ''))){
    toastr.error("<strong>Total Pembayaran</strong> Melebihi Total", 'Alert', {"positionClass": "toast-top-center"});
    return false;
  }
  



  // var i;
  // var tdetail     = $('#tdetail').val();

  // for (i = 0; i < tdetail; i++) {
  //   var jml_hutang      = parseFloat($('#jml_hutang'+i).val().replace(/\,/g, ''));
  //   var total           = parseFloat($('#total'+i).val().replace(/\,/g, ''));
  //   var po_no           = $('#po_no'+i).val();

  //   if(total > jml_hutang){
  //     toastr.error("Jumlah Pembayaran Melebihi Jumlah Hutang <strong>"+po_no+"</strong> ", 'Alert', {"positionClass": "toast-top-center"});
  //     $("#total"+i).focus();
  //     return false;
  //   }

  // }



  

  // if(!$('#supplier_id').val()){
  //   toastr.error("<strong>Supplier Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#supplier_id').select2('open');
  //   return false;
  // }
});

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#payment_type").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4,5],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ]
    });

    // $('#save').click(pr.save);

  }

};

pr.init();

</script>

<!--  
1. Tanggal Pembayaran Enable (Bisa di Isi.)
-->