<?php
// if($status=='excell'){
//   $file = "current_stock.xls";
//   header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//   header('Content-Disposition: attachment; filename='.$file);
// }
?>
<style>
/* table, td, th {
    border: 1px solid black;
} */

table.header {
    width: 100%;
    border-collapse: collapse;
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
}
table td{
    padding-left: 5px;
}
table.body {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid black;
}
table.body, td.body {
  border: 1px solid black;
}
</style>
<?php 
// test($detail,0);
?>
<table class="header" id="vtable">
  <thead>
    <tr>
        <td colspan="2"><strong>Souwmpies Corp</strong></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3" align="center"><strong>VOUCHER INVOICE PAYMENT</strong></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%' align="right"><strong>No.</strong></td>
        <td width='20%'>: <?php echo $header->fp_no; ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td align="right"><strong>Tanggal</strong> </td>
        <td>: <?php echo tgl_jurnal($header->fp_date); ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><br/></td>
    </tr>
</table>
<table class="body" id="vtable">
  <thead>
    <tr>
        <td class="body" align="center" width="20%"><strong>No. BG/Cek</strong></td>
        <td class="body" align="center" width="30%"><strong>Keterangan</strong></td>
        <td class="body" colspan="2" align="center"><strong>Jumlah</strong></td>
    </tr>
    <tr>
        <td class="body"></td>
        <td class="body"><?= $detail->db_info; ?></td>
        <td class="body" colspan="2" align="right">Rp. <?= money($detail->db_value); ?></td>
    </tr>
    <tr>
        <td class="body"><br/></td>
        <td class="body"></td>
        <td class="body" colspan="2"></td>
    </tr>
    <tr>
        <td class="body"><br/></td>
        <td class="body"></td>
        <td class="body" colspan="2"></td>
    </tr>
    <tr>
        <td class="body" colspan="2" align="center">Total</td>
        <td class="body" colspan="2" align="right">Rp. <?= money($detail->db_value); ?></td>
    </tr>
    <tr>
        <td  colspan="4"><br></td>
    </tr>
    <tr>
        <td></td>
        <td  colspan="3">Terbilang : <?= terbilang($detail->db_value); ?> Rupiah</td>
    </tr>    
    <tr>
        <td  colspan="4"><br></td>
    </tr>
    <tr>
        <td class="body" rowspan="2" align="center">Kode Perkiraan</td>
        <td class="body" rowspan="2" align="center" >Nama Perkiaan</td>
        <td class="body" colspan="2" align="center">Jumlah</td>
    </tr>
    <tr>
        <td class="body" align="center">Debete</td>
        <td class="body" align="center">Kredit</td>
    </tr>
    <tr>
        <td class="body" align="center"><?= $detail->db_coa_detail; ?></td>
        <td class="body"><?= $detail->db_coa_name; ?></td>
        <td class="body" align="right"><?= money($detail->db_value); ?></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body" align="center"><?= $detail->cr_coa_detail; ?></td>
        <td class="body">&ensp;&ensp;&ensp;&ensp;&ensp;<?= $detail->cr_coa_name; ?></td>
        <td class="body" width="25%"></td>
        <td class="body" align="right" width="25%"><?= money($detail->cr_value); ?></td>
    </tr>
    <tr>
        <td class="body"><br></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body"><br></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body"><br></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body" colspan="2" align="center">Total</td>
        <td class="body" align="right"><?= money($detail->db_value); ?></td>
        <td class="body" align="right"><?= money($detail->cr_value); ?></td>
    </tr>
    <tr>
        <td align="center">Dibuat <br><br><br><br><br><br>( Finance )</td>
        <td align="center" colspan="2">Diperiksa <br><br><br><br><br><br>( Owner )</td>
        <td align="center">Dibekukan <br><br><br><br><br><br>( Accounting )</td>
    </tr>
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>








<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2 style="text-align: center">Sales Payment</h2>
    </div>
</div>
<?php 
if($header->payment_type==1){
  $pembayaran   = 'Cash';
}elseif($header->payment_type==2){
  $pembayaran   = 'Transfer';
}elseif($header->payment_type==3){
  $pembayaran   = 'Giro';
}elseif($header->payment_type==4){
  $pembayaran   = 'Cek';
}
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox">
            <div class="row-form">
              <div class="col-md-12">
                <p><strong></strong></p>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Nomor Pembayaran</strong></p>
              </div>
              <div class="col-md-4">
                <div id="no_pr"><?php echo $header->fp_no; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Nama Company</strong></p>
              </div>
              <div class="col-md-4">
                <div id="company_id"><?php echo $header->company_name; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tanggal</strong></p>
              </div>
              <div class="col-md-4">
                <div id="pr_date"><?php echo tgl_singkat($header->fp_date); ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Customer</strong></p>
              </div>
              <div class="col-md-4">
                <div id="requester"><?php echo $header->fc_name; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tipe Pembayaran</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $pembayaran; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Total Pembayaran</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header->grand_total; ?></div>
              </div>
            </div>
            
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" style="padding-right: 0px !important;padding-left: 0px !important;">
                    <thead>
                      <tr>
                        <th>Nomor Pemesanan</th>
                        <th width="15%">Jumlah</th>
                      </tr>
                      <?php 
                      foreach ($detail as $key => $value) {
                      ?>
                      <tr>
                        <td><?php echo $value->sales_no; ?></td>
                        <td><?php echo number_format($value->total,2); ?></td>
                      </tr>
                      <?php 
                      }
                      ?>
                      <tr>
                        <td align="right" >Total : </td>
                        <td><?php echo number_format($header->grand_total,2); ?></td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <br/><br/>
            <div class="row-form">
              <div class="col-md-8">
                <p><strong></strong></p>
              </div>
              <div class="col-md-2">
                <p><strong>Penerima</strong><br/><br/><br/>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>window.print(); setTimeout(function(){window.close();},500);</script>
