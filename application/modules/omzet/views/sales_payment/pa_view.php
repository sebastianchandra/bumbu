<?php 
// test(,1);
if($this->session->flashdata('alert')!=''){
// if(isset($this->session->flashdata('alert'))){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Customer Payment</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Customer Payment</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_pr(this)">Input</button>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>No. Pembayaran</th>
                    <th>Nama Company</th>
                    <th>Tanggal</th>
                    <th>Nama Customer</th>
                    <th>Tipe Pembayaran</th>
                    <th>Grand Total</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_pa as $key => $value) {
                    // test($value,1);
                    if($value->is_active==1){
                      $status   = 'Proses';
                    }elseif($value->is_active==0){
                      $status   = 'Batal';
                    }

                    if($value->payment_type==1){
                      $pembayaran   = 'Cash';
                    }elseif($value->payment_type==2){
                      $pembayaran   = 'Transfer';
                    }elseif($value->payment_type==3){
                      $pembayaran   = 'Giro';
                    }elseif($value->payment_type==4){
                      $pembayaran   = 'Cek';
                    }
                  ?>
                  <tr>
                      <td>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                      data-id="<?php echo $value->fp_id; ?>" 
                      data-pp="<?php echo $value->fp_no; ?>"><?php echo $value->fp_no; ?></a></strong></td>
                      <!-- <td><?php echo tgl_singkat($value->pr_date); ?></td> -->
                      <td><?php echo $value->company_name; ?></td>
                      <td><?php echo tgl_singkat($value->fp_date); ?></td>
                      <td><?php echo $value->fc_name; ?></td>
                      <td><?php echo $pembayaran; ?></td>
                      <td align="right"><?php echo money($value->grand_total); ?></td>
                      <td><?php echo $status; ?></td>
                      <td align="center">
                        <?php 
                        if($value->is_active==1){
                        ?>
                        <button id="delete" data-pp_no='<?php echo $value->fp_no; ?>' data-pp_id='<?php echo $value->fp_id; ?>' class="btn btn-warning btn-xs" type="submit">Batal</button>
                        <!-- <a target="_blank" href="<?php echo base_url('omzet/sales_payment/view_print/'.$value->fp_id); ?>" class="btn btn-info btn-xs">Print</a> -->
                        <?php 
                        }elseif($value->is_active==0){
                          echo '<button class="btn btn-warning btn-xs" type="submit" disabled="">Batal</button>';
                          // echo '<button class="btn btn-info btn-xs" disabled="">Print</button>';
                        }
                        ?>
                      </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="myPr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Nomor Sales Order</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="form-group row">
              <div class="col-md-3">
                <p>Nama Customer</p>
              </div>
              <div class="col-md-7">
                <select class="form-control" id="fc_id" name="fc_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_customer as $key => $value) {
                    echo "<option value='".$value->fc_id."' data-fc_name='".$value->fc_name."'>".$value->fc_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="table-responsive">              
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th width="10%" align="center">Nomor SO</th>
                    <th align="center">Customer</th>
                    <th align="center">Nilai Invoice</th>
                    <th align="center">Sisa</th>
                    <th align="center">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Next</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Pembayaran Invoice</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Nomor</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_date"></div>
          </div>          
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="supplier_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tipe </strong></p>
          </div>
          <div class="col-md-4">
            <div id="payment_type"></div>
          </div>
        </div>
       
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nomor Pesanan Pembelian</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
// $("#supplier_id").select2();

function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }
  if(values.length>=1){
    $.ajax({
      url: baseUrl+'omzet/sales_payment/pa_selected',
      type : "POST",  
      data: {
        values      : values
      },
      success : function(resp){        
        setTimeout(function () {
          window.location.href = baseUrl+'omzet/sales_payment/pa_input'; 
        }, 100);
      }
    });
  }else{
    toastr.warning("Pilih Nomor Sales Order.", 'Alert', {"positionClass": "toast-top-center"});
    return false;
  }
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});


function show_pr(e){  
  // $.get({
  //   url: baseUrl + 'transaction/po_payment/view_po/',
  //   success: function(resp){

  //     popup_pr.clear().draw();
  //     var dataSrc = JSON.parse(resp.detail);                 
  //     popup_pr.rows.add(dataSrc).draw(false);
  //   }
  // });

  $('#myPr').modal('show'); 
}


$('#fc_id').change(function(e){
  var fc_id   = $('#fc_id').val();
  $.get({
    url: baseUrl + 'omzet/sales_payment/view_inv/'+fc_id,
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });
})
// function show_po(e){
//   alert('tes')
// }

function show_detail(e){
  var idPa     = $(e).data('id');
  var noPa     = $(e).data('pp');

  $.get({
    url: baseUrl + 'omzet/sales_payment/view_popup/'+idPa,
    success: function(resp){
      var type_payment;
      if(resp.header.payment_type=='1'){
        type_payment   = 'Cash';
      }else if(resp.header.payment_type=='2'){
        type_payment   = 'Transfer';
      }else if(resp.header.payment_type=='3'){
        type_payment   = 'Giro';
      }else if(resp.header.payment_type=='4'){
        type_payment   = 'Cek';
      }
      $('#pp_no').text(resp.header.fp_no);
      $('#pp_date').text(tanggal_indonesia(resp.header.fp_date));
      $('#supplier_name').text(resp.header.fc_name);
      $('#payment_type').text(type_payment);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pp_id  = $(this).data('pp_id');
    var pp_no  = $(this).data('pp_no');

    toastr.warning(
      'Apakah anda Ingin Membatalkan Nomor Pembayaran '+pp_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pp_no="'+pp_no+'" data-pp_id="'+pp_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var pp_id  = $(e).data('pp_id');
  var pp_no  = $(e).data('pp_no');

  $.ajax({
    data: {
      pp_id  : pp_id,
      pp_no  : pp_no
    },
    type : "POST",
    url: baseUrl+'omzet/sales_payment/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("Data tidak berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'omzet/sales_payment'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!--  
1. Penambahan Reject Untuk pembayaran PO
2. Penambahan Popup untuk detail dan header pembayaran.
-->