<?php if (isset($_SESSION['alert'])): ?>
  <script type="text/javascript">
    Command: toastr["info"]("<?php echo $_SESSION['alert']; ?>", "Alert",{
      "closeButton": true,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "5000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "5000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }).css("width","750px");
  </script>
<?php endif; ?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Delivery Order</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Delivery Order</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
        </nav>
      </div>
    </div>
  </div>
</div>

<?php echo form_open(base_url()."omzet/delivery_order/process_do_act"/*, array('id' => 'bform')*/); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal" method="post" action="<?php echo base_url().'stok/incoming_stok/form_act'; ?>">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Referensi</p>
              </div>
              <div class="col-md-5">
                <?= $header_so->sales_no; ?>
                <input class="form-control" type="hidden" id="sales_no" name="sales_no" value="<?= $header_so->sales_no; ?>">
                <input class="form-control" type="hidden" id="sales_id" name="sales_id" value="<?= $header_so->sales_id; ?>">
                <input type="hidden" id="tdetail" name="tdetail">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="do_date" name="do_date" value="<?php echo substr(dbnow(),0,10); ?>">
                <!-- <input type="hidden" class="form-control"  value="<?php echo substr(dbnow(),0,10); ?>"> -->
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Gudang</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="warehouse_id" name="warehouse_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' >".$value->warehouse_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail_popup">
                    <thead>
                      <tr>
                        <th>Nama Barang</th>
                        <th width="15%">SO Qty</th>
                        <th width="15%">DO Qty</th>
                        <th width="15%">Current Qty</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $max    = 0;
                      foreach ($detail_so as $key => $value) {
                      $max    = $value->qty_so-$value->qty_do;
                      ?>
                        <tr>
                          <td>
                            <?= $value->items_name; ?>
                            <input class="form-control" type="hidden" placeholder="Current Qty" name="items_name[]" id="items_name" value="<?php echo $value->items_name; ?>">
                            <input class="form-control" type="hidden" placeholder="Current Qty" name="items_id[]" id="items_id" value="<?php echo $value->items_id; ?>">
                            <input class="form-control" type="hidden" placeholder="Remarks" id="qty_so" name="qty_so[]" value="<?php echo $value->qty_so; ?>">
                            <input class="form-control" type="hidden" placeholder="Remarks" id="max" name="max[]" value="<?php echo $max; ?>">
                            <input class="form-control" type="hidden" placeholder="Remarks" id="status" name="status[]" value="<?php echo $value->status; ?>">
                          </td>
                          <td><?= $value->qty_so ?> <?= ($value->status=='BONUS')? '(BONUS)' : ''; ?></td>
                          <td><?= $value->qty_do ?></td>
                          <td>
                            <input class="form-control" type="number" placeholder="Current Qty" id="current" name="current[]" min="0" max="<?php echo $max; ?>" value="<?php echo $max; ?>">
                          </td>
                        </tr>
                      <?php
                      }
                      ?>                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm" type="submit" id='bform'><i class="fa fa-check"></i> Simpan</button>
                <!-- <button class="btn btn-primary btn-sm" type="submit" id="save">Save</button> -->
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('omzet/delivery_order'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>
<!-- </form> -->

<script type="text/javascript">
// $("#cek_warehouse").hide();
// $("#cek_project").hide();

$("#warehouse_id").select2();

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

$('#bform').click(function(){

  if(!$('#warehouse_id').val()){
    toastr.error("<strong>Gudang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#warehouse_id').select2('open');
    return false;
  }

  if(!$('#do_date').val()){
    toastr.error("<strong>Tanggal</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#do_date').focus();
    return false;
  }

  let myUrl = baseUrl+'omzet/Delivery_order/cek_stok';
  
    // alert(JSON.stringify(sales_detail_arr));
  $.ajax({
    url: myUrl,
    type : "POST",  
    // dataType : "json",
    data: {
      sales_no          : $('#sales_no').val(),
      sales_id          : $('#sales_id').val(),
      do_date           : $('#do_date').val(),
      warehouse_id      : $('#warehouse_id').val(),
      remarks           : $('#remarks').val(),
      current           : $("input[name='current[]']").map(function(){return $(this).val();}).get(),
      items_id          : $("input[name='items_id[]']").map(function(){return $(this).val();}).get(),
      items_name        : $("input[name='items_name[]']").map(function(){return $(this).val();}).get(),
      qty_so            : $("input[name='qty_so[]']").map(function(){return $(this).val();}).get(),
      max               : $("input[name='max[]']").map(function(){return $(this).val();}).get()

    },
    success : function(resp){
      // debugger
      var no = 0;
      if(resp.keterangan == 'Stok Kurang') {
        var error_message = '<strong>Terdapat Barang Yang Tidak Ada Stok. Nama Barang :</strong><br>';
        error_message += "<hr>";

        for (i = 0; i < resp.notif.length; i++) {
          no = no+1;
          error_message += no+". " + resp.notif[i].items_name + "<br>";
          // error_message += "<hr>";
        }

        toastr.warning(error_message, 'Alert', {"positionClass": "toast-top-center"});
        return false;

      }else{
        $('#bform').prop("disabled",true);
        var error_message = resp.flash;
        toastr.info(error_message, 'Info', {"positionClass": "toast-top-center"});
        setTimeout(function () {
          window.location.href = baseUrl+'omzet/delivery_order/'; 
        }, 7000);

      }    
    }

  });

  return false;
});

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    
    $("#warehouse_project").select2();
    $("#items_unit").select2();
    $("#referensi_id").select2().on('select2:select', function (e) {

      var idDoc     = $('#referensi_id option:selected').attr('data-doc_id');
      var noDoc     = $('#referensi_id option:selected').attr('data-doc_no');

      $.get({
        url: baseUrl + 'stok/incoming_stok/show_detail/'+noDoc+'/'+idDoc,
        success: function(resp){

          detail_popup.clear().draw();
          var dataSrc = JSON.parse(resp.detail);                 
          detail_popup.rows.add(dataSrc).draw(false);
          $("#tdetail").val(resp.tdetail);
          $("#remarks").val(resp.theader.remarks);
        }
      });

      $("#no_referensi").val(noDoc);
    });
    $("#supplier_id").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'item_id'},
        { data: 'item_name'}, 
        { data: 'item_qty', className: "text-right"}, 
        { data: 'item_info',"visible": false}, 
        { data: 'act', className: "text-center" }
      ],
    });

    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  }

};

pr.init();

</script>