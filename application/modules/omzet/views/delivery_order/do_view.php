<?php if (isset($_SESSION['alert'])): ?>
  <script type="text/javascript">
    Command: toastr["info"]("<?php echo $_SESSION['alert']; ?>", "Alert",{
      "closeButton": true,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "5000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "5000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }).css("width","750px");
  </script>
<?php endif; ?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Delivery Order</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Delivery Order</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_do(this)">Input</button>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="10%">No</th>
                    <th width="10%">No Ref</th>
                    <th>Tanggal</th>
                    <!-- <th>Project Name</th> -->
                    <!-- <th>Nama Customer</th> -->
                    <!-- <th>Supplier Name</th> -->
                    <!-- <th>Warehouse</th> -->
                    <th>Status</th>
                    <th width="15%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_sales as $key => $value) {
                  ?>
                  <tr>
                    <td>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                    data-id="<?php echo $value->do_id; ?>" 
                    data-do="<?php echo $value->do_no; ?>"><?php echo $value->do_no; ?></a></strong></td>
                    <td><?php echo $value->doc_ref; ?></td>
                    <td><?php echo tgl_singkat($value->do_date); ?></td>
                    <!-- <td><?php #echo $value->project_name; ?></td> -->
                    <!-- <td><?php echo $value->fc_name; ?></td> -->
                    <!-- <td><?php echo $value->supplier_name; ?></td> -->
                    <!-- <td><?php echo $value->warehouse_name; ?></td> -->
                    <td><?php echo $value->do_status; ?></td>
                    <td align="center">
                      <?php 
                      if($value->do_status =='Close'){
                        echo '<a target="_blank" href="'.base_url('omzet/delivery_order/view_print/'.$value->do_id).'" class="btn btn-info btn-xs">Print</a>';
                        ?>
                        <button id="reject" data-do_no='<?php echo $value->do_no; ?>' data-do_id='<?php echo $value->do_id; ?>' class="btn btn-warning btn-xs" type="submit">Reject</button>
                        <?php
                      }else{
                        ?>
                        <button class="btn btn-info btn-xs" disabled="">Print</button>
                        <button class="btn btn-warning btn-xs" disabled="">Reject</button>
                      <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Penjualan</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>No Dok</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_id"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_date"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="fc_name"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-4">
            <div id="remarks"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th width="15%">Qty</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myPr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Nomor Permintaan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th>SO No</th>
                    <th>SO Status</th>
                    <th width="15%">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<script>
var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_do(e){  
  $.get({
    url: baseUrl + 'omzet/delivery_order/view_so/',
    success: function(resp){
      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });

  $('#myPr').modal('show'); 
}

function show_detail(e){
  var doId     = $(e).data('id');
  var doNo     = $(e).data('do');
  
  var myUrl = baseUrl + 'omzet/delivery_order/view_popup/'+doId; 
  // alert(myUrl);
  $.get({
    url: myUrl,
    success: function(resp){
      $('#company_id').text(resp.header.company_name);
      $('#sales_no').text(resp.header.do_no);
      $('#sales_date').text(tanggal_indonesia(resp.header.do_date));
      $('#fc_name').text(resp.header.fc_name);
      $('#remarks').text(resp.header.remarks);
      $('#status').text(resp.header.do_status);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}
$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pr_id  = $(this).data('pr_id');

    toastr.warning(
      'Apakah Anda ingin menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pr_id="'+pr_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });

  $('#vtable').on('click','#reject', function(e){

    var do_id  = $(this).data('do_id');
    var do_no  = $(this).data('do_no');

    toastr.warning(
      'Apakah anda ingin menolak '+do_no+'?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return rejectRow(this)" data-do_id="'+do_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });
});

function rejectRow(e){
  var do_id  = $(e).data('do_id');

  $.ajax({
    data: {
      do_id  : do_id
    },
    type : "POST",
    url: baseUrl+'omzet/Delivery_order/reject_js',
    success : function(resp){
      
      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Tolak');
        return false;

      } else {
        toastr.success("Data Berhasil Di Tolak.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'omzet/delivery_order'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function deleteRow(e){
  var pr_id  = $(e).data('pr_id');

  $.ajax({
    data: {
      pr_id  : pr_id
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_requisition/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        // setTimeout(function () {
        //   window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
        // }, 2000);
      }
    }
  });
}
</script>




