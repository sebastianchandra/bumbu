<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2 style="text-align: center">Delivery Order</h2>
    </div>
</div>
<?php 
// test($header,1);
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox">
            <div class="row-form">
              <div class="col-md-12">
                <p><strong></strong></p>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Nomor Delivery Order</strong></p>
              </div>
              <div class="col-md-4">
                <div id="no_pr"><?php echo $header['do_no']; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Nama Company</strong></p>
              </div>
              <div class="col-md-4">
                <div id="company_id"><?php echo $header['company_name']; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tanggal</strong></p>
              </div>
              <div class="col-md-4">
                <div id="pr_date"><?php echo tgl_singkat($header['do_date']); ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Customer</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header['fc_name']; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" style="padding-right: 0px !important;padding-left: 0px !important;">
                    <thead>
                      <tr>
                        <th width="8%">No</th>
                        <th width="10%">Kode Barang</th>
                        <th>Nama Barang</th>
                        <th width="15%">Qty</th>
                      </tr>
                      <?php 
                      $no     = 0;
                      foreach ($detail as $key => $value) {
                      $no     = $no+1;
                      ?>
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $value->items_code; ?></td>
                        <td><?php echo $value->items_name; ?></td>
                        <td align="right"><?php echo number_format($value->qty,2); ?></td>
                      </tr>
                      <?php 
                      }
                      ?>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <br/><br/>
            <div class="row-form">
              <div class="col-md-8">
                <p><strong></strong></p>
              </div>
              <div class="col-md-2">
                <p><strong>Pengirim</strong><br/><br/><br/>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
              </div>
              <div class="col-md-2">
                <p><strong>Penerima</strong><br/><br/><br/>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>window.print(); setTimeout(function(){window.close();},500);</script>
