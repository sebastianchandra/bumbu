<?php 
// test($new_sales,1);
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Penjualan</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Penjualan</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<?php 
// test($new_sales['sales'],0);
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <!-- <form class="form-horizontal">       -->
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>

                <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_sales['sales']); ?>' required />
                <input type="hidden" name="items" id="tdiskon_val" value='0'/>
                <input type="hidden" name="items" id="tsubtotal_val" value='0'/>

              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="sales_date" value="<?php echo substr(dbnow(),0,10); ?>" >
              </div>
            </div>

            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Warehouse</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="warehouse_id" name="warehouse_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' data-project_name='".$value->warehouse_name."'>".$value->warehouse_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div> -->

            <div class="form-group row">
              <div class="col-md-2">
                <p>Customer</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="customer_id" name="customer_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_customer as $key => $value) {
                    echo "<option value='".$value['fc_id']."' data-customer_name='".$value['fc_name']."'>".$value['fc_name']."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-2">
                <p>Diskon</p>
              </div>
              <div class="col-md-1" id="id_diskon_persen">
                <input type="text" class="form-control price" id="disc_percent" placeholder="%">
              </div>
              <div class="col-md-3" id="id_diskon_value">
                <input type="text" class="form-control price" id="disc_value" placeholder="Nilai Diskon (Rp)">
              </div>
            </div>

            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Tipe Customer</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="customer_type" name="customer_type">
                  <option value="">Select</option>
                  <option value="Owner">Owner</option>
                  <option value="Customer">Customer</option>
                  <option value="Pemerintah">Pemerintah</option>
                  
                </select>
              </div>
            </div> -->
  
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>


            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama</p>
              </div>
              <div class="col-md-7">
                <select class="form-control" id="item_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_items as $key => $value) {
                    echo '<option value="'.$value->items_id.'" data-name="'.$value->items_name.'" data-stok="'.$value->current_stock.'" data-price_sell="'.$value->price_sell.'">'.$value->items_name.'</option>';
                  }
                  ?>
                </select>
                <input type="hidden" class="form-control " id="id" name="id" value="0"/>
              </div>
            </div>
      
            <div class="form-group row">
              <div class="col-md-2">
                <p>Quantity</p>
              </div>
              <div class="col-md-2">
                <input type="text" class="form-control price" id="qty">
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-2">
                <p>Harga Jual</p>
              </div>
              <div class="col-md-2">
                <input type="text" class="form-control price" id="price">
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-2">
                <p>Diskon Per Quantity</p>
              </div>
              <div class="col-md-1" id="id_diskon_persen_qty">
                <input type="text" class="form-control price" id="disc_percent_qty" placeholder="%">
              </div>
              <div class="col-md-3" id="id_diskon_value_qty">
                <input type="text" class="form-control price" id="disc_value_qty" placeholder="Nilai Diskon (Rp)">
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-2">
                <p><!-- Info --></p>
              </div>
              <div class="col-md-5">
                <input type="hidden" class="form-control" id="item_info">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-5">
                <button type="button" class="btn btn-primary btn-sm" id="add-items">Tambah Data</button>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th>Nama</th>
                        <th width="8%">Qty</th>
                        <th width="10%">Diskon</th>
                        <th width="10%">Harga</th>
                        <th width="10%">Subtotal</th>
                        <!-- <th width="10%">Subtotal</th> -->
                        <th width="5%">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <td></td>
                        <td></td>
                        <td colspan="3" class="col_right">Total</td>
                        <td colspan="2" class="col_right" id="tsubtotal"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td colspan="3" class="col_right">Total Diskon (Header + Detail)</td>
                        <td colspan="2" class="col_right" id="tdiskon"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td colspan="3" class="col_right">Total Bayar</td>
                        <td colspan="2" class="col_right" id="total"></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <!-- <button type="button" class="btn btn-primary btn-sm" id="cek-data">Cek Data</button> -->
                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('omzet/sales'); ?>">Batal</a>
              </div>
            </div>
          </div>
        <!-- </form> -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$("#requester").focus();
$("#item_id").select2().on('select2:select',function(e){
  $('#price').val($('#item_id option:selected').attr('data-price_sell'));
  $("#qty").focus();
});
$("#customer_id").select2();

$('#id_diskon_value').click(function(){
  $('#disc_percent').attr('disabled', true); 
  $('#disc_percent').val('0'); 
  $('#disc_value').attr('disabled', false); 
});

$('#id_diskon_persen').click(function(){
  $('#disc_value').attr('disabled', true); 
  $('#disc_value').val('0'); 
  $('#disc_percent').attr('disabled', false); 
});

$('#id_diskon_value_qty').click(function(){
  $('#disc_percent_qty').attr('disabled', true); 
  $('#disc_percent_qty').val('0');
  $('#disc_value_qty').attr('disabled', false); 
});

$('#id_diskon_persen_qty').click(function(){
  $('#disc_value_qty').attr('disabled', true); 
  $('#disc_value_qty').val('0');
  $('#disc_percent_qty').attr('disabled', false);   
});

sales = {
  data: {},
  processed: false,
  items: [],
  sales_detail: [],


  init: function(){

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4,5,6],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'item_id'},
        { data: 'item_name'}, 
        // { data: 'grade'}, 
        { data: 'qty', className: "col_right"}, 
        // { data: 'price',"visible": false}, 
        { data: 'diskon_detail', className: "col_right"}, 
        { data: 'price', className: "col_right"}, 
        { data: 'subTotal', className: "col_right"}, 
        { data: 'act', className: "col_right" }
      ],
    });

    this._set_items($('#sup_items').val());
    $('#add-items').click(sales.add_items);
    $('#save').click(sales.save);
    
    // $('#cek-data').click(sales.getSalesItems);

  },


  add_items: function(e){
    // debugger
    e.preventDefault();

    if($('#item_id').val()==''){
      toastr.error("<strong>Items Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
      $('#item_id').select2('open');
      return false;
    }

    if(!$('#qty').val()){
      toastr.error("<strong>Qty</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
      $("#qty").focus();
      return false;
    } 

    if(!$('#price').val()){
      toastr.error("<strong>Harga</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#price").focus();
      return false;
    }

    // debugger

    let item_id               = $('#item_id').val();
    let item_name             = $('#item_id option:selected').attr('data-name');
    let qty                   = $('#qty').val();
    let price                 = $('#price').val().replace(/\,/g, '');
    let tSubTotal             = (qty * price).toFixed(2);
    let id_diskon_value_qty   = $('#disc_value_qty').val().replace(/\,/g, '');
    let id_diskon_persen_qty  = $('#disc_percent_qty').val().replace(/\,/g, '');
    let id_diskon_value       = parseFloat($('#disc_value').val().replace(/\,/g, ''));
    let id_diskon_persen      = $('#disc_percent').val().replace(/\,/g, '');
    let diskon_detail         = 0;
    let diskon_header         = 0;
    // debugger
    if(id_diskon_value_qty>0){
      diskon_detail     = parseFloat(id_diskon_value_qty);
    }else if(id_diskon_persen_qty>0){
      diskon_detail     = (parseFloat(price.replace(/\,/g, ''))*id_diskon_persen_qty)/100;
    }
    let subTotal              = tSubTotal-(diskon_detail*qty);

    // Menghitung Subtotal bersih
    let subtotal_bersih       = $('#tsubtotal_val').val();
    let total_subtotal        =  parseFloat(subtotal_bersih) +  parseFloat(tSubTotal);
    subtotal_bersih       = subtotal_bersih + subTotal;
    $('#tsubtotal_val').val(total_subtotal);
    $('#tsubtotal').text(numberWithCommas(total_subtotal));

    if(id_diskon_value>0){
      if($('#tdiskon').text().replace(/\,/g, '')>0){

      }else{
      diskon_header     = id_diskon_value;
      }
    }else if(id_diskon_persen>0){
      diskon_header     = (subTotal*id_diskon_persen)/100;
    }

    // Menghitung Total Diskon
    let tdiskon               = parseFloat($('#tdiskon_val').val());
    let txtTotalDiskon        = tdiskon+(diskon_detail*qty)+diskon_header;
    $('#tdiskon_val').val(txtTotalDiskon);
    $('#tdiskon').text(numberWithCommas(txtTotalDiskon));
    
    // Menghitung Total Bayar
    // let ttotal                = parseFloat($('#ttotal_val').val());
    ttotal                    = total_subtotal-txtTotalDiskon;
    $('#total').text(numberWithCommas(ttotal));

    let id        = parseInt($('#id').val());
    var id_det    = id + 1;

    if(item_id){
    data = {
      det_id : id_det,
      item_id : item_id,
      item_name : item_name,
      qty : numberWithCommas(qty),
      price : numberWithCommas(price),
      subTotal : numberWithCommas(tSubTotal),
      diskon_detail : numberWithCommas(diskon_detail),
      id_diskon_value : id_diskon_value_qty,
      id_diskon_persen: id_diskon_persen_qty
    };

    // debugger

    sales._addtogrid(data);
    sales._clearitem();
    sales._focus();

    }
  },

  _focus: function(){
    $('#item_id').select2('open');
  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = sales.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    data.act = '<button det-id="'+data.item_id+'" det-disc_detail="'+data.diskon_detail+'" det-qty="'+data.qty+'" det-price="'+data.price+'" det-tsubdetail="'+data.subTotal+'" onclick="return sales._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;

    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      toastr.error("<strong>Items Name</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'omzet/Sales/add_item',
      data: {
        det_id    : data.det_id,
        item_id   : data.item_id,
        item_name : data.item_name,
        qty  : data.qty.replace(/\,/g, ''),
        price     : data.price.replace(/\,/g, ''),
        subTotal     : data.subTotal.replace(/\,/g, ''),
        diskon_detail : data.diskon_detail.replace(/\,/g, ''),
        id_diskon_value : data.id_diskon_value,
        id_diskon_persen: data.id_diskon_persen
      }
    });
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    var ddisk     = 0;
    var dtotal     = 0;
    var dbayar     = 0;
    items.map(function(i,e){
      // debugger
      var subtotal  = (i.price*i.qty).toFixed(2);
      var data = {
        det_id    : i.det_id,
        item_id   : i.item_id,
        item_name : i.item_name,
        qty  : numberWithCommas(i.qty),
        price : numberWithCommas(i.price),
        subTotal : numberWithCommas(subtotal),
        diskon_detail : numberWithCommas(i.diskon_detail)
      };
      
      dtotal    = (parseFloat(dtotal) + parseFloat(subtotal)).toFixed(2);
      ddisk     = parseFloat(ddisk) + parseFloat(i.diskon_detail);
      
      sales._addtogrid(data);
      sales._clearitem();
    });

    dbayar      = dtotal - ddisk;
    // debugger
    $('#tsubtotal').text(numberWithCommas(dtotal));
    $('#tdiskon').text(numberWithCommas(ddisk));
    $('#total').text(numberWithCommas(dbayar));
    $('#tsubtotal_val').val(dtotal);
    $('#tdiskon_val').val(ddisk);
    
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#qty').val('');
    $('#price').val('');
    $('#disc_value_qty').val('');
    $('#disc_percent_qty').val('');
  },

  _removefromgrid: function(el){
    let tsubtotal     = $('#tsubtotal').text().replace(/\,/g, '');
    let tdiskon       = $('#tdiskon').text().replace(/\,/g, '');
    let disc_percent  = $('#disc_percent').val();
    let disc_value    = $('#disc_value').val();

    let tsubdetail    = $(el).attr('det-tsubdetail').replace(/\,/g, '');
    if(disc_percent>0){
      disc_header     = (tsubdetail*disc_percent)/100
    }else if(disc_value>0){
      disc_header     = disc_value;
    }else{
      disc_header     = 0;
    }

    let vTotalHeader  = tsubtotal-disc_header;

    let id            = $(el).attr('det-id');
    let disc_detail   = $(el).attr('det-disc_detail').replace(/\,/g, '');
    let qty           = $(el).attr('det-qty').replace(/\,/g, '');
    let price         = $(el).attr('det-price').replace(/\,/g, '');

    let vDiscDetail   = disc_detail*qty;
    let txtSubTotal   = (tsubtotal - tsubdetail).toFixed(2);;
    $('#tsubtotal').text(numberWithCommas(txtSubTotal));
    $('#tsubtotal_val').val(txtSubTotal);

    let txtDiskon     = tdiskon - vDiscDetail - disc_header;
    $('#tdiskon').text(numberWithCommas(txtDiskon));
    $('#tdiskon_val').val(txtDiskon);

    let txtTotal      = txtSubTotal - txtDiskon;
    $('#total').text(numberWithCommas(txtTotal));

    sales.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'omzet/sales/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },


  /* START GET DATA HEADER */
  // getSalesHeader : function(e)
  // {
  //   var salesHeaderArr = [];
  //   salesHeaderArr.push ({
  //       company_id        : $('#company_id').val(),
  //       company_name      : $('#company_id').attr('data-company_id'),
  //       sales_date        : $('#sales_date').val(),
  //       requester         : $('#requester').val(),
  //       project_id        : $('#project_id').val(),
  //       project_name      : $('#project_id option:selected').attr('data-project_name'),
  //       customer_id       : $('#customer_id').val(),
  //       customer_name     : $('#customer_id option:selected').attr('data-customer_name'),
  //       remarks           : $('#remarks').val()
  //   });

  //   return salesHeaderArr;
  // }
  /* END GET DATA HEADER */

  /* START GET DATA DETAIL FROM TABLE */
  getSalesItems : function (e)
  {
    var salesItemsArr = [];
    var salesTable = $('#detail').DataTable().data()
    var dataCount = salesTable.count();
    // var dataCount = this.grids.data().count();
    for (var i = 0; i < dataCount; i++) {
      // debugger
      var dataRow = $('#detail').DataTable().row(i).data();

      tQty      = dataRow['qty'];
      tPrice    = dataRow['price'];
      tSubTotal = dataRow['subTotal'];
      tQty      = tQty.replace(/\,/g, '');
      tPrice    = tPrice.replace(/\,/g, '');
      tSubTotal = tSubTotal;

      salesItemsArr.push ({
        item_id    : dataRow['item_id'],  
        item_name  : dataRow['item_name'],  
        qty        : tQty,  
        price      : tPrice,  
        subTotal   : tSubTotal.replace(/\,/g, '') 
      });
      // alert(dataRow['subTotal']);         
    } 
    // alert(JSON.stringify(salesItemsArr));
    return salesItemsArr;
    // alert(JSON.parse(salesItemsArr));
  },
    
  /* END GET DATA DETAIL FROM TABLE */

  save: function(e){
    e.preventDefault();
    // if(!$('#warehouse_id').val()){
    //   toastr.error("<strong>Warehouse Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    //   $('#warehouse_id').select2('open');
    //   return false;
    // }

    if(!$('#customer_id').val()){
      toastr.error("<strong>Customer Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#customer_id').select2('open');
      return false;
    }
    
    if($('#detail').DataTable().data().count()<1){
      toastr.error("<strong>Detail Barang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      
      return false;
    }

    $('#save').prop("disabled",true);
    // let sales_detail_arr = sales.getSalesItems();
    // sales_detail_arr = sales.getSalesItems();
    let myUrl = baseUrl+'omzet/Sales/form_act';
    // alert(JSON.stringify(sales_detail_arr));
    $.ajax({
      url: myUrl,
      type : "POST",  
      // dataType : "json",
      data: {
        company_id        : $('#company_id').attr('data-company_id'),
        company_name      : $('#company_id').val(),
        sales_date        : $('#sales_date').val(),
        requester         : $('#requester').val(),
        warehouse_id      : '4',
        warehouse_name    : '',
        customer_type     : $('#customer_type').val(),
        customer_id       : $('#customer_id').val(),
        customer_name     : $('#customer_id option:selected').attr('data-customer_name'),
        remarks           : $('#remarks').val(),
        disc_percent      : $('#disc_percent').val().replace(/\,/g, ''),
        disc_value        : $('#disc_value').val().replace(/\,/g, ''),
        // sales_header      : sales.getSalesHeader(),  
        sales_detail      : sales.getSalesItems()

      },
      success : function(resp){
      // success : function(data){
        debugger

        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else {
          messages = 'Data Berhasil di Simpan dengan Nomor Dokumen '+resp.status;
          messages += "<hr>";
          toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});

          setTimeout(function () {
            window.location.href = baseUrl+'omzet/Sales/'; 
          }, 2000);
        }

      }
      
    });

  }

};

sales.init();

</script>