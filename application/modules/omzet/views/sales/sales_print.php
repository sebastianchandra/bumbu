<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2 style="text-align: center">Pesanan Penjualan</h2>
    </div>
</div>
<?php 
// test($detail,1);
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox">
            <div class="row-form">
              <div class="col-md-12">
                <p><strong></strong></p>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Nomor Penjualan</strong></p>
              </div>
              <div class="col-md-4">
                <div id="no_pr"><?php echo $header->sales_no; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Nama Company</strong></p>
              </div>
              <div class="col-md-4">
                <div id="company_id"><?php echo $header->company_name; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tanggal</strong></p>
              </div>
              <div class="col-md-4">
                <div id="pr_date"><?php echo tgl_singkat($header->sales_date); ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Customer</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header->fc_name; ?></div>
              </div>
            </div>
            <!-- <div class="row-form">
              <div class="col-md-2">
                <p><strong>Alamat</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header->delivery_address; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Total</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header->total; ?></div>
              </div>
            </div> -->
            <!-- <div class="row-form">
              
            </div> -->
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" style="padding-right: 0px !important;padding-left: 0px !important;">
                    <thead>
                      <tr>
                        <th width="8%">No</th>
                        <th width="10%">Kode Barang</th>
                        <th>Nama Barang</th>
                        <th width="15%">Qty</th>
                        <th width="12%">Price</th>
                        <th width="12%">Potongan</th>
                        <th width="13%">Sub Total</th>
                      </tr>
                      <?php 
                      $no               = 0;
                      $total            = 0;
                      $tpot_detail      = 0;
                      $pot_detail       = 0;
                      $subtotal         = 0;
                      foreach ($detail as $key => $value) {
                      $no     = $no+1;

                      if($value->disc_value>=1){
                        $tpot_detail  = $value->disc_value+$tpot_detail;
                        $pot_detail   = $value->disc_value;
                      }else{
                        $tpot_detail  = ((($value->qty*$value->cust_price)*$value->disc_percent)/100)+$tpot_detail;
                        $pot_detail   = ((($value->qty*$value->cust_price)*$value->disc_percent)/100);
                      }

                      $subtotal       = $value->subtotal+$subtotal;
                      ?>
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $value->items_code; ?></td>
                        <td><?php echo $value->items_name; ?></td>
                        <td align="right"><?= ($value->status=='03')? '(Bonus)' : ''; ?> <?php echo number_format($value->qty,2); ?></td>
                        <td align="right"><?php echo ($value->cust_price!='0') ? number_format($value->cust_price,2) : ''; ?></td>
                        <td align="right"><?php echo ($pot_detail!='0') ? number_format($pot_detail,2) : ''; ?></td>
                        <td align="right"><?php echo ($value->subtotal!='0') ? number_format($value->subtotal,2) : ''; ?></td>
                      </tr>
                      <?php 
                      }
                      // test($header,1);
                      if($header->disc_value>=1){
                        $pot_header       = $header->disc_value;
                      }else{
                        $pot_header       = ((($subtotal-$tpot_detail)*$header->disc_percent)/100);
                      }
                      
                      $tpotongan          = $pot_header+$tpot_detail;
                      $total_bayar        = $subtotal-$tpotongan;
                      ?>
                      <tr>
                        <td align="right" colspan="6">Subtotal : </td>
                        <td align="right"><?php echo number_format($subtotal,2); ?></td>
                      </tr>
                      <tr>
                        <td align="right" colspan="6">Total Potongan (Potongan Header Dan Detail) : </td>
                        <td align="right"><?php echo number_format($tpotongan,2); ?></td>
                      </tr>
                      <tr>
                        <td align="right" colspan="6">Total : </td>
                        <td align="right"><?php echo number_format($total_bayar,2); ?></td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <br/><br/>
            <div class="row-form">
              <div class="col-md-8">
                <p><strong></strong></p>
              </div>
              <div class="col-md-2">
                <p><strong>Pemesan</strong><br/><br/><br/>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>window.print(); setTimeout(function(){window.close();},500);</script>
