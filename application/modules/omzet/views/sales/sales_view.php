<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Omzet</li>
                  <li class="breadcrumb-item">Penjualan</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Penjualan</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a href="<?php echo base_url('omzet/Sales/form'); ?>" class="btn btn-primary">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="10%">No</th>
                    <th>Tanggal</th>
                    <!-- <th>Project Name</th> -->
                    <th>Nama Customer</th>
                    <!-- <th>Supplier Name</th> -->
                    <!-- <th>Warehouse</th> -->
                    <th>Status</th>
                    <th width="20%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_sales as $key => $value) {
                  ?>
                  <tr>
                    <td>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                    data-id="<?php echo $value->sales_id; ?>" 
                    data-sales="<?php echo $value->sales_no; ?>"><?php echo $value->sales_no; ?></a></strong></td>
                    <td><?php echo tgl_singkat($value->sales_date); ?></td>
                    <!-- <td><?php #echo $value->project_name; ?></td> -->
                    <td><?php echo $value->fc_name; ?></td>
                    <!-- <td><?php echo $value->supplier_name; ?></td> -->
                    <!-- <td><?php echo $value->warehouse_name; ?></td> -->
                    <td><?php echo $value->doc_status; ?></td>
                    <td align="center">
                      <?php 
                      if($value->doc_status =='Reject'){
                        echo '<button class="btn btn-info btn-xs" disabled="">Print</button>';
                        echo ' <a class="btn btn-danger btn-xs" href="'.base_url().'omzet/sales/copy/'.$value->sales_id.'">Copy</a>';
                        echo ' <button class="btn btn-success btn-xs" disabled="">Edit</button>';
                        echo ' <button class="btn btn-warning btn-xs" disabled="">Reject</button>';
                      }elseif($value->doc_status =='Close'){
                        echo '<a target="_blank" href="'.base_url('omzet/sales/view_print/'.$value->sales_id).'" class="btn btn-info btn-xs">Print</a> ';
                        echo ' <button class="btn btn-danger btn-xs" disabled="">Copy</button>';
                        echo ' <button class="btn btn-success btn-xs" disabled="">Edit</button>';
                        echo ' <button class="btn btn-warning btn-xs" disabled="">Reject</button>'; 
                      }else{
                        echo '<a target="_blank" href="'.base_url('omzet/sales/view_print/'.$value->sales_id).'" class="btn btn-info btn-xs">Print</a> ';
                        echo ' <button class="btn btn-danger btn-xs" disabled="">Copy</button>';
                        echo ' <a href="'.base_url('omzet/sales/edit/'.$value->sales_id).'" class="btn btn-success  btn-xs">Edit</a>';
                      ?>
                      <button id="reject" data-sales_no='<?php echo $value->sales_no; ?>' data-sales_id='<?php echo $value->sales_id; ?>' class="btn btn-warning btn-xs" type="submit">Reject</button>
                      <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Penjualan</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>No Dok</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_id"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="sales_date"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="fc_name"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-4">
            <div id="remarks"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th width="15%">Qty</th>
                    <th width="15%">Harga</th>
                    <th width="15%">Potongan</th>
                    <th width="15%">Sub Total</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1,2,3,4 ] 
    }
  ]
});

function show_detail(e){
  var salesId     = $(e).data('id');
  var salesNo     = $(e).data('sales');
  
  var myUrl = baseUrl + 'omzet/Sales/view_popup/'+salesId; 
  // alert(myUrl);
  $.get({
    url: myUrl,
    success: function(resp){
      $('#company_id').text(resp.header.company_name);
      $('#sales_no').text(resp.header.sales_no);
      $('#sales_date').text(tanggal_indonesia(resp.header.sales_date));
      $('#fc_name').text(resp.header.fc_name);
      $('#remarks').text(resp.header.remarks);
      $('#status').text(resp.header.doc_status);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}
$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pr_id  = $(this).data('pr_id');

    toastr.warning(
      'Apakah Anda ingin menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pr_id="'+pr_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });

  $('#vtable').on('click','#reject', function(e){

    var sales_id  = $(this).data('sales_id');
    var sales_no  = $(this).data('sales_no');

    toastr.warning(
      'Apakah anda ingin menolak '+sales_no+'?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return rejectRow(this)" data-sales_id="'+sales_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });
});

function rejectRow(e){
  var sales_id  = $(e).data('sales_id');

  $.ajax({
    data: {
      sales_id  : sales_id
    },
    type : "POST",
    url: baseUrl+'omzet/Sales/reject_js',
    success : function(resp){
      
      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Tolak');
        return false;

      } else {
        toastr.success("Data Berhasil Di Tolak.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'omzet/Sales'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function deleteRow(e){
  var pr_id  = $(e).data('pr_id');

  $.ajax({
    data: {
      pr_id  : pr_id
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_requisition/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>




