<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Allocation extends MY_Controller
{
    /* START CONSTRUCTOR */
    public function __construct()
    {
     	parent::__construct();
     	$this->load->helper('security');
     	$this->load->model('omzet/omzet_alocation_model');
        $this->load->model('omzet/Sales_01_model');
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Omzet', 'active_submenu' => 'omzet/allocation'));  
        $this->isMenu();

    }
    /* END CONSTRUCTOR */

    // query alter table
    
    // ALTER TABLE `db_omzet_shp`.`trn_omzet_allocation` CHANGE `sales_id` `project_id` INT(11) NULL;
    
    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('OMZ', 'Omzet Alocation', 'omzet/allocation', 'fa fa-window-restore', 'Omzet', '6', '1');
    
    // ALTER TABLE `db_omzet_shp`.`trn_sales_01` CHANGE `total_owner_price` `total_owner_price` DECIMAL(14,2) DEFAULT 0.00 NULL COMMENT 'Total Owner Price', CHANGE `total_cust_price` `total_cust_price` DECIMAL(14,2) DEFAULT 0.00 NULL COMMENT 'Total Cust Price', CHANGE `total_gov_price` `total_gov_price` DECIMAL(14,2) DEFAULT 0.00 NULL COMMENT 'Total Government Price'; 

    // ALTER TABLE `db_omzet_shp`.`trn_sales_02` CHANGE `owner_price` `owner_price` DECIMAL(12,2) DEFAULT 0.00 NULL COMMENT 'Harga Owner', CHANGE `cust_price` `cust_price` DECIMAL(12,2) DEFAULT 0.00 NULL COMMENT 'Harga Customer', CHANGE `gov_price` `gov_price` DECIMAL(12,2) DEFAULT 0.00 NULL COMMENT 'Harga Pemprov';

    // ALTER TABLE `db_omzet_shp`.`trn_fish_payment_01` CHANGE `grand_total` `grand_total` DECIMAL(14,2) DEFAULT 0.00 NULL COMMENT 'Total Keseluruhan';


    function index(){
        $data['data_sales']     = $this->omzet_alocation_model->getAllData();
        $this->template->load('body', 'omzet/allocation/allocation_view',$data);
    }

    function generate_allocation(){
        $data['data_sales']              = $this->Sales_01_model->get_all_omzet();
        // $generate           = $this->omzet_alocation_model->generate_allocation();
        $this->template->load('body', 'omzet/allocation/allocation_form',$data);
    }

    function process_allocation(){
        $data_sales             = $this->Sales_01_model->get_all_omzet();

        $allow_owner1   = 0.20;
        $allow_nahkoda  = 0.05;
        $allow_opr      = 0.75;
        $allow_owner2   = 0.50;
        $allow_abk      = 0.50;
        // test($data_sales,1);
        foreach ($data_sales as $key => $value) {
            $owner1       = $value->total*$allow_owner1;
            $nahkoda      = $value->total*$allow_nahkoda;
            $opr          = $value->total*$allow_opr;

            $biaya_opr    = $this->db->query("SELECT a.is_no,a.project_id,a.project_name,a.is_status,SUM(b.total) total
                                        FROM db_bumbu_transaction.trn_incoming_stock_01 a
                                        LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                                        WHERE a.is_status<>'Reject' AND a.project_id='".$value->project_id."' ")->row()->total;
            $loan_crew    = $this->db->query("SELECT a.project_id,a.project_name,SUM(a.amount) amount,a.cl_status FROM 
                                        db_loan_shp.trn_project_loan a
                                        WHERE a.is_active='1' AND a.cl_status<>'Paid' 
                                        AND a.project_id='".$value->project_id."'")->row()->amount;
            $total_opr    = $opr - ($biaya_opr+$loan_crew);
            $owner2       = $total_opr*$allow_owner2;
            $abk          = $total_opr*$allow_abk;
            $q_id   = $this->db->query('SELECT oa_id FROM db_omzet_shp.trn_omzet_allocation WHERE project_id="'.$value->project_id.'"');
            $cek    = $q_id->num_rows();
            if($cek=='0'){
                $oa_id      = $this->db->query("SELECT IFNULL(MAX(oa_id),0)+1 oa_id FROM db_omzet_shp.trn_omzet_allocation")->row()->oa_id;
                $this->omzet_alocation_model->setOaId($this->security->xss_clean($oa_id));
                $this->omzet_alocation_model->setProjectId($this->security->xss_clean($value->project_id));
                $this->omzet_alocation_model->setOaOwner($this->security->xss_clean($owner1));
                $this->omzet_alocation_model->setOaNakhoda($this->security->xss_clean($nahkoda));
                $this->omzet_alocation_model->setOaLoan($this->security->xss_clean($loan_crew));
                $this->omzet_alocation_model->setOprCost($this->security->xss_clean($biaya_opr));
                $this->omzet_alocation_model->setOaOwner2($this->security->xss_clean($owner2));
                $this->omzet_alocation_model->setOaAbk($this->security->xss_clean($abk));
                $this->omzet_alocation_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
                $this->omzet_alocation_model->setDataTime($this->security->xss_clean(dbnow()));
                $this->omzet_alocation_model->insert();
            }else{
                $data       = $q_id->row();
                $this->omzet_alocation_model->setOaId($this->security->xss_clean($data->oa_id));
                $this->omzet_alocation_model->setProjectId($this->security->xss_clean($value->project_id));
                $this->omzet_alocation_model->setOaOwner($this->security->xss_clean($owner1));
                $this->omzet_alocation_model->setOaNakhoda($this->security->xss_clean($nahkoda));
                $this->omzet_alocation_model->setOaLoan($this->security->xss_clean($loan_crew));
                $this->omzet_alocation_model->setOprCost($this->security->xss_clean($biaya_opr));
                $this->omzet_alocation_model->setOaOwner2($this->security->xss_clean($owner2));
                $this->omzet_alocation_model->setOaAbk($this->security->xss_clean($abk));
                $this->omzet_alocation_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
                $this->omzet_alocation_model->setDataTime($this->security->xss_clean(dbnow()));
                $this->omzet_alocation_model->update($data->oa_id);
            }

        }

        $this->session->set_flashdata('alert','Data Berhasil Disimpan Di Proses ');
        redirect('omzet/allocation');
    }


     /* START INSERT */
     public function ins()
     {
     	$this->M_trn_omzet_allocation->setOaId($this->security->xss_clean($_POST['oaId']));
        $this->M_trn_omzet_allocation->setProjectId($this->security->xss_clean($_POST['projectId']));
        $this->M_trn_omzet_allocation->setOaOwner($this->security->xss_clean($_POST['oaOwner']));
        $this->M_trn_omzet_allocation->setOaNakhoda($this->security->xss_clean($_POST['oaNakhoda']));
        $this->M_trn_omzet_allocation->setOaLoan($this->security->xss_clean($_POST['oaLoan']));
        $this->M_trn_omzet_allocation->setOprCost($this->security->xss_clean($_POST['oprCost']));
        $this->M_trn_omzet_allocation->setOaOwner2($this->security->xss_clean($_POST['oaOwner2']));
        $this->M_trn_omzet_allocation->setOaAbk($this->security->xss_clean($_POST['oaAbk']));
        $this->M_trn_omzet_allocation->setPicData($this->security->xss_clean($_POST['picData']));
        $this->M_trn_omzet_allocation->setDataTime($this->security->xss_clean($_POST['dataTime']));
        $this->M_trn_omzet_allocation->insert();
     }
     /* END INSERT */
     /* START UPDATE */
     public function upd()
     {
     	$id = $this->security->xss_clean($_POST['oaId']);
        $this->M_trn_omzet_allocation->getObjectById($id);
        $this->M_trn_omzet_allocation->setOaId($this->security->xss_clean($_POST['oaId']));
        $this->M_trn_omzet_allocation->setProjectId($this->security->xss_clean($_POST['projectId']));
        $this->M_trn_omzet_allocation->setOaOwner($this->security->xss_clean($_POST['oaOwner']));
        $this->M_trn_omzet_allocation->setOaNakhoda($this->security->xss_clean($_POST['oaNakhoda']));
        $this->M_trn_omzet_allocation->setOaLoan($this->security->xss_clean($_POST['oaLoan']));
        $this->M_trn_omzet_allocation->setOprCost($this->security->xss_clean($_POST['oprCost']));
        $this->M_trn_omzet_allocation->setOaOwner2($this->security->xss_clean($_POST['oaOwner2']));
        $this->M_trn_omzet_allocation->setOaAbk($this->security->xss_clean($_POST['oaAbk']));
        $this->M_trn_omzet_allocation->setPicData($this->security->xss_clean($_POST['picData']));
        $this->M_trn_omzet_allocation->setDataTime($this->security->xss_clean($_POST['dataTime']));
        $this->M_trn_omzet_allocation->update($id);
     }
     /* END UPDATE */
     /* START DELETE */
     public function del()
     {
     	$id = $this->security->xss_clean($_POST['salesId']);
     	$this->M_trn_sales_01->delete($id);
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->M_trn_sales_01->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */

     /* Added Manually */
     function form(){
        $this->session->unset_userdata('new_sales');
        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');
        $this->load->model('master/project_model');

        $new_sales = $this->session->userdata('new_sales');

        if(!$new_sales){
            $new_sales = array(
                'sales' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        // $data['data_items']     = $this->items_model->get_items();
        
        $data['data_project']   = $this->project_model->get_project();
        $data['data_customer']   = $this->Fish_customer_model->getAll();
        $data['data_fish']   = $this->Fish_model->getAll();
        $data['data_fish_grade']   = $this->Fish_grade_model->getAll();

        $data['new_sales']         = $new_sales;
        // test($data,1);
        $this->template->load('body', 'omzet/sales/sales_form', $data);
    }

    function form_act(){
        if(isset($_POST['company_id']))
        {
            /* START TRANSACTION */
            $this->db->trans_begin();

            /* HEADER */
            $salesId    = $this->Omzet_alocation_model->get_id()->sales_id;
            $period    = substr($this->input->post('sales_date'),0,4).substr($this->input->post('sales_date'),5,2);
            $docCode       = 'SL';
            $docNo    = $this->Omzet_alocation_model->get_nomor_dok($period,$docCode)->nomor_dok;
            // $docNo    = $this->Omzet_alocation_model->get_nomor_dok($period,$docCode);

            // echo $docNo; exit(0);

            $salesNo = $docCode.$period.$docNo;

            $companyId        = $_POST['company_id'];
            $companyName      = $_POST['company_name'];
            $salesDate        = $_POST['sales_date'];            
            $projectId        = $_POST['project_id'];
            $projectName      = $_POST['project_name'];
            $customerId       = $_POST['customer_id'];
            $customerName     = $_POST['customer_name'];
            $remarks          = $_POST['remarks'];
            $customer_type    = $_POST['customer_type'];


            /* DETAIL */
            $dataDetail = $_POST['sales_detail'];

            // echo "<pre>";
            // print_r($dataDetail);
            // echo "</pre>";
            // exit(0);
            $sdId = null;
            $total = 0;
            foreach ($dataDetail as $key => $value) {
                $subTotal = $this->security->xss_clean($value['subTotal']);
                $total  = $subTotal+$total;
            }

            $this->Omzet_alocation_model->setSalesId($this->security->xss_clean($salesId));
            $this->Omzet_alocation_model->setSalesDate($this->security->xss_clean($salesDate));
            $this->Omzet_alocation_model->setSalesNo($this->security->xss_clean($salesNo));
            $this->Omzet_alocation_model->setProjectId($this->security->xss_clean($projectId));
            $this->Omzet_alocation_model->setProjectName($this->security->xss_clean($projectName));
            $this->Omzet_alocation_model->setCompanyId($this->security->xss_clean($companyId));
            $this->Omzet_alocation_model->setCompanyName($this->security->xss_clean($companyName));
            $this->Omzet_alocation_model->setFcId($this->security->xss_clean($customerId));
            $this->Omzet_alocation_model->setFcName($this->security->xss_clean($customerName));
            $this->Omzet_alocation_model->setRemarks($this->security->xss_clean($remarks));
            $this->Omzet_alocation_model->setDocStatus($this->security->xss_clean('New'));
            if($customer_type=='Customer'){
                $this->Omzet_alocation_model->setTotalCustPrice($total);
            }else if($customer_type=='Owner'){
                $this->Omzet_alocation_model->setTotalOwnerPrice($total);
            }else if($customer_type=='Pemerintah'){
                $this->Omzet_alocation_model->setTotalGovPrice($total);
            }
            $this->Omzet_alocation_model->setIsCancel($this->security->xss_clean('0'));
            $this->Omzet_alocation_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
            $this->Omzet_alocation_model->setInputTime($this->security->xss_clean(dbnow()));
            // $this->Omzet_alocation_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
            // $this->Omzet_alocation_model->setEditTime($this->security->xss_clean($_POST['editTime']));
            $save = $this->Omzet_alocation_model->insert();

            $sdId = null;
            foreach ($dataDetail as $key => $value) {
                $this->Sales_02_model->resetValues();
                $this->Sales_02_model->setSalesId($salesId);

                $fishId = $this->security->xss_clean($value['fish_id']);
                $fishName = $this->security->xss_clean($value['fish_name']);
                $qty = $this->security->xss_clean($value['qty']);
                $price = $this->security->xss_clean($value['price']);
                $subTotal = $this->security->xss_clean($value['subTotal']);


                $this->Sales_02_model->setFishId($fishId);                
                $this->Sales_02_model->setFishName($fishName);

                $this->Sales_02_model->setQty($qty);
                if($customer_type=='Customer'){
                    $this->Sales_02_model->setCustPrice($price);
                }else if($customer_type=='Owner'){
                    $this->Sales_02_model->setOwnerPrice($price);
                }else if($customer_type=='Pemerintah'){
                    $this->Sales_02_model->setGovPrice($price);
                }
                $this->Sales_02_model->setSubtotal($subTotal);
                $this->Sales_02_model->insert();

                // exit(0);
            }

            // $storeId = $this->security->xss_clean($dataHeader['storeId']);

            /* VALIDATE TRANSACTION STATUS */
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                jsout(array('success' => false));
            }else{
                $this->db->trans_commit();
                jsout(array('success' => true, 'status' => $save));
            }
        }
    }

    function edit($id){
        $this->session->unset_userdata('new_sales');
        // $this->load->model('master/items_model');
        // $this->load->model('master/supplier_model');
        // $this->load->model('master/project_model');
        $detail     = $this->Sales_02_model->getBySalesId($id)->result();
        $tdetail    = $this->Sales_02_model->getBySalesId($id)->num_rows();

        $new_sales = $this->session->userdata('new_sales');

        $data['header']         = $this->Omzet_alocation_model->getById($id);

        if($tdetail==0){
            $new_sales = array(
                'items' => array()
            );
        }else{
            foreach($detail as $key=>$val){
                if($data['header']['total_owner_price']!='0.00'){
                $price        = $val->owner_price;
                }else if($data['header']['total_cust_price']!='0.00'){
                $price        = $val->cust_price;
                }else if($data['header']['total_gov_price']!='0.00'){
                $price        = $val->gov_price;
                }
                $new_sales['items'][$key] = array(
                    'det_id'       => $val->sales_02_id,
                    'fish_id'      => $val->fish_id,
                    'fish_name'    => $val->fish_name,
                    'price'        => $price,
                    'qty'          => $val->qty,
                    'subTotal'     => $val->subtotal
                );
            }
        }

        $this->session->set_userdata('new_sales', $new_sales);
        if($tdetail!=0){
            $data['id_detail']      = $val->sales_02_id;
        }else{  
            $data['id_detail']      = 0;
        }
        
        $data['new_sales']      = $new_sales;
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);

        // $data['data_customer']  = $this->Fish_customer_model->get_data();
        $data['data_customer']   = $this->Fish_customer_model->getAll();
        $data['data_fish']     = $this->Fish_model->get_data();
        $data['data_project']   = $this->Project_model->get_project();
        $this->template->load('body', 'omzet/Sales/sales_edit', $data);
        // exit();
    }

    function edit_act(){
        if(isset($_POST['sales_id']))
        {
            /* START TRANSACTION */
            // $this->db->trans_begin();            
            $salesId = $this->security->xss_clean($_POST['sales_id']);
            // echo $salesId; 
            $this->Sales_02_model->delete($salesId);
            $new_sales = $this->session->userdata('new_sales');        

            /* DETAIL */
            $dataDetail = $_POST['sales_detail'];
            $customer_type = $_POST['customer_type'];
            // echo "<pre>";
            // print_r($dataDetail);
            // echo "</pre>";
            // exit(0);
            $sdId = null;
            $total      = 0;
            foreach ($dataDetail as $key => $value) {
                $this->Sales_02_model->resetValues();
                $this->Sales_02_model->setSalesId($salesId);
                $fishId = $this->security->xss_clean($value['fish_id']);
                $fishName = $this->security->xss_clean($value['fish_name']);
                $qty = $this->security->xss_clean($value['qty']);
                $price = $this->security->xss_clean($value['price']);
                $subTotal = $this->security->xss_clean($value['subTotal']);
                $total       = $subTotal+$total;

                $this->Sales_02_model->setFishId($fishId);                
                $this->Sales_02_model->setFishName($fishName);
                $this->Sales_02_model->setQty($qty);
                if($customer_type=='Customer'){
                    $this->Sales_02_model->setCustPrice($price);
                    $this->Sales_02_model->setOwnerPrice(0.00);
                    $this->Sales_02_model->setGovPrice(0.00);
                }else if($customer_type=='Owner'){
                    $this->Sales_02_model->setOwnerPrice($price);
                    $this->Sales_02_model->setCustPrice(0.00);
                    $this->Sales_02_model->setGovPrice(0.00);
                }else if($customer_type=='Pemerintah'){
                    $this->Sales_02_model->setGovPrice($price);
                    $this->Sales_02_model->setCustPrice(0.00);
                    $this->Sales_02_model->setOwnerPrice(0.00);
                }
                $this->Sales_02_model->setSubtotal($subTotal);
                $this->Sales_02_model->insert();
            }
            // echo 'Done'; exit();

            $companyId        = $_POST['company_id'];
            $companyName      = $_POST['company_name'];
            $salesDate        = $_POST['sales_date'];            
            $projectId        = $_POST['project_id'];
            $projectName      = $_POST['project_name'];
            $customerId       = $_POST['customer_id'];
            $customerName     = $_POST['customer_name'];
            $remarks          = $_POST['remarks'];

            $this->Omzet_alocation_model->getObjectById($salesId);
            $this->Omzet_alocation_model->setSalesDate($this->security->xss_clean($salesDate));
            // $this->Omzet_alocation_model->setSalesNo($this->security->xss_clean($salesNo));
            $this->Omzet_alocation_model->setProjectId($this->security->xss_clean($projectId));
            $this->Omzet_alocation_model->setProjectName($this->security->xss_clean($projectName));
            $this->Omzet_alocation_model->setCompanyId($this->security->xss_clean($companyId));
            $this->Omzet_alocation_model->setCompanyName($this->security->xss_clean($companyName));
            $this->Omzet_alocation_model->setFcId($this->security->xss_clean($customerId));
            if($customer_type=='Customer'){
                $this->Omzet_alocation_model->setTotalCustPrice($total);
                $this->Omzet_alocation_model->setTotalOwnerPrice(0.00);
                $this->Omzet_alocation_model->setTotalGovPrice(0.00);
            }else if($customer_type=='Owner'){
                $this->Omzet_alocation_model->setTotalOwnerPrice($total);
                $this->Omzet_alocation_model->setTotalGovPrice(0.00);
                $this->Omzet_alocation_model->setTotalCustPrice(0.00);
            }else if($customer_type=='Pemerintah'){
                $this->Omzet_alocation_model->setTotalGovPrice($total);
                $this->Omzet_alocation_model->setTotalOwnerPrice(0.00);
                $this->Omzet_alocation_model->setTotalCustPrice(0.00);
            }
            $this->Omzet_alocation_model->setFcName($this->security->xss_clean($customerName));
            $this->Omzet_alocation_model->setRemarks($this->security->xss_clean($remarks));
            $this->Omzet_alocation_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
            $this->Omzet_alocation_model->setEditTime($this->security->xss_clean(dbnow()));

            $update   = $this->Omzet_alocation_model->update($salesId);

            /* VALIDATE TRANSACTION STATUS */
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                jsout(array('success' => false));
            }
            else
            {
                $this->db->trans_commit();
                $this->session->unset_userdata('new_sales');
                jsout(array('success' => true, 'status' => $update ));
            }
            // redirect('omzet/Sales');
        }
  
    }

    function view_popup($id){

        $header         = $this->Omzet_alocation_model->getById($id);
        $detail         = $this->Sales_02_model->getBySalesId($id)->result_array();
        $total          = 0;

        $myData = array();
        foreach ($detail as $key => $row) {
            $total      = $total+$row['subtotal'];
            $myData[] = array(
                $row['fish_name'],     
                number_format($row['qty'],2),        
                number_format($row['cust_price'],2),        
                number_format($row['subtotal'],2)        
            );            
        }  

        $myData[] = array(
            '',     
            '',        
            'Total',        
            number_format($total,2)        
        ); 

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function reject_js(){
        $delete = $this->Omzet_alocation_model->act_reject_js();
        jsout(array('success' => true, 'status' => $delete ));
    }
}
?>
