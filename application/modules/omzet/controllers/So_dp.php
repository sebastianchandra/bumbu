<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class So_dp extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Omzet', 'active_submenu' => 'omzet/so_dp'));  
        $this->isMenu();
        $this->load->model('omzet/so_dp_model');
    }


    function index(){
        $data['data_so_dp']     = $this->so_dp_model->get_so_dp();
        $this->template->load('body', 'omzet/so_dp/dp_view',$data);
    }

    function form(){
        $this->load->model('master/customer_model');

        $data['data_customer']  = $this->customer_model->get_data();
        $this->template->load('body', 'omzet/so_dp/dp_form',$data);
    }

    function form_act(){
        $id         = $this->so_dp_model->getDataCount()->sd_id+1;

        $periode            = date('Y').date('m');
        $nomor_dok          = $this->so_dp_model->get_nomor_dok($periode)->nomor_dok;
        $so_dp_code         = 'SDS'.$periode.$nomor_dok;

        $this->so_dp_model->setSdId($this->security->xss_clean($id));
        $this->so_dp_model->setcustomerId($this->security->xss_clean($_POST['customer_id']));
        $this->so_dp_model->setcustomerName($this->security->xss_clean($_POST['customer_name']));
        $this->so_dp_model->setSdNo($this->security->xss_clean($so_dp_code));
        $this->so_dp_model->setSdDate($this->security->xss_clean($_POST['pd_date']));
        // $this->so_dp_model->setPoId($this->security->xss_clean($_POST['poId']));
        // $this->so_dp_model->setPoNo($this->security->xss_clean($_POST['poNo']));
        $this->so_dp_model->setAmount($this->security->xss_clean($_POST['amount']));
        $this->so_dp_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->so_dp_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->so_dp_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->so_dp_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->so_dp_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        $save   = $this->so_dp_model->insert();
        // test($so_dp_code,1);

        jsout(array('success' => true, 'status' => $save ));
    }

    function delete($id){
        $delete = $this->so_dp_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('omzet/so_dp',$data);
        }
    }

    function delete_js(){
        $delete = $this->so_dp_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $this->load->model('master/customer_model');

        $data['data_customer']  = $this->customer_model->get_data();
        $data['so_dp']          = $this->so_dp_model->getById($id);
        // test($data,1);
        $this->template->load('body', 'omzet/so_dp/dp_edit', $data);
    }

    function edit_act(){
        // test($_POST['sd_id'],1);
        $id = $this->security->xss_clean($_POST['sd_id']);
        // $this->so_dp_model->getObjectById($id);
        $this->so_dp_model->setcustomerId($this->security->xss_clean($_POST['customer_id']));
        $this->so_dp_model->setcustomerName($this->security->xss_clean($_POST['customer_name']));
        $this->so_dp_model->setSdDate($this->security->xss_clean($_POST['sd_date']));
        $this->so_dp_model->setAmount($this->security->xss_clean($_POST['amount']));
        $this->so_dp_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->so_dp_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->so_dp_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->so_dp_model->update($id);
        jsout(array('success' => true, 'status' => $update ));
    }

    function detail_dp_customer(){
        $sup_id     = $this->input->post('id');
        $result     = $this->so_dp_model->get_so_dp_free($sup_id);
        echo json_encode($result);
    }

    // function form_act_in_pr(){
    //     $save   = $this->so_dp_model->act_form_in_pr();
    //     jsout(array('success' => true, 'status' => $save ));
    // }

}
?>