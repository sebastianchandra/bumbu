<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_payment extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Omzet', 'active_submenu' => 'omzet/sales_payment')); 
        $this->isMenu();
        $this->load->model('omzet/sales_payment_model01');
        $this->load->model('omzet/sales_payment_model02');
        $this->load->model('omzet/sales_01_model');
        $this->load->model('omzet/so_dp_model');
        $this->load->model('omzet/invoice_model01');
        $this->load->model('master/customer_model');
    }


    function index(){ 
        $this->session->unset_userdata('new_so');
        $data['data_customer']  = $this->customer_model->get_data();
        $data['data_pa']        = $this->sales_payment_model01->get_pa();
        $this->template->load('body', 'omzet/sales_payment/pa_view',$data);
    }

    function view_inv($fc_id){
        $header         = $this->invoice_model01->getInPayment($fc_id);

        $myData = array();
        
        $sisa       = 0;
        foreach ($header as $key => $row) {
            $total      = $row['total_cust_price'];
            $sisa       = $total - $row['amount'];
            $myData[] = array(
                $row['invoice_no'],       
                $row['fc_name'],       
                money($total),     
                money($sisa),  
                // '<a class="btn btn-primary" href="'.base_url('omzet/sales_payment/pa_input/'.$row['invoice_id']).'">Process</a>'
                '<input type="checkbox" value="'.$row['invoice_id'].'">'
            );            
        }  

        return jsout(array('detail'=> json_encode($myData)));
    }

    function pa_selected(){
        $this->session->unset_userdata('new_so');
        $new_so = $this->session->userdata('new_so');

        foreach($this->input->post('values') as $key=>$val){
            // test($val,1);
            $new_so['items'][$key] = array(
                'det_id'        => $val
            );
        }
        $this->session->set_userdata('new_so', $new_so);
    }

    function pa_input(){
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_so']       = $this->invoice_model01->get_inv_detail();
        
        // $data['data_dp']       = $this->so_dp_model->getDpCustomer($data['data_so']->fc_id);
        
        $this->template->load('body', 'omzet/sales_payment/pa_form',$data);

    }

    function form_act(){
        $this->db->trans_begin();

        $periode            = substr($this->input->post('pp_date'),0,4).substr($this->input->post('pp_date'),5,2);
        $no_pay             = 'INP'.$periode.$this->sales_payment_model01->get_no_pay_dok($periode)->nomor_dok;
        $id_pay             = $this->sales_payment_model01->get_id_pay()->fp_id;
        $company_id         = $this->input->post('company_id');
        $company_name       = $this->input->post('company_name');
        $pp_date            = $this->input->post('pp_date');
        $fc_id              = $this->input->post('fc_id');
        $fc_name            = $this->input->post('fc_name');
        $payment_type       = $this->input->post('payment_type');

        $total              = 0;
        $subtotal           = 0;

        // $tloop              = $this->input->post('tloop'); 
        // foreach ($this->input->post('invoice_id') as $key => $value) {
        //     $invoice_id         = $value;
        //     $invoice_no         = $this->input->post('invoice_no')[$key];
        //     $amount             = str_replace(',','',$this->input->post('total'))[$key];
        //     $jml_hutang         = $this->input->post('jml_hutang')[$key];
        //     $total              = $total + $amount;

        // }

        // $jml_hutang         = $this->input->post('jml_hutang')[0];
        // $invoice_id         = $this->input->post('invoice_id')[0];
        // $invoice_no         = $this->input->post('invoice_no')[0];

        // $so_dp              = $this->input->post('so_dp');
        // $total_dp           = 0;
        // foreach ($so_dp as $key => $value) {
        //     $cek_dp             = $this->so_dp_model->getDetailSoDp($value);
        //     $total_dp           = $cek_dp->amount+$total_dp;

        //     $this->so_dp_model->updatesoDp($invoice_no,$invoice_id,$cek_dp->sd_id);
        //     // Query Update DP Yang Sudah Dipakai

        // }


        // $total_bayar        = str_replace(',','',$this->security->xss_clean($this->db->escape_str($this->input->post('total_bayar'))))+$total_dp;
        // $pot_dp             = $this->input->post('pot_dp');

        // $amount             = $total_bayar;

        // test($total_bayar.' '.$jml_hutang.' '.$this->input->post('total_bayar').' '.$total_dp,1);

        // test('STOP',1);

        $total_bayar            = str_replace(',','',$this->input->post('total_pembayaran_hid'));
        // test($total_bayar,1);

        $this->sales_payment_model01->setFpId($this->security->xss_clean($id_pay));
        $this->sales_payment_model01->setFpNo($this->security->xss_clean($no_pay));
        $this->sales_payment_model01->setFpDate($this->security->xss_clean($pp_date));
        $this->sales_payment_model01->setCompanyId($this->security->xss_clean($company_id));
        $this->sales_payment_model01->setCompanyName($this->security->xss_clean($company_name));
        $this->sales_payment_model01->setFcId($this->security->xss_clean($fc_id));
        $this->sales_payment_model01->setFcName($this->security->xss_clean($fc_name));
        $this->sales_payment_model01->setPaymentType($this->security->xss_clean($payment_type));
        $this->sales_payment_model01->setGrandTotal($this->security->xss_clean($total_bayar));
        $this->sales_payment_model01->setIsActive($this->security->xss_clean(1));
        $this->sales_payment_model01->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->sales_payment_model01->setInputTime($this->security->xss_clean(dbnow()));
        $this->sales_payment_model01->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->sales_payment_model01->setEditTime($this->security->xss_clean(dbnow()));
        $this->sales_payment_model01->insert();

        foreach ($this->input->post('invoice_id') as $key => $value) {
            $invoice_id         = $value;
            $invoice_no         = $this->input->post('invoice_no')[$key];
            $amount             = str_replace(',','',$this->input->post('total'))[$key];
            $jml_hutang         = $this->input->post('jml_hutang')[$key];
            $total              = $total + $amount;

            $this->sales_payment_model02->setFpId($this->security->xss_clean($id_pay));
            $this->sales_payment_model02->setSalesId($this->security->xss_clean($invoice_id));
            $this->sales_payment_model02->setSalesNo($this->security->xss_clean($invoice_no));
            $this->sales_payment_model02->setAmount($this->security->xss_clean($amount));
            $this->sales_payment_model02->setDiscPercent($this->security->xss_clean(0));
            $this->sales_payment_model02->setDiscValue($this->security->xss_clean(0));
            $this->sales_payment_model02->setTotal($this->security->xss_clean($amount));
            $this->sales_payment_model02->setIsActive(1);
            $this->sales_payment_model02->insert();

            if($jml_hutang == $amount){
                $status         = "Closed";
            }else{
                $status         = "Outstanding";
            }
            
            // TInggal Update Status
            $this->invoice_model01->update_payment($invoice_id,$status);

        }

        

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('alert','Data Gagal Disimpan ');
            redirect('omzet/sales_payment');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('alert','Data Berhasil Disimpan Dengan Nomor '.$no_pay);
            redirect('omzet/sales_payment');
        }

        
    }

    function view_popup($id){

        $header         = $this->sales_payment_model01->detail_header($id);
        $detail         = $this->sales_payment_model02->detail_pembayaran($id)->result_array();
        $myData         = array();
        $total          = 0;
        foreach ($detail as $key => $row) {
            $total      = $row['total']+$total;
            $myData[] = array(
                $row['sales_no'],     
                number_format($row['total'],1)
            );     
        }  
            $myData[] = array(
                'Total',
                number_format($total,2)
            );       

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function reject_js(){
        $pp_id      = $this->input->post('pp_id');
        $pp_no      = $this->input->post('pp_no');

        $u_aktif_payment    = $this->sales_payment_model01->update_status($pp_id);
        $detail             = $this->sales_payment_model02->detail_pembayaran($pp_id)->result();

        foreach ($detail as $key => $value) {
            $this->invoice_model01->update_status($value->sales_id);
            $this->sales_payment_model02->update_active($value->fp_02_id);
        }

        jsout(array('success' => true, 'status' => $u_aktif_payment ));
    }

     function view_print($id){
        $data['header']         = $this->sales_payment_model01->print_header($id);
        $data['detail']         = $this->sales_payment_model02->print_detail($id);
        // $data['detail']         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $this->template->load('body', 'omzet/sales_payment/pa_print', $data);
    }

}
//1. Penambahan Function Untuk Popup.
//2. Penambahan Reject Untuk Pembayaran PO
// 3. Penambahan Print Payment
?>