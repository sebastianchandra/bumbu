<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales_payment_model01 extends CI_Model
{
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_sales_payment_01';
    private $ppId;
    private $ppNo;
    private $ppDate;
    private $companyId;
    private $companyName;
    private $supplierId;
    private $supplierName;
    private $paymentType;
    private $grandTotal;
    private $isActive;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->ppId = 0;
        $this->ppNo = '';
        $this->ppDate = "0000-00-00";
        $this->companyId = 0;
        $this->companyName = '';
        $this->supplierId = 0;
        $this->supplierName = '';
        $this->paymentType = 0;
        $this->grandTotal = 0;
        $this->isActive = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
      
    }

    public function setFpId($aPpId)
    {
        $this->ppId = $this->db->escape_str($aPpId);
    }
    public function getFpId()
    {
        return $this->ppId;
    }
    public function setFpNo($aPpNo)
    {
        $this->ppNo = $this->db->escape_str($aPpNo);
    }
    public function getFpNo()
    {
        return $this->ppNo;
    }
    public function setFpDate($aPpDate)
    {
        $this->ppDate = $this->db->escape_str($aPpDate);
    }
    public function getFpDate()
    {
        return $this->ppDate;
    }
    public function setCompanyId($aCompanyId)
    {
        $this->companyId = $this->db->escape_str($aCompanyId);
    }
    public function getCompanyId()
    {
        return $this->companyId;
    }
    public function setCompanyName($aCompanyName)
    {
        $this->companyName = $this->db->escape_str($aCompanyName);
    }
    public function getCompanyName()
    {
        return $this->companyName;
    }
    public function setFcId($aSupplierId)
    {
        $this->supplierId = $this->db->escape_str($aSupplierId);
    }
    public function getFcId()
    {
        return $this->supplierId;
    }
    public function setFcName($aSupplierName)
    {
        $this->supplierName = $this->db->escape_str($aSupplierName);
    }
    public function getFcName()
    {
        return $this->supplierName;
    }
    public function setPaymentType($aPaymentType)
    {
        $this->paymentType = $this->db->escape_str($aPaymentType);
    }
    public function getPaymentType()
    {
        return $this->paymentType;
    }
    public function setGrandTotal($aGrandTotal)
    {
        $this->grandTotal = $this->db->escape_str($aGrandTotal);
    }
    public function getGrandTotal()
    {
        return $this->grandTotal;
    }
    public function setIsActive($aIsActive)
    {
        $this->isActive = $this->db->escape_str($aIsActive);
    }
    public function getIsActive()
    {
        return $this->isActive;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

    function get_pa()
    {
        $sql ='SELECT fp_id,fp_no,fp_date,company_id,company_name,fc_id,fc_name,payment_type,grand_total,is_active FROM db_bumbu_transaction.trn_sales_payment_01 ORDER BY fp_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_po_detail(){
        $sql  ="SELECT a.po_id,a.po_no,a.po_date,a.fc_name,a.fc_id,a.total,sum(b.total) tbayar FROM trn_po_01 a 
                LEFT JOIN trn_po_payment_02 b ON a.po_id=b.po_id AND b.is_active='1'
                WHERE a.po_id IN ( ";
                foreach ($this->session->userdata('new_pa')['items'] as $key => $value) {
                    $sql .= "$value[det_id],";            
                }
        $sql .= "'') GROUP BY a.po_id ORDER BY a.po_id";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_no_pay_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(fp_no,10,4))+1,4,'0'),'0001') nomor_dok, fp_no FROM `trn_sales_payment_01` WHERE SUBSTRING(fp_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    function get_id_pay(){
        $query = $this->dbpurch->query("SELECT IFNULL(MAX(fp_id),0)+1 fp_id FROM `db_bumbu_transaction`.`trn_sales_payment_01` ")->row();
        return $query;
    }

    public function insert()
    {
        if($this->ppId =='' || $this->ppId == NULL )
        {
            $this->ppId = 0;
        }
        if($this->ppNo =='' || $this->ppNo == NULL )
        {
            $this->ppNo = '';
        }
        if($this->ppDate =='' || $this->ppDate == NULL )
        {
            $this->ppDate = "0000-00-00";
        }
        if($this->companyId =='' || $this->companyId == NULL )
        {
            $this->companyId = 0;
        }
        if($this->companyName =='' || $this->companyName == NULL )
        {
            $this->companyName = '';
        }
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->paymentType =='' || $this->paymentType == NULL )
        {
            $this->paymentType = 0;
        }
        if($this->grandTotal =='' || $this->grandTotal == NULL )
        {
            $this->grandTotal = 0;
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'fp_id,'; 
        $stQuery .=   'fp_no,'; 
        $stQuery .=   'fp_date,'; 
        $stQuery .=   'company_id,'; 
        $stQuery .=   'company_name,'; 
        $stQuery .=   'fc_id,'; 
        $stQuery .=   'fc_name,'; 
        $stQuery .=   'payment_type,'; 
        $stQuery .=   'grand_total,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time,'; 
        $stQuery .=   'pic_edit,'; 
        $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->ppId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->ppNo).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->ppDate).'",'; 
        $stQuery .=   $this->db->escape_str($this->companyId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->companyName).'",'; 
        $stQuery .=   $this->db->escape_str($this->supplierId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->supplierName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->paymentType).'",'; 
        $stQuery .=   $this->db->escape_str($this->grandTotal).','; 
        $stQuery .=   $this->db->escape_str($this->isActive).','; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'",'; 
        $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        // test($stQuery,1);
        $this->db->query($stQuery); 

        return $this->ppNo;
    }

    function detail_header($id){
        return $this->dbpurch->query("SELECT a.fp_no,a.fp_date,a.fc_id,b.fc_name,a.payment_type,a.grand_total 
                                    FROM trn_sales_payment_01 a
                                    LEFT JOIN db_bumbu_master.mst_customer b ON a.fc_id=b.fc_id
                                    WHERE a.fp_id='".$id."'")->row();
    }

    function update_status($id){
        $this->dbpurch->query("UPDATE trn_sales_payment_01 SET 
                    is_active = '0', 
                    pic_edit = '".$this->current_user['user_id']."',
                    edit_time = '".dbnow()."' 
                    WHERE fp_id = '".$id."'");
    }

    function print_header($id){
        return $this->dbpurch->query("SELECT a.fp_no,a.fp_date,a.company_id,b.company_name,a.fc_id,c.fc_name,a.payment_type,a.grand_total
            FROM trn_sales_payment_01 a 
            LEFT JOIN db_bumbu_master.mst_company b ON a.company_id=b.company_id
            LEFT JOIN db_bumbu_master.mst_customer c ON c.fc_id=a.fc_id WHERE a.fp_id='".$id."'")->row();
    }

    function get_so_detail(){
        $sql  ="SELECT a.sales_id,a.sales_no,a.sales_date,a.fc_name,a.fc_id,a.total_owner_price,a.total_cust_price,a.total_gov_price,sum(b.total) tbayar 
                FROM trn_sales_01 a 
                LEFT JOIN trn_sales_payment_02 b ON a.sales_id=b.sales_id AND b.is_active='1'
                WHERE a.sales_id IN ( ";
                foreach ($this->session->userdata('new_so')['items'] as $key => $value) {
                    $sql .= "$value[det_id],";            
                }
        $sql .= "'') GROUP BY a.sales_id ORDER BY a.sales_id";
        // test($sql,1);
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

}   
