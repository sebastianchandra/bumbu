

<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class So_dp_model extends CI_Model
{
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_so_dp';
    private $sdId;
    private $customerId;
    private $customerName;
    private $sdNo;
    private $sdDate;
    private $soId;
    private $soNo;
    private $amount;
    private $remarks;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->sdId = 0;
        $this->customerId = 0;
        $this->customerName = '';
        $this->sdNo = '';
        $this->sdDate = "0000-00-00";
        $this->soId = 0;
        $this->soNo = '';
        $this->amount = 0;
        $this->remarks = '';
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
    }

    public function setSdId($aSdId)
    {
        $this->sdId = $this->db->escape_str($aSdId);
    }
    public function getSdId()
    {
        return $this->sdId;
    }
    public function setCustomerId($aCustomerId)
    {
        $this->customerId = $this->db->escape_str($aCustomerId);
    }
    public function getCustomerId()
    {
        return $this->customerId;
    }
    public function setCustomerName($aCustomerName)
    {
        $this->customerName = $this->db->escape_str($aCustomerName);
    }
    public function getCustomerName()
    {
        return $this->customerName;
    }
    public function setSdNo($aSdNo)
    {
        $this->sdNo = $this->db->escape_str($aSdNo);
    }
    public function getSdNo()
    {
        return $this->sdNo;
    }
    public function setSdDate($aSdDate)
    {
        $this->sdDate = $this->db->escape_str($aSdDate);
    }
    public function getSdDate()
    {
        return $this->sdDate;
    }
    public function setSoId($aSoId)
    {
        $this->soId = $this->db->escape_str($aSoId);
    }
    public function getSoId()
    {
        return $this->soId;
    }
    public function setSoNo($aSoNo)
    {
        $this->soNo = $this->db->escape_str($aSoNo);
    }
    public function getSoNo()
    {
        return $this->soNo;
    }
    public function setAmount($aAmount)
    {
        $this->amount = $this->db->escape_str($aAmount);
    }
    public function getAmount()
    {
        return $this->amount;
    }
    public function setRemarks($aRemarks)
    {
        $this->remarks = $this->db->escape_str($aRemarks);
    }
    public function getRemarks()
    {
        return $this->remarks;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

	function get_so_dp()
    {
        $sql ='SELECT sd_id,sd_no,customer_name,sd_date,so_no,amount,remarks FROM trn_so_dp where is_active="1" ORDER BY sd_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_so_dp_free($id)
    {
        $sql ='SELECT sd_id,sd_no,customer_name,sd_date,so_no,amount,remarks FROM trn_so_dp where is_active="1" AND so_id IS NULL and customer_id="'.$id.'" ORDER BY sd_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    public function getDataCount()
    {
        $stQuery  = 'SELECT IFNULL(MAX(sd_id),0) sd_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
        $query = $this->dbpurch->query($stQuery);
        return $query->row();
    }

    function get_nomor_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(sd_no,10,4))+1,4,'0'),'0001') nomor_dok, sd_no FROM `trn_so_dp` WHERE SUBSTRING(sd_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    public function insert()
     {
        if($this->sdId =='' || $this->sdId == NULL )
        {
            $this->sdId = 0;
        }
        if($this->customerId =='' || $this->customerId == NULL )
        {
            $this->customerId = 0;
        }
        if($this->customerName =='' || $this->customerName == NULL )
        {
            $this->customerName = '';
        }
        if($this->sdNo =='' || $this->sdNo == NULL )
        {
            $this->sdNo = '';
        }
        if($this->sdDate =='' || $this->sdDate == NULL )
        {
            $this->sdDate = "0000-00-00";
        }
        if($this->soId =='' || $this->soId == NULL )
        {
            $this->soId = 0;
        }
        if($this->soNo =='' || $this->soNo == NULL )
        {
            $this->soNo = '';
        }
        if($this->amount =='' || $this->amount == NULL )
        {
            $this->amount = 0;
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'sd_id,'; 
        $stQuery .=   'customer_id,'; 
        $stQuery .=   'customer_name,'; 
        $stQuery .=   'sd_no,'; 
        $stQuery .=   'sd_date,'; 
        // $stQuery .=   'so_id,'; 
        // $stQuery .=   'so_no,'; 
        $stQuery .=   'amount,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->sdId).','; 
        $stQuery .=   $this->db->escape_str($this->customerId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->customerName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->sdNo).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->sdDate).'",'; 
        // $stQuery .=   $this->db->escape_str($this->soId).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->soNo).'",'; 
        $stQuery .=   $this->db->escape_str($this->amount).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        // test($stQuery,1);
        $this->db->query($stQuery); 
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('sd_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    public function update($id)
     {
        if($this->sdId =='' || $this->sdId == NULL )
        {
            $this->sdId = 0;
        }
        if($this->customerId =='' || $this->customerId == NULL )
        {
            $this->customerId = 0;
        }
        if($this->customerName =='' || $this->customerName == NULL )
        {
            $this->customerName = '';
        }
        if($this->sdNo =='' || $this->sdNo == NULL )
        {
            $this->sdNo = '';
        }
        if($this->sdDate =='' || $this->sdDate == NULL )
        {
            $this->sdDate = "0000-00-00";
        }
        if($this->soId =='' || $this->soId == NULL )
        {
            $this->soId = 0;
        }
        if($this->soNo =='' || $this->soNo == NULL )
        {
            $this->soNo = '';
        }
        if($this->amount =='' || $this->amount == NULL )
        {
            $this->amount = 0;
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        // $stQuery .=   'sd_id ='.$this->db->escape_str($this->sdId).','; 
        $stQuery .=   'customer_id ='.$this->db->escape_str($this->customerId).','; 
        $stQuery .=   'customer_name ="'.$this->db->escape_str($this->customerName).'",'; 
        // $stQuery .=   'sd_no ="'.$this->db->escape_str($this->sdNo).'",'; 
        $stQuery .=   'sd_date ="'.$this->db->escape_str($this->sdDate).'",'; 
        // $stQuery .=   'so_id ='.$this->db->escape_str($this->soId).','; 
        // $stQuery .=   'so_no ="'.$this->db->escape_str($this->soNo).'",'; 
        $stQuery .=   'amount ='.$this->db->escape_str($this->amount).','; 
        $stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
        // $stQuery .=   'pic_input ='.$this->db->escape_str($this->picInput).','; 
        // $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'sd_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
     }

    function act_delete_js(){
        $sql = "UPDATE trn_so_dp SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE sd_id = '".$this->input->post('sd_id')."'";
        //test($sql,1);
        $query = $this->dbpurch->query($sql);
        return $query;
    }

    function updatesoDp($noSo,$id_so,$id){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_so_dp` SET `so_no` = '".$noSo."',`so_id` = '".$id_so."'   
                WHERE `sd_id` = '".$id."'");
    }

    function getDetailSoDp($id){
        return $this->dbpurch->query("SELECT * FROM `db_bumbu_transaction`.`trn_so_dp` WHERE sd_id='".$id."'")->row();
    }

    function getTotalAmount($id){
        return $this->dbpurch->query("SELECT ifNULL(SUM(amount),0) tamount FROM `db_bumbu_transaction`.`trn_so_dp` WHERE so_id='".$id."'")->row();
    }

    function act_update_dp($id){
        $this->dbpurch->query("UPDATE trn_so_dp SET so_id = NULL, so_no = NULL WHERE so_id='".$id."'");
    }

    function getDpCustomer($id){
        return $this->dbpurch->query(" SELECT a.* FROM `db_bumbu_transaction`.`trn_so_dp` a WHERE a.customer_id='".$id."' AND a.so_id IS NULL ")->result();
    }



}	