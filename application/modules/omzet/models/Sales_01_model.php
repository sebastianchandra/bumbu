<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Apr 17, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Sales_01_model extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_bumbu_transaction';
     private $myTable = 'trn_sales_01';
     private $salesId;
     private $salesDate;
     private $salesNo;
     private $projectId;
     private $projectName;
     private $companyId;
     private $companyName;
     private $fcId;
     private $fcName;
     private $totalOwnerPrice;
     private $totalCustPrice;
     private $totalGovPrice;
     private $remarks;
     private $docStatus;
     private $isCancel;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
          $this->dbomzet = $this->load->database('purchasing',true);
          $this->salesId = 0;
          $this->salesDate = "0000-00-00";
          $this->salesNo = '';
          $this->projectId = 0;
          $this->projectName = '';
          $this->companyId = 0;
          $this->companyName = '';
          $this->fcId = 0;
          $this->fcName = '';
          $this->totalOwnerPrice = 0;
          $this->totalCustPrice = 0;
          $this->totalGovPrice = 0;
          $this->remarks = '';
          $this->docStatus = '';
          $this->isCancel = 0;
          $this->picInput = '';
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = '';
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setSalesId($aSalesId)
     {
     	$this->salesId = $this->db->escape_str($aSalesId);
     }
     public function getSalesId()
     {
     	return $this->salesId;
     }
     public function setSalesDate($aSalesDate)
     {
     	$this->salesDate = $this->db->escape_str($aSalesDate);
     }
     public function getSalesDate()
     {
     	return $this->salesDate;
     }
     public function setSalesNo($aSalesNo)
     {
     	$this->salesNo = $this->db->escape_str($aSalesNo);
     }
     public function getSalesNo()
     {
     	return $this->salesNo;
     }
     public function setProjectId($aProjectId)
     {
     	$this->projectId = $this->db->escape_str($aProjectId);
     }
     public function getProjectId()
     {
     	return $this->projectId;
     }
     public function setProjectName($aProjectName)
     {
     	$this->projectName = $this->db->escape_str($aProjectName);
     }
     public function getProjectName()
     {
     	return $this->projectName;
     }
     public function setCompanyId($aCompanyId)
     {
     	$this->companyId = $this->db->escape_str($aCompanyId);
     }
     public function getCompanyId()
     {
     	return $this->companyId;
     }
     public function setCompanyName($aCompanyName)
     {
     	$this->companyName = $this->db->escape_str($aCompanyName);
     }
     public function getCompanyName()
     {
     	return $this->companyName;
     }
     public function setFcId($aFcId)
     {
     	$this->fcId = $this->db->escape_str($aFcId);
     }
     public function getFcId()
     {
     	return $this->fcId;
     }
     public function setFcName($aFcName)
     {
     	$this->fcName = $this->db->escape_str($aFcName);
     }
     public function getFcName()
     {
     	return $this->fcName;
     }
     public function setTotalOwnerPrice($aTotalOwnerPrice)
     {
     	$this->totalOwnerPrice = $this->db->escape_str($aTotalOwnerPrice);
     }
     public function getTotalOwnerPrice()
     {
     	return $this->totalOwnerPrice;
     }
     public function setTotalCustPrice($aTotalCustPrice)
     {
     	$this->totalCustPrice = $this->db->escape_str($aTotalCustPrice);
     }
     public function getTotalCustPrice()
     {
     	return $this->totalCustPrice;
     }
     public function setTotalGovPrice($aTotalGovPrice)
     {
     	$this->totalGovPrice = $this->db->escape_str($aTotalGovPrice);
     }
     public function getTotalGovPrice()
     {
     	return $this->totalGovPrice;
     }
     public function setRemarks($aRemarks)
     {
     	$this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
     	return $this->remarks;
     }
     public function setDocStatus($aDocStatus)
     {
     	$this->docStatus = $this->db->escape_str($aDocStatus);
     }
     public function getDocStatus()
     {
     	return $this->docStatus;
     }
     public function setIsCancel($aIsCancel)
     {
     	$this->isCancel = $this->db->escape_str($aIsCancel);
     }
     public function getIsCancel()
     {
     	return $this->isCancel;
     }
     public function setPicInput($aPicInput)
     {
     	$this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
     	return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
     	$this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
     	return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
     	$this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
     	return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
     	$this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
     	return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
     	if($this->salesId =='' || $this->salesId == NULL )
     	{
          	$this->salesId = 0;
     	}
     	if($this->salesDate =='' || $this->salesDate == NULL )
     	{
          	$this->salesDate = "0000-00-00";
     	}
     	if($this->salesNo =='' || $this->salesNo == NULL )
     	{
          	$this->salesNo = '';
     	}
     	if($this->projectId =='' || $this->projectId == NULL )
     	{
          	$this->projectId = 0;
     	}
     	if($this->projectName =='' || $this->projectName == NULL )
     	{
          	$this->projectName = '';
     	}
     	if($this->companyId =='' || $this->companyId == NULL )
     	{
          	$this->companyId = 0;
     	}
     	if($this->companyName =='' || $this->companyName == NULL )
     	{
          	$this->companyName = '';
     	}
     	if($this->fcId =='' || $this->fcId == NULL )
     	{
          	$this->fcId = 0;
     	}
     	if($this->fcName =='' || $this->fcName == NULL )
     	{
          	$this->fcName = '';
     	}
     	if($this->totalOwnerPrice =='' || $this->totalOwnerPrice == NULL )
     	{
          	$this->totalOwnerPrice = 0;
     	}
     	if($this->totalCustPrice =='' || $this->totalCustPrice == NULL )
     	{
          	$this->totalCustPrice = 0;
     	}
     	if($this->totalGovPrice =='' || $this->totalGovPrice == NULL )
     	{
          	$this->totalGovPrice = 0;
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	if($this->docStatus =='' || $this->docStatus == NULL )
     	{
          	$this->docStatus = '';
     	}
     	if($this->isCancel =='' || $this->isCancel == NULL )
     	{
          	$this->isCancel = 0;
     	}
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= '( '; 
     	$stQuery .=   'sales_id,'; 
     	$stQuery .=   'sales_date,'; 
     	$stQuery .=   'sales_no,'; 
     	$stQuery .=   'warehouse_id,'; 
     	$stQuery .=   'warehouse_name,'; 
     	$stQuery .=   'company_id,'; 
     	$stQuery .=   'company_name,'; 
     	$stQuery .=   'fc_id,'; 
     	$stQuery .=   'fc_name,'; 
     	$stQuery .=   'total_owner_price,'; 
     	$stQuery .=   'total_cust_price,'; 
     	$stQuery .=   'total_gov_price,'; 
     	$stQuery .=   'remarks,'; 
     	$stQuery .=   'doc_status,'; 
     	$stQuery .=   'is_cancel,'; 
     	$stQuery .=   'pic_input,'; 
          $stQuery .=   'input_time,'; 
          $stQuery .=   'disc_value,'; 
     	$stQuery .=   'disc_percent'; 
     	// $stQuery .=   'pic_edit,'; 
     	// $stQuery .=   'edit_time'; 
     	$stQuery .= ') '; 
     	$stQuery .= 'VALUES '; 
     	$stQuery .= '( '; 
     	$stQuery .=   $this->db->escape_str($this->salesId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->salesDate).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->salesNo).'",'; 
     	$stQuery .=   $this->db->escape_str($this->projectId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->projectName).'",'; 
     	$stQuery .=   $this->db->escape_str($this->companyId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->companyName).'",'; 
     	$stQuery .=   $this->db->escape_str($this->fcId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->fcName).'",'; 
          $stQuery .=   $this->db->escape_str($this->totalOwnerPrice).','; 
     	$stQuery .=   $this->db->escape_str($this->totalCustPrice).','; 
     	$stQuery .=   $this->db->escape_str($this->totalGovPrice).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->docStatus).'",'; 
     	$stQuery .=   $this->db->escape_str($this->isCancel).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->picInput).'",'; 
     	$stQuery .=   '"'.$this->db->escape_str($this->inputTime).'",'; 
          $stQuery .=   $this->db->escape_str(($_POST['disc_value']!='') ? $_POST['disc_value'] : "0").','; 
     	$stQuery .=   $this->db->escape_str(($_POST['disc_percent']!='') ? $_POST['disc_percent'] : "0").''; 
     	// $stQuery .=   '"'.$this->db->escape_str($this->picEdit).'",'; 
     	// $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
     	$stQuery .= '); '; 
          // return $stQuery;
     	$this->db->query($stQuery); 
          return $this->salesNo;
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
     	if($this->salesId =='' || $this->salesId == NULL )
     	{
          	$this->salesId = 0;
     	}
     	if($this->salesDate =='' || $this->salesDate == NULL )
     	{
          	$this->salesDate = "0000-00-00";
     	}
     	if($this->salesNo =='' || $this->salesNo == NULL )
     	{
          	$this->salesNo = '';
     	}
     	if($this->projectId =='' || $this->projectId == NULL )
     	{
          	$this->projectId = 0;
     	}
     	if($this->projectName =='' || $this->projectName == NULL )
     	{
          	$this->projectName = '';
     	}
     	if($this->companyId =='' || $this->companyId == NULL )
     	{
          	$this->companyId = 0;
     	}
     	if($this->companyName =='' || $this->companyName == NULL )
     	{
          	$this->companyName = '';
     	}
     	if($this->fcId =='' || $this->fcId == NULL )
     	{
          	$this->fcId = 0;
     	}
     	if($this->fcName =='' || $this->fcName == NULL )
     	{
          	$this->fcName = '';
     	}
     	if($this->totalOwnerPrice =='' || $this->totalOwnerPrice == NULL )
     	{
          	$this->totalOwnerPrice = 0;
     	}
     	if($this->totalCustPrice =='' || $this->totalCustPrice == NULL )
     	{
          	$this->totalCustPrice = 0;
     	}
     	if($this->totalGovPrice =='' || $this->totalGovPrice == NULL )
     	{
          	$this->totalGovPrice = 0;
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	if($this->docStatus =='' || $this->docStatus == NULL )
     	{
          	$this->docStatus = '';
     	}
     	if($this->isCancel =='' || $this->isCancel == NULL )
     	{
          	$this->isCancel = 0;
     	}
     	if($this->picInput =='' || $this->picInput == NULL )
     	{
          	$this->picInput = '';
     	}
     	if($this->inputTime =='' || $this->inputTime == NULL )
     	{
          	$this->inputTime = '0000-00-00 00:00:00';
     	}
     	if($this->picEdit =='' || $this->picEdit == NULL )
     	{
          	$this->picEdit = '';
     	}
     	if($this->editTime =='' || $this->editTime == NULL )
     	{
          	$this->editTime = '0000-00-00 00:00:00';
     	}
     	
     	$stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'SET '; 
     	// $stQuery .=   'sales_id ='.$this->db->escape_str($this->salesId).','; 
     	$stQuery .=   'sales_date ="'.$this->db->escape_str($this->salesDate).'",'; 
     	// $stQuery .=   'sales_no ="'.$this->db->escape_str($this->salesNo).'",'; 
     	// $stQuery .=   'project_id ='.$this->db->escape_str($this->projectId).','; 
     	// $stQuery .=   'project_name ="'.$this->db->escape_str($this->projectName).'",'; 
     	$stQuery .=   'company_id ='.$this->db->escape_str($this->companyId).','; 
     	$stQuery .=   'company_name ="'.$this->db->escape_str($this->companyName).'",'; 
     	$stQuery .=   'fc_id ='.$this->db->escape_str($this->fcId).','; 
     	$stQuery .=   'fc_name ="'.$this->db->escape_str($this->fcName).'",'; 
     	$stQuery .=   'total_owner_price ='.$this->db->escape_str($this->totalOwnerPrice).','; 
     	$stQuery .=   'total_cust_price ='.$this->db->escape_str($this->totalCustPrice).','; 
     	$stQuery .=   'total_gov_price ='.$this->db->escape_str($this->totalGovPrice).','; 
     	$stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
     	$stQuery .=   'doc_status ="'.$this->db->escape_str($this->docStatus).'",'; 
     	$stQuery .=   'is_cancel ='.$this->db->escape_str($this->isCancel).','; 
     	// $stQuery .=   'pic_input ="'.$this->db->escape_str($this->picInput).'",'; 
     	// $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
     	$stQuery .=   'pic_edit ="'.$this->db->escape_str($this->picEdit).'",'; 
     	$stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'sales_id = '.$this->db->escape_str($id).''; 
          return $this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
     	$stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'WHERE sales_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->order_by('sales_id', 'ASC');
     	return $this->db->get()->result();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('sales_id', $this->db->escape_str($id));
     	return $this->db->get()->row_array();
     }
     public function getByIdRow($id)
     {
          $this->db->select('*');
          $this->db->from($this->myDb.'.'.$this->myTable);
          $this->db->where('sales_id', $this->db->escape_str($id));
          return $this->db->get()->row();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('sales_id', $this->db->escape_str($id));
     	$row = $this->db->get()->row_array();
     	$this->salesId = $row['sales_id']; 
     	$this->salesDate = $row['sales_date']; 
     	$this->salesNo = $row['sales_no']; 
     	$this->projectId = $row['project_id']; 
     	$this->projectName = $row['project_name']; 
     	$this->companyId = $row['company_id']; 
     	$this->companyName = $row['company_name']; 
     	$this->fcId = $row['fc_id']; 
     	$this->fcName = $row['fc_name']; 
     	$this->totalOwnerPrice = $row['total_owner_price']; 
     	$this->totalCustPrice = $row['total_cust_price']; 
     	$this->totalGovPrice = $row['total_gov_price']; 
     	$this->remarks = $row['remarks']; 
     	$this->docStatus = $row['doc_status']; 
     	$this->isCancel = $row['is_cancel']; 
     	$this->picInput = $row['pic_input']; 
     	$this->inputTime = $row['input_time']; 
     	$this->picEdit = $row['pic_edit']; 
     	$this->editTime = $row['edit_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
     	$stQuery  = 'SELECT sales_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$query = $this->db->query($stQuery);
     	return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->salesId = 0; 
     	$this->salesDate = '0000-00-00'; 
     	$this->salesNo = ''; 
     	$this->projectId = 0; 
     	$this->projectName = ''; 
     	$this->companyId = 0; 
     	$this->companyName = ''; 
     	$this->fcId = 0; 
     	$this->fcName = ''; 
     	$this->totalOwnerPrice = 0; 
     	$this->totalCustPrice = 0; 
     	$this->totalGovPrice = 0; 
     	$this->remarks = ''; 
     	$this->docStatus = ''; 
     	$this->isCancel = 0; 
     	$this->picInput = ''; 
     	$this->inputTime = '0000-00-00 00:00:00'; 
     	$this->picEdit = ''; 
     	$this->editTime = '0000-00-00 00:00:00'; 
     }
     /* END OF RESET VALUES */


     /* ADD MANUALLY BY MAURICE */
     function get_id(){
        $query = $this->dbomzet->query("SELECT IFNULL(MAX(sales_id)+1,1) sales_id FROM trn_sales_01")->row();
        return $query;
     }

     function get_nomor_dok($periode,$kode){

            $strSql = "SELECT IFNULL(LPAD(MAX(SUBSTRING(sales_no,10,4))+1,4,'0'),'0001') nomor_dok, sales_no FROM trn_sales_01 
                         WHERE SUBSTRING(sales_no,3,6) = '".$periode."' AND SUBSTRING(sales_no,1,2)='".$kode."' ";    

             $query = $this->dbomzet->query($strSql)->row();
             return $query;
             // return $strSql;

         }

     public function getAllData()
     {

          $this->db->select('sl.sales_id, sl.sales_date,sl.sales_no, fc.fc_id, fc.fc_name');
          $this->db->select('mp.warehouse_name,sl.doc_status, sl.total_cust_price');
          $this->db->from($this->myDb.'.'.$this->myTable.' AS sl ');    
          $this->db->join('db_bumbu_master.mst_warehouse AS mp', 'mp.warehouse_id = sl.warehouse_id', 'left');
          $this->db->join('db_bumbu_master.mst_customer AS fc', 'fc.fc_id = sl.fc_id', 'left');          
          $this->db->order_by('sales_id', 'DESC');
          return $this->db->get()->result();
     }

     function act_reject_js(){
        $sql = "UPDATE trn_sales_01 SET doc_status='Reject', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE sales_id = '".$this->input->post('sales_id')."'";
        //test($sql,1);
        $query = $this->dbomzet->query($sql);
        return $query;
     }

     function getSoPayment($fc_id){
          $sql    = "SELECT sales_id,sales_date,sales_no,fc_name,total_owner_price,total_cust_price,total_gov_price FROM trn_sales_01 WHERE doc_status NOT IN ('Closed','Force Closed','Reject') AND fc_id='".$fc_id."'";
                        // test($sql,1);
          return $this->dbomzet->query($sql)->result_array();
     }

     function update_payment($value,$status){
          $sql      = "UPDATE trn_sales_01 SET doc_status='".$status."' WHERE sales_id='".$value."'";
          return $this->dbomzet->query($sql);    
     }

     function get_all_omzet(){
          $sql      = "SELECT a.project_id,a.project_name,SUM(a.total_owner_price+a.total_cust_price+a.total_gov_price) total FROM trn_sales_01 a 
                         WHERE a.doc_status<>'Reject' GROUP BY a.project_id";
          return    $this->dbomzet->query($sql)->result();
     }

     function getBySoProcess(){
          $sql      = " SELECT * FROM trn_sales_01 WHERE doc_status IN ('New','Process') ORDER BY sales_id DESC ";
          return    $this->dbomzet->query($sql)->result_array();
     }

     function header_so($id){
          return $this->dbomzet->query(" SELECT a.sales_id,a.sales_date,a.sales_no,a.company_name,a.fc_id,a.fc_name,a.total_cust_price,a.remarks,
                         a.doc_status,a.disc_value,a.disc_percent
                         FROM trn_sales_01 a WHERE a.sales_id='".$id."' ")->row();
     }

	 function update_status($no_so){
		 $this->dbomzet->query("UPDATE `trn_sales_01` SET `doc_status` = 'New' WHERE `sales_no` = '".$no_so."'");
	 }
     

}  

?>