<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Production</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Production</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("stok/production/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Gudang</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="warehouse_id" name="warehouse_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' >".$value->warehouse_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Barang Produksi</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id="production_items_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_produksi as $key => $value) {
                    echo "<option value='".$value->items_id."' data-name='".$value->items_name."'>".$value->items_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Qty</p>
              </div>
              <div class="col-md-2">
                <input type="text" class="form-control price" id="qty" value="0">
              </div>
            </div>
            <?php //test($new_pro,1); ?>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks">
                <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_pro["items"]); ?>' required />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/config_production'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

$("#requester").focus();

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#supplier_id").select2();
    $("#production_items_id").select2();
    $("#warehouse_id").select2();

    $('#save').click(pr.save);

  },

  save: function(e){
    e.preventDefault();

    if($('#warehouse_id').val()==''){
      toastr.error("<strong>Warehouse</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#warehouse_id').select2('open');
      return false;
    }

    if($('#production_items_id').val()==''){
      toastr.error("<strong>Nama Barang Produksi</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#production_items_id').select2('open');
      return false;
    }

    if($('#qty').val()=='0'){
      toastr.error("<strong>Quantity</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#qty').val();
      return false;
    }
    
    $.ajax({
      url: baseUrl+'stok/production/form_act',
      type : "POST",  
      data: {
        warehouse_id              : $('#warehouse_id').val(),
        production_items_id       : $('#production_items_id').val(),
        production_items_name     : $('#production_items_id option:selected').attr('data-name'),
        qty                       : $('#qty').val(),
        remarks                   : $('#remarks').val()

      },
      success : function(resp){
        // debugger
        if(resp.success == 'gagal') {
          messages = 'Stok Barang Kurang  <br>';
          // messages += "<hr>";
          for (i = 0; i < resp.status.length; i++) {
            messages += "Nama Barang : " + resp.status[i].nama_items + "<br>";
            // messages += "<hr>";
          }

          toastr.error(messages, 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else {
          
          // $('#save').prop("disabled",true);

          messages = 'Data Berhasil di Simpan.';
          messages += "<hr>";

          toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
          
          // setTimeout(function () {
          //   window.location.href = baseUrl+'stok/production/'; 
          // }, 2000);
        }
      }
    });

  }

};

pr.init();

</script>