<div class="app-title">
  <div>
    <h1>View Stock Use</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Stock</li>
      <li class="breadcrumb-item active">Stock Use</li>
    </ul></p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a class="btn btn-primary" href="<?php echo base_url(); ?>stok/stock_use/form"><i class="fa fa-plus"></i> Input</a></li>
  </ul>
</div>
<div class="row">
  <div class="tile col-md-12">
    <table class="table table-hover table-bordered" id="isTable">
      <thead>
        <tr>
         <th>No. Stock Use</th>
         <th>Date</th>
         <th>Company Name</th>
         <th>Warehouse</th>
         <th>Department</th>
         <th>Action</th>
       </tr>
     </thead>
     <tbody>
      <?php 
      $no = 0;
      foreach ($data_os as $key => $value) {
       $no = $no+1;
       if($value->os_status==1){
        $status = 'Closed';
      }else if($value->os_status==2){
        $status = 'Outstanding';
      }
      ?>
      <tr>
       <td><strong><a href="#" id='detail' onclick="return show_detail_os(this)" 
        data-id="<?php echo $value->os_id; ?>" 
        data-os="<?php echo $value->os_no; ?>"><?php echo $value->os_no; ?></a></strong></td>
        <td><?php echo tgl_singkat($value->os_date); ?></td>
        <td><?php echo $value->company_name; ?></td>
        <td><?php echo $value->warehouse_name; ?></td>
        <td><?php echo $value->dept_name; ?></td>
        <td align="center">
          <a target="_blank" data-placement="right" title="Process IS" class="btn btn-success btn-sm" href="<?php echo base_url()."stok/stock_use/print_su/$value->os_id"; ?>"><i class="fa fa-print"></i></a>
          <button type="button" id='delete' class="element btn btn-danger btn-sm" data-toggle="modal" data-id="<?php echo $value->os_id; ?>" data-no="<?php echo $value->os_no; ?>" <?php echo ($value->os_status==1)? 'disabled' : '' ?>><i class="fa fa-close"></i></button>
        </td>
      </tr>
      <?php 
      }
      ?>
     </tbody>
    </table>
  </div>
</div>

<script type="text/template" id="void_dialog">
  <form id="form_void_dialog">
    <div class="form-group">
      <label for="void-type">Jenis Pembatalan</label>
      <select id="void-type" name="void-type" class="form-control">
        <option value="">Pilih Status</option>
      </select>
    </div>
  </form>
</script>

<script type="text/javascript">
  $(document).ready(function () {
    var $tabs = $('#horizontalTab');
    $tabs.responsiveTabs({
      active: 0,
      rotate: false,
      startCollapsed: 'accordion',
      collapsible: 'accordion',
      setHash: true,
        // disabled: [3,4],
      });
  });

  $('#isTable').DataTable({
    "paging": true, 
    "bLengthChange": false, // disable show entries dan page
    "bFilter": true,
    "bInfo": true, // disable Showing 0 to 0 of 0 entries
    "bAutoWidth": false,
    "language": {
      "emptyTable": "No Data"
    },
    "aaSorting": [],
    responsive: true
  });

  $('#poTable').DataTable({
    "paging": true, 
    "bLengthChange": false, // disable show entries dan page
    "bFilter": true,
    "bInfo": true, // disable Showing 0 to 0 of 0 entries
    "bAutoWidth": false,
    "language": {
      "emptyTable": "No Data"
    },
    "aaSorting": [],
    responsive: true
  });

  $('#prTable').DataTable({
    "paging": true, 
    "bLengthChange": false, // disable show entries dan page
    "bFilter": true,
    "bInfo": true, // disable Showing 0 to 0 of 0 entries
    "bAutoWidth": false,
    "language": {
      "emptyTable": "No Data"
    },
    "aaSorting": [],
    'dom': 'Bfrtip',
    buttons: [
    'print'
    ]
  });

  $('#isTable').on('click','#delete', function (e) {
    var idIs     = $(this).data('id');
    var noIs     = $(this).data('no');

    BootstrapDialog.show({
      title: 'Void Incoming Stok',
      type : BootstrapDialog.TYPE_DANGER,
      message: 'Do you want void Incoming Stock Nomor <strong>'+noIs+'</storng> ?',
      closable: false,
      buttons: [
      {
        label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
        action: function(dia){
          dia.close();
        }
      },
      {
          label: '<i class="fa fa-close"></i> Void', cssClass: 'btn-danger', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            $.ajax({
              data: {
                id_is : idIs,
                no_is : noIs
              },
              type : "POST",
              url: baseUrl+'stok/incoming_stok/void_is',
              success : function(resp){

                if(resp.no_po == 'ERROR INSERT' || resp.no_po == false) {
                  alert('Data Tidak berhasil di Proses');
                  return false;

                } else {
                  $.notify({
                    icon: "glyphicon glyphicon-save",
                    message: 'Void Success'
                  },{
                    type: 'success',
                    onClosed: function(){ 
                      //location.reload();
                    }
                  });

                  // setTimeout(function () {
                  //     window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
                  // }, 2000);
                }
              }
            });

          }
        }
        ],
      });
  });

  function show_detail_os(e){
    let noId      = $(e).data('id');
    let noOs      = $(e).data('os');

    $.get({
      url: baseUrl + 'stok/stock_use/popup_ss/'+noId,
      success: function(resp){
        BootstrapDialog.show({
          title: 'Nomor Stock Use # <strong> '+noOs+' </strong>', 
          nl2br: false, 
          message: resp,
          closable: true,
          size: 'size-full',
          buttons:[
          {
            label: 'Tutup',
            action: function(dia){ dia.close(); }
          }
          ]
        });
      },
      complete: function(){
        $('body').css('cursor','default');
      }
    });
    return false;
    
  };
</script>