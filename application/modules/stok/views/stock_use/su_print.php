<?php 
//test($header,1);
?>
<style>
  @media print{
    @page {
      size: portrait;

    }
  }
  table{
    border-collapse: collapse;
    font-family: arial;
    font-size: 16px;
  }
  .borderluar {
    border: 2px solid black;
    padding: 0px;
  }
  .borderdalem {
    border: 1px solid #000000b0;
    padding: 0px;
  }
  .borderdalemcenter {
    border: 1px solid #000000b0;
    padding: 0px;
    text-align: center;
  }
  .borderdalemangka {
    border: 1px solid #000000b0;
    padding: 0px 5px;
    text-align: right;
  }
  .bordertengah {
    border: 1px solid #000000b0;
    padding: 5px 5px 5px 5px; 
  }
  p.two {
    border-style: solid;
    border-width: 1px;
  }
  .header{
    padding: 0px; 
  }
  .header_alamat {
    border: 1px solid black;
    padding: 0px; 
    text-align: center;
  }
  .table_detail {
    border-collapse: collapse;
    border: 1px solid black;
  }
  .detail{
    padding: 0px 7px; 
  }
</style>

<div class="app-title">
	<div>
		<h1>Print Outgoing Stock</h1>
		<p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Stock</li>
      <li class="breadcrumb-item">Outgoing Stock</li>
      <li class="breadcrumb-item active">Print Outgoing Stock</li>
    </ul></p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"></li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <table style="width:100%">
        <tr>
          <td>
            <table style='width:100%'> 
              <tr>
                <td align="left" colspan="3" class="header" valign="bottom">
                  <?php echo ($header->company_id=='2')? '<img alt="" src="http://localhost/purchasing/assets/images/sangati_logo.png">' : '<img alt="" src="http://localhost/purchasing/assets/images/lm_logo1.png" width="110" height="90">'; ?>
                </td>
                <td align="left" colspan="1" class="header" valign="bottom" valign="top">
                  <table width="100%">
                    <tr>
                      <td width="10%"><strong></strong></td>
                      <td width="30%"><strong>Print Date</strong></td>
                      <td>: <?php echo tgl_singkat(dbnow()); ?></td>
                    </tr>
                    <tr>
                      <td><strong></strong></td>
                      <td><strong>Print Time</strong></td>
                      <td>: <?php echo date('H:i:s'); ?></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="left" colspan="2" class="header" valign="bottom">
                  <strong style="font-size:20px;"><?php echo ($header->company_name=='PT. Sangati')? 'PT SANGATI SOERYA SEJAHTERA' : 'L&M System Indonesia'; ?></strong>
                </td>
              </tr>
              <tr>
                <td align="left" colspan="4" width='50%'><br/></td>
              </tr>
              <tr>
                <td colspan='4' align="center"><strong style="font-size: 20px;">STOCK USE</strong></td>
              </tr>
              <tr>
                <td align="left" colspan="4" width='50%' class="header_alamat"></td>
              </tr>
              <tr>
                <td align="left" colspan="4" width='50%'><br/></td>
              </tr>
              <tr>
                <td colspan='3' valign="top">
                  <?php 
                  if($header->os_kind==1){
                    $kind = 'Transfer';
                  }elseif($header->os_kind==2){
                    $kind = 'Write Off';
                  }elseif($header->os_kind==3){
                    $kind = 'Retur';
                  }elseif($header->os_kind==4){
                    $kind = 'Others';
                  }elseif($header->os_kind==5){
                    $kind = 'Stock Use';
                  }

                  ?>
                  <table width="100%">
                    <tr>
                      <td width="40%"><strong>Outgoing Stock Number</strong></td>
                      <td>: <?php echo $header->os_no; ?></td>
                    </tr>
                    <tr>
                      <td><strong>Outgoing Stock Date</strong></td>
                      <td>: <?php echo tgl_singkat($header->os_date); ?></td>
                    </tr>
                    <tr>
                      <td><strong>Info</strong></td>
                      <td>: <?php echo $header->remarks; ?></td>
                    </tr>
                  </table>
                </td>
                <td colspan='1' valign="top">
                  <table width="100%">
                    <tr>
                      <td width="10%"><strong></strong></td>
                      <td  width="40%"><strong>Outgoing Stock Kind</strong></td>
                      <td>: <?php echo $kind; ?></td>
                    </tr>
                    <tr>
                      <td><strong></strong></td>
                      <td><strong>Warehouse</strong></td>
                      <td>: <?php echo $header->warehouse_name; ?></td>
                    </tr>
                    <tr>
                      <td><strong></strong></td>
                      <td><strong>Department</strong></td>
                      <td>: <?php echo $header->dept_name; ?></td>
                    </tr>
                    <tr>
                      <td><strong></strong></td>
                      <td><strong>Requester</strong></td>
                      <td>: <?php echo $header->os_requester; ?></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="left" colspan="4" width='50%'><br/></td>
              </tr>
              <tr>
                <td colspan='4'>
                  <table style='width:100%'>
                    <tbody>
                      <tr>
                        <td class='borderdalemcenter' width="3%">No.</td>
                        <td class='borderdalemcenter' width="30%">Items Name</td>
                        <td class='borderdalemcenter'>Remarks</td>
                        <td class='borderdalemcenter' width="10%">Qty</td>
                      </tr>
                    </tbody>
                    <thread>
                      <?php
                      $no     = 0;
                      $max    = 39;
                      $tqty   = 0;
                      foreach ($detail as $key => $value) {
                        $no = $no+1;
                        ?>
                        <tr>
                          <td class="detail" align="right"><?php echo $no; ?>.</td>
                          <td class="detail"><?php echo $value->items_name; ?></td>
                          <td class="detail"><?php echo $value->remarks; ?></td>
                          <td class="detail" align="right"><?php echo money($value->qty); ?></td>
                        </tr>
                        <?php 
                        $tqty = $tqty+$value->qty;
                      }
                      for($a=$no+1;$a<=$max;$a++){
                        ?>
                        <tr>
                          <td align="right" height="23px" colspan="4"></td>
                        </tr>
                        <?php
                      }
                      ?>
                        
                    </thread>
                        <tr>
                          <td align="left" colspan="4" class="header_alamat"></td>
                        </tr>
                        <tr>
                          <td align="center" colspan="3"><strong>Total</strong></td>
                          <td class="detail" align="right" colspan="1" ><strong><?php echo money($tqty); ?></strong></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="4" class="header_alamat"></td>
                        </tr>
                        <tr>
                          <td align="left" colspan="4">
                            <table>
                              <tr>
                                <td align="center" width="150px">Create By<br/><br/><br/><br/>( <?php echo $header->nama_pembuat; ?> )</td>
                                <td align="center" width="150px">Checked By<br/><br/><br/><br/>( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )</td>
                                <td align="center" width="150px">Received By<br/><br/><br/><br/>( <?php echo $header->os_requester; ?> )</td>
                              </tr>

                            </table>
                          </td>
                        </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

</head>
<script>window.print(); setTimeout(function(){window.close();},500);</script>