<div class="app-title">
  <div>
    <h1>Input Stock Use</h1>
    <p><ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Stock</li>
        <li class="breadcrumb-item">Stock Use</li>
        <li class="breadcrumb-item active">Input</li>
  </ul></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="control-label col-md-2">Company</label>
            <div class="col-md-5">
              <!-- <select class="form-control" id='company'>
                <option value=""> - </option>
                <?php 
                foreach ($data_company as $key => $value) {
                  echo '<option data-name="'.$value['name'].'" value="'.$value['code'].'">'.$value['name'].'</option>';
                }
                ?>
              </select> -->
              <?php echo $this->current_user['company_name']; ?>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Date</label>
            <div class="col-md-8">
              <input class="form-control col-md-4" type="date" placeholder="Date" id="date" value="<?php echo date('Y-m-d'); ?>" readonly>
              <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_os["items"]); ?>' required />
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Warehouse</label>
            <div class="col-md-5">
              <select class="form-control" id='warehouse'>
                <option value=""> - </option>
                <?php 
                foreach ($data_warehouse as $key => $value) {
                  echo '<option data-name="'.$value->warehouse_name.'" value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Department</label>
            <div class="col-md-5">
              <select class="form-control" id='dept'>
                <option value=""> - </option>
                <?php 
                foreach ($user_group as $key => $value) {
                  echo '<option data-name="'.$value->name.'" value="'.$value->id_user_group.'">'.$value->name.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Requester</label>
            <div class="col-md-4">
              <input class="form-control" type="text" placeholder="Requester" id="requester" value="<?php echo isset($new_os['siup']) ? $new_os['siup']:''; ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Info</label>
            <div class="col-md-8">
              <input class="form-control col-md-8" type="text" placeholder="Info Stock Use" id="info" value="<?php echo isset($new_os['info']) ? $new_os['info']:''; ?>">
            </div>
          </div>
        </form>
      </div>
      <div class="tile-footer">
        <div class="form-group row">
          <label class="control-label col-md-2">Items Name</label>
          <div class="col-md-5">
            <!-- <select class="form-control" id='item_id'>
              <option value=""> - </option>
              <?php 
              foreach ($data_items as $key => $value) {
                echo '<option data-name="'.$value->items_nama.'" value="'.$value->items_id.'">'.$value->items_nama.'</option>';
              }
              ?>
            </select> -->
            <!-- <select class="form-control" id='item_id'>
              <option value=""> - </option>
            </select> -->
            <input tabindex="102" type="text" class="form-control" id="id_items" name="id_items" placeholder="Kode Barang" onclick="return browse_item_barang()"/>
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Qty</label>
          <div class="col-md-8">
            <input class="form-control col-md-4" type="text" placeholder="Qty" id="item_qty">
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2">Info Items</label>
          <div class="col-md-8">
            <input class="form-control col-md-8" type="text" placeholder="Info" id="item_info">
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-md-2"></label>
          <div class="col-md-2">
            <button class="btn btn-warning" type="button" id="add-items"><i class="fa fa-plus-square"></i> Add Items</button>
          </div>
          <!-- <label class="control-label col-md-6">Items Name</label>
          <div class="col-md-6">
            <input class="form-control col-md-5" type="text" placeholder="Name Supplier" id="name">
          </div> -->
        </div>
      </div>
      <div class="tile-footer">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="detail">
              <thead>
                <tr>
                  <th width="1%">ID</th>
                  <th>Name</th>
                  <th width="8%">Qty</th>
                  <th>Info</th>
                  <th width="5%">Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
      <div class="tile-footer">
        <div class="row">
          <div class="col-md-8 col-md-offset-3">
            <button class="btn btn-primary" type="button" id="save"><i class="fa fa-floppy-o"></i> Save</button>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>stok/stock_use/reset"><i class="fa fa-reply"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php //test($new_os,0); ?>

<script type="text/template" class="modal_item" id="content_browse_barang">
  <table class='table table-bordered table-hover' id='table-browse_barang' style="cursor:pointer">
    <thead>
      <tr>  
        <th>Items ID</th>
        <th>Items Code</th>
        <th>Items Name</th>
        <th>Stock Qty</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</script>

<script type="text/javascript">

function browse_item_barang() {
  if(!$('#warehouse').val()){
    $.notify({
      title: "Erorr : ",
      message: "<strong>Warehouse</strong> Can't Be Empty",
      icon: 'fa fa-times' 
    },{
      type: "danger",
      delay: 1000
    });
    $('#warehouse').select2('open');
    return false;
  }

  let dialogshown = function(){
    var id  = $('#warehouse').val();
    var tbl = $('#table-browse_barang');
    pr.select_items_table = tbl.DataTable({
      serverSide  : true,
      fixedHeader : true,

      ajax        : {
        method : "GET",
        data : {
          id: id
        },
        url: baseUrl + 'stok/trn_stok/get_items_by_warehouse',
        
      },
      columns     : [
        {
          "data"   : "items_id",
          "visible": false
        },
        {"data"   : "items_code"},
        {"data"   : "items_name"},
        {
          "data"  : "current_stock",
          "render"  : $.fn.dataTable.render.number( ',', '.', 2 )
        }
      ],
      drawCallback: function( settings ) {
        var api = this.api();

        // Output the data for the visible rows to the browser's console
        api.$('td').click( function () {
          let item_data   = {};
          let items_code  = api.row($(this).parents('tr')).data().items_code;
          let items_name  = api.row($(this).parents('tr')).data().items_name;
          let items_id    = api.row($(this).parents('tr')).data().items_id;
          let items_price = api.row($(this).parents('tr')).data().items_price;
          let current_stock=api.row($(this).parents('tr')).data().current_stock;

          $('#id_items').val(items_name);
          $('#id_items').attr('data-code', items_code);
          $('#id_items').attr('data-id', items_id);
          $('#id_items').attr('data-stock', current_stock);
          $('#item_qty').attr('max',current_stock);
          

          //close modal
          pr.view_barang.close();
          $("#item_qty").focus();
        });
      },
      initComplete: function() {
          var $searchInput = $('div.dataTables_filter input');

          $searchInput.unbind();

          $searchInput.bind('keyup', function(e) {
            if(this.value.length > 3 || this.value.length == 0) {
                pr.select_items_table.search( this.value ).draw();
            }
          });
      }
    }); // DataTables()
    $('div.dataTables_filter input').focus();
  }

  pr.view_barang = BootstrapDialog.show({
    title     : 'Select Items',
    nl2br     : false,
    //cssClass: 'master_promo_dialog',
    size      : 'size-wide',
    message   : $('#content_browse_barang').html(),
    draggable : false,
    buttons   : [
      {
        label    : 'Tutup',
        cssClass : 'btn-info',
        action   : function(s){s.close(); }
      }
    ],
    onshown: dialogshown
  });
  
};

pr = {
  data: {},
  processed: false,
  items: [],
  init: function(){
    $('#date_pr').datepicker({
      format: "dd/mm/yyyy",
      autoclose: true,
      todayHighlight: true
    });

    $("#item_qty").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 1, 'digitsOptional': false, 'placeholder': '0'});

    $("#item_id").select2().on('select2:select',function(e){});
    // $("#warehouse").select2().on('select2:select',function(e){});
    $("#dept").select2().on('select2:select',function(e){});
    $("#os_kind").select2().on('select2:select',function(e){});

    $("#company").select2().on('select2:select',function(e){});

    $("#warehouse").select2().on('select2:select',function(e){
      var id = $('#warehouse').val();
        $.ajax({
          url : baseUrl+'stok/trn_stok/get_stok',
          method : "POST",
          data : {id: id},
          async : false,
          dataType : 'json',
          success: function(data){
            var html = '';
            var i;
            html += '<option value="0" > - </option>';
            for(i=0; i<data.length; i++){
              html += '<option value="'+data[i].items_id+'" data-name="'+data[i].items_name+'" data-stok="'+data[i].current_stock+'">'+data[i].items_name+' - '+data[i].current_stock+'</option>';
            }
            $('#item_id').html(html);
          }
        })
    });

    this.grids = $('#detail').DataTable({
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        "language": {
            "emptyTable": "No Data"
        },
        "columnDefs": [
          { // disable sorting on column process buttons
            "targets": [1,2,3,4],
            "orderable": false,
          },
          { 
            "targets": [0],
            "visible": false,
            "searchable": false
          }
        ],
        columns: [
          { data: 'item_id'},
          { data: 'item_name'}, 
          { data: 'item_qty', className: "text-right"}, 
          { data: 'item_info'}, 
          { data: 'act', className: "text-center" }
        ],
    });

    this._set_items($('#sup_items').val());
    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  add_items: function(e){
    e.preventDefault();
    
    if($('#item_id').val()==0){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $('#item_id').select2('open');
      return false;
    }

    if(!$('#item_qty').val()){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Qty</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $("#item_qty").focus();
      return false;
    }    

    var tes1 = $('#item_qty').val().toString();
    var tes2 = $('#id_items').attr('data-stock').toString();

    if(parseInt($('#item_qty').val())>parseInt($('#id_items').attr('data-stock'))){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Qty</strong> Greater than Stock in Warehouse",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $("#item_qty").focus();
      return false;
    }

    // if($('#item_id option:selected').attr('data-stok')<$('#item_qty').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "<strong>Qty</strong> Lebih Besar dari Stok.",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $("#item_qty").focus();
    //   return false;
    // }
    // debugger

    // let stok      = $('#item_id option:selected').attr('data-stok');
    // let item_id   = $('#item_id').val();
    // let item_name = $('#item_id option:selected').attr('data-name');
    let stok      = $('#id_items').attr('data-stock');
    let item_id   = $('#id_items').attr('data-id');
    let item_name = $('#id_items').val();
    let item_qty  = $('#item_qty').val();
    let item_info = $('#item_info').val();


      if(item_id){
      data = {
        item_id : item_id,
        item_name : item_name,
        item_qty : item_qty,
        item_info : item_info
      };

      pr._addtogrid(data);
      pr._clearitem();
      pr._focusadd();

    }
  },

  _addtogrid: function(data){
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    data.act = '<button item-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> already in the list",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'stok/outgoing_stok/add_item',
      data: {
        item_id   : data.item_id,
        item_name : data.item_name,
        item_qty  : data.item_qty,
        item_info : data.item_info
      }
    });
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_info : i.item_info
      };
      pr._addtogrid(data);
      pr._clearitem();
      pr._focusadd();
    });
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#id_items').val('');
    // $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_info').val('');
  },

  _focusadd: function(){
    $('#id_items').focus();
  },

  _removefromgrid: function(el){
    let id = $(el).attr('item-id');
    pr.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'stok/outgoing_stok/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },

  save: function(e){
    e.preventDefault();

    // if(!$('#company').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "<strong>Company</strong> Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#company').select2('open');
    //   return false;
    // }
    // if(!$('#warehouse_destination').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "<strong>Warehouse Destination</strong> Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#warehouse_destination').select2('open');
    //   return false;
    // }
    // if(!$('#os_kind').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "<strong>Outgoing Stock Kind</strong> Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#os_kind').select2('open');
    //   return false;
    // }

    if(!$('#warehouse').val()){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Warehouse</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $('#warehouse').select2('open');
      return false;
    }

    if(!$('#requester').val()){
      $.notify({
        title: "Erorr : ",
        message: "<strong>Requester</strong> Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $("#requester").focus();
      return false;
    }
    $('#save').prop("disabled",true);
    $.ajax({
      url: baseUrl+'stok/stock_use/form_act',
      type : "POST",  
      data: {
        company_code: $('#company').val(),
        company_name: $('#company option:selected').attr('data-name'),
        date      : $('#date').val(),
        warehouse : $('#warehouse').val(),
        dept      : $('#dept').val(),
        os_kind   : 5,
        requester : $('#requester').val(),
        info      : $('#info').val()
      },
      success : function(resp){
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          $.notify({
            message: 'Data Gagal disimpan'
          },{
            type: 'danger'
          });
          return false;

        } else {
          $.notify({
            message: 'Data successfully saved with document number '+resp.status
          },{
            type: 'info'
          });

          setTimeout(function () {
            window.location.href = baseUrl+'stok/stock_use/'; 
          }, 2000);
        }
      }
    });

  }
};

pr.init();
</script>