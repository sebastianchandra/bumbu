<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Stok Masuk</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Stok Masuk</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
        </nav>
      </div>
    </div>
  </div>
</div>

<!-- <form class="form-horizontal" method="post" action="<?php echo base_url().'stok/incoming_stok/form_act'; ?>"> -->
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal" method="post" action="<?php echo base_url().'stok/incoming_stok/form_act'; ?>">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="is_date" name="is_date" value="<?php echo substr(dbnow(),0,10); ?>">
                <!-- <input type="hidden" class="form-control"  value="<?php echo substr(dbnow(),0,10); ?>"> -->
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tujuan</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="warehouse_project" name="warehouse_project">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' >".$value->warehouse_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Referensi</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="referensi_id" name="referensi_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_referensi as $key => $value) {
                    echo "<option value='".$value->doc_no."' data-doc_id='".$value->doc_id."' data-doc_no='".$value->doc_no."' >".$value->doc_no."</option>";
                  }
                  ?>
                </select>
                <input type="hidden" id="no_referensi" name="no_referensi">
                <input type="hidden" id="tdetail" name="tdetail">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Pengirim</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="sender_pic" name="sender_pic">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Penerima</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="receiver_pic" name="receiver_pic">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home">Detail Barang</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1">Detail Biaya</a>
                  </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div id="home" class="container tab-pane active"><br>
                    <!-- <h3>Detail Barang</h3> -->
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered" id="detail_popup">
                        <thead>
                          <tr>
                            <th>Nama Barang</th>
                            <th width="15%">Qty</th>
                            <th width="15%">Qty Barang Masuk</th>
                            <th width="15%">Jumlah Terima</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                  <div id="menu1" class="container tab-pane fade"><br>
                    <div class="form-group row">
                      <div class="col-md-2">
                        <p>Nama Biaya</p>
                      </div>
                      <div class="col-md-3">
                        <input type="text" class="form-control" id="cost_name" name="cost_name">
                        <input type="hidden" class="form-control" id="cost_id" name="cost_id" value="1">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-2">
                        <p>Nominal</p>
                      </div>
                      <div class="col-md-2">
                        <input type="text" class="form-control price" id="cost_value" name="cost_value">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-2"></div>
                      <div class="col-md-5">
                        <a class="btn btn-primary btn-sm" id="add-items">Tambah Biaya</a>
                        <!-- <button type="button" class="btn btn-primary btn-sm" id="add-items">Tambah Biaya</button> -->
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered" id="detail">
                        <thead>
                          <tr>
                            <th>Id Biaya</th>
                            <th>Nama Biaya</th>
                            <th width="15%">Nominal</th>
                            <th width="15%">Action</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-sm" id="save" value="Simpan">
                <!-- <button class="btn btn-primary btn-sm" type="submit" id="save">Save</button> -->
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('stok/incoming_stok'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- </form> -->

<script type="text/javascript">

$("#warehouse_project").select2();

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

$('#save').click(function(){
  
  var i;
  var tdetail     = $('#tdetail').val();

  for (i = 0; i < tdetail; i++) {
    var qty_terima      = parseFloat($('#qty_terima'+i).val().replace(/\,/g, ''));
    var items_qty       = parseFloat($('#items_qty'+i).val().replace(/\,/g, ''));
    var items_name      = $('#items_name'+i).val();

    if(qty_terima > items_qty){
      toastr.error("Penerimaan barang lebih besar dari Permintaan <strong>"+items_name+"</strong> ", 'Alert', {"positionClass": "toast-top-center"});
      $("#qty_terima"+i).focus();
      return false;
    }

  }

  if(!$('#warehouse_project').val()){
    toastr.error("<strong>Lokasi</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#warehouse_project').select2('open');
    return false;
  }

  if(!$('#referensi_id').val()){
    toastr.error("<strong>Referensi</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#referensi_id').select2('open');
    return false;
  }

  // if(!$('#supplier_id').val()){
  //   toastr.error("<strong>Supplier Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#supplier_id').select2('open');
  //   return false;
  // }
});

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){    
    $("#warehouse_project").select2();
    $("#items_unit").select2();
    $("#supplier_id").select2();
    $("#item_id").select2();

    $("#referensi_id").select2().on('select2:select', function (e) {
      var idDoc     = $('#referensi_id option:selected').attr('data-doc_id');
      var noDoc     = $('#referensi_id option:selected').attr('data-doc_no');
      $.get({
        url: baseUrl + 'stok/incoming_stok/show_detail/'+noDoc+'/'+idDoc,
        success: function(resp){

          detail_popup.clear().draw();
          var dataSrc = JSON.parse(resp.detail);                 
          detail_popup.rows.add(dataSrc).draw(false);
          $("#tdetail").val(resp.tdetail);
          $("#remarks").val(resp.theader.remarks);
        }
      });

      $("#no_referensi").val(noDoc);
    });

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [0,1,2,3],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'cost_id'},
        { data: 'cost_name'}, 
        { data: 'cost_value', className: "text-right"}, 
        { data: 'act', className: "text-center" }
      ],
    });

    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  add_items: function(e){
    e.preventDefault();

    if($('#cost_name').val()==''){
      toastr.error("<strong>Nama Biaya</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#item_id').select2('open');
      return false;
    }

    if(!$('#cost_value').val()){
      toastr.error("<strong>Nominal</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#item_qty").focus();
      return false;
    } 

    let cost_id       = parseInt($('#cost_id').val());
    let cost_name     = $('#cost_name').val();
    let cost_value    = $('#cost_value').val();

    if(cost_id){
      data = {
        cost_id       : cost_id,
        cost_name     : cost_name,
        cost_value    : cost_value
        };

        pr._addtogrid(data);
        pr._clearitem(cost_id);

    }
  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.cost_id).index();
    //
    $('#id').val(data.cost_id);

    data.act = '<button det-id="'+data.cost_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.cost_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw(); 
    }else{ 
      toastr.error("<strong>Barang</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'stok/incoming_stok/add_item',
      data: {
        cost_id    : data.cost_id,
        cost_name  : data.cost_name,
        cost_value : data.cost_value.replace(/\,/g, '')
      }
    });

  },

  _clearitem: function(cost_id){
    var id_cost        = cost_id + 1;
    $('#cost_id').val(id_cost);
    $('#cost_name').val('');
    $('#cost_value').val('');
  },

  _removefromgrid: function(el){
    let id = $(el).attr('det-id');
    pr.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'stok/incoming_stok/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },


};

pr.init();

</script>