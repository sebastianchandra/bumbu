<div class="app-title">
  <div>
    <h1>Process Purchase Order</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Stock</li>
      <li class="breadcrumb-item">Incoming Stock</li>
      <li class="breadcrumb-item active">Process Purchase Order</li>
    </ul></p>
  </div>
</div>
<?php echo form_open(base_url()."stok/incoming_stok/process_po_act"/*, array('id' => 'bform')*/); ?>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <form class="form-horizontal">
          <div class="form-approve row">
            <label class="control-label col-md-2">Nomor Purchase Order</label>
            <div class="col-md-3">
              : <?php echo $header->po_no; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Company</label>
            <div class="col-md-3">
              : <?php echo $header->company_name; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Date</label>
            <div class="col-md-8">
              : <?php echo tanggal($header->po_date); ?>
              <input type='hidden' id="po_no" name="po_no" value="<?php echo $header->po_no; ?>">
              <input type='hidden' id="company_name" name="company_name" value="<?php echo $header->company_name; ?>">
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Requester</label>
            <div class="col-md-3">
              : <?php echo $header->requester; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Project Name</label>
            <div class="col-md-5">
              : <?php echo $header->project_name; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Supplier</label>
            <div class="col-md-3">
              : <?php echo $header->supplier_name; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Delivery Address</label>
            <div class="col-md-5">
              : <?php echo $header->delivery_address; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Info</label>
            <div class="col-md-8">
              : <?php echo $header->po_info; ?>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Warehouse</label>
            <div class="col-md-5">
              <select class="form-control" id='warehouse_id' name="warehouse_id">
                <option value=""> - </option>
                <?php 
                foreach ($data_warehouse as $key => $value) {
                  echo '<option value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Sender</label>
            <div class="col-md-5"><input class="form-control" type="text" placeholder="Sender" id="sender" name="sender"></div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Receiver</label>
            <div class="col-md-5"><input class="form-control" type="text" placeholder="Receiver" id="receiver" name="receiver"></div>
          </div>
          <div class="form-approve row">
            <label class="control-label col-md-2">Remarks</label>
            <div class="col-md-9"><input class="form-control" type="text" placeholder="Remarks" id="remarks" name="remarks_header"></div>
          </div>
        </form>
      </div>
      <div class="tile-footer">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="detail">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Info</th>
                  <th width="9%">IS Qty</th>
                  <th width="9%">PO Qty</th>
                  <th width="12%">Current Qty</th>
                  <th width="24%">Remaks</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $max=0;
                foreach ($detail as $key => $value) {
                  $max = $value->qty-$value->qty_current;
                  $qty_current = ($value->qty_current=='')? '0' : $value->qty_current;
                ?>
                <tr>
                  <td><?php echo $value->items_name; ?>
                  <input class="form-control" type="hidden" placeholder="Current Qty" name="items_name[]" id="items_name" value="<?php echo $value->items_name; ?>">
                  <input class="form-control" type="hidden" placeholder="Current Qty" name="items_id[]" id="items_id" value="<?php echo $value->items_id; ?>">
                  <input class="form-control" type="hidden" placeholder="Remarks" id="qty" name="qty[]" value="<?php echo $value->qty; ?>">
                  <input class="form-control" type="hidden" placeholder="Remarks" id="qty_current" name="qty_current[]" value="<?php echo $max; ?>"></td>
                  <td><?php echo $value->remarks; ?></td>
                  <td align="right"><?php echo $value->qty_current; ?></td>
                  <td align="right"><?php echo $value->qty; ?></td>
                  <td><input class="form-control" type="number" placeholder="Current Qty" id="current" name="current[]" min="0" max="<?php echo $max; ?>" value="<?php echo $max; ?>"></td>
                  <td><input class="form-control" type="text" placeholder="Remarks" id="remarks" name="remarks[]"></td>
                </tr>
                <?php 
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="tile-footer">
        <div class="row">
          <div class="col-md-8 col-md-offset-3">
            <button class="btn btn-primary" type="submit" onClick="divFunction()" id='bform'><i class="fa fa-check"></i> Process</button>
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>stok/incoming_stok#po"><i class="fa fa-reply"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close();?>
<?php //test($new_pr,0); ?>

<script type="text/javascript">
  $('#bform').click(function(){
    if(!$('#warehouse_id').val()){
      $.notify({
        title: "Erorr : ",
        message: "Warehouse Can't Be Empty",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      $('#warehouse_id').select2('open');
      return false;
    }
  });

  // function divFunction(){
    // if(!$('#warehouse_id').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "Warehouse Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#warehouse_id').select2('open');
    //   return false;
    // }else{
    //   return true;
    // }

    // if(!$('#sender').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "Sender Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#sender').focus();
    //   return false;
    // }
    // if(!$('#receiver').val()){
    //   $.notify({
    //     title: "Erorr : ",
    //     message: "Receiver Can't Be Empty",
    //     icon: 'fa fa-times' 
    //   },{
    //     type: "danger",
    //     delay: 1000
    //   });
    //   $('#receiver').focus();
    //   return false;
    // }
  // }
  pr = {
    data: {},
    processed: false,
    items: [],
    init: function(){
      $("#warehouse_id").select2().on('select2:select',function(e){});

      $('#unapprove').click(function(){
        dataID = $(this).data('id');
        dataNO = $(this).data('no');
        dataAPP= 0;
      //alert('test dong '+data_id+' '+data_app);
      pr.action_pr();
    });

      this.grids = $('#detail').DataTable({
        "paging": false, 
        "bLengthChange": false, // disable show entries dan page
        "bFilter": false,
        "bInfo": false, // disable Showing 0 to 0 of 0 entries
        "bAutoWidth": false,
        "language": {
          "emptyTable": "No Data"
        }
      });

    },

    _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    // data.act = '<button item-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      $.notify({
        title: "Erorr : ",
        message: "<strong>Items Name</strong> already in the list",
        icon: 'fa fa-times' 
      },{
        type: "danger",
        delay: 1000
      });
      return false;
    }
  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_price').val('');
    $('#item_info').val('');
  },

  action_pr: function(e){

    if(dataAPP==1){
      var message_info = 'Data di Approve';
      var type_box = 'info';
      var type_dialog =  BootstrapDialog.TYPE_SUCCESS;
      var btn_simpan = 'btn-primary'
    }else{
      var message_info = 'Data Not Approve';
      var type_box = 'danger';
      var type_dialog = BootstrapDialog.TYPE_DANGER;
      var btn_simpan = 'btn-danger'
    }

    BootstrapDialog.show({
      title: 'NOTE '+dataNO,
      type : type_dialog,
      onshown: function(dialog) {
        $('#note').focus();
      },
      message: $('<textarea id="note" class="form-control" placeholder="Note ... "></textarea>'),
      closable: false,
      buttons: [
      {
        label: 'Batal', cssClass: 'btn',
        action: function(dia){
          dia.close();
        }
      },
      {
          label: 'Simpan', cssClass: btn_simpan, id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            
            var keterangan = $('#note').val();
            var data_id    = dataID;
            var data_no    = dataNO;
            var data_app   = dataAPP;         

            if(!$('#note').val()){
              $.notify({
                title: "Erorr : ",
                message: "<strong>Note</strong> Can't Be Empty",
                icon: 'fa fa-times' 
              },{
                type: "danger",
                delay: 1000
              });
              return false;
            }

            $.ajax({
              data: {
                keterangan : keterangan,
                data_id    : data_id,
                data_no    : data_no,
                data_app   : data_app
              },

              type : "POST",
              url: baseUrl+'transaction/purchase_requisition/form_approve_act',
              success : function(resp){

                if(resp.kd_trans == 'ERROR INSERT' || resp.kd_trans == false) {
                  alert("Store tujuan Can't Be Empty");
                  return false;

                } else {
                  $.notify({
                    icon: "glyphicon glyphicon-save",
                    message: message_info
                  },{
                    type: type_box,
                    z_index : 1100,
                    onClosed: function(){ location.reload();}
                  });

                  // setTimeout(function () {
                  //     window.location.href = baseUrl+'transaction/purchase_requisition/view_approve'; //will redirect to google.
                  //   }, 2000);
                }
              }
            });

          }
        }
        ],
      });

  }
};

pr.init();
</script>