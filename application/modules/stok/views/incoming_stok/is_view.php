<?php 
if($this->session->flashdata('alert')!=''){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Stok Masuk</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Stok Masuk</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a href="<?php echo base_url('stok/incoming_stok/form_others'); ?>" class="btn btn-warning">Input Lain-Lain</a>
          <a href="<?php echo base_url('stok/incoming_stok/form'); ?>" class="btn btn-primary">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="15%">Stok Masuk</th>
                    <th>Company Name</th>
                    <th>Referensi</th>
                    <th>Tanggal</th>
                    <th>Gudang</th>
                    <th>Jenis</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_is as $key => $value) {
                    if($value->is_kind==1){
                      $kind   = 'Transfer';
                    }elseif($value->is_kind==2){
                      $kind   = 'Others';
                    }elseif($value->is_kind==3){
                      $kind   = 'Adjustment';
                    }
                  ?>
                  <tr>
                    <td>
                      <?php 
                      if($value->is_kind==1){
                      ?>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                      data-id="<?php echo $value->is_id; ?>" 
                      data-is="<?php echo $value->is_no; ?>"><?php echo $value->is_no; ?></a></strong>
                      <?php
                      }else{
                      ?>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail_others(this)" 
                      data-id="<?php echo $value->is_id; ?>" 
                      data-is="<?php echo $value->is_no; ?>"><?php echo $value->is_no; ?></a></strong>
                      <?php
                      }
                      ?>
                    </td>
                    <td><?php echo $value->company_name; ?></td>
                    <td><?php echo $value->doc_ref; ?></td>
                    <td><?php echo tgl_singkat($value->is_date); ?></td>
                    <td><?php echo ($value->warehouse_id!='0')? $value->warehouse_name : $value->project_name; ?></td>
                    <td><?php echo $kind; ?></td>
                    <td><?php echo $value->is_status; ?></td>
                    <td align="center">
                      <?php if($value->is_status=='Reject'){
                        echo '<button id="reject" class="btn btn-warning btn-xs"  disabled="">Batal</button> ';
                        echo '<button class="btn btn-info btn-xs" disabled="">Print</button> ';
                        if($value->is_kind!='1'){
                          echo '<a href="'.base_url('stok/incoming_stok/copy_input/'.$value->is_id).'" class="btn btn-success btn-xs">Copy</a>';
                        }
                      }elseif(substr($value->doc_ref,0,2)=='OS'){
                        echo '<button id="reject_os" class="btn btn-warning btn-xs" type="submit">Batal</button> ';
                        echo '<a target="_blank" href="'.base_url('stok/incoming_stok/view_print/'.$value->is_id).'" class="btn btn-info btn-xs">Print</a>';
                      }else{
                        echo '<button id="reject" data-is_no="'.$value->is_no.'"" data-is_id="'.$value->is_id.'" class="btn btn-warning btn-xs" type="submit">Batal</button> ';
                        echo '<a target="_blank" href="'.base_url('stok/incoming_stok/view_print/'.$value->is_id).'" class="btn btn-info btn-xs">Print</a>';
                      }
                      ?>
                    </td>
                  </tr>
                  <?php 
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myPr" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Select Purchase Requisition</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th>PR No</th>
                    <th width="15%">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Next</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Stok Masuk</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Is No</strong></p>
          </div>
          <div class="col-md-4">
            <div id="is_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Date</strong></p>
          </div>
          <div class="col-md-4">
            <div id="is_date"></div>
          </div>          
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Warehouse</strong></p>
          </div>
          <div class="col-md-4">
            <div id="to"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_name"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Doc Ref</strong></p>
          </div>
          <div class="col-md-4">
            <div id="doc_ref"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="is_status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-10">
            <div id="remarks"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th width="15%">Qty</th>
                    <th width="20%">Price</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModalOthers" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Stok masuk</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Is No</strong></p>
          </div>
          <div class="col-md-4">
            <div id="ois_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Date</strong></p>
          </div>
          <div class="col-md-4">
            <div id="ois_date"></div>
          </div>          
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Warehouse</strong></p>
          </div>
          <div class="col-md-4">
            <div id="oto"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="ocompany_name"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Doc Ref</strong></p>
          </div>
          <div class="col-md-4">
            <div id="odoc_ref"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="ois_status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-10">
            <div id="oremarks"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup_others">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th width="15%">Qty</th>
                    <th width="20%">Price</th>
                    <th width="15%">Disc</th>
                    <th width="35%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }

  $.ajax({
    url: baseUrl+'transaction/purchase_order/pr_selected',
    type : "POST",  
    data: {
      values      : values
    },
    success : function(resp){        
      setTimeout(function () {
        window.location.href = baseUrl+'transaction/purchase_order/po_input'; 
      }, 100);
    }
  });
  // alert(values);
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1,2,3 ] 
    }
  ]
});

var detail_popup_others = $('#detail_popup_others').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1,2,3,4 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_pr(e){  
  $.get({
    url: baseUrl + 'transaction/purchase_order/view_pr/',
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });

  $('#myPr').modal('show'); 
}

function show_detail(e){
  var idIs     = $(e).data('id');
  var noIs     = $(e).data('is');

  $.get({
    url: baseUrl + 'stok/incoming_stok/view_popup/'+idIs,
    success: function(resp){
      $('#company_name').text(resp.header.company_name);
      $('#is_no').text(resp.header.is_no);
      $('#is_date').text(tanggal_indonesia(resp.header.is_date));
      $('#doc_ref').text(resp.header.doc_ref);
      $('#remarks').text(resp.header.remarks);
      $('#is_status').text(resp.header.is_status);
      if(resp.header.warehouse_id==0){
        $('#to').text(resp.header.project_name);
      }else{
        $('#to').text(resp.header.warehouse_name);
      }
      // $('#supplier_name').text(resp.header.supplier_name);
      // $('#project_name').text(resp.header.project_name);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

function show_detail_others(e){
  var idIs     = $(e).data('id');
  var noIs     = $(e).data('is');

  $.get({
    url: baseUrl + 'stok/incoming_stok/view_popup_others/'+idIs,
    success: function(resp){
      $('#ocompany_name').text(resp.header.company_name);
      $('#ois_no').text(resp.header.is_no);
      $('#ois_date').text(tanggal_indonesia(resp.header.is_date));
      $('#odoc_ref').text(resp.header.doc_ref);
      $('#oremarks').text(resp.header.remarks);
      $('#ois_status').text(resp.header.is_status);
      if(resp.header.warehouse_id==0){
        $('#oto').text(resp.header.project_name);
      }else{
        $('#oto').text(resp.header.warehouse_name);
      }
      // $('#supplier_name').text(resp.header.supplier_name);
      // $('#project_name').text(resp.header.project_name);

      detail_popup_others.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup_others.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModalOthers').modal('show'); 
}

$(document).ready(function(){
  
  $('#reject_os').click(function(){
    toastr.success("Pembatalan Stok Masuk dengan Referensi Stok Keluar dilakukan di View Stok Keluar. <a href='"+baseUrl+"stok/incoming_stok'><strong>Klik</strong></a>.", 'Alert', {"positionClass": "toast-top-center"});
  });

  var oTable = $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  // minDateFilter = "";
  // maxDateFilter = "";

  // $('#search').click(function(date){

  //   // minDateFilter = new Date().getTime();;
  //   minDateFilter = new Date($('#datepicker_from').val()).getTime();

  //   // maxDateFilter = new Date(date).getTime();;
  //   maxDateFilter = new Date($('#datepicker_to').val()).getTime();


  //   oTable.fnDraw();
  // });

  // $.fn.dataTableExt.afnFiltering.push(
  //   function(oSettings, aData, iDataIndex) {
  //     if (typeof aData._date == 'undefined') {
  //       aData._date = new Date(aData[3]).getTime();
  //     }

  //     if (minDateFilter && !isNaN(minDateFilter)) {
  //       if (aData._date < minDateFilter) {
  //         return false;
  //       }
  //     }

  //     if (maxDateFilter && !isNaN(maxDateFilter)) {
  //       if (aData._date > maxDateFilter) {
  //         return false;
  //       }
  //     }

  //     return true;
  //   }
  // );

  $('#vtable').on('click','#reject', function(e){

    var is_id  = $(this).data('is_id');
    var is_no  = $(this).data('is_no');

    toastr.warning(
      'Apakah anda Ingin Membatalkan '+is_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-is_no="'+is_no+'" data-is_id="'+is_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var is_id  = $(e).data('is_id');
  var is_no  = $(e).data('is_no');

  $.ajax({
    data: {
      is_id  : is_id,
      is_no  : is_no
    },
    type : "POST",
    url: baseUrl+'stok/incoming_stok/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data berhasil di batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'stok/incoming_stok'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!-- 
1. Memunculkan Remarks pada Popup detail Incoming Stock
2. Penambahan Print IS
-->