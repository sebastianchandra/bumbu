<?php 
//test($detail,0);
?>
<section class="content-body" style="padding-top:0;">
    <div class="tile-body">
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">IS Number</label>
                    <div class="col-md-7">: <?php echo $header->is_no; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Referensi</label>
                    <div class="col-md-7">: <?php echo $header->doc_ref; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Date</label>
                    <div class="col-md-7">: <?php echo tanggal($header->is_date); ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Company</label>
                    <div class="col-md-7">: <?php echo $header->company_name; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Sender</label>
                    <div class="col-md-7">: <?php echo $header->sender_pic; ?></div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Receiver</label>
                    <div class="col-md-7">: <?php echo $header->receiver_pic; ?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-popup row">
                    <label class="control-label col-md-4">Info</label>
                    <div class="col-md-7">: <?php echo $header->remarks; ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-bordered" id="detail">
            <thead>
                <tr align="center">
                    <th>Name</th>
                    <th>Info</th>
                    <th width="8%">Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $total = 0;
                $subtotal = 0;
                foreach ($detail as $key => $value) {
                ?>
                <tr>
                    <td class="popup"><?php echo $value->items_name; ?></td>
                    <td class="popup"><?php echo $value->remarks; ?></td>
                    <td align="right" class="popup"><?php echo money($value->qty); ?></td>
                </tr>
                <?php 
                }
                ?>
            </tbody>
        </table>
    </div>
</section>