<?php 
if($this->session->flashdata('alert')!=''){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-sm-4">
    <h2>View Saldo Awal</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?php echo base_url(); ?>">Home</a>
      </li>
      <li class="breadcrumb-item active">
        <strong>Saldo Awal</strong>
      </li>
    </ol>
  </div>
  <div class="col-sm-8">
    <div class="title-action">
      <a href="<?php echo base_url('stok/saldo_awal/form_others'); ?>" class="btn btn-info">Input</a>
      <!-- <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_pr(this)">Input</button> -->
    </div>
  </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <!-- <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
                  <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">...</div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
              </div> -->
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Nomor Saldo Awal</th>
                    <!-- <th width="20%">Company Name</th> -->
                    <th>Date</th>
                    <th>Gudang / Project</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_is as $key => $value) {
                  ?>
                  <tr>
                    <td>
                      <strong><a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                      data-doc_no="<?php echo $value->doc_no; ?>"><?php echo $value->doc_no; ?></a></strong>
                    </td>
                    <!-- <td><?php echo $value->company_name; ?></td> -->
                    <td><?php echo tgl_singkat($value->trn_date); ?></td>
                    <td><?php echo ($value->warehouse_id!='')? 'Gudang '.$value->warehouse_name : 'Project '.$value->project_name; ?></td>
                    <td><?php echo $value->activity; ?></td>
                    <td></td>
                  </tr>
                  <?php 
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myPr" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Select Purchase Requisition</h4>
      </div>
      <div class="modal-body">
        <div class="row-form">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th>PR No</th>
                    <th width="15%">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Next</button>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Saldo Awal</h4>
      </div>
      <div class="modal-body">
        <!-- <div class="row-form">
          <div class="col-md-2">
            <p><strong>Is No</strong></p>
          </div>
          <div class="col-md-4">
            <div id="is_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Date</strong></p>
          </div>
          <div class="col-md-4">
            <div id="is_date"></div>
          </div>          
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Project / Warehouse</strong></p>
          </div>
          <div class="col-md-4">
            <div id="to"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_name"></div>
          </div>
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Doc Ref</strong></p>
          </div>
          <div class="col-md-4">
            <div id="doc_ref"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="is_status"></div>
          </div>
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-10">
            <div id="remarks"></div>
          </div>
        </div> -->
        <div class="row-form">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th width="15%">Qty</th>
                    <!-- <th width="20%">Price</th>
                    <th width="25%">Subtotal</th> -->
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }

  $.ajax({
    url: baseUrl+'transaction/purchase_order/pr_selected',
    type : "POST",  
    data: {
      values      : values
    },
    success : function(resp){        
      setTimeout(function () {
        window.location.href = baseUrl+'transaction/purchase_order/po_input'; 
      }, 100);
    }
  });
  // alert(values);
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_pr(e){  
  $.get({
    url: baseUrl + 'transaction/purchase_order/view_pr/',
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });

  $('#myPr').modal('show'); 
}

function show_detail(e){
  var doc_no     = $(e).data('doc_no');

  $.get({
    url: baseUrl + 'stok/saldo_awal/view_popup/'+doc_no,
    success: function(resp){
      // $('#company_name').text(resp.header.company_name);
      // $('#is_no').text(resp.header.is_no);
      // $('#is_date').text(resp.header.is_date);
      // $('#doc_ref').text(resp.header.doc_ref);
      // $('#remarks').text(resp.header.po_info);
      // $('#is_status').text(resp.header.is_status);
      // if(resp.header.warehouse_id==0){
      //   $('#to').text(resp.header.project_name);
      // }else{
      //   $('#to').text(resp.header.warehouse_name);
      // }
      // $('#supplier_name').text(resp.header.supplier_name);
      // $('#project_name').text(resp.header.project_name);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var po_id  = $(this).data('po_id');
    var po_no  = $(this).data('po_no');

    toastr.warning(
      'Do you want to Reject '+po_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-po_no="'+po_no+'" data-po_id="'+po_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var po_id  = $(e).data('po_id');
  var po_no  = $(e).data('po_no');

  $.ajax({
    data: {
      po_id  : po_id,
      po_no  : po_no
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_order/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_order'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>