<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Saldo Awal</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Saldo Awal</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('transaction/purchase_requisition/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="row-form">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>
                <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_sa["items"]); ?>' required />
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="is_date" value="<?php echo substr(dbnow(),0,10); ?>">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Tujuan</p>
              </div>
              <div class="col-md-5">
                <input type="radio" value="warehouse" id="to_local" name="to_local"> Gudang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" value="project" id="to_local" name="to_local"> Project
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p></p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="warehouse_project" name="warehouse_project">
                  <!-- <option value="">Select</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' >".$value->warehouse_name."</option>";
                  }
                  ?> -->
                </select>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Nama Barang</p>
              </div>
              <div class="col-md-7">
                <select class="form-control" id="item_id">
                  <option value="">Select</option>
                  <?php 
                  // foreach ($data_items as $key => $value) {
                  //   echo "<option value='".$value->items_id."' data-name='".$value->items_name."'>".$value->items_name."</option>";
                  // }
                  ?>
                </select>
                <input type="hidden" class="form-control " id="id" name="id" value="0"/>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Quantity</p>
              </div>
              <div class="col-md-2">
                <input type="number" class="form-control" id="item_qty">
              </div>
            </div>
            <div class="row-form">
              <!-- <div class="col-md-2">
                <p>Harga</p>
              </div> -->
              <div class="col-md-3">
                <input type="hidden" class="form-control" id="price" value="0">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><!-- Info --></p>
              </div>
              <div class="col-md-5">
                <input type="hidden" class="form-control" id="item_info">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-5">
                <button type="button" class="btn btn-primary btn-sm" id="add-items">Tambah Barang</button>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th>Nama Barang</th>
                        <th width="8%">Quantity</th>
                        <th width="10%">Harga</th>
                        <th width="5%">Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('stok/saldo_awal'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
// $("#warehouse").select2();
$("#warehouse_project").select2().on('select2:select',function(e){
  var id          = $('#warehouse_project').val();
  var to_local    = $('#to_local:checked').val();
  $.ajax({
    url : baseUrl+'master/items/items_sa',
    method : "POST",
    data : {id: id,to_local: to_local},
    async : false,
    dataType : 'json',
    success: function(data){
      var html = '';
      var i;
      html += '<option value="" > - </option>';
      for(i=0; i<data.length; i++){
        html += '<option value="'+data[i].items_id+'" data-name="'+data[i].items_name+'">'+data[i].items_name+'</option>';
      }
      $('#item_id').html(html);
    }
  })
});

$('input:radio').change(function(){
  $("#warehouse_project").val(null).trigger("change");
  var to_local    = $('#to_local:checked').val();
  if(to_local == 'warehouse'){
    
    $.ajax({
      url : baseUrl+'master/warehouse/select_warehouse',
      method : "POST",
      data : {id: ''},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option value="'+data[i].warehouse_id+'" >'+data[i].warehouse_name+'</option>';
        }
        $('#warehouse_project').html(html);
      }
    })

  }else if(to_local == 'project'){
    
    $.ajax({
      url : baseUrl+'master/project/select_project',
      method : "POST",
      data : {id: ''},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option value="'+data[i].project_id+'" >'+data[i].project_name+'</option>';
        }
        $('#warehouse_project').html(html);
      }
    })

  }
}); 

function stok_barang(e){
  alert('tes');
  let nama = $(e).attr('data-name');
  alert(nama);
}

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#supplier_id").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,4],
          "orderable": false,
        },
        { 
          "targets": [0,3],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'item_id'},
        { data: 'item_name'}, 
        { data: 'item_qty', className: "text-right"}, 
        { data: 'price', className: "text-right"}, 
        { data: 'act', className: "text-center" }
      ],
    });

    this._set_items($('#sup_items').val());
    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  add_items: function(e){
    e.preventDefault();

    if($('#item_id').val()==''){
      toastr.error("<strong>Nama Barang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#item_id').select2('open');
      return false;
    }

    if(!$('#item_qty').val()){
      toastr.error("<strong>Quantity</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#item_qty").focus();
      return false;
    } 

    if(!$('#price').val()){
      toastr.error("<strong>Harga</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#price").focus();
      return false;
    } 

    let item_id   = $('#item_id').val();
    let item_name = $('#item_id option:selected').attr('data-name');
    let item_qty  = $('#item_qty').val();
    let item_info = $('#item_info').val();
    let price     = $('#price').val();
    let id        = parseInt($('#id').val());
    var id_det    = id + 1;


      if(item_id){
      data = {
        det_id : id_det,
        item_id : item_id,
        item_name : item_name,
        item_qty : item_qty,
        item_info : item_info,
        price : price
      };

      pr._addtogrid(data);
      pr._clearitem();

    }
  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    data.act = '<button det-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      toastr.error("<strong>Barang</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'stok/saldo_awal/add_item',
      data: {
        det_id    : data.det_id,
        item_id   : data.item_id,
        item_name : data.item_name,
        item_qty  : data.item_qty,
        item_info : data.item_info,
        price     : data.price
      }
    });
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        det_id    : i.det_id,
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_info : i.item_info,
        price     : i.price
      };
      pr._addtogrid(data);
      pr._clearitem();
    });
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_info').val('');
    $('#price').val('0');
  },

  _removefromgrid: function(el){
    let id = $(el).attr('det-id');
    pr.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'stok/saldo_awal/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },

  save: function(e){
    e.preventDefault();

    if(!$('#warehouse_project').val()){
      toastr.error("<strong>Lokasi Gudang / Project </strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#warehouse_project').select2('open');
      return false;
    }

    if($('#detail').DataTable().data().count()<1){
      toastr.error("<strong>Detail</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    // if(!$('#is_requester').val()){
    //   toastr.error("<strong>Requester</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    //   $("#is_requester").focus();
    //   return false;
    // }

    // if(!$('#project_id').val()){
    //   toastr.error("<strong>Project Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    //   $('#project_id').select2('open');
    //   return false;
    // }

    
    $('#save').prop("disabled",true);
    
    $.ajax({
      url: baseUrl+'stok/saldo_awal/form_others_act',
      type : "POST",  
      data: {
        company_name      : $('#company_id').val(),
        company_id        : $('#company_id').attr('data-company_id'),
        is_date           : $('#is_date').val(),
        is_requester      : $('#is_requester').val(),
        warehouse_project : $('#warehouse_project').val(),
        remarks           : $('#remarks').val(),
        to_local          : $('input[name=to_local]:checked').val()
        
      },
      success : function(resp){
        // debugger
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else {
          messages = 'Data Berhasil disimpan Dengan Nomor '+resp.is_no;
          messages += "<hr>";

          toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
          
          setTimeout(function () {
            window.location.href = baseUrl+'stok/saldo_awal/'; 
          }, 2000);
        }
      }
    });

  }

};

pr.init();

</script>