<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Barang Masuk</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Barang Masuk</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('transaction/purchase_requisition/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>

<form class="form-horizontal" method="post" action="<?php echo base_url().'stok/incoming_stok/form_act'; ?>">
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="row-form">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="is_date" name="is_date" value="<?php echo substr(dbnow(),0,10); ?>">
                <!-- <input type="hidden" class="form-control"  value="<?php echo substr(dbnow(),0,10); ?>"> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Tujuan</p>
              </div>
              <div class="col-md-5">
                <input type="radio" value="warehouse" id="to_local" name="to_local"> Gudang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" value="project" id="to_local" name="to_local"> Project
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p></p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="warehouse_project" name="warehouse_project">
                  <!-- <option value="">Select</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' >".$value->warehouse_name."</option>";
                  }
                  ?> -->
                </select>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Referensi</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="referensi_id" name="referensi_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_referensi as $key => $value) {
                    echo "<option value='".$value->doc_no."' data-doc_id='".$value->doc_id."' data-doc_no='".$value->doc_no."' >".$value->doc_no."</option>";
                  }
                  ?>
                </select>
                <input type="hidden" id="no_referensi" name="no_referensi">
                <input type="hidden" id="tdetail" name="tdetail">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Pengirim</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="sender_pic" name="sender_pic">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p>Penerima</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="receiver_pic" name="receiver_pic">
              </div>
            </div>
            <!-- <div class="row-form">
              <div class="col-md-2">
                <p>Supplier Name</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="supplier_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_supplier as $key => $value) {
                    echo "<option value='".$value->supplier_id."' data-supplier_name='".$value->supplier_name."'>".$value->supplier_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div> -->
            <!-- <div class="row-form">
              <div class="col-md-2">
                <p>Delivery Address</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="delivery_address">
              </div>
            </div> -->
            <div class="row-form">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks">
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail_popup">
                    <thead>
                      <tr>
                        <th>Nama Barang</th>
                        <th width="15%">Qty</th>
                        <th width="15%">Qty Barang Masuk</th>
                        <th width="15%">Jumlah Terima</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-sm" id="save" value="Simpan">
                <!-- <button class="btn btn-primary btn-sm" type="submit" id="save">Save</button> -->
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('stok/incoming_stok'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
// $("#cek_warehouse").hide();
// $("#cek_project").hide();

$("#warehouse_project").select2();

$('input:radio').change(function(){
  $("#warehouse_project").val(null).trigger("change");
  var to_local    = $('#to_local:checked').val();
  if(to_local == 'warehouse'){
    
    $.ajax({
      url : baseUrl+'master/warehouse/select_warehouse',
      method : "POST",
      data : {id: ''},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option value="'+data[i].warehouse_id+'" >'+data[i].warehouse_name+'</option>';
        }
        $('#warehouse_project').html(html);
      }
    })

  }else if(to_local == 'project'){
    
    $.ajax({
      url : baseUrl+'master/project/select_project',
      method : "POST",
      data : {id: ''},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option value="'+data[i].project_id+'" >'+data[i].project_name+'</option>';
        }
        $('#warehouse_project').html(html);
      }
    })

  }
}); 

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

$('#save').click(function(){

  var i;
  var tdetail     = $('#tdetail').val();

  for (i = 0; i < tdetail; i++) {
    var qty_terima      = parseInt($('#qty_terima'+i).val());
    var items_qty       = parseInt($('#items_qty'+i).val());
    var items_name      = $('#items_name'+i).val();

    if(qty_terima > items_qty){
      toastr.error("Penerimaan barang lebih besar dari Permintaan <strong>"+items_name+"</strong> ", 'Alert', {"positionClass": "toast-top-center"});
      $("#qty_terima"+i).focus();
      return false;
    }

  }

  if(!$('#warehouse_project').val()){
    toastr.error("<strong>Lokasi</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#warehouse_project').select2('open');
    return false;
  }

  if(!$('#referensi_id').val()){
    toastr.error("<strong>Referensi</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#referensi_id').select2('open');
    return false;
  }

  // if(!$('#supplier_id').val()){
  //   toastr.error("<strong>Supplier Name</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#supplier_id').select2('open');
  //   return false;
  // }
});

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    
    $("#warehouse_project").select2();
    $("#items_unit").select2();
    $("#referensi_id").select2().on('select2:select', function (e) {

      var idDoc     = $('#referensi_id option:selected').attr('data-doc_id');
      var noDoc     = $('#referensi_id option:selected').attr('data-doc_no');

      $.get({
        url: baseUrl + 'stok/incoming_stok/show_detail/'+noDoc+'/'+idDoc,
        success: function(resp){

          detail_popup.clear().draw();
          var dataSrc = JSON.parse(resp.detail);                 
          detail_popup.rows.add(dataSrc).draw(false);
          $("#tdetail").val(resp.tdetail);
        }
      });

      $("#no_referensi").val(noDoc);
    });
    $("#supplier_id").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'item_id'},
        { data: 'item_name'}, 
        { data: 'item_qty', className: "text-right"}, 
        { data: 'item_info',"visible": false}, 
        { data: 'act', className: "text-center" }
      ],
    });

    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  }

};

pr.init();

</script>