<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Konversi Unit</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Konversi Unit</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a href="<?php echo base_url('stok/unit_conversion/form'); ?>" class="btn btn-primary">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal" method="post" action="<?php echo base_url().'stok/incoming_stok/form_act'; ?>">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Gudang</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id='warehouse_id'>
                                    <option value=""> - </option>
                                    <?php 
                                    foreach ($data_warehouse as $key => $value) {
                                        echo '<option data-name="'.$value->warehouse_name.'" value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Barang</p>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id='items_id'>
                                </select>
                                <input type="hidden" class="form-control price" id="items_unit">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Qty Barang</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control price" id="items_qty">
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <div class="col-md-2">
                                <p>Harga Barang (Satuan)</p>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="date_of_birth">
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Untuk Barang</p>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="items_id_to">
                                    <option value="">Select</option>
                                    <?php 
                                    foreach ($data_items as $key => $value) {
                                        echo "<option value='".$value->items_id."' data-price='".$value->price."' data-name='".$value->items_name."' data-items_unit='".$value->items_unit."'>".$value->items_name."</option>";
                                    }
                                    ?>
                                </select>
                                <input type="hidden" class="form-control price" id="items_unit_to">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Qty Barang</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control price" id="items_qty_to">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Keterangan</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="remarks">
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <div class="col-md-2">
                                <p>Harga barang (Satuan)</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="crew_address">
                            </div>
                        </div> -->
                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('stok/unit_conversion'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#full_name').focus();
$("#items_id").select2().on('select2:select',function(e){
    var items_unit = $('#items_id option:selected').attr('data-items_unit');
    $('#items_unit').val(items_unit);
});
$("#items_id_to").select2().on('select2:select',function(e){
    var items_unit = $('#items_id_to option:selected').attr('data-items_unit');
    $('#items_unit_to').val(items_unit);
});
$("#warehouse_id").select2().on('select2:select',function(e){
  var id = $('#warehouse_id').val();
    $.ajax({
      url : baseUrl+'stok/trn_stok/get_stok',
      method : "POST",
      data : {id: id},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option onclick="return stok_barang(this)" value="'+data[i].items_id+'" data-items_unit="'+data[i].items_unit+'" data-name="'+data[i].items_name+'" data-stok="'+data[i].current_stock+'">'+data[i].items_name+'</option>';
        }
        $('#items_id').html(html);
      }
    })
});

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#warehouse_id').val()){
            toastr.error("<b>Gudang</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#full_name').focus();
            return false;
        }
        if(!$('#items_id').val()){
            toastr.error("<b>Barang</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#full_name').focus();
            return false;
        }
        if(!$('#items_qty').val()){
            toastr.error("<strong>Qty</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#items_qty").focus();
            return false;
        }
        if($('#items_qty').val() > parseInt($('#items_id option:selected').attr('data-stok'))){
            var stok    = parseInt($('#items_id option:selected').attr('data-stok'));
            toastr.error("<strong>Qty Barang</strong> Lebih besar dari pada Stock. Stock : "+stok, 'Alert', {"positionClass": "toast-top-center"});
            $("#item_qty").focus();
            return false;
        }

        

        $.ajax({
            url  : baseUrl+'stok/unit_conversion/form_act',
            type : "POST",  
            data : {
                warehouse_id        : $('#warehouse_id').val(),
                items_id            : $('#items_id').val(),
                items_qty           : $('#items_qty').val().replace(/\,/g, ''),
                items_unit          : $('#items_unit').val(),
                items_id_to         : $('#items_id_to').val(),
                items_qty_to        : $('#items_qty_to').val().replace(/\,/g, ''),
                items_unit_to       : $('#items_unit_to').val(),
                remarks             : $('#remarks').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'stok/unit_conversion/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>