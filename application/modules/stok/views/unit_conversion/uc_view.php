<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Konversi Unit</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Konversi Unit</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a href="<?php echo base_url('stok/unit_conversion/form'); ?>" class="btn btn-primary">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="15%" rowspan="2">Gudang</th>
                    <th colspan="3" align="center">Dari</th>
                    <th colspan="3" align="center">Untuk</th>
                    <th width="11%" rowspan="2">Remarks</th>
                    <th width="13%" rowspan="2">Action</th>
                  </tr>
                  <tr>
                    <th>Barang</th>
                    <th width="8%">Qty</th>
                    <th>Harga</th>
                    <th>Barang</th>
                    <th width="8%">Qty</th>
                    <th>Harga</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_uc as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->warehouse_name; ?></td>
                    <td><?php echo $value->items_from; ?></td>
                    <td><?php echo number_format($value->qty_from).' ('.$value->unit_from.')'; ?></td>
                    <td align="right"><?php echo number_format($value->price_from); ?></td>
                    <td><?php echo $value->items_to; ?></td>
                    <td><?php echo number_format($value->qty_to).' ('.$value->unit_to.')'; ?></td>
                    <td align="right"><?php echo number_format($value->price_to); ?></td>
                    <td><?php echo $value->remarks; ?></td>
                    <td><?php 
                      if($value->is_active=='1'){
                        echo '<button id="reject" data-uv_no="'.$value->uv_no.'"" data-uv_id="'.$value->uv_id.'" class="btn btn-warning btn-xs" type="submit">Batal</button>';
                      }else{
                        echo '<button id="reject" class="btn btn-warning btn-xs"  disabled="">Batal</button>';
                      }
                      ?>
                    </td>
                  </tr>
                  <?php 
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#reject', function(e){
    $('#reject').prop("disabled",true);
    var uv_id  = $(this).data('uv_id');
    var uv_no  = $(this).data('uv_no');

    toastr.warning(
      'Apakah anda Ingin Membatalkan '+uv_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-uv_no="'+uv_no+'" data-uv_id="'+uv_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });


});

function deleteRow(e){ 
  
  $('#okBtn').prop("disabled",true);

  var uv_id  = $(e).data('uv_id');
  var uv_no  = $(e).data('uv_no');

  $.ajax({
    data: {
      uv_id  : uv_id,
      uv_no  : uv_no,
    },
    type : "POST",
    url: baseUrl+'stok/unit_conversion/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("'Data Tidak berhasil di Batalkan'.", 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data Berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'stok/unit_conversion'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!-- 
1. Terdapat Number Format pada tipe data varchar<td><?php echo $value->items_to; ?></td>
-->