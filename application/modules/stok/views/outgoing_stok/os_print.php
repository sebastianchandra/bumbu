<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2 style="text-align: center">Pengeluaran Stok</h2>
    </div>
</div>
<?php
// test($header,1);
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox">
            <div class="row-form">
              <div class="col-md-12">
                <p><strong></strong></p>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Pengeluaran Stok</strong></p>
              </div>
              <div class="col-md-4">
                <div id="no_pr"><?php echo $header->os_no; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Nama Company</strong></p>
              </div>
              <div class="col-md-4">
                <div id="company_id"><?php echo $header->company_name; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tanggal</strong></p>
              </div>
              <div class="col-md-4">
                <div id="pr_date"><?php echo tgl_singkat($header->os_date); ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Dari Gudang</strong></p>
              </div>
              <div class="col-md-4">
                <div id="requester"><?php echo $header->wh_from ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Jenis</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header->remarks; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Tujuan</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo ($header->project_id!=0)? $header->project_name : $header->wh_to; ?></div>
              </div>
            </div>
           
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="vtable">
                    <thead>
                      <tr>
                        <th width="8%">No</th>
                        <th>Nama Barang</th>
                        <th width="15%">Qty</th>
                        <th width="15%">Price</th>
                        <!-- <th width="15%">Potongan</th> -->
                        <th width="15%">Sub Total</th>
                      </tr>
                      <?php 
                      $ttotal     = 0;
                      $no         = 0;
                      foreach ($detail as $key => $value) {
                      $no       = $no+1;
                      $ttotal     = $ttotal+$value->total;
                      ?>
                      <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $value->items_name; ?></td>
                        <td align="right"><?php echo number_format($value->qty,2); ?></td>
                        <td align="right"><?php echo number_format($value->price,2); ?></td>
                        <!-- <td><?php echo number_format($value->discount,2); ?></td> -->
                        <td align="right"><?php echo number_format($value->total,2); ?></td>
                      </tr>
                      <?php 
                      }
                      ?>
                      <tr>
                        <td align="right" colspan="4">Total : </td>
                        <td align="right"><?php echo number_format($ttotal,2); ?></td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <br/><br/>
            <div class="row-form">
              <div class="col-md-8">
                <p><strong></strong></p>
              </div>
              <div class="col-md-2">
                <p><strong>Penerima</strong><br/><br/><br/>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": false, "paging": false, "ordering": false,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
  }); 
});

window.print(); setTimeout(function(){window.close();},500);
</script>