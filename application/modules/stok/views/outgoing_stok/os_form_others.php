<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Stok Keluar</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Stok Keluar Lain - Lain</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <form class="form-horizontal" method="post" action="<?php echo base_url().'stok/incoming_stok/form_act'; ?>">      
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Company</p>
              </div>
              <div class="col-md-5">
                <input type="text" class="form-control" id="company_id" data-company_id="<?php echo $data_company['company_id']; ?>" value="<?php echo $data_company['company_name']; ?>" disabled>
                <input type="hidden" name="items" id="sup_items" value='<?php echo json_encode($new_os["items"]); ?>' required />
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="os_date" value="<?php echo substr(dbnow(),0,10); ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Jenis Stok Keluar</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='os_kind'>
                  <!-- <option value=""> - </option> -->
                  <!-- <option value="Transfer">Transfer</option> -->
                  <option value="Others">Others</option>
                  <!-- <option value="Adjustment">Adjustment</option> -->
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Dari Gudang</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='warehouse'>
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo '<option data-name="'.$value->warehouse_name.'" value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                  }
                  ?>
                </select>
              </div>
              <div class="col-md-1" hidden="">
                <p>Tujuan </p>
              </div>
              <div class="col-md-3" hidden="">
                <select class="form-control" id="warehouse_project" name="warehouse_project">
                  <option value="">Lokasi</option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    echo "<option value='".$value->warehouse_id."' >".$value->warehouse_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Requester</p>
              </div>
              <div class="col-md-3">
                <input type="text" class="form-control" id="os_requester">
              </div>
            </div>
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Project Name</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="project_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_project as $key => $value) {
                    echo "<option value='".$value->project_id."' data-project_name='".$value->project_name."'>".$value->project_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div> -->
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Supplier Name</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id="supplier_id">
                  <option value="">Select</option>
                  <?php 
                  foreach ($data_supplier as $key => $value) {
                    echo "<option value='".$value->supplier_id."' data-supplier_name='".$value->supplier_name."'>".$value->supplier_name."</option>";
                  }
                  ?>
                </select>
              </div>
            </div> -->
            <!-- <div class="form-group row">
              <div class="col-md-2">
                <p>Delivery Address</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="delivery_address">
              </div>
            </div> -->
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Nama Barang</p>
              </div>
              <div class="col-md-7">
                <select class="form-control" id="item_id">
                  <!-- <option value="">Select</option>
                  <?php 
                  foreach ($data_items as $key => $value) {
                    echo "<option value='".$value->items_id."' data-name='".$value->items_name."'>".$value->items_name."</option>";
                  }
                  ?> -->
                </select>
                <input type="hidden" class="form-control " id="id" name="id" value="0"/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Quantity</p>
              </div>
              <div class="col-md-2">
                <input type="number" class="form-control" id="item_qty">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p><!-- Info --></p>
              </div>
              <div class="col-md-5">
                <input type="hidden" class="form-control" id="item_info">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-5">
                <button type="button" class="btn btn-primary btn-sm" id="add-items">Add Items</button>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th>Nama Barang</th>
                        <th width="8%">Qty</th>
                        <th>Info</th>
                        <th width="5%">Action</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                <button id="loading_progress" class="btn btn-primary btn-sm" disabled="">Simpan <span class="loading"></span> </button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('stok/outgoing_stok'); ?>">Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$("#loading_progress").hide();

$("#warehouse_project").attr("disabled", 'disabled');
$("#warehouse_project").select2();

$("#os_kind").select2().on('select2:select',function(e){
  var os_kind = $('#os_kind').val();
  if(os_kind=='Transfer'){
    $("#warehouse_project").removeAttr('disabled');
  }else{
    $("#warehouse_project").attr("disabled", 'disabled');
  }
});
$("#warehouse").select2().on('select2:select',function(e){
  var id = $('#warehouse').val();
    $.ajax({
      url : baseUrl+'stok/trn_stok/get_stok',
      method : "POST",
      data : {id: id},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option onclick="return stok_barang(this)" value="'+data[i].items_id+'" data-name="'+data[i].items_name+'" data-stok="'+data[i].current_stock+'">'+data[i].items_name+'</option>';
        }
        $('#item_id').html(html);
      }
    })
});

function stok_barang(e){
  alert('tes');
  let nama = $(e).attr('data-name');
  alert(nama);
}

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#supplier_id").select2();
    $("#item_id").select2();
    $("#os_kind").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ],
      columns: [
        { data: 'item_id'},
        { data: 'item_name'}, 
        { data: 'item_qty', className: "text-right"}, 
        { data: 'item_info',"visible": false}, 
        { data: 'act', className: "text-center" }
      ],
    });

    this._set_items($('#sup_items').val());
    $('#add-items').click(pr.add_items);
    $('#save').click(pr.save);

  },

  add_items: function(e){
    e.preventDefault();

    if($('#item_id').val()==''){
      toastr.error("<strong>Items Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
      $('#item_id').select2('open');
      return false;
    }

    if($('#item_qty').val() > parseFloat($('#item_id option:selected').attr('data-stok'))){
      var stok    = parseFloat($('#item_id option:selected').attr('data-stok'));
      toastr.error("<strong>Qty</strong> Lebih Besar Dari Pada Stok. Stok : "+stok, 'Alert', {"positionClass": "toast-top-center"});
      $("#item_qty").focus();
      return false;
    }

    if(!$('#item_qty').val()){
      toastr.error("<strong>Qty</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
      $("#item_qty").focus();
      return false;
    } 

    let item_id   = $('#item_id').val();
    let item_name = $('#item_id option:selected').attr('data-name');
    let item_qty  = $('#item_qty').val();
    let item_info = $('#item_info').val();
    let id        = parseInt($('#id').val());
    var id_det    = id + 1;


      if(item_id){
      data = {
        det_id : id_det,
        item_id : item_id,
        item_name : item_name,
        item_qty : item_qty,
        item_info : item_info
      };

      pr._addtogrid(data);
      pr._clearitem();

    }
  },

  _addtogrid: function(data){
    // debugger
    let grids = this.grids;
    let exist = pr.grids.row('#'+data.item_id).index();
    //
    $('#id').val(data.item_id);

    data.act = '<button det-id="'+data.item_id+'" onclick="return pr._removefromgrid(this);">x</button>';
    data.DT_RowId = data.item_id;
    //
    if(exist===undefined){
      grids.row.add(data).draw();
    }else{ 
      toastr.error("<strong>Barang</strong> Sudah ada pada List Detail", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    if(this.no_ajax) return false;

    $.post({
      url: baseUrl+'stok/outgoing_stok/add_item',
      data: {
        det_id    : data.det_id,
        item_id   : data.item_id,
        item_name : data.item_name,
        item_qty  : data.item_qty,
        item_info : data.item_info
      }
    });
  },

  _set_items: function(items){
    this.no_ajax = true;
    //
    if(items) items = JSON.parse(items);
    this.items = items;
    items.map(function(i,e){
      var data = {
        det_id    : i.det_id,
        item_id   : i.item_id,
        item_name : i.item_name,
        item_qty  : i.item_qty,
        item_info : i.item_info
      };
      pr._addtogrid(data);
      pr._clearitem();
    });
    this.no_ajax = false;

  },

  _clearitem: function(){
    $('#item_id').val('').trigger('change');
    $('#item_qty').val('');
    $('#item_info').val('');
  },

  _removefromgrid: function(el){
    let id = $(el).attr('det-id');
    pr.grids.row("#"+id).remove().draw();
    $.get({
      url: baseUrl+'stok/outgoing_stok/remove_item',
      data: {
        index_id: id
      }
    });
    return false;
  },

  save: function(e){
    e.preventDefault();

    if($('#os_kind').val()=='Transfer'){
      if(!$('#warehouse_project').val()){
        toastr.error("<strong>Lokasi</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
        $('#warehouse_project').select2('open');
        return false;
      }
    }

    if($('#detail').DataTable().data().count()<1){
      toastr.error("<strong>Detail Barang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      return false;
    }

    if(!$('#os_requester').val()){
      toastr.error("<strong>Requester</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
      $("#requester").focus();
      return false;
    }

    // if(!$('#project_id').val()){
    //   toastr.error("<strong>Project Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
    //   $('#project_id').select2('open');
    //   return false;
    // }

    // if(!$('#supplier_id').val()){
    //   toastr.error("<strong>Supplier Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
    //   $('#supplier_id').select2('open');
    //   return false;
    // }
    
    $("#save").hide();
    $("#loading_progress").show();
    
    $.ajax({
      url: baseUrl+'stok/outgoing_stok/form_act',
      type : "POST",  
      data: {
        company_name      : $('#company_id').val(),
        company_id        : $('#company_id').attr('data-company_id'),
        os_date           : $('#os_date').val(),
        os_requester      : $('#os_requester').val(),
        warehouse         : $('#warehouse').val(),
        remarks           : $('#remarks').val(),
        os_kind           : $('#os_kind').val(),
        warehouse_project : $('#warehouse_project').val(),
        to_local          : $('#to_local:checked').val(),
        
        // project_id        : $('#project_id').val(),
        // project_name      : $('#project_id option:selected').attr('data-project_name'),
        // supplier_id       : $('#supplier_id').val(),
        // supplier_name     : $('#supplier_id option:selected').attr('data-supplier_name'),
        // delivery_address  : $('#delivery_address').val(),

      },
      success : function(resp){
        // debugger
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else {
          messages = 'Data Berhasil Disimpan dengan Nomor '+resp.os_no;
          messages += "<hr>";

          toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
          
          setTimeout(function () {
            window.location.href = baseUrl+'stok/outgoing_stok/'; 
          }, 2000);
        }
      }
    });

  }

};

pr.init();

</script>







