<?php 
if($this->session->flashdata('alert')!=''){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Stok</li>
                  <li class="breadcrumb-item">Stok Keluar</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Stok Keluar</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a href="<?php echo base_url('stok/outgoing_stok/form_others'); ?>" class="btn btn-warning">Input Lain - Lain</a>
          <a href="<?php echo base_url('stok/outgoing_stok/form'); ?>" class="btn btn-primary">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="17%">Nomor Stok Keluar</th>
                    <th>Nama Company</th>
                    <!-- <th>Referensi</th> -->
                    <th>Tanggal</th>
                    <th>Gudang</th>
                    <th>Jenis</th>
                    <th>Tujuan</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_os as $key => $value) {
                  ?>
                  <tr>
                    <td>
                      <a data-toggle="modal"  href="#" id='detail' onclick="return show_detail(this)" 
                      data-id="<?php echo $value->os_id; ?>" 
                      data-os="<?php echo $value->os_no; ?>"><?php echo $value->os_no; ?></a></strong>
                    </td>
                    <td><?php echo $value->company_name; ?></td>
                    <!-- <td><?php echo $value->doc_ref; ?></td> -->
                    <td><?php echo tgl_singkat($value->os_date); ?></td>
                    <td><?php echo $value->warehouse_name; ?></td>
                    <td><?php echo $value->os_kind; ?></td>
                    <td><?php echo $value->tujuan_wh; ?></td>
                    <td><?php echo $value->os_status; ?></td>
                    <td align="center">
                      <?php if($value->os_status=='Reject'){
                        echo '<button id="reject" class="btn btn-warning btn-xs" type="submit" disabled="">Batal</button> ';
                        echo '<button class="btn btn-info btn-xs" disabled="">Print</button> ';
                        echo '<a href="'.base_url('stok/outgoing_stok/copy_input/'.$value->os_id).'" class="btn btn-success btn-xs">Copy</a>';
                      }else{
                        echo '<button id="reject" data-os_no="'.$value->os_no.'"" data-os_id="'.$value->os_id.'" class="btn btn-warning btn-xs" >Batal</button> ';
                        echo '<a target="_blank" href="'.base_url('stok/outgoing_stok/view_print/'.$value->os_id).'" class="btn btn-info btn-xs">Print</a>';
                      } 
                      ?>
                    </td>
                  </tr>
                  <?php 
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myPr" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Select Purchase Requisition</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="pr_popup">
                <thead>
                  <tr>
                    <th>PR No</th>
                    <th width="15%">Check</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Next</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Stok Keluar</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Nomor Stok Keluar</strong></p>
          </div>
          <div class="col-md-4">
            <div id="os_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="os_date"></div>
          </div>          
        </div>
        <div class="form-group row">          
          <div class="col-md-2">
            <p><strong>Jenis Stok Keluar</strong></p>
          </div>
          <div class="col-md-4">
            <div id="os_kind"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Nama Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_name"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Gudang</strong></p>
          </div>
          <div class="col-md-4">
            <div id="warehouse_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Gudang Tujuan</strong></p>
          </div>
          <div class="col-md-4">
            <div id="wh_tujuan"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-4">
            <div id="remarks"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="os_status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nama Barang</th>
                    <th width="15%">Qty</th>
                    <th width="20%">Harga</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }

  $.ajax({
    url: baseUrl+'transaction/purchase_order/pr_selected',
    type : "POST",  
    data: {
      values      : values
    },
    success : function(resp){        
      setTimeout(function () {
        window.location.href = baseUrl+'transaction/purchase_order/po_input'; 
      }, 100);
    }
  });
  // alert(values);
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 1,2,3 ] 
    }
  ]
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

function show_pr(e){  
  $.get({
    url: baseUrl + 'transaction/purchase_order/view_pr/',
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });

  $('#myPr').modal('show'); 
}

function show_detail(e){
  var idOs     = $(e).data('id');
  var noOs     = $(e).data('os');

  $.get({
    url: baseUrl + 'stok/outgoing_stok/view_popup/'+idOs,
    success: function(resp){
      $('#company_name').text(resp.header.company_name);
      $('#os_no').text(resp.header.os_no);
      $('#os_date').text(tanggal_indonesia(resp.header.os_date));
      $('#os_kind').text(resp.header.os_kind);
      $('#remarks').text(resp.header.remarks);
      $('#os_status').text(resp.header.os_status);
      $('#warehouse_name').text(resp.header.warehouse_name);
      $('#wh_tujuan').text(resp.header.dest_wh);
      // $('#supplier_name').text(resp.header.supplier_name);
      // $('#project_name').text(resp.header.project_name);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#reject', function(e){
    var os_id  = $(this).data('os_id');
    var os_no  = $(this).data('os_no');

    toastr.warning(
      'Apakah anda Ingin Membatalkan '+os_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-os_no="'+os_no+'" data-os_id="'+os_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var os_id  = $(e).data('os_id');
  var os_no  = $(e).data('os_no');

  $.ajax({
    data: {
      os_id  : os_id,
      os_no  : os_no
    },
    type : "POST",
    url: baseUrl+'stok/outgoing_stok/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("Data Gagal di batalkan.", 'Alert', {"positionClass": "toast-top-center"});
        return false;
      } else {
        toastr.success("Data berhasil di batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'stok/outgoing_stok'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!-- 
1. Memunculkan Remarks pada Popup detail Outgoing Stock
-->