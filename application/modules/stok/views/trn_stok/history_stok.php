<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Recallculate</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Stock</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Recallculate</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('stok/trn_stok/view_recallculate',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Action</p>
              </div>
              <div class="col-md-7">
                <button type="submit" id="recallculate" class="btn btn-primary btn-sm" name="recallculate" value="recallculate">Recallculate Stock</button>
                <button id="loading_progress" class="btn btn-primary btn-sm" disabled="">Recallculate Stock <span class="loading"></span> </button>


                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <?php echo form_close(); ?>   
            <div class="row-form ">
              <div class="col-md-2">
              </div>
              <div class="col-md-7">
                <?php 
                if($this->input->post('recallculate')!=''){
                ?>
                <button type="submit" id="insert" class="btn btn-warning btn-sm" name="recallculate" value="insert">Insert To Stock</button>
                <button id="insert_progress" class="btn btn-warning btn-sm" disabled="">Insert To Stock <span class="loading"></span> </button>
                <?php 
                }
                ?>
              </div>
            </div>              
            <div class="row-form">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <div>
              <table class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                  <tr>
                    <th width="12%">Tanggal</th>
                    <th>No. Transaksi</th>
                    <th>Keterangan</th>
                    <!-- <th>Transaksi</th> -->
                    <th width="12%">Saldo Awal</th>
                    <th width="12%">Barang Masuk</th>
                    <th width="12%">Barang Keluar</th>
                    <th width="12%">Jumlah Stok</th>
                  </tr>
                </thead>
                <!-- <tbody> -->
                  <?php 
                  foreach ($data_stok_current as $key => $value) {
                    
                  ?>
                  <tr>
                    <td colspan="7" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"><strong><?php echo $value->items_name; ?> - (<?php echo $value->warehouse_name ?>)</strong></td>
                  </tr>
                  <?php
                  $query  = $this->db->query("SELECT a.trn_date,a.items_id,b.items_name,a.doc_no,a.warehouse_id,a.activity,a.qty,a.current_stock,
                    CASE
                       WHEN SUBSTR(a.doc_no,1,3) = 'SAT' THEN 'Saldo Awal'
                       WHEN SUBSTR(a.doc_no,1,3) = 'ISS' THEN (SELECT doc_ref FROM db_bumbu_transaction.trn_incoming_stock_01 WHERE is_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'IST' THEN (SELECT IF(a1.project_id=0,b1.warehouse_name,c1.project_name) keterangan FROM db_bumbu_transaction.trn_incoming_stock_01 a1 LEFT JOIN db_bumbu_master.mst_warehouse b1 ON a1.warehouse_id=b1.warehouse_id LEFT JOIN db_bumbu_master.mst_project c1 ON a1.project_id=c1.project_id WHERE a1.is_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'ISR' THEN 'Reject Incoming'
                       WHEN SUBSTR(a.doc_no,1,3) = 'OSS' THEN (SELECT IF(a2.project_id=0,b2.warehouse_name,c2.project_name) keterangan FROM db_bumbu_transaction.trn_outgoing_stock_01 a2 LEFT JOIN db_bumbu_master.mst_warehouse b2 ON a2.dest_wh_id=b2.warehouse_id LEFT JOIN db_bumbu_master.mst_project c2 ON a2.project_id=c2.project_id WHERE a2.os_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'OST' THEN (SELECT remarks FROM db_bumbu_transaction.trn_outgoing_stock_01 a2 WHERE a2.os_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'OSR' THEN 'Reject Outgoing'
                       WHEN SUBSTR(a.doc_no,1,3) = 'OSJ' THEN 'Adjustment Stock'
                       WHEN SUBSTR(a.doc_no,1,3) = 'UVS' THEN 'Konversi Barang'
                       WHEN SUBSTR(a.doc_no,1,3) = 'UVR' THEN 'Reject Konversi Barang'
                       ELSE ' '
                                        END AS keterangan
                    FROM db_bumbu_transaction.trn_stock_hist_tmp a, db_bumbu_master.mst_items b
                    WHERE a.items_id=b.items_id
                    AND a.current_stock>=0 AND a.items_id='".$value->items_id."' AND a.warehouse_id='".$value->warehouse_id."' ORDER BY a.stock_hist_id ASC")->result();
                  foreach ($query as $key => $value2) {
                    $saldo_awal     = 0;
                    if($value2->activity=='Saldo Awal' OR $value2->activity=='In_Po' OR $value2->activity=='In_Adjustment' OR $value2->activity=='Rj_OutTrans' OR $value2->activity=='In_Trans' OR $value2->activity=='In_Others' OR $value2->activity=='In_Conv' OR $value2->activity=='Rj_ConvOut' OR $value2->activity=='in'){
                      $saldo_awal   = $value2->current_stock-$value2->qty;
                    }elseif($value2->activity=='Rj_InTrans' OR $value2->activity=='Out_Tr' OR $value2->activity=='Out_tr' OR $value2->activity=='Out_Oth' OR $value2->activity=='Out_Adj' OR $value2->activity=='Out_Conv' OR $value2->activity=='Rj_ConvIn' OR $value2->activity=='out'){
                      $saldo_awal   = $value2->current_stock+$value2->qty;
                    }
                  ?>
                  <tr>
                    <td><?php echo tgl_singkat($value2->trn_date); ?></td>
                    <td><?php echo $value2->doc_no; ?></td>
                    <td><?php echo $value2->keterangan; ?></td>
                    <!-- <td><?php echo $value2->activity; ?></td> -->
                    <td align="right"><?php echo number_format($saldo_awal,2); ?></td>
                    <td align="right"><?php if($value2->activity=='Saldo Awal' 
                                            OR $value2->activity=='in'
                                            OR $value2->activity=='In_Po' 
                                            OR $value2->activity=='In_Adjustment'
                                            OR $value2->activity=='Rj_OutTrans' 
                                            OR $value2->activity=='In_Trans' 
                                            OR $value2->activity=='In_Others' 
                                            OR $value2->activity=='In_Conv' 
                                            OR $value2->activity=='Rj_ConvOut'){ echo '(+) '.number_format($value2->qty,2); } ?>
                    </td>
                    <td align="right"><?php if($value2->activity=='Rj_InTrans' 
                                            OR $value2->activity=='out'
                                            OR $value2->activity=='Out_Tr' 
                                            OR $value2->activity=='Out_tr' 
                                            OR $value2->activity=='Out_Oth' 
                                            OR $value2->activity=='Out_Adj' 
                                            OR $value2->activity=='Out_Conv' 
                                            OR $value2->activity=='Rj_ConvIn' ){ echo '(-) '.number_format($value2->qty,2); } ?></td>
                    <td align="right"><?php echo $value2->current_stock; ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                  <tr>
                    <td align="right" colspan="6" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"><strong>Total Barang (<?php echo $value->items_name; ?>)</strong></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"><strong><?php echo number_format($value->current_stock,2); ?></strong></td>
                  </tr>
                  <?php
                  }
                  ?>
                  <!-- <?php 
                  foreach ($data_stok_current as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->items_code; ?></td>
                    <td><?php echo $value->items_name; ?></td>
                    <td><?php echo $value->warehouse_name; ?></td>
                    <td><?php echo $value->current_stock; ?></td>
                    <td><?php echo $value->items_unit_name; ?></td>
                  </tr>
                  <?php
                  }
                  ?> -->
                <!-- </tbody> -->
              </table>
            </div>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#loading_progress").hide();
$("#insert_progress").hide();
$("#warehouse_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": false, "paging": false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#recallculate').click(function(){
    $("#recallculate").hide();
    $("#loading_progress").show();
  });

  $('#insert').click(function(){
    $("#insert").hide();
    $("#insert_progress").show();

    $.ajax({
      url: baseUrl+'stok/trn_stok/insert_to_stock',
      type : "POST",  
      success : function(resp){
        // debugger
        if(resp.status == 'ERROR INSERT' || resp.status == false) {
          toastr.error("Data Gagal disimpan", 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else {
          messages = 'Data Berhasil Disimpan di Recallculate ';
          messages += "<hr>";

          toastr.info(messages, 'Info', {"positionClass": "toast-top-center"});
          
          setTimeout(function () {
            window.location.href = baseUrl+'stok/trn_stok/view_recallculate'; 
          }, 2000);
        }
      }
    });
  });
});
</script>
