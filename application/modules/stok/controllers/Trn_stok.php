<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trn_stok extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('stok/stok_model');
        $this->load->model('stok/trn_stok_model');
        $this->load->model('stok/stok_by_doc_model');
        $this->load->model('stok/stok_hist_model');
    }

    function get_stok(){
        $sup_id     = $this->input->post('id');
        $result     = $this->stok_model->last_stock($sup_id)->result();
        echo json_encode($result);
    }

    function get_stok_so(){
        // $sup_id     = $this->input->post('id');
        $result     = $this->stok_model->last_stock_so()->result();
        echo json_encode($result);
    }

    function view_recallculate(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Stock', 'active_submenu' => 'stok/trn_stok/view_recallculate'));
        $this->isMenu();

        if(!empty($this->input->post('recallculate'))){
            $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_by_doc_tmp");
            $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_hist_tmp");
            $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_tmp");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_by_doc_tmp 
                (items_id, doc_no, project_id, warehouse_id, trn_date, trn_year, trn_month, activity, items_in,items_out,items_remaining, 
                old_stock, current_stock, items_price, pic_data, data_time)
                SELECT items_id,doc_no,project_id,warehouse_id,trn_date,trn_year,trn_month,activity,items_in,items_out,current_stock,
                old_stock,current_stock,items_price,pic_data,NOW() FROM db_bumbu_transaction.trn_stock_by_doc WHERE activity='Saldo Awal'");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_hist_tmp 
                (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock)
                SELECT items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock
                FROM db_bumbu_transaction.trn_stock_hist WHERE activity='Saldo Awal'");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_tmp 
                (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,items_in,items_out,adj_in,adj_out,
                old_stock,current_stock )
                SELECT items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,'0','0','0','0',qty
                FROM db_bumbu_transaction.trn_stock_hist WHERE activity='Saldo Awal'");

            $company_id         = $this->current_user['company_id'];
            $company_name       = $this->current_user['company_name'];
            
            $query      = $this->trn_stok_model->qRecalculate();
            
            foreach ($query as $key => $value) {
                // test($value,1);
                $items_id       = $value->items_id;
                $doc_no         = $value->doc_no;
                $project_id     = $value->project_id;
                $warehouse_id   = $value->warehouse_id;
                $date_doc       = $value->date_doc;
                $trn_year       = $value->trn_year;
                $trn_month      = $value->trn_month;
                $kind           = $value->kind;

                $cek_old_stock  = $this->db->query("SELECT current_stock FROM db_bumbu_transaction.trn_stock_by_doc_tmp WHERE 
                                    warehouse_id='".$warehouse_id."' AND items_id='".$items_id."'")->num_rows();
                if($cek_old_stock>=1){
                    $old_stock      = $this->db->query("SELECT current_stock FROM db_bumbu_transaction.trn_stock_by_doc_tmp WHERE 
                                    warehouse_id='".$warehouse_id."' AND items_id='".$items_id."' order by stock_by_doc_id DESC limit 1")->row()->current_stock;
                }else{
                    $old_stock      = '0.00';
                }

                $kode_doc       = substr($doc_no,0,2);

                if($kode_doc=='IS'){
                    $items_in           = $value->qty;
                    $items_out          = 0;
                    $items_remaining    = $value->qty;
                    $current_stock      = $old_stock+$value->qty;
                    $items_price        = $value->price;
                    $activity           = 'in';
                }elseif($kode_doc=='OS'){

                    $data_stok      = $this->db->query("SELECT a.stock_by_doc_id,a.items_id,a.warehouse_id,a.items_in,a.items_out,
                             a.items_remaining,a.current_stock,a.items_price,a.data_time FROM db_bumbu_transaction.trn_stock_by_doc_tmp a
                             WHERE a.items_id='".$value->items_id."' AND a.warehouse_id='".$value->warehouse_id."' AND a.items_remaining >0
                             ORDER BY a.data_time")->result();
                    // $data_stok      = $this->stok_by_doc_model->dataStock($value->items_id,$value->warehouse_id)->result();
                    // test($data_stok,1);
                    $is_stop        = 0;
                    $tbarang        = 0;
                    $sisa           = 0;
                    $stok_barang1   = $value->qty;
                    $activity       = 'out';

                    foreach ($data_stok as $key => $value2) {

                        if($value2->items_remaining>=$stok_barang1){
                            $tbarang        = $tbarang+($stok_barang1*$value2->items_price);
                            $sisa           = $value2->items_remaining - $stok_barang1;
                            $stok_barang1   = $value2->items_remaining - $stok_barang1;

                            $is_stop        = 1;
                        }else{
                            $tbarang        = $tbarang+($value2->items_remaining * $value2->items_price);
                            $sisa           = 0;
                            $stok_barang1   = $stok_barang1 - $value2->items_remaining;

                            $is_stop        = 0;
                        }

                        // $this->stok_by_doc_model->update_stok($value2->stock_by_doc_id,$sisa);
                        $this->db->query(" UPDATE db_bumbu_transaction.trn_stock_by_doc_tmp SET items_remaining = '".$sisa."' WHERE stock_by_doc_id = '".$value2->stock_by_doc_id."'");
                        // test($sisa.' - '.$stok_barang1.' - '.$tbarang.' <--- ',0);

                        if($is_stop==1){
                            break; 
                        }                
                        
                    }

                    $price_out          = $tbarang / $value->qty;
                    $items_in           = 0;
                    $items_out          = $value->qty;
                    $items_remaining    = 0;
                    $current_stock      = $old_stock-$value->qty;
                    $items_price        = $price_out;
                }

                $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_by_doc_tmp 
                    (items_id, doc_no, project_id, warehouse_id, trn_date, trn_year, trn_month, activity, items_in,
                    items_out,items_remaining, old_stock, current_stock, items_price, pic_data, data_time)VALUES
                    ('".$items_id."','".$doc_no."','".$project_id."','".$warehouse_id."','".$date_doc."','".$trn_year."',
                    '".$trn_month."','".$activity."',
                    '".$items_in."','".$items_out."','".$items_remaining."','".$old_stock."','".$current_stock."','".$items_price."
                    ','1',NOW())");

                $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_hist_tmp 
                    (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,
                    current_stock)VALUES
                    ('".$items_id."','".$doc_no."','".$warehouse_id."','".$company_id."','".$company_name."','".$date_doc."','".$trn_year."','".$trn_month."','".$activity."',
                    '".$value->qty."','".$old_stock."','".$current_stock."')");

                $query_stok_tmp     = $this->db->query("SELECT stock_id,items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,
                                  activity,items_in,items_out,adj_in,adj_out,old_stock,current_stock FROM db_bumbu_transaction.trn_stock_tmp 
                                  WHERE items_id='".$items_id."' AND warehouse_id='".$warehouse_id."' ORDER BY stock_id DESC LIMIT 1");

                $cek_stok_tmp       = $query_stok_tmp->num_rows();
                // test($cek_stok_tmp,1);

                if($cek_stok_tmp>=1){
                    $detail_stok    = $query_stok_tmp->row();
                    // test($detail_stok,0);
                    $old_stok1      = $detail_stok->current_stock;
                    $current_stock1 = ($old_stok1+$items_in)-$items_out;
                    // test($old_stok1.' '.$current_stock1,0);
                    $id = $detail_stok->stock_id;
                    // test($id,0);

                    $this->db->query(' UPDATE db_bumbu_transaction.trn_stock_tmp SET items_id ="'.$items_id.'",doc_no ="'.$doc_no.'",
                        warehouse_id ='.$warehouse_id.',company_id ='.$company_id.',company_name ="'.$company_name.'",
                        trn_date ="'.$date_doc.'",trn_year ='.substr($date_doc,0,4).',trn_month ="'.substr($date_doc,5,2).'",
                        activity ="'.$activity.'",items_in ='.$items_in.',items_out ='.$items_out.',adj_in =0,adj_out =0,
                        old_stock ='.$old_stok1.',current_stock ='.$current_stock1.' WHERE stock_id = '.$id.' ');
                }else{
                    $this->db->query(' INSERT INTO db_bumbu_transaction.trn_stock_tmp ( items_id,doc_no,warehouse_id,company_id,company_name,
                        trn_date,trn_year,trn_month,activity,items_in,items_out,adj_in,adj_out,old_stock,current_stock) VALUES 
                        ( '.$items_id.',"'.$doc_no.'",'.$warehouse_id.','.$company_id.',"'.$company_name.'","'.$date_doc.'",'
                        .substr($date_doc,0,4).',"'.substr($date_doc,5,2).'","'.$activity.'",'.$items_in.','.$items_out.',0,0,0,
                        '.$items_in.')');
                }
            }

            $data['data_stok_current']  = $this->trn_stok_model->get_laststockhistory()->result();
            $data['jmlh_stok_current']  = $this->trn_stok_model->get_laststockhistory()->num_rows();

        }else{
            $data['data_stok_current']     = $this->trn_stok_model->get_laststockhistory()->result();
            $data['jmlh_stok_current']     = $this->trn_stok_model->get_laststockhistory()->num_rows();
        }

        $this->template->load('body', 'trn_stok/history_stok',$data);

    }

    function recallculate(){
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_by_doc_tmp");
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_hist_tmp");
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_tmp");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_by_doc_tmp 
                (items_id, doc_no, project_id, warehouse_id, trn_date, trn_year, trn_month, activity, items_in,items_out,items_remaining, 
                old_stock, current_stock, items_price, pic_data, data_time)
                SELECT items_id,doc_no,project_id,warehouse_id,trn_date,trn_year,trn_month,activity,items_in,items_out,current_stock,
                old_stock,current_stock,items_price,pic_data,NOW() FROM db_bumbu_transaction.trn_stock_by_doc WHERE activity='Saldo Awal'
                ORDER BY trn_date");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_hist_tmp 
                (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock)
                SELECT items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock
                FROM db_bumbu_transaction.trn_stock_hist WHERE activity='Saldo Awal' ORDER BY trn_date");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_tmp 
                (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,items_in,items_out,adj_in,adj_out,
                old_stock,current_stock )
                SELECT items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,'0','0','0','0',qty
                FROM db_bumbu_transaction.trn_stock_hist WHERE activity='Saldo Awal' ORDER BY trn_date");

        $company_id         = $this->current_user['company_id'];
        $company_name       = $this->current_user['company_name'];
        
        $query      = $this->trn_stok_model->qRecalculate();
        
        foreach ($query as $key => $value) {
            // test($value,1);
            $items_id       = $value->items_id;
            $doc_no         = $value->doc_no;
            $project_id     = $value->project_id;
            $warehouse_id   = $value->warehouse_id;
            $date_doc       = $value->date_doc;
            $trn_year       = $value->trn_year;
            $trn_month      = $value->trn_month;
            $kind           = $value->kind;

            $cek_old_stock  = $this->db->query("SELECT current_stock FROM db_bumbu_transaction.trn_stock_by_doc_tmp WHERE 
                                warehouse_id='".$warehouse_id."' AND items_id='".$items_id."'")->num_rows();
            if($cek_old_stock>=1){
                $old_stock      = $this->db->query("SELECT current_stock FROM db_bumbu_transaction.trn_stock_by_doc_tmp WHERE 
                                warehouse_id='".$warehouse_id."' AND items_id='".$items_id."' order by stock_by_doc_id DESC limit 1")->row()->current_stock;
            }else{
                $old_stock      = '0.00';
            }

            $kode_doc       = substr($doc_no,0,2);

            if($kode_doc=='IS'){
                $items_in           = $value->qty;
                $items_out          = 0;
                $items_remaining    = $value->qty;
                $current_stock      = $old_stock+$value->qty;
                $items_price        = $value->price;
                $activity           = 'in';
            }elseif($kode_doc=='OS'){

                $data_stok      = $this->db->query("SELECT a.stock_by_doc_id,a.items_id,a.warehouse_id,a.items_in,a.items_out,
                         a.items_remaining,a.current_stock,a.items_price,a.data_time FROM db_bumbu_transaction.trn_stock_by_doc_tmp a
                         WHERE a.items_id='".$value->items_id."' AND a.warehouse_id='".$value->warehouse_id."' AND a.items_remaining >0
                         ORDER BY a.data_time")->result();
                // $data_stok      = $this->stok_by_doc_model->dataStock($value->items_id,$value->warehouse_id)->result();
                // test($data_stok,1);
                $is_stop        = 0;
                $tbarang        = 0;
                $sisa           = 0;
                $stok_barang1   = $value->qty;
                $activity       = 'out';

                foreach ($data_stok as $key => $value2) {

                    if($value2->items_remaining>=$stok_barang1){
                        $tbarang        = $tbarang+($stok_barang1*$value2->items_price);
                        $sisa           = $value2->items_remaining - $stok_barang1;
                        $stok_barang1   = $value2->items_remaining - $stok_barang1;

                        $is_stop        = 1;
                    }else{
                        $tbarang        = $tbarang+($value2->items_remaining * $value2->items_price);
                        $sisa           = 0;
                        $stok_barang1   = $stok_barang1 - $value2->items_remaining;

                        $is_stop        = 0;
                    }

                    // $this->stok_by_doc_model->update_stok($value2->stock_by_doc_id,$sisa);
                    $this->db->query(" UPDATE db_bumbu_transaction.trn_stock_by_doc_tmp SET items_remaining = '".$sisa."' WHERE stock_by_doc_id = '".$value2->stock_by_doc_id."'");
                    // test($sisa.' - '.$stok_barang1.' - '.$tbarang.' <--- ',0);

                    if($is_stop==1){
                        break; 
                    }                
                    
                }

                $price_out          = $tbarang / $value->qty;
                $items_in           = 0;
                $items_out          = $value->qty;
                $items_remaining    = 0;
                $current_stock      = $old_stock-$value->qty;
                $items_price        = $price_out;
            }

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_by_doc_tmp 
                (items_id, doc_no, project_id, warehouse_id, trn_date, trn_year, trn_month, activity, items_in,
                items_out,items_remaining, old_stock, current_stock, items_price, pic_data, data_time)VALUES
                ('".$items_id."','".$doc_no."','".$project_id."','".$warehouse_id."','".$date_doc."','".$trn_year."',
                '".$trn_month."','".$activity."',
                '".$items_in."','".$items_out."','".$items_remaining."','".$old_stock."','".$current_stock."','".$items_price."
                ','1',NOW())");

            $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_hist_tmp 
                (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,
                current_stock)VALUES
                ('".$items_id."','".$doc_no."','".$warehouse_id."','".$company_id."','".$company_name."','".$date_doc."','".$trn_year."','".$trn_month."','".$activity."',
                '".$value->qty."','".$old_stock."','".$current_stock."')");

            $query_stok_tmp     = $this->db->query("SELECT stock_id,items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,
                              activity,items_in,items_out,adj_in,adj_out,old_stock,current_stock FROM db_bumbu_transaction.trn_stock_tmp 
                              WHERE items_id='".$items_id."' AND warehouse_id='".$warehouse_id."' ORDER BY stock_id DESC LIMIT 1");

            $cek_stok_tmp       = $query_stok_tmp->num_rows();
            // test($cek_stok_tmp,1);
            // Start
                if($cek_stok_tmp>=1){
                    $detail_stok    = $query_stok_tmp->row();
                    // test($detail_stok,0);
                    $old_stok1      = $detail_stok->current_stock;
                    $current_stock1 = ($old_stok1+$items_in)-$items_out;
                    // test($old_stok1.' '.$current_stock1,0);
                    $id = $detail_stok->stock_id;
                    // test($id,0);

                    $this->db->query(' UPDATE db_bumbu_transaction.trn_stock_tmp SET items_id ="'.$items_id.'",doc_no ="'.$doc_no.'",warehouse_id ='.$warehouse_id.',company_id ='.$company_id.',company_name ="'.$company_name.'",trn_date ="'.$date_doc.'",trn_year ='.substr($date_doc,0,4).',trn_month ="'.substr($date_doc,5,2).'",activity ="'.$activity.'",items_in ='.$items_in.',items_out ='.$items_out.',adj_in =0,adj_out =0,old_stock ='.$old_stok1.',current_stock ='.$current_stock1.' WHERE stock_id = '.$id.' ');
                }else{
                    $this->db->query(' INSERT INTO db_bumbu_transaction.trn_stock_tmp ( items_id,doc_no,warehouse_id,company_id,company_name,
                        trn_date,trn_year,trn_month,activity,items_in,items_out,adj_in,adj_out,old_stock,current_stock) VALUES 
                        ( '.$items_id.',"'.$doc_no.'",'.$warehouse_id.','.$company_id.',"'.$company_name.'","'.$date_doc.'",'
                        .substr($date_doc,0,4).',"'.substr($date_doc,5,2).'","'.$activity.'",'.$items_in.','.$items_out.',0,0,0,
                        '.$items_in.')');
                }
            // Finish
        }
    }

    function insert_to_stock(){
        // test('asd',1);
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_by_doc");
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_hist");
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock");

        // $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_by_doc
        //     (items_id, doc_no, project_id, warehouse_id, trn_date, trn_year, trn_month, activity, items_in,items_out,items_remaining, 
        //     old_stock, current_stock, items_price, pic_data, data_time)
        //     SELECT items_id,doc_no,project_id,warehouse_id,trn_date,trn_year,trn_month,activity,items_in,items_out,current_stock,
        //     old_stock,current_stock,items_price,pic_data,NOW() FROM db_bumbu_transaction.trn_stock_by_doc_tmp");

        // $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock_hist 
        //     (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock)
        //     SELECT items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock
        //     FROM db_bumbu_transaction.trn_stock_hist_tmp ");

        // $this->db->query("INSERT INTO db_bumbu_transaction.trn_stock
        //     (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,items_in,items_out,adj_in,adj_out,
        //     old_stock,current_stock )
        //     SELECT items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,'0','0','0','0',qty
        //     FROM db_bumbu_transaction.trn_stock_hist_tmp ");

        $data_stok_by_doc       = $this->db->query("SELECT items_id,doc_no,project_id,warehouse_id,trn_date,trn_year,trn_month,activity,items_in,items_out,items_remaining,
            old_stock,current_stock,items_price,pic_data,data_time FROM db_bumbu_transaction.trn_stock_by_doc_tmp ORDER BY stock_by_doc_id")->result();

        foreach ($data_stok_by_doc as $key => $value) {
           // $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
                $this->stok_by_doc_model->setItemsId($this->security->xss_clean($value->items_id));
                $this->stok_by_doc_model->setDocNo($this->security->xss_clean($value->doc_no));
                $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($value->warehouse_id));
                $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($value->trn_date));
                $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($value->trn_date,0,4)));
                $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($value->trn_date,5,2)));
                $this->stok_by_doc_model->setActivity($this->security->xss_clean($value->activity));
                $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($value->items_in));
                $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean($value->items_remaining));
                $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($value->items_out));
                $this->stok_by_doc_model->setOldStock($this->security->xss_clean($value->old_stock));
                $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($value->current_stock));
                $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($value->items_price));
                $this->stok_by_doc_model->setPicData($this->security->xss_clean($value->pic_data));
                $this->stok_by_doc_model->setDataTime($this->security->xss_clean($value->data_time));
                $this->stok_by_doc_model->insert();
        }

        $data_stock_hist        = $this->db->query("SELECT stock_hist_id,items_id,doc_no,warehouse_id,company_id,company_name,trn_date,
            trn_year,trn_month,activity,qty,old_stock,current_stock FROM db_bumbu_transaction.trn_stock_hist_tmp ORDER BY stock_hist_id")->result();

        foreach ($data_stock_hist as $key => $value2) {
            $this->stok_hist_model->setItemsId($this->security->xss_clean($value2->items_id));
            $this->stok_hist_model->setDocNo($this->security->xss_clean($value2->doc_no));
            $this->stok_hist_model->setWarehouseId($this->security->xss_clean($value2->warehouse_id));
            $this->stok_hist_model->setCompanyId($this->security->xss_clean($value2->company_id));
            $this->stok_hist_model->setCompanyName($this->security->xss_clean($value2->company_name));
            $this->stok_hist_model->setTrnDate($this->security->xss_clean($value2->trn_date));
            $this->stok_hist_model->setTrnYear($this->security->xss_clean($value2->trn_year));
            $this->stok_hist_model->setTrnMonth($this->security->xss_clean($value2->trn_month));
            $this->stok_hist_model->setActivity($this->security->xss_clean($value2->activity));
            $this->stok_hist_model->setQty($this->security->xss_clean($value2->qty));
            $this->stok_hist_model->setOldStock($this->security->xss_clean($value2->old_stock));
            $this->stok_hist_model->setCurrentStock($this->security->xss_clean($value2->current_stock));
            $this->stok_hist_model->insert();
        }

        $data_stock             = $this->db->query("SELECT stock_id,items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,
            trn_month,activity,items_in,items_out,adj_in,adj_out,old_stock,current_stock FROM db_bumbu_transaction.trn_stock_tmp 
            ORDER BY stock_id")->result();

        foreach ($data_stock as $key => $value3) {
            $this->stok_model->setItemsId($this->security->xss_clean($value3->items_id));
            $this->stok_model->setDocNo($this->security->xss_clean($value3->doc_no));
            $this->stok_model->setWarehouseId($this->security->xss_clean($value3->warehouse_id));
            $this->stok_model->setCompanyId($this->security->xss_clean($value3->company_id));
            $this->stok_model->setCompanyName($this->security->xss_clean($value3->company_name));
            $this->stok_model->setTrnDate($this->security->xss_clean($value3->trn_date));
            $this->stok_model->setTrnYear($this->security->xss_clean($value3->trn_year));
            $this->stok_model->setTrnMonth($this->security->xss_clean($value3->trn_month));
            $this->stok_model->setActivity($this->security->xss_clean($value3->activity));
            $this->stok_model->setItemsIn($this->security->xss_clean($value3->items_in));
            $this->stok_model->setItemsOut($this->security->xss_clean($value3->items_out));
            $this->stok_model->setAdjIn($this->security->xss_clean($value3->adj_in));
            $this->stok_model->setAdjOut($this->security->xss_clean($value3->adj_out));
            $this->stok_model->setOldStock($this->security->xss_clean($value3->old_stock));
            $this->stok_model->setCurrentStock($this->security->xss_clean($value3->current_stock));
            $this->stok_model->insert();
        }

        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_by_doc_tmp");
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_hist_tmp");
        $this->db->query("TRUNCATE TABLE db_bumbu_transaction.trn_stock_tmp");

        jsout(array('success' => true, 'status' => TRUE));
    }

    // function get_items_by_warehouse(){
    //     $data = array();

    //     $start  = $this->input->get('start');
    //     $length = $this->input->get('length');
    //     $search = $this->input->get('search');
    //     $id     = $this->input->get('id');
    //     //test($start.' '.$length.' '.$search['value'].' '.$id,1);

    //     $list = $this->trn_stok_model->get_all_items($start,$length,$search['value'],$id);
    //     if(is_for($list)){
    //         foreach ($list as $row) {
    //             $data[] = array(
    //                 'items_id'      => $row->items_id,
    //                 'items_code'    => $row->items_code,
    //                 'items_name'    => $row->items_name,
    //                 'current_stock'   => $row->current_stock
    //             );
    //         }
    //     }
    //     //test($data,0);     

    //     if ($search['value']) {
    //         $total   = $this->trn_stok_model->get_count_display($start,$length,$search['value'],$id);
    //     }else {
    //         $total   = $this->trn_stok_model->get_count($id);
    //     }
    //     // $display = $this->item_model->get_count_display($start,$length,$search['value']);
    //     // jsout(array('success'=>1, 'aaData'=>$data));
    //     jsout(array('success'=>1, 'aaData'=>$data,'iTotalRecords'=>$total,'iTotalDisplayRecords'=>$total));

    // }

}
?>

