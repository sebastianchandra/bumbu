<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('stok/production_model');
        $this->load->model('stok/production_model02');
        $this->load->model('master/warehouse_model');
        $this->load->model('stok/stok_model');
        $this->load->model('stok/stok_by_doc_model');
        $this->load->model('stok/stok_hist_model');        
        $this->load->model('master/config_production_01');
        $this->load->model('master/config_production_02');    
    }

    function index(){  
        $this->session->set_userdata('ses_menu', array('active_menu' => 'stok', 'active_submenu' => 'stok/production'));
        $this->isMenu();

        $data['data_production']        = $this->production_model->getAll();
        $this->template->load('body', 'stok/production/cp_view',$data);
    }

    function form(){
        $this->load->model('master/items_model');
        
        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_produksi']  = $this->items_model->get_items_type(1);
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        
        $this->template->load('body', 'stok/production/cp_form', $data);
    }

    function form_act(){
        $this->db->trans_begin();

        $id             = $this->production_model->getDataCount_id()->production_01_id;

        $this->production_model->setProduction01Id($this->security->xss_clean($id));
        $this->production_model->setItemsId($this->security->xss_clean($_POST['production_items_id']));
        $this->production_model->setItemsName($this->security->xss_clean($_POST['production_items_name']));
        $this->production_model->setQty($this->security->xss_clean($_POST['qty']));
        $this->production_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->production_model->setInputTime($this->security->xss_clean(dbnow()));
        // $save = $this->production_model->insert();

        $detail_barang      = $this->config_production_02->getDetailItems($_POST['production_items_id']);
        $warehouse_id       = $this->input->post('warehouse_id');

        $cek_stok                   = 0;
        $total_loop                 = 0;
        $stok_kurang                = array();
        foreach ($detail_barang as $key => $value) {

            $qty                        = preg_replace("/[^0-9]/", "", $_POST['qty']);
            $total_qty_items            = $value->qty*$qty;
            $items_id                   = $value->items_id;

            $data_stok                  = $this->stok_by_doc_model->dataQtyStock($items_id,$warehouse_id)->row(); 
            $row_stok                   = $this->stok_by_doc_model->dataQtyStock($items_id,$warehouse_id)->num_rows();
            // test($row_stok,1);

            if($row_stok>0){
                if($data_stok->items_remaining>$total_qty_items){
                    $cek_stok  = $cek_stok+1;
                }else{
                    $cek_stok  = $cek_stok+0;
                    $stok_kurang[]        = array('nama_items' => $value->items_name);
                } 
            }else{
                $cek_stok  = $cek_stok+0;
                $stok_kurang[]        = array('nama_items' => $value->items_name);
            }
            $total_loop         = $total_loop+1;
            
        }
        // test($stok_kurang,1);

        if($total_loop==$cek_stok){
            foreach ($detail_barang as $key => $value) {

                $data_stok                  = $this->stok_by_doc_model->dataStock($value->items_id,$warehouse_id)->result();
                
                foreach($data_stok as $key => $val_stok) {
                    $stock_by_doc_id            = $val_stok->stock_by_doc_id;
                    $items_remaining            = $val_stok->items_remaining;
                    test($items_remaining,1);
                }
                // // $this->production_model02->setProduction02Id($this->security->xss_clean($_POST['production02Id']));
                // // $this->production_model02->setProduction01Id($this->security->xss_clean($id));
                // // $this->production_model02->setItemsId($this->security->xss_clean($value['item_id']));
                // // $this->production_model02->setItemsName($this->security->xss_clean($value['item_name']));
                // // $this->production_model02->setItemsUnit($this->security->xss_clean($value['satuan']));
                // // $this->production_model02->setQty($this->security->xss_clean($value['item_qty']));
                // // $this->production_model02->insert();
            }
            
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                jsout(array('success' => 'gagal'));
            }else{
                $this->db->trans_commit();
                jsout(array('success' => true, 'status' => $save));
            }  

        }else{
            jsout(array('success' => 'gagal', 'status' => $stok_kurang));
        }
        
              
        
    }

    function edit($id){
        $this->session->unset_userdata('new_pro');
        $this->load->model('master/items_model');
        $new_pro = $this->session->userdata('new_pro');

        $detail     = $this->production_model02->getByIdCr($id)->result();
        $tdetail    = $this->production_model02->getByIdCr($id)->num_rows();
        // test($detail,1);

        $new_pro = $this->session->userdata('new_pro');

        if($tdetail==0){
            $new_pro = array(
                'items' => array()
            );
        }else{
            foreach($detail as $key=>$val){
                $new_pro['items'][$key] = array(
                    'det_id'        => $val->production_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'satuan'        => $val->items_unit,
                    'item_qty'      => $val->qty,
                    'item_info'     => ''
                );
            }
        }
        $this->session->set_userdata('new_pro', $new_pro);

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_mentah']    = $this->items_model->get_items_type(0);
        $data['data_produksi']  = $this->items_model->get_items_type(1);
        $data['header']         = $this->production_model->getById($id);
        $data['new_pro']        = $new_pro;
        // test($data,1);
        $this->template->load('body', 'stok/production/cp_edit', $data);
    }

    function edit_act(){

        $id = $this->security->xss_clean($_POST['production_01_id']);

        $this->production_model02->delete($id);
        $new_pro         = $this->session->userdata('new_pro');
        
        foreach ($new_pro['items'] as $key => $value) {
            // $this->production_model02->setProduction02Id($this->security->xss_clean($_POST['production02Id']));
            $this->production_model02->setProduction01Id($this->security->xss_clean($id));
            $this->production_model02->setItemsId($this->security->xss_clean($value['item_id']));
            $this->production_model02->setItemsName($this->security->xss_clean($value['item_name']));
            $this->production_model02->setItemsUnit($this->security->xss_clean($value['satuan']));
            $this->production_model02->setQty($this->security->xss_clean($value['item_qty']));
            $this->production_model02->insert();
        }

        // $this->production_model->setProduction01Id($this->security->xss_clean($id));
        $this->production_model->setItemsId($this->security->xss_clean($_POST['production_items_id']));
        $this->production_model->setItemsName($this->security->xss_clean($_POST['production_items_name']));
        $this->production_model->setQty($this->security->xss_clean($_POST['qty']));
        $this->production_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->production_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->production_model->update($id);

        $this->session->unset_userdata('new_pro');
        jsout(array('success' => true, 'status' => $update ));
    }

    function reject_js(){
        $delete = $this->production_model->act_reject_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    function delete_js(){
        $delete = $this->purchase_requisition_model->act_delete_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    

    function view_popup($id){

        $header         = $this->purchase_requisition_model->getById($id);
        $detail         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $myData = array();
        $no     = 0;
        foreach ($detail as $key => $row) {
            $no     = $no+1;
            $myData[] = array(
                $no,
                $row['items_code'],
                $row['items_name'],     
                number_format($row['qty'],2)        
            );            
        }  

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function view_print($id){
        $data['header']         = $this->purchase_requisition_model->print_header($id);
        $data['detail']         = $this->purchase_requisition_model02->print_detail($id);
        // $data['detail']         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $this->template->load('body_print', 'master/purchase_requisition/pr_print', $data);
    }





}

// Penambahan PR Print
?>