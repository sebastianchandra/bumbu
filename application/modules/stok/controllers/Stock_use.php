<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_use extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('stok/stock_use_model');
    }


    function index(){  
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Stock', 'active_submenu' => 'stok/stock_use'));
        $this->isMenu();

        $data['data_os']  = $this->stock_use_model->get_su();
        $this->template->load('body', 'stok/stock_use/su_view',$data);
    }

    function form(){
        $this->load->model('master/items_model');
        $this->load->model('master/warehouse_model');
        $this->load->model('master/users_group_model');

        $new_os = $this->session->userdata('new_os');

        if(!$new_os){
            $new_os = array(
                'items' => array()
            );
        }
        $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_items']     = $this->items_model->get_items();
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['user_group']     = $this->users_group_model->get_user_group();
        $data['new_os']        = $new_os;
        $this->template->load('body', 'stok/stock_use/su_form', $data);
    }

    function reset(){
        $this->session->unset_userdata('new_os');
        redirect('stok/stock_use');
    }

    function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_os = $this->session->userdata('new_os');
        $items = $new_os['items'];

        $exist = false;
        if($items!=''){
        foreach($items as $key=>$val){
                if($val['item_id'] == $this->input->post('item_id')){
                    $new_os['items'][$key] = array(
                        'item_id'       => $this->input->post('item_id'),
                        'item_name'     => $this->input->post('item_name'),
                        'item_qty'      => $this->input->post('item_qty'),
                        'item_info'     => $this->input->post('item_info')
                    );
                    $exist = true;
                    break;
                }
            }
        }

        if(!$exist){
            $new_os['items'][] = array(
                'item_id'       => $this->input->post('item_id'),
                'item_name'     => $this->input->post('item_name'),
                'item_qty'      => $this->input->post('item_qty'),
                'item_info'     => $this->input->post('item_info')
            );
        }
        
        $this->session->set_userdata('new_os', $new_os);        
    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_os = $this->session->userdata('new_os');

        $items = $new_os['items'];

        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                unset($new_os['items'][$key]);
                $new_os['items'] = array_values($new_os['items']);
                break;
            }
        }

        $this->session->set_userdata('new_os', $new_os);
        jsout(array('success'=>1)); 
    }

    function form_act(){
        $save   = $this->stock_use_model->act_form();

        $this->session->unset_userdata('new_os');
        jsout(array('success' => true, 'status' => $save ));
    }

    function print_su($id){
        $data['header']     = $this->stock_use_model->detail_os($id);
        $data['detail']     = $this->stock_use_model->detail_items_os($id);
        $this->template->load('body', 'stok/stock_use/su_print',$data);
    }

    function popup_ss($id){
        $data['id']     = $id;
        $data['header']     = $this->stock_use_model->detail_os($id);
        $data['detail']     = $this->stock_use_model->detail_items_os($id);

        $this->load->view('stok/stock_use/su_detail',$data);
        
    }

}
?>