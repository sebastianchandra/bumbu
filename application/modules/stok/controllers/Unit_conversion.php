<?php
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : May 06, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Unit_conversion extends MY_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
          $this->session->set_userdata('ses_menu', array('active_menu' => 'Stock', 'active_submenu' => 'stok/unit_conversion'));
          $this->isMenu();
     	$this->load->helper('security');
          $this->load->model('master/warehouse_model');
          $this->load->model('master/items_model');
          $this->load->model('stok/stok_model');
          $this->load->model('stok/stok_by_doc_model');
          $this->load->model('stok/stok_hist_model');
     	$this->load->model('stok/unit_conversion_model');
     }
     /* END CONSTRUCTOR */
     function index(){
          $data['data_uc']     = $this->unit_conversion_model->get_data();
          $this->template->load('body', 'stok/unit_conversion/uc_view',$data);
     }

     function form(){
          $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
          $data['data_items']     = $this->items_model->get_items();
          $this->template->load('body', 'stok/unit_conversion/uc_form',$data);
     }
     /* START INSERT */
     public function form_act()
     {
          $this->db->trans_begin();

          $stok_fifo      = $this->stok_by_doc_model->dataStock($_POST['items_id'],$_POST['warehouse_id'])->result();

          $stok_barang1   = $_POST['items_qty'];
          $is_stop        = 0;
          $tbarang        = 0;

          foreach ($stok_fifo as $key => $value2) {
               if($value2->items_remaining>=$stok_barang1){
                    $tbarang        = $tbarang+($stok_barang1*$value2->items_price);
                    $sisa           = $value2->items_remaining - $stok_barang1;
                    $stok_barang1   = $value2->items_remaining - $stok_barang1;
                    $cek            = ($stok_barang1*$value2->items_price);

                    $is_stop        = 1;
               }else{
                    $tbarang        = $tbarang+($value2->items_remaining * $value2->items_price);
                    $sisa           = 0;
                    $stok_barang1   = $stok_barang1 - $value2->items_remaining;
                    $cek            = ($value2->items_remaining * $value2->items_price);

                    $is_stop        = 0;
               }

               $this->stok_by_doc_model->update_stok($value2->stock_by_doc_id,$sisa);

               if($is_stop==1){
                   break; 
               }   
          }

          $price_out          = $tbarang / $_POST['items_qty'];
          $total              = $price_out*$_POST['items_qty'];
          $price_in           = $total / $_POST['items_qty_to'];

          // $data_id            = $this->unit_conversion_model->get_id()->is_id;
          $periode            = substr(dbnow(),0,4).substr(dbnow(),5,2);
          $kode               = 'UVS';
          $data_no            = $this->unit_conversion_model->get_nomor_dok($periode,$kode)->nomor_dok;
          $uv_no              = $kode.$periode.$data_no;

          $activity_in        = 'In_Conv';
          $activity_out       = 'Out_Conv';

          //Start Items In//
          $cek_stok           = $this->stok_model->cek_stock($_POST['items_id_to'],$_POST['warehouse_id'])->num_rows();
          $company_id         = $this->current_user['company_id'];
          $company_name       = $this->current_user['company_name'];
                
          if($cek_stok>=1){

               $detail_stok    = $this->stok_model->cek_stock($_POST['items_id_to'],$_POST['warehouse_id'])->row();

               $old_stok1      = $detail_stok->current_stock;
               $current_stock1 = $old_stok1+$_POST['items_qty_to'];
               
               $id = $detail_stok->stock_id;
               // $this->stok_model->getObjectById($id);
               // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
               $this->stok_model->setItemsId($this->security->xss_clean($_POST['items_id_to']));
               $this->stok_model->setDocNo($this->security->xss_clean($uv_no));
               $this->stok_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
               $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
               $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
               $this->stok_model->setTrnDate($this->security->xss_clean(dbnow()));
               $this->stok_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
               $this->stok_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
               $this->stok_model->setActivity($this->security->xss_clean($activity_in));
               $this->stok_model->setItemsIn($this->security->xss_clean($_POST['items_qty_to']));
               // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
               // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
               // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
               $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
               $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
               $this->stok_model->update($id);
               // test('1',1);
          }else{
               // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
               $this->stok_model->setItemsId($this->security->xss_clean($_POST['items_id_to']));
               $this->stok_model->setDocNo($this->security->xss_clean($uv_no));
               $this->stok_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
               $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
               $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
               $this->stok_model->setTrnDate($this->security->xss_clean(dbnow()));
               $this->stok_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
               $this->stok_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
               $this->stok_model->setActivity($this->security->xss_clean($activity_in));
               $this->stok_model->setItemsIn($this->security->xss_clean($_POST['items_qty_to']));
               $this->stok_model->setItemsOut($this->security->xss_clean(0));
               $this->stok_model->setAdjIn($this->security->xss_clean(0));
               $this->stok_model->setAdjOut($this->security->xss_clean(0));
               $this->stok_model->setOldStock($this->security->xss_clean(0));
               $this->stok_model->setCurrentStock($this->security->xss_clean($_POST['items_qty_to']));
               $this->stok_model->insert();
          }

          $id_by_doc      = substr(dbnow(),0,4).substr(dbnow(),5,2).$this->stok_by_doc_model->getId(substr(dbnow(),0,4),substr(dbnow(),5,2))->row()->id;

          $row_stok_doc   = $this->stok_by_doc_model->lastStok($_POST['items_id_to'],$_POST['warehouse_id'])->num_rows();

          if($row_stok_doc>=1){
               $laststok       = $this->stok_by_doc_model->lastStok($_POST['items_id_to'],$_POST['warehouse_id'])->row();

               $oldstock       = $laststok->current_stock;
               $current_stock  = $laststok->current_stock + $_POST['items_qty_to'];
          }else{
               $oldstock       = 0;
               $current_stock  = $_POST['items_qty_to'];
          }

          // $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
          $this->stok_by_doc_model->setItemsId($this->security->xss_clean($_POST['items_id_to']));
          $this->stok_by_doc_model->setDocNo($this->security->xss_clean($uv_no));
          $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
          $this->stok_by_doc_model->setTrnDate($this->security->xss_clean(dbnow()));
          $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
          $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
          $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity_in));
          $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($_POST['items_qty_to']));
          $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean($_POST['items_qty_to']));
          // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
          $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
          $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
          $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean(round($price_in,2)));
          $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
          $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
          $this->stok_by_doc_model->insert();

          $row_stok_doc   = $this->stok_hist_model->lastStokHist($_POST['items_id_to'],$_POST['warehouse_id'])->num_rows();
          if($row_stok_doc>=1){
               $laststok_doc       = $this->stok_hist_model->lastStokHist($_POST['items_id_to'],$_POST['warehouse_id'])->row();

               $oldstock_doc       = $laststok_doc->current_stock;
               $current_stock_doc  = $laststok_doc->current_stock + $_POST['items_qty_to'];
          }else{
               $oldstock_doc       = 0;
               $current_stock_doc  = $_POST['items_qty_to'];
          }
          // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
          $this->stok_hist_model->setItemsId($this->security->xss_clean($_POST['items_id_to']));
          $this->stok_hist_model->setDocNo($this->security->xss_clean($uv_no));
          $this->stok_hist_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
          $this->stok_hist_model->setCompanyId($this->security->xss_clean($company_id));
          $this->stok_hist_model->setCompanyName($this->security->xss_clean($company_name));
          $this->stok_hist_model->setTrnDate($this->security->xss_clean(dbnow()));
          $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
          $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
          $this->stok_hist_model->setActivity($this->security->xss_clean($activity_in));
          $this->stok_hist_model->setQty($this->security->xss_clean($_POST['items_qty_to']));
          $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
          $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
          $this->stok_hist_model->insert();
          //End Items In//

          //Start Items Out//
          $detail_stok    = $this->stok_model->cek_stock($_POST['items_id'],$_POST['warehouse_id'])->row();

          $old_stok1      = $detail_stok->current_stock;
          $current_stock1 = $old_stok1-$_POST['items_qty'];
           
          $id = $detail_stok->stock_id;
          // $this->stok_model->getObjectById($id);
          // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
          $this->stok_model->setItemsId($this->security->xss_clean($_POST['items_id']));
          $this->stok_model->setDocNo($this->security->xss_clean($uv_no));
          $this->stok_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
          $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
          $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
          $this->stok_model->setTrnDate($this->security->xss_clean(dbnow()));
          $this->stok_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
          $this->stok_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
          $this->stok_model->setActivity($this->security->xss_clean($activity_out));
          $this->stok_model->setItemsIn($this->security->xss_clean(0));
          $this->stok_model->setItemsOut($this->security->xss_clean($_POST['items_qty']));
          // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
          // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
          // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
          $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
          $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
          $this->stok_model->update($id);
          // test('2',1);

          $row_stok_doc   = $this->stok_hist_model->lastStokHist($_POST['items_id'],$_POST['warehouse_id'])->num_rows();
          if($row_stok_doc>=1){
               $laststok_doc       = $this->stok_hist_model->lastStokHist($_POST['items_id'],$_POST['warehouse_id'])->row();
               $oldstock_doc       = $laststok_doc->current_stock;
               $current_stock_doc  = $laststok_doc->current_stock - $_POST['items_qty'];
          }else{
               $oldstock_doc       = 0;
               $current_stock_doc  = $_POST['items_qty'];
          }
          // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
          $this->stok_hist_model->setItemsId($this->security->xss_clean($_POST['items_id']));
          $this->stok_hist_model->setDocNo($this->security->xss_clean($uv_no));
          $this->stok_hist_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
          $this->stok_hist_model->setCompanyId($this->security->xss_clean($company_id));
          $this->stok_hist_model->setCompanyName($this->security->xss_clean($company_name));
          $this->stok_hist_model->setTrnDate($this->security->xss_clean(dbnow()));
          $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
          $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
          $this->stok_hist_model->setActivity($this->security->xss_clean($activity_out));
          $this->stok_hist_model->setQty($this->security->xss_clean($_POST['items_qty']));
          $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
          $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
          $this->stok_hist_model->insert();

          $id_by_doc      = substr(dbnow(),0,4).substr(dbnow(),5,2).$this->stok_by_doc_model->getId(substr(dbnow(),0,4),substr(dbnow(),5,2))->row()->id;
          $row_stok_doc   = $this->stok_by_doc_model->lastStok($_POST['items_id'],$_POST['warehouse_id'])->num_rows();

          if($row_stok_doc>=1){
              $laststok       = $this->stok_by_doc_model->lastStok($_POST['items_id'],$_POST['warehouse_id'])->row();

              $oldstock       = $laststok->current_stock;
              $current_stock  = $laststok->current_stock - $_POST['items_qty'];
          }else{
              $oldstock       = 0;
              $current_stock  = $_POST['items_qty'];
          }

          $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
          $this->stok_by_doc_model->setItemsId($this->security->xss_clean($_POST['items_id']));
          $this->stok_by_doc_model->setDocNo($this->security->xss_clean($uv_no));
          $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
          $this->stok_by_doc_model->setTrnDate($this->security->xss_clean(dbnow()));
          $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr(dbnow(),0,4)));
          $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr(dbnow(),5,2)));
          $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity_out));
          $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['items_qty']));
          $this->stok_by_doc_model->setItemsIn($this->security->xss_clean(0));
          $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean(0));
          // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
          $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
          $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
          $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean(round($price_out,2)));
          $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
          $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
          $this->stok_by_doc_model->insert();
          //End Items Out//

          $this->unit_conversion_model->setUvNo($this->security->xss_clean($uv_no));
     	$this->unit_conversion_model->setWarehouseId($this->security->xss_clean($_POST['warehouse_id']));
     	$this->unit_conversion_model->setItemsIdFrom($this->security->xss_clean($_POST['items_id']));
     	$this->unit_conversion_model->setUnitsIdFrom($this->security->xss_clean($_POST['items_unit']));
     	$this->unit_conversion_model->setQtyFrom($this->security->xss_clean($_POST['items_qty']));
     	$this->unit_conversion_model->setPriceFrom($this->security->xss_clean(round($price_out,2)));
     	$this->unit_conversion_model->setItemsIdTo($this->security->xss_clean($_POST['items_id_to']));
     	$this->unit_conversion_model->setUnitsIdTo($this->security->xss_clean($_POST['items_unit_to']));
     	$this->unit_conversion_model->setQtyTo($this->security->xss_clean($_POST['items_qty_to']));
     	$this->unit_conversion_model->setPriceTo($this->security->xss_clean(round($price_in,2)));
     	$this->unit_conversion_model->setRemarks($this->security->xss_clean($_POST['remarks']));
     	$this->unit_conversion_model->setIsActive($this->security->xss_clean('1'));
     	$this->unit_conversion_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
     	$this->unit_conversion_model->setInputTime($this->security->xss_clean(dbnow()));
     	$save         = $this->unit_conversion_model->insert();

          if ($this->db->trans_status() === FALSE){
               $this->db->trans_rollback();
               jsout(array('success' => true, 'status' => 'ERROR INSERT'));
          }else{
               $this->db->trans_commit();
               jsout(array('success' => true, 'status' => $save/*,  'os_no' => $os_no*/));
        }
     }
     /* END INSERT */
     /* START UPDATE */
     public function upd()
     {
     	$id = $this->security->xss_clean($_POST['uvId']);
     	$this->unit_conversion_model->getObjectById($id);
     	$this->unit_conversion_model->setUvId($this->security->xss_clean($_POST['uvId']));
     	$this->unit_conversion_model->setWarehouseId($this->security->xss_clean($_POST['warehouseId']));
     	$this->unit_conversion_model->setItemsIdFrom($this->security->xss_clean($_POST['itemsIdFrom']));
     	$this->unit_conversion_model->setUnitsIdFrom($this->security->xss_clean($_POST['unitsIdFrom']));
     	$this->unit_conversion_model->setQtyFrom($this->security->xss_clean($_POST['qtyFrom']));
     	$this->unit_conversion_model->setPriceFrom($this->security->xss_clean($_POST['priceFrom']));
     	$this->unit_conversion_model->setItemsIdTo($this->security->xss_clean($_POST['itemsIdTo']));
     	$this->unit_conversion_model->setUnitsIdTo($this->security->xss_clean($_POST['unitsIdTo']));
     	$this->unit_conversion_model->setQtyTo($this->security->xss_clean($_POST['qtyTo']));
     	$this->unit_conversion_model->setPriceTo($this->security->xss_clean($_POST['priceTo']));
     	$this->unit_conversion_model->setRemarks($this->security->xss_clean($_POST['remarks']));
     	$this->unit_conversion_model->setIsActive($this->security->xss_clean($_POST['isActive']));
     	$this->unit_conversion_model->setPicInput($this->security->xss_clean($_POST['picInput']));
     	$this->unit_conversion_model->setInputTime($this->security->xss_clean($_POST['inputTime']));
     	$this->unit_conversion_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
     	$this->unit_conversion_model->setEditTime($this->security->xss_clean($_POST['editTime']));
     	$this->unit_conversion_model->update($id);
     }
     /* END UPDATE */
     /* START DELETE */
     public function del()
     {
     	$id = $this->security->xss_clean($_POST['uvId']);
     	$this->unit_conversion_model->delete($id);
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->unit_conversion_model->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */
     function reject_js(){
          $uv_no         = $this->input->post('uv_no');
          $uv_id         = $this->input->post('uv_id');

          $company_id     = $this->current_user['company_id'];
          $company_name   = $this->current_user['company_name'];

          $data_by_doc    = $this->stok_by_doc_model->data_by_doc($uv_no);

          foreach ($data_by_doc as $key => $value1) {
               if($value1->activity=='In_Conv'){
                    // test($value1,0);
                    $items_id           = $value1->items_id;
                    $doc_no             = $value1->doc_no;
                    $warehouse_id       = $value1->warehouse_id;
                    $items_in           = $value1->items_in;
                    $activity           = 'Rj_ConvIn';
                    $new_doc            = substr($doc_no,0,2).'R'.substr($doc_no,3,10);
                    $trn_date_new       = dbnow();
                    $trn_year_new       = substr(dbnow(),0,4);
                    $trn_month_new      = substr(dbnow(),5,2);

                    $trn_date           = $value1->trn_date;
                    $trn_year           = $value1->trn_year;
                    $trn_month          = $value1->trn_month;
                    $items_price        = $value1->items_price;

                    $old_stock          = $this->stok_model->old_stock($items_id,$warehouse_id)->current_stock;
                    $current_stock      = $old_stock-$items_in;

                    $this->stok_model->update_stock_is($trn_date_new,$trn_year_new,$trn_month_new,$activity,$items_in,$old_stock,$current_stock,$items_id,$warehouse_id);

                    $q_lastbydoc        = $this->stok_by_doc_model->q_lastbydocfor_uc($items_id,$warehouse_id);
                    $stock_by_doc_id    = $q_lastbydoc->stock_by_doc_id;
                    $last_remaining     = $q_lastbydoc->items_remaining-$items_in;

                    $this->stok_by_doc_model->ustok_remaining($last_remaining,$stock_by_doc_id);
                    $this->stok_hist_model->insert_hist($items_id,$new_doc,$warehouse_id,$company_id,$company_name,$trn_date,$trn_year,$trn_month,$activity,$items_in,$old_stock,$current_stock);
                    $this->stok_by_doc_model->insert_by_doc_in($items_id,$new_doc,$warehouse_id,$trn_date,$trn_year,$trn_month,$activity,$items_in,$old_stock,$current_stock,$items_price);
               }elseif($value1->activity=='Out_Conv'){
                    // test($value1,0);
                    $items_id           = $value1->items_id;
                    $doc_no             = $value1->doc_no;
                    $warehouse_id       = $value1->warehouse_id;
                    $items_out          = $value1->items_out;
                    $activity           = 'Rj_ConvOut';
                    $new_doc            = substr($doc_no,0,2).'R'.substr($doc_no,3,10);
                    $trn_date_new       = dbnow();
                    $trn_year_new       = substr(dbnow(),0,4);
                    $trn_month_new      = substr(dbnow(),5,2);

                    $trn_date           = $value1->trn_date;
                    $trn_year           = $value1->trn_year;
                    $trn_month          = $value1->trn_month;
                    $items_price        = $value1->items_price;

                    $old_stock          = $this->stok_model->old_stock($items_id,$warehouse_id)->current_stock;
                    $current_stock      = $old_stock+$items_out;

                    // test($current_stock,1);
                    $this->stok_model->update_stock($trn_date_new,$trn_year_new,$trn_month_new,$activity,$items_out,$old_stock,$current_stock,$items_id,$warehouse_id);

                    $q_lastbydoc        = $this->stok_by_doc_model->q_lastbydoc($items_id,$warehouse_id);
                    $stock_by_doc_id    = $q_lastbydoc->stock_by_doc_id;
                    $last_remaining     = $q_lastbydoc->items_remaining+$items_out;

                    $this->stok_by_doc_model->ustok_remaining($last_remaining,$stock_by_doc_id);
                    $this->stok_hist_model->insert_hist($items_id,$new_doc,$warehouse_id,$company_id,$company_name,$trn_date,$trn_year,$trn_month,$activity,$items_out,$old_stock,$current_stock);
                    $this->stok_by_doc_model->insert_by_doc($items_id,$new_doc,$warehouse_id,$trn_date,$trn_year,$trn_month,$activity,$items_out,$old_stock,$current_stock,$items_price);
               }
          }

          $this->unit_conversion_model->reject_js($uv_id);
     }
}
?>
