<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Incoming_stok extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Stock', 'active_submenu' => 'stok/incoming_stok'));
        $this->isMenu();

        // $this->load->model('master/project_model');
        $this->load->model('master/warehouse_model');
        $this->load->model('stok/incoming_stok_model');
        $this->load->model('stok/incoming_stok_model02');
        $this->load->model('stok/incoming_stok_model03');
        $this->load->model('stok/outgoing_stok_model');
        $this->load->model('stok/outgoing_stok_model02');
        $this->load->model('transaction/purchase_order_model');
        $this->load->model('transaction/purchase_order_model02');
        $this->load->model('stok/stok_model');
        $this->load->model('stok/stok_by_doc_model');
        $this->load->model('stok/stok_hist_model');
    }


    function index(){  
        // test($this->input->post('datepicker_to'),1);
        // $data['data_po']  = $this->incoming_stok_model->get_po_out();
        if($this->input->post('datepicker_from')!='' AND $this->input->post('datepicker_to')!='' ){
            $data['data_is']  = $this->incoming_stok_model->get_is_date($this->input->post('datepicker_from'),$this->input->post('datepicker_to'));
        }else{
            $data['data_is']  = $this->incoming_stok_model->get_is();
        }
        // $data['data_os']  = $this->incoming_stok_model->get_os_not_Stock_use();
        $this->template->load('body', 'stok/incoming_stok/is_view',$data);
    }

    function form(){
        $this->session->unset_userdata('new_co');

        $new_co = $this->session->userdata('new_co');

        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_referensi'] = $this->incoming_stok_model->get_po_out();
        $data['new_co']         = $new_co;

        $this->template->load('body', 'stok/incoming_stok/is_form',$data);
    }

    function show_detail($noDoc,$idDoc){
        $doc_code       = substr($noDoc,0,2);
        if($doc_code=='OS'){
            $detail         = $this->outgoing_stok_model02->getByIdOs($noDoc)->result_array();
            $tdetail        = $this->outgoing_stok_model02->getByIdOs($noDoc)->num_rows();
            $theader        = $this->outgoing_stok_model->getByDoc($noDoc)->row();
        }else{
            $detail         = $this->purchase_order_model02->getByIdPo($noDoc)->result_array();
            $tdetail        = $this->purchase_order_model02->getByIdPo($noDoc)->num_rows();
            $theader        = $this->purchase_order_model->getByDoc($noDoc)->row();
        }
        $myData         = array();
        foreach ($detail as $key => $row) {
            $sisa       = $row['qty']-$row['qty_in'];
            $myData[] = array(
                $row['items_name'],     
                money($row['qty']),
                money($row['qty_in']),
                '<input type="number" class="form-control" id="qty_terima'.$key.'" name="qty_terima[]" value="'.$sisa.'">
                 <input type="hidden" class="form-control" id="items_id" name="items_id[]" value="'.$row['items_id'].'">
                 <input type="hidden" class="form-control" id="items_qty'.$key.'" name="items_qty[]" value="'.$sisa.'">
                 <input type="hidden" class="form-control" id="items_name'.$key.'" name="items_name[]" value="'.$row['items_name'].'">
                '
            );     
        }        

        return jsout(array('detail'=> json_encode($myData),'tdetail'=>$tdetail,'theader'=>$theader ));
    }

    function add_item_lain(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_io    = $this->session->userdata('new_io');
        $items      = $new_io['items'];

        $new_io['items'][] = array(
            // 'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'item_info'     => $this->input->post('item_info'),
            'price'         => $this->input->post('price'),
            'disc'          => $this->input->post('disc')
        );

        $exist = false;
        // test($new_io,0);
        $this->session->set_userdata('new_io', $new_io);         
    }

    function add_item(){
        // test($_POST['cost_id'],1);
        if(!isset($_POST['cost_id'])) return;
        $new_co    = $this->session->userdata('new_co');
        $items      = $new_co['items'];

        $new_co['items'][] = array(
            'cost_id'        => $this->input->post('cost_id'),
            'cost_name'      => $this->input->post('cost_name'),
            'cost_value'     => $this->input->post('cost_value')
        );

        $exist = false;
        // test($new_co,0);
        $this->session->set_userdata('new_co', $new_co);         
    }

    function form_act(){


        $this->db->trans_begin();

        // $to_local           = $this->input->post('to_local');
        $sender_pic         = $this->input->post('sender_pic');
        $receiver_pic       = $this->input->post('receiver_pic');
        $remarks            = $this->input->post('remarks');
        $warehouse_project  = $this->input->post('warehouse_project');
        $company_id         = $this->current_user['company_id'];
        $company_name       = $this->current_user['company_name'];

        $data_id            = $this->incoming_stok_model->get_id()->is_id;
        $periode            = substr($this->input->post('is_date'),0,4).substr($this->input->post('is_date'),5,2);
        $kode               = 'IS';
        $data_no            = $this->incoming_stok_model->get_nomor_dok($periode,$kode)->nomor_dok;

        $doc_ref            = $this->input->post('referensi_id');
        $is_date            = $this->input->post('is_date');
        $is_no              = $kode.'S'.$periode.$data_no;
        // test($this->input->post('is_date').' '.$is_no,1);

        $tdetail            = $this->input->post('tdetail');

        $doc_code       = substr($this->input->post('no_referensi'),0,2);

        $new_co     = $this->session->userdata('new_co');

        $tqty       = 0;
        $tvalue     = 0;
        $this->dbpurch = $this->load->database('purchasing',true);

        if($new_co['items']!=''){
            foreach ($new_co['items'] as $key => $value) {
                $cost_name          = $value['cost_name'];
                $cost_value         = $value['cost_value'];
                $tvalue             = $tvalue+$cost_value;

                $this->dbpurch->query("INSERT INTO `db_bumbu_transaction`.`trn_incoming_stock_03` 
                                (is_id,cost_name,cost_value) VALUES 
                                ('".$data_id."','".$cost_name."','".$cost_value."')");
            }
        }

        foreach ($this->input->post('items_id') as $key => $value) {
            $qty_terima         = $this->input->post('qty_terima')[$key];
            $tqty               = $tqty+$qty_terima;
        }

        if($tvalue>0){
            $biaya_per_qty           = $tvalue/$tqty;
        }else{
            $biaya_per_qty           = 0;
        }

        if($doc_code=='OS'){
            $activity       = "In_Trans";
        }elseif($doc_code=='PO'){
            $activity       = "In_Po";
        }

        $complate       = 0;

        foreach ($this->input->post('items_id') as $key => $value) {
            $items_id               = $value;
            $items_qty              = $this->input->post('items_qty')[$key];
            $items_name             = $this->input->post('items_name')[$key];
            $qty_terima             = $this->input->post('qty_terima')[$key];

            if($items_qty == $qty_terima){
                $complate   = $complate+1;
            }

            if($doc_code=='OS'){
                $detail_po              = $this->outgoing_stok_model02->detail_os($doc_ref,$items_id);
                $diskon_value           = 0;
                if($items_qty==0){
                    $price              = $detail_po->price;
                }else{
                    $price              = round((($detail_po->price*$items_qty)-$diskon_value)/$items_qty,2);                    
                }
            }else{
                $detail_po              = $this->purchase_order_model02->detail_po($doc_ref,$items_id);
                $diskon_value           = $detail_po->disc_val;
                if($items_qty==0){
                    $price              = $detail_po->price;
                }else{
                    $price              = round((($detail_po->price*$items_qty)-$diskon_value)/$items_qty,2);                    
                }
            }
            
            $price                  = $price+$biaya_per_qty;
            $total                  = $qty_terima*$price;

            // $this->incoming_stok_model02->setIs02Id($this->security->xss_clean($_POST['is02Id']));
            $this->incoming_stok_model02->setIsId($this->security->xss_clean($data_id));
            $this->incoming_stok_model02->setItemsId($this->security->xss_clean($value));
            $this->incoming_stok_model02->setItemsName($this->security->xss_clean($this->input->post('items_name')[$key]));
            $this->incoming_stok_model02->setQty($this->security->xss_clean($this->input->post('qty_terima')[$key]));
            $this->incoming_stok_model02->setPoQty($this->security->xss_clean($this->input->post('items_qty')[$key]));
            $this->incoming_stok_model02->setPrice($this->security->xss_clean($price));
            $this->incoming_stok_model02->setTotal($this->security->xss_clean($total));
            $this->incoming_stok_model02->setRemarks($this->security->xss_clean(''));
            $this->incoming_stok_model02->insert();

            // if($to_local=='warehouse'){

            //Start From Purchase Order
            $cek_stok       = $this->stok_model->cek_stock($items_id,$warehouse_project)->num_rows();
            

            if($cek_stok>=1){

                $detail_stok    = $this->stok_model->cek_stock($items_id,$warehouse_project)->row();

                $old_stok1      = $detail_stok->current_stock;
                $current_stock1 = $old_stok1+$qty_terima;
                
                $id = $detail_stok->stock_id;
                // $this->stok_model->getObjectById($id);
                // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                $this->stok_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_model->setActivity($this->security->xss_clean($activity));
                $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
                // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
                // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
                $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
                $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
                $this->stok_model->update($id);
            }else{
                // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                $this->stok_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_model->setActivity($this->security->xss_clean($activity));
                $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                $this->stok_model->setItemsOut($this->security->xss_clean(0));
                $this->stok_model->setAdjIn($this->security->xss_clean(0));
                $this->stok_model->setAdjOut($this->security->xss_clean(0));
                $this->stok_model->setOldStock($this->security->xss_clean(0));
                $this->stok_model->setCurrentStock($this->security->xss_clean($qty_terima));
                $this->stok_model->insert();
            }

            $id_by_doc      = substr($this->input->post('is_date'),0,4).substr($this->input->post('is_date'),5,2).$this->stok_by_doc_model->getId(substr($this->input->post('is_date'),0,4),substr($this->input->post('is_date'),5,2))->row()->id;

            $row_stok_doc   = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->num_rows();

            if($row_stok_doc>=1){
                $laststok       = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->row();

                $oldstock       = $laststok->current_stock;
                $current_stock  = $laststok->current_stock + $qty_terima;
            }else{
                $oldstock       = 0;
                $current_stock  = $qty_terima;
            }

            // $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
            $this->stok_by_doc_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_by_doc_model->setDocNo($this->security->xss_clean($is_no));
            $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($warehouse_project));
            $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($is_date));
            $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
            $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
            $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity));
            $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($qty_terima));
            $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean($qty_terima));
            // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
            $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
            $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
            $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($price));
            $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
            $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
            $this->stok_by_doc_model->insert();


            $row_stok_doc   = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->num_rows();
            if($row_stok_doc>=1){
                $laststok_doc       = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->row();

                $oldstock_doc       = $laststok_doc->current_stock;
                $current_stock_doc  = $laststok_doc->current_stock + $qty_terima;
            }else{
                $oldstock_doc       = 0;
                $current_stock_doc  = $qty_terima;
            }
            // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
            $this->stok_hist_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_hist_model->setDocNo($this->security->xss_clean($is_no));
            $this->stok_hist_model->setWarehouseId($this->security->xss_clean($warehouse_project));
            $this->stok_hist_model->setCompanyId($this->security->xss_clean($company_id));
            $this->stok_hist_model->setCompanyName($this->security->xss_clean($company_name));
            $this->stok_hist_model->setTrnDate($this->security->xss_clean($is_date));
            $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
            $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
            $this->stok_hist_model->setActivity($this->security->xss_clean($activity));
            $this->stok_hist_model->setQty($this->security->xss_clean($qty_terima));
            $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
            $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
            $this->stok_hist_model->insert();

            // End From Purchase Order
                
            // }

        }

        if($complate==$tdetail){
            $status         = 'Closed';
        }else{
            $status         = 'Outstanding';
        }

        $this->incoming_stok_model->setIsId($this->security->xss_clean($data_id));
        $this->incoming_stok_model->setIsDate($this->security->xss_clean($is_date));
        $this->incoming_stok_model->setIsNo($this->security->xss_clean($is_no));
        $this->incoming_stok_model->setProjectId($this->security->xss_clean(''));
        $this->incoming_stok_model->setProjectName($this->security->xss_clean(''));
        $this->incoming_stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
        $this->incoming_stok_model->setIsKind($this->security->xss_clean('1'));
        $this->incoming_stok_model->setDocRef($this->security->xss_clean($doc_ref));
        $this->incoming_stok_model->setCompanyId($this->security->xss_clean($company_id));
        $this->incoming_stok_model->setCompanyName($this->security->xss_clean($company_name ));
        $this->incoming_stok_model->setIsStatus($this->security->xss_clean('Closed'));
        $this->incoming_stok_model->setSenderPic($this->security->xss_clean($sender_pic));
        $this->incoming_stok_model->setReceiverPic($this->security->xss_clean($receiver_pic));
        $this->incoming_stok_model->setRemarks($this->security->xss_clean($remarks));
        $this->incoming_stok_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->incoming_stok_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->incoming_stok_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->incoming_stok_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        $this->incoming_stok_model->insert();

        if($doc_code=='OS'){
            $this->outgoing_stok_model->update_status_outgoing($doc_ref,$status);
        }else{
            $this->purchase_order_model->update_status_incoming($doc_ref,$status);
        }        

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->session->set_flashdata('alert','Data Gagal Disimpan');
        }else{
            $this->db->trans_commit();
            $this->session->set_flashdata('alert','Data Berhasil disimpan Dengan Nomor '.$is_no);
        }
        redirect('stok/incoming_stok');

    }

    function view_popup($id){

        $header         = $this->incoming_stok_model->getById($id)->row();
        $detail         = $this->incoming_stok_model02->getDetailIs($id)->result_array();
        $myData         = array();
        $subtotal       = 0;
        $total          = 0;
        foreach ($detail as $key => $row) {
            $subtotal       = $row['price']*$row['qty'];
            $total          = $subtotal+$total;
            $myData[] = array(
                $row['items_name'],     
                number_format($row['qty'],2),
                number_format($row['price'],2),
                number_format($subtotal,2)
            );     
        }   
            $myData[] = array(
                '',
                '',
                'Total',
                number_format($total,2)
            );      

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function view_popup_others($id){

        $header         = $this->incoming_stok_model->getById($id)->row();
        $detail         = $this->incoming_stok_model02->getDetailIs($id)->result_array();
        $myData         = array();
        $subtotal       = 0;
        $total          = 0;
        foreach ($detail as $key => $row) {
            $subtotal       = ($row['price']*$row['qty'])-$row['discount'];
            $total          = $subtotal+$total;
            $myData[] = array(
                $row['items_name'],     
                number_format($row['qty'],2),
                number_format($row['price'],2),
                number_format($row['discount'],2),
                number_format($subtotal,2)
            );     
        }   
            $myData[] = array(
                '',
                '',
                '',
                'Total',
                number_format($total,2)
            );      

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function form_others(){
        $this->session->unset_userdata('new_io');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');


        $new_io = $this->session->userdata('new_io');

        if(!$new_io){
            $new_io = array(
                'items' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        $data['new_io']         = $new_io;
        // test($data,1);
        $this->template->load('body', 'stok/incoming_stok/is_form_others', $data);
    }

    /*function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_io    = $this->session->userdata('new_io');
        $items      = $new_io['items'];

        $new_io['items'][] = array(
            // 'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'item_info'     => $this->input->post('item_info'),
            'price'         => $this->input->post('price'),
            'disc'          => $this->input->post('disc')
        );

        $exist = false;
        // test($new_io,0);
        $this->session->set_userdata('new_io', $new_io);         
    }*/

    function form_others_act(){

        $this->db->trans_begin();
        
        $new_io             = $this->session->userdata('new_io');

        // $to_local           = $this->input->post('to_local');
        $remarks            = $this->input->post('remarks');
        $warehouse_project  = $this->input->post('warehouse_project');
        $company_id         = $this->current_user['company_id'];
        $company_name       = $this->current_user['company_name'];

        $data_id            = $this->incoming_stok_model->get_id()->is_id;
        $periode            = substr($this->input->post('is_date'),0,4).substr($this->input->post('is_date'),5,2);
        $kode               = 'IS';
        $data_no            = $this->incoming_stok_model->get_nomor_dok($periode,$kode)->nomor_dok;

        $is_date            = $this->input->post('is_date');
        $is_requester       = $this->input->post('is_requester');
        $is_no              = $kode.'T'.$periode.$data_no;

        if($this->input->post('is_kind')=='Others'){
            $activity           = "In_Others";
            $is_kind            = '2';
        }elseif($this->input->post('is_kind')=='Adjustment'){
            $activity           = "In_Adjustment";
            $is_kind            = '3';
        }
        


        // test($to_local.' '.$remarks.' '.$warehouse_project.' '.$is_date.' '.$is_requester.' '.$is_no,1);
        // test($new_io,1);


        // test($os_no.' '.$id,1);

        $new_io         = $this->session->userdata('new_io');
        
        foreach ($new_io['items'] as $key => $value) {

            $items_id               = $value['item_id'];
            $items_qty              = '0';
            $items_name             = $value['item_name'];
            $price                  = str_replace(',','',$value['price']);
            if($value['disc']!=''){
                $disc                   = str_replace(',','',$value['disc']);
            }else{
                $disc                   = '0.00';
            }
            $qty_terima             = str_replace(',','',$value['item_qty']);
            
            $total                  = ($qty_terima*$price)-$disc;

            // $this->incoming_stok_model02->setIs02Id($this->security->xss_clean($_POST['is02Id']));
            $this->incoming_stok_model02->setIsId($this->security->xss_clean($data_id));
            $this->incoming_stok_model02->setItemsId($this->security->xss_clean($items_id));
            $this->incoming_stok_model02->setItemsName($this->security->xss_clean($items_name));
            $this->incoming_stok_model02->setQty($this->security->xss_clean($qty_terima));
            $this->incoming_stok_model02->setPoQty($this->security->xss_clean($items_qty));
            $this->incoming_stok_model02->setPrice($this->security->xss_clean($price));
            $this->incoming_stok_model02->setDisc($this->security->xss_clean($disc));
            $this->incoming_stok_model02->setTotal($this->security->xss_clean($total));
            $this->incoming_stok_model02->setRemarks($this->security->xss_clean(''));
            $this->incoming_stok_model02->insert();

            // if($to_local=='warehouse'){

            //Start From Purchase Order
            $cek_stok       = $this->stok_model->cek_stock($items_id,$warehouse_project)->num_rows();
            // test($cek_stok,1);

            if($cek_stok>=1){

                $detail_stok    = $this->stok_model->cek_stock($items_id,$warehouse_project)->row();

                $old_stok1      = $detail_stok->current_stock;
                $current_stock1 = $old_stok1+$qty_terima;
                
                $id = $detail_stok->stock_id;
                // $this->stok_model->getObjectById($id);
                // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                $this->stok_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_model->setActivity($this->security->xss_clean($activity));
                $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
                // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
                // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
                $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
                $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
                $this->stok_model->update($id);
            }else{
                // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                $this->stok_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_model->setActivity($this->security->xss_clean($activity));
                $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                $this->stok_model->setItemsOut($this->security->xss_clean(0));
                $this->stok_model->setAdjIn($this->security->xss_clean(0));
                $this->stok_model->setAdjOut($this->security->xss_clean(0));
                $this->stok_model->setOldStock($this->security->xss_clean(0));
                $this->stok_model->setCurrentStock($this->security->xss_clean($qty_terima));
                $this->stok_model->insert();
            }

            $id_by_doc      = substr($this->input->post('is_date'),0,4).substr($this->input->post('is_date'),5,2).$this->stok_by_doc_model->getId(substr($this->input->post('is_date'),0,4),substr($this->input->post('is_date'),5,2))->row()->id;

            $row_stok_doc   = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->num_rows();

            if($row_stok_doc>=1){
                $laststok       = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->row();

                $oldstock       = $laststok->current_stock;
                $current_stock  = $laststok->current_stock + $qty_terima;
            }else{
                $oldstock       = 0;
                $current_stock  = $qty_terima;
            }

            $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
            $this->stok_by_doc_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_by_doc_model->setDocNo($this->security->xss_clean($is_no));
            $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($warehouse_project));
            $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($is_date));
            $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
            $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
            $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity));
            $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($qty_terima));
            $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean($qty_terima));
            // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
            $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
            $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
            $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($price));
            $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
            $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
            $this->stok_by_doc_model->insert();


            $row_stok_doc   = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->num_rows();
            if($row_stok_doc>=1){
                $laststok_doc       = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->row();

                $oldstock_doc       = $laststok_doc->current_stock;
                $current_stock_doc  = $laststok_doc->current_stock + $qty_terima;
            }else{
                $oldstock_doc       = 0;
                $current_stock_doc  = $qty_terima;
            }
            // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
            $this->stok_hist_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_hist_model->setDocNo($this->security->xss_clean($is_no));
            $this->stok_hist_model->setWarehouseId($this->security->xss_clean($warehouse_project));
            $this->stok_hist_model->setCompanyId($this->security->xss_clean($company_id));
            $this->stok_hist_model->setCompanyName($this->security->xss_clean($company_name));
            $this->stok_hist_model->setTrnDate($this->security->xss_clean($is_date));
            $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
            $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
            $this->stok_hist_model->setActivity($this->security->xss_clean($activity));
            $this->stok_hist_model->setQty($this->security->xss_clean($qty_terima));
            $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
            $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
            $this->stok_hist_model->insert();

            // End From Purchase Order
                
            // }

        }

        $status         = 'Closed';

        $this->incoming_stok_model->setIsId($this->security->xss_clean($data_id));
        $this->incoming_stok_model->setIsDate($this->security->xss_clean($is_date));
        $this->incoming_stok_model->setIsNo($this->security->xss_clean($is_no));
        // if($to_local=='warehouse'){
        $this->incoming_stok_model->setProjectId($this->security->xss_clean(''));
        $this->incoming_stok_model->setProjectName($this->security->xss_clean(''));
        $this->incoming_stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
        // }else if($to_local=='project'){
        // $project_name       = $this->project_model->getById($warehouse_project)->project_name;
        // $this->incoming_stok_model->setWarehouseId($this->security->xss_clean(''));
        // $this->incoming_stok_model->setProjectName($this->security->xss_clean($project_name));
        // $this->incoming_stok_model->setProjectId($this->security->xss_clean($warehouse_project));
        // }
        $this->incoming_stok_model->setIsKind($this->security->xss_clean($is_kind));
        $this->incoming_stok_model->setDocRef($this->security->xss_clean(''));
        $this->incoming_stok_model->setCompanyId($this->security->xss_clean($company_id));
        $this->incoming_stok_model->setCompanyName($this->security->xss_clean($company_name ));
        $this->incoming_stok_model->setIsStatus($this->security->xss_clean('Closed'));
        $this->incoming_stok_model->setSenderPic($this->security->xss_clean(''));
        $this->incoming_stok_model->setReceiverPic($this->security->xss_clean($is_requester));
        $this->incoming_stok_model->setRemarks($this->security->xss_clean($remarks));
        $this->incoming_stok_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->incoming_stok_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->incoming_stok_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->incoming_stok_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        
        $save       = $this->incoming_stok_model->insert();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            jsout(array('success' => false));
        }else{
            $this->db->trans_commit();
            jsout(array('success' => true, 'status' => $save,  'is_no' => $is_no));
        }

        

    }

    function reject_js(){
        $this->db->trans_begin();

        $delete = $this->incoming_stok_model->act_reject_js();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            jsout(array('success' => true, 'status' => $delete ));
            $this->db->trans_commit();
        }

    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_io = $this->session->userdata('new_io');

        $items = $new_io['items'];
        // test($items,1);
        foreach($items as $key=>$val){
            // test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                $item_qty   = str_replace(',','',$this->security->xss_clean($this->db->escape_str($new_io['items'][$key]['item_qty'])));
                // test($mbp_id.' '.$item_qty,1);
                unset($new_io['items'][$key]);
                $new_io['items'] = array_values($new_io['items']);
                break;
            }
        }
        // test($new_io,0);
        $this->session->set_userdata('new_io', $new_io);
        jsout(array('success'=>1)); 
    }

    function view_print($id){
        $data['header']         = $this->incoming_stok_model->print_header($id);
        $data['detail']         = $this->incoming_stok_model02->print_detail($id);

        $this->template->load('body_print', 'stok/incoming_stok/is_print', $data);
    }

    function update_harga_incoming(){
        $query      = $this->db->query("SELECT b.is_02_id,a.is_id,a.is_date,a.is_no,a.doc_ref,c.os_id,b.items_id,b.qty,b.price,b.total,
                                        b.discount,d.items_id,d.qty qty_out,d.price price_outgoing,d.total 
                                        FROM db_bumbu_transaction.trn_incoming_stock_01 a
                                        JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON b.is_id=a.is_id
                                        JOIN db_bumbu_transaction.trn_outgoing_stock_01 c ON a.doc_ref=c.os_no
                                        JOIN db_bumbu_transaction.trn_outgoing_stock_02 d ON d.os_id=c.os_id AND d.items_id=b.items_id
                                        WHERE a.doc_ref LIKE 'OS%' AND b.price<>d.price")->result();
        foreach ($query as $key => $value) {
            $total      = ($value->qty_out*$value->price_outgoing)-$value->discount;
            $this->db->query("UPDATE db_bumbu_transaction.trn_incoming_stock_02 SET price = '".$value->price_outgoing."', discount = '".$value->discount."', total = '".$total."' WHERE is_02_id = '".$value->is_02_id."'");
        }
        $this->session->set_flashdata('alert','Harga Berhasil diupdate ');
        redirect('stok/incoming_stok');
    }

    function copy_input($id){

        $this->session->unset_userdata('new_io');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');
        // $this->load->model('master/project_model');


        $new_io = $this->session->userdata('new_io');

        $data['header'] = $this->incoming_stok_model->print_header($id);
        $detail         = $this->incoming_stok_model02->print_detail($id);
        // test($detail,1);
            foreach($detail as $key=>$val){
                $new_io['items'][$key] = array(
                    'det_id'        => $val->is_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'item_qty'      => number_format($val->qty,2),
                    'item_info'     => $val->remarks,
                    'price'         => number_format($val->price,2),
                    'disc'          => number_format($val->discount,2)
                );
            }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));

        $this->session->set_userdata('new_io', $new_io);

        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        // $data['data_project']   = $this->project_model->get_project();
        $data['new_io']         = $new_io;
        // test($data,1);
        $this->template->load('body', 'stok/incoming_stok/is_copy_others', $data);
    }

}
// 1. Memunculkan Remarks pada Popup detail Incoming Stock
// 2. Penambahan Print IS
// 3. Update Harga Incoming stock
?>