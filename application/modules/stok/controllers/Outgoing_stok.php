<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outgoing_stok extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('stok/outgoing_stok_model');
        $this->load->model('stok/outgoing_stok_model02');
        $this->load->model('stok/incoming_stok_model');
        $this->load->model('stok/incoming_stok_model02');
        $this->load->model('master/warehouse_model');
        // $this->load->model('master/project_model');

        $this->load->model('stok/stok_model');
        $this->load->model('stok/stok_by_doc_model');
        $this->load->model('stok/stok_hist_model');
    }


    function index(){  
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Stock', 'active_submenu' => 'stok/outgoing_stok'));
        $this->isMenu();

        if($this->input->post('datepicker_from')!='' AND $this->input->post('datepicker_to')!='' ){
            $data['data_os']  = $this->outgoing_stok_model->get_os_date($this->input->post('datepicker_from'),$this->input->post('datepicker_to'));
        }else{
            $data['data_os']  = $this->outgoing_stok_model->get_os();
        }
        $this->template->load('body', 'stok/outgoing_stok/os_view',$data);
    }

    function form(){
        $this->session->unset_userdata('new_os');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');


        $new_os = $this->session->userdata('new_os');

        if(!$new_os){
            $new_os = array(
                'items' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        // $data['data_project']   = $this->project_model->get_project();
        $data['new_os']         = $new_os;
        // test($data,1);
        $this->template->load('body', 'stok/outgoing_stok/os_form', $data);

        // $this->load->model('master/items_model');
        // $this->load->model('master/warehouse_model');

        // $new_os = $this->session->userdata('new_os');

        // if(!$new_os){
        //     $new_os = array(
        //         'items' => array()
        //     );
        // }
        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        // $data['data_items']     = $this->items_model->get_items();
        // $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        // $data['new_os']        = $new_os;
        // $this->template->load('body', 'stok/outgoing_stok/os_form', $data);
    }

    function form_others(){
        $this->session->unset_userdata('new_os');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');


        $new_os = $this->session->userdata('new_os');

        if(!$new_os){
            $new_os = array(
                'items' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        // $data['data_project']   = $this->project_model->get_project();
        $data['new_os']         = $new_os;
        // test($data,1);
        $this->template->load('body', 'stok/outgoing_stok/os_form_others', $data);

    }

    function reset(){
        $this->session->unset_userdata('new_os');
        redirect('stok/outgoing_stok');
    }

    function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_os    = $this->session->userdata('new_os');
        $items      = $new_os['items'];

        $new_os['items'][] = array(
            // 'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'item_info'     => $this->input->post('item_info')
        );

        $exist = false;
        // test($new_os,0);
        $this->session->set_userdata('new_os', $new_os);         
    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_os = $this->session->userdata('new_os');

        $items = $new_os['items'];
        // test($items,1);
        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                // $mbp_id     = $new_os['items'][$key]['mbp_id'];
                $item_qty   = str_replace(',','',$this->security->xss_clean($this->db->escape_str($new_os['items'][$key]['item_qty'])));
                // test($mbp_id.' '.$item_qty,1);
                unset($new_os['items'][$key]);
                $new_os['items'] = array_values($new_os['items']);
                break;
            }
        }
        // test($new_os,0);
        $this->session->set_userdata('new_os', $new_os);
        jsout(array('success'=>1)); 
    }

    function form_act(){

        $this->db->trans_begin();

        $new_os         = $this->session->userdata('new_os');

        $os_id          = $this->outgoing_stok_model->getDataCount()->os_id;

        $periode        = substr($this->input->post('os_date'),0,4).substr($this->input->post('os_date'),5,2);
        $data_no        = $this->outgoing_stok_model->get_nomor_dok($periode);
        $nomor_dok      = $data_no->nomor_dok;
        // test($_POST['os_kind'],0);
        if($_POST['os_kind']=='Transfer'){
            $activity       = 'Out_tr';
            $jenis          = 'S';
        }elseif($_POST['os_kind']=='Others'){
            $activity       = 'Out_Oth';
            $jenis          = 'T';
        }elseif($_POST['os_kind']=='Adjustment'){
            $activity       = 'Out_Adj';
            $jenis          = 'J';
        }
        
        $os_no          = 'OS'.$jenis.$periode.$nomor_dok;
        $to_local       = '';

        // Start Incoming Header
        // $to_local           = $this->input->post('to_local');
        $sender_pic         = '';
        $receiver_pic       = $this->input->post('os_requester');
        $remarks            = $this->input->post('remarks');
        $warehouse_project  = $this->input->post('warehouse_project');
        $company_id         = $this->current_user['company_id'];
        $company_name       = $this->current_user['company_name'];

        $data_id            = $this->incoming_stok_model->get_id()->is_id;
        $periode            = substr($this->input->post('os_date'),0,4).substr($this->input->post('os_date'),5,2);
        $kode               = 'IS';
        $data_no            = $this->incoming_stok_model->get_nomor_dok($periode,$kode)->nomor_dok;

        $doc_ref            = $os_no;
        $is_date            = $this->input->post('os_date');
        $is_no              = $kode.'S'.$periode.$data_no;

        $activity_in        = "In_Trans";
        // Finish Incoming Header

        $new_os         = $this->session->userdata('new_os');
        foreach ($new_os['items'] as $key => $value) {

            $data_stok      = $this->stok_by_doc_model->dataStock($value['item_id'],$_POST['warehouse'])->result();
            $items_id       = $value['item_id'];
            $item_qty       = 0;
            $sisa           = 0;
            $stok_barang1   = $value['item_qty'];
            $is_stop        = 0;
            $tbarang        = 0;
            // test($data_stok,1);
            foreach ($data_stok as $key => $value2) {

                if($value2->items_remaining>=$stok_barang1){
                    $tbarang        = $tbarang+($stok_barang1*$value2->items_price);
                    $sisa           = $value2->items_remaining - $stok_barang1;
                    $stok_barang1   = $value2->items_remaining - $stok_barang1;

                    $is_stop        = 1;
                }else{
                    $tbarang        = $tbarang+($value2->items_remaining * $value2->items_price);
                    $sisa           = 0;
                    $stok_barang1   = $stok_barang1 - $value2->items_remaining;

                    $is_stop        = 0;
                }

                $this->stok_by_doc_model->update_stok($value2->stock_by_doc_id,$sisa);

                // test($sisa.' - '.$stok_barang1.' - '.$tbarang.' <--- ',0);

                if($is_stop==1){
                    break; 
                }                
                
            }

            $price_out          = $tbarang / $value['item_qty'];
            $total              = $price_out*$value['item_qty'];
            // test($tbarang.' / '.$value['item_qty'].' - '.$price_out,1);

            $detail_stok    = $this->stok_model->cek_stock($items_id,$_POST['warehouse'])->row();

            $old_stok1      = $detail_stok->current_stock;
            $current_stock1 = $old_stok1-$value['item_qty'];
            
            $id = $detail_stok->stock_id;
            // $this->stok_model->getObjectById($id);
            // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
            $this->stok_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_model->setDocNo($this->security->xss_clean($os_no));
            $this->stok_model->setWarehouseId($this->security->xss_clean($_POST['warehouse']));
            $this->stok_model->setCompanyId($this->security->xss_clean($_POST['company_id']));
            $this->stok_model->setCompanyName($this->security->xss_clean($_POST['company_name']));
            $this->stok_model->setTrnDate($this->security->xss_clean($_POST['os_date']));
            $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
            $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
            $this->stok_model->setActivity($this->security->xss_clean($activity));
            $this->stok_model->setItemsOut($this->security->xss_clean($value['item_qty']));
            // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
            // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
            // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
            $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
            $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
            $this->stok_model->update($id);

            $row_stok_doc   = $this->stok_hist_model->lastStokHist($items_id,$_POST['warehouse'])->num_rows();
            if($row_stok_doc>=1){
                $laststok_doc       = $this->stok_hist_model->lastStokHist($items_id,$_POST['warehouse'])->row();

                $oldstock_doc       = $laststok_doc->current_stock;
                $current_stock_doc  = $laststok_doc->current_stock - $value['item_qty'];
            }else{
                $oldstock_doc       = 0;
                $current_stock_doc  = $value['item_qty'];
            }
            // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
            $this->stok_hist_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_hist_model->setDocNo($this->security->xss_clean($os_no));
            $this->stok_hist_model->setWarehouseId($this->security->xss_clean($_POST['warehouse']));
            $this->stok_hist_model->setCompanyId($this->security->xss_clean($_POST['company_id']));
            $this->stok_hist_model->setCompanyName($this->security->xss_clean($_POST['company_name']));
            $this->stok_hist_model->setTrnDate($this->security->xss_clean($_POST['os_date']));
            $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
            $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
            $this->stok_hist_model->setActivity($this->security->xss_clean($activity));
            $this->stok_hist_model->setQty($this->security->xss_clean($value['item_qty']));
            $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
            $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
            $this->stok_hist_model->insert();

            $id_by_doc      = substr($this->input->post('os_date'),0,4).substr($this->input->post('os_date'),5,2).$this->stok_by_doc_model->getId(substr($this->input->post('os_date'),0,4),substr($this->input->post('os_date'),5,2))->row()->id;
            $row_stok_doc   = $this->stok_by_doc_model->lastStok($items_id,$_POST['warehouse'])->num_rows();

            if($row_stok_doc>=1){
                $laststok       = $this->stok_by_doc_model->lastStok($items_id,$_POST['warehouse'])->row();

                $oldstock       = $laststok->current_stock;
                $current_stock  = $laststok->current_stock - $value['item_qty'];
            }else{
                $oldstock       = 0;
                $current_stock  = $value['item_qty'];
            }

            $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
            $this->stok_by_doc_model->setItemsId($this->security->xss_clean($items_id));
            $this->stok_by_doc_model->setDocNo($this->security->xss_clean($os_no));
            $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($_POST['warehouse']));
            $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($_POST['os_date']));
            $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
            $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
            $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity));
            $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($value['item_qty']));
            // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
            $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
            $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
            $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($price_out));
            $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
            $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
            $this->stok_by_doc_model->insert();

            // $this->outgoing_stok_model02->setOs02Id($this->security->xss_clean($_POST['os02Id']));
            $this->outgoing_stok_model02->setOsId($this->security->xss_clean($os_id));
            $this->outgoing_stok_model02->setItemsId($this->security->xss_clean($value['item_id']));
            $this->outgoing_stok_model02->setItemsName($this->security->xss_clean($value['item_name']));
            $this->outgoing_stok_model02->setQty($this->security->xss_clean($value['item_qty']));
            $this->outgoing_stok_model02->setPrice($this->security->xss_clean($price_out));
            $this->outgoing_stok_model02->setTotal($this->security->xss_clean($total));
            $this->outgoing_stok_model02->setRemarks($this->security->xss_clean(''));
            $this->outgoing_stok_model02->insert();

            if($_POST['os_kind']=='Transfer'){

                $items_id           = $value['item_id'];
                $items_qty          = $value['item_qty'];
                $items_name         = $value['item_name'];
                $qty_terima         = $value['item_qty'];

                $price                  = $price_out;
                $total                  = $qty_terima*$price;

                $this->incoming_stok_model02->setIsId($this->security->xss_clean($data_id));
                $this->incoming_stok_model02->setItemsId($this->security->xss_clean($items_id));
                $this->incoming_stok_model02->setItemsName($this->security->xss_clean($items_name));
                $this->incoming_stok_model02->setQty($this->security->xss_clean($qty_terima));
                $this->incoming_stok_model02->setPoQty($this->security->xss_clean($items_qty));
                $this->incoming_stok_model02->setPrice($this->security->xss_clean($price));
                $this->incoming_stok_model02->setTotal($this->security->xss_clean($total));
                $this->incoming_stok_model02->setRemarks($this->security->xss_clean(''));
                $this->incoming_stok_model02->insert();

                // if($to_local=='warehouse'){
                    //Start From Purchase Order
                    $cek_stok       = $this->stok_model->cek_stock($items_id,$warehouse_project)->num_rows();                    

                    if($cek_stok>=1){

                        $detail_stok    = $this->stok_model->cek_stock($items_id,$warehouse_project)->row();

                        $old_stok1      = $detail_stok->current_stock;
                        $current_stock1 = $old_stok1+$qty_terima;
                        
                        $id = $detail_stok->stock_id;
                        // $this->stok_model->getObjectById($id);
                        // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                        $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                        $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                        $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                        $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                        $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                        $this->stok_model->setTrnDate($this->security->xss_clean($this->input->post('os_date')));
                        $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
                        $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
                        $this->stok_model->setActivity($this->security->xss_clean($activity_in));
                        $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                        // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
                        // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
                        // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
                        $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
                        $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
                        $this->stok_model->update($id);
                    }else{
                        // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                        $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                        $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                        $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                        $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                        $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                        $this->stok_model->setTrnDate($this->security->xss_clean($this->input->post('os_date')));
                        $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
                        $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
                        $this->stok_model->setActivity($this->security->xss_clean($activity_in));
                        $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                        $this->stok_model->setItemsOut($this->security->xss_clean(0));
                        $this->stok_model->setAdjIn($this->security->xss_clean(0));
                        $this->stok_model->setAdjOut($this->security->xss_clean(0));
                        $this->stok_model->setOldStock($this->security->xss_clean(0));
                        $this->stok_model->setCurrentStock($this->security->xss_clean($qty_terima));
                        $this->stok_model->insert();
                    }

                    $id_by_doc      = substr($this->input->post('os_date'),0,4).substr($this->input->post('os_date'),5,2).$this->stok_by_doc_model->getId(substr($this->input->post('os_date'),0,4),substr($this->input->post('os_date'),5,2))->row()->id;

                    $row_stok_doc   = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->num_rows();

                    if($row_stok_doc>=1){
                        $laststok       = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->row();

                        $oldstock       = $laststok->current_stock;
                        $current_stock  = $laststok->current_stock + $qty_terima;
                    }else{
                        $oldstock       = 0;
                        $current_stock  = $qty_terima;
                    }

                    // $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
                    $this->stok_by_doc_model->setItemsId($this->security->xss_clean($items_id));
                    $this->stok_by_doc_model->setDocNo($this->security->xss_clean($is_no));
                    $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                    $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($this->input->post('os_date')));
                    $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
                    $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
                    $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity_in));
                    $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($qty_terima));
                    $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean($qty_terima));
                    $this->stok_by_doc_model->setItemsOut($this->security->xss_clean('0'));
                    $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
                    $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
                    $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($price));
                    $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
                    $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
                    $this->stok_by_doc_model->insert();


                    $row_stok_doc   = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->num_rows();
                    if($row_stok_doc>=1){
                        $laststok_doc       = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->row();

                        $oldstock_doc       = $laststok_doc->current_stock;
                        $current_stock_doc  = $laststok_doc->current_stock + $qty_terima;
                    }else{
                        $oldstock_doc       = 0;
                        $current_stock_doc  = $qty_terima;
                    }
                    // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
                    $this->stok_hist_model->setItemsId($this->security->xss_clean($items_id));
                    $this->stok_hist_model->setDocNo($this->security->xss_clean($is_no));
                    $this->stok_hist_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                    $this->stok_hist_model->setCompanyId($this->security->xss_clean($company_id));
                    $this->stok_hist_model->setCompanyName($this->security->xss_clean($company_name));
                    $this->stok_hist_model->setTrnDate($this->security->xss_clean($this->input->post('os_date')));
                    $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr($this->input->post('os_date'),0,4)));
                    $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('os_date'),5,2)));
                    $this->stok_hist_model->setActivity($this->security->xss_clean($activity_in));
                    $this->stok_hist_model->setQty($this->security->xss_clean($qty_terima));
                    $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
                    $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
                    $this->stok_hist_model->insert();

                    // End From Purchase Order
                    
                // }

            }
        }

        $this->outgoing_stok_model->setOsId($this->security->xss_clean($os_id));
        $this->outgoing_stok_model->setOsDate($this->security->xss_clean($_POST['os_date']));
        $this->outgoing_stok_model->setOsNo($this->security->xss_clean($os_no));
        $this->outgoing_stok_model->setCompanyId($this->security->xss_clean($_POST['company_id']));
        $this->outgoing_stok_model->setCompanyName($this->security->xss_clean($_POST['company_name']));
        $this->outgoing_stok_model->setWarehouseId($this->security->xss_clean($_POST['warehouse']));
        // if($to_local=='warehouse'){
        $this->outgoing_stok_model->setProjectId($this->security->xss_clean(''));
        $this->outgoing_stok_model->setProjectName($this->security->xss_clean(''));
        $this->outgoing_stok_model->setDestWhId($this->security->xss_clean($warehouse_project));
        // }else if($to_local=='project'){
        // $project_name       = $this->project_model->getById($warehouse_project)->project_name;
        // $this->outgoing_stok_model->setDestWhId($this->security->xss_clean(''));
        // $this->outgoing_stok_model->setProjectName($this->security->xss_clean($project_name));
        // $this->outgoing_stok_model->setProjectId($this->security->xss_clean($warehouse_project)); 
        // }
        // $this->outgoing_stok_model->setDept($this->security->xss_clean($_POST['dept']));
        $this->outgoing_stok_model->setOsKind($this->security->xss_clean($_POST['os_kind']));
        $this->outgoing_stok_model->setOsStatus($this->security->xss_clean('Closed'));
        $this->outgoing_stok_model->setOsRequester($this->security->xss_clean($_POST['os_requester']));
        $this->outgoing_stok_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->outgoing_stok_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->outgoing_stok_model->setInputDate($this->security->xss_clean(dbnow()));
        // $this->outgoing_stok_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->outgoing_stok_model->setEditDate($this->security->xss_clean($_POST['editDate']));
        $save   = $this->outgoing_stok_model->insert();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            jsout(array('success' => true, 'status' => 'ERROR INSERT'));
        }else{
            $this->db->trans_commit();
            jsout(array('success' => true, 'status' => $save,  'os_no' => $os_no));
        }

    }

    function view_popup($id){

        $header         = $this->outgoing_stok_model->detail_os($id);
        $detail         = $this->outgoing_stok_model02->getDetailIs($id)->result_array();
        $myData         = array();
        $subtotal       = 0;
        $total          = 0;
        foreach ($detail as $key => $row) {
            $subtotal       = $row['price']*$row['qty'];
            $total          = $subtotal+$total;
            $myData[] = array(
                $row['items_name'],     
                number_format($row['qty'],2),
                number_format($row['price'],2),
                number_format($subtotal,2)
            );     
        }   
            $myData[] = array(
                '',
                '',
                'Total',
                number_format($total,2)
            );      

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function reject_js(){
        $this->db->trans_begin();

        // $delete = $this->outgoing_stok_model->act_reject_js();

        $os_id          = $this->input->post('os_id');
        $os_no          = $this->input->post('os_no');
        $company_id     = $this->current_user['company_id'];
        $company_name   = $this->current_user['company_name'];

        $this->outgoing_stok_model->update_status_os($os_id);

        $data_by_doc    = $this->outgoing_stok_model->data_by_doc($os_no);
        foreach ($data_by_doc as $key => $value1) {

            $items_id           = $value1->items_id;
            $doc_no             = $value1->doc_no;
            $warehouse_id       = $value1->warehouse_id;
            $items_out          = $value1->items_out;
            $activity           = 'Rj_OutTrans';
            $new_doc            = substr($doc_no,0,2).'R'.substr($doc_no,3,10);
            $trn_date_new       = dbnow();
            $trn_year_new       = substr(dbnow(),0,4);
            $trn_month_new      = substr(dbnow(),5,2);

            $trn_date           = $value1->trn_date;
            $trn_year           = $value1->trn_year;
            $trn_month          = $value1->trn_month;
            $items_price        = $value1->items_price;

            $old_stock          = $this->outgoing_stok_model->old_stock($items_id,$warehouse_id)->current_stock;
            $current_stock      = $old_stock+$items_out;

            $this->stok_model->update_stock($trn_date_new,$trn_year_new,$trn_month_new,$activity,$items_out,$old_stock,$current_stock,$items_id,$warehouse_id);

            $q_lastbydoc        = $this->stok_by_doc_model->q_lastbydoc($items_id,$warehouse_id);
            $stock_by_doc_id    = $q_lastbydoc->stock_by_doc_id;
            $last_remaining     = $q_lastbydoc->items_remaining+$items_out;

            $this->stok_by_doc_model->ustok_remaining($last_remaining,$stock_by_doc_id);
            $this->stok_hist_model->insert_hist($items_id,$new_doc,$warehouse_id,$company_id,$company_name,$trn_date,$trn_year,$trn_month,$activity,$items_out,$old_stock,$current_stock);
            $this->stok_by_doc_model->insert_by_doc($items_id,$new_doc,$warehouse_id,$trn_date,$trn_year,$trn_month,$activity,$items_out,$old_stock,$current_stock,$items_price);
        }

        $countRef       = $this->incoming_stok_model->doc_ref($os_no)->num_rows();
        if($countRef>=1){
            $header_is      = $this->incoming_stok_model->doc_ref($os_no)->row();
            $is_id          = $header_is->is_id;
            $is_no          = $header_is->is_no;
            $warehouse_id   = $header_is->warehouse_id;
            $project_id     = $header_is->project_id;
            $company_id     = $this->current_user['company_id'];
            $company_name   = $this->current_user['company_name'];

            $update_reject  = $this->incoming_stok_model->update_reject($is_id);
            
            $data_stock_hist= $this->stok_by_doc_model->data_stock_hist($is_no);

            foreach ($data_stock_hist as $key => $value){
                $items_id       = $value->items_id;
                $doc_no         = $value->doc_no;
                $new_doc        = substr($doc_no,0,2).'R'.substr($doc_no,3,10);
                $trn_date_new   = dbnow();
                $trn_year_new   = substr(dbnow(),0,4);
                $trn_month_new  = substr(dbnow(),5,2);

                $trn_date       = $value->trn_date;
                $trn_year       = $value->trn_year;
                $trn_month      = $value->trn_month;

                $activity       = 'Rj_InTrans';
                $items_in       = $value->items_in;
                $warehouse_id   = $value->warehouse_id;
                $item_price     = $value->items_price;   
                $items_remaining= $value->items_remaining; 
                $last_remaining = $items_remaining-$items_in;

                if($last_remaining<0){
                    $last_remaining = 0;
                }else{
                    $last_remaining = $last_remaining;
                }
                $stock_by_doc_id= $value->stock_by_doc_id;

                $old_stock      = $this->outgoing_stok_model->old_stock($items_id,$warehouse_id)->current_stock;
                $current_stock  = $old_stock-$items_in;

                $update_stock   = $this->stok_model->update_stock_is($trn_date_new,$trn_year_new,$trn_month_new,$activity,$items_in,$old_stock,$current_stock,$items_id,$warehouse_id);

                $ustok_remaining= $this->stok_by_doc_model->ustok_remaining_in($last_remaining,$stock_by_doc_id);

                $items_remaining= $this->stok_by_doc_model->items_remaining($doc_no,$items_id)->items_remaining;

                $insert_hist    = $this->stok_hist_model->insert_hist($items_id,$new_doc,$warehouse_id,$company_id,$company_name,$trn_date,$trn_year,$trn_month,$activity,$items_in,$old_stock,$current_stock);   

                $insert_by_doc  = $this->stok_by_doc_model->insert_by_doc_in($items_id,$new_doc,$warehouse_id,$trn_date,$trn_year,$trn_month,$activity,$items_in,$old_stock,$current_stock,$item_price);

            }

        }


        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            jsout(array('success' => false));
        }else{
            jsout(array('success' => true));
            $this->db->trans_commit();
        }

    }


    // function add_item(){
    //     // test($_POST['ppn'],1);
    //     if(!isset($_POST['item_id'])) return;
    //     $new_os = $this->session->userdata('new_os');
    //     $items = $new_os['items'];

    //     $exist = false;
    //     if($items!=''){
    //     foreach($items as $key=>$val){
    //             if($val['item_id'] == $this->input->post('item_id')){
    //                 $new_os['items'][$key] = array(
    //                     'item_id'       => $this->input->post('item_id'),
    //                     'item_name'     => $this->input->post('item_name'),
    //                     'item_qty'      => $this->input->post('item_qty'),
    //                     'item_info'     => $this->input->post('item_info')
    //                 );
    //                 $exist = true;
    //                 break;
    //             }
    //         }
    //     }

    //     if(!$exist){
    //         $new_os['items'][] = array(
    //             'item_id'       => $this->input->post('item_id'),
    //             'item_name'     => $this->input->post('item_name'),
    //             'item_qty'      => $this->input->post('item_qty'),
    //             'item_info'     => $this->input->post('item_info')
    //         );
    //     }
        
    //     $this->session->set_userdata('new_os', $new_os);        
    // }

    // function remove_item(){
    //     //test($_GET['index_id'],0);
    //     if(!isset($_GET['index_id'])) return;
    //     $index_id = $this->input->get('index_id');
    //     $new_os = $this->session->userdata('new_os');

    //     $items = $new_os['items'];

    //     foreach($items as $key=>$val){
    //         //test($val['item_id'],0);
    //         if($val['item_id'] == $index_id){
    //             unset($new_os['items'][$key]);
    //             $new_os['items'] = array_values($new_os['items']);
    //             break;
    //         }
    //     }

    //     $this->session->set_userdata('new_os', $new_os);
    //     jsout(array('success'=>1)); 
    // }

    // function form_act(){
    //     $save   = $this->outgoing_stok_model->act_form();

    //     $this->session->unset_userdata('new_req');
    //     jsout(array('success' => true, 'status' => $save ));
    // }

    function print_os($id){
        $data['header']     = $this->outgoing_stok_model->detail_os($id);
        $data['detail']     = $this->outgoing_stok_model->detail_items_os($id);
        $this->template->load('body', 'stok/outgoing_stok/os_print',$data);
    }

    function popup_os($id){
        $data['id']     = $id;
        $data['header']     = $this->outgoing_stok_model->detail_os($id);
        $data['detail']     = $this->outgoing_stok_model->detail_items_os($id);

        $this->load->view('stok/outgoing_stok/os_detail',$data);
        
    }

    function view_print($id){
        $data['header']         = $this->outgoing_stok_model->print_header($id);
        $data['detail']         = $this->outgoing_stok_model02->print_detail($id);

        $this->template->load('body_print', 'stok/outgoing_stok/os_print', $data);
    }

    function update_harga_outgoing(){

        $tmp        = $this->db->query("SELECT * FROM db_bumbu_transaction.trn_stock_by_doc_tmp ")->num_rows();

        if($tmp>=1){

            $query      = $this->db->query("SELECT a.os_id,a.os_no,a.os_kind,a.os_status,b.os_02_id,b.items_id,c.items_name,b.qty,b.price price_os,c.price 
                                            price_items,d.items_price tmp,b.total
                                            FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                                            LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                                            LEFT JOIN db_bumbu_master.mst_items c ON b.items_id=c.items_id
                                            LEFT JOIN db_bumbu_transaction.trn_stock_by_doc_tmp d ON d.items_id=b.items_id
                                            WHERE b.price<>c.price AND a.os_no=d.doc_no")->result();

            foreach ($query as $key => $value) {
                $total      = ($value->qty*$value->tmp);
                $this->db->query("UPDATE db_bumbu_transaction.trn_outgoing_stock_02 SET price = '".$value->tmp."',total = '".$total."' 
                                    WHERE os_02_id = '".$value->os_02_id."'");
                // $this->db->query("UPDATE db_bumbu_transaction.trn_incoming_stock_02 SET price = '".$value->price_outgoing."', 
                // discount = '".$value->discount."', total = '".$total."' WHERE is_02_id = '".$value->is_02_id."'");
            }

            $this->session->set_flashdata('alert','Harga Berhasil diupdate ');
            redirect('stok/outgoing_stok');

        }else{
            $this->session->set_flashdata('alert','data Recallculate Belum ada, <a class="btn btn-warning btn-sm" href="'.base_url('stok/trn_stok/view_recallculate').'">Klik</a> ');
            redirect('stok/outgoing_stok');
        }

    }

    function copy_input($id){
        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');
        
        $data['header']     = $this->outgoing_stok_model->detail_os($id);
        $detail             = $this->outgoing_stok_model->detail_items_os($id);
        // test($detail,1);
        $new_os = $this->session->userdata('new_os');

            foreach($detail as $key=>$val){
                $new_os['items'][$key] = array(
                    'det_id'        => $val->os_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'item_qty'      => number_format($val->qty,2),
                    'item_info'     => $val->remarks,
                    'price'         => number_format($val->price,2)
                    // 'disc'          => number_format($val->discount,2)
                );
            }

        $this->session->set_userdata('new_os', $new_os);

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_items']     = $this->items_model->get_items();
        $data['data_project']   = $this->project_model->get_project();
        $data['new_os']         = $new_os;
        // test($data,1);
        $this->template->load('body', 'stok/outgoing_stok/os_copy', $data);

    }











// 1. Penambahan Print OS
// 2. Perubahan Input  otomatis untuk incoming stock

}
?>