<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saldo_awal extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Stock', 'active_submenu' => 'stok/saldo_awal'));
        $this->isMenu();

        // $this->load->model('master/project_model');
        $this->load->model('master/warehouse_model');
        $this->load->model('stok/incoming_stok_model');
        $this->load->model('stok/incoming_stok_model02');
        $this->load->model('stok/outgoing_stok_model');
        $this->load->model('stok/outgoing_stok_model02');
        $this->load->model('transaction/purchase_order_model');
        $this->load->model('transaction/purchase_order_model02');
        $this->load->model('stok/stok_model');
        $this->load->model('stok/stok_by_doc_model');
        $this->load->model('stok/stok_hist_model');
    }


    function index(){  
        // $data['data_po']  = $this->incoming_stok_model->get_po_out();
        $data['data_is']  =  $this->dbpurch->query("SELECT a.doc_no,a.warehouse_id,a.trn_date,a.activity,b.warehouse_name,c.project_name FROM trn_stock_by_doc a 
            left join db_bumbu_master.mst_warehouse b on a.warehouse_id=b.warehouse_id 
            left join db_bumbu_master.mst_project c on c.project_id=a.project_id
            WHERE a.activity='Saldo Awal' GROUP BY a.doc_no order by a.doc_no DESC")->result();
        // $data['data_os']  = $this->incoming_stok_model->get_os_not_Stock_use();
        $this->template->load('body', 'stok/saldo_awal/sa_view',$data);
    }

    function view_popup($id){

        // $header         = $this->incoming_stok_model->getById($id)->row();
        $detail         = $this->dbpurch->query("SELECT a.*,b.items_name FROM trn_stock_by_doc a LEFT JOIN db_bumbu_master.mst_items b ON a.items_id=b.items_id WHERE a.doc_no='".$id."'")->result_array();
        $myData         = array();
        $subtotal       = 0;
        $total          = 0;
        foreach ($detail as $key => $row) {
            // $subtotal       = $row['price']*$row['qty'];
            // $total          = $subtotal+$total;
            $myData[] = array(
                $row['items_name'],     
                number_format($row['items_in'],2),
                // money($row['price']),
                // money($subtotal)
            );     
        }   
            // $myData[] = array(
            //     '',
            //     '',
            //     'Total',
            //     money($total)
            // );      

        return jsout(array(/*'header' => $header, */'detail'=> json_encode($myData)));

    }

    function form_others(){
        $this->session->unset_userdata('new_sa');

        $this->load->model('master/items_model');
        $this->load->model('master/supplier_model');
        $this->load->model('master/project_model');


        $new_sa = $this->session->userdata('new_sa');

        if(!$new_sa){
            $new_sa = array(
                'items' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        // $data['data_items']     = $this->items_model->get_items_sa();
        $data['data_project']   = $this->project_model->get_project();
        $data['new_sa']         = $new_sa;
        // test($data,1);
        $this->template->load('body', 'stok/saldo_awal/sa_form_others', $data);
    }

    function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_sa    = $this->session->userdata('new_sa');
        $items      = $new_sa['items'];

        $new_sa['items'][] = array(
            // 'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'item_info'     => $this->input->post('item_info'),
            'price'         => $this->input->post('price')
        );

        $exist = false;
        // test($new_sa,0);
        $this->session->set_userdata('new_sa', $new_sa);         
    }

    function form_others_act(){

        $this->db->trans_begin();
        
        $new_sa             = $this->session->userdata('new_sa');

        $to_local           = $this->input->post('to_local');
        $remarks            = $this->input->post('remarks');
        $warehouse_project  = $this->input->post('warehouse_project');
        $company_id         = $this->current_user['company_id'];
        $company_name       = $this->current_user['company_name'];

        $data_id            = $this->incoming_stok_model->get_id()->is_id;
        $periode            = substr($this->input->post('is_date'),0,4);
        $kode               = 'SA';

        $qdok               = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(doc_no,10,3))+1,3,'0'),'001') no_dok,doc_no FROM trn_stock_by_doc WHERE activity='Saldo Awal' AND SUBSTRING(doc_no,4,4)='".$periode."'")->row()->no_dok;

        $is_date            = $this->input->post('is_date');
        $is_requester       = $this->input->post('is_requester');
        $is_no              = $kode.'T'.$periode.$qdok;

        // $is_no              = "Saldo Awal";
        $activity           = "Saldo Awal"; 


        // test($to_local.' '.$remarks.' '.$warehouse_project.' '.$is_date.' '.$is_requester.' '.$is_no,1);
        // test($new_sa,1);


        // test($os_no.' '.$id,1);

        $new_sa         = $this->session->userdata('new_sa');
        foreach ($new_sa['items'] as $key => $value) {

            $items_id               = $value['item_id'];
            $items_qty              = '0';
            $items_name             = $value['item_name'];
            $price                  = $value['price'];
            $qty_terima             = $value['item_qty'];
            
            $total                  = $qty_terima*$price;

            // $this->incoming_stok_model02->setIs02Id($this->security->xss_clean($_POST['is02Id']));
            // $this->incoming_stok_model02->setIsId($this->security->xss_clean($data_id));
            // $this->incoming_stok_model02->setItemsId($this->security->xss_clean($items_id));
            // $this->incoming_stok_model02->setItemsName($this->security->xss_clean($items_name));
            // $this->incoming_stok_model02->setQty($this->security->xss_clean($qty_terima));
            // $this->incoming_stok_model02->setPoQty($this->security->xss_clean($items_qty));
            // $this->incoming_stok_model02->setPrice($this->security->xss_clean($price));
            // $this->incoming_stok_model02->setTotal($this->security->xss_clean($total));
            // $this->incoming_stok_model02->setRemarks($this->security->xss_clean(''));
            // $this->incoming_stok_model02->insert();

            if($to_local=='warehouse'){

                //Start From Purchase Order
                $cek_stok       = $this->stok_model->cek_stock($items_id,$warehouse_project)->num_rows();
                // test($cek_stok,1);

                if($cek_stok>=1){

                    $detail_stok    = $this->stok_model->cek_stock($items_id,$warehouse_project)->row();

                    $old_stok1      = $detail_stok->current_stock;
                    $current_stock1 = $old_stok1+$qty_terima;
                    
                    $id = $detail_stok->stock_id;
                    // $this->stok_model->getObjectById($id);
                    // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                    $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                    $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                    $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                    $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                    $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                    $this->stok_model->setTrnDate($this->security->xss_clean($is_date));
                    $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                    $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                    $this->stok_model->setActivity($this->security->xss_clean($activity));
                    $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                    // $this->stok_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
                    // $this->stok_model->setAdjIn($this->security->xss_clean($_POST['adjIn']));
                    // $this->stok_model->setAdjOut($this->security->xss_clean($_POST['adjOut']));
                    $this->stok_model->setOldStock($this->security->xss_clean($old_stok1));
                    $this->stok_model->setCurrentStock($this->security->xss_clean($current_stock1));
                    $this->stok_model->update($id);
                }else{
                    // $this->stok_model->setStockId($this->security->xss_clean($_POST['stockId']));
                    $this->stok_model->setItemsId($this->security->xss_clean($items_id));
                    $this->stok_model->setDocNo($this->security->xss_clean($is_no));
                    $this->stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                    $this->stok_model->setCompanyId($this->security->xss_clean($company_id));
                    $this->stok_model->setCompanyName($this->security->xss_clean($company_name));
                    $this->stok_model->setTrnDate($this->security->xss_clean($is_date));
                    $this->stok_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                    $this->stok_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                    $this->stok_model->setActivity($this->security->xss_clean($activity));
                    $this->stok_model->setItemsIn($this->security->xss_clean($qty_terima));
                    $this->stok_model->setItemsOut($this->security->xss_clean(0));
                    $this->stok_model->setAdjIn($this->security->xss_clean(0));
                    $this->stok_model->setAdjOut($this->security->xss_clean(0));
                    $this->stok_model->setOldStock($this->security->xss_clean(0));
                    $this->stok_model->setCurrentStock($this->security->xss_clean($qty_terima));
                    $this->stok_model->insert();
                }

                $id_by_doc      = substr($this->input->post('is_date'),0,4).substr($this->input->post('is_date'),5,2).$this->stok_by_doc_model->getId(substr($this->input->post('is_date'),0,4),substr($this->input->post('is_date'),5,2))->row()->id;

                $row_stok_doc   = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->num_rows();

                if($row_stok_doc>=1){
                    $laststok       = $this->stok_by_doc_model->lastStok($items_id,$warehouse_project)->row();

                    $oldstock       = $laststok->current_stock;
                    $current_stock  = $laststok->current_stock + $qty_terima;
                }else{
                    $oldstock       = 0;
                    $current_stock  = $qty_terima;
                }

                $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
                $this->stok_by_doc_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_by_doc_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_by_doc_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity));
                $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($qty_terima));
                // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
                $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
                $this->stok_by_doc_model->setItemsRemaining($this->security->xss_clean($qty_terima));
                $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
                $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($price));
                $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
                $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
                $this->stok_by_doc_model->insert();


                $row_stok_doc   = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->num_rows();
                if($row_stok_doc>=1){
                    $laststok_doc       = $this->stok_hist_model->lastStokHist($items_id,$warehouse_project)->row();

                    $oldstock_doc       = $laststok_doc->current_stock;
                    $current_stock_doc  = $laststok_doc->current_stock + $qty_terima;
                }else{
                    $oldstock_doc       = 0;
                    $current_stock_doc  = $qty_terima;
                }
                // $this->stok_hist_model->setStockHistId($this->security->xss_clean($_POST['stockHistId']));
                $this->stok_hist_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_hist_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_hist_model->setWarehouseId($this->security->xss_clean($warehouse_project));
                $this->stok_hist_model->setCompanyId($this->security->xss_clean($company_id));
                $this->stok_hist_model->setCompanyName($this->security->xss_clean($company_name));
                $this->stok_hist_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_hist_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_hist_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_hist_model->setActivity($this->security->xss_clean($activity));
                $this->stok_hist_model->setQty($this->security->xss_clean($qty_terima));
                $this->stok_hist_model->setOldStock($this->security->xss_clean($oldstock_doc));
                $this->stok_hist_model->setCurrentStock($this->security->xss_clean($current_stock_doc));
                $this->stok_hist_model->insert();

                // End From Purchase Order
                
            }else{

                $id_by_doc      = substr($this->input->post('is_date'),0,4).substr($this->input->post('is_date'),5,2).$this->stok_by_doc_model->getId(substr($this->input->post('is_date'),0,4),substr($this->input->post('is_date'),5,2))->row()->id;

                $row_stok_doc   = $this->stok_by_doc_model->lastProStok($items_id,$warehouse_project)->num_rows();

                if($row_stok_doc>=1){
                    $laststok       = $this->stok_by_doc_model->lastProStok($items_id,$warehouse_project)->row();

                    $oldstock       = $laststok->current_stock;
                    $current_stock  = $laststok->current_stock + $qty_terima;
                }else{
                    $oldstock       = 0;
                    $current_stock  = $qty_terima;
                }

                $this->stok_by_doc_model->setStockByDocId($this->security->xss_clean($id_by_doc));
                $this->stok_by_doc_model->setItemsId($this->security->xss_clean($items_id));
                $this->stok_by_doc_model->setDocNo($this->security->xss_clean($is_no));
                $this->stok_by_doc_model->setProjectId($this->security->xss_clean($warehouse_project));
                $this->stok_by_doc_model->setTrnDate($this->security->xss_clean($is_date));
                $this->stok_by_doc_model->setTrnYear($this->security->xss_clean(substr($this->input->post('is_date'),0,4)));
                $this->stok_by_doc_model->setTrnMonth($this->security->xss_clean(substr($this->input->post('is_date'),5,2)));
                $this->stok_by_doc_model->setActivity($this->security->xss_clean($activity));
                $this->stok_by_doc_model->setItemsIn($this->security->xss_clean($qty_terima));
                // $this->stok_by_doc_model->setItemsOut($this->security->xss_clean($_POST['itemsOut']));
                $this->stok_by_doc_model->setOldStock($this->security->xss_clean($oldstock));
                $this->stok_by_doc_model->setCurrentStock($this->security->xss_clean($current_stock));
                $this->stok_by_doc_model->setItemsPrice($this->security->xss_clean($price));
                $this->stok_by_doc_model->setPicData($this->security->xss_clean($this->current_user['user_id']));
                $this->stok_by_doc_model->setDataTime($this->security->xss_clean(dbnow()));
                $this->stok_by_doc_model->insert();

            }

        }

        $status         = 'Closed';

        $this->incoming_stok_model->setIsId($this->security->xss_clean($data_id));
        $this->incoming_stok_model->setIsDate($this->security->xss_clean($is_date));
        $this->incoming_stok_model->setIsNo($this->security->xss_clean($is_no));
        if($to_local=='warehouse'){
        $this->incoming_stok_model->setProjectId($this->security->xss_clean(''));
        $this->incoming_stok_model->setProjectName($this->security->xss_clean(''));
        $this->incoming_stok_model->setWarehouseId($this->security->xss_clean($warehouse_project));
        }else if($to_local=='project'){
        $project_name       = $this->project_model->getById($warehouse_project)->project_name;
        $this->incoming_stok_model->setWarehouseId($this->security->xss_clean(''));
        $this->incoming_stok_model->setProjectName($this->security->xss_clean($project_name));
        $this->incoming_stok_model->setProjectId($this->security->xss_clean($warehouse_project));
        }
        $this->incoming_stok_model->setIsKind($this->security->xss_clean('1'));
        $this->incoming_stok_model->setDocRef($this->security->xss_clean(''));
        $this->incoming_stok_model->setCompanyId($this->security->xss_clean($company_id));
        $this->incoming_stok_model->setCompanyName($this->security->xss_clean($company_name ));
        $this->incoming_stok_model->setIsStatus($this->security->xss_clean('Closed'));
        $this->incoming_stok_model->setSenderPic($this->security->xss_clean(''));
        $this->incoming_stok_model->setReceiverPic($this->security->xss_clean(''));
        $this->incoming_stok_model->setRemarks($this->security->xss_clean($remarks));
        $this->incoming_stok_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->incoming_stok_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->incoming_stok_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->incoming_stok_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        
        // $save       = $this->incoming_stok_model->insert();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            jsout(array('success' => false));
        }else{
            $this->db->trans_commit();
            jsout(array('success' => true, 'status' => 'save',  'is_no' => $is_no));
        }

        

    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_sa = $this->session->userdata('new_sa');

        $items = $new_sa['items'];
        // test($items,1);
        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                $item_qty   = str_replace(',','',$this->security->xss_clean($this->db->escape_str($new_sa['items'][$key]['item_qty'])));
                // test($mbp_id.' '.$item_qty,1);
                unset($new_sa['items'][$key]);
                $new_sa['items'] = array_values($new_sa['items']);
                break;
            }
        }
        // test($new_sa,0);
        $this->session->set_userdata('new_sa', $new_sa);
        jsout(array('success'=>1)); 
    }


}
?>