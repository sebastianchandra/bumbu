<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Incoming_stok_model extends CI_Model
{
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_incoming_stock_01';
    private $isId;
    private $isDate;
    private $isNo;
    private $warehouseId;
    private $projectId;
    private $projectName;
    private $isKind;
    private $docRef;
    private $companyId;
    private $companyName;
    private $isStatus;
    private $senderPic;
    private $receiverPic;
    private $remarks;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->isId = 0;
        $this->isDate = "0000-00-00";
        $this->isNo = '';
        $this->warehouseId = 0;
        $this->projectId = 0;
        $this->projectName = '';
        $this->isKind = 0;
        $this->docRef = '';
        $this->companyId = 0;
        $this->companyName = '';
        $this->isStatus = 0;
        $this->senderPic = '';
        $this->receiverPic = '';
        $this->remarks = '';
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
      
    }

    public function setIsId($aIsId)
     {
        $this->isId = $this->db->escape_str($aIsId);
     }
     public function getIsId()
     {
        return $this->isId;
     }
     public function setIsDate($aIsDate)
     {
        $this->isDate = $this->db->escape_str($aIsDate);
     }
     public function getIsDate()
     {
        return $this->isDate;
     }
     public function setIsNo($aIsNo)
     {
        $this->isNo = $this->db->escape_str($aIsNo);
     }
     public function getIsNo()
     {
        return $this->isNo;
     }
     public function setWarehouseId($aWarehouseId)
     {
        $this->warehouseId = $this->db->escape_str($aWarehouseId);
     }
     public function getWarehouseId()
     {
        return $this->warehouseId;
     }
     public function setProjectId($aProjectId)
     {
        $this->projectId = $this->db->escape_str($aProjectId);
     }
     public function getProjectId()
     {
        return $this->projectId;
     }
     public function setProjectName($aProjectName)
     {
        $this->projectName = $this->db->escape_str($aProjectName);
     }
     public function getProjectName()
     {
        return $this->projectName;
     }
     public function setIsKind($aIsKind)
     {
        $this->isKind = $this->db->escape_str($aIsKind);
     }
     public function getIsKind()
     {
        return $this->isKind;
     }
     public function setDocRef($aDocRef)
     {
        $this->docRef = $this->db->escape_str($aDocRef);
     }
     public function getDocRef()
     {
        return $this->docRef;
     }
     public function setCompanyId($aCompanyId)
     {
        $this->companyId = $this->db->escape_str($aCompanyId);
     }
     public function getCompanyId()
     {
        return $this->companyId;
     }
     public function setCompanyName($aCompanyName)
     {
        $this->companyName = $this->db->escape_str($aCompanyName);
     }
     public function getCompanyName()
     {
        return $this->companyName;
     }
     public function setIsStatus($aIsStatus)
     {
        $this->isStatus = $this->db->escape_str($aIsStatus);
     }
     public function getIsStatus()
     {
        return $this->isStatus;
     }
     public function setSenderPic($aSenderPic)
     {
        $this->senderPic = $this->db->escape_str($aSenderPic);
     }
     public function getSenderPic()
     {
        return $this->senderPic;
     }
     public function setReceiverPic($aReceiverPic)
     {
        $this->receiverPic = $this->db->escape_str($aReceiverPic);
     }
     public function getReceiverPic()
     {
        return $this->receiverPic;
     }
     public function setRemarks($aRemarks)
     {
        $this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
        return $this->remarks;
     }
     public function setPicInput($aPicInput)
     {
        $this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
        return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
        $this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
        return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
        $this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
        return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
        $this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
        return $this->editTime;
     }

    public function insert()
     {
        if($this->isId =='' || $this->isId == NULL )
        {
            $this->isId = 0;
        }
        if($this->isDate =='' || $this->isDate == NULL )
        {
            $this->isDate = "0000-00-00";
        }
        if($this->isNo =='' || $this->isNo == NULL )
        {
            $this->isNo = '';
        }
        if($this->warehouseId =='' || $this->warehouseId == NULL )
        {
            $this->warehouseId = 0;
        }
        if($this->projectId =='' || $this->projectId == NULL )
        {
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL )
        {
            $this->projectName = '';
        }
        if($this->isKind =='' || $this->isKind == NULL )
        {
            $this->isKind = 0;
        }
        if($this->docRef =='' || $this->docRef == NULL )
        {
            $this->docRef = '';
        }
        if($this->companyId =='' || $this->companyId == NULL )
        {
            $this->companyId = 0;
        }
        if($this->companyName =='' || $this->companyName == NULL )
        {
            $this->companyName = '';
        }
        if($this->isStatus =='' || $this->isStatus == NULL )
        {
            $this->isStatus = 0;
        }
        if($this->senderPic =='' || $this->senderPic == NULL )
        {
            $this->senderPic = '';
        }
        if($this->receiverPic =='' || $this->receiverPic == NULL )
        {
            $this->receiverPic = '';
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'is_id,'; 
        $stQuery .=   'is_date,'; 
        $stQuery .=   'is_no,'; 
        $stQuery .=   'warehouse_id,'; 
        $stQuery .=   'project_id,'; 
        $stQuery .=   'project_name,'; 
        $stQuery .=   'is_kind,'; 
        $stQuery .=   'doc_ref,'; 
        $stQuery .=   'company_id,'; 
        $stQuery .=   'company_name,'; 
        $stQuery .=   'is_status,'; 
        $stQuery .=   'sender_pic,'; 
        $stQuery .=   'receiver_pic,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->isId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->isDate).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->isNo).'",'; 
        $stQuery .=   $this->db->escape_str($this->warehouseId).','; 
        $stQuery .=   $this->db->escape_str($this->projectId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->projectName).'",'; 
        $stQuery .=   $this->db->escape_str($this->isKind).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->docRef).'",'; 
        $stQuery .=   $this->db->escape_str($this->companyId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->companyName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->isStatus).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->senderPic).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->receiverPic).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        // test($stQuery,0);
        $this->db->query($stQuery); 
        return $this->isNo;
    }

    function get_id(){
        $query = $this->dbpurch->query("SELECT IFNULL(MAX(is_id)+1,1) is_id FROM trn_incoming_stock_01")->row();
        return $query;
    }

    function get_nomor_dok($periode,$kode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(is_no,10,4))+1,4,'0'),'0001') nomor_dok, is_no FROM trn_incoming_stock_01 
                    WHERE SUBSTRING(is_no,4,6) = '".$periode."' AND SUBSTRING(is_no,1,2)='".$kode."' ")->row();
        return $query;
    }

    function get_po_out(){
        $sql    = "SELECT a.po_id doc_id,a.po_no doc_no,a.po_date doc_date FROM trn_po_01 a WHERE a.po_status IN 
                    ('Process','Outstanding')
                    UNION
                   SELECT b.os_id doc_id,b.os_no doc_no,b.os_date doc_date FROM trn_outgoing_stock_01 b WHERE b.os_status IN 
                   ('Process','Outstanding') AND b.os_kind='Transfer' ORDER BY doc_date";
        return $this->dbpurch->query($sql)->result();
    }

    function get_is(){
        $sql = 'SELECT a.is_id,a.is_date,a.is_kind,a.is_no,a.warehouse_id,b.warehouse_name,a.project_id,c.project_name,a.doc_ref,a.is_status,a.company_name FROM trn_incoming_stock_01 a 
            LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id 
            LEFT JOIN db_bumbu_master.mst_project c on a.project_id=c.project_id ORDER BY a.is_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_is_date($from,$to){
        $sql = 'SELECT a.is_id,a.is_date,a.is_kind,a.is_no,a.warehouse_id,b.warehouse_name,a.project_id,c.project_name,a.doc_ref,a.is_status,a.company_name FROM trn_incoming_stock_01 a 
            LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id 
            LEFT JOIN db_bumbu_master.mst_project c on a.project_id=c.project_id 
            WHERE a.is_date BETWEEN "'.$from.'" AND "'.$to.'" ORDER BY a.is_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function getById($id){
        $sql = " SELECT is_id,a.is_date,a.is_no,a.warehouse_id,b.warehouse_name,a.project_id,c.project_name,a.doc_ref,a.is_status,a.company_name,a.remarks FROM trn_incoming_stock_01 a 
                LEFT JOIN db_bumbu_master.mst_warehouse b ON b.warehouse_id=a.warehouse_id 
                LEFT JOIN db_bumbu_master.mst_project c on a.project_id=c.project_id
                WHERE a.is_id='".$id."' ";
        return $this->dbpurch->query($sql); 
    }

    function doc_ref($os_no){
        return $this->dbpurch->query('SELECT is_id,is_no,doc_ref,warehouse_id,project_id FROM trn_incoming_stock_01 
            WHERE doc_ref="'.$os_no.'"');
    }

    function update_reject($is_id){
         $this->dbpurch->query("UPDATE trn_incoming_stock_01 SET is_status='Reject' WHERE is_id='".$is_id."'");
    }

    function act_reject_js(){

        $id             = $this->input->post('is_id');
        $no             = $this->input->post('is_no');

        $header_is      = $this->getById($id)->row();
        $doc_ref        = $header_is->doc_ref;
        $warehouse_id   = $header_is->warehouse_id;
        $project_id     = $header_is->project_id;
        $company_id     = $this->current_user['company_id'];
        $company_name   = $this->current_user['company_name'];

        $doc_code       = substr($doc_ref,0,2);


        $update_is      = $this->dbpurch->query("UPDATE trn_incoming_stock_01 SET is_status='Reject' WHERE is_id='".$id."'");

        if($doc_code=='OS'){
            $update_ref      = $this->dbpurch->query("UPDATE trn_outgoing_stock_01 SET os_status='Process' WHERE os_no='".$doc_ref."'");
        }elseif($doc_code=='PO'){
            $update_ref      = $this->dbpurch->query("UPDATE trn_po_01 SET po_status='Process' WHERE po_no='".$doc_ref."'");
        }
        
        $data_stock_hist    = $this->dbpurch->query("SELECT stock_by_doc_id,items_id,doc_no,warehouse_id,project_id,trn_date,trn_year,trn_month,activity,items_in,items_price,items_remaining FROM trn_stock_by_doc WHERE doc_no='".$no."'")->result();
        foreach ($data_stock_hist as $key => $value){
            $items_id       = $value->items_id;
            $doc_no         = $value->doc_no;
            $new_doc        = substr($doc_no,0,2).'R'.substr($doc_no,3,10);
            $trn_date_new   = dbnow();
            $trn_year_new   = substr(dbnow(),0,4);
            $trn_month_new  = substr(dbnow(),5,2);

            $trn_date       = $value->trn_date;
            $trn_year       = $value->trn_year;
            $trn_month      = $value->trn_month;

            $activity       = 'Rj_InTrans';
            $items_in       = $value->items_in;
            $warehouse_id   = $value->warehouse_id;
            $item_price     = $value->items_price;   
            $items_remaining= $value->items_remaining; 
            $last_remaining = $items_remaining-$items_in;
            if($last_remaining<0){
                $last_remaining = 0;
            }else{
                $last_remaining = $last_remaining;
            }
            $stock_by_doc_id= $value->stock_by_doc_id;

            $old_stock      = $this->dbpurch->query(" SELECT current_stock FROM trn_stock WHERE items_id='".$items_id."' 
                AND warehouse_id='".$warehouse_id."' ")->row()->current_stock;
            $current_stock  = $old_stock-$items_in;

            $update_stock   = $this->dbpurch->query(" UPDATE trn_stock SET trn_date='".$trn_date_new."', trn_year='".$trn_year_new."', trn_month='".$trn_month_new."', activity='".$activity."', items_in='0', items_out='".$items_in."', 
                old_stock='".$old_stock."', current_stock='".$current_stock."' WHERE items_id='".$items_id."' AND 
                warehouse_id='".$warehouse_id."' ");

            $ustok_remaining= $this->dbpurch->query(" UPDATE trn_stock_by_doc SET items_remaining='".$last_remaining."' WHERE stock_by_doc_id='".$stock_by_doc_id."' ");

            $items_remaining     = $this->dbpurch->query(" SELECT items_remaining  FROM trn_stock_by_doc WHERE doc_no='".$doc_no."' AND items_id='".$items_id."' ")->row()->items_remaining;

            $insert_hist    = $this->dbpurch->query(" INSERT INTO db_bumbu_transaction.trn_stock_hist (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock)
VALUES
('".$items_id."','".$new_doc."','".$warehouse_id."','".$company_id."','".$company_name."','".$trn_date."','".$trn_year."','".$trn_month."','".$activity."','".$items_in."','".$old_stock."','".$current_stock."') ");       

            $insert_by_doc  = $this->dbpurch->query(" INSERT INTO db_bumbu_transaction.trn_stock_by_doc 
(items_id,doc_no,warehouse_id,project_id,trn_date,trn_year,trn_month,activity,items_in,items_out,items_remaining, 
old_stock,current_stock,items_price,pic_data,data_time)
VALUES
('".$items_id."','".$new_doc."','".$warehouse_id."',(NULL),'".$trn_date."','".$trn_year."','".$trn_month."','".$activity."','0','".$items_in."', 
'0','".$old_stock."','".$current_stock."','".$item_price."','".$this->current_user['user_id']."','".dbnow()."') ");

        }

    }

    function print_header($id){
        return $this->dbpurch->query("SELECT a.is_id,a.is_date,a.is_no,a.warehouse_id,b.warehouse_name,a.project_id,c.project_name,
            a.is_kind,a.doc_ref,a.company_id,d.company_name,a.sender_pic,a.receiver_pic,a.remarks
        FROM trn_incoming_stock_01 a 
        LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id
        LEFT JOIN db_bumbu_master.mst_project c ON a.project_id=c.project_id
        LEFT JOIN db_bumbu_master.mst_company d ON a.company_id=a.company_id
        WHERE a.is_id='".$id."'")->row();
    }


// 1. Memunculkan Remarks pada Popup detail Incoming Stock
// 2. Penambahan Print IS















    

    

}   