<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trn_stok_model extends CI_Model
{
    function __construct(){
      parent::__construct();
      
      $this->dbpurch = $this->load->database('purchasing',true);
      
    }

    function qRecalculate(){
        $query = $this->dbpurch->query("SELECT '' id,z.items_id,z.doc_no,z.project_id,z.warehouse_id,z.date_doc,YEAR(z.date_doc) trn_year,SUBSTR(z.date_doc,6,2) trn_month, z.qty,z.price,z.kind
          FROM (
            SELECT a1.is_id id_doc,a1.is_date date_doc,a1.is_no doc_no,a1.warehouse_id warehouse_id,a1.input_time input_time,
            a1.is_kind kind,a2.items_id,a2.qty,a1.project_id,a2.price
            FROM trn_incoming_stock_01 a1, trn_incoming_stock_02 a2 
            WHERE a1.is_id=a2.is_id AND a1.is_status<>'Reject' AND a1.warehouse_id<>0
          UNION
            SELECT b1.os_id id_doc,b1.os_date date_doc,b1.os_no doc_no,b1.warehouse_id warehouse_id,b1.input_date input_time,
            b1.os_kind kind,b2.items_id,b2.qty,IFNULL(b1.project_id,0)project_id,b2.price
            FROM trn_outgoing_stock_01 b1, trn_outgoing_stock_02 b2 
            WHERE b1.os_id=b2.os_id AND b1.os_status<>'Reject' AND b1.warehouse_id<>0) z
          ORDER BY z.date_doc,SUBSTR(z.doc_no,1,2)")->result();
        return $query;
    }

    function get_laststockhistory(){
      $sql        = " SELECT a.items_id,b.items_name,a.current_stock,a.warehouse_id,c.warehouse_name FROM trn_stock_tmp a left join db_bumbu_master.mst_warehouse c ON c.warehouse_id=a.warehouse_id, db_bumbu_master.mst_items b WHERE a.items_id=b.items_id ORDER BY  b.items_name";
      return $this->dbpurch->query($sql);
    }

}   