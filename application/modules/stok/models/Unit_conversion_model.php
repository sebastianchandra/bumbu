<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  May 08, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Unit_conversion_model extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_bumbu_transaction';
     private $myTable = 'trn_unit_conversion';
     private $uvId;
     private $uvNo;
     private $warehouseId;
     private $itemsIdFrom;
     private $unitsIdFrom;
     private $qtyFrom;
     private $priceFrom;
     private $itemsIdTo;
     private $unitsIdTo;
     private $qtyTo;
     private $priceTo;
     private $remarks;
     private $isActive;
     private $picInput;
     private $inputTime;
     private $picEdit;
     private $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
          parent::__construct();
          $this->dbpurch = $this->load->database('purchasing',true);
          $this->uvId = 0;
          $this->uvNo = '';
          $this->warehouseId = 0;
          $this->itemsIdFrom = 0;
          $this->unitsIdFrom = 0;
          $this->qtyFrom = 0;
          $this->priceFrom = 0;
          $this->itemsIdTo = 0;
          $this->unitsIdTo = 0;
          $this->qtyTo = 0;
          $this->priceTo = 0;
          $this->remarks = '';
          $this->isActive = 0;
          $this->picInput = 0;
          $this->inputTime = '0000-00-00 00:00:00';
          $this->picEdit = 0;
          $this->editTime = '0000-00-00 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setUvId($aUvId)
     {
          $this->uvId = $this->db->escape_str($aUvId);
     }
     public function getUvId()
     {
          return $this->uvId;
     }
     public function setUvNo($aUvNo)
     {
          $this->uvNo = $this->db->escape_str($aUvNo);
     }
     public function getUvNo()
     {
          return $this->uvNo;
     }
     public function setWarehouseId($aWarehouseId)
     {
          $this->warehouseId = $this->db->escape_str($aWarehouseId);
     }
     public function getWarehouseId()
     {
          return $this->warehouseId;
     }
     public function setItemsIdFrom($aItemsIdFrom)
     {
          $this->itemsIdFrom = $this->db->escape_str($aItemsIdFrom);
     }
     public function getItemsIdFrom()
     {
          return $this->itemsIdFrom;
     }
     public function setUnitsIdFrom($aUnitsIdFrom)
     {
          $this->unitsIdFrom = $this->db->escape_str($aUnitsIdFrom);
     }
     public function getUnitsIdFrom()
     {
          return $this->unitsIdFrom;
     }
     public function setQtyFrom($aQtyFrom)
     {
          $this->qtyFrom = $this->db->escape_str($aQtyFrom);
     }
     public function getQtyFrom()
     {
          return $this->qtyFrom;
     }
     public function setPriceFrom($aPriceFrom)
     {
          $this->priceFrom = $this->db->escape_str($aPriceFrom);
     }
     public function getPriceFrom()
     {
          return $this->priceFrom;
     }
     public function setItemsIdTo($aItemsIdTo)
     {
          $this->itemsIdTo = $this->db->escape_str($aItemsIdTo);
     }
     public function getItemsIdTo()
     {
          return $this->itemsIdTo;
     }
     public function setUnitsIdTo($aUnitsIdTo)
     {
          $this->unitsIdTo = $this->db->escape_str($aUnitsIdTo);
     }
     public function getUnitsIdTo()
     {
          return $this->unitsIdTo;
     }
     public function setQtyTo($aQtyTo)
     {
          $this->qtyTo = $this->db->escape_str($aQtyTo);
     }
     public function getQtyTo()
     {
          return $this->qtyTo;
     }
     public function setPriceTo($aPriceTo)
     {
          $this->priceTo = $this->db->escape_str($aPriceTo);
     }
     public function getPriceTo()
     {
          return $this->priceTo;
     }
     public function setRemarks($aRemarks)
     {
          $this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
          return $this->remarks;
     }
     public function setIsActive($aIsActive)
     {
          $this->isActive = $this->db->escape_str($aIsActive);
     }
     public function getIsActive()
     {
          return $this->isActive;
     }
     public function setPicInput($aPicInput)
     {
          $this->picInput = $this->db->escape_str($aPicInput);
     }
     public function getPicInput()
     {
          return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
          $this->inputTime = $this->db->escape_str($aInputTime);
     }
     public function getInputTime()
     {
          return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
          $this->picEdit = $this->db->escape_str($aPicEdit);
     }
     public function getPicEdit()
     {
          return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
          $this->editTime = $this->db->escape_str($aEditTime);
     }
     public function getEditTime()
     {
          return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
          if($this->uvId =='' || $this->uvId == NULL )
          {
               $this->uvId = 0;
          }
          if($this->uvNo =='' || $this->uvNo == NULL )
          {
               $this->uvNo = '';
          }
          if($this->warehouseId =='' || $this->warehouseId == NULL )
          {
               $this->warehouseId = 0;
          }
          if($this->itemsIdFrom =='' || $this->itemsIdFrom == NULL )
          {
               $this->itemsIdFrom = 0;
          }
          if($this->unitsIdFrom =='' || $this->unitsIdFrom == NULL )
          {
               $this->unitsIdFrom = 0;
          }
          if($this->qtyFrom =='' || $this->qtyFrom == NULL )
          {
               $this->qtyFrom = 0;
          }
          if($this->priceFrom =='' || $this->priceFrom == NULL )
          {
               $this->priceFrom = 0;
          }
          if($this->itemsIdTo =='' || $this->itemsIdTo == NULL )
          {
               $this->itemsIdTo = 0;
          }
          if($this->unitsIdTo =='' || $this->unitsIdTo == NULL )
          {
               $this->unitsIdTo = 0;
          }
          if($this->qtyTo =='' || $this->qtyTo == NULL )
          {
               $this->qtyTo = 0;
          }
          if($this->priceTo =='' || $this->priceTo == NULL )
          {
               $this->priceTo = 0;
          }
          if($this->remarks =='' || $this->remarks == NULL )
          {
               $this->remarks = '';
          }
          if($this->isActive =='' || $this->isActive == NULL )
          {
               $this->isActive = 0;
          }
          if($this->picInput =='' || $this->picInput == NULL )
          {
               $this->picInput = 0;
          }
          if($this->inputTime =='' || $this->inputTime == NULL )
          {
               $this->inputTime = '0000-00-00 00:00:00';
          }
          if($this->picEdit =='' || $this->picEdit == NULL )
          {
               $this->picEdit = 0;
          }
          if($this->editTime =='' || $this->editTime == NULL )
          {
               $this->editTime = '0000-00-00 00:00:00';
          }
          
          $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
          $stQuery .= '( '; 
          $stQuery .=   'uv_id,'; 
          $stQuery .=   'uv_no,'; 
          $stQuery .=   'warehouse_id,'; 
          $stQuery .=   'items_id_from,'; 
          $stQuery .=   'units_id_from,'; 
          $stQuery .=   'qty_from,'; 
          $stQuery .=   'price_from,'; 
          $stQuery .=   'items_id_to,'; 
          $stQuery .=   'units_id_to,'; 
          $stQuery .=   'qty_to,'; 
          $stQuery .=   'price_to,'; 
          $stQuery .=   'remarks,'; 
          $stQuery .=   'is_active,'; 
          $stQuery .=   'pic_input,'; 
          $stQuery .=   'input_time'; 
          // $stQuery .=   'pic_edit,'; 
          // $stQuery .=   'edit_time'; 
          $stQuery .= ') '; 
          $stQuery .= 'VALUES '; 
          $stQuery .= '( '; 
          $stQuery .=   $this->db->escape_str($this->uvId).','; 
          $stQuery .=   '"'.$this->db->escape_str($this->uvNo).'",'; 
          $stQuery .=   $this->db->escape_str($this->warehouseId).','; 
          $stQuery .=   $this->db->escape_str($this->itemsIdFrom).','; 
          $stQuery .=   $this->db->escape_str($this->unitsIdFrom).','; 
          $stQuery .=   $this->db->escape_str($this->qtyFrom).','; 
          $stQuery .=   $this->db->escape_str($this->priceFrom).','; 
          $stQuery .=   $this->db->escape_str($this->itemsIdTo).','; 
          $stQuery .=   $this->db->escape_str($this->unitsIdTo).','; 
          $stQuery .=   $this->db->escape_str($this->qtyTo).','; 
          $stQuery .=   $this->db->escape_str($this->priceTo).','; 
          $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
          $stQuery .=   $this->db->escape_str($this->isActive).','; 
          $stQuery .=   $this->db->escape_str($this->picInput).','; 
          $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
          // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
          // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
          $stQuery .= '); '; 
          $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function update($id)
     {
          if($this->uvId =='' || $this->uvId == NULL )
          {
               $this->uvId = 0;
          }
          if($this->uvNo =='' || $this->uvNo == NULL )
          {
               $this->uvNo = '';
          }
          if($this->warehouseId =='' || $this->warehouseId == NULL )
          {
               $this->warehouseId = 0;
          }
          if($this->itemsIdFrom =='' || $this->itemsIdFrom == NULL )
          {
               $this->itemsIdFrom = 0;
          }
          if($this->unitsIdFrom =='' || $this->unitsIdFrom == NULL )
          {
               $this->unitsIdFrom = 0;
          }
          if($this->qtyFrom =='' || $this->qtyFrom == NULL )
          {
               $this->qtyFrom = 0;
          }
          if($this->priceFrom =='' || $this->priceFrom == NULL )
          {
               $this->priceFrom = 0;
          }
          if($this->itemsIdTo =='' || $this->itemsIdTo == NULL )
          {
               $this->itemsIdTo = 0;
          }
          if($this->unitsIdTo =='' || $this->unitsIdTo == NULL )
          {
               $this->unitsIdTo = 0;
          }
          if($this->qtyTo =='' || $this->qtyTo == NULL )
          {
               $this->qtyTo = 0;
          }
          if($this->priceTo =='' || $this->priceTo == NULL )
          {
               $this->priceTo = 0;
          }
          if($this->remarks =='' || $this->remarks == NULL )
          {
               $this->remarks = '';
          }
          if($this->isActive =='' || $this->isActive == NULL )
          {
               $this->isActive = 0;
          }
          if($this->picInput =='' || $this->picInput == NULL )
          {
               $this->picInput = 0;
          }
          if($this->inputTime =='' || $this->inputTime == NULL )
          {
               $this->inputTime = '0000-00-00 00:00:00';
          }
          if($this->picEdit =='' || $this->picEdit == NULL )
          {
               $this->picEdit = 0;
          }
          if($this->editTime =='' || $this->editTime == NULL )
          {
               $this->editTime = '0000-00-00 00:00:00';
          }
          
          $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
          $stQuery .= 'SET '; 
          $stQuery .=   'uv_id ='.$this->db->escape_str($this->uvId).','; 
          $stQuery .=   'uv_no ="'.$this->db->escape_str($this->uvNo).'",'; 
          $stQuery .=   'warehouse_id ='.$this->db->escape_str($this->warehouseId).','; 
          $stQuery .=   'items_id_from ='.$this->db->escape_str($this->itemsIdFrom).','; 
          $stQuery .=   'units_id_from ='.$this->db->escape_str($this->unitsIdFrom).','; 
          $stQuery .=   'qty_from ='.$this->db->escape_str($this->qtyFrom).','; 
          $stQuery .=   'price_from ='.$this->db->escape_str($this->priceFrom).','; 
          $stQuery .=   'items_id_to ='.$this->db->escape_str($this->itemsIdTo).','; 
          $stQuery .=   'units_id_to ='.$this->db->escape_str($this->unitsIdTo).','; 
          $stQuery .=   'qty_to ='.$this->db->escape_str($this->qtyTo).','; 
          $stQuery .=   'price_to ='.$this->db->escape_str($this->priceTo).','; 
          $stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
          $stQuery .=   'is_active ='.$this->db->escape_str($this->isActive).','; 
          $stQuery .=   'pic_input ='.$this->db->escape_str($this->picInput).','; 
          $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
          $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
          $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
          $stQuery .= 'WHERE '; 
          $stQuery .=   'uv_id = '.$this->db->escape_str($id).''; 
          $this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
          $stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
          $stQuery .= 'WHERE uv_id = '.$this->db->escape_str($id).''; 
          $this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
          $this->db->select('*');
          $this->db->from($this->myDb.'.'.$this->myTable);
          $this->db->order_by('uv_id', 'ASC');
          return $this->db->get()->result_array();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
          $this->db->select('*');
          $this->db->from($this->myDb.'.'.$this->myTable);
          $this->db->where('uv_id', $this->db->escape_str($id));
          return $this->db->get()->row_array();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
          $this->db->select('*');
          $this->db->from($this->myDb.'.'.$this->myTable);
          $this->db->where('uv_id', $this->db->escape_str($id));
          $row = $this->db->get()->row_array();
          $this->uvId = $row['uv_id']; 
          $this->uvNo = $row['uv_no']; 
          $this->warehouseId = $row['warehouse_id']; 
          $this->itemsIdFrom = $row['items_id_from']; 
          $this->unitsIdFrom = $row['units_id_from']; 
          $this->qtyFrom = $row['qty_from']; 
          $this->priceFrom = $row['price_from']; 
          $this->itemsIdTo = $row['items_id_to']; 
          $this->unitsIdTo = $row['units_id_to']; 
          $this->qtyTo = $row['qty_to']; 
          $this->priceTo = $row['price_to']; 
          $this->remarks = $row['remarks']; 
          $this->isActive = $row['is_active']; 
          $this->picInput = $row['pic_input']; 
          $this->inputTime = $row['input_time']; 
          $this->picEdit = $row['pic_edit']; 
          $this->editTime = $row['edit_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
          $stQuery  = 'SELECT uv_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
          $query = $this->db->query($stQuery);
          return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
          $this->uvId = 0; 
          $this->uvNo = ''; 
          $this->warehouseId = 0; 
          $this->itemsIdFrom = 0; 
          $this->unitsIdFrom = 0; 
          $this->qtyFrom = 0; 
          $this->priceFrom = 0; 
          $this->itemsIdTo = 0; 
          $this->unitsIdTo = 0; 
          $this->qtyTo = 0; 
          $this->priceTo = 0; 
          $this->remarks = ''; 
          $this->isActive = 0; 
          $this->picInput = 0; 
          $this->inputTime = '0000-00-00 00:00:00'; 
          $this->picEdit = 0; 
          $this->editTime = '0000-00-00 00:00:00'; 
     }
     /* END OF RESET VALUES */

     /* END OF RESET VALUES */
     function get_data(){
          return $this->dbpurch->query("SELECT a.uv_id,d.warehouse_name,b1.items_name items_from,c1.items_unit_name unit_from,
                                   a.qty_from,a.price_from,b2.items_name items_to,c2.items_unit_name unit_to,a.qty_to,a.price_to,
                                   a.remarks,a.uv_no,a.is_active  FROM trn_unit_conversion a 
                                   LEFT JOIN db_bumbu_master.mst_items b1 ON a.items_id_from=b1.items_id
                                   LEFT JOIN db_bumbu_master.mst_items_unit c1 ON a.units_id_from=c1.items_unit_id
                                   LEFT JOIN db_bumbu_master.mst_items b2 ON a.items_id_to=b2.items_id
                                   LEFT JOIN db_bumbu_master.mst_items_unit c2 ON a.units_id_to=c2.items_unit_id
                                   LEFT JOIN db_bumbu_master.mst_warehouse d ON a.warehouse_id=d.warehouse_id
                                   /*WHERE a.is_active='1'*/
                                   ORDER BY a.uv_id DESC")->result();
     }

     function get_nomor_dok($periode,$kode){
          $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(uv_no,10,4))+1,4,'0'),'0001') nomor_dok, uv_no FROM trn_unit_conversion 
                    WHERE SUBSTRING(uv_no,4,6) = '".$periode."' ")->row();
          return $query;
     }

     function reject_js($id){
          $this->dbpurch->query(" UPDATE db_bumbu_transaction.trn_unit_conversion SET is_active='0' WHERE uv_id='".$id."' ");
     }
}
?>
