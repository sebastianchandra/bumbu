<?php
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Mar 24, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Incoming_stok_model02 extends CI_Model
{
     /* START PRIVATE VARIABLES */
     private $myDb = 'db_bumbu_transaction';
     private $myTable = 'trn_incoming_stock_02';
     private $is02Id;
     private $isId;
     private $itemsId;
     private $itemsName;
     private $qty;
     private $poQty;
     private $price;
     private $disc;
     private $total;
     private $remarks;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */
     public function __construct()
     {
     	parent::__construct();
          $this->dbpurch = $this->load->database('purchasing',true);
          $this->is02Id = 0;
          $this->isId = 0;
          $this->itemsId = 0;
          $this->itemsName = '';
          $this->qty = 0;
          $this->poQty = 0;
          $this->price = 0;
          $this->disc = 0;
          $this->total = 0;
          $this->remarks = '';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setIs02Id($aIs02Id)
     {
     	$this->is02Id = $this->db->escape_str($aIs02Id);
     }
     public function getIs02Id()
     {
     	return $this->is02Id;
     }
     public function setIsId($aIsId)
     {
     	$this->isId = $this->db->escape_str($aIsId);
     }
     public function getIsId()
     {
     	return $this->isId;
     }
     public function setItemsId($aItemsId)
     {
     	$this->itemsId = $this->db->escape_str($aItemsId);
     }
     public function getItemsId()
     {
     	return $this->itemsId;
     }
     public function setItemsName($aItemsName)
     {
     	$this->itemsName = $this->db->escape_str($aItemsName);
     }
     public function getItemsName()
     {
     	return $this->itemsName;
     }
     public function setQty($aQty)
     {
     	$this->qty = $this->db->escape_str($aQty);
     }
     public function getQty()
     {
     	return $this->qty;
     }
     public function setPoQty($aPoQty)
     {
     	$this->poQty = $this->db->escape_str($aPoQty);
     }
     public function getPoQty()
     {
     	return $this->poQty;
     }
     public function setPrice($aPrice)
     {
     	$this->price = $this->db->escape_str($aPrice);
     }
     public function getPrice()
     {
     	return $this->price;
     }
     public function setDisc($aDisc)
     {
          $this->disc = $this->db->escape_str($aDisc);
     }
     public function getDisc()
     {
          return $this->disc;
     }
     public function setTotal($aTotal)
     {
     	$this->total = $this->db->escape_str($aTotal);
     }
     public function getTotal()
     {
     	return $this->total;
     }
     public function setRemarks($aRemarks)
     {
     	$this->remarks = $this->db->escape_str($aRemarks);
     }
     public function getRemarks()
     {
     	return $this->remarks;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function insert()
     {
     	if($this->is02Id =='' || $this->is02Id == NULL )
     	{
          	$this->is02Id = 0;
     	}
     	if($this->isId =='' || $this->isId == NULL )
     	{
          	$this->isId = 0;
     	}
     	if($this->itemsId =='' || $this->itemsId == NULL )
     	{
          	$this->itemsId = 0;
     	}
     	if($this->itemsName =='' || $this->itemsName == NULL )
     	{
          	$this->itemsName = '';
     	}
     	if($this->qty =='' || $this->qty == NULL )
     	{
          	$this->qty = 0;
     	}
     	if($this->poQty =='' || $this->poQty == NULL )
     	{
          	$this->poQty = 0;
     	}
     	if($this->price =='' || $this->price == NULL )
     	{
          	$this->price = 0;
     	}
          if($this->disc =='' || $this->disc == NULL )
          {
               $this->disc = 0;
          }
     	if($this->total =='' || $this->total == NULL )
     	{
          	$this->total = 0;
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	
     	$stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= '( '; 
     	// $stQuery .=   'is_02_id,'; 
     	$stQuery .=   'is_id,'; 
     	$stQuery .=   'items_id,'; 
     	$stQuery .=   'items_name,'; 
     	$stQuery .=   'qty,'; 
     	$stQuery .=   'po_qty,'; 
          $stQuery .=   'price,'; 
     	$stQuery .=   'discount,'; 
     	$stQuery .=   'total,'; 
     	$stQuery .=   'remarks'; 
     	$stQuery .= ') '; 
     	$stQuery .= 'VALUES '; 
     	$stQuery .= '( '; 
     	// $stQuery .=   $this->db->escape_str($this->is02Id).','; 
     	$stQuery .=   $this->db->escape_str($this->isId).','; 
     	$stQuery .=   $this->db->escape_str($this->itemsId).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->itemsName).'",'; 
     	$stQuery .=   $this->db->escape_str($this->qty).','; 
     	$stQuery .=   $this->db->escape_str($this->poQty).','; 
          $stQuery .=   $this->db->escape_str($this->price).','; 
     	$stQuery .=   $this->db->escape_str($this->disc).','; 
     	$stQuery .=   $this->db->escape_str($this->total).','; 
     	$stQuery .=   '"'.$this->db->escape_str($this->remarks).'"'; 
     	$stQuery .= '); '; 
          // test($stQuery,0);
     	$this->db->query($stQuery); 
     }
     /* END INSERT */

     function getDetailIs($id){
          return $this->dbpurch->query("SELECT is_id,items_id,items_name,qty,price,discount,total FROM trn_incoming_stock_02 WHERE is_id='".$id."'");
     }

     /* START UPDATE */
     public function update($id)
     {
     	if($this->is02Id =='' || $this->is02Id == NULL )
     	{
          	$this->is02Id = 0;
     	}
     	if($this->isId =='' || $this->isId == NULL )
     	{
          	$this->isId = 0;
     	}
     	if($this->itemsId =='' || $this->itemsId == NULL )
     	{
          	$this->itemsId = 0;
     	}
     	if($this->itemsName =='' || $this->itemsName == NULL )
     	{
          	$this->itemsName = '';
     	}
     	if($this->qty =='' || $this->qty == NULL )
     	{
          	$this->qty = 0;
     	}
     	if($this->poQty =='' || $this->poQty == NULL )
     	{
          	$this->poQty = 0;
     	}
     	if($this->price =='' || $this->price == NULL )
     	{
          	$this->price = 0;
     	}
     	if($this->total =='' || $this->total == NULL )
     	{
          	$this->total = 0;
     	}
     	if($this->remarks =='' || $this->remarks == NULL )
     	{
          	$this->remarks = '';
     	}
     	
     	$stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'SET '; 
     	$stQuery .=   'is_02_id ='.$this->db->escape_str($this->is02Id).','; 
     	$stQuery .=   'is_id ='.$this->db->escape_str($this->isId).','; 
     	$stQuery .=   'items_id ='.$this->db->escape_str($this->itemsId).','; 
     	$stQuery .=   'items_name ="'.$this->db->escape_str($this->itemsName).'",'; 
     	$stQuery .=   'qty ='.$this->db->escape_str($this->qty).','; 
     	$stQuery .=   'po_qty ='.$this->db->escape_str($this->poQty).','; 
     	$stQuery .=   'price ='.$this->db->escape_str($this->price).','; 
     	$stQuery .=   'total ='.$this->db->escape_str($this->total).','; 
     	$stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'" '; 
     	$stQuery .= 'WHERE '; 
     	$stQuery .=   'is_02_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function delete($id)
     {
     	$stQuery  = 'DELETE FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$stQuery .= 'WHERE is_02_id = '.$this->db->escape_str($id).''; 
     	$this->db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->order_by('is_02_id', 'ASC');
     	return $this->db->get()->result_array();
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('is_02_id', $this->db->escape_str($id));
     	return $this->db->get()->row_array();
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function getObjectById($id)
     {
     	$this->db->select('*');
     	$this->db->from($this->myDb.'.'.$this->myTable);
     	$this->db->where('is_02_id', $this->db->escape_str($id));
     	$row = $this->db->get()->row_array();
     	$this->is02Id = $row['is_02_id']; 
     	$this->isId = $row['is_id']; 
     	$this->itemsId = $row['items_id']; 
     	$this->itemsName = $row['items_name']; 
     	$this->qty = $row['qty']; 
     	$this->poQty = $row['po_qty']; 
     	$this->price = $row['price']; 
     	$this->total = $row['total']; 
     	$this->remarks = $row['remarks']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF GET DATA COUNT */
     public function getDataCount()
     {
     	$stQuery  = 'SELECT is_02_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
     	$query = $this->db->query($stQuery);
     	return $query->num_rows();
     }
     /* END OF GET DATA COUNT */
     /* START OF RESET VALUES */
     public function resetValues()
     {
     	$this->is02Id = 0; 
     	$this->isId = 0; 
     	$this->itemsId = 0; 
     	$this->itemsName = ''; 
     	$this->qty = 0; 
     	$this->poQty = 0; 
     	$this->price = 0; 
     	$this->total = 0; 
     	$this->remarks = ''; 
     }
     /* END OF RESET VALUES */

     function print_detail($id){
          return $this->dbpurch->query("SELECT a.is_02_id,a.is_id,a.items_id,b.items_name,a.qty,a.price,a.discount,a.total,a.remarks
               FROM trn_incoming_stock_02 a
               LEFT JOIN db_bumbu_master.mst_items b ON a.items_id=b.items_id
               WHERE a.is_id='".$id."'")->result();
     }
}

// 1. Memunculkan Remarks pada Popup detail Incoming Stock
// 2. Penambahan Print IS
?>
