<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Outgoing_stok_model extends CI_Model
{
    
    private $myDb = 'db_bumbu_transaction';
    private $myTable = 'trn_outgoing_stock_01';
    private $osId;
    private $osDate;
    private $osNo;
    private $companyId;
    private $companyName;
    private $warehouseId;
    private $destWhId;
    private $projectId;
    private $projectName;
    private $dept;
    private $osKind;
    private $osStatus;
    private $osRequester;
    private $remarks;
    private $picInput;
    private $inputDate;
    private $picEdit;
    private $editDate;

    function __construct(){
        parent::__construct();      
        $this->dbpurch = $this->load->database('purchasing',true);
        $this->osId = 0;
        $this->osDate = "0000-00-00";
        $this->osNo = '';
        $this->companyId = 0;
        $this->companyName = '';
        $this->warehouseId = 0;
        $this->destWhId = 0;
        $this->projectId = 0;
        $this->projectName = '';
        $this->dept = 0;
        $this->osKind = 0;
        $this->osStatus = 0;
        $this->osRequester = '';
        $this->remarks = '';
        $this->picInput = 0;
        $this->inputDate = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editDate = '0000-00-00 00:00:00';
      
    }

    public function setOsId($aOsId)
    {
        $this->osId = $this->db->escape_str($aOsId);
    }
    public function getOsId()
    {
        return $this->osId;
    }
    public function setOsDate($aOsDate)
    {
        $this->osDate = $this->db->escape_str($aOsDate);
    }
    public function getOsDate()
    {
        return $this->osDate;
    }
    public function setOsNo($aOsNo)
    {
        $this->osNo = $this->db->escape_str($aOsNo);
    }
    public function getOsNo()
    {
        return $this->osNo;
    }
    public function setCompanyId($aCompanyId)
    {
        $this->companyId = $this->db->escape_str($aCompanyId);
    }
    public function getCompanyId()
    {
        return $this->companyId;
    }
    public function setCompanyName($aCompanyName)
    {
        $this->companyName = $this->db->escape_str($aCompanyName);
    }
    public function getCompanyName()
    {
        return $this->companyName;
    }
    public function setWarehouseId($aWarehouseId)
    {
        $this->warehouseId = $this->db->escape_str($aWarehouseId);
    }
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }
    public function setDestWhId($aDestWhId)
    {
        $this->destWhId = $this->db->escape_str($aDestWhId);
    }
    public function getDestWhId()
    {
        return $this->destWhId;
    }
    public function setProjectId($aProjectId)
    {
        $this->projectId = $this->db->escape_str($aProjectId);
    }
    public function getProjectId()
    {
        return $this->projectId;
    }
    public function setProjectName($aProjectName)
    {
        $this->projectName = $this->db->escape_str($aProjectName);
    }
    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setDept($aDept)
    {
        $this->dept = $this->db->escape_str($aDept);
    }
    public function getDept()
    {
        return $this->dept;
    }
    public function setOsKind($aOsKind)
    {
        $this->osKind = $this->db->escape_str($aOsKind);
    }
    public function getOsKind()
    {
        return $this->osKind;
    }
    public function setOsStatus($aOsStatus)
    {
        $this->osStatus = $this->db->escape_str($aOsStatus);
    }
    public function getOsStatus()
    {
        return $this->osStatus;
    }
    public function setOsRequester($aOsRequester)
    {
        $this->osRequester = $this->db->escape_str($aOsRequester);
    }
    public function getOsRequester()
    {
        return $this->osRequester;
    }
    public function setRemarks($aRemarks)
    {
        $this->remarks = $this->db->escape_str($aRemarks);
    }
    public function getRemarks()
    {
        return $this->remarks;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputDate($aInputDate)
    {
        $this->inputDate = $this->db->escape_str($aInputDate);
    }
    public function getInputDate()
    {
        return $this->inputDate;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditDate($aEditDate)
    {
        $this->editDate = $this->db->escape_str($aEditDate);
    }
    public function getEditDate()
    {
        return $this->editDate;
    }

    public function insert()
    {
        if($this->osId =='' || $this->osId == NULL )
        {
            $this->osId = 0;
        }
        if($this->osDate =='' || $this->osDate == NULL )
        {
            $this->osDate = "0000-00-00";
        }
        if($this->osNo =='' || $this->osNo == NULL )
        {
            $this->osNo = '';
        }
        if($this->companyId =='' || $this->companyId == NULL )
        {
            $this->companyId = 0;
        }
        if($this->companyName =='' || $this->companyName == NULL )
        {
            $this->companyName = '';
        }
        if($this->warehouseId =='' || $this->warehouseId == NULL )
        {
            $this->warehouseId = 0;
        }
        if($this->destWhId =='' || $this->destWhId == NULL )
        {
            $this->destWhId = 0;
        }
        if($this->projectId =='' || $this->projectId == NULL )
        {
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL )
        {
            $this->projectName = '';
        }
        if($this->dept =='' || $this->dept == NULL )
        {
            $this->dept = 0;
        }
        if($this->osKind =='' || $this->osKind == NULL )
        {
            $this->osKind = 0;
        }
        if($this->osStatus =='' || $this->osStatus == NULL )
        {
            $this->osStatus = 0;
        }
        if($this->osRequester =='' || $this->osRequester == NULL )
        {
            $this->osRequester = '';
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputDate =='' || $this->inputDate == NULL )
        {
            $this->inputDate = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editDate =='' || $this->editDate == NULL )
        {
            $this->editDate = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'os_id,'; 
        $stQuery .=   'os_date,'; 
        $stQuery .=   'os_no,'; 
        $stQuery .=   'company_id,'; 
        $stQuery .=   'company_name,'; 
        $stQuery .=   'warehouse_id,'; 
        $stQuery .=   'dest_wh_id,'; 
        $stQuery .=   'project_id,'; 
        $stQuery .=   'project_name,'; 
        // $stQuery .=   'dept,'; 
        $stQuery .=   'os_kind,'; 
        $stQuery .=   'os_status,'; 
        $stQuery .=   'os_requester,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_date'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_date'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   $this->db->escape_str($this->osId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->osDate).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->osNo).'",'; 
        $stQuery .=   $this->db->escape_str($this->companyId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->companyName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->warehouseId).'",';  
        $stQuery .=   $this->db->escape_str($this->destWhId).','; 
        $stQuery .=   $this->db->escape_str($this->projectId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->projectName).'",'; 
        // $stQuery .=   $this->db->escape_str($this->dept).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->osKind).'",';  
        $stQuery .=   '"'.$this->db->escape_str($this->osStatus).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->osRequester).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputDate).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editDate).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
    }

    function getDataCount()
    {
        $stQuery  = 'SELECT IFNULL(MAX(os_id),0)+1 os_id FROM trn_outgoing_stock_01 '; 
        $query = $this->dbpurch->query($stQuery);
        return $query->row();
    }

    function get_nomor_dok($periode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(os_no,10,4))+1,4,'0'),'0001') nomor_dok, os_no FROM `trn_outgoing_stock_01` WHERE SUBSTRING(os_no,4,6) = '".$periode."'")->row();
        return $query;
    }

    function update_status_outgoing($no_po,$status){
        $this->dbpurch->query("UPDATE `db_bumbu_transaction`.`trn_outgoing_stock_01` SET `os_status` = '".$status."' WHERE `os_no` = '".$no_po."'");
    }

    function getById($id){
        $sql = " SELECT a.*,b.warehouse_name FROM trn_outgoing_stock_01 a LEFT JOIN db_bumbu_master.mst_warehouse b ON 
                    b.warehouse_id=a.warehouse_id WHERE a.os_id='".$id."' ";
        return $this->dbpurch->query($sql); 
    }

    function getByDoc($noDoc){
        return $this->dbpurch->query('SELECT remarks AS remarks FROM trn_outgoing_stock_01 WHERE os_no="'.$noDoc.'"');
    }

    function update_status_os($os_id){
        $this->dbpurch->query("UPDATE trn_outgoing_stock_01 SET os_status='Reject' WHERE os_id='".$os_id."'");
    }

    function data_by_doc($os_no){
        return $this->dbpurch->query("SELECT * FROM trn_stock_by_doc WHERE doc_no='".$os_no."'")->result();
    }

    function old_stock($items_id,$warehouse_id){
        return $this->dbpurch->query(" SELECT current_stock FROM trn_stock WHERE items_id='".$items_id."' 
                AND warehouse_id='".$warehouse_id."' ")->row();
    }

    function act_reject_js(){        

//         // $update_status_os = $this->dbpurch->query("UPDATE trn_outgoing_stock_01 SET os_status='Reject' WHERE os_id='".$os_id."'");

//         // $data_by_doc    = $this->dbpurch->query("SELECT * FROM trn_stock_by_doc WHERE doc_no='".$os_no."'")->result();
//         // test($data_by_doc,1);
//         foreach ($data_by_doc as $key => $value) {
            
//             // $items_id           = $value->items_id;
//             // $doc_no             = $value->doc_no;
//             // $warehouse_id       = $value->warehouse_id;
//             // $items_out          = $value->items_out;
//             // $activity           = 'Rj_OutTrans';
//             // $new_doc            = substr($doc_no,0,2).'R'.substr($doc_no,3,10);
//             // $trn_date_new       = dbnow();
//             // $trn_year_new       = substr(dbnow(),0,4);
//             // $trn_month_new      = substr(dbnow(),5,2);

//             // $trn_date           = $value->trn_date;
//             // $trn_year           = $value->trn_year;
//             // $trn_month          = $value->trn_month;
//             // $items_price        = $value->items_price;

//             // $old_stock      = $this->dbpurch->query(" SELECT current_stock FROM trn_stock WHERE items_id='".$items_id."' 
//             //     AND warehouse_id='".$warehouse_id."' ")->row()->current_stock;
//             // $current_stock  = $old_stock+$items_out;

//             // $update_stock   = $this->dbpurch->query(" UPDATE trn_stock SET trn_date='".$trn_date_new."', trn_year='".$trn_year_new."', trn_month='".$trn_month_new."', activity='".$activity."', items_in='".$items_out."', items_out='0', 
//             //     old_stock='".$old_stock."', current_stock='".$current_stock."' WHERE items_id='".$items_id."' AND 
//             //     warehouse_id='".$warehouse_id."' ");

//             // $q_lastbydoc    = $this->dbpurch->query(" SELECT stock_by_doc_id,items_id,doc_no,items_remaining FROM trn_stock_by_doc WHERE items_id='".$items_id."' AND warehouse_id='".$warehouse_id."' AND items_remaining>0 
//             //     ORDER BY stock_by_doc_id ASC LIMIT 1 ")->row();
//             // $stock_by_doc_id    = $q_lastbydoc->stock_by_doc_id;
//             // $last_remaining     = $q_lastbydoc->items_remaining+$items_out;

//             // $ustok_remaining= $this->dbpurch->query(" UPDATE trn_stock_by_doc SET items_remaining='".$last_remaining."' WHERE stock_by_doc_id='".$stock_by_doc_id."' ");

// //             $insert_hist    = $this->dbpurch->query(" INSERT INTO db_bumbu_transaction.trn_stock_hist (items_id,doc_no,warehouse_id,company_id,company_name,trn_date,trn_year,trn_month,activity,qty,old_stock,current_stock)
// // VALUES
// // ('".$items_id."','".$new_doc."','".$warehouse_id."','".$company_id."','".$company_name."','".$trn_date."','".$trn_year."','".$trn_month."','".$activity."','".$items_out."','".$old_stock."','".$current_stock."') ");   

//             $insert_by_doc  = $this->dbpurch->query(" INSERT INTO db_bumbu_transaction.trn_stock_by_doc 
// (items_id,doc_no,warehouse_id,project_id,trn_date,trn_year,trn_month,activity,items_in,items_out,items_remaining, 
// old_stock,current_stock,items_price,pic_data,data_time)
// VALUES
// ('".$items_id."','".$new_doc."','".$warehouse_id."',(NULL),'".$trn_date."','".$trn_year."','".$trn_month."','".$activity."','".$items_out."','0', '0','".$old_stock."','".$current_stock."','".$items_price."','".$this->current_user['user_id']."','".dbnow()."') "); 

//         }
    }

    function act_form(){
        $new_os   = $this->session->userdata('new_os');

        $company_code   = $this->current_user['company_code'];
        $company_name   = $this->current_user['company_name'];
        $company_id     = $this->current_user['company_id'];
        $date           = $this->input->post('date');
        $wh             = $this->input->post('warehouse');
        $wh_des         = $this->input->post('warehouse_destination'); 
        $os_kind        = $this->input->post('os_kind');
        $requester      = $this->input->post('requester');
        $info           = $this->input->post('info');

        $periode        = date('Y').date('m');
        $data_id        = $this->get_id();
        $os_id          = $data_id->os_id;
        $kode           = 'OS';
        $data_no        = $this->get_nomor_dok($periode,$kode);
        $nomor_dok      = $data_no->nomor_dok;

        $company        = $this->current_user['company_code'];
        $os_no          = $kode.$company.$periode.$nomor_dok;

        $status         = 2;

        $sql_header = "INSERT INTO trn_outgoing_stock_01 (os_id,os_date,os_no,company_id,company_name,warehouse_id,dest_wh_id,os_kind,os_status,os_requester,remarks,pic_input,input_date)
                        VALUES 
            ('".$os_id."','".$date."','".$os_no."','".$company_id."','".$company_name."','".$wh."','".$wh_des."','".$os_kind."','".$status."','".$requester."','".$info."','".$this->current_user['user_id']."','".dbnow()."')";

        $query_header = $this->dbpurch->query($sql_header);

        foreach ($new_os['items'] as $key => $value) {
            $sql_detail     = "INSERT INTO trn_outgoing_stock_02 (os_id,items_id,items_name,qty,remarks) VALUES ('".$os_id."','".$value['item_id']."','".$value['item_name']."','".$value['item_qty']."','".$value['item_info']."')";
            $query_detail          = $this->dbpurch->query($sql_detail);
        }
        
        if ($query_header === false){
            return "ERROR INSERTT";
        }else{
            return $os_no; 
        }
    }

    function get_os()
    {
        $sql = 'SELECT a.*,b.warehouse_name,COALESCE(c.warehouse_name,a.project_name) tujuan_wh FROM trn_outgoing_stock_01 a 
                LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id 
                LEFT JOIN db_bumbu_master.mst_warehouse c ON a.dest_wh_id=c.warehouse_id 
                ORDER BY a.os_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_os_date($from,$to)
    {
        $sql = 'SELECT a.*,b.warehouse_name FROM trn_outgoing_stock_01 a LEFT JOIN db_bumbu_master.mst_warehouse b ON 
                    a.warehouse_id=b.warehouse_id WHERE a.os_date BETWEEN "'.$from.'" AND "'.$to.'" ORDER BY a.os_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function detail_os($id){
        $query = $this->dbpurch->query("SELECT a.os_id,a.os_date,a.os_no,a.company_id,a.company_name,a.warehouse_id,b.warehouse_name,a.dest_wh_id,COALESCE(d.warehouse_name,a.project_name) AS dest_wh,a.project_id,
            a.os_kind,a.os_status,a.os_requester,a.remarks,c.name nama_pembuat FROM trn_outgoing_stock_01 a 
        LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id
        LEFT JOIN db_bumbu_master.mst_warehouse d ON a.dest_wh_id=d.warehouse_id 
        LEFT JOIN db_bumbu_master.mst_user c ON a.pic_input=c.user_id 
        WHERE a.os_id='".$id."'")->row();
                
        return $query;
    }

    function detail_items_os($id){
        $query = $this->dbpurch->query("SELECT a.os_02_id,a.os_id,a.items_id,a.qty,a.remarks,b.items_name,b.price FROM trn_outgoing_stock_02 a LEFT JOIN db_bumbu_master.mst_items b 
                        ON a.items_id=b.items_id WHERE a.os_id='".$id."'")->result();
        return $query;
    }

    function detail_items_os_for_is($id){
        $query = $this->dbpurch->query("SELECT a.os_02_id, a.os_id, a.`items_id`, a.`items_name`, a.`qty`, a.`remarks`, b.os_no,
                (SELECT SUM(c.qty) qty FROM trn_incoming_stock_02 c, trn_incoming_stock_01 d WHERE c.is_id=d.is_id AND d.doc_ref=b.os_no AND c.items_id=a.items_id 
                GROUP BY c.items_id, c.items_name) qty_current 
                FROM trn_outgoing_stock_02 a, trn_outgoing_stock_01 b WHERE a.os_id='".$id."' AND a.`os_id`=b.`os_id`")->result();
        return $query;
    }

    function print_header($id){
        return $this->dbpurch->query("SELECT a.os_date,a.os_no,a.company_id,b.company_name,a.warehouse_id,c.warehouse_name wh_from,
            a.dest_wh_id,d.warehouse_name wh_to,a.project_id,e.project_name,a.os_kind,a.os_requester,a.remarks
            FROM trn_outgoing_stock_01 a 
            LEFT JOIN db_bumbu_master.mst_company b ON a.company_id=b.company_id
            LEFT JOIN db_bumbu_master.mst_warehouse c ON c.warehouse_id=a.warehouse_id
            LEFT JOIN db_bumbu_master.mst_warehouse d ON d.warehouse_id=a.dest_wh_id
            LEFT JOIN db_bumbu_master.mst_project e ON e.project_id=a.project_id
            WHERE a.os_id='".$id."'")->row();
    }

    // 1. Penambahan Print Outgoing Stock


}   