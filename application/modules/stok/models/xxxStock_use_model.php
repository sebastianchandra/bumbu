<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stock_use_model extends CI_Model
{
    function __construct(){
      parent::__construct();
      
      if($this->current_user['company_id']==1){
        $this->dbpurch = $this->load->database('purchasing_lm',true);
      }elseif($this->current_user['company_id']==2){
        $this->dbpurch = $this->load->database('purchasing_sg',true);
      }
      
    }

    function get_id(){
        $query = $this->dbpurch->query("SELECT IFNULL(MAX(os_id)+1,1) os_id FROM trn_outgoing_stock_01")->row();
        return $query;
    }

    function get_nomor_dok($periode,$kode){
        $query = $this->dbpurch->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(os_no,10,4))+1,4,'0'),'0001') nomor_dok, os_no FROM trn_outgoing_stock_01 
                    WHERE SUBSTRING(os_no,4,6) = '".$periode."' AND SUBSTRING(os_no,1,2)='".$kode."' ")->row();
        return $query;
    }

    function act_form(){
        $new_os   = $this->session->userdata('new_os');

        $company_code   = $this->current_user['company_code'];
        $company_name   = $this->current_user['company_name'];
        $company_id     = $this->current_user['company_id'];
        $date           = $this->input->post('date');
        $wh             = $this->input->post('warehouse');
        $wh_des         = 0;
        $dept           = $this->input->post('dept'); 
        $os_kind        = $this->input->post('os_kind');
        $requester      = $this->input->post('requester');
        $info           = $this->input->post('info');

        $periode        = date('Y').date('m');
        $data_id        = $this->get_id();
        $os_id          = $data_id->os_id;
        $kode           = 'SS';
        $data_no        = $this->get_nomor_dok($periode,$kode);
        $nomor_dok      = $data_no->nomor_dok;

        $company        = $this->current_user['company_code'];
        $os_no          = $kode.$company.$periode.$nomor_dok;

        $status         = 2;

        $sql_header = "INSERT INTO trn_outgoing_stock_01 (os_id,os_date,os_no,company_id,company_name,warehouse_id,dest_wh_id,dept,os_kind,os_status,os_requester,remarks,pic_input,input_date)
                        VALUES 
            ('".$os_id."','".$date."','".$os_no."','".$company_id."','".$company_name."','".$wh."','".$wh_des."','".$dept."','".$os_kind."','".$status."','".$requester."','".$info."','".$this->current_user['user_id']."','".dbnow()."')";
        
        $query_header = $this->dbpurch->query($sql_header);

        foreach ($new_os['items'] as $key => $value) {
            $sql_detail     = "INSERT INTO trn_outgoing_stock_02 (os_id,items_id,items_name,qty,remarks) VALUES ('".$os_id."','".$value['item_id']."','".$value['item_name']."','".$value['item_qty']."','".$value['item_info']."')";

            // test($sql_detail,0);
            $query_detail          = $this->dbpurch->query($sql_detail);
        }

        // test($sql_header,1);
        if ($query_header === false){
            return "ERROR INSERTT";
        }else{
            return $os_no; 
        }
    }

    function get_su()
    {
        $sql ='SELECT a.os_id,a.os_date,a.os_no,a.company_name,a.warehouse_id,b.warehouse_name,a.dest_wh_id,d.warehouse_name AS dest_wh,a.os_kind,a.os_status,
        a.os_requester,a.remarks,c.name dept_name FROM trn_outgoing_stock_01 a 
        LEFT JOIN db_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id
        LEFT JOIN db_master.mst_warehouse d ON a.dest_wh_id=d.warehouse_id
        LEFT JOIN db_master.mst_user_group c on a.dept=c.id_user_group WHERE a.os_kind=5 /*os_status="2"*/ order by a.os_id DESC';
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function detail_os($id){
        $query = $this->dbpurch->query("SELECT a.os_id,a.os_date,a.os_no,a.company_id,a.company_name,a.warehouse_id,b.warehouse_name,a.dest_wh_id,d.warehouse_name AS dest_wh,
            a.os_kind,a.os_status,a.os_requester,a.remarks,c.name nama_pembuat,e.name dept_name FROM trn_outgoing_stock_01 a 
        LEFT JOIN db_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id
        LEFT JOIN db_master.mst_warehouse d ON a.dest_wh_id=d.warehouse_id 
        LEFT JOIN db_master.mst_user c ON a.pic_input=c.user_id 
        LEFT JOIN db_master.mst_user_group e on a.dept=e.id_user_group
        WHERE a.os_id='".$id."'")->row();
                
        return $query;
    }

    function detail_items_os($id){
        $query = $this->dbpurch->query("SELECT a.os_02_id,a.os_id,a.items_id,a.qty,a.remarks,b.items_name FROM trn_outgoing_stock_02 a LEFT JOIN db_master.mst_items b 
                        ON a.items_id=b.items_id WHERE a.os_id='".$id."'")->result();
        return $query;
    }

    function detail_items_os_for_is($id){
        $query = $this->dbpurch->query("SELECT a.os_02_id, a.os_id, a.`items_id`, a.`items_name`, a.`qty`, a.`remarks`, b.os_no,
                (SELECT SUM(c.qty) qty FROM trn_incoming_stock_02 c, trn_incoming_stock_01 d WHERE c.is_id=d.is_id AND d.doc_ref=b.os_no AND c.items_id=a.items_id 
                GROUP BY c.items_id, c.items_name) qty_current 
                FROM trn_outgoing_stock_02 a, trn_outgoing_stock_01 b WHERE a.os_id='".$id."' AND a.`os_id`=b.`os_id`")->result();
        return $query;
    }


}   