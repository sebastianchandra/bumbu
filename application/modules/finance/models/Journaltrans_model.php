<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Journaltrans_model extends CI_Model
{
    public function __construct()
     {
        parent::__construct();
          $this->dbpurch = $this->load->database('purchasing',true);
     }

    function get_is($bulan,$tahun,$is_kind){
        $sql ="SELECT a.is_id,a.is_date as doc_date,a.is_no as doc_no,a.warehouse_id,a.company_name,a.is_status as doc_status,b.items_id,b.items_name,b.qty,b.price,SUM(b.total) total
                FROM trn_incoming_stock_01 a
                LEFT JOIN trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject' AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."' AND a.is_kind='".$is_kind."'
                GROUP BY a.is_id
                ORDER BY a.is_date ASC";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_is_act($bulan,$tahun,$is_kind){
        $sql ="SELECT a.is_id,a.is_date as doc_date,a.is_no as doc_no,a.warehouse_id,a.company_name,a.is_status as doc_status,b.items_id,b.items_name,b.qty,b.price,
                SUM(b.total) total,'' journal_dest,'' dest_id,b.items_name info
                FROM trn_incoming_stock_01 a
                LEFT JOIN trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject' AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."' AND a.is_kind='".$is_kind."'
                GROUP BY a.is_id,b.items_id
                ORDER BY a.is_date ASC";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_os($bulan,$tahun,$os_status){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                SUM(b.total),'' dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='".$os_status."' 
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id";
                // test($sql,1);
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_os_act($bulan,$tahun,$os_status){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='".$os_status."' 
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id,b.items_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_do($bulan,$tahun){
        $sql ="SELECT c.sales_id,c.sales_no,a.do_id,a.doc_ref,a.do_date AS doc_date,a.do_no AS doc_no,a.do_status AS doc_status, b.items_id,b.items_name,b.qty,d.cust_price,
                SUM(b.qty*d.cust_price) total
                FROM trn_do_01 a
                LEFT JOIN trn_do_02 b ON a.do_id=b.do_id, 
                trn_sales_01 c, trn_sales_02 d
                WHERE a.doc_ref=c.sales_no AND c.sales_id=d.sales_id AND b.items_id=d.items_id 
                AND YEAR(a.do_date)='".$tahun."' AND SUBSTR(a.do_date,6,2)='".$bulan."' AND a.do_status<>'Reject'
                GROUP BY a.do_id
                ORDER BY a.do_date ASC";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_do_act($bulan,$tahun){
        $sql ="SELECT c.sales_id,c.sales_no,a.do_id,a.doc_ref,a.do_date AS doc_date,a.do_no AS doc_no,a.do_status AS doc_status, b.items_id,b.items_name,b.qty,d.cust_price,
                SUM(b.qty*d.cust_price) total,'' journal_dest,'' dest_id,b.items_name info
                FROM trn_do_01 a
                LEFT JOIN trn_do_02 b ON a.do_id=b.do_id, 
                trn_sales_01 c, trn_sales_02 d
                WHERE a.doc_ref=c.sales_no AND c.sales_id=d.sales_id AND b.items_id=d.items_id
                AND YEAR(a.do_date)='".$tahun."' AND SUBSTR(a.do_date,6,2)='".$bulan."' AND a.do_status<>'Reject'
                GROUP BY a.do_id,b.items_id
                ORDER BY a.do_date ASC";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_ic($bulan,$tahun){
        $sql =" SELECT a.invoice_id,a.invoice_date AS doc_date,a.invoice_no AS doc_no,a.doc_status AS doc_status,a.disc_value,a.disc_percent,a.total_cust_price total,'' journal_dest,
                '' dest_id,'' info
                FROM trn_invoice_01 a WHERE doc_status IN ('New','Closed') 
                AND YEAR(a.invoice_date)='".$tahun."' AND SUBSTR(a.invoice_date,6,2)='".$bulan."'
                ORDER BY a.invoice_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_ic_act($bulan,$tahun){
        $sql =" SELECT a.invoice_id,a.invoice_date AS doc_date,a.invoice_no AS doc_no,a.doc_status AS doc_status,a.disc_value,a.disc_percent,a.total_cust_price total,'' journal_dest,
                '' dest_id,'' info
                FROM trn_invoice_01 a WHERE doc_status IN ('New','Closed') 
                AND YEAR(a.invoice_date)='".$tahun."' AND SUBSTR(a.invoice_date,6,2)='".$bulan."'
                ORDER BY a.invoice_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_ds($bulan,$tahun){
        $sql =" SELECT a.sd_id,a.sd_date AS doc_date,a.sd_no AS doc_no,a.sd_id,a.amount AS total,a.remarks,a.is_active,'Closed' AS doc_status,'' journal_dest,
                '' dest_id,'' info 
                FROM trn_so_dp a
                WHERE a.is_active='1'
                AND YEAR(a.sd_date)='".$tahun."' AND SUBSTR(a.sd_date,6,2)='".$bulan."'
                ORDER BY a.sd_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_ds_act($bulan,$tahun){
        $sql =" SELECT a.sd_id,a.sd_date AS doc_date,a.sd_no AS doc_no,a.sd_id,a.amount AS total,a.remarks,a.is_active,'Closed' AS doc_status,'' journal_dest,
                '' dest_id,'' info 
                FROM trn_so_dp a
                WHERE a.is_active='1'
                AND YEAR(a.sd_date)='".$tahun."' AND SUBSTR(a.sd_date,6,2)='".$bulan."'
                ORDER BY a.sd_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_pb($bulan,$tahun,$payment){
        if($payment=='Cash'){
            $where      = "a.payment_type='Cash'";
        }else{
            $where      = "a.payment_type<>'Cash'";
        }

        $sql =" SELECT a.pp_no AS doc_no,a.pp_date AS doc_date,a.payment_type doc_status,a.grand_total  AS total FROM trn_po_payment_01 a WHERE a.is_active='1' AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND ".$where."
                ORDER BY a.pp_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_pb_act($bulan,$tahun,$payment){
        if($payment=='Cash'){
            $where      = "a.payment_type='Cash'";
        }else{
            $where      = "a.payment_type<>'Cash'";
        }

        $sql =" SELECT a.pp_id, a.pp_no AS doc_no,a.pp_date AS doc_date,a.payment_type doc_status,a.grand_total  AS total,'Closed' AS doc_status,'' journal_dest,'' dest_id,'' info  FROM trn_po_payment_01 a WHERE a.is_active='1' AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND ".$where."
                ORDER BY a.pp_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();

    }

    function get_pi($bulan,$tahun,$payment){
        if($payment=='Cash'){
            $where      = "a.payment_type='1'";
        }else{
            $where      = "a.payment_type<>'1'";
        }

        $sql =" SELECT a.fp_no AS doc_no,a.fp_date AS doc_date,IF(a.payment_type='1','Cash','Bank') doc_status,a.grand_total  AS total FROM 
                    trn_sales_payment_01 a WHERE a.is_active='1' AND YEAR(a.fp_date)='".$tahun."' 
                    AND SUBSTR(a.fp_date,6,2)='".$bulan."' AND ".$where."
                ORDER BY a.fp_date ASC  ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_pi_act($bulan,$tahun,$payment){
        if($payment=='Cash'){
            $where      = "a.payment_type='1'";
        }else{
            $where      = "a.payment_type<>'1'";
        }

        $sql =" SELECT a.fp_id,a.fp_no AS doc_no,a.fp_date AS doc_date,a.grand_total AS total,a.is_active,'Closed' AS doc_status,'' journal_dest,'' dest_id,'' info 
                FROM trn_sales_payment_01 a
                WHERE a.is_active='1'
                AND YEAR(a.fp_date)='".$tahun."' AND SUBSTR(a.fp_date,6,2)='".$bulan."' AND ".$where."
                ORDER BY a.fp_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }


    

    

    

    function get_sp($bulan,$tahun){
        $sql =" SELECT a.fp_id,a.fp_no AS doc_no,a.fp_date AS doc_date,a.grand_total AS total,a.is_active,'Closed' AS doc_status,'' journal_dest,'' dest_id,'' info 
                FROM trn_sales_payment_01 a
                WHERE a.is_active='1'
                AND YEAR(a.fp_date)='".$tahun."' AND SUBSTR(a.fp_date,6,2)='".$bulan."'
                ORDER BY a.fp_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    function get_sp_act($bulan,$tahun){
        $sql =" SELECT a.fp_id,a.fp_no AS doc_no,a.fp_date AS doc_date,a.grand_total AS total,a.is_active,'Closed' AS doc_status,'' journal_dest,'' dest_id,'' info 
                FROM trn_sales_payment_01 a
                WHERE a.is_active='1'
                AND YEAR(a.fp_date)='".$tahun."' AND SUBSTR(a.fp_date,6,2)='".$bulan."'
                ORDER BY a.fp_date ASC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    

    







































    function get_po($bulan,$tahun){
        $sql ="SELECT a.po_id doc_id,a.po_no doc_no,a.po_date doc_date,a.po_status doc_status,a.total total,a.total total,a.project_id dest_id,
                a.project_name journal_dest
                FROM db_bumbu_transaction.trn_po_01 a LEFT JOIN db_bumbu_master.mst_project b ON a.project_id=b.project_id 
                WHERE a.po_status<>'Reject' AND YEAR(a.po_date)='".$tahun."' AND SUBSTR(a.po_date,6,2)='".$bulan."'
                ORDER BY a.po_no ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_po_act($bulan,$tahun){
        $sql ="SELECT a.po_id doc_id,a.po_no doc_no,a.po_date doc_date,a.po_status doc_status,a.total total,a.total total,
                a.project_id dest_id,a.project_name journal_dest,c.subtotal total,c.items_name info
                FROM db_bumbu_transaction.trn_po_01 a 
                LEFT JOIN db_bumbu_master.mst_project b ON a.project_id=b.project_id 
                LEFT JOIN db_bumbu_transaction.trn_po_02 c ON a.po_id=c.po_id
                WHERE a.po_status<>'Reject' AND YEAR(a.po_date)='".$tahun."' AND SUBSTR(a.po_date,6,2)='".$bulan."'
                ORDER BY a.po_no ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_so($bulan,$tahun){
        $sql ="SELECT a.sales_id doc_id,a.sales_no doc_no,a.sales_date doc_date,a.doc_status doc_status,
SUM(a.total_owner_price+a.total_cust_price+a.total_gov_price) total,a.project_id dest_id,a.project_name journal_dest
                FROM db_omzet_shp.trn_sales_01 a 
                WHERE a.doc_status<>'Reject' AND YEAR(a.sales_date)='".$tahun."' AND SUBSTR(a.sales_date,6,2)='".$bulan."'
                GROUP BY a.sales_id
                ORDER BY a.sales_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_so_act($bulan,$tahun){
        $sql ="SELECT a.sales_id doc_id,a.sales_no doc_no,a.sales_date doc_date,a.doc_status doc_status,
SUM(a.total_owner_price+a.total_cust_price+a.total_gov_price) total_header,a.project_id dest_id,a.project_name journal_dest,
b.subtotal total,b.fish_name info
                FROM db_omzet_shp.trn_sales_01 a LEFT JOIN db_omzet_shp.trn_sales_02 b ON a.sales_id=b.sales_id
                WHERE a.doc_status<>'Reject' AND YEAR(a.sales_date)='".$tahun."' AND SUBSTR(a.sales_date,6,2)='".$bulan."'
                GROUP BY a.sales_id,b.fish_id
                ORDER BY a.sales_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_oso($bulan,$tahun){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                SUM(b.total),'' dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='Others'
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_oso_act($bulan,$tahun){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='Others'
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id,b.items_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_osp($bulan,$tahun){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                SUM(b.total),'' dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='Transfer' AND a.project_id IS NOT NULL
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_osp_act($bulan,$tahun){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='Transfer' AND a.project_id IS NOT NULL
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id,b.items_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_osa($bulan,$tahun){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                SUM(b.total),'' dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='Adjustment'
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_osa_act($bulan,$tahun){
        $sql ="SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info
                FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                WHERE a.os_status<>'Reject' AND a.os_kind='Adjustment'
                AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                GROUP BY a.os_id,b.items_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    

    

    function get_isp($bulan,$tahun){
        $sql ="SELECT a.is_id doc_id,a.is_date doc_date,a.is_no doc_no,IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,SUM(b.total) total,is_status doc_status
                FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject' AND a.project_id<>0 AND a.is_kind='1'
                AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                GROUP BY a.is_id
                ORDER BY a.is_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_isp_act($bulan,$tahun){
        $sql ="SELECT a.is_id doc_id,a.is_date doc_date,a.is_no doc_no,IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,SUM(b.total) total,is_status doc_status,b.items_name info
                FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject' AND a.project_id<>0 AND a.is_kind='1'
                AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                GROUP BY a.is_id,b.items_id
                ORDER BY a.is_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_isa($bulan,$tahun){
        $sql ="SELECT a.is_id doc_id,a.is_date doc_date,a.is_no doc_no,IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,SUM(b.total) total,is_status doc_status
                FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject'  AND a.is_kind='3'
                AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                GROUP BY a.is_id
                ORDER BY a.is_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_isa_act($bulan,$tahun){
        $sql ="SELECT a.is_id doc_id,a.is_date doc_date,a.is_no doc_no,IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,SUM(b.total) total,is_status doc_status,b.items_name info
                FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject'  AND a.is_kind='3'
                AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                GROUP BY a.is_id,b.items_id
                ORDER BY a.is_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_isi($bulan,$tahun){
        $sql ="SELECT a.is_id doc_id,a.is_date doc_date,a.is_no doc_no,IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,SUM(b.total) total,is_status doc_status
                FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject' AND a.warehouse_id<>0 AND a.is_kind='1'
                AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                GROUP BY a.is_id
                ORDER BY a.is_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_isi_act($bulan,$tahun){
        $sql ="SELECT a.is_id doc_id,a.is_date doc_date,a.is_no doc_no,IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,SUM(b.total) total,is_status doc_status,b.items_name info
                FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                WHERE a.is_status<>'Reject' AND a.warehouse_id<>0 AND a.is_kind='1'
                AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                GROUP BY a.is_id,b.items_id
                ORDER BY a.is_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pm($bulan,$tahun){
        $sql ="SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total total
                ,''dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_po_payment_01 a
                WHERE a.is_active='1'
                AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type NOT IN ('Giro','Cheque')
                GROUP BY a.pp_id
                ORDER BY a.pp_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pm_act($bulan,$tahun){
        $sql ="SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total 
                ,''dest_id,'' journal_dest,b.po_no info,b.total
                FROM db_bumbu_transaction.trn_po_payment_01 a LEFT JOIN db_bumbu_transaction.trn_po_payment_02 b ON b.pp_id=a.pp_id
                WHERE a.is_active='1'
                AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type NOT IN ('Giro','Cheque')
                GROUP BY a.pp_id,b.pp_02_id
                ORDER BY a.pp_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pmc($bulan,$tahun){
        $sql ="SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total total
                ,''dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_po_payment_01 a
                WHERE a.is_active='1'
                AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Cash'
                GROUP BY a.pp_id
                ORDER BY a.pp_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pmc_act($bulan,$tahun){
        $sql ="SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total 
                ,''dest_id,'' journal_dest,b.po_no info,b.total
                FROM db_bumbu_transaction.trn_po_payment_01 a LEFT JOIN db_bumbu_transaction.trn_po_payment_02 b ON b.pp_id=a.pp_id
                WHERE a.is_active='1'
                AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Cash'
                GROUP BY a.pp_id,b.pp_02_id
                ORDER BY a.pp_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pmb($bulan,$tahun){
        $sql ="SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total total
                ,''dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_po_payment_01 a
                WHERE a.is_active='1'
                AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Transfer'
                GROUP BY a.pp_id
                ORDER BY a.pp_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pmb_act($bulan,$tahun){
        $sql ="SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total 
                ,''dest_id,'' journal_dest,b.po_no info,b.total
                FROM db_bumbu_transaction.trn_po_payment_01 a LEFT JOIN db_bumbu_transaction.trn_po_payment_02 b ON b.pp_id=a.pp_id
                WHERE a.is_active='1'
                AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Transfer'
                GROUP BY a.pp_id,b.pp_02_id
                ORDER BY a.pp_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_pd($bulan,$tahun){
        $sql ="SELECT a.pd_id doc_id,a.pd_no doc_no,a.pd_date doc_date,a.amount total, '' doc_status,''dest_id,'' journal_dest
                FROM db_bumbu_transaction.trn_po_dp a
                WHERE a.is_active='1'
                AND YEAR(a.pd_date)='".$tahun."' AND SUBSTR(a.pd_date,6,2)='".$bulan."'
                ORDER BY a.pd_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_lc($bulan,$tahun){
        $sql ="SELECT cl_id doc_id,cl_id doc_no,cl_date doc_date,amount total,cl_status doc_status,''dest_id,'' journal_dest FROM 
                db_loan_shp.trn_crew_loan WHERE is_active='1'
                AND YEAR(cl_date)='".$tahun."' AND SUBSTR(cl_date,6,2)='".$bulan."'
                ORDER BY cl_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_lp($bulan,$tahun){
        $sql ="SELECT cl_id doc_id,cl_id doc_no,cl_date doc_date,amount total,cl_status doc_status,project_id dest_id,
                project_name journal_dest FROM 
                db_loan_shp.trn_project_loan WHERE is_active='1' 
                AND YEAR(cl_date)='".$tahun."' AND SUBSTR(cl_date,6,2)='".$bulan."'
                ORDER BY cl_date ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function config_journal($trn_code){
        $sql ="SELECT db_coa_id,db_coa_code,db_coa_detail,db_coa_name,cr_coa_id,cr_coa_code,cr_coa_detail,cr_coa_name FROM mst_journal_config 
                WHERE trn_code='".$trn_code."'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function config_journal_num($trn_code){
        $sql ="SELECT db_coa_id,db_coa_code,db_coa_detail,db_coa_name,cr_coa_id,cr_coa_code,cr_coa_detail,cr_coa_name FROM mst_journal_config 
                WHERE trn_code='".$trn_code."'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function query_all($bulan,$tahun){
        $sql ="
            SELECT a.po_id doc_id,a.po_no doc_no,a.po_date doc_date,a.po_status doc_status,a.total total,a.project_id dest_id,
                a.project_name journal_dest
                FROM db_bumbu_transaction.trn_po_01 a LEFT JOIN db_bumbu_master.mst_project b ON a.project_id=b.project_id 
                WHERE a.po_status<>'Reject' AND YEAR(a.po_date)='".$tahun."' AND SUBSTR(a.po_date,6,2)='".$bulan."'

        UNION SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total total
                    ,''dest_id,'' journal_dest
                    FROM db_bumbu_transaction.trn_po_payment_01 a
                    WHERE a.is_active='1'
                    AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Cash'
                    GROUP BY a.pp_id

        UNION SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,a.grand_total total
                    ,''dest_id,'' journal_dest
                    FROM db_bumbu_transaction.trn_po_payment_01 a
                    WHERE a.is_active='1'
                    AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Transfer'
                    GROUP BY a.pp_id

        UNION SELECT a.pd_id doc_id,a.pd_no doc_no,a.pd_date doc_date, '' doc_status,a.amount total,''dest_id,'' journal_dest
                    FROM db_bumbu_transaction.trn_po_dp a
                    WHERE a.is_active='1'
                    AND YEAR(a.pd_date)='".$tahun."' AND SUBSTR(a.pd_date,6,2)='".$bulan."'

        UNION SELECT cl_id doc_id,cl_id doc_no,cl_date doc_date,cl_status doc_status,amount total,''dest_id,'' journal_dest FROM 
                    db_loan_shp.trn_crew_loan WHERE is_active='1'
                    AND YEAR(cl_date)='".$tahun."' AND SUBSTR(cl_date,6,2)='".$bulan."'

        UNION SELECT cl_id doc_id,cl_id doc_no,cl_date doc_date,cl_status doc_status,amount total,project_id dest_id,
                    project_name journal_dest FROM 
                    db_loan_shp.trn_project_loan WHERE is_active='1' 
                    AND YEAR(cl_date)='".$tahun."' AND SUBSTR(cl_date,6,2)='".$bulan."'

        UNION SELECT a.is_id doc_id,a.is_no doc_no,a.is_date doc_date,is_status doc_status,SUM(b.total) total,
                    IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                    IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest
                    FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                    WHERE a.is_status<>'Reject' AND a.warehouse_id<>0 AND a.is_kind='1'
                    AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                    GROUP BY a.is_id

        UNION SELECT a.is_id doc_id,a.is_no doc_no,a.is_date doc_date,is_status doc_status,SUM(b.total) total,
                    IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                    IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest
                    FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                    WHERE a.is_status<>'Reject' AND a.project_id<>0 AND a.is_kind='1'
                    AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                    GROUP BY a.is_id

        UNION SELECT a.is_id doc_id,a.is_no doc_no,a.is_date doc_date,is_status doc_status,SUM(b.total) total,
                    IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                    IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest
                    FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                    WHERE a.is_status<>'Reject'  AND a.is_kind='3'
                    AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                    GROUP BY a.is_id

        UNION SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                    '' dest_id,'' journal_dest
                    FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                    LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                    WHERE a.os_status<>'Reject' AND a.os_kind='Others'
                    AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                    GROUP BY a.os_id
        UNION SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                    '' dest_id,'' journal_dest
                    FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                    LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                    WHERE a.os_status<>'Reject' AND a.os_kind='Transfer' AND a.project_id IS NOT NULL
                    AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                    GROUP BY a.os_id
        UNION SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                    '' dest_id,'' journal_dest
                    FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                    LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                    WHERE a.os_status<>'Reject' AND a.os_kind='Adjustment'
                    AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                    GROUP BY a.os_id
        UNION SELECT a.sales_id doc_id,a.sales_no doc_no,a.sales_date doc_date,a.doc_status doc_status,
                    SUM(a.total_owner_price+a.total_cust_price+a.total_gov_price) total,a.project_id dest_id,a.project_name journal_dest
                    FROM db_omzet_shp.trn_sales_01 a 
                    WHERE a.doc_status<>'Reject' AND YEAR(a.sales_date)='".$tahun."' AND SUBSTR(a.sales_date,6,2)='".$bulan."'
                    GROUP BY a.sales_id
        UNION SELECT a.fp_id doc_id,a.fp_no doc_no,a.fp_date doc_date,'' doc_status,a.grand_total total,'' dest_id,'' journal_dest
                    FROM db_omzet_shp.trn_fish_payment_01 a
                    WHERE YEAR(a.fp_date)='".$tahun."' AND SUBSTR(a.fp_date,6,2)='".$bulan."'
        "; 
    $query = $this->db->query($sql);
    return $query->result();
    }

    function query_all_act($bulan,$tahun){
        $sql    = "
        SELECT a.po_id doc_id,a.po_no doc_no,a.po_date doc_date,a.po_status doc_status,c.subtotal total,
                a.project_id dest_id,a.project_name journal_dest,c.items_name info,'PO' trn_code
                FROM db_bumbu_transaction.trn_po_01 a 
                LEFT JOIN db_bumbu_master.mst_project b ON a.project_id=b.project_id 
                LEFT JOIN db_bumbu_transaction.trn_po_02 c ON a.po_id=c.po_id
                WHERE a.po_status<>'Reject' AND YEAR(a.po_date)='".$tahun."' AND SUBSTR(a.po_date,6,2)='".$bulan."'
        UNION
        SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,b.total, 
                        ''dest_id,'' journal_dest,b.po_no info,'PMC' trn_code
                        FROM db_bumbu_transaction.trn_po_payment_01 a LEFT JOIN db_bumbu_transaction.trn_po_payment_02 b ON b.pp_id=a.pp_id
                        WHERE a.is_active='1'
                        AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Cash'
                        GROUP BY a.pp_id,b.pp_02_id
        UNION
        SELECT a.pp_id doc_id,a.pp_no doc_no,a.pp_date doc_date,a.payment_type doc_status,b.total 
                        ,''dest_id,'' journal_dest,b.po_no info,'PMB' trn_code
                        FROM db_bumbu_transaction.trn_po_payment_01 a LEFT JOIN db_bumbu_transaction.trn_po_payment_02 b ON b.pp_id=a.pp_id
                        WHERE a.is_active='1'
                        AND YEAR(a.pp_date)='".$tahun."' AND SUBSTR(a.pp_date,6,2)='".$bulan."' AND a.payment_type='Transfer'
                        GROUP BY a.pp_id,b.pp_02_id
        UNION
        SELECT a.pd_id doc_id,a.pd_no doc_no,a.pd_date doc_date, '' doc_status,a.amount total,''dest_id,'' journal_dest,
                        a.pd_no info,'PD' trn_code
                        FROM db_bumbu_transaction.trn_po_dp a
                        WHERE a.is_active='1'
                        AND YEAR(a.pd_date)='".$tahun."' AND SUBSTR(a.pd_date,6,2)='".$bulan."'
        UNION
        SELECT cl_id doc_id,cl_id doc_no,cl_date doc_date,cl_status doc_status,amount total,''dest_id,'' journal_dest,
                        cl_id info,'LC' trn_code
                        FROM 
                        db_loan_shp.trn_crew_loan WHERE is_active='1'
                        AND YEAR(cl_date)='".$tahun."' AND SUBSTR(cl_date,6,2)='".$bulan."'
        UNION
        SELECT cl_id doc_id,cl_id doc_no,cl_date doc_date,cl_status doc_status,amount total,project_id dest_id,
                        project_name journal_dest,cl_id info,'LP' trn_code
                        FROM 
                        db_loan_shp.trn_project_loan WHERE is_active='1' 
                        AND YEAR(cl_date)='".$tahun."' AND SUBSTR(cl_date,6,2)='".$bulan."'
        UNION
        SELECT a.is_id doc_id,a.is_no doc_no,a.is_date doc_date,is_status doc_status,SUM(b.total) total,
                        IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                        IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,b.items_name info,'ISI' trn_code
        FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                        WHERE a.is_status<>'Reject' AND a.warehouse_id<>0 AND a.is_kind='1'
                        AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                        GROUP BY a.is_id,b.items_id
        UNION
        SELECT a.is_id doc_id,a.is_no doc_no,a.is_date doc_date,is_status doc_status,SUM(b.total) total,
                        IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                        IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,b.items_name info,'ISP' trn_code
        FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                        WHERE a.is_status<>'Reject' AND a.project_id<>0 AND a.is_kind='1'
                        AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                        GROUP BY a.is_id,b.items_id
        UNION
        SELECT a.is_id doc_id,a.is_no doc_no,a.is_date doc_date,is_status doc_status,SUM(b.total) total,
                        IF(a.warehouse_id=0, a.project_id,a.warehouse_id) dest_id,
                        IF(a.warehouse_id=0, a.project_name, a.warehouse_id) journal_dest,b.items_name info,'ISA' trn_code
        FROM db_bumbu_transaction.trn_incoming_stock_01 a LEFT JOIN db_bumbu_transaction.trn_incoming_stock_02 b ON a.is_id=b.is_id
                        WHERE a.is_status<>'Reject'  AND a.is_kind='3'
                        AND YEAR(a.is_date)='".$tahun."' AND SUBSTR(a.is_date,6,2)='".$bulan."'
                        GROUP BY a.is_id,b.items_id
        UNION
        SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                        /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info,'OSO' trn_code
                        FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                        LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                        WHERE a.os_status<>'Reject' AND a.os_kind='Others'
                        AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                        GROUP BY a.os_id,b.items_id
        UNION
        SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                        /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info,'OSP' trn_code
                        FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                        LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                        WHERE a.os_status<>'Reject' AND a.os_kind='Transfer' AND a.project_id IS NOT NULL
                        AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                        GROUP BY a.os_id,b.items_id
        UNION
        SELECT a.os_id doc_id,a.os_no doc_no,a.os_date doc_date,a.os_status doc_status,SUM(b.qty*b.price)total,
                        /*SUM(b.total),*/'' dest_id,'' journal_dest,b.items_name info,'OSA' trn_code
                        FROM db_bumbu_transaction.trn_outgoing_stock_01 a
                        LEFT JOIN db_bumbu_transaction.trn_outgoing_stock_02 b ON a.os_id=b.os_id
                        WHERE a.os_status<>'Reject' AND a.os_kind='Adjustment'
                        AND YEAR(a.os_date)='".$tahun."' AND SUBSTR(a.os_date,6,2)='".$bulan."'
                        GROUP BY a.os_id,b.items_id
        UNION
        SELECT a.sales_id doc_id,a.sales_no doc_no,a.sales_date doc_date,a.doc_status doc_status,b.subtotal total,
                        a.project_id dest_id,a.project_name journal_dest,b.fish_name info,'SL' trn_code
                        FROM db_omzet_shp.trn_sales_01 a LEFT JOIN db_omzet_shp.trn_sales_02 b ON a.sales_id=b.sales_id
                        WHERE a.doc_status<>'Reject' AND YEAR(a.sales_date)='".$tahun."' AND SUBSTR(a.sales_date,6,2)='".$bulan."'
                        GROUP BY a.sales_id,b.fish_id
        UNION
        SELECT a.fp_id doc_id,a.fp_no doc_no,a.fp_date doc_date,'' doc_status,b.total,'' dest_id,'' journal_dest,
                        b.sales_no info,'SP' trn_code
                        FROM db_omzet_shp.trn_fish_payment_01 a LEFT JOIN db_omzet_shp.trn_fish_payment_02 b ON a.fp_id=b.fp_id
                        WHERE YEAR(a.fp_date)='".$tahun."' AND SUBSTR(a.fp_date,6,2)='".$bulan."'
                ";
        // test($sql,1);
        $query = $this->db->query($sql);
        return $query->result();
    }

}   