<?php
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Jul 28, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Closing_journal extends MY_Controller
{
     public function __construct()
     {
          parent::__construct();
          $this->session->set_userdata('ses_menu', array('active_menu' => 'Finance', 'active_submenu' => 'finance/closing_journal')); 
          $this->isMenu();
          $this->load->helper('security');
          $this->load->model('finance/closing_journal_model');
     }

     // CREATE TABLE `db_fa_shp`.`trn_closing_journal`( `closing_journal_id` INT(11) COMMENT 'Id Closing', `month_periode` VARCHAR(2) COMMENT 'Bulan Closing', `year_periode` YEAR COMMENT 'Tahun Closing', `is_active` INT(1) DEFAULT 1 COMMENT '1 = Closing , 0 = Open', `pic_closing` INT(4), `time_closing` DATETIME ); 

     // ALTER TABLE `db_fa_shp`.`trn_closing_journal` CHANGE `closing_journal_id` `closing_journal_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Closing', ADD PRIMARY KEY (`closing_journal_id`);

     // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('FIN', 'Closing Journal', 'finance/closing_journal', 'fa fa-bar-chart-o', 'Finance', '2', '1');

     function index(){
          $data['data_pa']        = $this->closing_journal_model->get_cj();
          $this->template->load('body', 'finance/closing_journal/cj_view',$data);
     }

     function form(){
          $this->template->load('body', 'finance/closing_journal/cj_form');
     }

     public function ins()
     {
          $cek           = $this->closing_journal_model->cek_data($_POST['month_periode'],$_POST['year_periode']);
          if($cek>=1){
               jsout(array('success' => true, 'status' => 'ada' ));
          }else{
               $this->closing_journal_model->setMonthPeriode($this->security->xss_clean($_POST['month_periode']));
               $this->closing_journal_model->setYearPeriode($this->security->xss_clean($_POST['year_periode']));
               $this->closing_journal_model->setIsActive($this->security->xss_clean(1));
               $this->closing_journal_model->setPicClosing($this->security->xss_clean($this->current_user['user_id']));
               $this->closing_journal_model->setTimeClosing($this->security->xss_clean(dbnow()));
               $save    = $this->closing_journal_model->insert();
               jsout(array('success' => true, 'status' => $save ));
          }
     }

     public function upd()
     {
          $id = $this->security->xss_clean($_POST['closingJournalId']);
          $this->closing_journal_model->getObjectById($id);
          $this->closing_journal_model->setClosingJournalId($this->security->xss_clean($_POST['closingJournalId']));
          $this->closing_journal_model->setMonthPeriode($this->security->xss_clean($_POST['monthPeriode']));
          $this->closing_journal_model->setYearPeriode($this->security->xss_clean($_POST['yearPeriode']));
          $this->closing_journal_model->setIsActive($this->security->xss_clean($_POST['isActive']));
          $this->closing_journal_model->setPicClosing($this->security->xss_clean($_POST['picClosing']));
          $this->closing_journal_model->setTimeClosing($this->security->xss_clean($_POST['timeClosing']));
          $this->closing_journal_model->update($id);
     }

     public function del()
     {
          $id = $this->security->xss_clean($_POST['closingJournalId']);
          $this->closing_journal_model->delete($id);
     }

     public function loadAll()
     {
          $rs = $this->closing_journal_model->getAll();
          echo json_encode($rs); 
     }

     function open_js(){
        $delete = $this->closing_journal_model->act_delete_js($this->input->post('closing_journal_id'),0);
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
     }

     function close_js(){
        $delete = $this->closing_journal_model->act_delete_js($this->input->post('closing_journal_id'),1);
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
     }
     
}
?>
