<?php
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Jul 28, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_journal extends MY_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
          parent::__construct();
          $this->session->set_userdata('ses_menu', array('active_menu' => 'Finance', 'active_submenu' => 'finance/payment_journal')); 
          $this->isMenu();
          $this->load->helper('security');
          $this->load->model('finance/journal_model');
          $this->load->model('master/config_journal_model');
          $this->load->model('finance/journaltrans_model');
          $this->load->model('finance/closing_journal_model');
     }
     /* END CONSTRUCTOR */

     // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('FIN', 'Payment (Invoice / PO) Journal', 'finance/payment_journal', 'fa fa-laptop', 'Finance', '5', '1');

     function index(){
          $data['data_pa']        = $this->journal_model->get_data_payment();
          $data['data_journal']   = $this->config_journal_model->get_data();
          $this->template->load('body', 'finance/payment_journal/pj_view',$data);
     }

     function cetak($id){
          $data['detail']          = $this->journal_model->getByIdRow($id);
          $data['status']          = 'html';
          $this->load->view('finance/payment_journal/pj_cetak',$data);
     }

}
?>
