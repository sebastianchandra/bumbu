<?php
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Jul 28, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
defined('BASEPATH') OR exit('No direct script access allowed');
class Assets_journal extends MY_Controller
{
     /* START CONSTRUCTOR */
     public function __construct()
     {
          parent::__construct();
          $this->session->set_userdata('ses_menu', array('active_menu' => 'Finance', 'active_submenu' => 'finance/assets_journal')); 
          $this->isMenu();
          $this->load->helper('security');
          $this->load->model('finance/journal_model');
          $this->load->model('master/config_journal_model');
          $this->load->model('finance/journaltrans_model');
          $this->load->model('finance/closing_journal_model');
     }
     /* END CONSTRUCTOR */

     // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('FIN', 'Bank / Cash Journal', 'finance/asset_journal', 'fa fa-bar-chart-o', 'Finance', '4', '1');

     function index(){
          $data['data_pa']        = $this->journal_model->get_data_manual(3);
          $data['data_journal']   = $this->config_journal_model->get_data();
          $this->template->load('body', 'finance/assets_journal/aj_view',$data);
     }

     function form(){
          $this->template->load('body', 'finance/assets_journal/aj_form');     
     }

     function form_act(){
          $tahun              = substr($this->input->post('journal_date'),0,4);
          $bulan              = substr($this->input->post('journal_date'),5,2);
          $cek_closing        = $this->closing_journal_model->cek_data($bulan,$tahun);

          $trn_code           = $this->input->post('trn_code');

          if($trn_code=='BM'){
               if($this->input->post('db_coa_id')=='176'){
                    $kode          = 'KM';
               }else{
                    $kode          = 'BM';
               }
          }else{
               if($this->input->post('cr_coa_id')=='176'){
                    $kode          = 'KK';
               }else{
                    $kode          = 'BK';
               }
          }

          if($cek_closing>=1){
               jsout(array('success' => 'closing', 'status' => 'Periode Bulan '.$bulan.' Tahun '.$tahun.' Sudah Di Closing'));
          }else{

               $getDataCount            = $tahun.$bulan.$this->journal_model->getNoDoc($tahun,$bulan);
               $getUrut                 = $kode.'/'.$tahun.'/'.$bulan.'/'.$this->journal_model->getNourut($tahun,$bulan,$kode);
               $this->journal_model->setJournalId($this->security->xss_clean($getDataCount));
               $this->journal_model->setTrnCode($this->security->xss_clean($this->input->post('trn_code')));
               $this->journal_model->setJournalDate($this->security->xss_clean($this->input->post('journal_date')));
               $this->journal_model->setYearPeriod($this->security->xss_clean($tahun));
               $this->journal_model->setMonthPeriod($this->security->xss_clean($bulan));
               $this->journal_model->setJournalDest($this->security->xss_clean(''));
               $this->journal_model->setDestId($this->security->xss_clean(''));
               $this->journal_model->setTrnId($this->security->xss_clean($getUrut));
               $this->journal_model->setDbCoaId($this->security->xss_clean($this->input->post('db_coa_id')));
               $this->journal_model->setDbCoaDetail($this->security->xss_clean($this->input->post('db_coa_detail')));
               $this->journal_model->setDbCoaName($this->security->xss_clean($this->input->post('db_coa_name')));
               $this->journal_model->setDbValue($this->security->xss_clean($this->input->post('mj_value')));
               $this->journal_model->setDbInfo($this->security->xss_clean($this->input->post('db_info')));
               $this->journal_model->setCrCoaId($this->security->xss_clean($this->input->post('cr_coa_id')));
               $this->journal_model->setCrCoaDetail($this->security->xss_clean($this->input->post('cr_coa_detail')));
               $this->journal_model->setCrCoaName($this->security->xss_clean($this->input->post('cr_coa_name')));
               $this->journal_model->setCrValue($this->security->xss_clean($this->input->post('mj_value')));
               $this->journal_model->setCrInfo($this->security->xss_clean($this->input->post('cr_info')));
               $this->journal_model->setPicPosting($this->security->xss_clean($this->current_user['user_id']));
               $save     = $this->journal_model->insert_manual(3);

               jsout(array('success' => 'true', 'status' => $save));

          }
     }

     function edit($id){
          $data['detail']          = $this->journal_model->getByIdRow($id);
          $this->template->load('body', 'finance/assets_journal/aj_edit',$data);     
     }

     /* START INSERT */
     public function ins()
     {
          $this->journal_model->setJournalId($this->security->xss_clean($_POST['journalId']));
          $this->journal_model->setTrnCode($this->security->xss_clean($_POST['trnCode']));
          $this->journal_model->setJournalDate($this->security->xss_clean($_POST['journalDate']));
          $this->journal_model->setYearPeriod($this->security->xss_clean($_POST['yearPeriod']));
          $this->journal_model->setMonthPeriod($this->security->xss_clean($_POST['monthPeriod']));
          $this->journal_model->setJournalDest($this->security->xss_clean($_POST['journalDest']));
          $this->journal_model->setDestId($this->security->xss_clean($_POST['destId']));
          $this->journal_model->setTrnId($this->security->xss_clean($_POST['trnId']));
          $this->journal_model->setDbCoaId($this->security->xss_clean($_POST['dbCoaId']));
          $this->journal_model->setDbCoaDetail($this->security->xss_clean($_POST['dbCoaDetail']));
          $this->journal_model->setDbCoaName($this->security->xss_clean($_POST['dbCoaName']));
          $this->journal_model->setDbValue($this->security->xss_clean($_POST['dbValue']));
          $this->journal_model->setCrCoaId($this->security->xss_clean($_POST['crCoaId']));
          $this->journal_model->setCrCoaDetail($this->security->xss_clean($_POST['crCoaDetail']));
          $this->journal_model->setCrCoaName($this->security->xss_clean($_POST['crCoaName']));
          $this->journal_model->setCrValue($this->security->xss_clean($_POST['crValue']));
          $this->journal_model->setPicPosting($this->security->xss_clean($_POST['picPosting']));
          $this->journal_model->insert();
     }
     /* END INSERT */
     /* START UPDATE */
     public function upd()
     {
          $tahun              = substr($this->input->post('journal_date'),0,4);
          $bulan              = substr($this->input->post('journal_date'),5,2);
          $cek_closing        = $this->closing_journal_model->cek_data($bulan,$tahun);

          if($cek_closing>=1){
               jsout(array('success' => 'closing', 'status' => 'Periode Bulan '.$bulan.' Tahun '.$tahun.' Sudah Di Closing'));
          }else{
               $id = $this->security->xss_clean($_POST['journal_id']);
               // $this->journal_model->getObjectById($id);
               // $this->journal_model->setJournalId($this->security->xss_clean($_POST['journalId']));
               $this->journal_model->setTrnCode($this->security->xss_clean($this->input->post('trn_code')));
               $this->journal_model->setJournalDate($this->security->xss_clean($this->input->post('journal_date')));
               $this->journal_model->setYearPeriod($this->security->xss_clean($tahun));
               $this->journal_model->setMonthPeriod($this->security->xss_clean($bulan));
               $this->journal_model->setJournalDest($this->security->xss_clean(''));
               $this->journal_model->setDestId($this->security->xss_clean(''));
               $this->journal_model->setTrnId($this->security->xss_clean(''));
               $this->journal_model->setDbCoaId($this->security->xss_clean($this->input->post('db_coa_id')));
               $this->journal_model->setDbCoaDetail($this->security->xss_clean($this->input->post('db_coa_detail')));
               $this->journal_model->setDbCoaName($this->security->xss_clean($this->input->post('db_coa_name')));
               $this->journal_model->setDbValue($this->security->xss_clean($this->input->post('mj_value')));
               $this->journal_model->setDbInfo($this->security->xss_clean($this->input->post('db_info')));
               $this->journal_model->setCrCoaId($this->security->xss_clean($this->input->post('cr_coa_id')));
               $this->journal_model->setCrCoaDetail($this->security->xss_clean($this->input->post('cr_coa_detail')));
               $this->journal_model->setCrCoaName($this->security->xss_clean($this->input->post('cr_coa_name')));
               $this->journal_model->setCrValue($this->security->xss_clean($this->input->post('mj_value')));
               $this->journal_model->setCrInfo($this->security->xss_clean($this->input->post('cr_info')));
               $this->journal_model->setPicPosting($this->security->xss_clean($this->current_user['user_id']));
               $save          = $this->journal_model->update($id);

               jsout(array('success' => 'true', 'status' => $save));

          }
     }
     /* END UPDATE */
     /* START DELETE */
     public function del()
     {
          $id = $this->security->xss_clean($_POST['journalId']);
          $this->journal_model->delete($id);
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
          $rs = $this->journal_model->getAll();
          echo json_encode($rs); 
     }
     /* END GET ALL */

     function delete_js(){
          $data          = $this->journal_model->getByIdRow($this->input->post('journal_id'));
          $cek_closing        = $this->closing_journal_model->cek_data($data->month_period,$data->year_period);
          if($cek_closing>=1){
               jsout(array('success' => 'closing', 'status' => 'Periode Bulan '.$data->month_period.' Tahun '.$data->year_period.' Sudah Di Closing'));
          }else{
               $delete = $this->journal_model->act_delete_js($this->input->post('journal_id'));
               jsout(array('success' => true, 'status' => $delete ));
          }
     }

     function cetak($id){
          $data['detail']          = $this->journal_model->getByIdRow($id);
          $data['status']          = 'html';
          $this->load->view('finance/assets_journal/aj_cetak',$data);
     }

}
?>
