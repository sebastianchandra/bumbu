<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Closing Journal</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Closing Journal</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Bulan Closing</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id="month_periode">
                                    <option value="01">Januari</option>
                                    <option value="02">Febuari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Tahun Closing</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id="year_periode">
                                    <?php 
                                    $x      = (int)date('Y');
                                    $y      = $x-1;

                                    for ($i=$y; $i <= $x; $i++) { 
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                       
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/fish'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("#month_periode").select2();
$("#year_periode").select2();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#month_periode').val()){
            toastr.error("<b>Bulan Closing</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#month_periode').focus();
            return false;
        }

        if(!$('#year_periode').val()){
            toastr.error("<b>Tahun Closing</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#year_periode").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'finance/closing_journal/ins',
            type : "POST",  
            data : {
                month_periode       : $('#month_periode').val(),
                year_periode        : $('#year_periode').val(),
                
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'finance/closing_journal/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>