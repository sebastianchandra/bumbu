<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Journal</li>
                  <li class="breadcrumb-item">Input Closing Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a href="<?php echo base_url('finance/closing_journal/form'); ?>" class="btn btn-primary">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Bulan Closing</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id="month_periode">
                                    <option value="01">Januari</option>
                                    <option value="02">Febuari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Tahun Closing</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id="year_periode">
                                    <?php 
                                    $x      = (int)date('Y');
                                    $y      = $x-1;

                                    for ($i=$y; $i <= $x; $i++) { 
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('finance/closing_journal'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$("#month_periode").select2();
$("#year_periode").select2();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#month_periode').val()){
            toastr.error("<b>Bulan Closing</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#month_periode').focus();
            return false;
        }

        if(!$('#year_periode').val()){
            toastr.error("<b>Tahun Closing</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#year_periode").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'finance/closing_journal/ins',
            type : "POST",  
            data : {
                month_periode       : $('#month_periode').val(),
                year_periode        : $('#year_periode').val(),
                
            },
            success : function(resp){
                
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else if(resp.status == 'ada' || resp.status==true){
                    toastr.error("Data Periode Closing Sudah Ada.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'finance/closing_journal/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>