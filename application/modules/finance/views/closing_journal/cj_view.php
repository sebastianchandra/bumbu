<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Journal</li>
                  <li class="breadcrumb-item">View Closing Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a href="<?php echo base_url('finance/closing_journal/form'); ?>" class="btn btn-primary">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<?php 
// test($data_items,1);
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="11%">No</th>
                    <th width="20%">Periode</th>
                    <th >Status</th>
                    <th width="18%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no   = 0;
                  foreach ($data_pa as $key => $value) {
                    $no     = $no+1;
                  ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $value->month_periode.' '.$value->year_periode; ?></td>
                    <td><?php echo ($value->is_active==1)? 'Closing' : 'Open'; ?></td>
                    <td align="center">
                      <!-- <a href="<?php echo base_url('finance/closing_journal/edit/'.$value->closing_journal_id); ?>" class="btn btn-danger btn-xs">Edit</a> -->
                      <!-- <button id="delete" data-closing_journal_id='<?php echo $value->closing_journal_id; ?>' data-bulan='<?php echo $value->month_periode; ?>' data-tahun='<?php echo $value->year_periode; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button> -->
                      <?php 
                      if($value->is_active==1){
                      ?>
                      <button id="open_periode" data-closing_journal_id='<?php echo $value->closing_journal_id; ?>' data-bulan='<?php echo $value->month_periode; ?>' data-tahun='<?php echo $value->year_periode; ?>' class="btn btn-warning btn-xs" type="submit">Open Periode</button>
                      <?php 
                      }else{
                      ?>
                      <button id="close_periode" data-closing_journal_id='<?php echo $value->closing_journal_id; ?>' data-bulan='<?php echo $value->month_periode; ?>' data-tahun='<?php echo $value->year_periode; ?>' class="btn btn-warning btn-xs" type="submit">Closing Periode</button>
                      <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Crew', exportOptions:{messageTop: 'Master Crew',columns: [0,1,2,3,4,5]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#open_periode', function(e){
    var closing_journal_id  = $(this).data('closing_journal_id');
    var month_periode       = $(this).data('bulan');
    var year_periode        = $(this).data('tahun');

    toastr.warning(
      'Apakah anda Ingin Membuka Periode Bulan '+month_periode+' Tahun '+year_periode+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return openRow(this)" data-closing_journal_id="'+closing_journal_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });

  $('#vtable').on('click','#close_periode', function(e){
    var closing_journal_id  = $(this).data('closing_journal_id');
    var month_periode       = $(this).data('bulan');
    var year_periode        = $(this).data('tahun');

    toastr.info(
      'Apakah anda Ingin Menutup Periode Bulan '+month_periode+' Tahun '+year_periode+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return closeRow(this)" data-closing_journal_id="'+closing_journal_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });

  $('#vtable').on('click','#delete', function(e){
    var crew_id  = $(this).data('crew_id');
    var nama     = $(this).data('nama');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+nama+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-closing_journal_id="'+closing_journal_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });

});
function openRow(e){
  var closing_journal_id  = $(e).data('closing_journal_id');

  $.ajax({
    data: {
      closing_journal_id  : closing_journal_id
    },
    type : "POST",
    url: baseUrl+'finance/closing_journal/open_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Periode Berhasil di Buka.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'finance/closing_journal'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function closeRow(e){
  var closing_journal_id  = $(e).data('closing_journal_id');

  $.ajax({
    data: {
      closing_journal_id  : closing_journal_id
    },
    type : "POST",
    url: baseUrl+'finance/closing_journal/close_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Periode Berhasil di Tutup.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'finance/closing_journal'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function deleteRow(e){
  var crew_id  = $(e).data('crew_id');

  $.ajax({
    data: {
      crew_id  : crew_id
    },
    type : "POST",
    url: baseUrl+'finance/closing_journal/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Periode Berhasil di Buka.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'finance/closing_journal'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
