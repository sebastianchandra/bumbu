<?php 
// test(,1);
if($this->session->flashdata('alert')!=''){
// if(isset($this->session->flashdata('alert'))){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Journal</li>
                  <li class="breadcrumb-item">View Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <button id="input_po" class="btn btn-primary" type="submit" onclick="return show_pr(this)">Input</button>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Kode Transaksi</th>
                    <th>Periode</th>
                    <th>Coa Debet</th>
                    <th>Nama Debet</th>
                    <th>Coa Kredit</th>
                    <th>Nama Kredit</th>
                    <th>Value</th>
                    <!-- <th>Action</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_pa as $key => $value) {
                  ?>
                  <tr>
                      <td><?php echo $value->trn_code; ?></td>
                      <td><?php echo $value->month_period.' '.$value->year_period; ?></td>
                      <td><?php echo $value->db_coa_detail; ?></td>
                      <td><?php echo $value->db_coa_name; ?></td>
                      <td><?php echo $value->cr_coa_detail; ?></td>
                      <td><?php echo $value->cr_coa_name; ?></td>
                      <td align="right"><?php echo number_format($value->db_value,2); ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myPr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Nomor Permintaan Pembelian</h4>
      </div>
      <form method="post" action="<?php echo base_url().'finance/journal/form'; ?>">
        <div class="modal-body">
          <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Bulan </p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id="bulan" name="bulan">
                  <option value="">Bulan</option>
                  <option value="01">Januari</option>
                  <option value="02">Februari</option>
                  <option value="03">Maret</option>
                  <option value="04">April</option>
                  <option value="05">Mei</option>
                  <option value="06">Juni</option>
                  <option value="07">Juli</option>
                  <option value="08">Agustus</option>
                  <option value="09">September</option>                  
                  <option value="10">Oktober</option>                  
                  <option value="11">November</option>                  
                  <option value="12">Desember</option>                  
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tahun</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id="tahun" name="tahun">
                  <option value="">Tahun</option>
                  <?php 
                  $tahun    = date('Y')+1;
                  $old      = $tahun - 2;
                  for ($i=$old; $i <= $tahun; $i++) { 
                    echo '<option value="'.$i.'">'.$i.'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Kode Transaksi </p>
              </div>
              <div class="col-md-4">
                <select class="form-control" id="trn_code" name="trn_code">
                  <option value="">Select</option>
                  <!-- <option value="All">ALL</option> -->
                  <?php 
                  foreach ($data_journal as $key => $value) {
                    if($value->trn_code=="IS"){
                      $kd_trans     = "Incomming Stock ";
                    }elseif($value->trn_code=="IT"){
                      $kd_trans     = "Incomming Others ";
                    }elseif($value->trn_code=="OS"){
                      $kd_trans     = "Outgoing Stock ";
                    }elseif($value->trn_code=="OT"){
                      $kd_trans     = "Outgoing Others ";
                    }elseif($value->trn_code=="DO"){
                      $kd_trans     = "Delivery Order ";
                    }elseif($value->trn_code=="IC"){
                      $kd_trans     = "Invoice ";
                    }elseif($value->trn_code=="DS"){
                      $kd_trans     = "DP Sales Order ";
                    }elseif($value->trn_code=="PB"){
                      $kd_trans     = "PO Payment by Bank ";
                    }elseif($value->trn_code=="PC"){
                      $kd_trans     = "PO Payment by Cash ";
                    }elseif($value->trn_code=="IB"){
                      $kd_trans     = "Invoice Payment By Bank ";
                    }elseif($value->trn_code=="IH"){
                      $kd_trans     = "Invoice Payment By Cash ";
                    }
                    echo "<option value='".$value->trn_code."'>".$value->trn_code." - ".$kd_trans."</option>";
                  }
                  ?>
                </select>
              </div>    
            </div>     
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary btn-sm" id="save" value="Next">
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Pembayaran Pesanan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Nomor</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_date"></div>
          </div>          
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="supplier_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tipe </strong></p>
          </div>
          <div class="col-md-4">
            <div id="payment_type"></div>
          </div>
        </div>
       
        <div class="row-form">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nomor Pesanan Pembelian</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>

$('#save').click(function(){
  var isValid = true;

  if(!$('#bulan').val()){
    toastr.error("<strong>Bulan</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#bulan').focus();
    return false;
  }  
  if(!$('#tahun').val()){
    toastr.error("<strong>Tahun</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
    $('#tahun').focus();
    return false;
  }
  if(!$('#trn_code').val()){
    toastr.error("<strong>Kode Transaksi</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
    $('#trn_code').focus();
    return false;
  }

});
// $("#supplier_id").select2();

function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }
  if(values.length>=1){
    $.ajax({
      url: baseUrl+'omzet/sales_payment/pa_selected',
      type : "POST",  
      data: {
        values      : values
      },
      success : function(resp){        
        setTimeout(function () {
          window.location.href = baseUrl+'omzet/sales_payment/pa_input'; 
        }, 100);
      }
    });
  }else{
    toastr.warning("Pilih Nomor Sales Order.", 'Alert', {"positionClass": "toast-top-center"});
    return false;
  }
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});


function show_pr(e){  
  // $.get({
  //   url: baseUrl + 'transaction/po_payment/view_po/',
  //   success: function(resp){

  //     popup_pr.clear().draw();
  //     var dataSrc = JSON.parse(resp.detail);                 
  //     popup_pr.rows.add(dataSrc).draw(false);
  //   }
  // });

  $('#myPr').modal('show'); 
}


$('#fc_id').change(function(e){
  var fc_id   = $('#fc_id').val();
  $.get({
    url: baseUrl + 'omzet/sales_payment/view_so/'+fc_id,
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });
})
// function show_po(e){
//   alert('tes')
// }

function show_detail(e){
  var idPa     = $(e).data('id');
  var noPa     = $(e).data('pp');

  $.get({
    url: baseUrl + 'omzet/sales_payment/view_popup/'+idPa,
    success: function(resp){
      var type_payment;
      if(resp.header.payment_type=='1'){
        type_payment   = 'Cash';
      }else if(resp.header.payment_type=='2'){
        type_payment   = 'Transfer';
      }else if(resp.header.payment_type=='3'){
        type_payment   = 'Giro';
      }else if(resp.header.payment_type=='4'){
        type_payment   = 'Cek';
      }
      $('#pp_no').text(resp.header.fp_no);
      $('#pp_date').text(resp.header.fp_date);
      $('#supplier_name').text(resp.header.fc_name);
      $('#payment_type').text(type_payment);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pp_id  = $(this).data('pp_id');
    var pp_no  = $(this).data('pp_no');

    toastr.warning(
      'Apakah anda Ingin Membatalkan Nomor Pembayaran '+pp_no+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pp_no="'+pp_no+'" data-pp_id="'+pp_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var pp_id  = $(e).data('pp_id');
  var pp_no  = $(e).data('pp_no');

  $.ajax({
    data: {
      pp_id  : pp_id,
      pp_no  : pp_no
    },
    type : "POST",
    url: baseUrl+'transaction/po_payment/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("Data tidak berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/po_payment'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!--  
1. Penambahan Reject Untuk pembayaran PO
2. Penambahan Popup untuk detail dan header pembayaran.
-->