<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2 style="text-align: center">Sales Payment</h2>
    </div>
</div>
<?php 
if($header->payment_type==1){
  $pembayaran   = 'Cash';
}elseif($header->payment_type==2){
  $pembayaran   = 'Transfer';
}elseif($header->payment_type==3){
  $pembayaran   = 'Giro';
}elseif($header->payment_type==4){
  $pembayaran   = 'Cek';
}
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox">
            <div class="row-form">
              <div class="col-md-12">
                <p><strong></strong></p>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Nomor Pembayaran</strong></p>
              </div>
              <div class="col-md-4">
                <div id="no_pr"><?php echo $header->fp_no; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Nama Company</strong></p>
              </div>
              <div class="col-md-4">
                <div id="company_id"><?php echo $header->company_name; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tanggal</strong></p>
              </div>
              <div class="col-md-4">
                <div id="pr_date"><?php echo $header->fp_date; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Customer</strong></p>
              </div>
              <div class="col-md-4">
                <div id="requester"><?php echo $header->fc_name; ?></div>
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-2">
                <p><strong>Tipe Pembayaran</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $pembayaran; ?></div>
              </div>
              <div class="col-md-2">
                <p><strong>Total Pembayaran</strong></p>
              </div>
              <div class="col-md-4">
                <div id="remarks"><?php echo $header->grand_total; ?></div>
              </div>
            </div>
            
            <div class="row-form">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" style="padding-right: 0px !important;padding-left: 0px !important;">
                    <thead>
                      <tr>
                        <th>Nomor Pemesanan</th>
                        <th width="15%">Jumlah</th>
                      </tr>
                      <?php 
                      foreach ($detail as $key => $value) {
                      ?>
                      <tr>
                        <td><?php echo $value->sales_no; ?></td>
                        <td><?php echo number_format($value->total,2); ?></td>
                      </tr>
                      <?php 
                      }
                      ?>
                      <tr>
                        <td align="right" >Total : </td>
                        <td><?php echo number_format($header->grand_total,2); ?></td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <br/><br/>
            <div class="row-form">
              <div class="col-md-8">
                <p><strong></strong></p>
              </div>
              <div class="col-md-2">
                <p><strong>Penerima</strong><br/><br/><br/>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>window.print(); setTimeout(function(){window.close();},500);</script>
