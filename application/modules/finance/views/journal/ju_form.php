<?php 
//test($data_so[0]->supplier_id,1);
?>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Journal</li>
                  <li class="breadcrumb-item">Input Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a href="<?php echo base_url('finance/closing_journal/form'); ?>" class="btn btn-primary">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<?php 
if($trn_code=="IS"){
  $kd_trans     = "Incomming Stock ";
}elseif($trn_code=="IT"){
  $kd_trans     = "Incomming Stock Others ";
}elseif($trn_code=="OS"){
  $kd_trans     = "Outgoing Stock ";
}elseif($trn_code=="OT"){
  $kd_trans     = "Outgoing Stock Others ";
}elseif($trn_code=="DO"){
  $kd_trans     = "Delivery Order ";
}elseif($trn_code=="IC"){
  $kd_trans     = "Invoice ";
}elseif($trn_code=="DS"){
  $kd_trans     = "DP Sales Order ";
}elseif($trn_code=="PB"){
  $kd_trans     = "PO Payment by Bank ";
}elseif($trn_code=="PC"){
  $kd_trans     = "PO Payment by Cash ";
}elseif($trn_code=="IB"){
  $kd_trans     = "Invoice Payment By Bank ";
}elseif($trn_code=="IH"){
  $kd_trans     = "Invoice Payment By Bank ";
}
?>
<form class="form-horizontal" method="post" action="<?php echo base_url().'finance/journal/form_act'; ?>">
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">      
        <div class="card-body-form">
            <div class="form-group row">
              <div class="col-md-2">
                <p>Bulan</p>
              </div>
              <div class="col-md-5">
                <?php echo $bulan; ?>
                <input type="hidden" name="bulan" value="<?php echo $bulan; ?>">
                <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                <input type="hidden" name="trn_code" value="<?php echo $trn_code; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Tahun </p>
              </div>
              <div class="col-md-3">
                <?php echo $tahun; ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Kode Transaksi</p>
              </div>
              <div class="col-md-3">
                <?php echo $kd_trans; ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-bordered" id="detail">
                    <thead>
                      <tr>
                        <th width="1%">ID</th>
                        <th width="15%">Document No</th>
                        <th width="15%">Document Date</th>
                        <th width="15%">Document Status</th>
                        <th>Nilai</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $total     = 0;
                      // test($list_data,1);
                      foreach ($list_data as $key => $value) {
                        // test($value,1);
                      ?>
                      <tr>
                        <td></td>
                        <td><?php echo $value->doc_no; ?></td>
                        <td><?php echo $value->doc_date; ?></td>
                        <td><?php echo $value->doc_status; ?></td>
                        <td align="right"><?php echo number_format($value->total,2); ?></td>
                      </tr>
                      <?php 
                      $total    = $total+(float)$value->total;
                      }
                      ?>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td align="right"><?php echo number_format($total,2); ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- <table class="table table-hover table-bordered" id="detail">
                    <tr>
                      <td>Journal</td>
                      <td>Debet</td>
                      <td>Kredit</td>
                    </tr>
                    <tr>
                      <td><?php echo $conf_journal->db_coa_name; ?></td>
                      <td><?php echo number_format($total,2) ?></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td><?php echo $conf_journal->cr_coa_name; ?></td>
                      <td></td>
                      <td><?php echo number_format($total,2) ?></td>
                    </tr>
                  </table> -->
                </div>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm" type="submit" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('finance/journal'); ?>">Batal</a>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">

// $('#save').click(function(){
  
  // if(!$('#pp_date').val()){
  //   toastr.error("<strong>Tanggal Pembayaran</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#pp_date').focus();
  //   return false;
  // }
  
  // if(!$('#payment_type').val()){
  //   toastr.error("<strong>Tipe Pembayaran</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#payment_type').select2('open');
  //   return false;
  // }

  
  // var i;
  // var tdetail     = $('#tdetail').val();

  // for (i = 0; i < tdetail; i++) {
  //   var jml_hutang      = parseFloat($('#jml_hutang'+i).val().replace(/\,/g, ''));
  //   var total           = parseFloat($('#total'+i).val().replace(/\,/g, ''));
  //   var po_no           = $('#po_no'+i).val();

  //   if(total > jml_hutang){
  //     toastr.error("Jumlah Pembayaran Melebihi Jumlah Hutang <strong>"+po_no+"</strong> ", 'Alert', {"positionClass": "toast-top-center"});
  //     $("#total"+i).focus();
  //     return false;
  //   }

  // }



  

  // if(!$('#supplier_id').val()){
  //   toastr.error("<strong>Supplier Name</strong> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
  //   $('#supplier_id').select2('open');
  //   return false;
  // }
// });

pr = {
  data: {},
  processed: false,
  items: [],

  init: function(){
    $("#items_unit").select2();
    $("#project_id").select2();
    $("#payment_type").select2();
    $("#item_id").select2();

    this.grids = $('#detail').DataTable({
      "paging": false, 
      "bLengthChange": false, // disable show entries dan page
      "bFilter": false,
      "bInfo": false, // disable Showing 0 to 0 of 0 entries
      "bAutoWidth": false,
      "language": {
          "emptyTable": "Tidak Ada Data"
      },
      "columnDefs": [
        { // disable sorting on column process buttons
          "targets": [1,2,3,4],
          "orderable": false,
        },
        { 
          "targets": [0],
          "visible": false,
          "searchable": false
        }
      ]
    });

    // $('#save').click(pr.save);

  }

};

pr.init();

</script>

<!--  
1. Tanggal Pembayaran Enable (Bisa di Isi.)
-->