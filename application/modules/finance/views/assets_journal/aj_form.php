<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Journal</li>
                  <li class="breadcrumb-item">Input Bank / Cash Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Bank / Cash Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a href="<?php echo base_url('finance/closing_journal/form'); ?>" class="btn btn-primary">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <!-- <div class="form-group row">
                            <div class="col-md-2">
                                <p>Kode Transaksi</p>
                            </div>
                            <div class="col-md-5">
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Tanggal</p>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="journal_date" value="<?php echo substr(dbnow(),0,10); ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Type</p>
                            </div>
                            <div class="col-md-3" style="top: 10px;">
                                <input type="radio" value="in" id="type_journal" name="type_journal"> In &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="out" id="type_journal" name="type_journal"> Out
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Coa Debet</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="coa_debet" onclick="return browse_debet()">
                                <input type="text" class="form-control" id="trn_code" value="MJ" hidden="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Info Debet</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="db_info" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Coa Kredit</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="coa_kredit" onclick="return browse_kredit()">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Info Kredit</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="cr_info" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Value</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control price" id="mj_value" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('finance/assets_journal'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myCoaDebet" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Parent Coa</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="coa_popup_debet">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Coa Code</th>
                    <th>Coa Detail</th>
                    <th>Coa Name</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myCoaKredit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Parent Coa</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="coa_popup_kredit">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Coa Code</th>
                    <th>Coa Detail</th>
                    <th>Coa Name</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<script>
var url_debet   = '';
var url_kredit  = '';

$('input:radio[id=type_journal]').change(function() {
  if (this.value == 'in') {
      // alert("INNNN");
      url_debet   = baseUrl + 'master/coa/get_low_coa_special';
      url_kredit  = baseUrl + 'master/coa/get_low_coa_new';

      $('#trn_code').val('BM');
      $('#coa_debet').val('');
      $('#coa_debet').attr('data-id', '');
      $('#coa_debet').attr('data-code', '');
      $('#coa_debet').attr('data-detail', '');

      $('#coa_kredit').val('');
      $('#coa_kredit').attr('data-id', '');
      $('#coa_kredit').attr('data-code', '');
      $('#coa_kredit').attr('data-detail', '');
  }
  else if (this.value == 'out') {
      // alert("OUTTTT");
      url_debet   = baseUrl + 'master/coa/get_low_coa_new';
      url_kredit  = baseUrl + 'master/coa/get_low_coa_special';

      $('#trn_code').val('BK');
      $('#coa_debet').val('');
      $('#coa_debet').attr('data-id', '');
      $('#coa_debet').attr('data-code', '');
      $('#coa_debet').attr('data-detail', '');
      
      $('#coa_kredit').val('');
      $('#coa_kredit').attr('data-id', '');
      $('#coa_kredit').attr('data-code', '');
      $('#coa_kredit').attr('data-detail', '');
  }
});

var popup_debet = $('#coa_popup_debet').DataTable({ 
  "paging":   true,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { // disable sorting on column process buttons
      "targets": [0,1,2,3],
      "orderable": false,
    }
  ]
});

popup_debet.on('click', 'tr', function (){
  // debugger
  var data = popup_debet.row(this).data();
  var taskID = data[0];

  $('#coa_debet').val(data[3]);
  $('#coa_debet').attr('data-id', data[0]);
  $('#coa_debet').attr('data-code', data[1]);
  $('#coa_debet').attr('data-detail', data[2]);

  $('#myCoaDebet').modal('hide'); 
});

function browse_debet(e){  
  $.get({
    url: url_debet,
    success: function(resp){
      popup_debet.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_debet.rows.add(dataSrc).draw(false);
    }
  });

  $('#myCoaDebet').modal('show'); 
}

var popup_kredit = $('#coa_popup_kredit').DataTable({ 
  "paging":   true,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { // disable sorting on column process buttons
      "targets": [0,1,2,3],
      "orderable": false,
    }
  ]
});

popup_kredit.on('click', 'tr', function (){
  // debugger
  var data = popup_kredit.row(this).data();
  var taskID = data[0];

  $('#coa_kredit').val(data[3]);
  $('#coa_kredit').attr('data-id', data[0]);
  $('#coa_kredit').attr('data-code', data[1]);
  $('#coa_kredit').attr('data-detail', data[2]);

  $('#myCoaKredit').modal('hide'); 
});

function browse_kredit(e){  
  $.get({
    url: url_kredit,
    success: function(resp){
      popup_kredit.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_kredit.rows.add(dataSrc).draw(false);
    }
  });

  $('#myCoaKredit').modal('show'); 
}

$(document).ready(function(){
  

    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#journal_date').val()){
            toastr.error("<b>Tanggal</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#journal_date').focus();
            return false;
        }

        if(!$('#trn_code').val()){
            toastr.error("<b>Kode Transaksi</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#trn_code').focus();
            return false;
        }

        if(!$('#coa_debet').val()){
            toastr.error("<b>Coa Debet</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#coa_debet').focus();
            return false;
        }

        if(!$('#db_info').val()){
            toastr.error("<b>Info Debet</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#db_info').focus();
            return false;
        }

        if(!$('#coa_kredit').val()){
            toastr.error("<b>Coa Kredit</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#coa_kredit').focus();
            return false;
        }

        if(!$('#cr_info').val()){
            toastr.error("<b>Info Kredit</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#cr_info').focus();
            return false;
        }

        if(!$('#mj_value').val()){
            toastr.error("<b>Value</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#mj_value').focus();
            return false;
        }
        $('#save').prop("disabled",true);
        $.ajax({
            url  : baseUrl+'finance/assets_journal/form_act',
            type : "POST",  
            data : {
                trn_code            : $('#trn_code').val(),
                cr_coa_name         : $('#coa_kredit').val(),
                cr_coa_id           : $('#coa_kredit').attr('data-id'),
                cr_coa_code         : $('#coa_kredit').attr('data-code'),
                cr_coa_detail       : $('#coa_kredit').attr('data-detail'),
                db_coa_name         : $('#coa_debet').val(),
                db_coa_code         : $('#coa_debet').attr('data-code'),
                db_coa_id           : $('#coa_debet').attr('data-id'),
                db_coa_detail       : $('#coa_debet').attr('data-detail'),
                journal_date        : $('#journal_date').val(),
                db_info             : $('#db_info').val(),
                cr_info             : $('#cr_info').val(),
                mj_value            : $('#mj_value').val().replace(/\,/g, '')
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;
                }else if(resp.success=='closing'){
                    toastr.error(resp.status, 'Alert', {"positionClass": "toast-top-center"});
                    return false;
                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'finance/assets_journal/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>