<?php
if($status=='excell'){
  $file = "current_stock.xls";
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment; filename='.$file);
}
?>
<style>
/* table, td, th {
    border: 1px solid black;
} */

table.header {
    width: 100%;
    border-collapse: collapse;
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
}
table td{
    padding-left: 5px;
}
table.body {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid black;
}
table.body, td.body {
  border: 1px solid black;
}
</style>
<?php 
// test($detail,0);
?>
<table class="header" id="vtable">
  <thead>
    <tr>
        <td colspan="2"><strong>Souwmpies Corp</strong></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3" align="center"><strong>VOUCHER PENERIMAAN KAS / BANK</strong></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%'></td>
        <td width='20%' align="right"><strong>No.</strong></td>
        <td width='20%'>: <?= $detail->trn_id; ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td align="right"><strong>Tanggal</strong> </td>
        <td>: <?= tgl_jurnal($detail->journal_date); ?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><br/></td>
    </tr>
</table>
<table class="body" id="vtable">
  <thead>
    <tr>
        <td class="body" align="center" width="20%"><strong>No. BG/Cek</strong></td>
        <td class="body" align="center" width="30%"><strong>Keterangan</strong></td>
        <td class="body" colspan="2" align="center"><strong>Jumlah</strong></td>
    </tr>
    <tr>
        <td class="body"></td>
        <td class="body"><?= $detail->db_info; ?></td>
        <td class="body" colspan="2" align="right">Rp. <?= money($detail->db_value); ?></td>
    </tr>
    <tr>
        <td class="body"><br/></td>
        <td class="body"></td>
        <td class="body" colspan="2"></td>
    </tr>
    <tr>
        <td class="body"><br/></td>
        <td class="body"></td>
        <td class="body" colspan="2"></td>
    </tr>
    <tr>
        <td class="body" colspan="2" align="center">Total</td>
        <td class="body" colspan="2" align="right">Rp. <?= money($detail->db_value); ?></td>
    </tr>
    <tr>
        <td  colspan="4"><br></td>
    </tr>
    <tr>
        <td></td>
        <td  colspan="3">Terbilang : <?= terbilang($detail->db_value); ?> Rupiah</td>
    </tr>    
    <tr>
        <td  colspan="4"><br></td>
    </tr>
    <tr>
        <td class="body" rowspan="2" align="center">Kode Perkiraan</td>
        <td class="body" rowspan="2" align="center" >Nama Perkiaan</td>
        <td class="body" colspan="2" align="center">Jumlah</td>
    </tr>
    <tr>
        <td class="body" align="center">Debete</td>
        <td class="body" align="center">Kredit</td>
    </tr>
    <tr>
        <td class="body" align="center"><?= $detail->db_coa_detail; ?></td>
        <td class="body"><?= $detail->db_coa_name; ?></td>
        <td class="body" align="right"><?= money($detail->db_value); ?></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body" align="center"><?= $detail->cr_coa_detail; ?></td>
        <td class="body">&ensp;&ensp;&ensp;&ensp;&ensp;<?= $detail->cr_coa_name; ?></td>
        <td class="body" width="25%"></td>
        <td class="body" align="right" width="25%"><?= money($detail->cr_value); ?></td>
    </tr>
    <tr>
        <td class="body"><br></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body"><br></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body"><br></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
    </tr>
    <tr>
        <td class="body" colspan="2" align="center">Total</td>
        <td class="body" align="right"><?= money($detail->db_value); ?></td>
        <td class="body" align="right"><?= money($detail->cr_value); ?></td>
    </tr>
    <tr>
        <td align="center">Dibuat <br><br><br><br><br><br>( Finance )</td>
        <td align="center" colspan="2">Diperiksa <br><br><br><br><br><br>( Owner )</td>
        <td align="center">Dibekukan <br><br><br><br><br><br>( Accounting )</td>
    </tr>
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>