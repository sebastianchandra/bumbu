<?php 
// test(,1);
if($this->session->flashdata('alert')!=''){
// if(isset($this->session->flashdata('alert'))){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Journal</li>
                  <li class="breadcrumb-item">View Payment Invoice / PO Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Payment Invoice / PO Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-primary" type="submit" href="<?php echo base_url('finance/assets_journal/form') ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="3%">Kode</th>
                    <th width="6%">Periode</th>
		    <th width="10%">Tgl</th>
                    <th>Coa Debet</th>
                    <th>Nama Debet</th>
                    <th>Coa Kredit</th>
                    <th>Nama Kredit</th>
                    <th>Value</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_pa as $key => $value) {
                  ?>
                  <tr>
                      <td><?php echo $value->trn_id; ?></td>
                      <td><?php echo $value->month_period.' '.$value->year_period; ?></td>
                      <td><?php echo tgl_singkat($value->journal_date); ?></td>
		      <td><?php echo $value->db_coa_detail; ?></td>
                      <td><?php echo $value->db_coa_name; ?></td>
                      <td><?php echo $value->cr_coa_detail; ?></td>
                      <td><?php echo $value->cr_coa_name; ?></td>
                      <td align="right"><?php echo number_format($value->db_value,2); ?></td>
                      <td>
                        <!-- <a href="<?php echo base_url('finance/assets_journal/edit/'.$value->journal_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                        <button id="delete" data-journal_id='<?php echo $value->journal_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button> -->
                        <a target="_blank" href="<?= base_url('finance/payment_journal/cetak/'.$value->journal_id) ?>" class="btn btn-info btn-xs">Print</a>
                      </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myPr" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Transaksi Journal</h4>
      </div>
      <form method="post" action="<?php echo base_url().'finance/journal/form'; ?>">
        <div class="modal-body">
          <div class="row-form">
            <div class="col-md-12">
              <div class="row-form">
                <div class="col-md-2">
                  <p>Bulan</p>
                </div>
                <div class="col-md-3">
                  <select class="form-control" id="bulan" name="bulan">
                    <option value="">Bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>                  
                    <option value="10">Oktober</option>                  
                    <option value="11">November</option>                  
                    <option value="12">Desember</option>                  
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row-form">
                <div class="col-md-2">
                  <p>Tahun</p>
                </div>
                <div class="col-md-3">
                  <select class="form-control" id="tahun" name="tahun">
                    <option value="">Tahun</option>
                    <?php 
                    $tahun    = date('Y');
                    $old      = $tahun - 3;
                    for ($i=$old; $i <= $tahun; $i++) { 
                      echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                    ?>
                  </select>
                </div>     
              </div>
            </div>
            <div class="col-md-12">
              <div class="row-form">
                <div class="col-md-2">
                  <p>Kode Transaksi</p>
                </div>
                <div class="col-md-3">
                  <select class="form-control" id="trn_code" name="trn_code">
                    <option value="">Select</option>
                    <?php 
                    foreach ($data_journal as $key => $value) {
                      echo "<option value='".$value->trn_code."'>".$value->trn_code."</option>";
                    }
                    ?>
                  </select>
                </div>    
              </div>
            </div>     
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary btn-sm" id="save" value="Next">
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Pembayaran Pesanan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Nomor</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_no"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pp_date"></div>
          </div>          
        </div>
        <div class="row-form">
          <div class="col-md-2">
            <p><strong>Customer</strong></p>
          </div>
          <div class="col-md-4">
            <div id="supplier_name"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Tipe </strong></p>
          </div>
          <div class="col-md-4">
            <div id="payment_type"></div>
          </div>
        </div>
       
        <div class="row-form">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th>Nomor Pesanan Pembelian</th>
                    <th width="25%">Subtotal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>

$('#save').click(function(){
  var isValid = true;

  if(!$('#bulan').val()){
    toastr.error("<strong>Bulan</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#bulan').focus();
    return false;
  }  
  if(!$('#tahun').val()){
    toastr.error("<strong>Tahun</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
    $('#tahun').focus();
    return false;
  }
  if(!$('#trn_code').val()){
    toastr.error("<strong>Kode Transaksi</strong> Harus di Pilih", 'Alert', {"positionClass": "toast-top-center"});
    $('#trn_code').focus();
    return false;
  }

});
// $("#supplier_id").select2();

function send_query(check) {
  var values = [];
  for (i = 0; i < check.length; i++) {
      if (check[i].checked == true) {
          values.push(check[i].value);
      }
  }
  if(values.length>=1){
    $.ajax({
      url: baseUrl+'omzet/sales_payment/pa_selected',
      type : "POST",  
      data: {
        values      : values
      },
      success : function(resp){        
        setTimeout(function () {
          window.location.href = baseUrl+'omzet/sales_payment/pa_input'; 
        }, 100);
      }
    });
  }else{
    toastr.warning("Pilih Nomor Sales Order.", 'Alert', {"positionClass": "toast-top-center"});
    return false;
  }
}

var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});

var popup_pr = $('#pr_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  }
});


function show_pr(e){  
  // $.get({
  //   url: baseUrl + 'transaction/po_payment/view_po/',
  //   success: function(resp){

  //     popup_pr.clear().draw();
  //     var dataSrc = JSON.parse(resp.detail);                 
  //     popup_pr.rows.add(dataSrc).draw(false);
  //   }
  // });

  $('#myPr').modal('show'); 
}


$('#fc_id').change(function(e){
  var fc_id   = $('#fc_id').val();
  $.get({
    url: baseUrl + 'omzet/sales_payment/view_so/'+fc_id,
    success: function(resp){

      popup_pr.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_pr.rows.add(dataSrc).draw(false);
    }
  });
})
// function show_po(e){
//   alert('tes')
// }

function show_detail(e){
  var idPa     = $(e).data('id');
  var noPa     = $(e).data('pp');

  $.get({
    url: baseUrl + 'omzet/sales_payment/view_popup/'+idPa,
    success: function(resp){
      var type_payment;
      if(resp.header.payment_type=='1'){
        type_payment   = 'Cash';
      }else if(resp.header.payment_type=='2'){
        type_payment   = 'Transfer';
      }else if(resp.header.payment_type=='3'){
        type_payment   = 'Giro';
      }else if(resp.header.payment_type=='4'){
        type_payment   = 'Cek';
      }
      $('#pp_no').text(resp.header.fp_no);
      $('#pp_date').text(resp.header.fp_date);
      $('#supplier_name').text(resp.header.fc_name);
      $('#payment_type').text(type_payment);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}

$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var journal_id  = $(this).data('journal_id');

    toastr.warning(
      'Apakah anda Ingin Menghapus  ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-journal_id="'+journal_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var journal_id  = $(e).data('journal_id');

  $.ajax({
    data: {
      journal_id  : journal_id,
    },
    type : "POST",
    url: baseUrl+'finance/assets_journal/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("Data tidak berhasil di Batalkan.", 'Alert', {"positionClass": "toast-top-center"});
        return false;
      }else if(resp.success=='closing'){
        toastr.success(resp.status, 'Alert', {"positionClass": "toast-top-center"});
        return false;
      } else {
        toastr.success("Data berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'finance/assets_journal'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!--  
1. Penambahan Reject Untuk pembayaran PO
2. Penambahan Popup untuk detail dan header pembayaran.
-->