<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Config_bonus_model extends CI_Model
{
    private $myDb = 'db_bumbu_master';
    private $myTable = 'mst_bonus_config';
    private $bcId;
    private $itemsId;
    // private $itemsCode;
    // private $itemsName;
    // private $itemsKind;
    // private $itemsInfo;
    private $qtyItems;
    private $qtyBonus;
    // private $itemsUnit;
    // private $itemsGroup;
    // private $categoryItems;
    private $isActive;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->bcId = '';
        $this->itemsId = '';
        // $this->itemsCode = '';
        // $this->itemsName = '';
        // $this->itemsKind = '';
        // $this->itemsInfo = '';
        $this->qtyItems = 0;
        $this->qtyBonus = 0;
        // $this->itemsUnit = '';
        // $this->itemsGroup = '';
        // $this->categoryItems = '';
        $this->isActive = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
    }

    public function setBcId($aBcId)
    {
        $this->bcId = $this->db->escape_str($aBcId);
    }
    public function getBcId()
    {
        return $this->bcId;
    }
    public function setItemsId($aItemsId)
    {
        $this->itemsId = $this->db->escape_str($aItemsId);
    }
    public function getItemsId()
    {
        return $this->itemsId;
    }
    
    public function setQtyItems($aQtyItems)
    {
        $this->qtyItems = $this->db->escape_str($aQtyItems);
    }
    public function getQtyItems()
    {
        return $this->qtyItems;
    }

    public function setQtyBonus($aQtyBonus)
    {
        $this->qtyBonus = $this->db->escape_str($aQtyBonus);
    }
    public function getQtyBonus()
    {
        return $this->qtyBonus;
    }

    public function setItemsGroup($aItemsGroup)
    {
        $this->itemsGroup = $this->db->escape_str($aItemsGroup);
    }
    public function getItemsGroup()
    {
        return $this->itemsGroup;
    }
    public function getIsActive()
    {
        return $this->isActive;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

	function get_config()
    {
        $sql ='SELECT *
            FROM mst_bonus_config a 
            LEFT JOIN mst_items b ON b.items_id=a.items_id 
            WHERE a.is_active="1" ORDER BY bc_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_items_all()
    {
        $sql ='SELECT *
            FROM mst_items a 
            WHERE a.is_active="1" ORDER BY items_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_items_sa($to_local,$id)
    {
        $sql    ='SELECT a.*
                    FROM mst_items a 
                    LEFT JOIN db_bumbu_transaction.trn_stock_by_doc c ON a.items_id=c.items_id ';
        if($to_local=='warehouse'){
            $sql   .=' AND c.warehouse_id="'.$id.'" ';
        }elseif($to_local=='project'){
            $sql   .=' AND c.project_id="'.$id.'" ';
        }

        $sql   .=' LEFT JOIN mst_items_unit b ON b.items_unit_id=a.items_unit 
            WHERE a.is_active="1" AND c.stock_by_doc_id IS NULL ORDER BY a.items_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDataCount()
    {
        $stQuery  = 'SELECT IFNULL(MAX(items_id),0) items_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
        $query = $this->db->query($stQuery);
        return $query->row();
    }

    function get_nomor_dok($tahun){
        $query = $this->db->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(items_code,9,4))+1,4,'0'),'0001') nomor_dok,items_code 
                                FROM mst_items WHERE SUBSTRING(items_code,3,6)='".$tahun."'")->row();
        return $query;
    }

    function get_items_unit(){
        $sql    = "SELECT items_unit_id,items_unit_name FROM mst_items_unit";
        return $this->db->query($sql)->result();
    }

    public function act_form()
    {
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'items_id,'; 
        $stQuery .=   'qty_items,';
        $stQuery .=   'qty_bonus,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   '"'.$this->db->escape_str($this->input->post('items_id')).'",'; 
        $stQuery .=   $this->db->escape_str($this->input->post('qty_items')).',';
        $stQuery .=   '"'.$this->input->post('qty_bonus').'" ,';
        $stQuery .=   '"1" ,'; 
        $stQuery .=   $this->db->escape_str($this->current_user['user_id']).','; 
        $stQuery .=   '"'.$this->db->escape_str(dbnow()).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('bc_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    public function act_edit()
    {
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'items_id ="'.$this->db->escape_str($this->input->post('items_id')).'",'; 
        $stQuery .=   'qty_items ='.$this->db->escape_str($this->input->post('qty_items')).','; 
        $stQuery .=   'qty_bonus ='.$this->db->escape_str($this->input->post('qty_bonus')).','; 
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->current_user['user_id']).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str(dbnow()).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'bc_id = '.$this->input->post('bc_id'); 
        // test($stQuery,1);
        $this->db->query($stQuery); 
    }

    function act_delete_js(){
        $sql = "UPDATE mst_bonus_config SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE bc_id = '".$this->input->post('bc_id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

    function update_price($items_id,$price){
        $sql = "UPDATE mst_items SET price='".$price."', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE items_id = '".$items_id."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

}	