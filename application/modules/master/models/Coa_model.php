<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Coa_model extends CI_Model
{
    function __construct(){
        parent::__construct();
        // $this->db = $this->load->database('finance',true);     
      
    }

	function get_coa()
    {
        $sql ='SELECT b.coa_id,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a 
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active="1"
                ORDER BY b.coa_detail';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_post_lv1()
    {
        $sql ='SELECT a.coa_id,a.coa_level,a.coa_detail,a.coa_name FROM mst_coa a WHERE a.is_active="1" AND a.coa_level="1" order by a.coa_id';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_coa_all()
    {
        $sql ='SELECT b.coa_id,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a 
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active="1"
                ORDER BY b.coa_detail';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_coa_level()
    {
        $sql ='SELECT `coa_level`,level_name FROM mst_coa_level';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_all_bp_new($id){
        $sql = "SELECT b.coa_level,b.coa_id,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.coa_level='".$id."'
                ORDER BY b.coa_detail ";

        $item = $this->db->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_all_bp($start=0,$length=10,$search='',$id){
        $sql = "SELECT b.coa_level,b.coa_id,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.coa_level='".$id."'
                AND (b.coa_detail LIKE '%".$search."%' OR b.coa_name LIKE '%".$search."%')
                ORDER BY b.coa_detail LIMIT ".$start.", ".$length."";

        $item = $this->db->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_count($id){
        $sql =" SELECT COUNT(b.coa_id) total FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.coa_level='".$id."' 
                ORDER BY b.coa_detail ";

        $item = $this->db->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function get_count_display($start,$length,$search = false,$id) {
        $str = '';
        if ($search) {
            $str = " AND (b.coa_detail LIKE '%".$search."%' OR b.coa_name LIKE '%".$search."%') ";
        }

        $sql = "SELECT COUNT(b.coa_id) total FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.coa_level='".$id."' ".$str."
                ORDER BY b.coa_detail  ";

        $item = $this->db->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function cek_coa($kode,$id_parent){
        return $this->db->query("SELECT * FROM mst_coa WHERE coa_parent_id='".$id_parent."' AND coa_code='".$kode."'");
    }

    function cek_coa_num($kode,$id_parent){
        return $this->db->query("SELECT * FROM mst_coa WHERE coa_parent_id='".$id_parent."' AND coa_code='".$kode."' AND is_active='1'");
    }

    function act_form(){
        $coa_name       = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_name')));
        $id_level       = $this->security->xss_clean($this->db->escape_str($this->input->post('id_level')));
        $id_parent      = $this->security->xss_clean($this->db->escape_str($this->input->post('id_parent')));
        $coa_code       = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_code')));

        
        $data_no        = $this->get_nomor_dok($id_parent);
        // $kode           = $data_no->kode; Karena Pakai Inputan, jadi Penomoran Otomatis tidak dijalankan.
        $kode           = $coa_code;

        $data_detail    = $this->get_coa_detail($id_parent);

        // test($data_detail->coa_detail,1);

        if($id_level==1){
            $coa_detail = $kode;
        }else{
            if($id_level==2){
                $nilai_str  = (($id_level-1)*3);
                $coa_detail = substr($data_detail->coa_detail,0,$nilai_str).'.'.$kode;
            }else{
                $nilai_str  = (($id_level-1)*3);
                $coa_detail = substr($data_detail->coa_detail,0,$nilai_str).'.'.$kode;
            }

            $sql1    = "UPDATE mst_coa SET is_lowest = '0' WHERE coa_id = '".$id_parent."'";
            $query1  = $this->db->query($sql1);

        }

            $sql    = "INSERT INTO mst_coa (coa_level,coa_code,coa_detail,coa_name,coa_parent_id,is_active,is_lowest,pic_input,input_time)VALUES
            ('".$id_level."','".$kode."','".$coa_detail."','".$coa_name."','".$id_parent."','1','1','".$this->current_user['user_id']."','".dbnow()."')";
            $query = $this->db->query($sql);

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }      
        
    }

    function get_nomor_dok($id_parent){
        $query = $this->db->query("SELECT IFNULL(LPAD(MAX(coa_code)+1,2,'0'),'01') kode, coa_detail, coa_code FROM mst_coa WHERE coa_parent_id='".$id_parent."'")->row();
        return $query;
    }

    function get_coa_detail($coa_id){
        $query = $this->db->query("SELECT coa_detail FROM mst_coa WHERE coa_id='".$coa_id."'")->row();
        return $query;
    }

    function get_all_post_by_detail($start=0,$length=10,$search='',$id_posting,$det_posting) {

        $sql = "SELECT a.coa_id,a.coa_detail,a.coa_name,b.coa_name name_parent 
                FROM mst_coa a LEFT JOIN mst_coa b ON a.coa_parent_id=b.coa_id WHERE a.is_active='1' AND a.is_lowest='1' 
                AND a.coa_detail LIKE '".$det_posting.".%' AND (a.coa_name LIKE '%".$search."%')
                LIMIT ".$start.", ".$length." ";

        $item = $this->db->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_count_by_detail($id_posting,$det_posting){

        $sql =" SELECT count(a.coa_id) total FROM mst_coa a LEFT JOIN mst_coa b ON a.coa_parent_id=b.coa_id WHERE a.is_active='1' AND a.is_lowest='1' 
                AND a.coa_detail LIKE '".$det_posting.".%'";

        $item = $this->db->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function get_count_display_by_detail($start,$length,$search = false,$id,$det_posting) {

        $str = '';
        if ($search) {
            $str = " AND (a.coa_name LIKE '%".$search."%') ";
        }

        $sql = "SELECT count(a.coa_id) total FROM mst_coa a LEFT JOIN mst_coa b ON a.coa_parent_id=b.coa_id WHERE a.is_active='1' AND a.is_lowest='1' 
                AND a.coa_detail LIKE '".$det_posting.".%' ".$str." ";

        $item = $this->db->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function get_detail_coa($id){
        $query = $this->db->query("SELECT b.coa_id,b.coa_level,b.coa_id,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id,b.coa_code,a.coa_detail detail_parent FROM mst_coa a 
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id where b.coa_id='".$id."'")->row();
        return $query;
    }

    function act_edit(){
        $coa_name       = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_name')));
        $id_level       = $this->security->xss_clean($this->db->escape_str($this->input->post('id_level')));
        $id_parent      = $this->security->xss_clean($this->db->escape_str($this->input->post('id_parent')));
        $coa_id         = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_id')));
        $coa_code       = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_code')));

        $data_no        = $this->get_nomor_dok($id_parent);
        // $kode           = $data_no->kode; Karena Pakai Inputan, jadi Penomoran Otomatis tidak dijalankan.
        $kode           = $coa_code;

        $data_detail    = $this->get_coa_detail($id_parent);

        // test($data_detail->coa_detail,1);

        if($id_level==1){
            $coa_detail = $kode;
        }else{
            if($id_level==2){
                $nilai_str  = (($id_level-1)*3);
                $coa_detail = substr($data_detail->coa_detail,0,$nilai_str).'.'.$kode;
            }else{
                $nilai_str  = (($id_level-1)*3);
                $coa_detail = substr($data_detail->coa_detail,0,$nilai_str).'.'.$kode;
            }

            $sql1    = "UPDATE mst_coa SET is_lowest = '0' WHERE coa_id = '".$id_parent."'";
            $query1  = $this->db->query($sql1);

        }

        $sql    = "UPDATE mst_coa 
                    SET
                    coa_level = '".$id_level."' , 
                    coa_code = '".$kode."' , 
                    coa_detail = '".$coa_detail."' , 
                    coa_name = '".$coa_name."' , 
                    coa_parent_id = '".$id_parent."' , 
                    pic_edit = '".$this->current_user['user_id']."' , 
                    edit_time = '".dbnow()."'
                    WHERE
                    coa_id = '".$coa_id."'";
                    
        $query = $this->db->query($sql);

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function get_all_coa_new(){
        $sql = "SELECT b.coa_level,b.coa_id,b.coa_code,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.is_lowest='1'
                ORDER BY b.coa_detail ";

        $item = $this->db->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_all_coa_special(){
        $sql = "SELECT b.coa_level,b.coa_id,b.coa_code,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.is_lowest='1' AND b.coa_name LIKE 'KAS%' OR b.coa_name LIKE 'BANK%'
                ORDER BY b.coa_detail ";

        $item = $this->db->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_all_coa($start=0,$length=10,$search=''){
        $sql = "SELECT b.coa_level,b.coa_id,b.coa_code,b.coa_detail,b.coa_name,a.coa_name parent,b.coa_parent_id FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.is_lowest='1'
                AND (b.coa_detail LIKE '%".$search."%' OR b.coa_name LIKE '%".$search."%')
                ORDER BY b.coa_detail LIMIT ".$start.", ".$length."";

        $item = $this->db->query($sql)->result();
        return is_for($item) ? $item : false;
    }

    function get_count_display_coa($start,$length,$search = false) {
        $str = '';
        if ($search) {
            $str = " AND (b.coa_detail LIKE '%".$search."%' OR b.coa_name LIKE '%".$search."%') ";
        }

        $sql = "SELECT COUNT(b.coa_id) total FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.is_lowest='1' ".$str."
                ORDER BY b.coa_detail  ";

        $item = $this->db->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function get_count_coa(){
        $sql =" SELECT COUNT(b.coa_id) total FROM mst_coa a
                RIGHT JOIN mst_coa b ON b.coa_parent_id=a.coa_id
                WHERE b.is_active='1' AND b.is_lowest='1' 
                ORDER BY b.coa_detail ";

        $item = $this->db->query($sql)->row();
        return isset($item->total) ? $item->total : 0;
    }

    function act_delete_js(){
        $sql = "UPDATE mst_coa SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE coa_id = '".$this->input->post('coa_id')."'";
          //test($sql,1);
          $query = $this->db->query($sql);
          return $query;
    }
    
















}	