<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project_model extends CI_Model
{
    private $myDb = 'db_bumbu_master';
    private $myTable = 'mst_project';
    private $projectId;
    private $projectName;
    private $projectLocation;
    private $projectInfo;
    private $isActive;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;
    
    public function __construct(){
        parent::__construct();
        $this->projectId = 0;
        $this->projectName = '';
        $this->projectLocation = '';
        $this->projectInfo = '';
        $this->isActive = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
    }

    public function setProjectId($aProjectId)
    {
       $this->projectId = $this->db->escape_str($aProjectId);
    }
    public function getProjectId()
    {
        return $this->projectId;
    }
    public function setProjectName($aProjectName)
    {
        $this->projectName = $this->db->escape_str($aProjectName);
    }
    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setProjectLocation($aProjectLocation)
    {
        $this->projectLocation = $this->db->escape_str($aProjectLocation);
    }
    public function getProjectLocation()
    {
        return $this->projectLocation;
    }
    public function setProjectInfo($aProjectInfo)
    {
        $this->projectInfo = $this->db->escape_str($aProjectInfo);
    }
    public function getProjectInfo()
    {
        return $this->projectInfo;
    }
    public function setIsActive($aIsActive)
    {
        $this->isActive = $this->db->escape_str($aIsActive);
    }
    public function getIsActive()
    {
        return $this->isActive;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

	function get_project(){
        $sql ='SELECT project_id,project_name,project_location,project_info,is_active FROM mst_project where is_active=1 ORDER BY project_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_project_view(){
        $sql ='SELECT project_id,project_name,project_location,project_info,is_active FROM mst_project where is_active<>0 ORDER BY project_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function act_form(){
        if($this->projectId =='' || $this->projectId == NULL )
        {
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL )
        {
            $this->projectName = '';
        }
        if($this->projectLocation =='' || $this->projectLocation == NULL )
        {
            $this->projectLocation = '';
        }
        if($this->projectInfo =='' || $this->projectInfo == NULL )
        {
            $this->projectInfo = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        // $stQuery .=   'project_id,'; 
        $stQuery .=   'project_name,'; 
        $stQuery .=   'project_location,'; 
        $stQuery .=   'project_info,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        // $stQuery .=   $this->db->escape_str($this->projectId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->projectName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->projectLocation).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->projectInfo).'",'; 
        $stQuery .=   $this->db->escape_str($this->isActive).','; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        $query = $this->db->query($stQuery); 

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    public function getById($id)
    {
          $this->db->select('*');
          $this->db->from($this->myDb.'.'.$this->myTable);
          $this->db->where('project_id', $this->db->escape_str($id));
          return $this->db->get()->row();
    }

    public function act_edit($id)
     {
        if($this->projectId =='' || $this->projectId == NULL )
        {
            $this->projectId = 0;
        }
        if($this->projectName =='' || $this->projectName == NULL )
        {
            $this->projectName = '';
        }
        if($this->projectLocation =='' || $this->projectLocation == NULL )
        {
            $this->projectLocation = '';
        }
        if($this->projectInfo =='' || $this->projectInfo == NULL )
        {
            $this->projectInfo = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'project_name ="'.$this->db->escape_str($this->projectName).'",'; 
        $stQuery .=   'project_location ="'.$this->db->escape_str($this->projectLocation).'",'; 
        $stQuery .=   'project_info ="'.$this->db->escape_str($this->projectInfo).'",'; 
        $stQuery .=   'is_active ='.$this->db->escape_str($this->isActive).',';  
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'project_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
     }

    function act_delete_js($is_active){
        $sql = "UPDATE mst_project SET is_active='".$is_active."', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE project_id = '".$this->input->post('project_id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

}	