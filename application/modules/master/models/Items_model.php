<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items_model extends CI_Model
{
    private $myDb = 'db_bumbu_master';
    private $myTable = 'mst_items';
    private $itemsId;
    private $itemsCode;
    private $itemsName;
    private $itemsKind;
    private $itemsInfo;
    private $price;
    private $itemsUnit;
    private $itemsGroup;
    private $categoryItems;
    private $isActive;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    function __construct(){
        parent::__construct();
        $this->itemsId = '';
        $this->itemsCode = '';
        $this->itemsName = '';
        $this->itemsKind = '';
        $this->itemsInfo = '';
        $this->itemsUnit = '';
        $this->itemsGroup = '';
        $this->categoryItems = '';
        $this->price = 0;
        $this->isActive = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
    }

    public function setItemsId($aItemsId)
    {
        $this->itemsId = $this->db->escape_str($aItemsId);
    }
    public function getItemsId()
    {
        return $this->itemsId;
    }
    public function setItemsCode($aItemsCode)
    {
        $this->itemsCode = $this->db->escape_str($aItemsCode);
    }
    public function getItemsCode()
    {
        return $this->itemsCode;
    }
    public function setItemsName($aItemsName)
    {
        $this->itemsName = $this->db->escape_str($aItemsName);
    }
    public function getItemsName()
    {
        return $this->itemsName;
    }
    public function setItemsKind($aItemsKind)
    {
        $this->itemsKind = $this->db->escape_str($aItemsKind);
    }
    public function getItemsKind()
    {
        return $this->itemsKind;
    }
    public function setItemsInfo($aItemsInfo)
    {
        $this->itemsInfo = $this->db->escape_str($aItemsInfo);
    }
    public function getItemsInfo()
    {
        return $this->itemsInfo;
    }
    public function setItemsUnit($aItemsUnit)
    {
        $this->itemsUnit = $this->db->escape_str($aItemsUnit);
    }
    public function getItemsUnit()
    {
        return $this->itemsUnit;
    }
    public function setPrice($aPrice)
     {
        $this->price = $this->db->escape_str($aPrice);
     }
     public function getPrice()
     {
        return $this->price;
     }
    public function setItemsGroup($aItemsGroup)
    {
        $this->itemsGroup = $this->db->escape_str($aItemsGroup);
    }
    public function getItemsGroup()
    {
        return $this->itemsGroup;
    }
    public function setCategoryItems($aCategoryItems)
    {
        $this->categoryItems = $this->db->escape_str($aCategoryItems);
    }
    public function getCategoryItems()
    {
        return $this->categoryItems;
    }
    public function setIsActive($aIsActive)
    {
        $this->isActive = $this->db->escape_str($aIsActive);
    }
    public function getIsActive()
    {
        return $this->isActive;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

	function get_items()
    {
        $sql ='SELECT *
            FROM mst_items a 
            LEFT JOIN mst_items_unit b ON b.items_unit_id=a.items_unit 
            WHERE a.is_active="1" ORDER BY items_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_items_type($is_production)
    {
        $sql ='SELECT *
            FROM mst_items a 
            LEFT JOIN mst_items_unit b ON b.items_unit_id=a.items_unit 
            WHERE a.is_active="1" AND a.is_production="'.$is_production.'" ORDER BY items_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_items_all()
    {
        $sql ='SELECT *
            FROM mst_items a 
            WHERE a.is_active="1" ORDER BY items_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_items_sa($to_local,$id)
    {
        $sql    ='SELECT a.*
                    FROM mst_items a 
                    LEFT JOIN db_bumbu_transaction.trn_stock_by_doc c ON a.items_id=c.items_id ';
        if($to_local=='warehouse'){
            $sql   .=' AND c.warehouse_id="'.$id.'" ';
        }elseif($to_local=='project'){
            $sql   .=' AND c.project_id="'.$id.'" ';
        }

        $sql   .=' LEFT JOIN mst_items_unit b ON b.items_unit_id=a.items_unit 
            WHERE a.is_active="1" AND c.stock_by_doc_id IS NULL ORDER BY a.items_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDataCount()
    {
        $stQuery  = 'SELECT IFNULL(MAX(items_id),0) items_id FROM '.$this->myDb.'.'.$this->myTable.' '; 
        $query = $this->db->query($stQuery);
        return $query->row();
    }

    function get_nomor_dok($tahun){
        $query = $this->db->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(items_code,9,4))+1,4,'0'),'0001') nomor_dok,items_code 
                                FROM mst_items WHERE SUBSTRING(items_code,3,6)='".$tahun."'")->row();
        return $query;
    }

    function get_items_unit(){
        $sql    = "SELECT items_unit_id,items_unit_name FROM mst_items_unit";
        return $this->db->query($sql)->result();
    }

    public function act_form()
    {
        if($this->itemsId =='' || $this->itemsId == NULL )
        {
            $this->itemsId = '';
        }
        if($this->itemsCode =='' || $this->itemsCode == NULL )
        {
            $this->itemsCode = '';
        }
        if($this->itemsName =='' || $this->itemsName == NULL )
        {
            $this->itemsName = '';
        }
        if($this->itemsKind =='' || $this->itemsKind == NULL )
        {
            $this->itemsKind = '';
        }
        if($this->itemsInfo =='' || $this->itemsInfo == NULL )
        {
            $this->itemsInfo = '';
        }
        if($this->itemsUnit =='' || $this->itemsUnit == NULL )
        {
            $this->itemsUnit = '';
        }
        if($this->itemsGroup =='' || $this->itemsGroup == NULL )
        {
            $this->itemsGroup = '';
        }
        if($this->categoryItems =='' || $this->categoryItems == NULL )
        {
            $this->categoryItems = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->price =='' || $this->price == NULL )
        {
            $this->price = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        $stQuery .=   'items_id,'; 
        $stQuery .=   'items_code,'; 
        $stQuery .=   'items_name,'; 
        // $stQuery .=   'items_kind,'; 
        $stQuery .=   'items_info,'; 
        $stQuery .=   'price,';
        $stQuery .=   'price_sell,';
        $stQuery .=   'price_percent,';
        $stQuery .=   'items_unit,'; 
        // $stQuery .=   'items_group,'; 
        $stQuery .=   'category_items,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'is_production,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        $stQuery .=   '"'.$this->db->escape_str($this->itemsId).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->itemsCode).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->itemsName).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->itemsKind).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->itemsInfo).'",'; 
        $stQuery .=   $this->db->escape_str($this->price).',';
        $stQuery .=   '"'.$this->input->post('price_sell').'" ,';
        $stQuery .=   '"'.$this->input->post('items_price_percent').'" ,';
        $stQuery .=   '"'.$this->db->escape_str($this->itemsUnit).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->itemsGroup).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->categoryItems).'",'; 
        $stQuery .=   $this->db->escape_str($this->isActive).','; 
        $stQuery .=   $this->db->escape_str($this->input->post('is_production')).','; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('items_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    public function act_edit($id)
    {
        // test($id,1);
        if($this->itemsId =='' || $this->itemsId == NULL )
        {
            $this->itemsId = '';
        }
        if($this->itemsCode =='' || $this->itemsCode == NULL )
        {
            $this->itemsCode = '';
        }
        if($this->itemsName =='' || $this->itemsName == NULL )
        {
            $this->itemsName = '';
        }
        if($this->itemsInfo =='' || $this->itemsInfo == NULL )
        {
            $this->itemsInfo = '';
        }
        if($this->itemsUnit =='' || $this->itemsUnit == NULL )
        {
            $this->itemsUnit = '';
        }
        if($this->categoryItems =='' || $this->categoryItems == NULL )
        {
            $this->categoryItems = '';
        }
        if($this->price =='' || $this->price == NULL )
        {
            $this->price = 0;
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        // $stQuery .=   'items_id ="'.$this->db->escape_str($this->itemsId).'",'; 
        // $stQuery .=   'items_code ="'.$this->db->escape_str($this->itemsCode).'",'; 
        $stQuery .=   'items_name ="'.$this->db->escape_str($this->itemsName).'",'; 
        $stQuery .=   'items_info ="'.$this->db->escape_str($this->itemsInfo).'",'; 
        $stQuery .=   'items_unit ="'.$this->db->escape_str($this->itemsUnit).'",'; 
        $stQuery .=   'category_items ="'.$this->db->escape_str($this->categoryItems).'",'; 
        $stQuery .=   'price ='.$this->db->escape_str($this->price).','; 
        $stQuery .=   'price_sell ="'.$this->input->post('price_sell').'",'; 
        $stQuery .=   'price_percent ="'.$this->input->post('items_price_percent').'",'; 
        $stQuery .=   'is_production ="'.$this->input->post('is_production').'",'; 
        // $stQuery .=   'pic_input ='.$this->db->escape_str($this->picInput).','; 
        // $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'items_id = '.$id; 
        $this->db->query($stQuery); 
    }

    function act_delete_js(){
        $sql = "UPDATE mst_items SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE items_id = '".$this->input->post('items_id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

    function update_price($items_id,$price){
        $sql = "UPDATE mst_items SET price='".$price."', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE items_id = '".$items_id."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

}	