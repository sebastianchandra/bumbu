<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier_model extends CI_Model
{
    private $myDb = 'db_bumbu_master';
    private $myTable = 'mst_supplier';
    private $supplierId;
    private $supplierCode;
    private $supplierKindId;
    private $supplierName;
    private $npwp;
    private $siup;
    private $address;
    private $city;
    private $contact1;
    private $contact2;
    private $contact3;
    private $email1;
    private $email2;
    private $picSales;
    private $top;
    private $supplierInfo;
    private $isPpn;
    private $isActive;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;
    private $fileNpwp;
    
    public function __construct(){
        parent::__construct();
        $this->supplierId = 0;
        $this->supplierCode = '';
        $this->supplierKindId = 0;
        $this->supplierName = '';
        $this->npwp = '';
        $this->siup = '';
        $this->address = '';
        $this->city = '';
        $this->contact1 = '';
        $this->contact2 = '';
        $this->contact3 = '';
        $this->email1 = '';
        $this->email2 = '';
        $this->picSales = '';
        $this->top = 0;
        $this->supplierInfo = '';
        $this->isPpn = 0;
        $this->isActive = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
        $this->fileNpwp = '';
    }

    public function setSupplierId($aSupplierId)
    {
        $this->supplierId = $this->db->escape_str($aSupplierId);
    }
    public function getSupplierId()
    {
        return $this->supplierId;
    }
    public function setSupplierCode($aSupplierCode)
    {
        $this->supplierCode = $this->db->escape_str($aSupplierCode);
    }
    public function getSupplierCode()
    {
        return $this->supplierCode;
    }
    public function setSupplierKindId($aSupplierKindId)
    {
        $this->supplierKindId = $this->db->escape_str($aSupplierKindId);
    }
    public function getSupplierKindId()
    {
        return $this->supplierKindId;
    }
    public function setSupplierName($aSupplierName)
    {
        $this->supplierName = $this->db->escape_str($aSupplierName);
    }
    public function getSupplierName()
    {
        return $this->supplierName;
    }
    public function setNpwp($aNpwp)
    {
        $this->npwp = $this->db->escape_str($aNpwp);
    }
    public function getNpwp()
    {
       return $this->npwp;
    }
    public function setSiup($aSiup)
    {
        $this->siup = $this->db->escape_str($aSiup);
    }
    public function getSiup()
    {
        return $this->siup;
    }
    public function setAddress($aAddress)
    {
        $this->address = $this->db->escape_str($aAddress);
    }
    public function getAddress()
    {
        return $this->address;
    }
    public function setCity($aCity)
    {
        $this->city = $this->db->escape_str($aCity);
    }
    public function getCity()
    {
        return $this->city;
    }
    public function setContact1($aContact1)
    {
        $this->contact1 = $this->db->escape_str($aContact1);
    }
    public function getContact1()
    {
        return $this->contact1;
    }
    public function setContact2($aContact2)
    {
        $this->contact2 = $this->db->escape_str($aContact2);
    }
    public function getContact2()
    {
        return $this->contact2;
    }
    public function setContact3($aContact3)
    {
        $this->contact3 = $this->db->escape_str($aContact3);
    }
    public function getContact3()
    {
        return $this->contact3;
    }
    public function setEmail1($aEmail1)
    {
        $this->email1 = $this->db->escape_str($aEmail1);
    }
    public function getEmail1()
    {
        return $this->email1;
    }
    public function setEmail2($aEmail2)
    {
        $this->email2 = $this->db->escape_str($aEmail2);
    }
    public function getEmail2()
    {
        return $this->email2;
    }
    public function setPicSales($aPicSales)
    {
        $this->picSales = $this->db->escape_str($aPicSales);
    }
    public function getPicSales()
    {
        return $this->picSales;
    }
    public function setTop($aTop)
    {
        $this->top = $this->db->escape_str($aTop);
    }
    public function getTop()
    {
        return $this->top;
    }
    public function setSupplierInfo($aSupplierInfo)
    {
        $this->supplierInfo = $this->db->escape_str($aSupplierInfo);
    }
    public function getSupplierInfo()
    {
        return $this->supplierInfo;
    }
    public function setIsPpn($aIsPpn)
    {
        $this->isPpn = $this->db->escape_str($aIsPpn);
    }
    public function getIsPpn()
    {
        return $this->isPpn;
    }
    public function setIsActive($aIsActive)
    {
        $this->isActive = $this->db->escape_str($aIsActive);
    }
    public function getIsActive()
    {
        return $this->isActive;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }
    public function setFileNpwp($aFileNpwp)
    {
        $this->fileNpwp = $this->db->escape_str($aFileNpwp);
    }
    public function getFileNpwp()
    {
        return $this->fileNpwp;
    }

	function get_supplier (){
        $sql ='SELECT * FROM mst_supplier where is_active=1 ORDER BY supplier_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_supplier_payment (){
        $sql ='SELECT * FROM mst_supplier where is_active=1 ORDER BY supplier_name ASC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insert()
    {
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierCode =='' || $this->supplierCode == NULL )
        {
            $this->supplierCode = '';
        }
        if($this->supplierKindId =='' || $this->supplierKindId == NULL )
        {
            $this->supplierKindId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->npwp =='' || $this->npwp == NULL )
        {
            $this->npwp = '';
        }
        if($this->siup =='' || $this->siup == NULL )
        {
            $this->siup = '';
        }
        if($this->address =='' || $this->address == NULL )
        {
            $this->address = '';
        }
        if($this->city =='' || $this->city == NULL )
        {
            $this->city = '';
        }
        if($this->contact1 =='' || $this->contact1 == NULL )
        {
            $this->contact1 = '';
        }
        if($this->contact2 =='' || $this->contact2 == NULL )
        {
            $this->contact2 = '';
        }
        if($this->contact3 =='' || $this->contact3 == NULL )
        {
            $this->contact3 = '';
        }
        if($this->email1 =='' || $this->email1 == NULL )
        {
            $this->email1 = '';
        }
        if($this->email2 =='' || $this->email2 == NULL )
        {
            $this->email2 = '';
        }
        if($this->picSales =='' || $this->picSales == NULL )
        {
            $this->picSales = '';
        }
        if($this->top =='' || $this->top == NULL )
        {
            $this->top = 0;
        }
        if($this->supplierInfo =='' || $this->supplierInfo == NULL )
        {
            $this->supplierInfo = '';
        }
        if($this->isPpn =='' || $this->isPpn == NULL )
        {
            $this->isPpn = 0;
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        if($this->fileNpwp =='' || $this->fileNpwp == NULL )
        {
            $this->fileNpwp = '';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        // $stQuery .=   'supplier_id,'; 
        $stQuery .=   'supplier_code,'; 
        // $stQuery .=   'supplier_kind_id,'; 
        $stQuery .=   'supplier_name,'; 
        $stQuery .=   'npwp,'; 
        // $stQuery .=   'siup,'; 
        $stQuery .=   'address,'; 
        $stQuery .=   'city,'; 
        $stQuery .=   'contact1,'; 
        // $stQuery .=   'contact2,'; 
        // $stQuery .=   'contact3,'; 
        $stQuery .=   'email1,'; 
        // $stQuery .=   'email2,'; 
        $stQuery .=   'pic_sales,'; 
        $stQuery .=   'top,'; 
        $stQuery .=   'supplier_info,'; 
        // $stQuery .=   'is_ppn,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        // $stQuery .=   'file_npwp'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        // $stQuery .=   $this->db->escape_str($this->supplierId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->supplierCode).'",'; 
        // $stQuery .=   $this->db->escape_str($this->supplierKindId).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->supplierName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->npwp).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->siup).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->address).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->city).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->contact1).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->contact2).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->contact3).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->email1).'",'; 
        // $stQuery .=   '"'.$this->db->escape_str($this->email2).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->picSales).'",'; 
        $stQuery .=   $this->db->escape_str($this->top).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->supplierInfo).'",'; 
        // $stQuery .=   $this->db->escape_str($this->isPpn).','; 
        $stQuery .=   $this->db->escape_str($this->isActive).','; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->fileNpwp); 
        $stQuery .= '); '; 
        // test($stQuery,1);
        $this->db->query($stQuery); 
    }

    public function getById($id)
    {
          $this->db->select('*');
          $this->db->from($this->myDb.'.'.$this->myTable);
          $this->db->where('supplier_id', $this->db->escape_str($id));
          return $this->db->get()->row();
    }

    public function act_edit($id)
    {
        if($this->supplierId =='' || $this->supplierId == NULL )
        {
            $this->supplierId = 0;
        }
        if($this->supplierCode =='' || $this->supplierCode == NULL )
        {
            $this->supplierCode = '';
        }
        if($this->supplierKindId =='' || $this->supplierKindId == NULL )
        {
            $this->supplierKindId = 0;
        }
        if($this->supplierName =='' || $this->supplierName == NULL )
        {
            $this->supplierName = '';
        }
        if($this->npwp =='' || $this->npwp == NULL )
        {
            $this->npwp = '';
        }
        if($this->siup =='' || $this->siup == NULL )
        {
            $this->siup = '';
        }
        if($this->address =='' || $this->address == NULL )
        {
            $this->address = '';
        }
        if($this->city =='' || $this->city == NULL )
        {
            $this->city = '';
        }
        if($this->contact1 =='' || $this->contact1 == NULL )
        {
            $this->contact1 = '';
        }
        if($this->contact2 =='' || $this->contact2 == NULL )
        {
            $this->contact2 = '';
        }
        if($this->contact3 =='' || $this->contact3 == NULL )
        {
            $this->contact3 = '';
        }
        if($this->email1 =='' || $this->email1 == NULL )
        {
            $this->email1 = '';
        }
        if($this->email2 =='' || $this->email2 == NULL )
        {
            $this->email2 = '';
        }
        if($this->picSales =='' || $this->picSales == NULL )
        {
            $this->picSales = '';
        }
        if($this->top =='' || $this->top == NULL )
        {
            $this->top = 0;
        }
        if($this->supplierInfo =='' || $this->supplierInfo == NULL )
        {
            $this->supplierInfo = '';
        }
        if($this->isPpn =='' || $this->isPpn == NULL )
        {
            $this->isPpn = 0;
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        if($this->fileNpwp =='' || $this->fileNpwp == NULL )
        {
            $this->fileNpwp = '';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        // $stQuery .=   'supplier_id ='.$this->db->escape_str($this->supplierId).','; 
        // $stQuery .=   'supplier_code ="'.$this->db->escape_str($this->supplierCode).'",'; 
        // $stQuery .=   'supplier_kind_id ='.$this->db->escape_str($this->supplierKindId).','; 
        $stQuery .=   'supplier_name ="'.$this->db->escape_str($this->supplierName).'",'; 
        $stQuery .=   'npwp ="'.$this->db->escape_str($this->npwp).'",'; 
        // $stQuery .=   'siup ="'.$this->db->escape_str($this->siup).'",'; 
        $stQuery .=   'address ="'.$this->db->escape_str($this->address).'",'; 
        $stQuery .=   'city ="'.$this->db->escape_str($this->city).'",'; 
        $stQuery .=   'contact1 ="'.$this->db->escape_str($this->contact1).'",'; 
        // $stQuery .=   'contact2 ="'.$this->db->escape_str($this->contact2).'",'; 
        // $stQuery .=   'contact3 ="'.$this->db->escape_str($this->contact3).'",'; 
        $stQuery .=   'email1 ="'.$this->db->escape_str($this->email1).'",'; 
        // $stQuery .=   'email2 ="'.$this->db->escape_str($this->email2).'",'; 
        $stQuery .=   'pic_sales ="'.$this->db->escape_str($this->picSales).'",'; 
        $stQuery .=   'top ='.$this->db->escape_str($this->top).','; 
        $stQuery .=   'supplier_info ="'.$this->db->escape_str($this->supplierInfo).'",'; 
        // $stQuery .=   'is_ppn ='.$this->db->escape_str($this->isPpn).','; 
        // $stQuery .=   'is_active ='.$this->db->escape_str($this->isActive).','; 
        // $stQuery .=   'pic_input ='.$this->db->escape_str($this->picInput).','; 
        // $stQuery .=   'input_time ="'.$this->db->escape_str($this->inputTime).'",'; 
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'"'; 
        // $stQuery .=   'file_npwp ='.$this->db->escape_str($this->fileNpwp).' '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'supplier_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
    }

    function act_delete_js(){
        $sql = "UPDATE mst_supplier SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' WHERE supplier_id = '".$this->input->post('supplier_id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }

    function get_nomor_dok($tahun){
        $query = $this->db->query("SELECT IFNULL(LPAD(MAX(SUBSTRING(supplier_code,7,5))+1,5,'0'),'00001') nomor_dok FROM mst_supplier WHERE SUBSTR(supplier_code,3,4)='".$tahun."'")->row();
        return $query;
    }

}	