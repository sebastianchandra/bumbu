<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Warehouse_model extends CI_Model
{
    private $myDb = 'db_bumbu_master';
    private $myTable = 'mst_warehouse';
    private $warehouseId;
    private $warehouseCode;
    private $warehouseName;
    private $address;
    private $telp;
    private $city;
    private $remarks;
    private $isActive;
    private $picInput;
    private $inputTime;
    private $picEdit;
    private $editTime;

    public function __construct()
    {
        parent::__construct();
        $this->warehouseId = 0;
        $this->warehouseCode = '';
        $this->warehouseName = '';
        $this->address = '';
        $this->telp = '';
        $this->city = '';
        $this->remarks = '';
        $this->isActive = 0;
        $this->picInput = 0;
        $this->inputTime = '0000-00-00 00:00:00';
        $this->picEdit = 0;
        $this->editTime = '0000-00-00 00:00:00';
    }

    public function setWarehouseId($aWarehouseId)
    {
        $this->warehouseId = $this->db->escape_str($aWarehouseId);
    }
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }
    public function setWarehouseCode($aWarehouseCode)
    {
        $this->warehouseCode = $this->db->escape_str($aWarehouseCode);
    }
    public function getWarehouseCode()
    {
        return $this->warehouseCode;
    }
    public function setWarehouseName($aWarehouseName)
    {
        $this->warehouseName = $this->db->escape_str($aWarehouseName);
    }
    public function getWarehouseName()
    {
        return $this->warehouseName;
    }
    public function setAddress($aAddress)
    {
        $this->address = $this->db->escape_str($aAddress);
    }
    public function getAddress()
    {
        return $this->address;
    }
    public function setTelp($aTelp)
    {
        $this->telp = $this->db->escape_str($aTelp);
    }
    public function getTelp()
    {
        return $this->telp;
    }
    public function setCity($aCity)
    {
        $this->city = $this->db->escape_str($aCity);
    }
    public function getCity()
    {
        return $this->city;
    }
    public function setRemarks($aRemarks)
    {
        $this->remarks = $this->db->escape_str($aRemarks);
    }
    public function getRemarks()
    {
        return $this->remarks;
    }
    public function setIsActive($aIsActive)
    {
        $this->isActive = $this->db->escape_str($aIsActive);
    }
    public function getIsActive()
    {
        return $this->isActive;
    }
    public function setPicInput($aPicInput)
    {
        $this->picInput = $this->db->escape_str($aPicInput);
    }
    public function getPicInput()
    {
        return $this->picInput;
    }
    public function setInputTime($aInputTime)
    {
        $this->inputTime = $this->db->escape_str($aInputTime);
    }
    public function getInputTime()
    {
        return $this->inputTime;
    }
    public function setPicEdit($aPicEdit)
    {
        $this->picEdit = $this->db->escape_str($aPicEdit);
    }
    public function getPicEdit()
    {
        return $this->picEdit;
    }
    public function setEditTime($aEditTime)
    {
        $this->editTime = $this->db->escape_str($aEditTime);
    }
    public function getEditTime()
    {
        return $this->editTime;
    }

	function get_warehouse()
    {
        $sql ='SELECT warehouse_id,warehouse_code,warehouse_name,address,telp,city,remarks FROM mst_warehouse WHERE is_active="1" ORDER BY warehouse_id DESC';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function act_form()
    {
        if($this->warehouseId =='' || $this->warehouseId == NULL )
        {
            $this->warehouseId = 0;
        }
        if($this->warehouseCode =='' || $this->warehouseCode == NULL )
        {
            $this->warehouseCode = '';
        }
        if($this->warehouseName =='' || $this->warehouseName == NULL )
        {
            $this->warehouseName = '';
        }
        if($this->address =='' || $this->address == NULL )
        {
            $this->address = '';
        }
        if($this->telp =='' || $this->telp == NULL )
        {
            $this->telp = '';
        }
        if($this->city =='' || $this->city == NULL )
        {
            $this->city = '';
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'INSERT INTO '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= '( '; 
        // $stQuery .=   'warehouse_id,'; 
        // $stQuery .=   'warehouse_code,'; 
        $stQuery .=   'warehouse_name,'; 
        $stQuery .=   'address,'; 
        $stQuery .=   'telp,'; 
        $stQuery .=   'city,'; 
        $stQuery .=   'remarks,'; 
        $stQuery .=   'is_active,'; 
        $stQuery .=   'pic_input,'; 
        $stQuery .=   'input_time'; 
        // $stQuery .=   'pic_edit,'; 
        // $stQuery .=   'edit_time'; 
        $stQuery .= ') '; 
        $stQuery .= 'VALUES '; 
        $stQuery .= '( '; 
        // $stQuery .=   $this->db->escape_str($this->warehouseId).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->warehouseCode).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->warehouseName).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->address).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->telp).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->city).'",'; 
        $stQuery .=   '"'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   $this->db->escape_str($this->isActive).','; 
        $stQuery .=   $this->db->escape_str($this->picInput).','; 
        $stQuery .=   '"'.$this->db->escape_str($this->inputTime).'"'; 
        // $stQuery .=   $this->db->escape_str($this->picEdit).','; 
        // $stQuery .=   '"'.$this->db->escape_str($this->editTime).'"'; 
        $stQuery .= '); '; 
        $this->db->query($stQuery); 
    }

    public function getById($id)
    {
        $this->db->select('*');
        $this->db->from($this->myDb.'.'.$this->myTable);
        $this->db->where('warehouse_id', $this->db->escape_str($id));
        return $this->db->get()->row();
    }

    public function edit_act($id)
    {
        if($this->warehouseId =='' || $this->warehouseId == NULL )
        {
            $this->warehouseId = 0;
        }
        if($this->warehouseCode =='' || $this->warehouseCode == NULL )
        {
            $this->warehouseCode = '';
        }
        if($this->warehouseName =='' || $this->warehouseName == NULL )
        {
            $this->warehouseName = '';
        }
        if($this->address =='' || $this->address == NULL )
        {
            $this->address = '';
        }
        if($this->telp =='' || $this->telp == NULL )
        {
            $this->telp = '';
        }
        if($this->city =='' || $this->city == NULL )
        {
            $this->city = '';
        }
        if($this->remarks =='' || $this->remarks == NULL )
        {
            $this->remarks = '';
        }
        if($this->isActive =='' || $this->isActive == NULL )
        {
            $this->isActive = 0;
        }
        if($this->picInput =='' || $this->picInput == NULL )
        {
            $this->picInput = 0;
        }
        if($this->inputTime =='' || $this->inputTime == NULL )
        {
            $this->inputTime = '0000-00-00 00:00:00';
        }
        if($this->picEdit =='' || $this->picEdit == NULL )
        {
            $this->picEdit = 0;
        }
        if($this->editTime =='' || $this->editTime == NULL )
        {
            $this->editTime = '0000-00-00 00:00:00';
        }
        
        $stQuery  = 'UPDATE '.$this->myDb.'.'.$this->myTable.' '; 
        $stQuery .= 'SET '; 
        $stQuery .=   'warehouse_code ="'.$this->db->escape_str($this->warehouseCode).'",'; 
        $stQuery .=   'warehouse_name ="'.$this->db->escape_str($this->warehouseName).'",'; 
        $stQuery .=   'address ="'.$this->db->escape_str($this->address).'",'; 
        $stQuery .=   'telp ="'.$this->db->escape_str($this->telp).'",'; 
        $stQuery .=   'city ="'.$this->db->escape_str($this->city).'",'; 
        $stQuery .=   'remarks ="'.$this->db->escape_str($this->remarks).'",'; 
        $stQuery .=   'is_active ='.$this->db->escape_str($this->isActive).','; 
        $stQuery .=   'pic_edit ='.$this->db->escape_str($this->picEdit).','; 
        $stQuery .=   'edit_time ="'.$this->db->escape_str($this->editTime).'" '; 
        $stQuery .= 'WHERE '; 
        $stQuery .=   'warehouse_id = '.$this->db->escape_str($id).''; 
        $this->db->query($stQuery); 
    }

    function act_delete_js(){
        $sql = "UPDATE mst_warehouse SET is_active='0', pic_edit = '".$this->current_user['user_id']."', edit_time = '".dbnow()."' where warehouse_id = '".$this->input->post('warehouse_id')."'";
        //test($sql,1);
        $query = $this->db->query($sql);
        return $query;
    }








}	