<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends MY_Model
{

	function get_user($usr,$pwd)
    {
        $sql =' SELECT user_id,nip,`name`,username,`password`,id_user_group,is_active,pic_input,input_time,pic_edit,edit_time,id_user_level FROM mst_user '.
              ' WHERE username = "'.$usr.'"'.
              ' AND password = "'.$pwd.'" AND is_active = 1 ';
              
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    function detail_user($usr,$pwd){
    	$query = $this->db->query("SELECT a.`user_id`, a.nip, a.`name`, a.`username`, a.`id_user_group`, a.`id_user_level`, c.`name` AS group_name FROM mst_user a 
                LEFT JOIN mst_user_group c ON a.`id_user_group`=c.`id_user_group`
                WHERE a.`is_active`='1' AND a.`username`='".$usr."' AND a.`password`='".$pwd."'")->row();
            	
        return $query;
    }

    function get_user_id(){
        $query = $this->db->query("SELECT IFNULL(MAX(user_id)+1,1) user_id FROM mst_user")->row();
        return $query;
    }

    function aktifitas_user($status){
        if($status=='logged in'){
            $sql = "UPDATE mst_user SET last_login = '".dbnow()."',last_ip = '".$this->input->ip_address()."' 
                    WHERE user_id = '".$this->session->userdata('ses')['user_id']."'";

            $query = $this->db->query($sql);
        }

        $this->activity_user($status, 'User', 'mst_user');
    }

    function all_user(){
    	return $this->db->get('user')->result();
    }

    function user_all(){
        $sql =  'SELECT user_id,nip, `name`,username,`password` FROM mst_user WHERE is_active="1" order by user_id desc';
        $query = $this->db->query($sql)->result();
        return $query;
    }

    function act_delete_js(){
        $sql = "UPDATE mst_user SET is_active = '0',pic_edit = '".$this->current_user['user_id']."',edit_time = '".dbnow()."' WHERE user_id = '".$this->input->post('user_id')."'";

        $query = $this->db->query($sql);
        return $query;
    }

    function act_form(){
        
        $data_id    = $this->get_user_id();
        $user_id    = $data_id->user_id;

        $nip        = $this->security->xss_clean($this->db->escape_str($this->input->post('nip')));
        $name       = $this->security->xss_clean($this->db->escape_str($this->input->post('name')));
        $username   = $this->security->xss_clean($this->db->escape_str($this->input->post('username')));
        $password1  = $this->security->xss_clean($this->db->escape_str($this->input->post('password1')));

        $sql = "INSERT INTO mst_user (nip,`name`,username,`password`,is_active,pic_input,input_time,id_user_group,id_user_level)VALUES
            ('".$nip."','".$name."','".$username."','".$password1."','1','".$this->current_user['user_id']."','".dbnow()."',0,0)";
        // test($sql,1);
        // $sql = "INSERT INTO mst_user (nip,`name`,username,`password`,id_user_group,id_user_level,is_active,pic_input,input_time) VALUES 
        //     ('".$nip."','".$name."','".$username."','".$password1."','".$users_group."','".$user_level."','1','".$this->current_user['user_id']."','".dbnow()."');";

        // foreach ($this->input->post('lang') as $key => $value) {
        //     $sql_user_group = "INSERT INTO mst_user_group_permissions (user_id,id_user_group) VALUES ('".$user_id."','".$value."')";
        //     $query = $this->db->query($sql_user_group);
        // }

        $query = $this->db->query($sql);
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }

    }

    function data_user($id){
        $query = $this->db->query("SELECT user_id,nip,`name`,username,`password` FROM mst_user WHERE user_id='".$id."'")->row();
        return $query;
    }

    function act_edit(){
                

        $sql    = "UPDATE mst_user
                    SET nip = '".$this->security->xss_clean($this->db->escape_str($this->input->post('nip')))."',
                      `name` = '".$this->security->xss_clean($this->db->escape_str($this->input->post('name')))."',
                      username = '".$this->security->xss_clean($this->db->escape_str($this->input->post('username')))."',
                      `password` = '".$this->security->xss_clean($this->db->escape_str($this->input->post('password1')))."',
                      pic_edit = '".$this->current_user['user_id']."',
                      edit_time = '".dbnow()."'
                    WHERE user_id = '".$this->security->xss_clean($this->db->escape_str($this->input->post('user_id')))."'";
        $query = $this->db->query($sql);
        
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function act_permission(){
        // $menu_id    = $this->security->xss_clean($this->db->escape_str($this->input->post('menu_id')));
        // $company_id = $this->security->xss_clean($this->db->escape_str($this->input->post('company_id')));

        $user_id    = $this->security->xss_clean($this->db->escape_str($this->input->post('user_id')));
        $delete = $this->delete_permission($user_id);

        if($delete=='true'){
            if($this->input->post('lang')!=''){
                foreach ($this->input->post('lang') as $key => $value) {
                    $sql_user_group = "INSERT INTO mst_menu_permissions (user_id,menu_id) VALUES ('".$user_id."','".$value."')";
                    $query = $this->db->query($sql_user_group);
                }
            }
        }
    }

    function delete_permission($id){
        $sql    = "DELETE FROM mst_menu_permissions WHERE user_id = '".$id."'";
        $query  = $this->db->query($sql);

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function delete_permission_cp($id){
        $sql    = "DELETE FROM mst_user_company WHERE user_id = '".$id."'";

        $query  = $this->db->query($sql);

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function delete_permission_group($id){
        $sql    = "DELETE FROM mst_user_group_permissions WHERE user_id = '".$id."'";
        $query  = $this->db->query($sql);

        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function insert_permission($user_id,$menu_id){
        $sql    = "INSERT INTO mst_menu_permissions (user_id,menu_id) VALUES ('".$user_id."','".$menu_id."')";
        $query  = $this->db->query($sql);
        return $query;
    }

    function insert_permission_cp($user_id,$company_id){
        $sql    = "INSERT INTO mst_user_company (company_id,user_id) VALUES ('".$company_id."','".$user_id."')";
        $query  = $this->db->query($sql);
        return $query;
    }

    function act_password(){
        $sql    = "UPDATE mst_user
                    SET username = '".$this->security->xss_clean($this->db->escape_str($this->input->post('username')))."',
                      `password` = '".$this->security->xss_clean($this->db->escape_str($this->input->post('password1')))."',
                      pic_edit = '".$this->current_user['user_id']."',
                      edit_time = '".dbnow()."'
                    WHERE user_id = '".$this->security->xss_clean($this->db->escape_str($this->input->post('user_id')))."'";
        $query = $this->db->query($sql);
        
        if ($query === false){
            return "ERROR INSERTT";
        }else{
            return $query; 
        }
    }

    function permission_user_group($id){
        $sql    = "SELECT a.id_user_group, a.name,b.user_id FROM mst_user_group a 
                    LEFT JOIN mst_user_group_permissions b ON a.id_user_group=b.id_user_group AND b.user_id='".$id."'  ORDER BY id_user_group";
        $query = $this->db->query($sql)->result();
        return $query;
    }

    function permission_user_group_login($id){
        $sql    = "SELECT a.user_id,a.id_user_group,b.name FROM mst_user_group_permissions a LEFT JOIN mst_user_group b ON a.id_user_group=b.id_user_group 
        WHERE a.user_id='".$id."'";
        $query = $this->db->query($sql)->result();
        return $query;
    }
}	