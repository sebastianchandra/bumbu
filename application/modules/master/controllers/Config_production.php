<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_production extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('master/config_production_01');
        $this->load->model('master/config_production_02');        
    }

    function index(){  
        $this->session->set_userdata('ses_menu', array('active_menu' => 'master', 'active_submenu' => 'master/config_production'));
        $this->isMenu();

        $data['data_config']        = $this->config_production_01->getAll();
        $this->template->load('body', 'master/config_production/cp_view',$data);
    }

    function form(){
        $this->session->unset_userdata('new_pro');

        $this->load->model('master/items_model');

        $new_pro = $this->session->userdata('new_pro');

        if(!$new_pro){
            $new_pro = array(
                'items' => array(),
                'budget' => array()
            );
        }

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_mentah']    = $this->items_model->get_items_type(0);
        $data['data_produksi']  = $this->items_model->get_items_type(1);
        $data['new_pro']         = $new_pro;
        // test($data,1);
        $this->template->load('body', 'master/config_production/cp_form', $data);
    }

    function add_item(){
        // test($_POST['ppn'],1);
        if(!isset($_POST['item_id'])) return;
        $new_pro     = $this->session->userdata('new_pro');
        // test($this->session->userdata('new_pro'),1);
        $items      = $new_pro['items'];

        $new_pro['items'][] = array(
            // 'mbp_id'        => $this->input->post('mbp_id'),
            'det_id'        => $this->input->post('det_id'),
            'item_id'       => $this->input->post('item_id'),
            'item_name'     => $this->input->post('item_name'),
            'item_qty'      => $this->input->post('item_qty'),
            'satuan'        => $this->input->post('satuan'),
            'item_info'     => $this->input->post('item_info')
        );

        $exist = false;
        // test($new_pro,0);
        $this->session->set_userdata('new_pro', $new_pro);         
    }

    function remove_item(){
        //test($_GET['index_id'],0);
        if(!isset($_GET['index_id'])) return;
        $index_id = $this->input->get('index_id');
        $new_pro = $this->session->userdata('new_pro');

        $items = $new_pro['items'];
        // test($items,1);
        foreach($items as $key=>$val){
            //test($val['item_id'],0);
            if($val['item_id'] == $index_id){
                // $mbp_id     = $new_pro['items'][$key]['mbp_id'];
                // $item_qty   = str_replace(',','',$this->security->xss_clean($this->db->escape_str($new_pro['items'][$key]['item_qty'])));
                // test($mbp_id.' '.$item_qty,1);
                unset($new_pro['items'][$key]);
                $new_pro['items'] = array_values($new_pro['items']);
                break;
            }
        }
        // test($new_pro,1);
        $this->session->set_userdata('new_pro', $new_pro);
        jsout(array('success'=>1)); 
    }

    function form_act(){
        $this->db->trans_begin();


        // $periode        = substr($this->input->post('pr_date'),0,4).substr($this->input->post('pr_date'),5,2);
        // $data_no        = $this->purchase_requisition_model->get_nomor_dok($periode);
        // $nomor_dok      = $data_no->nomor_dok;
        // $pr_no          = 'PRS'.$periode.$nomor_dok;

        $id             = $this->config_production_01->getDataCount_id()->production_01_id;

        $this->config_production_01->setProduction01Id($this->security->xss_clean($id));
        $this->config_production_01->setItemsId($this->security->xss_clean($_POST['production_items_id']));
        $this->config_production_01->setItemsName($this->security->xss_clean($_POST['production_items_name']));
        $this->config_production_01->setQty($this->security->xss_clean($_POST['qty']));
        $this->config_production_01->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->config_production_01->setInputTime($this->security->xss_clean(dbnow()));
        $save = $this->config_production_01->insert();

        $new_pro         = $this->session->userdata('new_pro');
        // test($new_pro,1);
        foreach ($new_pro['items'] as $key => $value) {
            // $this->config_production_02->setProduction02Id($this->security->xss_clean($_POST['production02Id']));
            $this->config_production_02->setProduction01Id($this->security->xss_clean($id));
            $this->config_production_02->setItemsId($this->security->xss_clean($value['item_id']));
            $this->config_production_02->setItemsName($this->security->xss_clean($value['item_name']));
            $this->config_production_02->setItemsUnit($this->security->xss_clean($value['satuan']));
            $this->config_production_02->setQty($this->security->xss_clean($value['item_qty']));
            $this->config_production_02->insert();
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            jsout(array('success' => false));
        }else{
            $this->db->trans_commit();
            jsout(array('success' => true, 'status' => $save));
        }        
        
    }

    function edit($id){
        $this->session->unset_userdata('new_pro');
        $this->load->model('master/items_model');
        $new_pro = $this->session->userdata('new_pro');

        $detail     = $this->config_production_02->getByIdCr($id)->result();
        $tdetail    = $this->config_production_02->getByIdCr($id)->num_rows();
        // test($detail,1);

        $new_pro = $this->session->userdata('new_pro');

        if($tdetail==0){
            $new_pro = array(
                'items' => array()
            );
        }else{
            foreach($detail as $key=>$val){
                $new_pro['items'][$key] = array(
                    'det_id'        => $val->production_02_id,
                    'item_id'       => $val->items_id,
                    'item_name'     => $val->items_name,
                    'satuan'        => $val->items_unit,
                    'item_qty'      => $val->qty,
                    'item_info'     => ''
                );
            }
        }
        $this->session->set_userdata('new_pro', $new_pro);

        // $data['data_company']   = array(array('code' => 'S', 'name' => 'PT. Sangati'),array('code' => 'L', 'name' => 'PT. LM System Indonesia'));
        $data['data_company']   = array('company_id' => $this->current_user['company_id'], 'company_name' => $this->current_user['company_name']);
        $data['data_mentah']    = $this->items_model->get_items_type(0);
        $data['data_produksi']  = $this->items_model->get_items_type(1);
        $data['header']         = $this->config_production_01->getById($id);
        $data['new_pro']        = $new_pro;
        // test($data,1);
        $this->template->load('body', 'master/config_production/cp_edit', $data);
    }

    function edit_act(){

        $id = $this->security->xss_clean($_POST['production_01_id']);

        $this->config_production_02->delete($id);
        $new_pro         = $this->session->userdata('new_pro');
        
        foreach ($new_pro['items'] as $key => $value) {
            // $this->config_production_02->setProduction02Id($this->security->xss_clean($_POST['production02Id']));
            $this->config_production_02->setProduction01Id($this->security->xss_clean($id));
            $this->config_production_02->setItemsId($this->security->xss_clean($value['item_id']));
            $this->config_production_02->setItemsName($this->security->xss_clean($value['item_name']));
            $this->config_production_02->setItemsUnit($this->security->xss_clean($value['satuan']));
            $this->config_production_02->setQty($this->security->xss_clean($value['item_qty']));
            $this->config_production_02->insert();
        }

        // $this->config_production_01->setProduction01Id($this->security->xss_clean($id));
        $this->config_production_01->setItemsId($this->security->xss_clean($_POST['production_items_id']));
        $this->config_production_01->setItemsName($this->security->xss_clean($_POST['production_items_name']));
        $this->config_production_01->setQty($this->security->xss_clean($_POST['qty']));
        $this->config_production_01->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->config_production_01->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->config_production_01->update($id);

        $this->session->unset_userdata('new_pro');
        jsout(array('success' => true, 'status' => $update ));
    }

    function reject_js(){
        $delete = $this->config_production_01->act_reject_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    function delete_js(){
        $delete = $this->purchase_requisition_model->act_delete_js();
        jsout(array('success' => true, 'status' => $delete ));
    }

    

    function view_popup($id){

        $header         = $this->purchase_requisition_model->getById($id);
        $detail         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $myData = array();
        $no     = 0;
        foreach ($detail as $key => $row) {
            $no     = $no+1;
            $myData[] = array(
                $no,
                $row['items_code'],
                $row['items_name'],     
                number_format($row['qty'],2)        
            );            
        }  

        return jsout(array('header' => $header, 'detail'=> json_encode($myData)));

    }

    function view_print($id){
        $data['header']         = $this->purchase_requisition_model->print_header($id);
        $data['detail']         = $this->purchase_requisition_model02->print_detail($id);
        // $data['detail']         = $this->purchase_requisition_model02->getByIdPr($id)->result_array();

        $this->template->load('body_print', 'master/purchase_requisition/pr_print', $data);
    }





}

// Penambahan PR Print
?>