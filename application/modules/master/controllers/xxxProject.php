<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/project'));  
        $this->isMenu();
        $this->load->model('master/project_model');
    }


	function index(){
        $data['data_project']     = $this->project_model->get_project_view();
        $this->template->load('body', 'master/project/project_view',$data);
	}

    function form(){
        $this->template->load('body', 'master/project/project_form');
    }

    function form_act(){
        $this->project_model->setProjectName($this->security->xss_clean($this->input->post('name')));
        $this->project_model->setProjectLocation($this->security->xss_clean($this->input->post('location')));
        $this->project_model->setProjectInfo($this->security->xss_clean($this->input->post('info')));
        $this->project_model->setIsActive($this->security->xss_clean(1));
        $this->project_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->project_model->setInputTime($this->security->xss_clean(dbnow()));
        $save   = $this->project_model->act_form();
        jsout(array('success' => true, 'status' => $save ));
    }
    
    function edit($id){
        $data['data_project']       = $this->project_model->getById($id);
        $this->template->load('body', 'master/project/project_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['project_id']);
        $this->project_model->setProjectName($this->security->xss_clean($_POST['name']));
        $this->project_model->setProjectLocation($this->security->xss_clean($_POST['location']));
        $this->project_model->setProjectInfo($this->security->xss_clean($_POST['info']));
        $this->project_model->setIsActive($this->security->xss_clean(1));
        $this->project_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->project_model->setEditTime($this->security->xss_clean(dbnow()));

        $update   = $this->project_model->act_edit($id);
        jsout(array('success' => true, 'status' => $update ));
    }

    function delete_js(){
        $delete = $this->project_model->act_delete_js(0);
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function close_js(){
        $delete = $this->project_model->act_delete_js(2);
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function select_project(){
        $result     = $this->project_model->get_project();
        echo json_encode($result);
    }

}
?>