<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Crew extends MY_Controller
{
     /* START CONSTRUCTOR */
     public function __construct(){
          parent::__construct();
          $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/crew'));  
          $this->isMenu();
          $this->load->model('master/crew_model');
     }
     /* END CONSTRUCTOR */

     function index(){
          $data['data_crew']     = $this->crew_model->get_data();
          $this->template->load('body', 'master/crew/crew_view',$data);
     }

     function form(){
        $this->template->load('body', 'master/crew/crew_form');
    }
     
     /* START INSERT */
     public function form_act()
     {
          $periode       = date('Y').date('m');
          $no_doc        = $this->crew_model->get_nomor_doc($periode)->no_urut;
          $id            = $periode.$no_doc;

     	$this->crew_model->setCrewId($this->security->xss_clean($id));
     	$this->crew_model->setFullName($this->security->xss_clean($_POST['full_name']));
     	$this->crew_model->setGender($this->security->xss_clean($_POST['gender']));
     	$this->crew_model->setPlaceOfBirth($this->security->xss_clean($_POST['place_of_birth']));
     	$this->crew_model->setDateOfBirth($this->security->xss_clean($_POST['date_of_birth']));
     	$this->crew_model->setIdCardNo($this->security->xss_clean($_POST['id_card_no']));
     	$this->crew_model->setNpwpNo($this->security->xss_clean($_POST['npwp_no']));
     	$this->crew_model->setCrewAddress($this->security->xss_clean($_POST['crew_address']));
     	$this->crew_model->setMartialStatus($this->security->xss_clean($_POST['martial_status']));
     	$this->crew_model->setPosition($this->security->xss_clean($_POST['position']));
     	$this->crew_model->setCellNo($this->security->xss_clean($_POST['cell_no']));
     	$this->crew_model->setIsActive($this->security->xss_clean('1'));
     	$this->crew_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
     	$this->crew_model->setInputTime($this->security->xss_clean(dbnow()));
     	$save    = $this->crew_model->insert();

          jsout(array('success' => true, 'status' => $save ));
     }
     /* END INSERT */
     function edit($id){
        $data['crew']          = $this->crew_model->getById($id);
        $this->template->load('body', 'master/crew/crew_edit', $data);
     }

     /* START UPDATE */
     public function upd()
     {
     	$id = $this->security->xss_clean($_POST['crew_id']);
     	$this->crew_model->setFullName($this->security->xss_clean($_POST['full_name']));
     	$this->crew_model->setGender($this->security->xss_clean($_POST['gender']));
     	$this->crew_model->setPlaceOfBirth($this->security->xss_clean($_POST['place_of_birth']));
     	$this->crew_model->setDateOfBirth($this->security->xss_clean($_POST['date_of_birth']));
     	$this->crew_model->setIdCardNo($this->security->xss_clean($_POST['id_card_no']));
     	$this->crew_model->setNpwpNo($this->security->xss_clean($_POST['npwp_no']));
     	$this->crew_model->setCrewAddress($this->security->xss_clean($_POST['crew_address']));
     	$this->crew_model->setMartialStatus($this->security->xss_clean($_POST['martial_status']));
     	$this->crew_model->setPosition($this->security->xss_clean($_POST['position']));
     	$this->crew_model->setCellNo($this->security->xss_clean($_POST['cell_no']));
     	$this->crew_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
     	$this->crew_model->setEditTime($this->security->xss_clean(dbnow()));
     	$save    = $this->crew_model->update($id);

          jsout(array('success' => true, 'status' => $save ));
     }
     /* END UPDATE */
     /* START DELETE */
     function delete_js(){
        $delete = $this->crew_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->crew_model->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */
}
?>
