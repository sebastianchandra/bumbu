<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Config_journal extends MY_Controller
{
     /* START CONSTRUCTOR */
     public function __construct(){
          parent::__construct();
          $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/config_journal'));  
          $this->isMenu();
          $this->load->model('master/config_journal_model');
     }
     /* END CONSTRUCTOR */

     function index(){
          $data['data_config_journal']     = $this->config_journal_model->get_data();
          $this->template->load('body', 'master/config_journal/cj_view',$data);
     }

     function form(){
        $this->template->load('body', 'master/config_journal/cj_form');
    }
     
     /* START INSERT */
     public function form_act()
     {
          // $periode       = date('Y').date('m');
          // $no_doc        = $this->config_journal_model->get_nomor_doc($periode)->no_urut;
          // $id            = $periode.$no_doc;

          $jc_id         = $this->config_journal_model->get_last_id()->jc_id;

     	$this->config_journal_model->setJcId($this->security->xss_clean($jc_id));
          $this->config_journal_model->setTrnCode($this->security->xss_clean($_POST['trn_code']));
          $this->config_journal_model->setDbCoaId($this->security->xss_clean($_POST['db_coa_id']));
          $this->config_journal_model->setDbCoaCode($this->security->xss_clean($_POST['db_coa_code']));
          $this->config_journal_model->setDbCoaDetail($this->security->xss_clean($_POST['db_coa_detail']));
          $this->config_journal_model->setDbCoaName($this->security->xss_clean($_POST['db_coa_name']));
          $this->config_journal_model->setCrCoaId($this->security->xss_clean($_POST['cr_coa_id']));
          $this->config_journal_model->setCrCoaCode($this->security->xss_clean($_POST['cr_coa_code']));
          $this->config_journal_model->setCrCoaDetail($this->security->xss_clean($_POST['cr_coa_detail']));
          $this->config_journal_model->setCrCoaName($this->security->xss_clean($_POST['cr_coa_name']));
     	$save    = $this->config_journal_model->insert();

          jsout(array('success' => true, 'status' => $save ));
     }
     /* END INSERT */
     function edit($id){
        $data['detail']          = $this->config_journal_model->getById($id);
        $this->template->load('body', 'master/config_journal/cj_edit', $data);
     }

     /* START UPDATE */
     public function edit_act()
     {
     	$id = $this->security->xss_clean($_POST['jc_id']);
          $this->config_journal_model->getObjectById($id);
          // $this->config_journal_model->setJcId($this->security->xss_clean($_POST['jcId']));
          $this->config_journal_model->setTrnCode($this->security->xss_clean($_POST['trn_code']));
          $this->config_journal_model->setDbCoaId($this->security->xss_clean($_POST['db_coa_id']));
          $this->config_journal_model->setDbCoaCode($this->security->xss_clean($_POST['db_coa_code']));
          $this->config_journal_model->setDbCoaDetail($this->security->xss_clean($_POST['db_coa_detail']));
          $this->config_journal_model->setDbCoaName($this->security->xss_clean($_POST['db_coa_name']));
          $this->config_journal_model->setCrCoaId($this->security->xss_clean($_POST['cr_coa_id']));
          $this->config_journal_model->setCrCoaCode($this->security->xss_clean($_POST['cr_coa_code']));
          $this->config_journal_model->setCrCoaDetail($this->security->xss_clean($_POST['cr_coa_detail']));
          $this->config_journal_model->setCrCoaName($this->security->xss_clean($_POST['cr_coa_name']));
          $save    = $this->config_journal_model->update($id);

          jsout(array('success' => true, 'status' => $save ));
     }
     /* END UPDATE */
     /* START DELETE */
     function delete_js(){
          $delete = $this->config_journal_model->act_delete_js($this->input->post('jc_id'));
          //test($delete,1);
          jsout(array('success' => true, 'status' => $delete ));
     }
     /* END DELETE */
     /* START GET ALL */
     public function loadAll()
     {
     	$rs = $this->config_journal_model->getAll();
     	echo json_encode($rs); 
     }
     /* END GET ALL */
}
?>
