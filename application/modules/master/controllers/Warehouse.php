<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/warehouse'));  
        $this->isMenu();
        $this->load->model('master/warehouse_model');
    }


    function index(){
        $data['data_warehouse']     = $this->warehouse_model->get_warehouse();
        $this->template->load('body', 'master/warehouse/warehouse_view',$data);
    }

    function form(){
        $this->template->load('body', 'master/warehouse/warehouse_form');
    }

    function form_act(){
        // $this->warehouse_model->setWarehouseCode($this->security->xss_clean($_POST['warehouseCode']));
        $this->warehouse_model->setWarehouseName($this->security->xss_clean($_POST['name']));
        $this->warehouse_model->setAddress($this->security->xss_clean($_POST['address']));
        $this->warehouse_model->setTelp($this->security->xss_clean($_POST['telp']));
        $this->warehouse_model->setCity($this->security->xss_clean($_POST['city']));
        $this->warehouse_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->warehouse_model->setIsActive($this->security->xss_clean(1));
        $this->warehouse_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->warehouse_model->setInputTime($this->security->xss_clean(dbnow()));
        $save   = $this->warehouse_model->act_form();
        jsout(array('success' => true, 'status' => $save ));

    }
    
    function edit($id){
        $data['data_warehouse']       = $this->warehouse_model->getById($id);
        $this->template->load('body', 'master/warehouse/warehouse_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['warehouse_id']);

        $this->warehouse_model->setWarehouseName($this->security->xss_clean($_POST['name']));
        $this->warehouse_model->setAddress($this->security->xss_clean($_POST['address']));
        $this->warehouse_model->setTelp($this->security->xss_clean($_POST['telp']));
        $this->warehouse_model->setCity($this->security->xss_clean($_POST['city']));
        $this->warehouse_model->setRemarks($this->security->xss_clean($_POST['remarks']));
        $this->warehouse_model->setIsActive($this->security->xss_clean(1));
        $this->warehouse_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->warehouse_model->setEditTime($this->security->xss_clean(dbnow()));
        $update   = $this->warehouse_model->edit_act($id);

        jsout(array('success' => true, 'status' => $update ));
    }

    function delete_js(){
        $delete = $this->warehouse_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function select_warehouse(){
        $result     = $this->warehouse_model->get_warehouse();
        echo json_encode($result);
    }

}
?>