<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/supplier'));  
        $this->isMenu();
        $this->load->model('master/supplier_model');
    }


	function index(){
        $data['data_supplier']     = $this->supplier_model->get_supplier();
        $this->template->load('body', 'master/supplier/supplier_view',$data);
	}

    function form(){
        $this->template->load('body', 'master/supplier/supplier_form');
    }

    function form_act(){
        $tahun              = date('Y');
        $nomor_dok          = $this->supplier_model->get_nomor_dok($tahun)->nomor_dok;
        $supplier_code      = 'SP'.$tahun.$nomor_dok;
        // test($supplier_code,1);
        $this->supplier_model->setSupplierCode($this->security->xss_clean($supplier_code));
        $this->supplier_model->setSupplierName($this->security->xss_clean($_POST['supplier_name']));
        $this->supplier_model->setNpwp($this->security->xss_clean($_POST['npwp']));
        // $this->supplier_model->setSiup($this->security->xss_clean($_POST['siup']));
        $this->supplier_model->setAddress($this->security->xss_clean($_POST['address']));
        $this->supplier_model->setCity($this->security->xss_clean($_POST['city']));
        $this->supplier_model->setContact1($this->security->xss_clean($_POST['contact1']));
        // $this->supplier_model->setContact2($this->security->xss_clean($_POST['contact2']));
        // $this->supplier_model->setContact3($this->security->xss_clean($_POST['contact3']));
        $this->supplier_model->setEmail1($this->security->xss_clean($_POST['email']));
        // $this->supplier_model->setEmail2($this->security->xss_clean($_POST['email2']));
        $this->supplier_model->setPicSales($this->security->xss_clean($_POST['pic_sales']));
        $this->supplier_model->setTop($this->security->xss_clean($_POST['top']));
        $this->supplier_model->setSupplierInfo($this->security->xss_clean($_POST['supplier_info']));
        // $this->supplier_model->setIsPpn($this->security->xss_clean($_POST['isPpn']));
        $this->supplier_model->setIsActive($this->security->xss_clean(1));
        $this->supplier_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->supplier_model->setInputTime($this->security->xss_clean(dbnow()));
        // $this->supplier_model->setPicEdit($this->security->xss_clean($_POST['picEdit']));
        // $this->supplier_model->setEditTime($this->security->xss_clean($_POST['editTime']));
        // $this->supplier_model->setFileNpwp($this->security->xss_clean($_POST['fileNpwp']));
        $save   = $this->supplier_model->insert();

        jsout(array('success' => true, 'status' => $save ));
    }
    
    function edit($id){
        $data['data_supplier']       = $this->supplier_model->getById($id);
        $this->template->load('body', 'master/supplier/supplier_edit', $data);
    }

    function edit_act(){
        $id = $this->security->xss_clean($_POST['supplier_id']);
        $this->supplier_model->setSupplierName($this->security->xss_clean($_POST['supplier_name']));
        $this->supplier_model->setNpwp($this->security->xss_clean($_POST['npwp']));
        $this->supplier_model->setAddress($this->security->xss_clean($_POST['address']));
        $this->supplier_model->setCity($this->security->xss_clean($_POST['city']));
        $this->supplier_model->setContact1($this->security->xss_clean($_POST['contact1']));
        $this->supplier_model->setEmail1($this->security->xss_clean($_POST['email']));
        $this->supplier_model->setPicSales($this->security->xss_clean($_POST['pic_sales']));
        $this->supplier_model->setTop($this->security->xss_clean($_POST['top']));
        $this->supplier_model->setSupplierInfo($this->security->xss_clean($_POST['supplier_info']));
        $this->supplier_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->supplier_model->setEditTime($this->security->xss_clean(dbnow()));

        $update   = $this->supplier_model->act_edit($id);
        jsout(array('success' => true, 'status' => $update ));
    }

    function delete_js(){
        $delete = $this->supplier_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

}
?>