<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coa extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/coa'));  
        $this->isMenu();
        $this->load->model('master/coa_model');
    }

    function index(){
        $data['data_coa']     = $this->coa_model->get_coa();
        $this->template->load('body', 'master/coa/coa_view',$data);
    }

    function form(){
        $data['level_budget']       = $this->coa_model->get_coa_level();
        $this->template->load('body', 'master/coa/coa_input',$data);
    }

    function get_bp(){
        // $this->load->model('master/items_model');
        $data = array(); 

        $start      = $this->input->get('start');
        $length     = $this->input->get('length');
        $search     = $this->input->get('search');

        $id_level   = $this->input->get('id');
        // test($start.' '.$length.' '.$search['value'].' '.$id.' '.$request_id,1);

        $list = $this->coa_model->get_all_bp($start,$length,$search['value'],$id_level);
        if(is_for($list)){
            foreach ($list as $row) {
                $data[] = array(
                    'coa_id'        => $row->coa_id,
                    'coa_detail'    => $row->coa_detail,
                    'coa_name'      => $row->coa_name
                );
            }
        }     

        if ($search['value']) {
            $total   = $this->coa_model->get_count_display($start,$length,$search['value'],$id_level);
        }else {
            $total   = $this->coa_model->get_count($id_level);
        }
        // $display = $this->item_model->get_count_display($start,$length,$search['value']);
        // jsout(array('success'=>1, 'aaData'=>$data));
        jsout(array('success'=>1, 'aaData'=>$data,'iTotalRecords'=>$total,'iTotalDisplayRecords'=>$total));
    }

    function get_bp_new($id_level){
        // $header         = $this->purchase_requisition_model->getByPrProcess();
        $header         = $this->coa_model->get_all_bp_new($id_level);

        $myData = array();
        foreach ($header as $key => $row) {
            $myData[] = array(
                $row->coa_id,       
                $row->coa_detail,
                $row->coa_name
            );            
        }  

        return jsout(array('detail'=> json_encode($myData)));
    }

    function get_low_coa_new(){
        $header         = $this->coa_model->get_all_coa_new();

        $myData = array();
        foreach ($header as $key => $row) {
            $myData[] = array(
                $row->coa_id,      
                $row->coa_code, 
                $row->coa_detail,
                $row->coa_name
            );            
        }  

        return jsout(array('detail'=> json_encode($myData)));
    }

    function get_low_coa_special(){
        $header         = $this->coa_model->get_all_coa_special();

        $myData = array();
        foreach ($header as $key => $row) {
            $myData[] = array(
                $row->coa_id,      
                $row->coa_code, 
                $row->coa_detail,
                $row->coa_name
            );            
        }  

        return jsout(array('detail'=> json_encode($myData)));
    }

    function get_low_coa(){
        // $this->load->model('master/items_model');
        $data = array(); 

        $start      = $this->input->get('start');
        $length     = $this->input->get('length');
        $search     = $this->input->get('search');

        // $id_level   = $this->input->get('id');
        // test($start.' '.$length.' '.$search['value'].' '.$id.' '.$request_id,1);

        $list = $this->coa_model->get_all_coa($start,$length,$search['value']);
        if(is_for($list)){
            foreach ($list as $row) {
                $data[] = array(
                    'coa_id'        => $row->coa_id,
                    'coa_code'      => $row->coa_code,
                    'coa_detail'    => $row->coa_detail,
                    'coa_name'      => $row->coa_name
                );
            }
        }     

        if ($search['value']) {
            $total   = $this->coa_model->get_count_display_coa($start,$length,$search['value']);
        }else {
            $total   = $this->coa_model->get_count_coa();
        }
        // $display = $this->item_model->get_count_display($start,$length,$search['value']);
        // jsout(array('success'=>1, 'aaData'=>$data));
        jsout(array('success'=>1, 'aaData'=>$data,'iTotalRecords'=>$total,'iTotalDisplayRecords'=>$total));
    }

    function form_act(){
        $id_parent      = $this->security->xss_clean($this->db->escape_str($this->input->post('id_parent')));
        $coa_code       = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_code')));

        $cek_data       = $this->coa_model->cek_coa($coa_code,$id_parent)->row();
        $cek_nomor      = $this->coa_model->cek_coa_num($coa_code,$id_parent)->num_rows();

        if($cek_nomor>=1){
            jsout(array('success' => "Ada", 'status' => "Nomor '".$coa_code."' Sudah Ada Dengan Nama Coa ".$cek_data->coa_name));
        }else{
            $save   = $this->coa_model->act_form();
            jsout(array('success' => "Berhasil", 'status' => $save ));
        }
    }

    function view_all(){
        $data['data_coa']     = $this->coa_model->get_coa_all();
        $this->template->load('body', 'master/coa/bp_view_all',$data);
    }

    function delete($id){
        $delete = $this->items_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('master/items',$data);
        }
    }

    function delete_js(){
        $delete = $this->coa_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $data['level_budget']       = $this->coa_model->get_coa_level();
        $data['detail']             = $this->coa_model->get_detail_coa($id);
        $this->template->load('body', 'master/coa/coa_edit',$data);
    }

    function edit_act(){
        $id_parent      = $this->security->xss_clean($this->db->escape_str($this->input->post('id_parent')));
        $coa_code       = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_code')));
        $id_parent_old  = $this->security->xss_clean($this->db->escape_str($this->input->post('id_parent_old')));
        $coa_code_old   = $this->security->xss_clean($this->db->escape_str($this->input->post('coa_code_old')));

        if($id_parent==$id_parent_old AND $coa_code==$coa_code_old){
            $update   = $this->coa_model->act_edit();
            jsout(array('success' => true, 'status' => $update ));
        }else{
            $cek_data       = $this->coa_model->cek_coa($coa_code,$id_parent)->row();
            $cek_nomor      = $this->coa_model->cek_coa($coa_code,$id_parent)->num_rows();

            if($cek_nomor>=1){
                jsout(array('success' => "Ada", 'status' => "Nomor '".$coa_code."' Sudah Ada Dengan Nama Coa ".$cek_data->coa_name));
            }else{
                $update   = $this->coa_model->act_edit();
                jsout(array('success' => true, 'status' => $update ));
            }
        }

    }

    function form_act_in_pr(){
        $save   = $this->items_model->act_form_in_pr();
        jsout(array('success' => true, 'status' => $save ));
    }

}
?>