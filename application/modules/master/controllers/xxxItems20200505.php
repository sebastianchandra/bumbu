<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	function __construct(){
        
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/items'));  
        $this->isMenu();
        $this->load->model('master/items_model');
    }


	function index(){
        $data['data_items']     = $this->items_model->get_items();
        $this->template->load('body', 'master/items/items_view',$data);
	}

    function form(){
        $data['items_unit']     = $this->items_model->get_items_unit();  
        $this->template->load('body', 'master/items/items_form',$data);
    }

    function form_act(){
        $id         = $this->items_model->getDataCount()->items_id+1;

        $periode            = date('Y').date('m');
        $nomor_dok          = $this->items_model->get_nomor_dok($periode)->nomor_dok;
        $items_code         = 'MA'.$periode.$nomor_dok;

        $this->items_model->setItemsId($this->security->xss_clean($id));
        $this->items_model->setItemsCode($this->security->xss_clean($items_code));
        $this->items_model->setItemsName($this->security->xss_clean($_POST['items_name']));
        // $this->items_model->setItemsKind($this->security->xss_clean($_POST['itemsKind']));
        $this->items_model->setItemsInfo($this->security->xss_clean($_POST['items_info']));
        $this->items_model->setPrice($this->security->xss_clean($_POST['items_price']));
        $this->items_model->setItemsUnit($this->security->xss_clean($_POST['items_unit']));
        // $this->items_model->setItemsGroup($this->security->xss_clean($_POST['itemsGroup']));
        $this->items_model->setCategoryItems($this->security->xss_clean($_POST['items_category']));
        $this->items_model->setIsActive($this->security->xss_clean(1));
        $this->items_model->setPicInput($this->security->xss_clean($this->current_user['user_id']));
        $this->items_model->setInputTime($this->security->xss_clean(dbnow()));
        $save   = $this->items_model->act_form();

        jsout(array('success' => true, 'status' => $save ));
    }

    function delete($id){
        $delete = $this->items_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('master/items',$data);
        }
    }

    function delete_js(){
        $delete = $this->items_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $data['items_unit']     = $this->items_model->get_items_unit();
        $data['items']          = $this->items_model->getById($id);
        $this->template->load('body', 'master/items/items_edit', $data);
    }

    function edit_act(){

        $id = $this->security->xss_clean($_POST['items_id']);
        $this->items_model->setItemsName($this->security->xss_clean($_POST['items_name']));
        // $this->items_model->setItemsKind($this->security->xss_clean($_POST['itemsKind']));
        $this->items_model->setItemsInfo($this->security->xss_clean($_POST['items_info']));
        $this->items_model->setPrice($this->security->xss_clean($_POST['items_price']));
        $this->items_model->setItemsUnit($this->security->xss_clean($_POST['items_unit']));
        // $this->items_model->setItemsGroup($this->security->xss_clean($_POST['itemsGroup']));
        $this->items_model->setCategoryItems($this->security->xss_clean($_POST['items_category']));
        $this->items_model->setIsActive($this->security->xss_clean(1));
        $this->items_model->setPicEdit($this->security->xss_clean($this->current_user['user_id']));
        $this->items_model->setEditTime($this->security->xss_clean(dbnow()));

        $update   = $this->items_model->act_edit($_POST['items_id']);

        $cek_price= $this->db->query(" SELECT items_price FROM db_bumbu_transaction.trn_stock_by_doc WHERE items_id='".$_POST['items_id']."' AND activity='Saldo Awal' AND items_price='0.00' ")->num_rows();

        if($cek_price=='1'){            
            $u_price  = $this->db->query("UPDATE db_bumbu_transaction.trn_stock_by_doc SET items_price = '".$_POST['items_price']."' WHERE activity = 'Saldo Awal' AND items_id='".$_POST['items_id']."'");
        }

        jsout(array('success' => true, 'status' => $update ));
    }

    function form_act_in_pr(){
        $save   = $this->items_model->act_form_in_pr();
        jsout(array('success' => true, 'status' => $save ));
    }

    function items_sa(){
        $to_local   = $this->input->post('to_local');
        $id         = $this->input->post('id');
        $result     = $this->items_model->get_items_sa($to_local,$id);
        echo json_encode($result);
    }

}
?>