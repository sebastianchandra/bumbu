<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_bonus extends MY_Controller {

	function __construct(){
        
        parent::__construct();
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Master', 'active_submenu' => 'master/config_bonus'));  
        $this->isMenu();
        $this->load->model('master/config_bonus_model');
        $this->load->model('master/items_model');
    }


	function index(){
        $data['data_items']     = $this->config_bonus_model->get_config();
        $this->template->load('body', 'master/config_bonus/bonus_view',$data);
	}

    function form(){
        $data['data_items']     = $this->items_model->get_items();  
        $this->template->load('body', 'master/config_bonus/bonus_form',$data);
    }

    function form_act(){
        
        $save   = $this->config_bonus_model->act_form();

        jsout(array('success' => true, 'status' => $save ));
    }

    function delete($id){
        $delete = $this->config_bonus_model->act_delete($id);
        //test($delete,1);
        if($delete === true){
            $data['message'] = 'message';
            redirect('master/items',$data);
        }
    }

    function delete_js(){
        $delete = $this->config_bonus_model->act_delete_js();
        //test($delete,1);
        jsout(array('success' => true, 'status' => $delete ));
    }

    function edit($id){
        $data['data_items']     = $this->items_model->get_items();  
        $data['config']          = $this->config_bonus_model->getById($id);
        $this->template->load('body', 'master/config_bonus/bonus_edit', $data);
    }

    function edit_act(){

        $update   = $this->config_bonus_model->act_edit();

        $cek_price= $this->db->query(" SELECT items_price FROM db_bumbu_transaction.trn_stock_by_doc WHERE items_id='".$_POST['items_id']."' AND activity='Saldo Awal' AND items_price='0.00' ")->num_rows();

        if($cek_price=='1'){            
            $u_price  = $this->db->query("UPDATE db_bumbu_transaction.trn_stock_by_doc SET items_price = '".$_POST['items_price']."' WHERE activity = 'Saldo Awal' AND items_id='".$_POST['items_id']."'");
        }

        jsout(array('success' => true, 'status' => $update ));
    }

    function form_act_in_pr(){
        $save   = $this->config_bonus_model->act_form_in_pr();
        jsout(array('success' => true, 'status' => $save ));
    }

    function items_sa(){
        $to_local   = $this->input->post('to_local');
        $id         = $this->input->post('id');
        $result     = $this->config_bonus_model->get_items_sa($to_local,$id);
        echo json_encode($result);
    }

    function priceUpdate(){
        $miSql = "SELECT * FROM db_bumbu_master.mst_items WHERE price > 0"; 
        $rs = $this->db->query($miSql)->result();
        foreach ($rs as $value) {
            # code...
            $myText = '';
            $itmesId = $value->items_id;
            $itemsPrice = $value->price; 

            $dtSql = 'UPDATE db_bumbu_transaction.trn_incoming_stock_02 ';
            $dtSql.= 'SET price = '.$itemsPrice.', total = qty*'.$itemsPrice.' ';
            $dtSql.=' WHERE items_id= '.$itmesId.' AND price <= 0 ';
            $this->db->query($dtSql);
            // $myText .= $dtSql; 
            // $myText .= ' > '; 

            $dtSql = 'UPDATE db_bumbu_transaction.trn_outgoing_stock_02 ';
            $dtSql.= 'SET price = '.$itemsPrice.', total = qty*'.$itemsPrice.' ';
            $dtSql.=' WHERE items_id= '.$itmesId.' AND price <= 0 ';
            $this->db->query($dtSql);
            // $myText .= $dtSql; 
            // $myText .= ' > ';

            $dtSql = 'UPDATE db_bumbu_transaction.trn_stock_by_doc ';
            $dtSql.= 'SET items_price = '.$itemsPrice.' ';
            $dtSql.=' WHERE items_id= '.$itmesId.' AND items_price <= 0 ';
            $this->db->query($dtSql);
            // $myText .= $dtSql; 

        }
    }

    function cetak($status){
        $data['data_items']             = $this->config_bonus_model->get_items();
        $data['status']                 = $status;
        $this->load->view('master/items/items_view_print',$data);
    }

}
?>