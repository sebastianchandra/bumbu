<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Customer</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Customer</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama Customer</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="fc_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Alamat</p>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Kota</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="city_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>No. Telp</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="phone_01" placeholder="No Telp 1">
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="phone_02" placeholder="No Telp 2">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/customer'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#fc_name').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#fc_name').val()){
            toastr.error("<b>Nama Customer</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#fc_name').focus();
            return false;
        }
        if($('#fc_name').val().length>=100){
            toastr.error("<b>Nama Customer</b> Max 100 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#fc_name").focus();
            return false;
        }
        if(!$('#address').val()){
            toastr.error("<b>Alamat</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#address').focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/customer/form_act',
            type : "POST",  
            data : {
                fc_name             : $('#fc_name').val(),
                address             : $('#address').val(),
                city_name           : $('#city_name').val(),
                phone_01            : $('#phone_01').val(),
                phone_02            : $('#phone_02').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/customer'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>