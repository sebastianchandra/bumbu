<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Customer</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Customer</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/customer/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>
<?php 
// test($data_items,1);
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="vtable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th width="15%">Kode Customer</th>
                  <th width="35%">Nama Customer</th>
                  <th>Kota</th>
                  <th>No. Telp </th>
                  <th width="13%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($data_fish as $key => $value) {
                ?>
                <tr>
                  <td><?php echo $value->fc_code; ?></td>
                  <td><?php echo $value->fc_name; ?></td>
                  <td><?php echo $value->city_name; ?></td>
                  <td><?php echo $value->phone_01.' / '.$value->phone_02; ?></td>
                  <td class="center">
                    <a href="<?php echo base_url('master/customer/edit/'.$value->fc_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                    <button id="delete" data-fc_id='<?php echo $value->fc_id; ?>' data-nama='<?php echo $value->fc_name; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Customer', exportOptions:{messageTop: 'Master Customer', columns: [0,1,2,3]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var fc_id  = $(this).data('fc_id');
    var nama     = $(this).data('nama');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+nama+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-fc_id="'+fc_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var fc_id  = $(e).data('fc_id');

  $.ajax({
    data: {
      fc_id  : fc_id
    },
    type : "POST",
    url: baseUrl+'master/customer/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/customer'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
