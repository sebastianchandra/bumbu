<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Warehouse</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Warehouse</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Warehouse Name</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="warehouse_name" value="<?php echo $data_warehouse->warehouse_name ?>">
                                <input type="hidden" class="form-control" id="warehouse_id" value="<?php echo $data_warehouse->warehouse_id ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Address</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="address" value="<?php echo $data_warehouse->address ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Telp</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="telp" value="<?php echo $data_warehouse->telp ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>City</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="city" value="<?php echo $data_warehouse->city ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Remarks</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="remarks" value="<?php echo $data_warehouse->remarks ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/project'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#warehouse_name').focus();
    
    $('#save').click(
        function(e){
        e.preventDefault();

        if(!$('#warehouse_name').val()){
            toastr.error("<b>Project Name</b> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#warehouse_name").focus();
            return false;
        }

        if($('#warehouse_name').val().length>=51){
            toastr.error("<b>Warehouse Name</b> Max 50 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#warehouse_name").focus();
            return false;
        }

        if(!$('#address').val()){
            toastr.error("<b>Project Location</b> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#address").focus();
            return false;
        }

        if(!$('#remarks').val()){
            toastr.error("<b>Project Info</b> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#remarks").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/warehouse/edit_act',
            type : "POST",  
            data : {
                name    : $('#warehouse_name').val(),
                address : $('#address').val(),
                telp    : $('#telp').val(),
                city    : $('#city').val(),
                remarks : $('#remarks').val(),
                warehouse_id : $('#warehouse_id').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data has been save.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/warehouse'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>