<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Warehouse</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Warehouse</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/warehouse/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
              <thead>
                <tr>
                  <th width="20%">Warehouse Name</th>
                  <th>Address</th>
                  <th>Remarks</th>
                  <th>Telp / City</th>
                  <th width="13%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($data_warehouse as $key => $value) {
                ?>
                <tr>
                  <td><?php echo $value->warehouse_name; ?></td>
                  <td><?php echo $value->address; ?></td>
                  <td><?php echo $value->remarks; ?></td>
                  <td><?php echo $value->telp; ?> / <?php echo $value->city; ?></td>
                  <td class="center">
                    <a href="<?php echo base_url('master/warehouse/edit/'.$value->warehouse_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                    <button id="delete" data-warehouse_id='<?php echo $value->warehouse_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Gudang', exportOptions:{messageTop: 'Master Gudang', columns: [0,1,2,3]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var warehouse_id  = $(this).data('warehouse_id');

    toastr.warning(
      'Do you want to delete ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-warehouse_id="'+warehouse_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var warehouse_id  = $(e).data('warehouse_id');

  $.ajax({
    data: {
      warehouse_id  : warehouse_id
    },
    type : "POST",
    url: baseUrl+'master/warehouse/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/warehouse'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
