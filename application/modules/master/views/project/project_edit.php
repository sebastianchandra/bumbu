<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Edit Project</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Project</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/project/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<?php 
// test($data_project,1);
?>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Project Name</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="project_name" value="<?php echo $data_project->project_name; ?>">
                                <input type="hidden" class="form-control" id="project_id" value="<?php echo $data_project->project_id; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Project Location</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="project_location" value="<?php echo $data_project->project_location; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Project Info</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="project_info" value="<?php echo $data_project->project_info; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/project'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#project_name').focus();
    
    $('#save').click(
        function(e){
        e.preventDefault();

        if(!$('#project_name').val()){
            toastr.error("<b>Project Name</b> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#project_name").focus();
            return false;
        }

        if($('#project_name').val().length>=56){
            toastr.error("<b>Project Name</b> Max 55 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#project_name").focus();
            return false;
        }

        if(!$('#project_location').val()){
            toastr.error("<b>Project Location</b> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#project_location").focus();
            return false;
        }

        if(!$('#project_info').val()){
            toastr.error("<b>Project Info</b> Can't Be Empty", 'Alert', {"positionClass": "toast-top-center"});
            $("#project_info").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/project/edit_act',
            type : "POST",  
            data : {
                name    : $('#project_name').val(),
                location: $('#project_location').val(),
                info    : $('#project_info').val(),
                project_id    : $('#project_id').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data has been save.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/project/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>