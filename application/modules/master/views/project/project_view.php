<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>View Project</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Project</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?php echo base_url('master/project/form'); ?>" class="btn btn-primary">Input</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Project Name</th>
                    <th width="20%">Location</th>
                    <th>Info</th>
                    <th width="10%">Status</th>
                    <th width="15%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_project as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->project_name; ?></td>
                    <td><?php echo $value->project_location; ?></td>
                    <td><?php echo $value->project_info; ?></td>
                    <td><?php echo ($value->is_active==1)? 'Active' : 'Closed'; ?></td>
                    <td class="center">
                      <?php 
                      if($value->is_active==1){
                      ?>
                      <a href="<?php echo base_url('master/project/edit/'.$value->project_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-project_id='<?php echo $value->project_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                      <button id="close" data-project_id='<?php echo $value->project_id; ?>' class="btn btn-info btn-xs" type="submit">Close</button>
                      <?php 
                      }else{
                      ?>
                      <button id="delete" class="btn btn-danger btn-xs" type="submit" disabled="">Edit</button>
                      <button id="delete" class="btn btn-warning btn-xs" type="submit" disabled="">Delete</button>
                      <button id="close" class="btn btn-info btn-xs" type="submit" disabled="">Close</button>
                      <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Project', exportOptions:{messageTop: 'Master Project', columns: [0,1,2]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var project_id  = $(this).data('project_id');

    toastr.warning(
      'Do you want to delete ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-project_id="'+project_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });

  $('#vtable').on('click','#close', function(e){
    var project_id  = $(this).data('project_id');

    toastr.warning(
      'Do you want to Close Project ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return closeRow(this)" data-project_id="'+project_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var project_id  = $(e).data('project_id');

  $.ajax({
    data: {
      project_id  : project_id
    },
    type : "POST",
    url: baseUrl+'master/project/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/project'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
function closeRow(e){
  var project_id  = $(e).data('project_id');

  $.ajax({
    data: {
      project_id  : project_id
    },
    type : "POST",
    url: baseUrl+'master/project/close_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Close.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/project'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
