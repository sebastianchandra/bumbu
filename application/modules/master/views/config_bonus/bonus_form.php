<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Config Bonus</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Config Bonus</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama Barang</p>
                            </div>
                            <div class="col-md-7">
                            <select class="form-control" id="items_id">
                                    <option value="">Select</option>
                                    <?php 
                                    foreach ($data_items as $key => $value) {
                                        echo "<option value='".$value->items_id."'>".$value->items_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Qty Items</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control price" id="qty_items" placeholder="Qty Items">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Qty Bonus</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control price" id="qty_bonus" placeholder="Qty Bonus">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/config_bonus'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#items_id').focus();
$("#items_id").select2();

$(document).ready(function(){
    $('#items_price_percent').change(function(e){
        var beli        = parseFloat($('#items_price').val().replace(/\,/g, ''));
        var percent     = parseFloat($('#items_price_percent').val().replace(/\,/g, ''));

        let hasil       = parseFloat((beli+((beli*percent)/100)));
        $('#items_price_sell').val(hasil);  
    });

    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#items_id').val()){
            toastr.error("<b>Nama Barang</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#items_id').select2('open');
            return false;
        }

        if(!$('#qty_items').val()){
            toastr.error("<b>Qty Bonus</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#qty_items').focus();
            return false;
        }

        if(!$('#qty_bonus').val()){
            toastr.error("<b>Qty Bonus</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#qty_bonus').focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/config_bonus/form_act',
            type : "POST",  
            data : {
                items_id          : $('#items_id').val(),
                qty_items         : $('#qty_items').val(),
                qty_bonus         : $('#qty_bonus').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/config_bonus'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>