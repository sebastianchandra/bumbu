<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Config Bonus</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Config Bonus</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/config_bonus/form"); ?>">Input</a>
          <!-- <a target="_blank" href="<?= base_url('master/items/cetak/html') ?>" class="btn btn-info ">Print</a>
          <a target="_blank" href="<?= base_url('master/items/cetak/excell') ?>" class="btn btn-success ">Excell</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="vtable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th width="25%">Nama Barang</th>
                  <th width="11%">Qty Items</th>
                  <th width="10%">Qty Bonus</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($data_items as $key => $value) {
                ?>
                <tr>
                  <td><?php echo $value->items_name; ?></td>
                  <td align="right"><?php echo number_format($value->qty_items,2); ?></td>
                  <td align="right"><?php echo number_format($value->qty_bonus,2); ?></td>
                  <td class="center">
                    <a href="<?php echo base_url('master/config_bonus/edit/'.$value->bc_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                    <button id="delete" data-bc_id='<?php echo $value->bc_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Barang', exportOptions:{messageTop: 'Master Barang', columns: [0,1,2,3,4,5]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var bc_id  = $(this).data('bc_id');

    toastr.warning(
      'Apakah anda Ingin Menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-bc_id="'+bc_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var bc_id  = $(e).data('bc_id');

  $.ajax({
    data: {
      bc_id  : bc_id
    },
    type : "POST",
    url: baseUrl+'master/config_bonus/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success('Data Tidak berhasil di Hapus', 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/config_bonus'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function updateHarga(e){
  $.ajax({
    type : "POST",
    url: baseUrl+'master/items/priceUpdate',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Harga Berhasil di Update.", 'Alert', {"positionClass": "toast-top-center"});

        // setTimeout(function () {
        //   window.location.href = baseUrl+'master/items'; //will redirect to google.
        // }, 2000);
      }
    }
  });
}
</script>
