<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Unit</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Unit</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama Unit</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="items_unit_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/unit'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#items_unit_name').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#items_unit_name').val()){
            toastr.error("<b>Nama Unit</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#items_unit_name').focus();
            return false;
        }
        if($('#items_unit_name').val().length>=21){
            toastr.error("<b>Nama Unit</b> Max 20 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#items_unit_name").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/unit/form_act',
            type : "POST",  
            data : {
                itemsUnitName           : $('#items_unit_name').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/unit/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>