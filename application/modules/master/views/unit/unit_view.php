<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Unit</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Unit</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>
<?php 
// test($data_items,1);
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Nama Unit</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_fish as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->items_unit_name; ?></td>
                    <td class="center">
                      <a href="<?php echo base_url('master/unit/edit/'.$value->items_unit_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-items_unit_id='<?php echo $value->items_unit_id; ?>' data-items_unit_name='<?php echo $value->items_unit_name; ?>' class="btn btn-warning btn-xs" type="submit">Hapus</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Unit', exportOptions:{messageTop: 'Master Unit', columns: [0,1]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var items_unit_id       = $(this).data('items_unit_id');
    var items_unit_name     = $(this).data('items_unit_name');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+items_unit_name+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-items_unit_id="'+items_unit_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var items_unit_id  = $(e).data('items_unit_id');

  $.ajax({
    data: {
      itemsUnitId  : items_unit_id
    },
    type : "POST",
    url: baseUrl+'master/unit/del',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/unit'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
