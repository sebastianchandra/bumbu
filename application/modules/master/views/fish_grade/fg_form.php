<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Tingkatan Ikan</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Tingkatan Ikan</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Tingkatan</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="fg_name">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Remarks</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="remarks">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/fish_grade'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#fg_name').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#fg_name').val()){
            toastr.error("<b>Nama Tingkatan</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#fg_name').focus();
            return false;
        }
        if($('#fg_name').val().length>=26){
            toastr.error("<b>Nama Tingkatan</b> Max 25 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#fg_name").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/fish_grade/form_act',
            type : "POST",  
            data : {
                fg_name           : $('#fg_name').val(),
                remarks             : $('#remarks').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/fish_grade/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>