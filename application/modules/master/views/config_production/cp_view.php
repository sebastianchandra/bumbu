<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Config Production</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Config Production</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/config_production/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="10%">Nomor</th>
                    <th>Nama Kategori</th>
                    <th>Nama Barang Produksi</th>
                    <th width="15%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_config as $key => $value) {
                    // test($value,1);
                  ?>
                  <tr>
                    <td><strong><?php echo $value->items_code; ?></strong></td>
                    <td><?php echo $value->category_items; ?></td>
                    <td><?php echo $value->items_name; ?></td>
                    <td align="center">
                      <a href="<?php echo base_url('master/config_production/edit/'.$value->production_01_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="reject" data-production_01_id='<?php echo $value->production_01_id; ?>' class="btn btn-warning btn-xs" type="submit">Hapus</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header" style="padding: 15px 15px;display: block;">
        <button type="button" class="close" data-dismiss="modal" 
        style="padding: 0;background-color: transparent;border: 0; -webkit-appearance: none;float: right;font-size: 1.5rem;font-weight: 700;line-height: 1;color: #000;
        text-shadow: 0 1px 0 #fff;opacity: .5;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Detail Permintaan Pembelian</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>PR No</strong></p>
          </div>
          <div class="col-md-4">
            <div id="no_pr"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Nama Company</strong></p>
          </div>
          <div class="col-md-4">
            <div id="company_id"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Tanggal</strong></p>
          </div>
          <div class="col-md-4">
            <div id="pr_date"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Requester</strong></p>
          </div>
          <div class="col-md-4">
            <div id="requester"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-2">
            <p><strong>Remarks</strong></p>
          </div>
          <div class="col-md-4">
            <div id="remarks"></div>
          </div>
          <div class="col-md-2">
            <p><strong>Status</strong></p>
          </div>
          <div class="col-md-4">
            <div id="status"></div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="detail_popup">
                <thead>
                  <tr>
                    <th width="8%">No</th>
                    <th width="10%">Kode Barang</th>
                    <th width="50%">Nama Barang</th>
                    <th width="20%">Qty</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<script>
var detail_popup = $('#detail_popup').DataTable({ 
  "paging":   false,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { 
      className: "col_right", 
      targets: [ 3 ] 
    }
  ]
});

function show_detail(e){
  var idPr     = $(e).data('id');
  var noPr     = $(e).data('pr');

  $.get({
    url: baseUrl + 'transaction/purchase_requisition/view_popup/'+idPr,
    success: function(resp){
      $('#company_id').text(resp.header.company_name);
      $('#no_pr').text(resp.header.pr_no);
      $('#pr_date').text(tanggal_indonesia(resp.header.pr_date));
      $('#requester').text(resp.header.requester);
      $('#remarks').text(resp.header.pr_info);
      $('#status').text(resp.header.pr_status);

      detail_popup.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      detail_popup.rows.add(dataSrc).draw(false);
    }
  });


  $('#myModal').modal('show'); 
}
$(document).ready(function(){
  

  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var pr_id  = $(this).data('pr_id');

    toastr.warning(
      'Apakah Anda ingin menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-pr_id="'+pr_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });

  $('#vtable').on('click','#reject', function(e){
    var production_01_id  = $(this).data('production_01_id');

    toastr.warning(
      'Apakah anda ingin Menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return rejectRow(this)" data-production_01_id="'+production_01_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );    
  });
});

function rejectRow(e){
  var production_01_id  = $(e).data('production_01_id');

  $.ajax({
    data: {
      production_01_id  : production_01_id
    },
    type : "POST",
    url: baseUrl+'master/config_production/reject_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Tolak');
        return false;

      } else {
        toastr.success("Data Berhasil Di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/config_production'; //will redirect to google.
        }, 2000);
      }
    }
  });
}

function deleteRow(e){
  var pr_id  = $(e).data('pr_id');

  $.ajax({
    data: {
      pr_id  : pr_id
    },
    type : "POST",
    url: baseUrl+'transaction/purchase_requisition/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>




