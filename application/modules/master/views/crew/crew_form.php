<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Crew</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Crew</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Crew</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="full_name">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Kelamin</p>
                            </div>
                            <div class="col-md-5">
                                <input type="radio" value="L" id="gender" name="gender"> Laki-Laki &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="P" id="gender" name="gender"> Perempuan
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Tempat Lahir</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="place_of_birth">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Tanggal Lahir</p>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="date_of_birth">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>No KTP</p>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="id_card_no">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>No NPWP</p>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="npwp_no">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Alamat</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="crew_address">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Status</p>
                            </div>
                            <div class="col-md-6">
                                <input type="radio" value="Lajang" id="martial_status" name="martial_status"> Lajang &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="Nikah" id="martial_status" name="martial_status"> Nikah &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" value="Cerai" id="martial_status" name="martial_status"> Cerai
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Posisi</p>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="position">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>No Telp</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="cell_no">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/fish'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#full_name').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#full_name').val()){
            toastr.error("<b>Nama Crew</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#full_name').focus();
            return false;
        }

        if($('#full_name').val().length>=26){
            toastr.error("<b>Nama Crew</b> Max 25 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#full_name").focus();
            return false;
        }

        if(!$('#gender:checked').val()){
            toastr.error("<b>Kelamin</b> Harus Di Pilih", 'Alert', {"positionClass": "toast-top-center"});
            return false;
        }
        if(!$('#place_of_birth').val()){
            toastr.error("<b>Tempat Lahir</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#place_of_birth').focus();
            return false;
        }
        if(!$('#date_of_birth').val()){
            toastr.error("<b>Tanggal Lahir</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#date_of_birth').focus();
            return false;
        }
        if(!$('#id_card_no').val()){
            toastr.error("<b>No KTP</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#id_card_no').focus();
            return false;
        }
        if(!$('#position').val()){
            toastr.error("<b>Posisi</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#position').focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/crew/form_act',
            type : "POST",  
            data : {
                full_name           : $('#full_name').val(),
                gender              : $('#gender:checked').val(),
                place_of_birth      : $('#place_of_birth').val(),
                date_of_birth       : $('#date_of_birth').val(),
                id_card_no          : $('#id_card_no').val(),
                npwp_no             : $('#npwp_no').val(),
                crew_address        : $('#crew_address').val(),
                martial_status      : $('#martial_status:checked').val(),
                position            : $('#position').val(),
                cell_no             : $('#cell_no').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/crew/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>