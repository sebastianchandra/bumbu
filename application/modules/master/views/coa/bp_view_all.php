<div class="app-title">
  <div>
    <h1>Input Budget Plan</h1>
    <p><ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item">Budget Plan</li>
        <li class="breadcrumb-item active">Input Budget Plan</li>
  </ul></p>
  </div>
  <!-- <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a class="btn btn-secondary" href="<?php echo base_url(); ?>master/items">Back</a></li>
  </ul> -->
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Parent</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          foreach ($data_budget_plan as $key => $value) {
          ?>
          <tr>
            <td><?php echo $value->mbp_detail; ?></td>
            <td><?php echo $value->mbp_name; ?></td>
            <td><?php echo $value->parent; ?></td>
          </tr>
          <?php 
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
$('#itemsTable').DataTable({
    "paging": true, 
    "bLengthChange": false, // disable show entries dan page
    "bFilter": true,
    "bInfo": true, // disable Showing 0 to 0 of 0 entries
    "bAutoWidth": false,
    "language": {
        "emptyTable": "No Data"
    },
    "aaSorting": [],
    responsive: true
});

</script>