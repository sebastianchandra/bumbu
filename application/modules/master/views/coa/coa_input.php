<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Coa</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Coa</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Level</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id='id_level'>
                                    <option value=""> - </option>
                                    <?php 
                                    foreach ($level_budget as $key => $value) {
                                      echo '<option value="'.$value->coa_level.'">'.$value->level_name.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Induk</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-12" type="text" placeholder="Induk" id="id_parent" onclick="return show_coa(this)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nomor Urut</p>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control col-md-10" type="text" placeholder="Kode" id="kode" disabled="">
                            </div>
                            <div class="col-md-5" >
                                <input class="form-control col-md-8" type="text" placeholder="Nomor Urut" id="coa_code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-8" type="text" placeholder="Nama" id="coa_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/coa'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/template" class="modal_item" id="content_browse_parent">
  <table class='table table-bordered table-hover' id='table-browse_parent' style="cursor:pointer">
    <thead>
      <tr>  
        <th>ID</th>
        <th>Code</th>
        <th>Coa Name</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</script>

<div class="modal inmodal" id="myCoa" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Parent Coa</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="coa_popup">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Code</th>
                          <th>Coa Name</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$("#id_level").select2().on('select2:select',function(e){
  if($('#id_level').val()==1){
    $("#id_parent").attr('disabled','disabled');
    $('#id_parent').attr('data-id', '0');
    $('#id_parent').val('');
  }else{
    $("#id_parent").removeAttr('disabled');
  }
});

var popup_coa = $('#coa_popup').DataTable({ 
  "paging":   true,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { // disable sorting on column process buttons
      "targets": [0,1,2],
      "orderable": false,
    }
  ]
});

popup_coa.on('click', 'tr', function (){
  // debugger
  var data = popup_coa.row(this).data();
  var taskID = data[0];

  $('#id_parent').val(data[2]);
  $('#id_parent').attr('data-id', data[0]);
  $('#id_parent').attr('data-detail', data[1]);
  $('#kode').val(data[1]);

  $('#myCoa').modal('hide'); 
});

function show_coa(e){  
  popup_coa.clear().draw();
  if(!$('#id_level').val()){
    toastr.error("<b>Level</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#id_level').select2('open');
    return false;
  }

  var id_level  = $('#id_level').val()-1;
  $.get({
    url: baseUrl + 'master/coa/get_bp_new/'+id_level,
    success: function(resp){
      popup_coa.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_coa.rows.add(dataSrc).draw(false);
    }
  });

  $('#myCoa').modal('show'); 
}

$('#save').click(
  function(e){
    e.preventDefault();

    if(!$('#id_level').val()){
      toastr.error("<b>Level</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#id_level').select2('open');
      return false;
    }

    if($('#id_level').val()!=1 && !$('#id_parent').val()){
      toastr.error("<b>Induk</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#id_parent").focus();
      return false;
    }

    if(!$('#coa_name').val()){
      toastr.error("<b>Nama</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#coa_name").focus();
      return false;
    }

    if($('#coa_name').val().length>=151){
        toastr.error("<b>Nama </b> Max 150 Huruf", 'Alert', {"positionClass": "toast-top-center"});
        $("#coa_name").focus();
        return false;
    }
    
    $.ajax({
      url: baseUrl+'master/coa/form_act',
      type : "POST",  
      data: {
        coa_name      : $('#coa_name').val(),
        coa_code      : $('#coa_code').val(),
        id_level      : $('#id_level').val(),
        id_parent     : $('#id_parent').attr('data-id')
      },
      success : function(resp){
        if(resp.success == "Ada") {
          toastr.error(resp.status, 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else if(resp.status == 'ERROR INSERT'){
          toastr.success("Data Gagal di Simpan.", 'Alert', {"positionClass": "toast-top-center"});
        } else {
          toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

          setTimeout(function () {
            window.location.href = baseUrl+'master/coa'; //will redirect to google.
          }, 2000);
        }
      }
    });

  }
);
</script>