<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Customer</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Customer</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Level</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id='id_level'>
                                    <option value=""> - </option>
                                    <?php 
                                    foreach ($level_budget as $key => $value) {
                                      if($value->coa_level==$detail->coa_level){
                                        echo '<option value="'.$value->coa_level.'" selected>'.$value->level_name.'</option>';
                                      }else{
                                        echo '<option value="'.$value->coa_level.'">'.$value->level_name.'</option>';
                                      }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Induk</p>
                            </div>
                            <div class="col-md-5">
                                <!-- <input class="form-control col-md-12" type="text" placeholder="Induk" id="id_parent" onclick="return browse_parent()" data-id="<?php echo $detail->coa_parent_id; ?>" value="<?php echo $detail->parent; ?>"> -->
                                <input class="form-control col-md-12" type="text" placeholder="Induk" id="id_parent" onclick="return show_coa(this)" data-id="<?php echo $detail->coa_parent_id; ?>" value="<?php echo $detail->parent; ?>">
                                <input class="form-control col-md-12" type="hidden" placeholder="Parent" id="coa_id" value="<?php echo $detail->coa_id; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nomor Urut</p>
                            </div>
                            <div class="col-md-2">
                              <input class="form-control col-md-10" type="text" placeholder="Kode" id="kode" disabled="" value="<?php echo $detail->detail_parent; ?>">
                            </div>
                            <div class="col-md-5" >
                                <input class="form-control col-md-8" type="text" placeholder="Nomor Urut" id="coa_code" value="<?php echo $detail->coa_code; ?>">
                                <input type="hidden" id="coa_code_old" value="<?php echo $detail->coa_code; ?>">
                                <input type="hidden" id="id_parent_old" value="<?php echo $detail->coa_parent_id; ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-8" type="text" placeholder="Nama" id="coa_name" value="<?php echo $detail->coa_name; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/coa'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/template" class="modal_item" id="content_browse_parent">
  <table class='table table-bordered table-hover' id='table-browse_parent' style="cursor:pointer">
    <thead>
      <tr>  
        <th>ID</th>
        <th>Code</th>
        <th>Coa Name</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</script>

<div class="modal inmodal" id="myCoa" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Parent Coa</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="coa_popup">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Code</th>
                    <th>Coa Name</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

if($('#id_level').val()==1){
  $("#id_parent").attr('disabled','disabled');
}

$("#id_level").select2().on('select2:select',function(e){
  if($('#id_level').val()==1){
    $("#id_parent").attr('disabled','disabled');
    $('#id_parent').attr('data-id', '0');
    $('#id_parent').val('');
  }else{
    $("#id_parent").removeAttr('disabled');
  }
});

var popup_coa = $('#coa_popup').DataTable({ 
  "paging":   true,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { // disable sorting on column process buttons
      "targets": [0,1,2],
      "orderable": false,
    }
  ]
});

popup_coa.on('click', 'tr', function (){
  // debugger
  var data = popup_coa.row(this).data();
  var taskID = data[0];

  $('#id_parent').val(data[2]);
  $('#id_parent').attr('data-id', data[0]);
  $('#id_parent').attr('data-detail', data[1]);
  $('#kode').val(data[1]);

  $('#myCoa').modal('hide'); 
});

function show_coa(e){  
  popup_coa.clear().draw();
  if(!$('#id_level').val()){
    toastr.error("<b>Level</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#id_level').select2('open');
    return false;
  }

  var id_level  = $('#id_level').val()-1;
  $.get({
    url: baseUrl + 'master/coa/get_bp_new/'+id_level,
    success: function(resp){
      popup_coa.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_coa.rows.add(dataSrc).draw(false);
    }
  });

  $('#myCoa').modal('show'); 
}

function browse_parent(){
  
  if(!$('#id_level').val()){
    toastr.error("<b>Level</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
    $('#id_level').select2('open');
    return false;
  }

  let dialogshown = function(){
    var id_level  = $('#id_level').val()-1;
    var tbl       = $('#table-browse_parent');
    select_items_table = tbl.DataTable({
        pageLength  : 50,
        serverSide  : true,
        fixedHeader : true,
        ajax        : {
        method      : "GET",
        data : {
          id: id_level
        },
        url: baseUrl + 'master/coa/get_bp',
      },
      columns     : [
        {
          "data"   : "coa_id",
          "visible": false
        },
        {"data"   : "coa_detail"},
        {"data"   : "coa_name"}

      ],
      drawCallback: function( settings ) {
        var api = this.api();

        // Output the data for the visible rows to the browser's console
        api.$('td').click( function () {
          let coa_id      = api.row($(this).parents('tr')).data().coa_id;
          let coa_detail  = api.row($(this).parents('tr')).data().coa_detail;
          let coa_name    = api.row($(this).parents('tr')).data().coa_name;

          $('#id_parent').val(coa_name);
          $('#id_parent').attr('data-id', coa_id);
          $('#id_parent').attr('data-detail', coa_detail);

          //close modal
          view_barang.close();
          // $("#item_price").focus();
        });
      },
      initComplete: function() {
          var $searchInput = $('div.dataTables_filter input');

          $searchInput.unbind();

          $searchInput.bind('keyup', function(e) {
            if(this.value.length > 3 || this.value.length == 0) {
                select_items_table.search( this.value ).draw();
            }
          });
      }
    }); // DataTables()
    $('div.dataTables_filter input').focus();
  }

  view_barang = BootstrapDialog.show({
    title     : 'Select Parent ',
    nl2br     : false,
    //cssClass: 'master_promo_dialog',
    size      : 'size-wide',
    message   : $('#content_browse_parent').html(),
    draggable : false,
    buttons   : [
      {
        label    : 'Tutup',
        cssClass : 'btn-info',
        action   : function(s){s.close(); }
      }
    ],
    onshown: dialogshown
  });
};

// $("#id_level").select2();

$('#save').click(
  function(e){
    e.preventDefault();

    if(!$('#id_level').val()){
      toastr.error("<b>Level</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#id_level').select2('open');
      return false;
    }

    if($('#id_level').val()!=1 && !$('#id_parent').val()){
      toastr.error("<b>Induk</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#id_parent").focus();
      return false;
    }

    if(!$('#coa_name').val()){
      toastr.error("<b>Nama</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $("#coa_name").focus();
      return false;
    }

    if($('#coa_name').val().length>=151){
      toastr.error("<b>Nama </b> Max 150 Huruf", 'Alert', {"positionClass": "toast-top-center"});
      $("#coa_name").focus();
      return false;
    }
    
    $.ajax({
      url: baseUrl+'master/coa/edit_act',
      type : "POST",  
      data: {
        coa_name      : $('#coa_name').val(),
        id_level      : $('#id_level').val(),
        coa_id        : $('#coa_id').val(),
        coa_code      : $('#coa_code').val(),
        id_parent     : $('#id_parent').attr('data-id'),
        coa_code_old  : $('#coa_code_old').val(),
        id_parent_old : $('#id_parent_old').val()
      },
      success : function(resp){
        if(resp.success == "Ada") {
          toastr.error(resp.status, 'Alert', {"positionClass": "toast-top-center"});
          return false;

        } else if(resp.status == 'ERROR INSERT'){
          toastr.success("Data Gagal di Simpan.", 'Alert', {"positionClass": "toast-top-center"});
        } else {
          toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

          setTimeout(function () {
            window.location.href = baseUrl+'master/coa/'; //will redirect to google.
          }, 2000);
        }
      }
    });

  }
);
</script>