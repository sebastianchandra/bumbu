<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Coa</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Coa</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/coa/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Parent</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 0;
                  foreach ($data_coa as $key => $value) {
                    $no = $no+1;
                  ?>
                  <tr>
                    <td><?php echo $no; ?>.</td>
                    <td><?php echo $value->coa_detail; ?></td>
                    <td><?php echo $value->coa_name; ?></td>
                    <td><?php echo $value->parent; ?></td>
                    <td align="center">
                      <a class="btn btn-info btn-sm" href="<?php echo base_url()."master/coa/edit/$value->coa_id"; ?>">Edit</a>
                      <!-- <a onclick="return confirmDelete()" class="btn btn-warning btn-sm" href="<?php echo base_url()."master/items/delete/$value->coa_id"; ?>">Delete</a> -->
                      <button type="button" id='delete' class="element btn btn-danger btn-sm" data-toggle="modal" data-coa_name="<?php echo $value->coa_name; ?>" data-coa_id="<?php echo $value->coa_id; ?>">Delete</button></td>
                  </tr>
                  <?php 
                  }
                  ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Coa', exportOptions:{messageTop: 'Master Coa',columns: [0,1,2,3]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var coa_id  = $(this).data('coa_id');
    var nama     = $(this).data('coa_name');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+nama+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-coa_id="'+coa_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var coa_id  = $(e).data('coa_id');

  $.ajax({
    data: {
      coa_id  : coa_id
    },
    type : "POST",
    url: baseUrl+'master/coa/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/coa'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>

<!-- <div class="app-title">
	<div>
		<h1>View Budget Plan</h1>
		<p><ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item">Budget Plan</li>
        <li class="breadcrumb-item active">View Budget Plan</li>
	</ul></p>
	</div>
	<ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><a class="btn btn-info" href="<?php echo base_url(); ?>master/budget_plan/view_all"> View All</a> <a class="btn btn-primary" href="<?php echo base_url(); ?>master/budget_plan/form"><i class="fa fa-plus"></i> Input</a></li>
	</ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="table-responsive">
        <table class="table table-hover table-bordered" id="itemsTable">
          <thead>
      			<tr>
      				<th>No</th>
      				<th>Code</th>
      				<th>Name</th>
      				<th>Parent</th>
      				<th>Action</th>
      			</tr>
          </thead>
          <tbody>
          	<?php 
          	$no = 0;
        		foreach ($data_coa as $key => $value) {
        			$no = $no+1;
          	?>
      			<tr>
      				<td><?php echo $no; ?>.</td>
      				<td><?php echo $value->coa_detail; ?></td>
      				<td><?php echo $value->coa_name; ?></td>
      				<td><?php echo $value->parent; ?></td>
      				<td align="center">
      					<a class="btn btn-info btn-sm" href="<?php echo base_url()."master/budget_plan/edit/$value->coa_id"; ?>"><i class="fa fa-files-o"></i></a>
      					<button type="button" id='delete' class="element btn btn-danger btn-sm" data-toggle="modal" data-item_code="<?php echo $value->coa_detail; ?>" data-item_id="<?php echo $value->coa_id; ?>"><i class="fa fa-close"></i></button></td>
      			</tr>
      			<?php 
      			}
      			?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div> -->

<script type="text/javascript">
$('#itemsTable').DataTable({
  	"paging": true, 
  	"bLengthChange": false, // disable show entries dan page
  	"bFilter": true,
  	"bInfo": true, // disable Showing 0 to 0 of 0 entries
  	"bAutoWidth": false,
  	"language": {
      	"emptyTable": "No Data"
    },
  	"aaSorting": [],
  	responsive: true
});

$('#itemsTable').on('click','#delete', function (e) {
	var items_id 			= $(this).data('item_id');
	var items_code 	= $(this).data('item_code');

	BootstrapDialog.show({
      title: 'Delete Items ',
      type : BootstrapDialog.TYPE_DANGER,
      message: 'Do you want to delete '+items_code+' ?',
      closable: false,
      buttons: [
        {
          label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: '<i class="fa fa-close"></i> Delete', cssClass: 'btn-danger', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            $.ajax({
                data: {
                    items_id : items_id
                },
                type : "POST",
                url: baseUrl+'master/items/delete_js',
                success : function(resp){

                  if(resp.status == 'ERROR INSERT' || resp.status == false) {
                    alert('Data Tidak berhasil di Hapus');
                    return false;

                  } else {
                    $.notify({
                          icon: "glyphicon glyphicon-save",
                          message: 'Data successfully deleted'
                        },{
                          type: 'success',
                          onClosed: function(){ location.reload();}
                        });

                    setTimeout(function () {
                      window.location.href = baseUrl+'master/coa'; //will redirect to google.
                    }, 2000);
                  }
                }
            });

          }
        }
      ],
    });
});

function confirmDelete() {
	return confirm("Anda yakin untuk Menghapus data ini ?");
}
</script>