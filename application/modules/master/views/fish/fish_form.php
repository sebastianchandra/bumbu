<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Ikan</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Ikan</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Ikan</p>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" id="fish_name">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Remarks</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="remarks">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/fish'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#fish_name').focus();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#fish_name').val()){
            toastr.error("<b>Nama Ikan</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#fish_name').focus();
            return false;
        }
        if($('#fish_name').val().length>=36){
            toastr.error("<b>Nama Ikan</b> Max 35 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#fish_name").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/fish/form_act',
            type : "POST",  
            data : {
                fish_name           : $('#fish_name').val(),
                remarks             : $('#remarks').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/fish/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>