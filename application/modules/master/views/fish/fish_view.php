<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>View Ikan</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Ikan</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?php echo base_url('master/fish/form'); ?>" class="btn btn-primary">Input</a>
        </div>
    </div>
</div>
<?php 
// test($data_items,1);
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="35%">Nama Ikan</th>
                    <th>Remarks</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_fish as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->fish_name; ?></td>
                    <td><?php echo $value->remarks; ?></td>
                    <td class="center">
                      <a href="<?php echo base_url('master/fish/edit/'.$value->fish_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-fish_id='<?php echo $value->fish_id; ?>' data-nama='<?php echo $value->fish_name; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Ikan', exportOptions:{messageTop: 'Master Ikan', columns: [0,1]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var fish_id  = $(this).data('fish_id');
    var nama     = $(this).data('nama');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+nama+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-fish_id="'+fish_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});

function deleteRow(e){
  var fish_id  = $(e).data('fish_id');

  $.ajax({
    data: {
      fish_id  : fish_id
    },
    type : "POST",
    url: baseUrl+'master/fish/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/fish'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
