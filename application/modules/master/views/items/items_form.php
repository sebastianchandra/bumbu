<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Items</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Items</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Nama Barang</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="items_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Satuan Barang</p>
                            </div>
                            <div class="col-md-5">
                                <select class="form-control" id="items_unit">
                                    <option value="">Select</option>
                                    <?php 
                                    foreach ($items_unit as $key => $value) {
                                        echo "<option value='".$value->items_unit_id."'>".$value->items_unit_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Category Barang</p>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="items_category">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Harga Beli</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control price" id="items_price" placeholder="Price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Harga Jual</p>
                            </div>
                            <div class="col-md-1">
                                <input type="text" class="form-control price" id="items_price_percent" placeholder="%">
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control price" id="items_price_sell" placeholder="Price" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Info</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="items_info" placeholder="Items Info">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Produksi</p>
                            </div>
                            <div class="col-md-5">
                                <input type="checkbox" name="is_production" id="is_production" class="checkbox" value="1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/items'); ?>">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#items_name').focus();
$("#items_unit").select2();

$(document).ready(function(){
    $('#items_price_percent').change(function(e){
        var beli        = parseFloat($('#items_price').val().replace(/\,/g, ''));
        var percent     = parseFloat($('#items_price_percent').val().replace(/\,/g, ''));

        let hasil       = parseFloat((beli+((beli*percent)/100)));
        $('#items_price_sell').val(hasil);  
    });

    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#items_name').val()){
            toastr.error("<b>Nama Barang</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#items_name').select2('open');
            return false;
        }

        if($('#items_name').val().length>=151){
            toastr.error("<b>Items Name</b> Max 150 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#items_name").focus();
            return false;
        }

        if(!$('#items_unit').val()){
            toastr.error("<b>Satuan</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#items_unit').select2('open');
            return false;
        }

        if(!$('#items_price').val()){
            toastr.error("<b>Harga</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#items_price").focus();
            return false;
        }
        var is_production;
        if($('#is_production:checked').val()==1){
            is_production = $('#is_production:checked').val();
        }else{
            is_production = 0;
        }

        $.ajax({
            url  : baseUrl+'master/items/form_act',
            type : "POST",  
            data : {
                items_name          : $('#items_name').val(),
                items_info          : $('#items_info').val(),
                items_unit          : $('#items_unit').val(),
                items_category      : $('#items_category').val(),
                items_price         : $('#items_price').val().replace(/\,/g, ''),
                price_sell          : $('#items_price_sell').val().replace(/\,/g, ''),
                items_price_percent : $('#items_price_percent').val().replace(/\,/g, ''),
                is_production       : is_production
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil di Simpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/items'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>