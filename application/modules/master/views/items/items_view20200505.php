<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>View Items</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Items</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a>
        </div>
    </div>
</div>
<?php 
// test($data_items,1);
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="11%">Items Code</th>
                    <th width="20%">Items Name</th>
                    <th width="11%">Unit Items</th>
                    <th width="20%">Category Items</th>
                    <th>Info</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_items as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->items_code; ?></td>
                    <td><?php echo $value->items_name; ?></td>
                    <td><?php echo $value->items_unit_name; ?></td>
                    <td><?php echo $value->category_items; ?></td>
                    <td><?php echo $value->items_info; ?></td>
                    <td class="center">
                      <a href="<?php echo base_url('master/items/edit/'.$value->items_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-items_id='<?php echo $value->items_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var items_id  = $(this).data('items_id');

    toastr.warning(
      'Do you want to delete ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-items_id="'+items_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var items_id  = $(e).data('items_id');

  $.ajax({
    data: {
      items_id  : items_id
    },
    type : "POST",
    url: baseUrl+'master/items/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/items'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
