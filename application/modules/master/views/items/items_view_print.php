<?php
if($status=='excell'){
  $file = "master_items.xls";
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment; filename='.$file);
}
?>
<style>
table, td, th {
  border: 1px solid black;
}

table {
  width: 100%;
  border-collapse: collapse;
}
</style>

<table id="vtable" class="table table-striped table-bordered">
  <thead>
    <tr>
      <th width="11%">Kode Barang</th>
      <th width="25%">Nama Barang</th>
      <th width="11%">Satuan</th>
      <th width="10%">Kategori</th>
      <th>Info</th>
      <th width="11%">Harga Beli</th>
      <th width="11%">Harga Jual</th>
      <!-- <th width="10%">Action</th> -->
    </tr>
  </thead>
  <tbody>
    <?php 
    foreach ($data_items as $key => $value) {
    ?>
    <tr>
      <td><?php echo $value->items_code; ?></td>
      <td><?php echo $value->items_name; ?></td>
      <td><?php echo $value->items_unit_name; ?></td>
      <td><?php echo $value->category_items; ?></td>
      <td><?php echo $value->items_info; ?></td>
      <td align="right"><?php echo number_format($value->price,2); ?></td>
      <td align="right"><?php echo number_format($value->price_sell,2); ?></td>
      <!-- <td class="center">
        <a href="<?php echo base_url('master/items/edit/'.$value->items_id); ?>" class="btn btn-danger btn-xs">Edit</a>
        <button id="delete" data-items_id='<?php echo $value->items_id; ?>' class="btn btn-warning btn-xs" type="submit">Delete</button>
      </td> -->
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>