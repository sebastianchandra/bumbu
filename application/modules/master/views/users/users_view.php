<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Users</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Users</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/users/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIP</th>
                  <th>Name</th>
                  <th>Username</th>
                  <!-- <th>User Level</th>
                  <th>Email</th> -->
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no = 0;
                  foreach ($data_user as $key => $value) {
                    $no = $no+1;
                  ?>
                <tr>
                  <td><?php echo $no; ?>.</td>
                  <td><?php echo $value->nip; ?></td>
                  <td><?php echo $value->name; ?></td>
                  <td><?php echo $value->username; ?></td>
                  <!-- <td><?php echo $value->user_level; ?></td> -->
                  <!-- <td><?php echo $value->email; ?></td> -->
                  <td align="center">
                    <a class="btn btn-info btn-sm" href="<?php echo base_url()."master/users/permission/$value->user_id"; ?>">Permission</a>
                    <a class="btn btn-danger btn-sm" href="<?php echo base_url()."master/users/edit/$value->user_id"; ?>">Edit</a>
                    <button id="delete" data-user_id='<?php echo $value->user_id; ?>' data-name='<?php echo $value->name; ?>' class="btn btn-warning btn-sm" type="submit">Delete</button></td>
                </tr>
                <?php 
                }
                ?>
              </tbody>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var user_id = $(this).data('user_id');
    var name    = $(this).data('name');

    toastr.warning(
      'Do you want to delete '+name+'?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-user_id="'+user_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var user_id  = $(e).data('user_id');

  $.ajax({
    data: {
      user_id  : user_id
    },
    type : "POST",
    url: baseUrl+'master/users/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/users'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>