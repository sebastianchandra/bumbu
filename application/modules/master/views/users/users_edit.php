<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Users</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Edit Users</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>NIP</p>              
                            </div>
                            <div class="col-md-3">
                                <input class="form-control col-md-12" type="text" placeholder="NIP User" id="nip" value="<?php echo $data_user->nip; ?>">
                                <input class="form-control col-md-12" type="hidden" placeholder="NIP User" id="user_id" value="<?php echo $data_user->user_id; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Name</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-12" type="text" placeholder="Name User" id="name" value="<?php echo $data_user->name; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Username</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-12" type="text" placeholder="Username Aplication" id="username" value="<?php echo $data_user->username; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Password</p>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-12" type="password" placeholder="Password" id="password1" value="<?php echo $data_user->password; ?>">
                            </div>
                            <div class="col-md-5">
                                <input class="form-control col-md-12" type="password" placeholder="Konfirmasi Password" id="password2">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/users'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#nip").focus();
$('#save').click(
  function(e){
    e.preventDefault();

    if(!$('#nip').val()){
      var message  = "<strong>NIP</strong> Can't Be Empty";
      toastr.error(message, 'Alert', {"positionClass": "toast-top-center"});
      $("#nip").focus();
      return false;
    }

    if(!$('#name').val()){
      var message =  "<strong>Name</strong> Can't Be Empty";
      toastr.error(message, 'Alert', {"positionClass": "toast-top-center"});
      $("#name").focus();
      return false;
    }

    if(!$('#username').val()){
      var message= "<strong>Username</strong> Can't Be Empty";
      toastr.error(message, 'Alert', {"positionClass": "toast-top-center"});
      $("#username").focus();
      return false;
    }

    if(!$('#password1').val()){
      var message = "<strong>Password</strong> Can't Be Empty";
      toastr.error(message, 'Alert', {"positionClass": "toast-top-center"});
      $("#password1").focus();
      return false;
    }

    if($('#password1').val() != $('#password2').val()){
      var message= "<strong>Password</strong> And <storng>Konfirmasi Password</storng> Not match";
      toastr.error(message, 'Alert', {"positionClass": "toast-top-center"});
      $("#password2").focus();
      return false;
    }

    $.ajax({
      url: baseUrl+'master/users/edit_act',
      type : "POST",  
      data: {
        nip           : $('#nip').val(),
        name          : $('#name').val(),
        username      : $('#username').val(),
        password1     : $('#password1').val(),
        user_id       : $('#user_id').val(),
      },
      success : function(resp){
        if(resp.status == 'ERROR INSERT' || resp.status==false){
          toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
          return false;

        }else{
          toastr.success("Data has been save.", 'Alert', {"positionClass": "toast-top-center"});

          setTimeout(function(){
            window.location.href = baseUrl+'master/users/'; //will redirect to google.
          }, 2000);
        }
      }
    });

  }
);
</script>