<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Users</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Permission Users</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                              NIP              
                            </div>
                            <div class="col-md-5">: <?php echo $data_user->nip; ?>
                              <input class="form-control col-md-3" type="hidden" id="user_id" name="user_id" value="<?php echo $data_user->user_id; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                              Name
                            </div>
                            <div class="col-md-5">
                                : <?php echo $data_user->name; ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <h5>Permission Menu</h5>
                            </div>
                            <div class="col-md-5" style="padding-top: 8px;">
                              <input type="checkbox" id="select_all" /><span class="label-text"> Select All</span>
                            </div>
                        </div>
                        <?php 
                        $menu = '';
                        $asubmenu = array();
                        foreach ($submenu as $key => $val) {
                          $asubmenu[$val->menu_id]=$val;
                        }

                        foreach ($data_menu as $key => $value) {
                          if(isset($asubmenu[$value->menu_id]->menu_id)){
                            $permission_menu_id = $asubmenu[$value->menu_id]->menu_id;
                          }else{
                            $permission_menu_id = '';
                          }
                        ?>
                        <div class="form-group row">
                            <div class="col-md-2">
                              <?php echo ($menu!=$value->menu_group)? $value->menu_group.'' : ''; ?>
                            </div>
                            <div class="col-md-5">
                                <input type="checkbox" <?php echo ($permission_menu_id==$value->menu_id)? 'checked=""' : ''; ?> name="menu_id" class="checkbox" value="<?php echo $value->menu_id; ?>"/>  <span class="label-text"><?php echo $value->menu_name; ?></span>
                            </div>
                        </div>
                        <?php 
                        $menu = $value->menu_group;
                        }
                        ?>
                        
                        <div class="form-group row">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/users'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#select_all').on('click',function(){
  if(this.checked){
    $('.checkbox').each(function(){
      this.checked = true;
    });
  }else{
    $('.checkbox').each(function(){
      this.checked = false;
    });
  }
});

$('.checkbox').on('click',function(){
    if($('.checkbox:checked').length == $('.checkbox').length){
        $('#select_all').prop('checked',true);
    }else{
        $('#select_all').prop('checked',false);
    }
});

$('#save').click(function(e){
e.preventDefault();
  var lang = [];

  $("input[name='menu_id']:checked").each(function(){
    lang.push(this.value);
  });

  $.ajax({
    url: baseUrl+'master/users/permission_act',
    type : "POST",  
    data: {
      user_id       : $('#user_id').val(),
      lang          : lang
    },
    success : function(resp){
      if(resp.status == 'ERROR INSERT' || resp.status==false){
        toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
        return false;

      }else{
        toastr.success("Data has been save.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function(){
          window.location.href = baseUrl+'master/users/'; //will redirect to google.
        }, 2000);
      }
    }
  });

});


</script>