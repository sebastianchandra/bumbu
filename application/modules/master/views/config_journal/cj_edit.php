<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Config Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Edit Config Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form class="form-horizontal">      
                    <div class="card-body-form">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Kode Transaksi</p>
                            </div>
                            <div class="col-md-5">
                              <select class="form-control" id="trn_code">
                                <option value="IS" <?php echo ($detail->trn_code=='IS')? 'selected=""' : '';?>>IS - Incomming Stock </option>
                                <option value="IT" <?php echo ($detail->trn_code=='IT')? 'selected=""' : '';?>>IT - Incomming Stock Others</option>
                                <option value="OS" <?php echo ($detail->trn_code=='OS')? 'selected=""' : '';?>>OS - Outgoing Stock </option>
                                <option value="OT" <?php echo ($detail->trn_code=='OT')? 'selected=""' : '';?>>OT - Outgoing Stock Others</option>
                                <option value="DO" <?php echo ($detail->trn_code=='DO')? 'selected=""' : '';?>>DO - Delivery Order</option>
                                <option value="IC" <?php echo ($detail->trn_code=='IC')? 'selected=""' : '';?>>IC - Invoice</option>
                                <option value="DS" <?php echo ($detail->trn_code=='DS')? 'selected=""' : '';?>>DS - DP Sales Order</option>
                                <option value="PB" <?php echo ($detail->trn_code=='PB')? 'selected=""' : '';?>>PB - PO Payment by Bank</option>
                                <option value="PC" <?php echo ($detail->trn_code=='PC')? 'selected=""' : '';?>>PC - PO Payment by Cash</option>
                                <option value="IB" <?php echo ($detail->trn_code=='IB')? 'selected=""' : '';?>>IB - Invoice Payment By Bank</option>
                                <option value="IH" <?php echo ($detail->trn_code=='IH')? 'selected=""' : '';?>>IH - Invoice Payment By Cash</option>
                              </select>

                                <!-- <input type="text" class="form-control" id="trn_code" value="<?php echo $detail->trn_code; ?>"> -->
                              <input type="hidden" class="form-control" id="jc_id" value="<?php echo $detail->jc_id; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Coa Debet</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="coa_debet" onclick="return browse_debet()" value="<?php echo $detail->db_coa_name; ?>" data-id="<?php echo $detail->db_coa_id; ?>" data-code="<?php echo $detail->db_coa_code; ?>" data-detail="<?php echo $detail->db_coa_detail; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <p>Coa Kredit</p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="coa_kredit" onclick="return browse_kredit()" value="<?php echo $detail->cr_coa_name; ?>" data-id="<?php echo $detail->cr_coa_id; ?>" data-id="<?php echo $detail->cr_coa_id; ?>" data-code="<?php echo $detail->cr_coa_code; ?>" data-detail="<?php echo $detail->cr_coa_detail; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/config_journal'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="myCoaDebet" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Parent Coa</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="coa_popup_debet">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Coa Code</th>
                    <th>Coa Detail</th>
                    <th>Coa Name</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal inmodal" id="myCoaKredit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
      <div class="modal-header-popup">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4>Pilih Parent Coa</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="coa_popup_kredit">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Coa Code</th>
                    <th>Coa Detail</th>
                    <th>Coa Name</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" class="btn btn-primary" id="next" onclick="send_query(document.getElementsByTagName('input'))">Selanjutnya</button> -->
      </div>
    </div>
  </div>
</div>

<script>

$("#trn_code").select2();

var popup_debet = $('#coa_popup_debet').DataTable({ 
  "paging":   true,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { // disable sorting on column process buttons
      "targets": [0,1,2,3],
      "orderable": false,
    }
  ]
});

popup_debet.on('click', 'tr', function (){
  // debugger
  var data = popup_debet.row(this).data();
  var taskID = data[0];

  $('#coa_debet').val(data[3]);
  $('#coa_debet').attr('data-id', data[0]);
  $('#coa_debet').attr('data-code', data[1]);
  $('#coa_debet').attr('data-detail', data[2]);

  $('#myCoaDebet').modal('hide'); 
});

function browse_debet(e){  
  $.get({
    url: baseUrl + 'master/coa/get_low_coa_new',
    success: function(resp){
      popup_debet.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_debet.rows.add(dataSrc).draw(false);
    }
  });

  $('#myCoaDebet').modal('show'); 
}

var popup_kredit = $('#coa_popup_kredit').DataTable({ 
  "paging":   true,
  "ordering": false,
  "info":     false,
  "filter":   false,
  "language": {
    "emptyTable": "Tidak Ada Data"
  },
  "columnDefs": [
    { // disable sorting on column process buttons
      "targets": [0,1,2,3],
      "orderable": false,
    }
  ]
});

popup_kredit.on('click', 'tr', function (){
  // debugger
  var data = popup_kredit.row(this).data();
  var taskID = data[0];

  $('#coa_kredit').val(data[3]);
  $('#coa_kredit').attr('data-id', data[0]);
  $('#coa_kredit').attr('data-code', data[1]);
  $('#coa_kredit').attr('data-detail', data[2]);

  $('#myCoaKredit').modal('hide'); 
});

function browse_kredit(e){  
  $.get({
    url: baseUrl + 'master/coa/get_low_coa_new',
    success: function(resp){
      popup_kredit.clear().draw();
      var dataSrc = JSON.parse(resp.detail);                 
      popup_kredit.rows.add(dataSrc).draw(false);
    }
  });

  $('#myCoaKredit').modal('show'); 
}

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#trn_code').val()){
            toastr.error("<b>Kode Transaksi</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#trn_code').focus();
            return false;
        }

        if(!$('#coa_debet').val()){
            toastr.error("<b>Coa Debet</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#coa_debet').focus();
            return false;
        }

        if(!$('#coa_kredit').val()){
            toastr.error("<b>Coa Kredit</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#coa_kredit').focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/config_journal/edit_act',
            type : "POST",  
            data : {
                trn_code            : $('#trn_code').val(),
                cr_coa_name         : $('#coa_kredit').val(),
                cr_coa_id           : $('#coa_kredit').attr('data-id'),
                cr_coa_code         : $('#coa_kredit').attr('data-code'),
                cr_coa_detail       : $('#coa_kredit').attr('data-detail'),
                db_coa_name         : $('#coa_debet').val(),
                db_coa_code         : $('#coa_debet').attr('data-code'),
                db_coa_id           : $('#coa_debet').attr('data-id'),
                db_coa_detail       : $('#coa_debet').attr('data-detail'),
                jc_id               : $('#jc_id').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/config_journal/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>