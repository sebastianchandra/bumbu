<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Config Journal</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Config Journal</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/config_journal/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>
<?php 
// test($data_items,1);
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Kode Transaksi</th>
                    <th width="15%">Coa (D) Detail</th>
                    <th width="15%">Coa (D) Nama</th>
                    <th width="15%">Coa (K) Detail</th>
                    <th width="15%">Coa (K) Nama</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_config_journal as $key => $value) {

                    if($value->trn_code=="IS"){
                      $kd_trans     = "Incomming Stock ";
                    }elseif($value->trn_code=="IT"){
                      $kd_trans     = "Incomming Others ";
                    }elseif($value->trn_code=="OS"){
                      $kd_trans     = "Outgoing Stock ";
                    }elseif($value->trn_code=="OT"){
                      $kd_trans     = "Outgoing Others ";
                    }elseif($value->trn_code=="DO"){
                      $kd_trans     = "Delivery Order ";
                    }elseif($value->trn_code=="IC"){
                      $kd_trans     = "Invoice ";
                    }elseif($value->trn_code=="DS"){
                      $kd_trans     = "DP Sales Order ";
                    }elseif($value->trn_code=="PB"){
                      $kd_trans     = "PO Payment by Bank ";
                    }elseif($value->trn_code=="PC"){
                      $kd_trans     = "PO Payment by Cash ";
                    }elseif($value->trn_code=="IB"){
                      $kd_trans     = "Invoice Payment By Bank ";
                    }elseif($value->trn_code=="IH"){
                      $kd_trans     = "Invoice Payment By Bank ";
                    }
                  ?>
                  <tr>
                    <td><?php echo $value->trn_code." (".$kd_trans.")"; ?></td>
                    <td><?php echo $value->db_coa_detail; ?></td>
                    <td><?php echo $value->db_coa_name; ?></td>
                    <td><?php echo $value->cr_coa_detail; ?></td>
                    <td><?php echo $value->cr_coa_name; ?></td>
                    <td class="center">
                      <a href="<?php echo base_url('master/config_journal/edit/'.$value->jc_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-jc_id='<?php echo $value->jc_id; ?>' data-nama='<?php echo $kd_trans; ?>' class="btn btn-warning btn-xs" type="submit">Hapus</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Crew', exportOptions:{messageTop: 'Master Crew',columns: [0,1,2,3,4,5]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var jc_id    = $(this).data('jc_id');
    var nama     = $(this).data('nama');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+nama+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-jc_id="'+jc_id+'">Yes</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var jc_id  = $(e).data('jc_id');

  $.ajax({
    data: {
      jc_id  : jc_id
    },
    type : "POST",
    url: baseUrl+'master/config_journal/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/config_journal'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
