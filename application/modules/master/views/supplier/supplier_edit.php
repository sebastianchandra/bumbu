<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Suplier</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Edit Suplier</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                    <form class="form-horizontal">      
                        <div class="card-body-form">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Nama Supplier </p>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="supplier_name" value="<?php echo $data_supplier->supplier_name; ?>">
                                    <input type="hidden" class="form-control" id="supplier_id" value="<?php echo $data_supplier->supplier_id; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>NPWP</p>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="npwp" data-mask="99.999.999.9-999.999" value="<?php echo $data_supplier->npwp; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Alamat</p>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="address" value="<?php echo $data_supplier->address; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Kota</p>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="city" value="<?php echo $data_supplier->city; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Kontak</p>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="contact1" value="<?php echo $data_supplier->contact1; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Email</p>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="email" value="<?php echo $data_supplier->email1; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Pic Sales</p>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="pic_sales" value="<?php echo $data_supplier->pic_sales; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>Remarks</p>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="supplier_info" value="<?php echo $data_supplier->supplier_info; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p>TOP</p>
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" id="top" value="<?php echo $data_supplier->top; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <hr/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2"></div>
                                <div class="col-md-7">
                                    <button type="button" class="btn btn-primary btn-sm" id="save">Simpan</button>
                                    <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/supplier'); ?>">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#supplier_name').focus();
    
    // $("#email").inputmask({ alias: "email"});
    $('#save').click(
        function(e){
        e.preventDefault();

        if(!$('#supplier_name').val()){
            toastr.error("<b>Nama Supplier</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#supplier_name").focus();
            return false;
        }

        if($('#supplier_name').val().length>=51){
            toastr.error("<b>Supplier Name</b> Max 50 Huruf", 'Alert', {"positionClass": "toast-top-center"});
            $("#supplier_name").focus();
            return false;
        }

        if(!$('#address').val()){
            toastr.error("<b>Alamat</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#address").focus();
            return false;
        }

        if(!$('#pic_sales').val()){
            toastr.error("<b>PIC Sales</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $("#pic_sales").focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'master/supplier/edit_act',
            type : "POST",  
            data : {
                supplier_name       : $('#supplier_name').val(),
                supplier_id         : $('#supplier_id').val(),
                npwp                : $('#npwp').val(),
                address             : $('#address').val(),
                city                : $('#city').val(),
                contact1            : $('#contact1').val(),
                email               : $('#email').val(),
                pic_sales           : $('#pic_sales').val(),
                supplier_info       : $('#supplier_info').val(),
                top                 : $('#top').val()                
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data berhasil di Update.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'master/supplier'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>