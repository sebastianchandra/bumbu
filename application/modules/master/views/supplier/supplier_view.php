<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Master</li>
                  <li class="breadcrumb-item">Suplier</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >View Suplier</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <a class="btn btn-outline-secondary" href="<?= base_url("master/supplier/form"); ?>">Input</a>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Kode</th>
                  <th width="20%">Nama Supplier</th>
                  <!-- <th>Kota</th> -->
                  <th>Alamat</th>
                  <th>Kontak</th>
                  <th>Pic</th>
                  <th>Remarks</th>
                  <th width="13%">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                foreach ($data_supplier as $key => $value) {
                ?>
                <tr>
                  <td><?php echo $value->supplier_code; ?></td>
                  <td><?php echo $value->supplier_name; ?></td>
                  <!-- <td><?php echo $value->city; ?></td> -->
                  <td><?php echo $value->address; ?></td>
                  <td><?php echo $value->contact1; ?></td>
                  <td><?php echo $value->pic_sales; ?></td>
                  <td><?php echo $value->supplier_info; ?></td>
                  <td class="center">
                    <a href="<?php echo base_url('master/supplier/edit/'.$value->supplier_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                    <button id="delete" data-supplier_id='<?php echo $value->supplier_id; ?>' class="btn btn-warning btn-xs" type="submit">Hapus</button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
    $('#zero_config').DataTable();
</script>

<script>
$(document).ready(function(){
  $('#form-group row').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Master Supplier', exportOptions:{messageTop: 'Master Supplier', columns: [0,1,2,3,4]}},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#zero_config').on('click','#delete', function(e){
    var supplier_id  = $(this).data('supplier_id');

    toastr.warning(
      'Apakah anda Ingin Menghapus ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-supplier_id="'+supplier_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var supplier_id  = $(e).data('supplier_id');

  $.ajax({
    data: {
      supplier_id  : supplier_id
    },
    type : "POST",
    url: baseUrl+'master/supplier/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        toastr.success("Data Tidak berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});
        return false;

      } else {
        toastr.success("Data berhasi di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'master/supplier'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
