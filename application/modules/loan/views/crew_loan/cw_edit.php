<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Edit Pinjaman Crew</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Pinjaman Crew</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Tanggal </p>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="cl_date" value="<?php echo $detail->cl_date; ?>">
                                <input type="hidden" class="form-control" id="cl_id" value="<?php echo $detail->cl_id; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Crew</p>
                            </div>
                            <div class="col-md-5">
                                 <select class="form-control" id="crew_id">
                                    <option value="">Select</option>
                                    <?php 
                                    foreach ($list_crew as $key => $value) {
                                        if($value->crew_id==$detail->crew_id){
                                            echo "<option value='".$value->crew_id."' data-full_name='".$value->full_name."' selected>".$value->full_name."</option>";
                                        }else{
                                            echo "<option value='".$value->crew_id."' data-full_name='".$value->full_name."'>".$value->full_name."</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Jumlah Pinjaman</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control price" id="amount" value="<?php echo $detail->amount; ?>">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/fish'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#cl_date').focus();
$("#crew_id").select2();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#cl_date').val()){
            toastr.error("<b>Tanggal</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#cl_date').focus();
            return false;
        }
        if(!$('#crew_id').val()){
            toastr.error("<b>Nama</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#crew_id').focus();
            return false;
        }
        if(!$('#amount').val()){
            toastr.error("<b>Jumlah Pinjaman</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#amount').focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'loan/crew_loan/upd',
            type : "POST",  
            data : {
                cl_date             : $('#cl_date').val(),
                crew_id             : $('#crew_id').val(),
                full_name           : $('#crew_id option:selected').attr('data-full_name'),
                amount              : $('#amount').val().replace(/\,/g, ''),
                cl_id               : $('#cl_id').val()
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'loan/crew_loan/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>