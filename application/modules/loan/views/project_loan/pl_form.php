<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Input Pinjaman Project</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Pinjaman Project</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <!-- <a href="<?php echo base_url('master/items/form'); ?>" class="btn btn-primary">Input</a> -->
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Tanggal </p>
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control" id="cl_date">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Nama Project</p>
                            </div>
                            <div class="col-md-5">
                                 <select class="form-control" id="project_id">
                                    <option value="">Select</option>
                                    <?php 
                                    foreach ($list_project as $key => $value) {
                                        echo "<option value='".$value->project_id."' data-project_name='".$value->project_name."'>".$value->project_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2">
                                <p>Jumlah Pinjaman</p>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control price" id="amount">
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-12">
                                <hr/>
                            </div>
                        </div>
                        <div class="row-form">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">
                                <button type="button" class="btn btn-primary btn-sm" id="save">Save</button>
                                <a class="btn btn-warning btn-sm" href="<?php echo base_url('master/fish'); ?>">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#cl_date').focus();
$("#project_id").select2();

$(document).ready(function(){
    $('#save').click(
        function(e){
        e.preventDefault();
        if(!$('#cl_date').val()){
            toastr.error("<b>Tanggal</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#cl_date').focus();
            return false;
        }
        if(!$('#project_id').val()){
            toastr.error("<b>Nama Project</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#project_id').focus();
            return false;
        }
        if(!$('#amount').val()){
            toastr.error("<b>Jumlah Pinjaman</b> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
            $('#amount').focus();
            return false;
        }

        $.ajax({
            url  : baseUrl+'loan/project_loan/form_act',
            type : "POST",  
            data : {
                cl_date             : $('#cl_date').val(),
                project_id          : $('#project_id').val(),
                project_name        : $('#project_id option:selected').attr('data-project_name'),
                amount              : $('#amount').val().replace(/\,/g, '')
            },
            success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status==false){
                    toastr.error("Data Gagal disimpan.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;

                }else{
                    toastr.success("Data Berhasil Disimpan.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function(){
                        window.location.href = baseUrl+'loan/project_loan/'; //will redirect to google.
                    }, 2000);
                }
            }
        });
    });
});
</script>