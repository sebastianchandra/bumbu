<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>View Pinjaman Project</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Pinjaman Project</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?php echo base_url('loan/project_loan/form'); ?>" class="btn btn-primary">Input</a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="13%">ID Pinjaman</th>
                    <th>Nama Project</th>
                    <th width="11%">Tanggal</th>
                    <th width="13%">Jumlah</th>
                    <th width="13%">Sisa</th>
                    <th width="11%">Status</th>
                    <th width="13%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_project as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->cl_id; ?></td>
                    <td><?php echo $value->project_name; ?></td>
                    <td><?php echo tgl_singkat($value->cl_date); ?></td>
                    <td align="right"><?php echo money($value->amount); ?></td>
                    <td align="right"><?php echo money($value->remaining); ?></td>
                    <td><?php echo $value->cl_status; ?></td>
                    <td class="center">
                      <a href="<?php echo base_url('loan/project_loan/edit/'.$value->cl_id); ?>" class="btn btn-danger btn-xs">Edit</a>
                      <button id="delete" data-cl_id='<?php echo $value->cl_id; ?>' data-nama='<?php echo $value->project_name; ?>' class="btn btn-warning btn-xs" type="submit">Hapus</button>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      {extend: 'pdf', title: 'ExampleFile'},
      {extend: 'print',
        customize: function (win){
          $(win.document.body).addClass('white-bg');
          $(win.document.body).css('font-size', '10px');

          $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
        }
      }
    ]
  }); 

  $('#vtable').on('click','#delete', function(e){
    var cl_id    = $(this).data('cl_id');
    var nama     = $(this).data('nama');

    toastr.warning(
      'Apakah anda Ingin Menghapus '+cl_id+' ?<br /><br />'+
      '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-cl_id="'+cl_id+'">Ya</button> '+
      '<button type="button" id="noBtn" class="btn btn-info">Tidak</button>', 
      '<u>ALERT</u>', 
      {
        "positionClass": "toast-top-center",
        "onclick": null,
        "closeButton": false,
      }
    );
    
  });
});
function deleteRow(e){
  var cl_id  = $(e).data('cl_id');

  $.ajax({
    data: {
      cl_id  : cl_id
    },
    type : "POST",
    url: baseUrl+'loan/project_loan/delete_js',
    success : function(resp){

      if(resp.status == 'ERROR INSERT' || resp.status == false) {
        alert('Data Tidak berhasil di Hapus');
        return false;

      } else {
        toastr.success("Data Berhasil di Hapus.", 'Alert', {"positionClass": "toast-top-center"});

        setTimeout(function () {
          window.location.href = baseUrl+'loan/project_loan'; //will redirect to google.
        }, 2000);
      }
    }
  });
}
</script>
