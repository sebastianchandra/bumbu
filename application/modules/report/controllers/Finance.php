<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Finance extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('report/finance_model');
        $this->load->model('finance/closing_journal_model');
        $this->load->model('finance/journal_model');
        $this->load->model('master/coa_model');
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Laba Rugi', 'report/finance/laba_rugi', 'fa fa-wpforms', 'Report', '7', '1'); 

    function laba_rugi(){
        // test($this->input->post('warehouse_id'),1);
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/finance/laba_rugi'));
        $this->isMenu();
        $this->load->model('master/warehouse_model');
        $this->load->model('master/config_journal_model');
        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;
        $data['data_month']  = array(
                array('id' => '01', 'nama_bulan' => 'January'),
                array('id' => '02', 'nama_bulan' => 'February'),
                array('id' => '03', 'nama_bulan' => 'March'),
                array('id' => '04', 'nama_bulan' => 'April'),
                array('id' => '05', 'nama_bulan' => 'May'),
                array('id' => '06', 'nama_bulan' => 'June'),
                array('id' => '07', 'nama_bulan' => 'July'),
                array('id' => '08', 'nama_bulan' => 'August'),
                array('id' => '09', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        if(!empty($this->input->post('year'))){
            $tahun                      = $this->input->post('year');
            $bulan                      = $this->input->post('month');
            $cek_closing                = $this->closing_journal_model->cek_data($this->input->post('month'),$this->input->post('year'));
            $data['total_penjualan']    = $this->finance_model->total_penjualan($tahun,$bulan);
            $data['total_hpp']          = $this->finance_model->total_hpp($tahun,$bulan);
            $data['total_biaya']        = $this->finance_model->total_biaya($tahun,$bulan)->result();
            $data['row_biaya']          = $this->finance_model->total_biaya($tahun,$bulan)->num_rows();
            $data['total_lain2']        = $this->finance_model->total_lain2($tahun,$bulan)->result();
            $data['row_lain2']          = $this->finance_model->total_lain2($tahun,$bulan)->num_rows();
            // test($data,0);
            if($cek_closing==0){
                $penjualan              = $data['total_penjualan']->penjualan;
                $hpp                    = $data['total_hpp']->hpp;
                $tbiaya                 = 0;
                $lain2                  = 0;
                foreach ($data['total_biaya'] as $key => $value1) {
                    $tbiaya             = $tbiaya + $value1->db_value;
                }
                foreach ($data['total_lain2'] as $key => $value2) {
                    if($value2->kredit!=''){
                        $lain2    = $lain2+$value2->kredit;
                    }else if($value2->debet!=''){
                        $lain2    = $lain2 - $value2->debet;
                    }
                }
                $laba           = (($penjualan-$hpp)-$tbiaya)+$lain2;

                // $coa_debet      = $this->coa_model->get_detail_coa(192);
                // $coa_kredit     = $this->coa_model->get_detail_coa(191);

                // $getDataCount            = $tahun.$bulan.$this->journal_model->getNoDoc($tahun,$bulan);
                // $this->journal_model->setJournalId($this->security->xss_clean($getDataCount));
                // $this->journal_model->setTrnCode($this->security->xss_clean('RL'));
                // $this->journal_model->setJournalDate($this->security->xss_clean(dbnow()));
                // $this->journal_model->setYearPeriod($this->security->xss_clean($tahun));
                // $this->journal_model->setMonthPeriod($this->security->xss_clean($bulan));
                // $this->journal_model->setJournalDest($this->security->xss_clean(''));
                // $this->journal_model->setDestId($this->security->xss_clean(''));
                // $this->journal_model->setTrnId($this->security->xss_clean(''));
                // $this->journal_model->setDbCoaId($this->security->xss_clean(192));
                // $this->journal_model->setDbCoaDetail($this->security->xss_clean($coa_debet->coa_detail));
                // $this->journal_model->setDbCoaName($this->security->xss_clean($coa_debet->coa_name));
                // $this->journal_model->setDbValue($this->security->xss_clean($laba));
                // $this->journal_model->setDbInfo($this->security->xss_clean('Laporan R/L'));
                // $this->journal_model->setCrCoaId($this->security->xss_clean(191));
                // $this->journal_model->setCrCoaDetail($this->security->xss_clean($coa_kredit->coa_detail));
                // $this->journal_model->setCrCoaName($this->security->xss_clean($coa_kredit->coa_name));
                // $this->journal_model->setCrValue($this->security->xss_clean($laba));
                // $this->journal_model->setCrInfo($this->security->xss_clean('Laporan R/L'));
                // $this->journal_model->setPicPosting($this->security->xss_clean($this->current_user['user_id']));

                $cek_data       = $this->journal_model->cek_rl($tahun,$bulan)->num_rows();
                $jourdata       = $this->journal_model->cek_rl($tahun,$bulan)->row();
                $cek_config     = $this->config_journal_model->cek_config("RL")->num_rows();
                $config_journal = $this->config_journal_model->cek_config("RL")->row();

                if($cek_config<=0){
                    $this->session->set_flashdata('alert','Input Config Journal Untuk Report Rugi Laba Terlebih Dahulu');
                    redirect('report/finance/laba_rugi');
                }else{
                    if($laba>=0){
                        $coa_debet      = $this->coa_model->get_detail_coa($config_journal->db_coa_id);
                        $coa_kredit     = $this->coa_model->get_detail_coa($config_journal->cr_coa_id);

                    }else{
                        $coa_debet      = $this->coa_model->get_detail_coa($config_journal->cr_coa_id);
                        $coa_kredit     = $this->coa_model->get_detail_coa($config_journal->db_coa_id);
                    }
                }

                
                
                if($cek_data==0){
                    $getDataCount            = $tahun.$bulan.$this->journal_model->getNoDoc($tahun,$bulan);
                    $this->journal_model->setJournalId($this->security->xss_clean($getDataCount));
                    $this->journal_model->setTrnCode($this->security->xss_clean('RL'));
                    $this->journal_model->setJournalDate($this->security->xss_clean(dbnow()));
                    $this->journal_model->setYearPeriod($this->security->xss_clean($tahun));
                    $this->journal_model->setMonthPeriod($this->security->xss_clean($bulan));
                    $this->journal_model->setJournalDest($this->security->xss_clean(''));
                    $this->journal_model->setDestId($this->security->xss_clean(''));
                    $this->journal_model->setTrnId($this->security->xss_clean(''));
                    $this->journal_model->setDbCoaId($this->security->xss_clean($coa_debet->coa_id));
                    $this->journal_model->setDbCoaDetail($this->security->xss_clean($coa_debet->coa_detail));
                    $this->journal_model->setDbCoaName($this->security->xss_clean($coa_debet->coa_name));
                    $this->journal_model->setDbValue($this->security->xss_clean($laba));
                    $this->journal_model->setDbInfo($this->security->xss_clean('Laporan R/L'));
                    $this->journal_model->setCrCoaId($this->security->xss_clean($coa_kredit->coa_id));
                    $this->journal_model->setCrCoaDetail($this->security->xss_clean($coa_kredit->coa_detail));
                    $this->journal_model->setCrCoaName($this->security->xss_clean($coa_kredit->coa_name));
                    $this->journal_model->setCrValue($this->security->xss_clean($laba));
                    $this->journal_model->setCrInfo($this->security->xss_clean('Laporan R/L'));
                    $this->journal_model->setPicPosting($this->security->xss_clean($this->current_user['user_id']));
                    $save     = $this->journal_model->insert_manual(1);
                    // test('1',0);
                }else{
                    $this->journal_model->setTrnCode($this->security->xss_clean('RL'));
                    $this->journal_model->setJournalDate($this->security->xss_clean(dbnow()));
                    $this->journal_model->setYearPeriod($this->security->xss_clean($tahun));
                    $this->journal_model->setMonthPeriod($this->security->xss_clean($bulan));
                    $this->journal_model->setJournalDest($this->security->xss_clean(''));
                    $this->journal_model->setDestId($this->security->xss_clean(''));
                    $this->journal_model->setTrnId($this->security->xss_clean(''));
                    $this->journal_model->setDbCoaId($this->security->xss_clean($coa_debet->coa_id));
                    $this->journal_model->setDbCoaDetail($this->security->xss_clean($coa_debet->coa_detail));
                    $this->journal_model->setDbCoaName($this->security->xss_clean($coa_debet->coa_name));
                    $this->journal_model->setDbValue($this->security->xss_clean($laba));
                    $this->journal_model->setDbInfo($this->security->xss_clean('Laporan R/L'));
                    $this->journal_model->setCrCoaId($this->security->xss_clean($coa_kredit->coa_id));
                    $this->journal_model->setCrCoaDetail($this->security->xss_clean($coa_kredit->coa_detail));
                    $this->journal_model->setCrCoaName($this->security->xss_clean($coa_kredit->coa_name));
                    $this->journal_model->setCrValue($this->security->xss_clean($laba));
                    $this->journal_model->setCrInfo($this->security->xss_clean('Laporan R/L'));
                    $this->journal_model->setPicPosting($this->security->xss_clean($this->current_user['user_id']));
                    $save          = $this->journal_model->update($jourdata->journal_id);
                    // test('2',0);
                }

                // test($penjualan-($hpp+$tbiaya).' '.$lain2,0);
                // test($laba,0);
            }
        }else{
            $data['total_penjualan']     = array();
        }

        $this->template->load('body', 'report/laba_rugi',$data);
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Neraca', 'report/finance/neraca', 'fa fa-wpforms', 'Report', '8', '1'); 

    // Query Aktifa
    // SELECT a.coa_detail,a.coa_name,a.is_lowest,SUM(b.db_value)debet FROM db_bumbu_master.mst_coa a 
    // LEFT JOIN db_fa_shp.trn_journal b ON b.db_coa_id=a.coa_id 
    // WHERE SUBSTR(a.coa_detail,1,2)IN ('01','02') AND a.is_active='1' 
    // AND IF(a.is_lowest=1,b.db_value IS NOT NULL, a.is_lowest=0)
    // GROUP BY a.coa_id
    // ORDER BY a.coa_detail

    // Query Pasifa
    // SELECT a.coa_detail,a.coa_name,a.is_lowest,SUM(b.db_value)debet FROM db_bumbu_master.mst_coa a 
    // LEFT JOIN db_fa_shp.trn_journal b ON b.db_coa_id=a.coa_id 
    // WHERE SUBSTR(a.coa_detail,1,2)IN ('03','04','05','06','07','08') AND a.is_active='1' 
    // AND IF(a.is_lowest=1,b.db_value IS NOT NULL, a.is_lowest=0)
    // GROUP BY a.coa_id
    // ORDER BY a.coa_detail

//Neraca
//     SELECT a.coa_id,a.coa_name,
// (SELECT SUM(db_value) FROM db_fa_shp.trn_journal a1 WHERE SUBSTR(a1.db_coa_detail,1,2)='01' AND a1.db_coa_id=a.coa_id GROUP BY a1.db_coa_id) debet,
// (SELECT SUM(cr_value) FROM db_fa_shp.trn_journal a1 WHERE SUBSTR(a1.cr_coa_detail,1,2)='01' AND a1.cr_coa_id=a.coa_id GROUP BY a1.cr_coa_id) kredit
// FROM db_bumbu_master.mst_coa a WHERE SUBSTR(a.coa_detail,1,2)='01' AND a.is_active='1'

    function neraca(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/finance/neraca'));
        $this->isMenu();
        $this->load->model('master/warehouse_model');
        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;
        $data['data_month']  = array(
                array('id' => '01', 'nama_bulan' => 'January'),
                array('id' => '02', 'nama_bulan' => 'February'),
                array('id' => '03', 'nama_bulan' => 'March'),
                array('id' => '04', 'nama_bulan' => 'April'),
                array('id' => '05', 'nama_bulan' => 'May'),
                array('id' => '06', 'nama_bulan' => 'June'),
                array('id' => '07', 'nama_bulan' => 'July'),
                array('id' => '08', 'nama_bulan' => 'August'),
                array('id' => '09', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        $kode_aktifa        = '01';
        $kode_pasifa        = "'02','03'";

        if(!empty($this->input->post('year'))){
            $tahun                      = $this->input->post('year');
            $bulan                      = $this->input->post('month');
            $data['total_aktiva']       = $this->finance_model->neraca_query_aktiva($kode_aktifa/*,$tahun,$bulan*/)->result();
            $data['total_pasiva']       = $this->finance_model->neraca_query_pasiva($kode_pasifa/*,$tahun,$bulan*/)->result();
            // test($data,0);
            
        }else{
            $data['total_penjualan']     = array();
        }

        $this->template->load('body', 'report/neraca',$data);
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Buku Besar', 'report/finance/buku_besar', 'fa fa-wpforms', 'Report', '9', '1'); 

    function buku_besar(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/finance/buku_besar'));
        $this->isMenu();
        $this->load->model('master/warehouse_model');
        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;
        $data['data_month']  = array(
                array('id' => '01', 'nama_bulan' => 'January'),
                array('id' => '02', 'nama_bulan' => 'February'),
                array('id' => '03', 'nama_bulan' => 'March'),
                array('id' => '04', 'nama_bulan' => 'April'),
                array('id' => '05', 'nama_bulan' => 'May'),
                array('id' => '06', 'nama_bulan' => 'June'),
                array('id' => '07', 'nama_bulan' => 'July'),
                array('id' => '08', 'nama_bulan' => 'August'),
                array('id' => '09', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        if(!empty($this->input->post('year'))){
            $tahun                      = $this->input->post('year');
            $bulan                      = $this->input->post('month');
            $tahun_sebelum              = date('Y', strtotime('-1 month', strtotime($tahun.'-'.$bulan.'-01')));
            $bulan_sebelum              = date('m', strtotime('-1 month', strtotime($tahun.'-'.$bulan.'-01')));
            $data['coa_history']        = $this->finance_model->coa_history($tahun_sebelum,$bulan_sebelum)->result();
            // $data['journal_history']    = $this->finance_model->journal_history($tahun,$bulan)->result();
            
        }else{
            $data['total_penjualan']     = array();
        }

        $this->template->load('body', 'report/buku_besar',$data);
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Sisa Hutang Supplier / Project', 'report/finance/sisa_hutang', 'fa fa-wpforms', 'Report', '10', '1'); 

    function sisa_hutang(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/finance/sisa_hutang'));
        $this->isMenu();

        $this->load->model('master/supplier_model');
        $this->load->model('master/project_model');
        $data['data_supplier']  = $this->supplier_model->get_supplier();
        $data['data_project']   = $this->project_model->get_project();

        if(($this->input->post('supplier_id')!='') OR ($this->input->post('project_id')!='')){
            $data['data_hutang_supplier']     = $this->finance_model->sisa_hutang($this->input->post('supplier_id'),$this->input->post('project_id'))->result();
        }else{
            $data['data_hutang_supplier']     = array();
        }

        $this->template->load('body', 'report/sisa_hutang',$data);
    }

}
?>