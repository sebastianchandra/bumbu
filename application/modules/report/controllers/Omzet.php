<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Omzet extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('report/omzet_model');
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Price History', 'report/omzet/history_price', 'fa fa-wpforms', 'Report', '10', '1'); 

    function history_price(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/omzet/history_price'));
        $this->isMenu();
        $this->load->model('master/warehouse_model');
        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;
        $data['data_month']  = array(
                array('id' => '01', 'nama_bulan' => 'January'),
                array('id' => '02', 'nama_bulan' => 'February'),
                array('id' => '03', 'nama_bulan' => 'March'),
                array('id' => '04', 'nama_bulan' => 'April'),
                array('id' => '05', 'nama_bulan' => 'May'),
                array('id' => '06', 'nama_bulan' => 'June'),
                array('id' => '07', 'nama_bulan' => 'July'),
                array('id' => '08', 'nama_bulan' => 'August'),
                array('id' => '09', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        if(!empty($this->input->post('tgl_dari'))){
            $tgl_dari                      = $this->input->post('tgl_dari');
            $tgl_sampai                    = $this->input->post('tgl_sampai');
            // $data['history_price']      = $this->omzet_model->history_price_group_by_sales_no($tahun,$bulan)->result();
            $data['row1']               = $this->omzet_model->sales_no_list($tgl_dari,$tgl_sampai)->result();
            $data['history_price']      = $this->omzet_model->history_price($tgl_dari,$tgl_sampai)->result();
            
        }else{
            $data['history_price']      = array();
        }

        $this->template->load('body', 'report/history_harga',$data);
    }

    function total_penjualan(){
        // $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/stok/history_stok'));
        // $this->isMenu();
        // $this->load->model('master/warehouse_model');
        // $data['data_warehouse'] = $this->warehouse_model->get_warehouse();
        // $data['data_month']  = array(
        //         array('id' => '1', 'nama_bulan' => 'January'),
        //         array('id' => '2', 'nama_bulan' => 'February'),
        //         array('id' => '3', 'nama_bulan' => 'March'),
        //         array('id' => '4', 'nama_bulan' => 'April'),
        //         array('id' => '5', 'nama_bulan' => 'May'),
        //         array('id' => '6', 'nama_bulan' => 'June'),
        //         array('id' => '7', 'nama_bulan' => 'July'),
        //         array('id' => '8', 'nama_bulan' => 'August'),
        //         array('id' => '9', 'nama_bulan' => 'September'),
        //         array('id' => '10', 'nama_bulan' => 'October'),
        //         array('id' => '11', 'nama_bulan' => 'November'),
        //         array('id' => '12', 'nama_bulan' => 'December'));

        // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Total Penjualan', 'report/omzet/total_penjualan', 'mdi mdi-move-resize-variant', 'Report', '14', '1'); 

        if(!empty($this->input->post('tgl_dari'))){
            $data['data_sales']  = $this->omzet_model->get_data_sales($this->input->post('tgl_dari'),$this->input->post('tgl_sampai'));
        }else{
            $data['data_sales']     = array();
        }

        $this->template->load('body', 'report/total_penjualan',$data);
    }

    function total_penjualan_excell($tgl_dari,$tgl_sampai,$status){
        $data['tgl_dari']    = $tgl_dari;
        $data['tgl_sampai']  = $tgl_sampai;
        $data['status']      = $status;
        $data['data_sales']  = $this->omzet_model->get_data_sales($tgl_dari,$tgl_sampai);
        $this->load->view('report/total_penjualan_excell',$data);
    }

    function payment_customer(){
        if(!empty($this->input->post('tgl_dari'))){
            $data['data_sales']  = $this->omzet_model->payment_customer($this->input->post('tgl_dari'),$this->input->post('tgl_sampai'));
        }else{
            $data['data_sales']     = array();
        }

        $this->template->load('body', 'report/payment_customer',$data);
    }

    function payment_customer_excell($tgl_dari,$tgl_sampai,$status){
        $data['tgl_dari']    = $tgl_dari;
        $data['tgl_sampai']  = $tgl_sampai;
        $data['status']      = $status;
        $data['data_sales']  = $this->omzet_model->payment_customer($tgl_dari,$tgl_sampai);
        $this->load->view('report/payment_customer_excell',$data);
    }

}
?>