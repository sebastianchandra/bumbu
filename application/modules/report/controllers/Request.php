<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('report/request_model');
    }

    function index(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/request'));
        $this->isMenu();

        $this->load->model('master/users_group_model');
        $data['user_group']         = $this->users_group_model->get_user_group();

        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;

        if(!empty($this->input->post('year')) or !empty($this->input->post('dept'))){
            $data['data_request']  = $this->request_model->get_request();
        }

        $this->template->load('body', 'report/request_department',$data);
    }

    function movement_request(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/request/movement_request'));
        $this->isMenu();

        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;

        if(!empty($this->input->post('year')) or !empty($this->input->post('document'))){
            $data['data_movement']        = $this->request_model->get_movement();
        }

        $this->template->load('body', 'report/search_movement_request',$data);
    }

}
?>