<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('report/purchase_order_model');
    }

    function history(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/purchase_order/history'));
        $this->isMenu();
        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;
        $data['data_month']  = array(
                array('id' => '01', 'nama_bulan' => 'January'),
                array('id' => '02', 'nama_bulan' => 'February'),
                array('id' => '03', 'nama_bulan' => 'March'),
                array('id' => '04', 'nama_bulan' => 'April'),
                array('id' => '05', 'nama_bulan' => 'May'),
                array('id' => '06', 'nama_bulan' => 'June'),
                array('id' => '07', 'nama_bulan' => 'July'),
                array('id' => '08', 'nama_bulan' => 'August'),
                array('id' => '09', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        if(!empty($this->input->post('no_po'))){
            $data['data_po']        = $this->purchase_order_model->get_po();
            $data['data_hist']      = $this->purchase_order_model->get_hist_is();
            $data['data_is_detail'] = $this->purchase_order_model->get_hist_is_detail();
        }

        $this->template->load('body', 'report/search_po',$data);
    }

    function index(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/purchase_order'));
        $this->isMenu();
        $this->load->model('master/project_model');
        $data['data_project']       = $this->project_model->get_project();
        $data['year_now']           = date('Y');
        $data['year_old']           = date('Y')-2;
        $data['data_month']  = array(
                array('id' => '01', 'nama_bulan' => 'January'),
                array('id' => '02', 'nama_bulan' => 'February'),
                array('id' => '03', 'nama_bulan' => 'March'),
                array('id' => '04', 'nama_bulan' => 'April'),
                array('id' => '05', 'nama_bulan' => 'May'),
                array('id' => '06', 'nama_bulan' => 'June'),
                array('id' => '07', 'nama_bulan' => 'July'),
                array('id' => '08', 'nama_bulan' => 'August'),
                array('id' => '09', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        if(!empty($this->input->post('month')) or !empty($this->input->post('year')) or !empty($this->input->post('project'))){
            $data['data_po']        = $this->purchase_order_model->purchase_order();
            $data['data_po_sup']    = $this->purchase_order_model->purchase_order_supplier();
        }

        $this->template->load('body', 'report/purchase_order',$data);
    }

}
?>