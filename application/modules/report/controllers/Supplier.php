<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    require('./vendor/autoload.php');
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Supplier extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('report/purchase_order_model');
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'Accounts Payable Suppliers', 'report/supplier/account_payable_suppliers', 'fa fa-wpforms', 'Report', '6', '1');

    function account_payable_suppliers(){
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/supplier/account_payable_suppliers'));
        $this->isMenu();
        $this->load->model('master/supplier_model');
        $data['data_supplier'] = $this->supplier_model->get_supplier();
        if(!empty($this->input->post('supplier_id'))){
            $data['data_hutang_supplier']     = $this->purchase_order_model->get_hutang_supplier();
        }else{
            $data['data_hutang_supplier']     = array();
        }

        $this->template->load('body', 'report/hutang_supplier',$data);
    }

    function print_excell($warehouse_id,$tgl_dari,$tgl_sampai){

        $spreadsheet = new Spreadsheet();

        $boldFont = [
          'font' => [
            'bold' => true
              // 'color' => ['argb' => '0000FF'],
          ],
        ];

        $totalStyle = [
          'font' => [
            'bold' => true,
            'color' => ['argb' => '0000FF'],
          ],
        ];

        $allBorderStyle = [
          'borders' => [
            'allBorders' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $outlineBorderStyle = [
          'borders' => [
            'outline' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $topBorderStyle = [
          'borders' => [
            'top' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000'],
            ],
          ],
        ];

        $bottomBorderStyle = [  
          'borders' => [
            'bottom' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => ['argb' => '00000000']
            ],
          ],
        ];

        $bottomBorderStyleBold = [  
          'borders' => [
            'bottom' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
              'color' => ['argb' => '00000000']
            ],
          ],
        ];

        $fontColorRed = array(
            'font'  => array(
                'color' => array('rgb' => 'FF0002')
            )
        );

        $center = array();
        $center['alignment'] = array();
        $center['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER; 
        $center['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER; 

        $right = array();
        $right['alignment'] = array();
        $right['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT; 
        $right['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $left = array();
        $left['alignment'] = array();
        $left['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT; 
        $left['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);

        $header         = $this->stok_model->get_laststockhistoryExcell($warehouse_id);

        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'History Stok')
            ->setCellValue('A3', 'Tanggal')
            ->setCellValue('B3', 'No Transaksi')
            ->setCellValue('C3', 'Keterangan')
            ->setCellValue('D3', 'Harga')
            ->setCellValue('E3', 'Saldo Awal')
            ->setCellValue('F3', 'Barang Masuk')
            ->setCellValue('G3', 'Barang Keluar')
            ->setCellValue('H3', 'Jumlah Stok');

        $spreadsheet->getActiveSheet()->mergeCells("A1:H1");
        $spreadsheet->getActiveSheet()->getStyle("A1:H1")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A1:H1")->applyFromArray($boldFont);
        $spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true)->setSize(16);

        $spreadsheet->getActiveSheet()->getStyle("A3:H3")->applyFromArray($center);
        $spreadsheet->getActiveSheet()->getStyle("A3:H3")->applyFromArray($boldFont);
        $spreadsheet->getActiveSheet()->getStyle("A3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("B3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("C3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("D3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("E3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("F3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("G3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle("H3")->applyFromArray($outlineBorderStyle);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('989898');

        $no         = 3;
        foreach ($header as $key => $value) {
            $no         = $no+1;
            $debit      = '';
            $kredit     = '';
            $spreadsheet->getActiveSheet()->setCellValue('A'.$no, $value->items_name.' ('.$value->items_code.')');
            $spreadsheet->getActiveSheet()->mergeCells("A".$no.":H".$no);
            $spreadsheet->getActiveSheet()->getStyle("A".$no.":H".$no)->applyFromArray($left);
            $spreadsheet->getActiveSheet()->getStyle("A".$no.":H".$no)->applyFromArray($boldFont);
            $spreadsheet->getActiveSheet()->getStyle("A".$no.":H".$no)->applyFromArray($outlineBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("A".$no.":H".$no)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('DDDDDD');

            $query  = $this->db->query("SELECT a.trn_date,a.items_id, b.items_name,a.doc_no,a.warehouse_id,a.activity,a.qty,a.current_stock,
                        CASE
                            WHEN SUBSTR(a.doc_no,1,3) = 'SAT' THEN 'Saldo Awal'
                            WHEN SUBSTR(a.doc_no,1,3) = 'ISS' THEN (SELECT doc_ref FROM db_bumbu_transaction.trn_incoming_stock_01 WHERE is_no=a.doc_no)
                            WHEN SUBSTR(a.doc_no,1,3) = 'IST' THEN (SELECT IF(a1.project_id=0,b1.warehouse_name,c1.project_name) keterangan FROM db_bumbu_transaction.trn_incoming_stock_01 a1 LEFT JOIN db_bumbu_master.mst_warehouse b1 ON a1.warehouse_id=b1.warehouse_id LEFT JOIN db_bumbu_master.mst_project c1 ON a1.project_id=c1.project_id WHERE a1.is_no=a.doc_no)
                            WHEN SUBSTR(a.doc_no,1,3) = 'ISR' THEN 'Reject Incoming'
                            WHEN SUBSTR(a.doc_no,1,3) = 'OSS' THEN (SELECT IF(a2.project_id=0,b2.warehouse_name,c2.project_name) keterangan FROM db_bumbu_transaction.trn_outgoing_stock_01 a2 LEFT JOIN db_bumbu_master.mst_warehouse b2 ON a2.dest_wh_id=b2.warehouse_id LEFT JOIN db_bumbu_master.mst_project c2 ON a2.project_id=c2.project_id WHERE a2.os_no=a.doc_no)
                            WHEN SUBSTR(a.doc_no,1,3) = 'OST' THEN (SELECT remarks FROM db_bumbu_transaction.trn_outgoing_stock_01 a2 WHERE a2.os_no=a.doc_no)
                            WHEN SUBSTR(a.doc_no,1,3) = 'OSR' THEN 'Reject Outgoing'
                            WHEN SUBSTR(a.doc_no,1,3) = 'OSJ' THEN 'Adjustment Stock'
                            WHEN SUBSTR(a.doc_no,1,3) = 'UVS' THEN 'Konversi Barang'
                            WHEN SUBSTR(a.doc_no,1,3) = 'UVR' THEN 'Reject Konversi Barang'
                            ELSE ' '
                        END AS keterangan,c.items_price
                        FROM db_bumbu_transaction.trn_stock_hist a
                        LEFT JOIN db_bumbu_transaction.trn_stock_by_doc c ON a.items_id=c.items_id AND a.warehouse_id=c.warehouse_id AND a.doc_no=c.doc_no, db_bumbu_master.mst_items b
                        WHERE a.items_id=b.items_id AND a.trn_date BETWEEN '".$tgl_dari."' AND '".$tgl_sampai."' 
                        AND a.current_stock>=0 AND a.warehouse_id='".$warehouse_id."' AND a.items_id='".$value->items_id."' ORDER BY a.stock_hist_id ASC")->result();
            foreach ($query as $key => $value2) {
                $no         = $no+1;
                $saldo_awal     = 0;
                if($value2->activity=='Saldo Awal' OR $value2->activity=='In_Po' OR $value2->activity=='In_Adjustment' OR $value2->activity=='Rj_OutTrans' OR $value2->activity=='In_Trans' OR $value2->activity=='In_Others' OR $value2->activity=='In_Conv' OR $value2->activity=='Rj_ConvOut' OR $value2->activity=='in'){
                    $saldo_awal   = $value2->current_stock-$value2->qty;
                }elseif($value2->activity=='Rj_InTrans' OR $value2->activity=='Out_Tr' OR $value2->activity=='Out_tr' OR $value2->activity=='Out_Oth' OR $value2->activity=='Out_Adj' OR $value2->activity=='Out_Conv' OR $value2->activity=='Rj_ConvIn' OR $value2->activity=='out'){
                    $saldo_awal   = $value2->current_stock+$value2->qty;
                }

                if($value2->activity=='Saldo Awal' 
                    OR $value2->activity=='in'
                    OR $value2->activity=='In_Po' 
                    OR $value2->activity=='In_Adjustment'
                    OR $value2->activity=='Rj_OutTrans' 
                    OR $value2->activity=='In_Trans' 
                    OR $value2->activity=='In_Others' 
                    OR $value2->activity=='In_Conv' 
                    OR $value2->activity=='Rj_ConvOut'){ 
                    $debit  = number_format($value2->qty,2); 
                }

                if($value2->activity=='Rj_InTrans' 
                    OR $value2->activity=='out'
                    OR $value2->activity=='Out_Tr' 
                    OR $value2->activity=='Out_tr' 
                    OR $value2->activity=='Out_Oth' 
                    OR $value2->activity=='Out_Adj' 
                    OR $value2->activity=='Out_Conv' 
                    OR $value2->activity=='Rj_ConvIn'){ 
                    $kredit = number_format($value2->qty,2); 
                }

                $spreadsheet->getActiveSheet()
                    ->setCellValue('A'.$no, $value2->trn_date)
                    ->setCellValue('B'.$no, $value2->doc_no)
                    ->setCellValue('C'.$no, $value2->keterangan)
                    ->setCellValue('D'.$no, $value2->items_price)
                    ->setCellValue('E'.$no, $saldo_awal)
                    ->setCellValue('F'.$no, $debit)
                    ->setCellValue('G'.$no, $kredit)
                    ->setCellValue('H'.$no, $value2->current_stock);

                $spreadsheet->getActiveSheet()->getStyle("A".$no.":H".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("G".$no)->applyFromArray($fontColorRed);
                $spreadsheet->getActiveSheet()->getStyle("A".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("B".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("C".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("D".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("E".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("F".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("G".$no)->applyFromArray($outlineBorderStyle);
                $spreadsheet->getActiveSheet()->getStyle("H".$no)->applyFromArray($outlineBorderStyle);

                $debit      = '';
                $kredit     = '';

            }

            $no     = $no+1;
            $spreadsheet->getActiveSheet()
            ->setCellValue('A'.$no, 'Total Barang ('.$value->items_name.')')
            ->setCellValue('H'.$no, $value->current_stock);

            $spreadsheet->getActiveSheet()->mergeCells("A".$no.":G".$no);
            $spreadsheet->getActiveSheet()->getStyle("A".$no)->applyFromArray($right);
            $spreadsheet->getActiveSheet()->getStyle("A3:H3")->applyFromArray($boldFont);
            $spreadsheet->getActiveSheet()->getStyle("A".$no)->applyFromArray($outlineBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("H".$no)->applyFromArray($outlineBorderStyle);
            $spreadsheet->getActiveSheet()->getStyle("A".$no.":H".$no)->applyFromArray($boldFont);

        }

        // $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
        // $spreadsheet->getDefaultStyle()->getProtection()->setLocked(true);
        // $spreadsheet->getActiveSheet()->getStyle('A1:H'.$no)->getProtection()->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

        unset($allBorderStyle);
        unset($center);
        unset($right);
        unset($left);

        $spreadsheet->setActiveSheetIndex(0);

        $str = 'History Stok';
        $fileName = preg_replace('/\s+/', '', $str);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.Xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');

        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
        header('Cache-Control: cache, must-revalidate'); 
        header('Pragma: public'); 

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit(0);
    }

    // INSERT INTO `db_bumbu_master`.`mst_menu` (`app_code`, `menu_name`, `url`, `icon`, `menu_group`, `no`, `active`) VALUES ('PURC', 'History Items', 'report/stok/history_items', 'fa fa-wpforms ', 'Report', '5', '1'); 

    function history_items(){
        // test($this->input->post('warehouse_id'),1);
        $this->session->set_userdata('ses_menu', array('active_menu' => 'Report', 'active_submenu' => 'report/stok/history_items'));
        $this->isMenu();
        $this->load->model('master/items_model');
        $data['data_items'] = $this->items_model->get_items_all();
        $data['data_month']  = array(
                array('id' => '1', 'nama_bulan' => 'January'),
                array('id' => '2', 'nama_bulan' => 'February'),
                array('id' => '3', 'nama_bulan' => 'March'),
                array('id' => '4', 'nama_bulan' => 'April'),
                array('id' => '5', 'nama_bulan' => 'May'),
                array('id' => '6', 'nama_bulan' => 'June'),
                array('id' => '7', 'nama_bulan' => 'July'),
                array('id' => '8', 'nama_bulan' => 'August'),
                array('id' => '9', 'nama_bulan' => 'September'),
                array('id' => '10', 'nama_bulan' => 'October'),
                array('id' => '11', 'nama_bulan' => 'November'),
                array('id' => '12', 'nama_bulan' => 'December'));

        if(!empty($this->input->post('items_id'))){
            $data['data_stok_current']  = $this->stok_model->get_historyitems();
        }else{
            $data['data_stok_current']     = array();
        }

        $this->template->load('body', 'report/history_items',$data);
    }

}
?>