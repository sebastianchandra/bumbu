<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Sisa Hutang Suppliers / Project</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Report</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Sisa Hutang Suppliers / Project</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>
<?php 
// test($data_hutang_supplier,0);
$res_data1 = array();
$res_data2 = array();

foreach($data_hutang_supplier as $key=>$val1){ 
  $res_data1[$val1->po_id]=$val1; 
}
foreach($data_hutang_supplier as $key=>$val2){ 
  $res_data2[$val2->po_id][]=$val2; 
}
?>
<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('report/finance/sisa_hutang',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Supplier</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id='supplier_id' name="supplier_id" data-keterangan='supplier'>
                  <option value="0" > - ALL - </option>
                  <?php 
                  foreach ($data_supplier as $key => $value) {
                    if($this->input->post('supplier_id')==$value->supplier_id){
                    echo '<option value="'.$value->supplier_id.'" data-keterangan="supplier" selected>'.$value->supplier_name.'</option>';
                    }else{
                    echo '<option value="'.$value->supplier_id.'" data-keterangan="supplier">'.$value->supplier_name.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Project</p>
              </div>
              <div class="col-md-5">
                <select class="form-control" id='project_id' name="project_id">
                  <option value="0" > - ALL - </option>
                  <?php 
                  foreach ($data_project as $key => $value) {
                    if($this->input->post('project_id')==$value->project_id){
                    echo '<option value="'.$value->project_id.'" data-keterangan="project" selected>'.$value->project_name.'</option>';
                    }else{
                    echo '<option value="'.$value->project_id.'" data-keterangan="project">'.$value->project_name.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>           
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('supplier_id')!=''){ ?>
                <!-- <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a> -->
                <!-- <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a> -->
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <?php echo form_close(); ?>                 
            <div>
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Project Name</th>
                    <th>Supplier Name</th>
                    <th>Po Date</th>
                    <th>PO Number</th>
                    <th>PO Status</th>
                    <!-- <th>Payment No</th> -->
                    <th>Payment Status</th>
                    <th>Total PO</th>
                    <th>Total Payment</th>
                    <th>Sisa</th>
                  </tr>
                </thead>
                  <?php
                  $sisa   = 0;
                  $tsisa  = 0;
                  foreach ($data_hutang_supplier as $key => $value2) {
                    $sisa     = $value2->t_po-$value2->t_payment;
                    $tsisa    = $tsisa+$sisa;
                    ?>
                    <tr>
                      <td><?php echo $value2->project_name ?></td>
                      <td><?php echo $value2->supplier_name ?></td>
                      <td><?php echo tgl_singkat($value2->po_date); ?></td>
                      <td><?php echo $value2->po_no ?></td>
                      <td><?php echo $value2->po_status ?></td>
                      <!-- <td><?php echo $value2->pp_no; ?></td> -->
                      <td><?php echo $value2->payment_status ?></td>
                      <td align="right"><?php echo number_format($value2->t_po,2) ?></td>
                      <td align="right"><?php echo number_format($value2->t_payment,2) ?></td>
                      <td align="right"><?php echo number_format($sisa,2) ?></td>
                    </tr>   
                    <?php
                  }
                  ?>
                <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <!-- <th></th> -->
                    <th></th>
                    <th></th>
                    <th align="right">Total : </th>
                    <th align="right"><?php echo number_format($tsisa,2); ?></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#supplier_id").select2();
$("#project_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": true, "paging": false, "ordering": false,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Hutang Per Supplier', exportOptions:{messageTop: 'Hutang Per Supplier'}},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

  // $('#save').click(function(){
  //   if($('#supplier_id').val()==0){
  //     toastr.error("<strong>Nama Supplier</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
  //     $('#supplier_id').select2('open');
  //     return false;
  //   }
  // });
});
</script>
