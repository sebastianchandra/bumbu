<?php
function lastOfMonth($year, $month) {
  return date("d M Y", strtotime('-1 second', strtotime('+1 month',strtotime($month . '/01/' . $year. ' 00:00:00'))));
}
?>  
<style type="text/css">
  .bold{
    font-weight: bold;
  }
</style>
<?php 
if($this->session->flashdata('alert')!=''){
?>
<script type="text/javascript">
toastr.info('<?php echo $this->session->flashdata('alert'); ?>', 'Info', {"positionClass": "toast-top-center"});
</script>
<?php
}
?>
<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-sm-4">
        <h2>Laba Rugi</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Report</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Laba Rugi</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('report/finance/laba_rugi',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Tahun</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>   
            <div class="row-form ">
              <div class="col-md-2">
                <p>Bulan</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    if($this->input->post('month')==$value['id']){
                      echo '<option value="'.$value['id'].'" selected>'.$value['nama_bulan'].'</option>';
                    }else{
                      echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>       
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('warehouse_id')!=''){ ?>
                <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a>
                <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <?php echo form_close(); ?>
            <?php
            if($this->input->post('year')!='' AND $this->input->post('month')!=''){

            $laba_kotor     = $total_penjualan->penjualan - $total_hpp->hpp;
            ?>              
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <tr class="bold">
                  <td colspan="3" align="center">
                      LEVONDI SOUWMPIE <br/>
                      LAPORAN LABA RUGI <br/>
                      <?php echo lastOfMonth($this->input->post('year'),$this->input->post('month')) ?>
                  </td>
                </tr>
                <tr class="bold">
                  <td>Penjualan</td>
                  <td width="30%"></td>
                  <td width="30%" align="right">Rp. <?php echo number_format($total_penjualan->penjualan,2); ?></td>
                </tr>
                <tr class="bold">
                  <td>HPP</td>
                  <td></td>
                  <td align="right" style="border-bottom-color: black;">Rp. <?php echo number_format($total_hpp->hpp,2); ?></td>
                </tr>
                <tr class="bold">
                  <td>Laba Kotor</td>
                  <td></td>
                  <td align="right">Rp. <?php echo number_format($laba_kotor,2); ?></td>
                </tr>
                <tr class="bold">
                  <td>Biaya Operasional</td>
                  <td></td>
                  <td></td>
                </tr>
                <?php 
                $tbiaya     = 0;
                $no         = 0;
                foreach ($total_biaya as $key => $value) {
                $tbiaya     = $tbiaya+$value->db_value;
                $no         = $no+1;
                ?>
                <tr>
                  <td align="right"><?php echo $value->coa_name; ?></td>
                  <td align="right">Rp. <?php echo number_format($value->db_value,2); ?></td>
                  <td <?php echo ($no==$row_biaya)? 'style="border-bottom-color: black;"' : '' ?>></td>
                </tr>
                <?php 
                }
                ?>
                <tr class="bold">
                  <td></td>
                  <td></td>
                  <td align="right">Rp. <?php echo number_format($tbiaya,2); ?></td>
                </tr>
                <tr class="bold">
                  <td>Pendapatan / Beban Lain-Lain</td>
                  <td></td>
                  <td></td>
                </tr>
                <?php 
                $total      = 0;
                $no         = 0;
                // test($total_lain2,1);
                foreach ($total_lain2 as $key => $value2) {
                if($value2->kredit!='' OR $value2->debet!=''){
                $no         = $no+1;
                if($value2->kredit!=''){
                  $total    = $total+$value2->kredit;
                }else if($value2->debet!=''){
                  $total    = $total - $value2->debet;
                }
                ?>
                <tr>
                  <td align="right"><?php echo $value2->coa_name; ?></td>
                  <td align="right">Rp. <?php echo ($value2->kredit!='')? number_format($value2->kredit,2) : '-'.number_format($value2->debet,2).'';?></td>
                  <td <?php echo ($no==$row_lain2)? 'style="border-bottom-color: black;"' : '' ?>></td>
                </tr>
                <?php 
                }
                }
                ?>
                <tr class="bold">
                  <td></td>
                  <td></td>
                  <td align="right" style="border-bottom-color: black;">Rp. <?php echo number_format($total,2); ?></td>
                </tr>
                <?php 
                $labarugi     = ($laba_kotor-$tbiaya)+$total;
                ?>
                <tr class="bold">
                  <td>Laba / Rugi Sebelum Pajak</td>
                  <td></td>
                  <td align="right">Rp. <?php echo number_format($labarugi,2); ?></td>
                </tr>
              </table>
            </div>
            <?php 
            }
            ?>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#year").select2();
$("#month").select2();

$(document).ready(function(){
  // $('#vtable').DataTable({
  //   aaSorting: [],
  //   "searching": false, "paging": false, "ordering": false,"bInfo" : false,
  //   pageLength: 25,
  //   responsive: false,
  //   dom: '<"html5buttons"B>lTfgitp',
  //   buttons: 
  //   [
  //     // {extend: 'copy'},
  //     // {extend: 'csv'},
  //     {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Stok'}},
  //     // {extend: 'pdf', title: 'ExampleFile'},
  //     // {extend: 'print',
  //     //   customize: function (win){
  //     //     $(win.document.body).addClass('white-bg');
  //     //     $(win.document.body).css('font-size', '10px');

  //     //     $(win.document.body).find('table')
  //     //       .addClass('compact')
  //     //       .css('font-size', 'inherit');
  //     //   }
  //     // }
  //   ]
  // }); 

  $('#save').click(function(){
    if(!$('#year').val()){
      toastr.error("<strong>Tahun</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
    if(!$('#month').val()){
      toastr.error("<strong>Bulan</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
  });
});
</script>
