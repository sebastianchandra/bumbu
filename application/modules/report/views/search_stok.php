<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Report</li>
                  <li class="breadcrumb-item">Stock</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Report History Stock</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="d-print-none">
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <?php echo form_open('report/stok',array('class' => 'form-horizontal')); ?>
          <div class="tile-body">
            <div class="form-group row">
              <label class="control-label col-md-2">Warehouse</label>
              <div class="col-md-5">
                <select class="form-control" id='warehouse' name="warehouse_id">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {
                    if($this->input->post('warehouse_id')==$value->warehouse_id){
                      echo '<option value="'.$value->warehouse_id.'" selected>'.$value->warehouse_name.'</option>';
                    }else{
                      echo '<option value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                    }
                    
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Year</label>
              <div class="col-md-2">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
              <label class="control-label col-md-1">Month</label>
              <div class="col-md-2">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    if($this->input->post('month')==$value['id']){
                      echo '<option value="'.$value['id'].'" selected>'.$value['nama_bulan'].'</option>';
                    }else{
                      echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Items Name</label>
              <div class="col-md-5">
                <select class="form-control" id='item_id' name="item_id">
                  <option value=""> - </option>
                </select>
              </div>
            </div>
          </div>
          <div class="tile-footer">
            <div class="row">
              <div class="col-md-8 col-md-offset-3">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-search"></i></button>&nbsp;&nbsp;&nbsp;
                <?php 
                if(!empty($this->input->post('warehouse_id')) or !empty($this->input->post('year')) or !empty($this->input->post('month'))){
                ?>
                <a class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i></a>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<?php 
  if(!empty($this->input->post('warehouse_id')) or !empty($this->input->post('year')) or !empty($this->input->post('month'))){
  $res_stok = array();
  foreach($data_stok_history as $key=>$val){ 
    $res_stok[$val->items_id][]=$val; 
  }
  //test($res_stok,0);
?>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <table>
            <tr></tr>
          </table>
          <?php 
          //test($data_stok_awal,0);
          foreach ($data_stok_awal as $key => $value) {
          ?>
          <table>
            <tr>
              <td width="175px"><strong>Code Name</strong></td>
              <td width="399px">: <?php echo $value->items_code; ?></td>
              <td width="175px"><strong>Warehouse</strong></td>
              <td>: <?php echo $value->warehouse_name; ?></td>
            </tr>
            <tr>
              <td><strong>Items Name</strong></td>
              <td>: <?php echo $value->items_name; ?></td>
              <td></td>
              <td></td>
            </tr>
          </table>
          <table class="table table-print">
            <tr>
              <th><strong>Items Name</strong></th>
              <th width="10%"><strong>Items Code</strong></th>
              <th width="10%"><strong>Doc Number</strong></th>
              <th width="8%"><strong>Date</strong></th>
              <th><strong>Activity</strong></th>
              <th width="10%"><strong>Saldo Awal</strong></th>
              <th width="8%"><strong>Qty In</strong></th>
              <th width="8%"><strong>Qty Out</strong></th>
              <th width="10%"><strong>Current Stock</strong></th>
            </tr>
            <?php 
            $current = 0;
            foreach ($res_stok[$value->items_id] as $key => $detail) {
            
            if($detail->activity=='In_Po' OR $detail->activity=='In_Trans' OR $detail->activity=='In_Other' OR $detail->activity=='Vo_po' OR $detail->activity=='Vo_Trans'){
              $current = $current+$detail->qty;
            }elseif($detail->activity=='Out_Tr' OR $detail->activity=='Out_Wo' OR $detail->activity=='Out_Ss'/*  OR $detail->activity=='Vo_po' OR $detail->activity=='Vo_Trans'*/){ 
              $current = $current-$detail->qty;
            }
              ?>
                <tr>
                  <td><?php echo $detail->items_name; ?></td>
                  <td><?php echo $detail->items_code; ?></td>
                  <td><?php echo $detail->doc_no; ?></td>
                  <td><?php echo tgl_singkat($detail->trn_date); ?></td>
                  <td><?php echo $detail->activity; ?></td>
                  <td align="right"><?php echo $detail->old_stock; ?></td>
                  <td align="right">
                    <?php 
                      if($detail->activity=='In_Po' OR $detail->activity=='In_Trans' OR $detail->activity=='In_Other' OR $detail->activity=='Vo_po' OR $detail->activity=='Vo_Trans'){ 
                        echo $detail->qty; 
                      }
                    ?>
                  </td>
                  <td align="right">
                    <?php 
                      if($detail->activity=='Out_Tr' OR $detail->activity=='Out_Wo' OR $detail->activity=='Out_Ss' /* OR $detail->activity=='Vo_po' OR $detail->activity=='Vo_Trans'*/){ 
                        echo $detail->qty; 
                      }
                    ?>
                  </td>
                  <td align="right"><?php echo $current; ?></td>
                </tr>
              <?php
            }
            ?>
          </table>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
<?php 
  }
?>

<script type="text/javascript">
  $("#warehouse").select2().on('select2:select',function(e){
    // var id = $('#warehouse').val();
    // $.ajax({
    //   url : baseUrl+'stok/trn_stok/get_stok',
    //   method : "POST",
    //   data : {id: id},
    //   async : false,
    //   dataType : 'json',
    //   success: function(data){
    //     var html = '';
    //     var i;
    //     html += '<option value="0" > - </option>';
    //     for(i=0; i<data.length; i++){
    //       html += '<option value="'+data[i].items_id+'" data-name="'+data[i].items_name+'" data-stok="'+data[i].current_stock+'">'+data[i].items_name+'</option>';
    //     }
    //     $('#item_id').html(html);
    //   }
    // })
  });

  $("#item_id").select2().on('select2:open',function(e){
    debugger
    var id = $('#warehouse').val();
    $.ajax({
      url : baseUrl+'stok/trn_stok/get_stok',
      method : "POST",
      data : {id: id},
      async : false,
      dataType : 'json',
      success: function(data){
        var html = '';
        var i;
        html += '<option value="0" > - </option>';
        for(i=0; i<data.length; i++){
          html += '<option value="'+data[i].items_id+'" data-name="'+data[i].items_name+'" data-stok="'+data[i].current_stock+'">'+data[i].items_name+'</option>';
        }
        $('#item_id').html(html);
      }
    })
  });

  $("#year").select2({
    allowClear: true
  });

  $("#month").select2({
    allowClear: true
  });

</script>