<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Report</li>
                  <li class="breadcrumb-item">Accounts payable</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Accounts payable Suppliers</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<?php 
// test($data_hutang_supplier,0);
$res_data1 = array();
$res_data2 = array();

foreach($data_hutang_supplier as $key=>$val1){ 
  $res_data1[$val1->po_id]=$val1; 
}
foreach($data_hutang_supplier as $key=>$val2){ 
  $res_data2[$val2->po_id][]=$val2; 
}
?>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <?php echo form_open('report/supplier/account_payable_suppliers',array('class' => 'container-fluid d-print-none')); ?>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Supplier</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='supplier_id' name="supplier_id">
                  <option value="0" > - Pilih - </option>
                  <?php 
                  foreach ($data_supplier as $key => $value) {
                    if($this->input->post('supplier_id')==$value->supplier_id){
                    echo '<option value="'.$value->supplier_id.'" selected>'.$value->supplier_name.'</option>';
                    }else{
                    echo '<option value="'.$value->supplier_id.'">'.$value->supplier_name.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>           
            <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('supplier_id')!=''){ ?>
                <!-- <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a> -->
                <!-- <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a> -->
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <?php echo form_close(); ?>                 
            <div>
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Po Date</th>
                    <th>PO Number</th>
                    <th>Project Name</th>
                    <th>Supplier Name</th>
                    <th>PO Status</th>
                    <th>Payment Status</th>
                    <th>Items Name</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                  <?php
                  $subtotal1  = 0; 
                  $subtotal2  = 0; 
                  $po_no      = '';
                  $urut       = 0;
                  foreach ($res_data1 as $key => $value1) {
                    foreach ($res_data2[$value1->po_id] as $key => $value2) {
                    $subtotal2   = $value2->price*$value2->qty;
                    ?>
                    <tr>
                      <td><?php echo tgl_singkat($value2->po_date); ?></td>
                      <td><?php echo $value2->po_no ?></td>
                      <td><?php echo $value2->project_name ?></td>
                      <td><?php echo $value2->supplier_name ?></td>
                      <td><?php echo $value2->po_status ?></td>
                      <td><?php echo $value2->payment_status ?></td>
                      <td><?php echo $value2->items_name ?></td>
                      <td><?php echo $value2->qty ?></td>
                      <td><?php echo $value2->price ?></td>
                      <td align="right"><?php echo number_format($subtotal2,2) ?></td>
                    </tr>   
                    <?php
                    $subtotal1    = $subtotal1+$subtotal2;
                    }
                  ?>
                <thead>
                  <tr align="right">
                    <th colspan="9"><strong>TOTAL <?php echo $value1->po_no; ?></strong></th>
                    <th><strong><u><?php echo number_format($subtotal1,2) ?></u></strong></th>
                  </tr>     
                </thead>             
                  <?php
                  $urut     = $urut + 1;
                  }
                  ?>
              </table>
            </div>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#supplier_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": true, "paging": false, "ordering": false,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Hutang Per Supplier', exportOptions:{messageTop: 'Hutang Per Supplier'}},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

  $('#save').click(function(){
    if($('#supplier_id').val()==0){
      toastr.error("<strong>Nama Supplier</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#supplier_id').select2('open');
      return false;
    }
  });
});
</script>
