<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<?php
function lastOfMonth($year, $month) {
  return date("d M Y", strtotime('-1 second', strtotime('+1 month',strtotime($month . '/01/' . $year. ' 00:00:00'))));
}
?>  
<style type="text/css">
  .bold{
    font-weight: bold;
  }
</style>

<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Report</li>
                  <li class="breadcrumb-item">History Price (Grafik)</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >History Price</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <?php echo form_open('report/omzet/history_price',array('class' => 'container-fluid d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="tgl_dari" name="tgl_dari" value="<?php //echo substr(dbnow(),0,10); ?>" >
                <!-- <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select> -->
              </div>
              <div class="col-md-1"> Sampai </div>
              <div class="col-md-3">
                <input type="date" class="form-control" id="tgl_sampai" name="tgl_sampai" value="<?php //echo substr(dbnow(),0,10); ?>" >
                <!-- <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select> -->
              </div>
            </div>   
            <!-- <div class="row-form ">
              <div class="col-md-2">
                <p>Bulan</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    if($this->input->post('month')==$value['id']){
                      echo '<option value="'.$value['id'].'" selected>'.$value['nama_bulan'].'</option>';
                    }else{
                      echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>  -->      
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('warehouse_id')!=''){ ?>
                <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a>
                <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <?php echo form_close(); ?>
            <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <div class="col-md-12">
            <?php
            if($this->input->post('tgl_dari')!='' AND $this->input->post('tgl_sampai')!=''){
            // $row1       = array();
            $row2       = array();
            $row3       = array();

            $tgl_dari         = $this->input->post('tgl_dari');
            $tgl_sampai       = $this->input->post('tgl_sampai');

            // foreach($history_price as $key=>$val1){ 
            //   $row1[$val1->sales_no]=$val1->sales_no; 
            // }     

            foreach($history_price as $key=>$val2){ 
              $row2[$val2->fish_name]=$val2; 
            }                   
            
            foreach ($history_price as $key => $val3) {
              $row3[$val3->fish_name][]=$val3; 
            }
            $jumlah     = count($row1);
            if($jumlah>='1'){
            ?>              
            
              <div id="container"></div>
            
            <script>
              Highcharts.chart('container', {
                  chart: {
                      type: 'line'
                  },
                  title: {
                      text: 'Grafik Harga Jual Ikan Per Sales'
                  },
                  subtitle: {
                      text: "Periode Tanggal <?php echo tgl_singkat($tgl_dari).' Sampai Tanggal '.tgl_singkat($tgl_sampai) ?>"
                  },
                  xAxis: {
                      categories: [<?php 
                      $jumlah     = count($row1);
                      $no         = 0;
                      foreach ($row1 as $key => $value) {
                        $no     = $no+1;
                        echo "'".$value->sales_no."'";
                        if($jumlah!=$no){
                          echo ',';
                        }
                      }
                      ?>]
                  },
                  yAxis: {
                      title: {
                          text: 'Harga'
                      }
                  },
                  plotOptions: {
                      line: {
                          dataLabels: {
                              enabled: true
                          },
                          enableMouseTracking: false
                      }
                  },
                  series: 
                  [
                    <?php 
                    $jumlah_barang    = count($row2);
                    $no2              = 0;
                    foreach ($row2 as $key => $value) {
                      $no2     = $no2+1;
                    ?>
                    {
                      name: <?php echo '"'.$value->fish_name.'"' ?>,
                      data: 
                      [
                        <?php 
                        $jumlah_price     = count($row1);
                        $no_price         = 0;
                        foreach ($row1 as $key => $val5) {
                          $no_price   = $no_price+1;
                          $query      = $this->db->query("SELECT a.sales_id,a.sales_no,a.sales_date,a.project_id,a.doc_status,b.fish_id,
                                              b.owner_price,b.cust_price,b.gov_price,b.qty,b.subtotal
                                              FROM db_omzet_shp.trn_sales_01 a 
                                              LEFT JOIN db_omzet_shp.trn_sales_02 b ON a.sales_id=b.sales_id
                                              WHERE a.doc_status<>'Reject' AND a.sales_date BETWEEN '".$tgl_dari."' AND '".$tgl_sampai."'
                                              AND a.sales_no='".$val5->sales_no."' 
                                              AND b.fish_id='".$value->fish_id."'");
                          $tot_sales      = $query->num_rows();
                          $data_sales     = $query->row();
                          if($tot_sales>=1){
                            $owner_price    = $data_sales->cust_price;
                          }else{
                            $owner_price    = $owner_price;
                          }
                          echo $owner_price;
                          if($jumlah_price!=$no_price){
                            echo ',';
                          }
                        }
                        ?>
                      ]
                    }
                    <?php 
                      if($jumlah_barang!=$no2){
                        echo ',';
                      }
                    }
                    ?>

                  ]
              });
            </script>
            <?php 
            }else{
              echo "Data Omzet Bulan ".$bulan." Tahun ".$tahun." Tidak ada";
            }
            }
            ?>
            </div>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#year").select2();
$("#month").select2();

$(document).ready(function(){
  // $('#vtable').DataTable({
  //   aaSorting: [],
  //   "searching": false, "paging": false, "ordering": false,"bInfo" : false,
  //   pageLength: 25,
  //   responsive: false,
  //   dom: '<"html5buttons"B>lTfgitp',
  //   buttons: 
  //   [
  //     // {extend: 'copy'},
  //     // {extend: 'csv'},
  //     {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Stok'}},
  //     // {extend: 'pdf', title: 'ExampleFile'},
  //     // {extend: 'print',
  //     //   customize: function (win){
  //     //     $(win.document.body).addClass('white-bg');
  //     //     $(win.document.body).css('font-size', '10px');

  //     //     $(win.document.body).find('table')
  //     //       .addClass('compact')
  //     //       .css('font-size', 'inherit');
  //     //   }
  //     // }
  //   ]
  // }); 

  $('#save').click(function(){
    if(!$('#tgl_dari').val()){
      toastr.error("<strong>Tanggal Dari</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
    if(!$('#tgl_sampai').val()){
      toastr.error("<strong>Tanggal Sampai</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
  });
});
</script>
