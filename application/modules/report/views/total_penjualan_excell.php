<?php
if($status=='excell'){
  $file = "Total_penjualan.xls";
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment; filename='.$file);
}
?>
<style>
/* table, td, th {
    border: 1px solid black;
} */

table.header {
    width: 100%;
    border-collapse: collapse;
    border-top: 1px solid black;
    border-left: 1px solid black;
    border-right: 1px solid black;
}
table td{
    padding-left: 5px;
}
table.body {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid black;
}
table.body, td.body {
  border: 1px solid black;
}
</style>
<?php 
// test($detail,0);
?>
<table class="header" id="vtable">
  <thead>
    <tr>
        <td></td>
        <td></td>
        <td colspan="2" align="center"><strong>Total Penjualan</strong></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2"><strong></strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td colspan="2" align="center"><strong><?php echo tgl_singkat($tgl_dari).' s/d '.tgl_singkat($tgl_sampai) ?></strong></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2"><strong></strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<table class="body" id="vtable">
  <thead>
    <tr>
        <th class="body" align="center">No</th>
        <th class="body" align="center">No Ref</th>
        <th class="body" align="center">Tanggal</th>
        <th class="body" align="center">Nama Customer</th>
        <th class="body" align="center">Status</th>
        <th class="body" align="center">Total</th>
    </tr>
    <?php
    $total = 0;
    foreach ($data_sales as $key => $value) {
        $total = $total+$value->total_cust_price;
    ?>
    <tr>
        <td class="body"><?php echo $value->invoice_no; ?></td>
        <td class="body"><?php echo $value->doc_ref; ?></td>
        <td class="body"><?php echo tgl_singkat($value->invoice_date); ?></td>
        <td class="body"><?php echo $value->fc_name; ?></td>
        <td class="body"><?php echo $value->doc_status; ?> <?= ($value->is_cancel==1)? '(Reject)' : '';?></td> 
        <td align="right" class="body"><?php echo number_format($value->total_cust_price,2); ?> </td> 
    </tr>
    <?php } ?>
    <tr>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body"></td>
        <td class="body" colspan="2">Total</td> 
        <td align="right" class="body"><?php echo number_format($total,2); ?> </td> 
    </tr>
    
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>