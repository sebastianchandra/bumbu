<?php 
// test($data_pr,0);
?>
<div class="app-title">
	<div>
		<h1>View Purchase Requisition Approve</h1>
		<p><ul class="app-breadcrumb breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
        <li class="breadcrumb-item">Report</li>
        <li class="breadcrumb-item active">Purchase Requisition Approve</li>
	</ul></p>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="table-responsive">
	            <table class="table table-hover table-bordered" id="prTable">
	                <thead>
        						<tr>
        							<th>PR Number</th>
                      <th>Request Number</th>
                      <th>Date</th>
        							<th>Project Name</th>
        							<th>Company Name</th>
        							<th>Supplier Name</th>
                      <th>requester</th>
        							<th>Status</th>
        						</tr>
	                </thead>
	                <tbody>
	                	<?php 
	                	$no = 0;
                		foreach ($data_pr as $key => $value) {
	                	?>
      						<tr>
      							<td><strong><a href="#" id='detail' onclick="return show_detail(this)" 
                    data-id="<?php echo $value->pr_id; ?>" 
                    data-pr="<?php echo $value->pr_no; ?>"><?php echo $value->pr_no; ?></a></strong></td>
                    <td><?php echo $value->request_no; ?></td>
                    <td><?php echo tgl_singkat($value->pr_date); ?></td>
      							<td><?php echo $value->project_name; ?></td>
      							<td><?php echo $value->company_name; ?></td>
      							<td><?php echo $value->supplier_name; ?></td>
      							<td><?php echo $value->requester; ?></td>
                    <td><?php 
                          if($value->jml_not_approve>=1){
                            echo '<strong><font color="red">Not Approve</font></strong>';
                          }else{
                            echo 'Approve';
                          }
                        ?>
                    </td>
      						</tr>
        						<?php 
        						}
        						?>
	                </tbody>
	            </table>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="void_dialog">
  <form id="form_void_dialog">
    <div class="form-group">
      <label for="void-type">Jenis Pembatalan</label>
      <select id="void-type" name="void-type" class="form-control">
        <option value="">Pilih Status</option>
      </select>
    </div>
  </form>
</script>

<script type="text/javascript">
$('#prTable').DataTable({
  	"paging": true, 
  	"bLengthChange": false, // disable show entries dan page
  	"bFilter": true,
  	"bInfo": true, // disable Showing 0 to 0 of 0 entries
  	"bAutoWidth": false,
  	"language": {
      	"emptyTable": "Tidak Ada Data"
    },
  	"aaSorting": [],
  	responsive: true
});

$('#prTable').on('click','#process', function (e) {
  var idPr     = $(this).data('id');
  var noPr     = $(this).data('pr');

  BootstrapDialog.show({
      title: 'Process Purchase Requisition ',
      type : BootstrapDialog.TYPE_INFO,
      message: 'Do you want to process Purchase Requisition Number <strong>'+noPr+'</storng> ?',
      closable: false,
      buttons: [
        {
          label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: '<i class="fa fa-check"></i> Process', cssClass: 'btn-info', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            $.ajax({
                data: {
                    id_pr : idPr,
                    no_pr : noPr
                },
                type : "POST",
                url: baseUrl+'transaction/purchase_requisition/process_po',
                success : function(resp){

                  if(resp.no_po == 'ERROR INSERT' || resp.no_po == false) {
                    alert('Data Tidak berhasil di Proses');
                    return false;

                  } else {
                    $.notify({
                          icon: "glyphicon glyphicon-save",
                          message: 'Data has been process : '+resp.no_po
                        },{
                          type: 'success',
                          onClosed: function(){ location.reload();}
                        });

                    setTimeout(function () {
                      window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
                    }, 2000);
                  }
                }
            });

          }
        }
      ],
    });
});

function show_detail(e){
  let noId      = $(e).data('id');
  let noPr      = $(e).data('pr');

  $.get({
    url: baseUrl + 'transaction/purchase_requisition/view_popup/'+noId,
    success: function(resp){
      BootstrapDialog.show({
        title: 'Nomor Purchase Requisition Approve # <strong> '+noPr+' </strong>', 
        nl2br: false, 
        message: resp,
        closable: true,
        size: 'size-full',
        buttons:[
          {
            label: 'Tutup',
            action: function(dia){ dia.close(); }
          }
        ]
      });
    },
    complete: function(){
      $('body').css('cursor','default');
    }
  });
  return false;
  
};
</script>