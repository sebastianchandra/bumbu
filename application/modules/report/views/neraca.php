<?php
function lastOfMonth($year, $month) {
  return date("d M Y", strtotime('-1 second', strtotime('+1 month',strtotime($month . '/01/' . $year. ' 00:00:00'))));
}
?>  
<style type="text/css">
  .bold{
    font-weight: bold;
  }
</style>

<?php 

?>
<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-sm-4">
        <h2>Neraca</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Report</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Neraca</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('report/finance/neraca',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Tahun</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>   
            <div class="row-form ">
              <div class="col-md-2">
                <p>Bulan</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    if($this->input->post('month')==$value['id']){
                      echo '<option value="'.$value['id'].'" selected>'.$value['nama_bulan'].'</option>';
                    }else{
                      echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>       
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('warehouse_id')!=''){ ?>
                <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a>
                <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <!-- <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div> -->
            <?php echo form_close(); ?>
            <?php
            if($this->input->post('year')!='' AND $this->input->post('month')!=''){

            $row1     = array();
            $row2     = array();
            $row3     = array();

            $row4     = array();
            $row5     = array();
            $row6     = array();

            // test($total_aktiva,0);
            foreach ($total_aktiva as $key => $val1) {
              $row1[$val1->coa_level][]=$val1;
            }

            foreach ($total_aktiva as $key => $val2) {
              $row2[$val2->coa_parent_id][]=$val2;
            }

            foreach ($total_pasiva as $key => $val3) {
              $row4[$val3->coa_level][]=$val3;
            }

            foreach ($total_pasiva as $key => $val4) {
              $row5[$val4->coa_parent_id][]=$val4;
            }
            // test($row2,0);

            ?>              
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable" style="margin-bottom: 0px !important;">
                <tr class="bold">
                  <td colspan="4" align="center">
                      LEVONDI SOUWMPIE <br/>
                      NERACA <br/>
                      <?php echo lastOfMonth($this->input->post('year'),$this->input->post('month')) ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 0px !important;" width="50%">
                    <table width="100%">
                      <?php 
                      // Start Lv 1
                      $taktiva1     = 0;
                      $taktiva2     = 0;
                      $taktiva3     = 0;
                      $taktiva4     = 0;
                      foreach ($row1[1] as $key => $val1) {
                      ?>
                      <tr>
                        <td width="69%"><strong><u><?php echo $val1->coa_name; ?></u></strong></td>
                        <td></td>
                      </tr>
                        <?php
                        // Start Lv 2
                        foreach ($row2[$val1->coa_id] as $key => $val2) { 
                        ?>
                        <tr>
                          <td><strong><u><?php echo $val2->coa_name; ?></u></strong></td>
                          <td></td>
                        </tr>
                          <?php
                          // Start Lv 3
                          foreach ($row2[$val2->coa_id] as $key => $val3) { 
                            $aktiva3      = $val3->debet-$val3->kredit;
                            $taktiva3     = $aktiva3+$taktiva3;
                            $taktiva1     = $aktiva3+$taktiva1;
                          ?>
                          <tr>
                            <td><?php echo $val3->coa_name; ?></td>
                            <td align="right"><?php echo ($aktiva3!=0)? number_format($aktiva3,2) : ''; ?></td>
                          </tr>
                          <?php 
                          // End Lv 3
                          }
                          ?>
                        <tr>
                          <td><strong>Total <?php echo $val2->coa_name; ?></strong></td>
                          <td style="border-top-width: 2px;border-top-color: black;" align="right"><?php echo number_format($taktiva3,2); ?></td>
                        </tr>
                        <?php
                        $taktiva3     = 0;
                        // End Lv 2
                        }
                        ?>
                      <tr>
                        <td><strong><u>Total <?php echo $val1->coa_name; ?></u></strong></td>
                        <td style="border-top-width: 2px;border-top-color: black;border-bottom: 3px double;" align="right"><?php echo number_format($taktiva1,2); ?></td>
                      </tr>
                      <?php
                      // End Lv 1
                      }
                      ?>
                    </table>
                  </td>
                  <td style="padding: 0px !important;" width="50%">
                    <table width="100%">
                      <?php 
                      // Start Lv 1
                      $tpasiva1     = 0;
                      $tpasiva2     = 0;
                      $tpasiva3     = 0;
                      $tpasiva4     = 0;
                      foreach ($row4[1] as $key => $val4) {
                      ?>
                      <tr>
                        <td width="69%"><strong><u><?php echo $val4->coa_name; ?></u></strong></td>
                        <td></td>
                      </tr>
                        <?php
                        // Start Lv 2
                        foreach ($row5[$val4->coa_id] as $key => $val5) { 
                          $pasiva2      = $val5->kredit-$val5->debet;
                          $tpasiva1     = $pasiva2+$tpasiva1;
                          $tpasiva3     = $pasiva2+$tpasiva3;
                        ?>
                        <tr>
                          <td><?php echo $val5->coa_name; ?></td>
                          <td align="right"><?php echo ($pasiva2!=0)? number_format($pasiva2,2) : ''; ?></td>
                        </tr>
                        <?php
                        $pasiva2     = 0;
                        // End Lv 2
                        }
                        ?>
                      <tr>
                        <td><strong><u>Total <?php echo $val4->coa_name; ?></u></strong></td>
                        <td style="border-top-width: 2px;border-top-color: black;" align="right"><?php echo number_format($tpasiva1,2); ?></td>
                      </tr>
                      <?php
                      // End Lv 1
                      }
                      ?>
                      <tr>
                        <td><strong><u>Total Utang Dan Equitas</u></strong></td>
                        <td style="border-top-width: 2px;border-top-color: black;border-bottom: 3px double;" align="right">
                          <div style=""><?php echo number_format($tpasiva3,2); ?></div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <tr class="bold">
                  <td colspan="4" align="center">
                  </td>
                </tr>
              </table>
            </div>
            <?php 
            }
            ?>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#year").select2();
$("#month").select2();

$(document).ready(function(){
  // $('#vtable').DataTable({
  //   aaSorting: [],
  //   "searching": false, "paging": false, "ordering": false,"bInfo" : false,
  //   pageLength: 25,
  //   responsive: false,
  //   dom: '<"html5buttons"B>lTfgitp',
  //   buttons: 
  //   [
  //     // {extend: 'copy'},
  //     // {extend: 'csv'},
  //     {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Stok'}},
  //     // {extend: 'pdf', title: 'ExampleFile'},
  //     // {extend: 'print',
  //     //   customize: function (win){
  //     //     $(win.document.body).addClass('white-bg');
  //     //     $(win.document.body).css('font-size', '10px');

  //     //     $(win.document.body).find('table')
  //     //       .addClass('compact')
  //     //       .css('font-size', 'inherit');
  //     //   }
  //     // }
  //   ]
  // }); 

  $('#save').click(function(){
    if(!$('#year').val()){
      toastr.error("<strong>Tahun</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
    if(!$('#month').val()){
      toastr.error("<strong>Bulan</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
  });
});
</script>
