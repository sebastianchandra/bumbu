<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Report</li>
                  <li class="breadcrumb-item">Stock</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >History Stock</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <?php echo form_open('report/stok/history_stok',array('class' => 'container-fluid d-print-none')); ?>
            <div class="form-group row ">
              <div class="col-md-2">
                <p>Nama Gudang</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='warehouse_id' name="warehouse_id">
                  <!-- <option value="All"> Semua </option> -->
                  <?php 
                  foreach ($data_warehouse as $key => $value) {

                    echo '<option value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>   
            <div class="form-group row ">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input placeholder="Dari" name="tgl_dari" value="<?php echo ($this->input->post('tgl_dari')!='')? $this->input->post('tgl_dari') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
              <div class="col-md-3">
                <input placeholder="Sampai" name="tgl_sampai" value="<?php echo ($this->input->post('tgl_sampai')!='')? $this->input->post('tgl_sampai') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
            </div>         
            <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('warehouse_id')!=''){ ?>
                <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a>
                <a target="_blank" href="<?php echo base_url('report/stok/print_pdf/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">PDF</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <?php echo form_close(); ?>   
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="12%">Tanggal</th>
                    <th>No. Transaksi</th>
                    <th>Keterangan</th>
                    <!-- <th>Transaksi</th> -->
                    <th width="10%">Harga</th>
                    <th width="10%">Saldo Awal</th>
                    <th width="10%">Barang Masuk</th>
                    <th width="10%">Barang Keluar</th>
                    <th width="10%">Jumlah Stok</th>
                  </tr>
                </thead>                
                <!-- <tbody> -->
                  <?php 
                  foreach ($data_stok_current as $key => $value) {
                  ?>
                  <tr>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"><strong><?php echo $value->items_name.' ('.$value->items_code; ?>)</strong></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"></td>
                    <!-- <td colspan="8" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);"><strong><?php echo $value->items_name.' ('.$value->items_code; ?>)</strong></td> -->
                  </tr>
                <tbody>
                  <?php
                  $query  = $this->db->query("SELECT a.trn_date,a.items_id, b.items_name,a.doc_no,a.warehouse_id,a.activity,a.qty,a.current_stock,
                    CASE
                       WHEN SUBSTR(a.doc_no,1,3) = 'SAT' THEN 'Saldo Awal'
                       WHEN SUBSTR(a.doc_no,1,3) = 'ISS' THEN (SELECT doc_ref FROM db_bumbu_transaction.trn_incoming_stock_01 WHERE is_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'IST' THEN (SELECT IF(a1.project_id=0,b1.warehouse_name,c1.project_name) keterangan FROM db_bumbu_transaction.trn_incoming_stock_01 a1 LEFT JOIN db_bumbu_master.mst_warehouse b1 ON a1.warehouse_id=b1.warehouse_id LEFT JOIN db_bumbu_master.mst_project c1 ON a1.project_id=c1.project_id WHERE a1.is_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'ISR' THEN 'Reject Incoming'
                       WHEN SUBSTR(a.doc_no,1,3) = 'OSS' THEN (SELECT IF(a2.project_id=0,b2.warehouse_name,c2.project_name) keterangan FROM db_bumbu_transaction.trn_outgoing_stock_01 a2 LEFT JOIN db_bumbu_master.mst_warehouse b2 ON a2.dest_wh_id=b2.warehouse_id LEFT JOIN db_bumbu_master.mst_project c2 ON a2.project_id=c2.project_id WHERE a2.os_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'OST' THEN (SELECT remarks FROM db_bumbu_transaction.trn_outgoing_stock_01 a2 WHERE a2.os_no=a.doc_no)
                       WHEN SUBSTR(a.doc_no,1,3) = 'OSR' THEN 'Reject Outgoing'
                       WHEN SUBSTR(a.doc_no,1,3) = 'OSJ' THEN 'Adjustment Stock'
                       WHEN SUBSTR(a.doc_no,1,3) = 'UVS' THEN 'Konversi Barang'
                       WHEN SUBSTR(a.doc_no,1,3) = 'UVR' THEN 'Reject Konversi Barang'
                       WHEN SUBSTR(a.doc_no,1,3) = 'DOR' THEN 'Delivery Order Reject'
                       WHEN SUBSTR(a.doc_no,1,2) = 'DO' THEN 'Delivery Order'
                       ELSE ' '
                                        END AS keterangan,c.items_price
                    FROM db_bumbu_transaction.trn_stock_hist a
                    LEFT JOIN db_bumbu_transaction.trn_stock_by_doc c ON a.items_id=c.items_id AND a.warehouse_id=c.warehouse_id AND a.doc_no=c.doc_no, db_bumbu_master.mst_items b
                    WHERE a.items_id=b.items_id AND a.trn_date BETWEEN '".$this->input->post('tgl_dari')."' AND '".$this->input->post('tgl_sampai')."' 
                    AND a.current_stock>=0 AND a.warehouse_id='".$this->input->post('warehouse_id')."' AND a.items_id='".$value->items_id."' ORDER BY a.stock_hist_id ASC")->result();
                  foreach ($query as $key => $value2) {
                    $saldo_awal     = 0;
                    if($value2->activity=='Saldo Awal' OR $value2->activity=='In_Po' OR $value2->activity=='In_Adjustment' OR $value2->activity=='Rj_OutTrans' OR $value2->activity=='In_Trans' OR $value2->activity=='In_Others' OR $value2->activity=='In_Conv' OR $value2->activity=='Rj_ConvOut' OR $value2->activity=='in'){
                      $saldo_awal   = $value2->current_stock-$value2->qty;
                    }elseif($value2->activity=='Rj_InTrans' OR $value2->activity=='Out_Tr' OR $value2->activity=='Out_tr' OR $value2->activity=='Out_Oth' OR $value2->activity=='Out_Adj' OR $value2->activity=='Out_Conv' OR $value2->activity=='Rj_ConvIn' OR $value2->activity=='out'){
                      $saldo_awal   = $value2->current_stock+$value2->qty;
                    }
                  ?>
                  <tr>
                    <td><?php echo tgl_singkat($value2->trn_date); ?></td>
                    <td><?php echo $value2->doc_no; ?></td>
                    <td><?php echo $value2->keterangan; ?></td>
                    <td align="right"><?php echo $value2->items_price; ?></td>
                    <!-- <td><?php echo $value2->activity; ?></td> -->
                    <td align="right"><?php echo number_format($saldo_awal,2); ?></td>
                    <td align="right"><?php if($value2->activity=='Saldo Awal' 
                                            OR $value2->activity=='in'
                                            OR $value2->activity=='In_Po' 
                                            OR $value2->activity=='In_Adjustment'
                                            OR $value2->activity=='Rj_OutTrans' 
                                            OR $value2->activity=='In_Trans' 
                                            OR $value2->activity=='In_Others' 
                                            OR $value2->activity=='In_Conv' 
                                            OR $value2->activity=='Rj_ConvOut'
                                            OR $value2->activity=='Rj_do'){ echo '(+) '.number_format($value2->qty,2); } ?>
                    </td>
                    <td align="right"><?php if($value2->activity=='Rj_InTrans' 
                                            OR $value2->activity=='out'
                                            OR $value2->activity=='Out_Tr' 
                                            OR $value2->activity=='Out_tr' 
                                            OR $value2->activity=='Out_Oth' 
                                            OR $value2->activity=='Out_Adj' 
                                            OR $value2->activity=='Out_Conv' 
                                            OR $value2->activity=='Rj_ConvIn'
                                            OR $value2->activity=='DO_Trans' ){ echo '(-) '.number_format($value2->qty,2); } ?></td>
                    <td><?php echo $value2->current_stock; ?></td>
                  </tr>
                </tbody>
                  <?php
                  }
                  ?>
                  <tr>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"></td>
                    <td align="right" style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"><strong>Total Barang (<?php //echo $value->items_name; ?>)</strong></td>
                    <td style="background-color: rgb(230, 230, 230);border: 1px solid hsl(0, 0%, 91%);border-bottom: 1px solid hsl(0, 0%, 0%);"><strong><?php echo number_format($value->current_stock,2); ?></strong></td>
                  </tr>
                  <?php
                  }
                  ?>
              </table>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#warehouse_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": true, "paging": false, "ordering": false,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Stok'}},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

  $('#save').click(function(){
    if(!$('#warehouse_id').val()){
      toastr.error("<strong>Gudang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#warehouse_id').select2('open');
      return false;
    }
  });
});
</script>
