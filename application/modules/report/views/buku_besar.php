<?php 
// $a=array('a','b');
// if(isset($a[1])) echo $a[1]; 

// test('1',1);
?>


<?php
function lastOfMonth($year, $month) {
  return date("d M Y", strtotime('-1 second', strtotime('+1 month',strtotime($month . '/01/' . $year. ' 00:00:00'))));
}
?>  
<style type="text/css">
  .bold{
    font-weight: bold;
  }
</style>
<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-sm-4">
        <h2>Buku Besar</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Report</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Buku Besar</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('report/finance/buku_besar',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Tahun</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>   
            <div class="row-form ">
              <div class="col-md-2">
                <p>Bulan</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    if($this->input->post('month')==$value['id']){
                      echo '<option value="'.$value['id'].'" selected>'.$value['nama_bulan'].'</option>';
                    }else{
                      echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>       
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('warehouse_id')!=''){ ?>
                <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a>
                <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <?php echo form_close(); ?>
            <?php
            if($this->input->post('year')!='' AND $this->input->post('month')!=''){
            $row1       = array();
            $tahun      = $this->input->post('year');
            $bulan      = $this->input->post('month');

            // foreach ($journal_history as $key => $val1) {
            //   $row1[$val1->coa_id][]    = $val1;
            // }
            ?>              
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <tr>
                  <td colspan="7" align="center">
                      LEVONDI SOUWMPIE <br/>
                      BUKU BESAR <br/>
                      <?php echo lastOfMonth($this->input->post('year'),$this->input->post('month')) ?>
                  </td>
                </tr>
                <?php 
                foreach ($coa_history as $key => $value) {
                if(substr($value->coa_detail,0,2)=='01'){
                  $saldo    = $value->debet - $value->kredit;
                }else{
                  $saldo    = $value->kredit - $value->debet;
                }

                ?>
                <tr class="bold">
                  <td colspan="7"><?php echo $value->coa_name.' ('.$value->coa_detail.')'; ?></td>
                </tr>
                <tr class="bold">
                  <td width="11%">Tanggal</td>
                  <td width="11%">Referensi</td>
                  <td width="11%">No Voucer</td>
                  <td>Keterangan</td>
                  <td width="13%">Debet</td>
                  <td width="13%">Kredit</td>
                  <td width="13%">Saldo</td>
                </tr>

                <tr>
                  <td></td>
                  <td><?php //echo $value->coa_detail; ?></td>
                  <td><?php //echo $value->coa_detail; ?></td>
                  <td>Saldo Awal</td>
                  <td align="right"><?php echo number_format($value->debet); ?></td>
                  <td align="right"><?php echo number_format($value->kredit); ?></td>
                  <td align="right"><?php echo number_format($saldo); ?></td>
                </tr>
                <?php 
                $saldo_hist         = 0;
                $saldo_hist         = $saldo;
                $total_debet        = 0;
                $total_kredit       = 0;
                $saldo_total        = 0;
                // test($saldo_hist,0);
                $history_journal    = $this->finance_model->journal_history($tahun,$bulan,$value->coa_id)->result();
                foreach ($history_journal as $key => $val1) {
                $coa_detail     = substr($val1->coa_detail,0,2);
                if($coa_detail=='01'){
                  $saldo_hist       = ($saldo_hist+(float)$val1->debet)-(float)$val1->kredit;                  
                }else{
                  $saldo_hist       = ($saldo_hist+(float)$val1->kredit)-(float)$val1->debet;                  
                }

                $total_debet      = $total_debet+(float)$val1->debet;
                $total_kredit     = $total_kredit+(float)$val1->kredit;
                // test($coa_detail,1);
                // $saldo_hist     = abs($saldo)
                ?>
                <tr>
                  <td><?php echo tgl_singkat($val1->tgl); ?></td>
                  <td><?php echo ($val1->journal_type=='1')? $val1->trn_id : ''; ?></td>
                  <td><?php echo ($val1->journal_type!='1')? $val1->trn_id : ''; ?></td>
                  <td><?php echo ($val1->debet!='')? $val1->info_debet : $val1->info_kredit; ?></td>
                  <td align="right"><?php echo ($val1->debet!='')? number_format($val1->debet) : ''; ?></td>
                  <td align="right"><?php echo ($val1->kredit!='')? number_format($val1->kredit) : ''; ?></td>
                  <td align="right"><?php echo number_format(abs($saldo_hist)); ?></td>
                </tr>
                <?php 
                }
                if(substr($value->coa_detail,0,2)=='01'){
                  $total_debet      = $total_debet+($value->debet - $value->kredit);
                  $total_kredit     = $total_kredit;
                }else{
                  $total_debet      = $total_debet;
                  $total_kredit     = $total_kredit+($value->kredit - $value->debet);
                }
                $saldo_total      = $total_debet+$total_kredit;
                ?>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td align="right"><strong><?php echo number_format($total_debet); ?></strong></td>
                  <td align="right"><strong><?php echo number_format($total_kredit); ?></strong></td>
                  <td align="right"><strong><?php echo number_format(abs($saldo_total)); ?></strong></td>
                </tr>
                <tr>
                  <td colspan="7"></td>
                </tr>
                <?php 
                }
                ?>
              </table>
            </div>
            <?php 
            }
            ?>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#year").select2();
$("#month").select2();

$(document).ready(function(){
  // $('#vtable').DataTable({
  //   aaSorting: [],
  //   "searching": false, "paging": false, "ordering": false,"bInfo" : false,
  //   pageLength: 25,
  //   responsive: false,
  //   dom: '<"html5buttons"B>lTfgitp',
  //   buttons: 
  //   [
  //     // {extend: 'copy'},
  //     // {extend: 'csv'},
  //     {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Stok'}},
  //     // {extend: 'pdf', title: 'ExampleFile'},
  //     // {extend: 'print',
  //     //   customize: function (win){
  //     //     $(win.document.body).addClass('white-bg');
  //     //     $(win.document.body).css('font-size', '10px');

  //     //     $(win.document.body).find('table')
  //     //       .addClass('compact')
  //     //       .css('font-size', 'inherit');
  //     //   }
  //     // }
  //   ]
  // }); 

  $('#save').click(function(){
    if(!$('#year').val()){
      toastr.error("<strong>Tahun</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
    if(!$('#month').val()){
      toastr.error("<strong>Bulan</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      // $('#warehouse_id').select2('open');
      return false;
    }
  });
});
</script>
