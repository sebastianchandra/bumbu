<div class="app-title">
  <div>
    <h1>Report History Purchase Order</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Report</li>
      <li class="breadcrumb-item active">History Purchase Order</li>
    </ul></p>
  </div>
</div>

<div class="d-print-none">
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <?php echo form_open('report/purchase_order/history',array('class' => 'form-horizontal')); ?>
          <div class="tile-body">
            <!-- <div class="form-group row">
              <label class="control-label col-md-2">Month</label>
              <div class="col-md-5">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Year</label>
              <div class="col-md-2">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    echo '<option value="'.$year.'">'.$year.'</option>';
                  }
                  ?>
                </select>
              </div>
            </div> -->
            <div class="form-group row">
              <label class="control-label col-md-2">Number Purchase Order</label>
              <div class="col-md-6">
                <input class="form-control col-md-8" type="text" id="no_po" name="no_po">
              </div>
            </div>
          </div>
          <div class="tile-footer">
            <div class="row">
              <div class="col-md-8 col-md-offset-3">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-search"></i></button>&nbsp;&nbsp;&nbsp;
                <!-- <a class="btn btn-success" href="<?php echo base_url(); ?>master/items"><i class="fa fa-print"></i></a> -->
              </div>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<?php 
  if(!empty($this->input->post('no_po'))){
  $res_hist = array();
  foreach($data_is_detail as $key=>$val){ 
    $res_hist[$val->is_id][]=$val; 
  }
?>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <?php 
          //test($data_po[0],1);
          //foreach ($data_po as $key => $value) {
          ?>
          <table class="table table-print">
            <tr>
              <td width="175px"><strong>Purchase Order Number</strong></td>
              <td width="399px">: <?php echo $data_po[0]->po_no; ?></td>
              <td width="175px"><strong>Company</strong></td>
              <td>: <?php echo $data_po[0]->company_name; ?></td>
            </tr>
            <tr>
              <td><strong>Purchase Order Date</strong></td>
              <td>: <?php echo $data_po[0]->po_date; ?></td>
              <td><strong>Project Name</strong></td>
              <td>: <?php echo $data_po[0]->project_name; ?></td>
            </tr>
            <tr>
              <td><strong>Supplier</strong></td>
              <td>: <?php echo $data_po[0]->supplier_name; ?></td>
              <td><strong>Status</strong></td>
              <td>: <?php echo $data_po[0]->po_status; ?></td>
            </tr>
          </table>
          <table class="table table-print">
            <tr>
              <th><strong>Items Name</strong></th>
              <th width="20%"><strong>Items Code</strong></th>
              <th width="15%"><strong>Purchase Order Qty</strong></th>
              <th width="15%"><strong>Incoming Stock Qty</strong></th>
            </tr>
            <?php 
            $current = 0;
            foreach ($data_po as $key => $detail) {
              ?>
                <tr>
                  <td><?php echo $detail->items_name; ?></td>
                  <td><?php echo $detail->items_code; ?></td>
                  <td align="right"><?php echo $detail->qty; ?></td>
                  <td align="right"><?php echo $detail->qty_is; ?></td>
                </tr>
              <?php
            }
            ?>
          </table>
          <?php
          //}
          ?>
          <?php
          //Columns must be a factor of 12 (1,2,3,4,6,12)
          $numOfCols = 2;
          $rowCount = 0;
          $bootstrapColWidth = 12 / $numOfCols;
          ?>
          <div class="row">
          <?php
          foreach ($data_hist as $row){
          ?>  
            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                <table class="table-print-header table-print-header" width="100%">
                  <tr>
                    <td width="15%"><strong>IS Number</strong></td>
                    <td width="20%">: <?php echo $row->is_no; ?></td>
                    <td width="15%"><strong>Company</strong></td>
                    <td>: <?php echo $row->company_name; ?></td>
                  </tr>
                  <tr>
                    <td><strong>IS Date</strong></td>
                    <td>: <?php echo $row->is_date; ?></td>
                    <td><strong>Warehouse</strong></td>
                    <td>: <?php echo $row->warehouse_name; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Sender</strong></td>
                    <td>: <?php echo $row->sender_pic; ?></td>
                    <td><strong>Receiver</strong></td>
                    <td>: <?php echo $row->receiver_pic; ?></td>
                  </tr>
                  <tr>
                    <td><strong>Status</strong></td>
                    <td>: <?php echo $row->sender_pic; ?></td>
                    <td><strong>Remarks</strong></td>
                    <td>: <?php echo $row->receiver_pic; ?></td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <table class="table-print-detail" width="100%">
                        <thead>
                          <tr>
                            <th width="20%"><strong>Items Code</strong></th>
                            <th><strong>Items name</strong></th>
                            <th width="20%"><strong>Qty</strong></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          foreach ($res_hist[$row->is_id] as $key => $value) {
                          ?>
                          <tr>
                            <td><?php echo $value->items_code; ?></td>
                            <td><?php echo $value->items_name; ?></td>
                            <td align="right"><?php echo $value->qty; ?></td>
                          </tr>
                          <?php 
                          }
                          ?>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </table>
            </div>
          <?php
              $rowCount++;
              if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
          }
          ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php 
  }
?>

<script type="text/javascript">
  $("#warehouse_id").select2({
    allowClear: true});

  $("#year").select2({
    allowClear: true
  });

  $("#month").select2({
    allowClear: true
  });

</script>