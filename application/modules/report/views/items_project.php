<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Items Project</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Report</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Items Project</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('report/stok/items_project',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Nama Project</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='project_id' name="project_id">
                  <!-- <option value="All"> Semua </option> -->
                  <?php 
                  foreach ($data_project as $key => $value) {

                    echo '<option value="'.$value->project_id.'">'.$value->project_name.'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>   
            <!-- <div class="row-form ">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input placeholder="Dari" name="tgl_dari" value="<?php echo ($this->input->post('datepicker_from')!='')? $this->input->post('datepicker_from') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
              <div class="col-md-3">
                <input placeholder="Sampai" name="tgl_sampai" value="<?php echo ($this->input->post('datepicker_from')!='')? $this->input->post('datepicker_from') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
            </div>  -->        
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('project_id')!=''){ ?>
                <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <?php echo form_close(); ?>                 
            <div>
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="9%">Tanggal</th>
                    <th>Nama Barang</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th width="10%">Harga</th>
                    <th width="5%">Disc</th>
                    <th width="10%">Jumlah</th>
                    <th width="13%">Supplier</th>
                  </tr>
                </thead>
                <!-- <tbody> -->
                  <?php 
                  $tjumlah      = 0;
                  $jumlah       = 0;
                  foreach ($data_items_project as $key => $value) {
                    $jumlah     = $value->qty*$value->price;
                    $tjumlah    = $tjumlah+$jumlah;
                  ?>
                  <tr>
                    <td><?php echo tgl_singkat($value->is_date); ?></td>
                    <td><?php echo $value->items_name; ?></td>
                    <td align="right"><?php echo number_format($value->qty,2); ?></td>
                    <td><?php echo $value->items_unit_name; ?></td>
                    <td align="right"><?php echo number_format($value->price,2); ?></td>
                    <td align="right"><?php echo number_format($value->discount,2); ?></td>
                    <td align="right"><?php echo number_format($jumlah,2); ?></td>
                    <td><?php echo $value->supplier_name; ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                <thead>
                  <tr>
                    <th colspan="6" align="right">Total Project <?php echo $value->project_name; ?> : </th>
                    <th align="right"><?php echo number_format($tjumlah,2); ?></th>
                    <th></th>
                  </tr>
                </thead>
                <!-- </tbody> -->
              </table>
            </div>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#project_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": true, "paging": false, "ordering": true,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Items Project', exportOptions:{messageTop: 'Items Project'}},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }le: 'ExampleFile'},
      // 
    ]
  }); 

  $('#save').click(function(){
    if(!$('#project_id').val()){
      toastr.error("<strong>Gudang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#project_id').select2('open');
      return false;
    }
  });
});
</script>
