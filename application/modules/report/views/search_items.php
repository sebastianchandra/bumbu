<div class="app-title">
  <div>
    <h1>Report Items & Supplier</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Report</li>
      <li class="breadcrumb-item active">Items & Supplier</li>
    </ul></p>
  </div>
  <!-- <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a class="btn btn-secondary" href="<?php echo base_url(); ?>master/items">Back</a></li>
  </ul> -->
</div>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="tile-body">
        <?php echo form_open('items_supplier/search_items',array('class' => 'form-horizontal')); ?>
          <div class="form-group row">
            <label class="control-label col-md-2">Price</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="Price" id="price1" value="0">
            </div>
            <div class="col-md-1" style="align-self: center;padding-left: 35px;">Until</div>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="Price" id="price2" value="0">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Items Name</label>
            <div class="col-md-10">
              <input class="form-control col-md-3" type="text" placeholder="Items Name" id="name">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Items Supplier</label>
            <div class="col-md-10">
              <input class="form-control col-md-3" type="text" placeholder="Items Supplier" id="name">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">City</label>
            <div class="col-md-10">
              <input class="form-control col-md-3" type="text" placeholder="City" id="name">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-md-2">Group Code</label>
            <div class="col-md-2">
              <select class="form-control" id='items_kind'>
                <option value=""> - </option>
                <?php 
                foreach ($data_items_kind as $key => $value) {
                  echo '<option data-kind="'.$value->items_kind.'" value="'.$value->items_kind_name.'">'.$value->items_kind_name.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
      <div class="tile-footer">
        <div class="row">
          <div class="col-md-8 col-md-offset-3">
            <button class="btn btn-primary" type="button" id="save"><i class="fa fa-search"></i> Search</button>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>master/items"></i><i class="fa fa-reply"></i> Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#price1").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 1, 'digitsOptional': false, 'placeholder': '0'});
  $("#price2").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 1, 'digitsOptional': false, 'placeholder': '0'});

  $("#items_kind").select2({
    placeholder: 'Select a Group Code',
    allowClear: true});

  $("#items_group").select2({
    placeholder: 'Select a Group Items',
    allowClear: true
  });

  $("#unit").select2({
    placeholder: 'Select a Unit',
    allowClear: true
  });

  $('#save').click(
    function(e){
      e.preventDefault();

      if(!$('#items_kind').val()){
        $.notify({
          title: "Erorr : ",
          message: "Group Code Tidak Boleh Kosong",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        $('#items_kind').select2('open');
        return false;
      }

      if(!$('#name').val()){
        $.notify({
          title: "Erorr : ",
          message: "Items Name Tidak Boleh Kosong",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        $("#name").focus();
        return false;
      }

      if(!$('#unit').val()){
        $.notify({
          title: "Erorr : ",
          message: "Items Unit Tidak Boleh Kosong",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        $('#unit').select2('open');
        return false;
      }

      if(!$('#items_group').val()){
        $.notify({
          title: "Erorr : ",
          message: "Items Group Tidak Boleh Kosong",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        $('#items_group').select2('open');
        return false;
      }

      if(!$('#info').val()){
        $.notify({
          title: "Erorr : ",
          message: "Items Info Tidak Boleh Kosong",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        $('#info').focus();
        return false;
      }

      if(!$('#category').val()){
        $.notify({
          title: "Erorr : ",
          message: "Category Items Tidak Boleh Kosong",
          icon: 'fa fa-times' 
        },{
          type: "danger",
          delay: 1000
        });
        $('#category').focus();
        return false;
      }

      $.ajax({
        url: baseUrl+'master/items/form_act',
        type : "POST",  
        data: {
          code    : $('#items_kind').val(),
          kind    : $('#items_kind option:selected').attr('data-kind'),
          name    : $('#name').val(),
          unit    : $('#unit').val(),
          group   : $('#items_group').val(),
          info    : $('#info').val(),
          category: $('#category').val()
        },
        success : function(resp){
          if(resp.status == 'ERROR INSERT' || resp.status == false) {
            $.notify({
              message: 'Data Gagal disimpan'
            },{
              type: 'danger'
            });
            return false;

          } else {
            $.notify({
              message: 'Data sudah berhasil disimpan'
            },{
              type: 'info'
            });

            setTimeout(function () {
            window.location.href = baseUrl+'master/items/'; //will redirect to google.
          }, 2000);
          }
        }
      });

    }
    );
  </script>