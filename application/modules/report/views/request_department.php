<div class="app-title">
  <div>
    <h1>Request Department</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Report</li>
      <li class="breadcrumb-item active">Request Department</li>
    </ul></p>
  </div>
</div>
<?php
// test($user_group,1);
?>
<div class="d-print-none">
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <?php echo form_open('report/request',array('class' => 'form-horizontal')); ?>
          <div class="tile-body">
            <div class="form-group row">
              <label class="control-label col-md-2">Year</label>
              <div class="col-md-2">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Department</label>
              <div class="col-md-2">
                <select class="form-control" id='dept' name="dept">
                  <option value=""> - </option>
                  <?php 
                  foreach ($user_group as $key => $value) {
                    if($this->input->post('dept')==$value->id_user_group){
                      echo '<option value="'.$value->id_user_group.'" selected>'.$value->name.'</option>';
                    }else{
                      echo '<option value="'.$value->id_user_group.'">'.$value->name.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="tile-footer">
            <div class="row">
              <div class="col-md-8 col-md-offset-3">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-search"></i></button>&nbsp;&nbsp;&nbsp;
                <?php 
                if(!empty($this->input->post('year')) or !empty($this->input->post('dept'))){
                ?>
                <a class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i></a>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<?php 
if(!empty($this->input->post('year')) or !empty($this->input->post('dept'))){
?>
<div class="row">
  <div class="col-md-12">
    <div class="tile">
      <div class="table-responsive">
        <table class="table table-hover table-bordered" id="prTable">
          <thead>
            <tr>
              <th>No</th>
              <th>Date</th>
              <th>Company Name</th>
              <th>Department</th>
              <th>requester</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $no = 0;
            foreach ($data_request as $key => $value) {
              $no = $no+1;
            ?>
            <tr>
              <td><strong><a href="#" id='detail' onclick="return show_detail(this)" 
              data-id="<?php echo $value->request_id; ?>" 
              data-rq="<?php echo $value->request_no; ?>"><?php echo $value->request_no; ?></a></strong></td>
              <td><?php echo tgl_singkat($value->request_date); ?></td>
              <td><?php echo $value->company_name; ?></td>
              <td><?php echo $value->name_dept; ?></td>
              <td><?php echo $value->requester; ?></td>
              <td><?php if($value->status_approve==0){ echo 'New';}elseif($value->status_approve==1){ echo 'Approved';}elseif($value->status_approve==2){ echo 'Not Approved';} ?></td>
            </tr>
            <?php 
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php 
}
?>

<script type="text/javascript">
$("#year").select2();
$("#dept").select2();

$('#prTable').DataTable({
    "paging": true, 
    "bLengthChange": false, // disable show entries dan page
    "bFilter": true,
    "bInfo": true, // disable Showing 0 to 0 of 0 entries
    "bAutoWidth": false,
    "language": {
        "emptyTable": "Tidak Ada Data"
    },
    "aaSorting": [],
    'dom': 'Bfrtip',
        buttons: [
            'print'
        ]
});

$('#prTable').on('click','#process', function (e) {
  var idPr     = $(this).data('id');
  var noPr     = $(this).data('pr');

  BootstrapDialog.show({
      title: 'Process Purchase Requisition ',
      type : BootstrapDialog.TYPE_INFO,
      message: 'Do you want process purchase requisition number <strong>'+noPr+'</storng> ?',
      closable: false,
      buttons: [
        {
          label: '<i class="fa fa-reply"></i> Cancel', cssClass: 'btn',
          action: function(dia){
            dia.close();
          }
        },
        {
          label: '<i class="fa fa-check"></i> Process', cssClass: 'btn-info', id: 'update_sales', //hotkey: 'alt'+'s',
          // icon: 'glyphicon glyphicon-check',
          action: function(dia){
            dia.close();
            $.ajax({
                data: {
                    id_pr : idPr,
                    no_pr : noPr
                },
                type : "POST",
                url: baseUrl+'transaction/purchase_requisition/process_po',
                success : function(resp){

                  if(resp.no_po == 'ERROR INSERT' || resp.no_po == false) {
                    alert('Data Tidak berhasil di Proses');
                    return false;

                  } else {
                    $.notify({
                          icon: "glyphicon glyphicon-save",
                          message: 'Data successfully saved with document number : '+resp.no_po
                        },{
                          type: 'success',
                          onClosed: function(){ location.reload();}
                        });

                    setTimeout(function () {
                      window.location.href = baseUrl+'transaction/purchase_requisition'; //will redirect to google.
                    }, 2000);
                  }
                }
            });

          }
        }
      ],
    });
});

function show_detail(e){
  let noId      = $(e).data('id');
  let noRq      = $(e).data('rq');

  $.get({
    url: baseUrl + 'transaction/request/view_popup/'+noId,
    success: function(resp){
      BootstrapDialog.show({
        title: 'Nomor Request # <strong> '+noRq+' </strong>', 
        nl2br: false, 
        message: resp,
        closable: true,
        size: 'size-full',
        buttons:[
          {
            label: 'Tutup',
            action: function(dia){ dia.close(); }
          }
        ]
      });
    },
    complete: function(){
      $('body').css('cursor','default');
    }
  });
  return false;
  
};
</script>