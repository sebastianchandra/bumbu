<?php
if($status=='excell'){
  $file = "current_stock.xls";
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment; filename='.$file);
}
?>
<style>
table, td, th {
  border: 1px solid black;
}

table {
  width: 100%;
  border-collapse: collapse;
}
</style>
    
<table class="table table-bordered table-hover dataTables-example" id="vtable">
  <thead>
    <tr>
      <th>Kode Barang</th>
      <th>Nama Barang</th>
      <th>Nama Gudang</th>
      <!-- <th>Items In</th>
      <th>Items Out</th>
      <th>Adj In</th>
      <th>Adj Out</th> -->
      <th>Current Stock</th>
      <th width="5%">Satuan</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    foreach ($data_stok_current as $key => $value) {
    ?>
    <tr>
      <td><?php echo $value->items_code; ?></td>
      <td><?php echo $value->items_name; ?></td>
      <td><?php echo $value->warehouse_name; ?></td>
      <!-- <td><?php echo $value->items_in; ?></td>
      <td><?php echo $value->items_out; ?></td>
      <td><?php echo $value->adj_in; ?></td>
      <td><?php echo $value->adj_out; ?></td> -->
      <td><?php echo $value->current_stock; ?></td>
      <td><?php echo $value->items_unit_name; ?></td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>

<script>window.print(); setTimeout(function(){window.close();},500);</script>