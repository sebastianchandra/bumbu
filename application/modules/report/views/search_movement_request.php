<div class="app-title">
  <div>
    <h1>Movement Request</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Report</li>
      <li class="breadcrumb-item active">Movement Request</li>
    </ul></p>
  </div>
</div>

<div class="d-print-none">
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <?php echo form_open('report/request/movement_request',array('class' => 'form-horizontal')); ?>
          <div class="tile-body">
            <div class="form-group row">
              <label class="control-label col-md-2">Year</label>
              <div class="col-md-2">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Document Number</label>
              <div class="col-md-8">
                <input class="form-control" name="document" type="text" placeholder="Input Document Number Request / Purchase Request / Purchase Order" id="document" value="<?php echo $this->input->post('document'); ?>">
              </div>
            </div>
          </div>
          <div class="tile-footer">
            <div class="row">
              <div class="col-md-8 col-md-offset-3">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-search"></i></button>&nbsp;&nbsp;&nbsp;
                <?php 
                if(!empty($this->input->post('warehouse_id')) or !empty($this->input->post('year')) or !empty($this->input->post('month'))){
                ?>
                <a class="btn btn-success" onclick="window.print()"><i class="fa fa-print"></i></a>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<?php 
  if(!empty($this->input->post('year')) or !empty($this->input->post('document'))){
?>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <table>
            <tr></tr>
          </table>
          <?php 
          //test($data_movement,0);
          ?>
          <table class="table table-print">
            <tr style="font-size: 11px;">
              <th><strong>Company Name</strong></th>
              <th><strong>Req No</strong></th>
              <th><strong>Req Date</strong></th>
              <th><strong>Req status</strong></th>
              <th><strong>Project</strong></th>
              <th><strong>PR No</strong></th>
              <th><strong>PR Date</strong></th>
              <th><strong>PR Status</strong></th>
              <th><strong>Supplier</strong></th>
              <th><strong>PO No</strong></th>
              <th><strong>PO Date</strong></th>
              <th><strong>PO Status</strong></th>
            </tr>
            <?php 
            foreach ($data_movement as $key => $detail) {
            ?>
              <tr style="font-size: 10px;">
                <td><?php echo $detail->company_name; ?></td>
                <td><?php echo $detail->request_no; ?></td>
                <td><?php echo ($detail->request_date!='') ? tgl_singkat($detail->request_date) : ''; ?></td>
                <td><?php echo $detail->req_status; ?></td>
                <td><?php echo $detail->project_name; ?></td>
                <td><?php echo $detail->pr_no; ?></td>
                <td><?php echo ($detail->pr_date!='') ? tgl_singkat($detail->pr_date) : ''; ?></td>
                <td><?php if($detail->pr_notapprove>0){ echo 'Not Approved'; } elseif($detail->pr_approve>0){ echo 'Approved'; } else { echo $detail->pr_status; } ?></td>
                <td><?php echo $detail->supplier_name; ?></td>
                <td><?php echo $detail->po_no; ?></td>
                <td><?php echo ($detail->po_date!='') ? tgl_singkat($detail->po_date) : ''; ?></td>
                <td><?php echo $detail->po_status; ?></td>
              </tr>
            <?php
            }
            ?>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php 
}
?>

<script type="text/javascript">
  $("#year").select2();

  

</script>