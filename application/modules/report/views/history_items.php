<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>History Items</h2>
        <ol class="breadcrumb d-print-none">
            <li class="breadcrumb-item">
                <a href="index.html">Report</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>History Items</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action"></div>
    </div>
</div>

<div class="wrapper wrapper-content">
  <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-content">
            <?php echo form_open('report/stok/history_items',array('class' => 'form-horizontal d-print-none')); ?>
            <div class="row-form ">
              <div class="col-md-2">
                <p>Nama Barang</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='items_id' name="items_id">
                  <option value="0" > - Pilih - </option>
                  <?php 
                  foreach ($data_items as $key => $value) {
                    if($this->input->post('items_id')==$value->items_id){
                    echo '<option value="'.$value->items_id.'" selected>'.$value->items_name.'</option>';
                    }else{
                    echo '<option value="'.$value->items_id.'">'.$value->items_name.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>   
            <div class="row-form ">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input placeholder="Dari" name="tgl_dari" value="<?php echo ($this->input->post('tgl_dari')!='')? $this->input->post('tgl_dari') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
              <div class="col-md-3">
                <input placeholder="Sampai" name="tgl_sampai" value="<?php echo ($this->input->post('tgl_sampai')!='')? $this->input->post('tgl_sampai') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
            </div>         
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('items_id')!=''){ ?>
                <!-- <a target="_blank" href="" onclick="window.print()" class="btn btn-info btn-sm">Print</a> -->
                <!-- <a target="_blank" href="<?php echo base_url('report/stok/print_excell/'.$this->input->post('warehouse_id').'/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai')); ?>" class="btn btn-success btn-sm">Excell</a> -->
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <br/>
              </div>
            </div>
            <?php echo form_close(); ?>                 
            <div>
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="12%">Tanggal</th>
                    <th width="12%">Nama Barang</th>
                    <th>No. Transaksi</th>
                    <th>Tujuan</th>
                    <th>Keterangan</th>
                    <th width="10%">Harga</th>
                    <th width="10%">Qty</th>
                  </tr>
                </thead>
                  <?php 
                  foreach ($data_stok_current as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo tgl_singkat($value->doc_date); ?></td>
                    <td><?php echo $value->items_name ?></td>
                    <td><?php echo $value->doc_no ?></td>
                    <td><?php echo $value->gudang_project ?></td>
                    <td><?php echo $value->remarks ?></td>
                    <td><?php echo $value->price ?></td>
                    <td><?php echo $value->qty ?></td>
                  </tr>                  
                  <?php
                  }
                  ?>
              </table>
            </div>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#items_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": true, "paging": false, "ordering": false,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Items'}},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

  $('#save').click(function(){
    if(!$('#items_id').val()){
      toastr.error("<strong>Nama Barang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
      $('#warehouse_id').select2('open');
      return false;
    }
  });
});
</script>
