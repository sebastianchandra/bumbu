<div class="app-title">
  <div>
    <h1>Report Purchase Order</h1>
    <p><ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>welcome">Home</a></li>
      <li class="breadcrumb-item">Report</li>
      <li class="breadcrumb-item active">Purchase Order</li>
    </ul></p>
  </div>
</div>

<div class="d-print-none">
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <?php echo form_open('report/purchase_order',array('class' => 'form-horizontal')); ?>
          <div class="tile-body">
            <div class="form-group row">
              <label class="control-label col-md-2">Year</label>
              <div class="col-md-2">
                <select class="form-control" id='year' name="year">
                  <option value=""> - </option>
                  <?php 
                  for($year=$year_old;$year<=$year_now;$year++){
                    if($this->input->post('year')==$year){
                      echo '<option value="'.$year.'" selected>'.$year.'</option>';
                    }else{
                      echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Month</label>
              <div class="col-md-5">
                <select class="form-control" id='month' name="month">
                  <option value=""> - </option>
                  <?php 
                  foreach ($data_month as $key => $value) {
                    if($this->input->post('month')==$value['id']){
                      echo '<option value="'.$value['id'].'" selected>'.$value['nama_bulan'].'</option>';
                    }else{
                      echo '<option value="'.$value['id'].'">'.$value['nama_bulan'].'</option>';
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-md-2">Project</label>
              <div class="col-md-3">
                <select class="form-control" id='project' name="project">
                  <option value=""> - </option>
                  <?php 
                    foreach ($data_project as $key => $value) {
                      if($this->input->post('project')==$value->project_id){
                        echo '<option value="'.$value->project_id.'" selected>'.$value->project_name.'</option>';
                      }else{
                        echo '<option value="'.$value->project_id.'">'.$value->project_name.'</option>';
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="tile-footer">
            <div class="row">
              <div class="col-md-8 col-md-offset-3">
                <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-search"></i></button>&nbsp;&nbsp;&nbsp;
                <!-- <a class="btn btn-success" href="<?php echo base_url(); ?>master/items"><i class="fa fa-print"></i></a> -->
              </div>
            </div>
          </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<?php 
if(!empty($this->input->post('month')) or !empty($this->input->post('year')) or !empty($this->input->post('project'))){
?>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <table>
            <tr></tr>
          </table>
          <?php 
          //test($data_po,0);
          $res_hist = array();
          foreach($data_po as $key=>$val){ 
            $res_hist[$val->project_id][]=$val; 
          }
          ?>
          <table class="table table-print">
            <tr style="font-size: 11px;">
              <th><strong>Company Name</strong></th>
              <th><strong>Project</strong></th>
              <!-- <th><strong>PR No</strong></th> -->
              <th><strong>Supplier</strong></th>
              <th><strong>PO No</strong></th>
              <th><strong>PO Date</strong></th>
              <th><strong>PO Status</strong></th>
              <th><strong>Total</strong></th>
            </tr>
            <?php 
            $total      = 0;
            foreach ($data_po_sup as $key => $detail) {
              foreach ($res_hist[$detail->project_id] as $key => $value) {
                //test($value,0);
                $total  = $total+$value->total;
            ?>
              <tr style="font-size: 10px;">
                <td><?php echo $value->company_name; ?></td>
                <td><?php echo $value->project_name; ?></td>
                <!-- <td><?php echo $value->pr_no; ?></td> -->
                <td><?php echo $value->supplier_name; ?></td>
                <td><?php echo $value->po_no; ?></td>
                <td><?php echo ($value->po_date!='') ? tgl_singkat($value->po_date) : ''; ?></td>
                <td><?php echo $value->po_status; ?></td>
                <td align="right">Rp. <?php echo money($value->total); ?></td>
              </tr>
              <?php
              }
              ?>
              <tr style="font-size: 10px;">
                <td colspan="6" align="right"><strong>Total Project </strong></td>
                <td align="right">Rp. <?php echo money($total); ?></td>
              </tr>
              <?php
              $total      = 0;
            }
            ?>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php 
}
?>

<script type="text/javascript">
  $("#warehouse_id").select2({
    allowClear: true});

  $("#year").select2();
  $("#month").select2();
  $("#project").select2();

</script>