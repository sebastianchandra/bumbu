<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Report</li>
                  <li class="breadcrumb-item">Omzet</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Payment Customer</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>
<!-- 
  <option value="1">Cash</option>
  <option value="2">Transfer</option>
  <option value="3">Giro</option>
  <option value="4">Cek</option> 
-->
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <?php echo form_open('report/omzet/payment_customer',array('class' => 'container-fluid d-print-none')); ?>
            <div class="form-group row ">
              <div class="col-md-2">
                <p>Tanggal</p>
              </div>
              <div class="col-md-3">
                <input placeholder="Dari" name="tgl_dari" value="<?php echo ($this->input->post('tgl_dari')!='')? $this->input->post('tgl_dari') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
              <div class="col-md-3">
                <input placeholder="Sampai" name="tgl_sampai" value="<?php echo ($this->input->post('tgl_sampai')!='')? $this->input->post('tgl_sampai') : ''; ?>" class="form-control" type="date" id="datepicker_from"/>
              </div>
            </div>         
            <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('tgl_dari')!=''){ ?>
                  <a target="_blank" href="<?php echo base_url('report/omzet/payment_customer_excell/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai').'/html'); ?>" class="btn btn-info btn-sm">Print</a>
                  <a target="_blank" href="<?php echo base_url('report/omzet/payment_customer_excell/'.$this->input->post('tgl_dari').'/'.$this->input->post('tgl_sampai').'/excell'); ?>" class="btn btn-success btn-sm">Excell</a>
                <?php } ?>
              </div>
            </div>
            <?php echo form_close(); ?>   
            <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th width="10%">Payment No</th>
                    <th width="10%">Invoice No</th>
                    <th width="10%">Payment Date</th>
                    <!-- <th>Project Name</th> -->
                    <th>Customer</th>
                    <!-- <th>Supplier Name</th> -->
                    <!-- <th>Warehouse</th> -->
                    <th>Type</th>
                    <th>Total</th>
                    <!-- <th width="15%">Action</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $total = 0;
                  foreach ($data_sales as $key => $value) {
                    $total = $total+$value->total;
                    if($value->payment_type==1){
                      $type   = "Cash";
                    }elseif($value->payment_type==2){
                      $type   = "Transfer";
                    }elseif($value->payment_type==3){
                      $type   = "Giro";
                    }elseif($value->payment_type==4){
                      $type   = "Cheque";
                    }
                  ?>
                  <tr>
                    <td><?php echo $value->inv_no; ?></td>
                    <td><?php echo $value->inv_no; ?></td>
                    <td><?php echo tgl_singkat($value->fp_date); ?></td>
                    <!-- <td><?php #echo $value->project_name; ?></td> -->
                    <td><?php echo $value->fc_name; ?></td>
                    <!-- <td><?php echo $value->supplier_name; ?></td> -->
                    <!-- <td><?php echo $value->warehouse_name; ?></td> -->
                    <td><?php echo $type;?></td> 
                    <td align="right"><?php echo number_format($value->total,2); ?> </td> 
                    
                  </tr>
                  <?php
                  }
                  ?>
                  <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>Total</td>
                      <td align="right"><?= number_format($total,2); ?></td>
                  </tr>
                </tbody>
              </table>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$("#warehouse_id").select2();

$(document).ready(function(){
  $('#vtable').DataTable({
    aaSorting: [],
    "searching": true, "paging": false, "ordering": false,"bInfo" : false,
    pageLength: 25,
    responsive: true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: 
    [
      // {extend: 'copy'},
      // {extend: 'csv'},
      {extend: 'excel', title: 'Histori Stok', exportOptions:{messageTop: 'Histori Stok'}},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    ]
  }); 

//   $('#save').click(function(){
//     if(!$('#warehouse_id').val()){
//       toastr.error("<strong>Gudang</strong> Tidak Boleh Kosong", 'Alert', {"positionClass": "toast-top-center"});
//       $('#warehouse_id').select2('open');
//       return false;
//     }
//   });
});
</script>
