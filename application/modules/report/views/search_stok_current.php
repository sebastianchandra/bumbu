<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Report</li>
                  <li class="breadcrumb-item">Stock</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Current Stock</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <!-- <a class="btn btn-outline-secondary" href="<?= base_url("master/unit/form"); ?>">Input</a> -->
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <?php echo form_open('report/stok/current_stok',array('class' => 'container-fluid d-print-none')); ?>
            <div class="form-group row">
              <div class="col-md-2">
                <p>Warehouse Name</p>
              </div>
              <div class="col-md-3">
                <select class="form-control" id='warehouse_id' name="warehouse_id">
                  <option value="All"> All </option>
                  <?php 
                  foreach ($data_warehouse as $key => $value) {

                    echo '<option value="'.$value->warehouse_id.'">'.$value->warehouse_name.'</option>';
                  }
                  ?>
                </select>
              </div>
            </div>            
            <div class="row-form">
              <div class="col-md-2"></div>
              <div class="col-md-7">
                <button type="submit" id="save" class="btn btn-primary btn-sm" id="save">Search</button>
                <?php if($this->input->post('warehouse_id')!=''){ ?>
                  <a target="_blank" href="<?= base_url('report/stok/current_stock_print/'.$this->input->post('warehouse_id')."/html") ?>" class="btn btn-info btn-sm">Print</a>
                  <a target="_blank" href="<?= base_url('report/stok/current_stock_print/'.$this->input->post('warehouse_id')."/excell") ?>" class="btn btn-success btn-sm">Excell</a>
                <?php } ?>
                <!-- <a class="btn btn-warning btn-sm" onclick="window.print()">Print</a> -->
              </div>
            </div>
            <div class="row-form">
              <div class="col-md-12">
                <hr/>
              </div>
            </div>
            <?php echo form_close(); ?>   
              <table class="table table-striped table-bordered table-hover dataTables-example" id="vtable">
                <thead>
                  <tr>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Nama Gudang</th>
                    <!-- <th>Items In</th>
                    <th>Items Out</th>
                    <th>Adj In</th>
                    <th>Adj Out</th> -->
                    <th>Current Stock</th>
                    <th width="5%">Satuan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  foreach ($data_stok_current as $key => $value) {
                  ?>
                  <tr>
                    <td><?php echo $value->items_code; ?></td>
                    <td><?php echo $value->items_name; ?></td>
                    <td><?php echo $value->warehouse_name; ?></td>
                    <!-- <td><?php echo $value->items_in; ?></td>
                    <td><?php echo $value->items_out; ?></td>
                    <td><?php echo $value->adj_in; ?></td>
                    <td><?php echo $value->adj_out; ?></td> -->
                    <td><?php echo $value->current_stock; ?></td>
                    <td><?php echo $value->items_unit_name; ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
          </div>     
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $('#vtable').DataTable({
    "paging":   false,
    "ordering": true,
    "info":     false,
    "filter":   true,
    // 'pageLength': 25,  
    "language": {
      "emptyTable": "Tidak Ada Data"
    }
    // aaSorting: [],
    // responsive: true,
    // dom: '<"html5buttons"B>lTfgitp',
    // buttons: 
    // [
      // {extend: 'copy'},
      // {extend: 'csv'},
      // {extend: 'excel', title: 'ExampleFile'},
      // {extend: 'pdf', title: 'ExampleFile'},
      // {extend: 'print',
      //   customize: function (win){
      //     $(win.document.body).addClass('white-bg');
      //     $(win.document.body).css('font-size', '10px');

      //     $(win.document.body).find('table')
      //       .addClass('compact')
      //       .css('font-size', 'inherit');
      //   }
      // }
    // ]
  }); 
});
</script>
