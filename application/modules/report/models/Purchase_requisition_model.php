<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_requisition_model extends CI_Model
{
    function __construct(){
        parent::__construct();

        if($this->current_user['company_id']==1){
            $this->dbpurch = $this->load->database('purchasing_lm',true);
        }elseif($this->current_user['company_id']==2){
            $this->dbpurch = $this->load->database('purchasing_sg',true);
        }
      
    }

    function get_pr_approve()
    {
        $sql ="SELECT a.pr_id, a.pr_no, c.request_no, a.pr_date, a.project_name, a.company_name, a.supplier_name, a.requester, a.pr_status, 
                (SELECT COUNT(b.pr_no) jumlah FROM trn_pr_approval b WHERE b.pr_id=a.pr_id AND b.is_approve='1') jml_approve,
                (SELECT COUNT(b.pr_no) jumlah FROM trn_pr_approval b WHERE b.pr_id=a.pr_id AND b.is_approve='0') jml_not_approve,
                (SELECT COUNT(b.pr_no) jumlah FROM trn_pr_approval b WHERE b.pr_id=a.pr_id) approve
                FROM trn_pr_01 a LEFT JOIN trn_request_01 c ON a.request_id=c.request_id
                ORDER BY a.pr_id DESC";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }
    
    
}   