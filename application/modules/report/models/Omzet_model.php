<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Omzet_model extends CI_Model
{
    function __construct(){
        parent::__construct();
        // $this->dbpurch = $this->load->database('purchasing',true);
      
    }

    // function history_price_group_by_sales_no($tahun,$bulan){
    //     return $this->db->query("SELECT a.sales_id,a.sales_no,a.sales_date,a.project_id,c.project_name,a.doc_status,b.fish_id,d.fish_name,b.owner_price,
    //                     b.cust_price,b.gov_price,b.qty,b.subtotal
    //                     FROM db_omzet_shp.trn_sales_01 a 
    //                     LEFT JOIN db_omzet_shp.trn_sales_02 b ON a.sales_id=b.sales_id
    //                     LEFT JOIN db_bumbu_master.mst_project c ON a.project_id=c.project_id
    //                     LEFT JOIN db_bumbu_master.mst_fish d ON d.fish_id=b.fish_id
    //                     WHERE a.doc_status<>'Reject' AND SUBSTR(a.sales_date,6,2)='".$bulan."' AND YEAR(a.sales_date)='".$tahun."'
    //                     GROUP BY a.sales_no");
    // }

    function history_price($tgl_dari,$tgl_sampai){
        return $this->db->query("SELECT a.sales_id,a.sales_no,a.sales_date,a.project_id,c.project_name,a.doc_status,b.fish_id,d.fish_name,
            b.owner_price,b.cust_price,b.gov_price,b.qty,b.subtotal
            FROM db_omzet_shp.trn_sales_01 a 
            LEFT JOIN db_omzet_shp.trn_sales_02 b ON a.sales_id=b.sales_id
            LEFT JOIN db_bumbu_master.mst_project c ON a.project_id=c.project_id
            LEFT JOIN db_bumbu_master.mst_fish d ON d.fish_id=b.fish_id
            WHERE a.doc_status<>'Reject' AND a.sales_date BETWEEN '".$tgl_dari."' AND '".$tgl_sampai."'
            ORDER BY b.fish_id");
    }

    function sales_no_list($tgl_dari,$tgl_sampai){
        return $this->db->query("SELECT a.sales_no FROM db_omzet_shp.trn_sales_01 a WHERE a.doc_status<>'Reject' AND 
                a.sales_date BETWEEN '".$tgl_dari."' AND '".$tgl_sampai."'
                ORDER BY a.sales_no");
    }

    function get_data_sales($tgl_dari,$tgl_sampai){
        return $this->db->query("SELECT * FROM db_bumbu_transaction.trn_invoice_01 a WHERE a.is_cancel='0' AND 
                a.invoice_date BETWEEN '".$tgl_dari."' AND '".$tgl_sampai."'
                ORDER BY a.invoice_date ASC")->result();
    }

    function payment_customer($tgl_dari,$tgl_sampai){
        return $this->db->query("SELECT a.sales_no inv_no,a.sales_id inv_id,a.total,c.total_cust_price,b.fp_date,b.fp_no,b.fp_id,c.fc_name,a.total,b.payment_type
                        FROM db_bumbu_transaction.trn_sales_payment_02 a 
                        LEFT JOIN db_bumbu_transaction.trn_sales_payment_01 b ON a.fp_id=b.fp_id
                        LEFT JOIN db_bumbu_transaction.trn_invoice_01 c ON a.sales_id=c.invoice_id
                        WHERE b.is_active=1 AND b.fp_date BETWEEN '".$tgl_dari."' AND '".$tgl_sampai."'
                        ORDER BY b.fp_date ASC ")->result();
    }
    
}   