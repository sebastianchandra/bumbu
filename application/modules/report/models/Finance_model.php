<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Finance_model extends CI_Model
{
    function __construct(){
        parent::__construct();
        // $this->dbpurch = $this->load->database('purchasing',true);
      
    }

    function total_penjualan($tahun,$bulan){
        return $this->db->query("SELECT cr_coa_id,cr_coa_name,SUM(cr_value) penjualan FROM db_fa_shp.trn_journal WHERE cr_coa_id='195' AND year_period='".$tahun."' AND month_period='".$bulan."'")->row();
    }

    function total_hpp($tahun,$bulan){
        return $this->db->query("SELECT db_coa_id,db_coa_name,SUM(db_value) hpp FROM db_fa_shp.trn_journal WHERE db_coa_id='194' AND year_period='".$tahun."' AND month_period='".$bulan."'")->row();
    }

    function total_biaya($tahun,$bulan){
        return $this->db->query("SELECT a.coa_name,b.* FROM db_bumbu_master.mst_coa a LEFT JOIN db_fa_shp.trn_journal b ON a.coa_id=b.db_coa_id 
                WHERE a.coa_parent_id='197' AND b.year_period='".$tahun."' AND b.month_period='".$bulan."'");
    }

    function total_lain2($tahun,$bulan){
        return $this->db->query("SELECT a.coa_name,
                                IF(a.coa_name=b.db_coa_name,b.db_value,'')debet,
                                IF(a.coa_name=c.cr_coa_name,c.cr_value,'')kredit 
                                FROM db_bumbu_master.mst_coa a 
                                LEFT JOIN db_fa_shp.trn_journal b ON a.coa_id=b.db_coa_id 
                                        AND b.year_period='".$tahun."' AND b.month_period='".$bulan."' 
                                LEFT JOIN db_fa_shp.trn_journal c ON a.coa_id=c.cr_coa_id 
                                        AND c.year_period='".$tahun."' AND c.month_period='".$bulan."' 
                                WHERE a.coa_parent_id='208'  
                                ORDER BY debet");
    }

    function neraca_query_aktiva($kode/*,$tahun,$bulan*/){
        return $this->db->query("SELECT a.coa_level,a.coa_detail,a.coa_id,a.coa_name,a.is_lowest,a.coa_parent_id,
            (SELECT SUM(db_value) FROM db_fa_shp.trn_journal a1 WHERE SUBSTR(a1.db_coa_detail,1,2)='".$kode."' AND a1.db_coa_id=a.coa_id 
            GROUP BY a1.db_coa_id) debet,
            (SELECT SUM(cr_value) FROM db_fa_shp.trn_journal a1 WHERE SUBSTR(a1.cr_coa_detail,1,2)='".$kode."' AND a1.cr_coa_id=a.coa_id 
            GROUP BY a1.cr_coa_id) kredit
            FROM db_bumbu_master.mst_coa a WHERE SUBSTR(a.coa_detail,1,2)='".$kode."' AND a.is_active='1'
            ORDER BY a.coa_detail");

        // return $this->db->query("SELECT a.coa_detail,a.coa_name,a.is_lowest,SUM(b.db_value)debet FROM db_bumbu_master.mst_coa a 
        //                             LEFT JOIN db_fa_shp.trn_journal b ON b.db_coa_id=a.coa_id 
        //                             WHERE SUBSTR(a.coa_detail,1,2)IN ('".$kode."') AND a.is_active='1' 
        //                             AND IF(a.is_lowest=1,b.db_value IS NOT NULL, a.is_lowest=0)
        //                             GROUP BY a.coa_id
        //                             ORDER BY a.coa_detail");
    }

    function neraca_query_pasiva($kode/*,$tahun,$bulan*/){
        return $this->db->query("SELECT a.coa_level,a.coa_detail,a.coa_id,a.coa_name,a.is_lowest,a.coa_parent_id,
            (SELECT SUM(db_value) FROM db_fa_shp.trn_journal a1 WHERE SUBSTR(a1.db_coa_detail,1,2) IN (".$kode.") AND a1.db_coa_id=a.coa_id 
            GROUP BY a1.db_coa_id) debet,
            (SELECT SUM(cr_value) FROM db_fa_shp.trn_journal a1 WHERE SUBSTR(a1.cr_coa_detail,1,2) IN (".$kode.") AND a1.cr_coa_id=a.coa_id 
            GROUP BY a1.cr_coa_id) kredit
            FROM db_bumbu_master.mst_coa a WHERE SUBSTR(a.coa_detail,1,2) IN (".$kode.") AND a.is_active='1' AND a.coa_id<>'212'
            ORDER BY a.coa_detail");
    }

    function coa_history($tahun,$bulan){
        return $this->db->query("SELECT a.coa_level,a.coa_detail,a.coa_id,a.coa_name,a.is_lowest,a.coa_parent_id,
            (SELECT SUM(db_value) FROM db_fa_shp.trn_journal a1 WHERE a1.db_coa_id=a.coa_id AND a1.year_period='".$tahun."' AND a1.month_period='".$bulan."'
            GROUP BY a1.db_coa_id) debet,
            (SELECT SUM(cr_value) FROM db_fa_shp.trn_journal a1 WHERE a1.cr_coa_id=a.coa_id AND a1.year_period='".$tahun."' AND a1.month_period='".$bulan."'
            GROUP BY a1.cr_coa_id) kredit
            FROM db_bumbu_master.mst_coa a WHERE a.is_active='1' AND a.is_lowest=1
            ORDER BY a.coa_detail");
    }

    function journal_history($tahun,$bulan,$coa_id){
        return $this->db->query("SELECT * FROM (
            SELECT a.trn_id,a.journal_type,a.journal_id id,a.journal_date tgl,a.db_coa_id coa_id,a.db_coa_detail coa_detail,a.db_coa_name coa_name,a.db_value debet
            ,a.db_info info_debet,'' kredit,a.cr_info info_kredit 
            FROM db_fa_shp.trn_journal a WHERE a.year_period='".$tahun."' AND a.month_period='".$bulan."' AND a.db_coa_id='".$coa_id."'
            UNION ALL
            SELECT a.trn_id,a.journal_type,a.journal_id id,a.journal_date tgl,a.cr_coa_id coa_id,a.cr_coa_detail coa_detail,a.cr_coa_name coa_name,'' debet
            ,a.db_info info_debet,a.cr_value debet,a.cr_info info_kredit
            FROM db_fa_shp.trn_journal a WHERE a.year_period='".$tahun."' AND a.month_period='".$bulan."' AND a.cr_coa_id='".$coa_id."'
        ) b
        ORDER BY b.id");
    }

    function sisa_hutang($supplier,$project){
        $query      = "SELECT b.supplier_name,a.po_id,a.po_no,a.po_date,a.project_id,a.project_name,a.po_status,a.payment_status,
                        a.total t_po,SUM(c.total) t_payment,d.pp_no,d.pp_id
                        FROM db_bumbu_transaction.trn_po_01 a 
                        LEFT JOIN db_bumbu_master.mst_supplier b ON a.supplier_id=b.supplier_id
                        LEFT JOIN db_bumbu_transaction.trn_po_payment_02 c ON a.po_id=c.po_id
                        LEFT JOIN db_bumbu_transaction.trn_po_payment_01 d ON d.pp_id=c.pp_id
                        WHERE a.payment_status IN ('Outstanding','Process') AND a.po_status NOT IN ('Reject','Force Close') ";
        if($supplier!='0'){
            $query     .= " AND a.supplier_id='".$supplier."'";
        }
        if($project!='0'){
            $query     .= " AND a.project_id='".$project."'";
        }
        $query         .= " GROUP BY a.po_no ORDER BY b.supplier_name, a.po_no ";
        // test($query,1);
        return $this->db->query($query);
        
    }
    
}   