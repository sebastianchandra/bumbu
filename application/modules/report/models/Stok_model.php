<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stok_model extends CI_Model
{
    function __construct(){
      parent::__construct();
      $this->dbpurch = $this->load->database('purchasing',true);
      
    }

    function get_current_stok(){
        $sql        = " SELECT a.stock_id,c.items_code,a.items_id,c.items_name,a.warehouse_id,IFNULL(b.warehouse_name,'') warehouse_name,
                        a.items_in,a.items_out,a.adj_in,d.items_unit_name,a.adj_out,IFNULL(a.current_stock,'0') current_stock
                        FROM db_bumbu_master.mst_items c
                        LEFT JOIN db_bumbu_transaction.trn_stock a ON a.items_id=c.items_id 
                        LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id
                        LEFT JOIN db_bumbu_master.mst_items_unit d ON d.items_unit_id =c.items_unit WHERE ";
        $sql       .= " c.is_active='1' ";
        if($this->input->post('warehouse_id')!='All'){
        $sql       .= " AND a.warehouse_id='".$this->input->post('warehouse_id')."' ";            
        }
        $sql       .= " ORDER BY a.warehouse_id ";
        return $this->dbpurch->query($sql)->result();

    }

    function get_current_stok_print($warehouse_id){
        $sql        = " SELECT a.stock_id,c.items_code,a.items_id,c.items_name,a.warehouse_id,IFNULL(b.warehouse_name,'') warehouse_name,
                        a.items_in,a.items_out,a.adj_in,d.items_unit_name,a.adj_out,IFNULL(a.current_stock,'0') current_stock
                        FROM db_bumbu_master.mst_items c
                        LEFT JOIN db_bumbu_transaction.trn_stock a ON a.items_id=c.items_id 
                        LEFT JOIN db_bumbu_master.mst_warehouse b ON a.warehouse_id=b.warehouse_id
                        LEFT JOIN db_bumbu_master.mst_items_unit d ON d.items_unit_id =c.items_unit WHERE ";
        $sql       .= " c.is_active='1' ";
        if($warehouse_id!='All'){
        $sql       .= " AND a.warehouse_id='".$warehouse_id."' ";            
        }
        $sql       .= " ORDER BY a.warehouse_id ";
        return $this->dbpurch->query($sql)->result();

    }

    function get_laststockhistory(){
          $query         = $this->dbpurch->query("SELECT a.items_id,b.items_name,b.items_code,a.current_stock FROM trn_stock a, db_bumbu_master.mst_items b WHERE a.items_id=b.items_id AND a.warehouse_id='".$this->input->post('warehouse_id')."' ORDER BY b.items_name")->result();
          return $query;
    }

    function get_history($items_id,$warehouse_id){
          return $query       = $this->dbpurch->query("SELECT a.trn_date,a.items_id,b.items_name,a.doc_no,a.warehouse_id,a.activity,a.qty,a.current_stock FROM trn_stock_hist a, db_bumbu_master.mst_items b
               WHERE a.items_id=b.items_id AND a.trn_date BETWEEN '2020-05-01' AND '2020-05-31' AND a.current_stock>=0 AND a.warehouse_id='".$warehouse_id."' AND a.items_id='".$items_id."'")->result();
    }

    function get_items_project(){
        return $this->dbpurch->query("SELECT a.is_date,a.is_no,a.project_id,d.project_name,a.doc_ref,a.doc_ref,b.items_id,c.items_name,e.items_unit_name,b.qty,b.price,b.discount,b.total,
            f.supplier_id,IFNULL(g.supplier_name,'Stok') supplier_name
            FROM trn_incoming_stock_01 a
            LEFT JOIN trn_incoming_stock_02 b ON a.is_id=b.is_id
            LEFT JOIN db_bumbu_master.mst_items c ON b.items_id=c.items_id
            LEFT JOIN db_bumbu_master.mst_project d ON a.project_id=d.project_id
            LEFT JOIN db_bumbu_master.mst_items_unit e ON e.items_unit_id=c.items_unit
            LEFT JOIN trn_po_01 f ON f.po_no=a.doc_ref
            LEFT JOIN db_bumbu_master.mst_supplier g ON g.supplier_id=f.supplier_id
            WHERE a.is_status<>'Reject' AND a.project_id='".$this->input->post('project_id')."' ORDER BY a.is_date")->result();
    }

    function get_laststockhistoryExcell($warehouse_id){
          $query         = $this->dbpurch->query("SELECT a.items_id,b.items_name,b.items_code,a.current_stock FROM trn_stock a, db_bumbu_master.mst_items b WHERE a.items_id=b.items_id AND a.warehouse_id='".$warehouse_id."' ORDER BY b.items_name")->result();
          return $query;
    }

    function get_historyitems(){
        $query         = $this->dbpurch->query("SELECT a.* FROM
                    (SELECT b.is_id doc_id,b.is_date doc_date,b.is_no doc_no,b.warehouse_id wh_id,b.project_id project_id,b.is_kind kind,
                    CASE WHEN b.warehouse_id=0 THEN d.project_name WHEN b.project_id=0 THEN c.warehouse_name END AS gudang_project,
                    a.qty qty,a.price price,a.total total,a.items_name items_name,a.items_id items_id,b.remarks remarks
                    FROM trn_incoming_stock_02 a 
                    LEFT JOIN trn_incoming_stock_01 b ON a.is_id=b.is_id
                    LEFT JOIN db_bumbu_master.mst_warehouse c ON c.warehouse_id=b.warehouse_id
                    LEFT JOIN db_bumbu_master.mst_project d ON d.project_id=b.project_id
                    UNION
                    SELECT b.os_id doc_id,b.os_date doc_date,b.os_no doc_no,b.warehouse_id wh_id,b.project_id project_id,b.os_kind kind,
                    CASE WHEN b.warehouse_id=0 THEN d.project_name WHEN b.project_id=0 THEN c.warehouse_name END AS gudang_project,
                    a.qty qty,a.price price,a.total total, a.items_name items_name,a.items_id items_id,b.remarks remarks
                    FROM trn_outgoing_stock_02 a
                    LEFT JOIN trn_outgoing_stock_01 b ON a.os_id=b.os_id
                    LEFT JOIN db_bumbu_master.mst_warehouse c ON c.warehouse_id=b.warehouse_id
                    LEFT JOIN db_bumbu_master.mst_project d ON d.project_id=b.project_id) a
                    WHERE a.doc_date BETWEEN '".$this->input->post('tgl_dari')."' AND '".$this->input->post('tgl_sampai')."' 
                    AND a.items_id='".$this->input->post('items_id')."'
                    ORDER BY a.doc_date")->result();
        return $query;
    }
















































    function get_stok_awal()
    {
        $sql    ="SELECT a.stock_hist_id, a.items_id, b.items_name, b.items_code, a.doc_no, a.warehouse_id, c.warehouse_name, a.trn_date, a.trn_year, a.trn_month, 
                a.activity, a.qty, a.old_stock, a.current_stock FROM trn_stock_hist a 
                LEFT JOIN db_bumbu_master.mst_items b ON a.items_id=b.items_id 
                LEFT JOIN db_bumbu_master.mst_warehouse c on a.warehouse_id=c.warehouse_id
                WHERE b.is_active='1' AND c.is_active='1' ";

        if(!empty($this->input->post('warehouse_id'))) {
            $sql    .=" AND a.warehouse_id='".$this->input->post('warehouse_id')."' ";
        }
        if(!empty($this->input->post('year'))) {
            $sql    .=" AND a.trn_year='".$this->input->post('year')."' ";
        }
        if(!empty($this->input->post('month'))) {
            $sql    .=" AND a.trn_month='".$this->input->post('month')."' ";
        }
        if(!empty($this->input->post('item_id'))) {
            $sql    .=" AND a.items_id='".$this->input->post('item_id')."' ";
        }
        $sql    .="GROUP BY a.items_id ORDER BY a.items_id DESC, a.stock_hist_id ";
        
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    // function get_stok_akhir()
    // {
    //     $sql    ="SELECT a.stock_hist_id, a.items_id, a.doc_no, a.warehouse_id, a.trn_date, a.trn_year, a.trn_month, a.activity, a.qty, a.old_stock, a.current_stock
    //             FROM trn_stock_hist a ";

    //     if(!empty($this->input->post('warehouse_id')) or !empty($this->input->post('year')) or !empty($this->input->post('month'))){
    //         $sql    .="WHERE ";
    //     }
    //     if(!empty($this->input->post('warehouse_id'))) {
    //         $sql    .="a.warehouse_id=1 AND ";
    //     }
    //     if(!empty($this->input->post('year'))) {
    //         $sql    .="a.trn_year='2018' AND ";
    //     }
    //     if(!empty($this->input->post('month'))) {
    //         $sql    .="a.trn_month='8' ";
    //     }
    //     $sql    .="GROUP BY a.items_id ORDER BY a.items_id DESC, a.stock_hist_id ";
        
    //     $query = $this->dbpurch->query($sql);
    //     return $query->result();
    // }

    function get_stok_history()
    {
        $sql    ="SELECT a.stock_hist_id, a.items_id, b.items_name, b.items_code, a.doc_no, a.warehouse_id, a.trn_date, a.trn_year, a.trn_month, a.activity, a.qty, 
                a.old_stock, a.current_stock FROM trn_stock_hist a LEFT JOIN db_bumbu_master.mst_items b ON a.items_id=b.items_id WHERE b.is_active='1'  ";

        if(!empty($this->input->post('warehouse_id'))) {
            $sql    .=" AND a.warehouse_id='".$this->input->post('warehouse_id')."' ";
        }
        if(!empty($this->input->post('year'))) {
            $sql    .=" AND a.trn_year='".$this->input->post('year')."'  ";
        }
        if(!empty($this->input->post('month'))) {
            $sql    .=" AND a.trn_month='".$this->input->post('month')."'  ";
        }
        if(!empty($this->input->post('item_id'))) {
            $sql    .=" AND a.items_id='".$this->input->post('item_id')."' ";
        }
        $sql    .="ORDER BY a.items_id, a.stock_hist_id ";
        
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }

    // function get_current_stok(){
    //     $sql    ="SELECT a.items_id, b.items_code,b.items_name,a.warehouse_id, c.warehouse_name, a.current_stock FROM trn_stock a 
    //                 LEFT JOIN db_bumbu_master.mst_items b ON a.items_id=b.items_id
    //                 LEFT JOIN db_bumbu_master.mst_warehouse c ON a.warehouse_id=c.warehouse_id
    //                 WHERE a.warehouse_id='".$this->input->post('warehouse_id')."'";
    //     $query = $this->dbpurch->query($sql);
    //     return $query->result();
    // }
    
}   