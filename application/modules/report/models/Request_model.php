<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_model extends CI_Model
{
    function __construct(){
      parent::__construct();
      
      if($this->current_user['company_id']==1){
        $this->dbpurch = $this->load->database('purchasing_lm',true);
      }elseif($this->current_user['company_id']==2){
        $this->dbpurch = $this->load->database('purchasing_sg',true);
      }
      
    }

    function get_movement()
    {
        $sql="SELECT b.company_name,a.company_id,a.request_id,a.request_no,a.request_date,a.status_approve,
                CASE a.status_approve 
                WHEN '0' THEN 'NEW'
                WHEN '1' THEN 'Approved'
                WHEN '2' THEN 'Not Approved'
                END AS 'req_status',c.pr_id,c.pr_no,c.pr_date,c.project_id,c.pr_status,d.project_name,c.supplier_id,e.supplier_name,f.po_id,f.po_no,f.po_date,f.po_status,
                (SELECT COUNT(pr_approval_id) jml FROM trn_pr_approval WHERE pr_id=c.pr_id AND is_approve='0') pr_notapprove,
                (SELECT COUNT(pr_approval_id) jml FROM trn_pr_approval WHERE pr_id=c.pr_id AND is_approve='1') pr_approve
                FROM trn_request_01 a 
                LEFT JOIN db_bumbu_master.mst_company b ON a.company_id=b.company_id
                LEFT JOIN trn_pr_01 c ON a.request_id=c.request_id
                LEFT JOIN db_bumbu_master.mst_project d ON c.project_id=d.project_id
                LEFT JOIN db_bumbu_master.mst_supplier e ON c.supplier_id=e.supplier_id
                LEFT JOIN trn_po_01 f ON f.pr_id=c.pr_id
                WHERE (a.request_no LIKE '%".$this->input->post('document')."%' OR c.pr_no LIKE '%".$this->input->post('document')."%' OR f.po_no LIKE '%".$this->input->post('document')."%') ";
        if($this->input->post('year')!=''){
            $sql.=" AND YEAR(a.request_date)='".$this->input->post('year')."' ";
        }
        
        $query = $this->dbpurch->query($sql);
        return $query->result();

    }

    function get_request(){
        $sql =" SELECT a.request_id,a.request_no,a.request_date,a.company_name,a.dept,b.name name_dept,a.requester,a.remark,COUNT(c.pr_id) total,a.status_approve 
                FROM trn_request_01 a 
                LEFT JOIN db_bumbu_master.mst_user_group b ON a.dept=b.id_user_group 
                LEFT JOIN trn_pr_01 c ON a.request_id=c.request_id
                WHERE a.request_id<>'' ";
        if($this->input->post('dept')!=''){
            $sql .=" AND a.dept='".$this->input->post('dept')."' ";
        }
        if($this->input->post('year')!=''){
            $sql .=" AND YEAR(a.request_date)='".$this->input->post('year')."' ";
        }        
        $sql .=" GROUP BY a.request_id ORDER BY a.request_id DESC ";
        $query = $this->dbpurch->query($sql);
        return $query->result();
    }
    
}   