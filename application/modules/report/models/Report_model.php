<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_model extends CI_Model
{

	function get_items()
    {
        $sql    ="SELECT a.supplier_code,a.supplier_name,a.address,a.city,a.contact1,a.contact2,a.contact3,a.pic_sales,a.supplier_info,b.items_price,c.items_code,
                c.items_name,c.items_kind,c.items_info,c.items_group 
                FROM mst_supplier a, mst_supplier_items b LEFT JOIN mst_items c ON b.items_id=c.items_id
                WHERE a.supplier_id=b.supplier_id
                AND a.is_active='1' AND c.is_active='1'";
        $sql    .="AND a.supplier_name LIKE '%%'";
        $sql    .="AND c.items_name LIKE '%%'";
        $sql    .="AND b.items_price BETWEEN '' AND ''";
        $sql    .="AND c.items_kind=''";
        
        $query = $this->db->query($sql);
        return $query->result();
    }

    
}	