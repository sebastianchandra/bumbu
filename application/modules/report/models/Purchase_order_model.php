<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Purchase_order_model extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->dbpurch = $this->load->database('purchasing',true);
    }

    function get_hutang_supplier(){
        $query      = "SELECT a.po_id,a.po_no,a.po_date,a.project_id,c.project_name,a.supplier_id,d.supplier_name,a.po_status,
                        a.payment_status,b.items_id,e.items_name,b.qty,b.price,b.subtotal
                        FROM trn_po_01 a
                        LEFT JOIN trn_po_02 b ON a.po_id=b.po_id
                        LEFT JOIN db_bumbu_master.mst_project c ON a.project_id=c.project_id
                        LEFT JOIN db_bumbu_master.mst_supplier d ON a.supplier_id=d.supplier_id
                        LEFT JOIN db_bumbu_master.mst_items e ON b.items_id=e.items_id
                        WHERE a.payment_status<>'Closed' AND a.supplier_id='".$this->input->post('supplier_id')."'";
        return $this->dbpurch->query($query)->result();
    }
    
    
}   