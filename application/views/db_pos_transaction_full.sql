/*
SQLyog Community v13.0.0 (64 bit)
MySQL - 10.4.13-MariaDB : Database - db_pos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_pos` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_pos`;

/*Table structure for table `mst_coa` */

DROP TABLE IF EXISTS `mst_coa`;

CREATE TABLE `mst_coa` (
  `coa_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Id COA',
  `coa_level` int(3) DEFAULT NULL COMMENT 'Level COA',
  `coa_code` varchar(5) DEFAULT NULL COMMENT 'Kode COA',
  `coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA',
  `coa_name` varchar(150) DEFAULT NULL COMMENT 'Nama COA',
  `coa_parent_id` int(3) DEFAULT NULL COMMENT 'Id Induk COA',
  `is_active` int(1) DEFAULT NULL COMMENT 'Status Aktif COA',
  `is_lowest` int(11) DEFAULT NULL COMMENT 'Status Level COA',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`coa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;

/*Data for the table `mst_coa` */

/*Table structure for table `mst_coa_level` */

DROP TABLE IF EXISTS `mst_coa_level`;

CREATE TABLE `mst_coa_level` (
  `coa_level` int(2) NOT NULL AUTO_INCREMENT COMMENT 'Level COA',
  `level_name` varchar(50) DEFAULT NULL COMMENT 'Keterangan',
  PRIMARY KEY (`coa_level`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `mst_coa_level` */

/*Table structure for table `mst_journal_config` */

DROP TABLE IF EXISTS `mst_journal_config`;

CREATE TABLE `mst_journal_config` (
  `jc_id` varchar(10) NOT NULL COMMENT 'Id Data',
  `trn_code` varchar(10) DEFAULT NULL COMMENT 'Kode Dokumen Transaksi',
  `db_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Debet',
  `db_coa_code` varchar(5) DEFAULT NULL COMMENT 'Kode COA Debet',
  `db_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Debet',
  `db_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Debet',
  `cr_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Kredit',
  `cr_coa_code` varchar(5) DEFAULT NULL COMMENT 'Kode COA Kredit',
  `cr_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Kredit',
  `cr_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Kredit',
  PRIMARY KEY (`jc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_journal_config` */

/*Table structure for table `trn_closing_journal` */

DROP TABLE IF EXISTS `trn_closing_journal`;

CREATE TABLE `trn_closing_journal` (
  `closing_journal_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Closing',
  `month_periode` varchar(2) DEFAULT NULL COMMENT 'Bulan Closing',
  `year_periode` year(4) DEFAULT NULL COMMENT 'Tahun Closing',
  `is_active` int(1) DEFAULT 1 COMMENT '1 = Closing , 0 = Open',
  `pic_closing` int(4) DEFAULT NULL,
  `time_closing` datetime DEFAULT NULL,
  PRIMARY KEY (`closing_journal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `trn_closing_journal` */

/*Table structure for table `trn_do_01` */

DROP TABLE IF EXISTS `trn_do_01`;

CREATE TABLE `trn_do_01` (
  `do_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Incoming Stok',
  `do_date` date DEFAULT NULL COMMENT 'Tanggal',
  `do_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen (CCCCYYYYMMNNNN) ex : LMIS2018070001',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Id Gudang',
  `doc_ref` varchar(15) DEFAULT NULL COMMENT 'Referensi Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `do_status` varchar(20) DEFAULT NULL COMMENT '1 = Close, 2 = Void',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trn_do_01` */

insert  into `trn_do_01`(`do_id`,`do_date`,`do_no`,`warehouse_id`,`doc_ref`,`company_id`,`company_name`,`do_status`,`remarks`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-06-15','DO2021060001',5,'SL2021060001',1,'Suhanda Souwmpie','Close','',1,'2021-06-14 09:48:07',NULL,NULL),
(2,'2021-06-16','DO2021060002',5,'SL2021060002',1,'Suhanda Souwmpie','Close','',1,'2021-06-14 09:48:30',NULL,NULL),
(3,'2021-06-14','DO2021060003',5,'SL2021060003',1,'Suhanda Souwmpie','Close','',1,'2021-06-14 09:48:48',NULL,NULL);

/*Table structure for table `trn_do_02` */

DROP TABLE IF EXISTS `trn_do_02`;

CREATE TABLE `trn_do_02` (
  `do_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `do_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Incoming',
  `do_qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty PO',
  `price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(12,2) DEFAULT 0.00,
  `total` decimal(12,2) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`do_02_id`),
  KEY `is_id` (`do_02_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `trn_do_02` */

insert  into `trn_do_02`(`do_02_id`,`do_id`,`items_id`,`items_name`,`qty`,`do_qty`,`price`,`discount`,`total`,`remarks`) values 
(1,1,482,'Engsel Dolpin',3.0,3.0,0.00,0.00,0.00,'0'),
(2,1,483,'Overfal',3.0,3.0,0.00,0.00,0.00,'0'),
(3,2,480,'Busa 7.5 x 84 x 210',2.0,2.0,0.00,0.00,0.00,'0'),
(4,2,481,'Siku Besi',3.0,3.0,0.00,0.00,0.00,'0'),
(5,3,479,'Busa 4 x 100 x 120',2.0,2.0,0.00,0.00,0.00,'0'),
(6,3,480,'Busa 7.5 x 84 x 210',2.0,2.0,0.00,0.00,0.00,'0');

/*Table structure for table `trn_fish_payment_01` */

DROP TABLE IF EXISTS `trn_fish_payment_01`;

CREATE TABLE `trn_fish_payment_01` (
  `fp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO Payment',
  `fp_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO Payment',
  `fp_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(30) DEFAULT NULL COMMENT 'Nama Customer',
  `payment_type` smallint(2) DEFAULT 0 COMMENT 'Jenis Pembayaran 1=CASH, 2=Transfer, 3=Giro, 4=Check',
  `grand_total` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Keseluruhan',
  `is_active` int(2) DEFAULT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fp_id`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  CONSTRAINT `trn_fish_payment_01_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_master_shp`.`mst_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_fish_payment_01_ibfk_2` FOREIGN KEY (`fc_id`) REFERENCES `db_master_shp`.`mst_fish_customer` (`fc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_fish_payment_01` */

/*Table structure for table `trn_fish_payment_02` */

DROP TABLE IF EXISTS `trn_fish_payment_02`;

CREATE TABLE `trn_fish_payment_02` (
  `fp_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '⁯Id Detail Fish Payment',
  `fp_id` int(11) DEFAULT NULL COMMENT 'Id Header Fish Payment',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Id Sales',
  `sales_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Sales',
  `amount` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Bayar',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Persen Diskon',
  `disc_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Diskon',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `is_active` smallint(1) DEFAULT 1,
  PRIMARY KEY (`fp_02_id`),
  KEY `fp_id` (`fp_id`),
  CONSTRAINT `trn_fish_payment_02_ibfk_1` FOREIGN KEY (`fp_id`) REFERENCES `trn_fish_payment_01` (`fp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_fish_payment_02` */

/*Table structure for table `trn_incoming_stock_01` */

DROP TABLE IF EXISTS `trn_incoming_stock_01`;

CREATE TABLE `trn_incoming_stock_01` (
  `is_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Incoming Stok',
  `is_date` date DEFAULT NULL COMMENT 'Tanggal',
  `is_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen (CCCCYYYYMMNNNN) ex : LMIS2018070001',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Id Gudang',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `project_name` varchar(55) DEFAULT NULL COMMENT 'Nama Project',
  `is_kind` int(11) DEFAULT NULL COMMENT '1 = MR PO, 2 = MR Transfer, 3 = MR Other',
  `doc_ref` varchar(15) DEFAULT NULL COMMENT 'Referensi Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `is_status` varchar(20) DEFAULT NULL COMMENT '1 = Close, 2 = Void',
  `sender_pic` varchar(50) DEFAULT NULL COMMENT 'Pengirim barang',
  `receiver_pic` varchar(50) DEFAULT NULL COMMENT 'Penerima barang',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`is_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `trn_incoming_stock_01` */

insert  into `trn_incoming_stock_01`(`is_id`,`is_date`,`is_no`,`warehouse_id`,`project_id`,`project_name`,`is_kind`,`doc_ref`,`company_id`,`company_name`,`is_status`,`sender_pic`,`receiver_pic`,`remarks`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-06-07','ISS2021060001',5,0,'',1,'POS2021060005',1,'Suhanda Souwmpie','Closed','Test 1','Test 1','Test 1',1,'2021-06-13 14:39:38',NULL,NULL),
(2,'2021-06-13','ISS2021060002',5,0,'',1,'POS2021060004',1,'Suhanda Souwmpie','Closed','','','Test 4',1,'2021-06-13 14:41:15',NULL,NULL),
(3,'2021-06-14','ISS2021060003',5,0,'',1,'POS2021060003',1,'Suhanda Souwmpie','Reject','','','Test 3',1,'2021-06-13 14:45:43',NULL,NULL),
(4,'2021-06-13','ISS2021060004',5,0,'',1,'POS2021060003',1,'Suhanda Souwmpie','Closed','Test 1','Test 1','Test 3',1,'2021-06-13 15:39:52',NULL,NULL),
(5,'2021-06-15','IST2021060005',5,0,'',2,'',1,'Suhanda Souwmpie','Closed','','test','test',1,'2021-06-13 16:59:42',NULL,NULL);

/*Table structure for table `trn_incoming_stock_02` */

DROP TABLE IF EXISTS `trn_incoming_stock_02`;

CREATE TABLE `trn_incoming_stock_02` (
  `is_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `is_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Incoming',
  `po_qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty PO',
  `price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(12,2) DEFAULT 0.00,
  `total` decimal(12,2) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`is_02_id`),
  KEY `is_id` (`is_02_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `trn_incoming_stock_02` */

insert  into `trn_incoming_stock_02`(`is_02_id`,`is_id`,`items_id`,`items_name`,`qty`,`po_qty`,`price`,`discount`,`total`,`remarks`) values 
(1,1,477,'Bulu Roll Besar (PCS)',2.0,2.0,10000.00,0.00,20000.00,''),
(2,1,478,'Bandul',2.0,2.0,7500.00,0.00,15000.00,''),
(3,1,479,'Busa 4 x 100 x 120',2.0,2.0,100000.00,0.00,200000.00,''),
(4,1,480,'Busa 7.5 x 84 x 210',2.0,2.0,275000.00,0.00,550000.00,''),
(5,1,481,'Siku Besi',2.0,2.0,8500.00,0.00,17000.00,''),
(6,1,482,'Engsel Dolpin',2.0,2.0,35000.00,0.00,70000.00,''),
(7,1,483,'Overfal',2.0,2.0,5000.00,0.00,10000.00,''),
(8,2,477,'Bulu Roll Besar (PCS)',2.0,2.0,20000.00,0.00,40000.00,''),
(9,2,478,'Bandul',2.0,2.0,17500.00,0.00,35000.00,''),
(10,2,479,'Busa 4 x 100 x 120',2.0,2.0,110000.00,0.00,220000.00,''),
(11,2,480,'Busa 7.5 x 84 x 210',2.0,2.0,285000.00,0.00,570000.00,''),
(12,2,481,'Siku Besi',2.0,2.0,18500.00,0.00,37000.00,''),
(13,2,482,'Engsel Dolpin',2.0,2.0,45000.00,0.00,90000.00,''),
(14,2,483,'Overfal',2.0,2.0,15000.00,0.00,30000.00,''),
(15,3,477,'Bulu Roll Besar (PCS)',8.0,8.0,11000.00,0.00,88000.00,''),
(16,3,478,'Bandul',8.0,8.0,8500.00,0.00,68000.00,''),
(17,3,479,'Busa 4 x 100 x 120',8.0,8.0,101000.00,0.00,808000.00,''),
(18,3,480,'Busa 7.5 x 84 x 210',8.0,8.0,276000.00,0.00,2208000.00,''),
(19,3,481,'Siku Besi',8.0,8.0,9500.00,0.00,76000.00,''),
(20,3,482,'Engsel Dolpin',8.0,8.0,36000.00,0.00,288000.00,''),
(21,3,483,'Overfal',8.0,8.0,2500.00,0.00,20000.00,''),
(22,4,477,'Bulu Roll Besar (PCS)',8.0,8.0,10000.00,0.00,80000.00,''),
(23,4,478,'Bandul',8.0,8.0,7500.00,0.00,60000.00,''),
(24,4,479,'Busa 4 x 100 x 120',8.0,8.0,100000.00,0.00,800000.00,''),
(25,4,480,'Busa 7.5 x 84 x 210',8.0,8.0,275000.00,0.00,2200000.00,''),
(26,4,481,'Siku Besi',8.0,8.0,8500.00,0.00,68000.00,''),
(27,4,482,'Engsel Dolpin',8.0,8.0,35000.00,0.00,280000.00,''),
(28,4,483,'Overfal',8.0,8.0,1500.00,0.00,12000.00,''),
(29,5,483,'Overfal',2.0,0.0,5000.00,0.00,10000.00,''),
(30,5,482,'Engsel Dolpin',2.0,0.0,35000.00,0.00,70000.00,''),
(31,6,477,'Bulu Roll Besar (PCS)',2.0,2.0,10000.00,0.00,20000.00,''),
(32,6,478,'Bandul',2.0,2.0,7500.00,0.00,15000.00,''),
(33,6,478,'Bandul',3.0,3.0,14166.67,0.00,42500.00,'');

/*Table structure for table `trn_incoming_stock_03` */

DROP TABLE IF EXISTS `trn_incoming_stock_03`;

CREATE TABLE `trn_incoming_stock_03` (
  `is_03_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Is 02',
  `is_id` int(11) DEFAULT NULL COMMENT 'Id Is',
  `cost_name` varchar(250) DEFAULT NULL COMMENT 'Nama biaya',
  `cost_value` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`is_03_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trn_incoming_stock_03` */

insert  into `trn_incoming_stock_03`(`is_03_id`,`is_id`,`cost_name`,`cost_value`) values 
(1,2,'Biaya 1',100000.0),
(2,2,'Biaya 2',40000.0),
(3,3,'Biaya 1',56000.0);

/*Table structure for table `trn_invoice_01` */

DROP TABLE IF EXISTS `trn_invoice_01`;

CREATE TABLE `trn_invoice_01` (
  `invoice_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `invoice_date` date DEFAULT NULL COMMENT 'Tanggal',
  `invoice_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen',
  `doc_ref` varchar(15) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `warehouse_name` varchar(55) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(10) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(25) DEFAULT NULL COMMENT 'Nama Customer',
  `total_owner_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Owner Price',
  `total_cust_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Cust Price',
  `total_gov_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Government Price',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Keterangan',
  `doc_status` varchar(15) DEFAULT NULL COMMENT 'New, Outstanding, Closed',
  `is_cancel` tinyint(1) DEFAULT 0 COMMENT 'Flag Pembatalan',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  UNIQUE KEY `sales_no` (`invoice_no`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  KEY `trn_sales_01_ibfk_1` (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_invoice_01` */

insert  into `trn_invoice_01`(`invoice_id`,`invoice_date`,`invoice_no`,`doc_ref`,`warehouse_id`,`warehouse_name`,`company_id`,`company_name`,`fc_id`,`fc_name`,`total_owner_price`,`total_cust_price`,`total_gov_price`,`remarks`,`doc_status`,`is_cancel`,`disc_value`,`disc_percent`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-06-14','IN2021060001','SL2021060001',1,'',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',0.00,113400.00,0.00,'Test','Closed',0,0.00,10.00,'1','2021-06-14 09:48:07',NULL,NULL),
(2,'2021-06-14','IN2021060002','SL2021060002',1,'',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',0.00,636000.00,0.00,'','Closed',0,50000.00,0.00,'1','2021-06-14 09:48:31',NULL,NULL),
(3,'2021-06-14','IN2021060003','SL2021060003',1,'',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',0.00,769500.00,0.00,'','Closed',0,0.00,10.00,'1','2021-06-14 09:48:48',NULL,NULL);

/*Table structure for table `trn_invoice_02` */

DROP TABLE IF EXISTS `trn_invoice_02`;

CREATE TABLE `trn_invoice_02` (
  `invoice_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `items_name` varchar(30) DEFAULT NULL COMMENT 'Nama Ikan',
  `owner_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Owner',
  `cust_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Customer',
  `gov_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Pemprov',
  `qty` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty Masuk',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `subtotal` decimal(14,2) DEFAULT 0.00 COMMENT 'Subtotal',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`invoice_02_id`),
  KEY `trn_sales_02_ibfk_5` (`items_id`),
  KEY `sales_id` (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `trn_invoice_02` */

insert  into `trn_invoice_02`(`invoice_02_id`,`invoice_id`,`items_id`,`items_name`,`owner_price`,`cust_price`,`gov_price`,`qty`,`disc_value`,`disc_percent`,`subtotal`,`remarks`) values 
(1,1,482,'Engsel Dolpin',0.00,40000.00,0.00,3.0,0.00,10.00,120000.00,''),
(2,1,483,'Overfal',0.00,8000.00,0.00,3.0,2000.00,0.00,24000.00,''),
(3,2,480,'Busa 7.5 x 84 x 210',0.00,325000.00,0.00,2.0,0.00,0.00,650000.00,''),
(4,2,481,'Siku Besi',0.00,12000.00,0.00,3.0,0.00,0.00,36000.00,''),
(5,3,479,'Busa 4 x 100 x 120',0.00,150000.00,0.00,2.0,0.00,10.00,300000.00,''),
(6,3,480,'Busa 7.5 x 84 x 210',0.00,325000.00,0.00,2.0,0.00,10.00,650000.00,'');

/*Table structure for table `trn_journal` */

DROP TABLE IF EXISTS `trn_journal`;

CREATE TABLE `trn_journal` (
  `journal_id` varchar(10) NOT NULL COMMENT 'Id Data',
  `trn_code` varchar(10) DEFAULT NULL COMMENT 'Kode Dokumen (IS, OS, PO,..)',
  `journal_date` date DEFAULT NULL COMMENT 'Tanggal Posting',
  `year_period` int(4) DEFAULT NULL COMMENT 'Tahun',
  `month_period` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `journal_dest` varchar(50) DEFAULT NULL COMMENT 'Gudang/ Project',
  `dest_id` int(10) NOT NULL COMMENT 'Id Project/ Id Gudang',
  `trn_id` varchar(16) DEFAULT NULL COMMENT 'Id Dok (ISS2020040001,..)',
  `db_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Debet',
  `db_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Debet',
  `db_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Debet',
  `db_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Debet',
  `db_info` varchar(100) DEFAULT NULL,
  `cr_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Kredit',
  `cr_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Kredit',
  `cr_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Kredit',
  `cr_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Kredit',
  `cr_info` varchar(100) DEFAULT NULL,
  `pic_posting` int(11) DEFAULT NULL,
  `date_posting` datetime DEFAULT NULL,
  `journal_type` int(1) DEFAULT NULL COMMENT '1 = Auto Journal, 2 = Manual Journal, 3 = Jounal Cash/Bank',
  PRIMARY KEY (`journal_id`),
  KEY `db_coa_id` (`db_coa_id`),
  KEY `cr_coa_id` (`cr_coa_id`),
  CONSTRAINT `trn_journal_ibfk_1` FOREIGN KEY (`db_coa_id`) REFERENCES `db_master_shp`.`mst_coa` (`coa_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_journal_ibfk_2` FOREIGN KEY (`cr_coa_id`) REFERENCES `db_master_shp`.`mst_coa` (`coa_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_journal` */

insert  into `trn_journal`(`journal_id`,`trn_code`,`journal_date`,`year_period`,`month_period`,`journal_dest`,`dest_id`,`trn_id`,`db_coa_id`,`db_coa_detail`,`db_coa_name`,`db_value`,`db_info`,`cr_coa_id`,`cr_coa_detail`,`cr_coa_name`,`cr_value`,`cr_info`,`pic_posting`,`date_posting`,`journal_type`) values 
('2021060001','MJ','2021-06-17',2021,'06','',0,'0',184,'01.02.02','Mobil',35000000.00,'Test',176,'01.01.01','Kas Levondi',35000000.00,'Test ',1,'2021-06-17 08:48:33',2),
('2021060002','BM','2021-06-17',2021,'06','',0,'0',176,'01.01.01','Kas Levondi',2000000.00,'Test 2',178,'01.01.03','Piutang Usaha',2000000.00,'Test 2',1,'2021-06-17 08:53:21',3),
('2021060003','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',15000.00,'Bandul',176,'01.01.01','Kas Levondi',15000.00,'Bandul',1,'2021-06-17 09:49:47',1),
('2021060004','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',550000.00,'Busa 7.5 x 84 x 210',176,'01.01.01','Kas Levondi',550000.00,'Busa 7.5 x 84 x 210',1,'2021-06-17 09:49:47',1),
('2021060005','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',70000.00,'Engsel Dolpin',176,'01.01.01','Kas Levondi',70000.00,'Engsel Dolpin',1,'2021-06-17 09:49:47',1),
('2021060006','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',20000.00,'Bulu Roll Besar (PCS)',176,'01.01.01','Kas Levondi',20000.00,'Bulu Roll Besar (PCS)',1,'2021-06-17 09:49:47',1),
('2021060007','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',200000.00,'Busa 4 x 100 x 120',176,'01.01.01','Kas Levondi',200000.00,'Busa 4 x 100 x 120',1,'2021-06-17 09:49:47',1),
('2021060008','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',17000.00,'Siku Besi',176,'01.01.01','Kas Levondi',17000.00,'Siku Besi',1,'2021-06-17 09:49:47',1),
('2021060009','IS','2021-06-30',2021,'06','',0,'ISS2021060001',180,'01.01.05','Persediaan',10000.00,'Overfal',176,'01.01.01','Kas Levondi',10000.00,'Overfal',1,'2021-06-17 09:49:47',1),
('2021060010','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',40000.00,'Bulu Roll Besar (PCS)',176,'01.01.01','Kas Levondi',40000.00,'Bulu Roll Besar (PCS)',1,'2021-06-17 09:49:47',1),
('2021060011','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',220000.00,'Busa 4 x 100 x 120',176,'01.01.01','Kas Levondi',220000.00,'Busa 4 x 100 x 120',1,'2021-06-17 09:49:47',1),
('2021060012','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',37000.00,'Siku Besi',176,'01.01.01','Kas Levondi',37000.00,'Siku Besi',1,'2021-06-17 09:49:47',1),
('2021060013','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',30000.00,'Overfal',176,'01.01.01','Kas Levondi',30000.00,'Overfal',1,'2021-06-17 09:49:47',1),
('2021060014','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',60000.00,'Bandul',176,'01.01.01','Kas Levondi',60000.00,'Bandul',1,'2021-06-17 09:49:47',1),
('2021060015','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',2200000.00,'Busa 7.5 x 84 x 210',176,'01.01.01','Kas Levondi',2200000.00,'Busa 7.5 x 84 x 210',1,'2021-06-17 09:49:47',1),
('2021060016','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',280000.00,'Engsel Dolpin',176,'01.01.01','Kas Levondi',280000.00,'Engsel Dolpin',1,'2021-06-17 09:49:47',1),
('2021060017','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',35000.00,'Bandul',176,'01.01.01','Kas Levondi',35000.00,'Bandul',1,'2021-06-17 09:49:47',1),
('2021060018','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',570000.00,'Busa 7.5 x 84 x 210',176,'01.01.01','Kas Levondi',570000.00,'Busa 7.5 x 84 x 210',1,'2021-06-17 09:49:47',1),
('2021060019','IS','2021-06-30',2021,'06','',0,'ISS2021060002',180,'01.01.05','Persediaan',90000.00,'Engsel Dolpin',176,'01.01.01','Kas Levondi',90000.00,'Engsel Dolpin',1,'2021-06-17 09:49:47',1),
('2021060020','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',80000.00,'Bulu Roll Besar (PCS)',176,'01.01.01','Kas Levondi',80000.00,'Bulu Roll Besar (PCS)',1,'2021-06-17 09:49:47',1),
('2021060021','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',800000.00,'Busa 4 x 100 x 120',176,'01.01.01','Kas Levondi',800000.00,'Busa 4 x 100 x 120',1,'2021-06-17 09:49:47',1),
('2021060022','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',68000.00,'Siku Besi',176,'01.01.01','Kas Levondi',68000.00,'Siku Besi',1,'2021-06-17 09:49:47',1),
('2021060023','IS','2021-06-30',2021,'06','',0,'ISS2021060004',180,'01.01.05','Persediaan',12000.00,'Overfal',176,'01.01.01','Kas Levondi',12000.00,'Overfal',1,'2021-06-17 09:49:47',1),
('2021060024','IS','2021-06-30',2021,'06','',0,'IST2021060005',180,'01.01.05','Persediaan',10000.00,'Overfal',176,'01.01.01','Kas Levondi',10000.00,'Overfal',1,'2021-06-17 09:49:47',1),
('2021060025','IS','2021-06-30',2021,'06','',0,'IST2021060005',180,'01.01.05','Persediaan',70000.00,'Engsel Dolpin',176,'01.01.01','Kas Levondi',70000.00,'Engsel Dolpin',1,'2021-06-17 09:49:47',1),
('2021060026','DO','2021-06-30',2021,'06','',0,'DO2021060003',194,'07','Harga Pokok Penjualan',650000.00,'Busa 7.5 x 84 x 210',180,'01.01.05','Persediaan',650000.00,'Busa 7.5 x 84 x 210',1,'2021-06-17 11:03:19',1),
('2021060027','DO','2021-06-30',2021,'06','',0,'DO2021060003',194,'07','Harga Pokok Penjualan',300000.00,'Busa 4 x 100 x 120',180,'01.01.05','Persediaan',300000.00,'Busa 4 x 100 x 120',1,'2021-06-17 11:03:19',1),
('2021060028','DO','2021-06-30',2021,'06','',0,'DO2021060001',194,'07','Harga Pokok Penjualan',24000.00,'Overfal',180,'01.01.05','Persediaan',24000.00,'Overfal',1,'2021-06-17 11:03:19',1),
('2021060029','DO','2021-06-30',2021,'06','',0,'DO2021060001',194,'07','Harga Pokok Penjualan',120000.00,'Engsel Dolpin',180,'01.01.05','Persediaan',120000.00,'Engsel Dolpin',1,'2021-06-17 11:03:19',1),
('2021060030','DO','2021-06-30',2021,'06','',0,'DO2021060002',194,'07','Harga Pokok Penjualan',650000.00,'Busa 7.5 x 84 x 210',180,'01.01.05','Persediaan',650000.00,'Busa 7.5 x 84 x 210',1,'2021-06-17 11:03:19',1),
('2021060031','DO','2021-06-30',2021,'06','',0,'DO2021060002',194,'07','Harga Pokok Penjualan',36000.00,'Siku Besi',180,'01.01.05','Persediaan',36000.00,'Siku Besi',1,'2021-06-17 11:03:19',1);

/*Table structure for table `trn_outgoing_stock_01` */

DROP TABLE IF EXISTS `trn_outgoing_stock_01`;

CREATE TABLE `trn_outgoing_stock_01` (
  `os_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id OS',
  `os_date` date DEFAULT NULL COMMENT 'Tanggal OS',
  `os_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen (CCCCYYYYMMNNNN) ex : LMOS2018070001',
  `company_id` int(11) DEFAULT NULL COMMENT 'Company Id',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Company Name',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Dari mana item itu digunakan',
  `dest_wh_id` int(11) DEFAULT NULL COMMENT 'Tujuan Item',
  `project_id` int(11) DEFAULT NULL,
  `project_name` varchar(55) DEFAULT NULL,
  `dept` int(11) DEFAULT NULL COMMENT 'Departemen Yang Menggunakan Barang',
  `os_kind` varchar(20) DEFAULT NULL COMMENT '1= Transfer, 2= write off, 3= Retur, 4=Others 5=stock Use',
  `os_status` varchar(20) DEFAULT NULL COMMENT '1=Closed (selesai), 2=Outstanding (belum ada tarikan), 3=Void',
  `os_requester` varchar(50) DEFAULT NULL COMMENT 'Requester',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  `pic_input` int(11) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`os_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `trn_outgoing_stock_01` */

insert  into `trn_outgoing_stock_01`(`os_id`,`os_date`,`os_no`,`company_id`,`company_name`,`warehouse_id`,`dest_wh_id`,`project_id`,`project_name`,`dept`,`os_kind`,`os_status`,`os_requester`,`remarks`,`pic_input`,`input_date`,`pic_edit`,`edit_date`) values 
(1,'2021-06-16','OSS2021060001',1,'Suhanda Souwmpie',5,4,0,'',NULL,'Transfer','Closed','bambang','',1,'2021-06-13 17:15:49',NULL,NULL),
(2,'2021-06-13','OSS2021060002',1,'Suhanda Souwmpie',5,4,0,'',NULL,'Transfer','Closed','Test','',1,'2021-06-13 17:17:13',NULL,NULL);

/*Table structure for table `trn_outgoing_stock_02` */

DROP TABLE IF EXISTS `trn_outgoing_stock_02`;

CREATE TABLE `trn_outgoing_stock_02` (
  `os_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Os 02',
  `os_id` int(11) DEFAULT NULL COMMENT 'Id Os',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama Items',
  `qty` decimal(10,1) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Items',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Sub Total',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`os_02_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trn_outgoing_stock_02` */

insert  into `trn_outgoing_stock_02`(`os_02_id`,`os_id`,`items_id`,`items_name`,`qty`,`price`,`total`,`remarks`) values 
(1,1,477,'Bulu Roll Besar (PCS)',2.0,10000.00,20000.00,''),
(2,1,478,'Bandul',2.0,7500.00,15000.00,''),
(3,2,478,'Bandul',3.0,14166.67,42500.00,'');

/*Table structure for table `trn_po_01` */

DROP TABLE IF EXISTS `trn_po_01`;

CREATE TABLE `trn_po_01` (
  `po_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO',
  `po_no` varchar(15) NOT NULL COMMENT 'Nomor PO',
  `pr_id` int(11) NOT NULL COMMENT 'Id PR',
  `pr_no` varchar(15) NOT NULL COMMENT 'Nomor PR',
  `po_date` date NOT NULL COMMENT 'Tanggal PO',
  `requester` varchar(30) DEFAULT NULL COMMENT 'User Request',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `project_name` varchar(50) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(50) DEFAULT NULL,
  `delivery_address` text DEFAULT NULL COMMENT 'Alamat Pengiriman',
  `po_info` varchar(50) DEFAULT NULL COMMENT 'Keterangan Pr',
  `currency_id` int(3) DEFAULT NULL COMMENT 'mata uang Po',
  `po_status` varchar(20) DEFAULT NULL COMMENT 'Status PO (closed, outstanding, pending)',
  `payment_status` varchar(20) DEFAULT NULL COMMENT 'Status Pembayaran',
  `transport_fee` decimal(12,2) DEFAULT 0.00 COMMENT 'Biaya Transport (Non PPN)',
  `ppn_percent` decimal(12,2) DEFAULT 0.00 COMMENT 'PPN Persen',
  `ppn_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai PPN',
  `pph` int(1) DEFAULT NULL COMMENT '1 = Menggunakan PPH 0 = Tidak ada PPH',
  `pph_value` decimal(12,2) DEFAULT NULL COMMENT 'Value PPH',
  `pbbkb` int(1) DEFAULT NULL COMMENT '1 = Menggunakan pbbkb 0 = Tidak ada pbbkb',
  `pbbkb_value` decimal(12,2) DEFAULT NULL COMMENT 'Value Pbbkb',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Diskon Persen',
  `disc_val` decimal(12,2) DEFAULT 0.00 COMMENT 'Diskon Nilai',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `reamaining` decimal(12,2) DEFAULT 0.00 COMMENT 'Sisa Pembayaran',
  `top` varchar(30) DEFAULT NULL,
  `no_print` int(2) DEFAULT 0 COMMENT 'Berapa Kali Po Dicetak',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`po_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_01` */

insert  into `trn_po_01`(`po_id`,`po_no`,`pr_id`,`pr_no`,`po_date`,`requester`,`project_id`,`project_name`,`company_id`,`company_name`,`supplier_id`,`supplier_name`,`delivery_address`,`po_info`,`currency_id`,`po_status`,`payment_status`,`transport_fee`,`ppn_percent`,`ppn_value`,`pph`,`pph_value`,`pbbkb`,`pbbkb_value`,`disc_percent`,`disc_val`,`total`,`reamaining`,`top`,`no_print`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'POS2021060001',0,'','2021-06-02',NULL,0,'',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','Test','Test 1',NULL,'Reject','Process',0.00,0.00,0.00,NULL,NULL,NULL,NULL,0.0,2000.00,680000.00,0.00,NULL,0,1,'2021-06-13 11:21:15',NULL,NULL),
(2,'POS2021060002',0,'','2021-06-05',NULL,0,'',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','','Test 1',NULL,'Reject','Closed',0.00,0.00,0.00,NULL,NULL,NULL,NULL,0.0,32000.00,450000.00,0.00,NULL,0,1,'2021-06-13 11:32:07',1,'2021-06-13 11:36:37'),
(3,'POS2021060003',0,'','2021-06-05',NULL,0,'',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','','Test 3',NULL,'Closed','Closed',0.00,0.00,0.00,NULL,NULL,NULL,NULL,0.0,28000.00,3400000.00,0.00,NULL,0,1,'2021-06-13 11:32:38',1,'2021-06-13 11:37:25'),
(4,'POS2021060004',0,'','2021-06-05',NULL,0,'',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','','Test 4',NULL,'Closed','Closed',0.00,0.00,0.00,NULL,NULL,NULL,NULL,0.0,0.00,882000.00,0.00,NULL,0,1,'2021-06-13 11:33:02',1,'2021-06-13 11:36:37'),
(5,'POS2021060005',0,'','2021-06-06',NULL,0,'',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','Test 1','Test 1',NULL,'Closed','Process',0.00,0.00,0.00,NULL,NULL,NULL,NULL,0.0,0.00,482000.00,0.00,NULL,0,1,'2021-06-13 14:38:00',NULL,NULL);

/*Table structure for table `trn_po_02` */

DROP TABLE IF EXISTS `trn_po_02`;

CREATE TABLE `trn_po_02` (
  `po_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO 02',
  `po_id` int(11) NOT NULL COMMENT 'Id PO 01',
  `items_id` int(11) NOT NULL COMMENT 'Id Items',
  `items_name` varchar(150) DEFAULT NULL COMMENT 'Nama Items',
  `qty` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty Items',
  `price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Items',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Dikon Persen',
  `disc_val` decimal(12,2) DEFAULT 0.00 COMMENT 'Diskon Nilai',
  `exchange_rate` decimal(10,2) DEFAULT 0.00 COMMENT 'nilai tukar',
  `subtotal` decimal(12,2) DEFAULT 0.00 COMMENT '(Harga Items x Qty Items)',
  `remarks` varchar(50) DEFAULT NULL COMMENT 'Info',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`po_02_id`),
  KEY `po_id` (`po_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_02` */

insert  into `trn_po_02`(`po_02_id`,`po_id`,`items_id`,`items_name`,`qty`,`price`,`disc_percent`,`disc_val`,`exchange_rate`,`subtotal`,`remarks`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,1,477,'Bulu Roll Besar (PCS)',2.0,10000.00,0.0,0.00,0.00,20000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(2,1,478,'Bandul',2.0,7500.00,0.0,0.00,0.00,15000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(3,1,479,'Busa 4 x 100 x 120',2.0,100000.00,0.0,0.00,0.00,200000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(4,1,480,'Busa 7.5 x 84 x 210',2.0,275000.00,0.0,0.00,0.00,550000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(5,1,481,'Siku Besi',2.0,8500.00,0.0,0.00,0.00,17000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(6,1,482,'Engsel Dolpin',2.0,35000.00,0.0,0.00,0.00,70000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(7,1,483,'Overfal',2.0,5000.00,0.0,2000.00,0.00,8000.00,'',1,1,'2021-06-13 11:21:15',NULL,NULL),
(8,2,477,'Bulu Roll Besar (PCS)',2.0,10000.00,0.0,0.00,0.00,20000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(9,2,478,'Bandul',2.0,7500.00,0.0,0.00,0.00,15000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(10,2,479,'Busa 4 x 100 x 120',2.0,100000.00,0.0,0.00,0.00,200000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(11,2,480,'Busa 7.5 x 84 x 210',2.0,275000.00,0.0,0.00,0.00,550000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(12,2,481,'Siku Besi',2.0,8500.00,0.0,0.00,0.00,17000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(13,2,482,'Engsel Dolpin',2.0,35000.00,0.0,0.00,0.00,70000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(14,2,483,'Overfal',2.0,5000.00,0.0,32000.00,0.00,-22000.00,'',1,1,'2021-06-13 11:32:07',NULL,NULL),
(15,3,477,'Bulu Roll Besar (PCS)',8.0,10000.00,0.0,0.00,0.00,80000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(16,3,478,'Bandul',8.0,7500.00,0.0,0.00,0.00,60000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(17,3,479,'Busa 4 x 100 x 120',8.0,100000.00,0.0,0.00,0.00,800000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(18,3,480,'Busa 7.5 x 84 x 210',8.0,275000.00,0.0,0.00,0.00,2200000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(19,3,481,'Siku Besi',8.0,8500.00,0.0,0.00,0.00,68000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(20,3,482,'Engsel Dolpin',8.0,35000.00,0.0,0.00,0.00,280000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(21,3,483,'Overfal',8.0,5000.00,0.0,28000.00,0.00,12000.00,'',1,1,'2021-06-13 11:32:38',NULL,NULL),
(22,4,477,'Bulu Roll Besar (PCS)',2.0,10000.00,0.0,0.00,0.00,20000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(23,4,478,'Bandul',2.0,7500.00,0.0,0.00,0.00,15000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(24,4,479,'Busa 4 x 100 x 120',2.0,100000.00,0.0,0.00,0.00,200000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(25,4,480,'Busa 7.5 x 84 x 210',2.0,275000.00,0.0,0.00,0.00,550000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(26,4,481,'Siku Besi',2.0,8500.00,0.0,0.00,0.00,17000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(27,4,482,'Engsel Dolpin',2.0,35000.00,0.0,0.00,0.00,70000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(28,4,483,'Overfal',2.0,5000.00,0.0,0.00,0.00,10000.00,'',1,1,'2021-06-13 11:33:02',NULL,NULL),
(29,5,477,'Bulu Roll Besar (PCS)',2.0,10000.00,0.0,0.00,0.00,20000.00,'',1,1,'2021-06-13 14:37:59',NULL,NULL),
(30,5,478,'Bandul',2.0,7500.00,0.0,0.00,0.00,15000.00,'',1,1,'2021-06-13 14:38:00',NULL,NULL),
(31,5,479,'Busa 4 x 100 x 120',2.0,100000.00,0.0,0.00,0.00,200000.00,'',1,1,'2021-06-13 14:38:00',NULL,NULL),
(32,5,480,'Busa 7.5 x 84 x 210',2.0,275000.00,0.0,0.00,0.00,550000.00,'',1,1,'2021-06-13 14:38:00',NULL,NULL),
(33,5,481,'Siku Besi',2.0,8500.00,0.0,0.00,0.00,17000.00,'',1,1,'2021-06-13 14:38:00',NULL,NULL),
(34,5,482,'Engsel Dolpin',2.0,35000.00,0.0,0.00,0.00,70000.00,'',1,1,'2021-06-13 14:38:00',NULL,NULL),
(35,5,483,'Overfal',2.0,5000.00,0.0,0.00,0.00,10000.00,'',1,1,'2021-06-13 14:38:00',NULL,NULL);

/*Table structure for table `trn_po_dp` */

DROP TABLE IF EXISTS `trn_po_dp`;

CREATE TABLE `trn_po_dp` (
  `pd_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(30) DEFAULT NULL COMMENT 'Nama Supplier',
  `pd_no` varchar(15) DEFAULT NULL COMMENT 'No Dokumen',
  `pd_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `po_id` int(11) DEFAULT NULL COMMENT 'Id PO',
  `po_no` varchar(15) DEFAULT NULL COMMENT 'No PO',
  `amount` decimal(10,2) DEFAULT 0.00 COMMENT 'Nilai DP',
  `remarks` varchar(180) DEFAULT NULL COMMENT 'Keterangan Singkat',
  `is_active` tinyint(1) DEFAULT 1 COMMENT 'Status Dokumen',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pd_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `po_id` (`po_id`),
  CONSTRAINT `trn_po_dp_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `db_master_shp`.`mst_supplier` (`supplier_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_dp` */

insert  into `trn_po_dp`(`pd_id`,`supplier_id`,`supplier_name`,`pd_no`,`pd_date`,`po_id`,`po_no`,`amount`,`remarks`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,106,'Terus Jaya (Chandra)','PDS2021060001','2021-06-02',5,'POS2021060005',200000.00,'',1,1,'2021-06-13 11:18:47',NULL,NULL),
(2,106,'Terus Jaya (Chandra)','PDS2021060002','2021-06-05',3,'POS2021060003',100000.00,'',1,1,'2021-06-13 11:18:58',NULL,NULL),
(3,106,'Terus Jaya (Chandra)','PDS2021060003','2021-06-02',5,'POS2021060005',200000.00,'',1,1,'2021-06-13 11:30:49',NULL,NULL);

/*Table structure for table `trn_po_payment_01` */

DROP TABLE IF EXISTS `trn_po_payment_01`;

CREATE TABLE `trn_po_payment_01` (
  `pp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO Payment',
  `pp_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO Payment',
  `pp_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(50) DEFAULT NULL,
  `payment_type` varchar(30) DEFAULT '0' COMMENT 'Jenis Pembayaran',
  `grand_total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total Keseluruhan',
  `is_active` int(2) DEFAULT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_payment_01` */

insert  into `trn_po_payment_01`(`pp_id`,`pp_no`,`pp_date`,`company_id`,`company_name`,`supplier_id`,`supplier_name`,`payment_type`,`grand_total`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'PMS2021060001','2021-06-06',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','Transfer',1332000.00,1,1,'2021-06-13 11:36:38',1,'2021-06-13 11:36:38'),
(2,'PMS2021060002','2021-06-07',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','Cash',2000000.00,1,1,'2021-06-13 11:37:09',1,'2021-06-13 11:37:09'),
(3,'PMS2021060003','2021-06-08',1,'Suhanda Souwmpie',106,'Terus Jaya (Chandra)','Cash',1400000.00,1,1,'2021-06-13 11:37:25',1,'2021-06-13 11:37:25');

/*Table structure for table `trn_po_payment_02` */

DROP TABLE IF EXISTS `trn_po_payment_02`;

CREATE TABLE `trn_po_payment_02` (
  `pp_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '⁯Id Detail PO Generate',
  `pp_id` int(11) DEFAULT NULL COMMENT 'Id Header PO Generate',
  `po_id` int(11) DEFAULT NULL COMMENT 'Id PO',
  `po_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO',
  `amount` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai PO',
  `disc_percent` decimal(4,1) DEFAULT 0.0,
  `disc_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Diskon',
  `total` decimal(12,2) DEFAULT 0.00,
  `is_active` int(2) DEFAULT NULL,
  PRIMARY KEY (`pp_02_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_payment_02` */

insert  into `trn_po_payment_02`(`pp_02_id`,`pp_id`,`po_id`,`po_no`,`amount`,`disc_percent`,`disc_value`,`total`,`is_active`) values 
(1,1,2,'POS2021060002',450000.00,0.0,0.00,450000.00,1),
(2,1,4,'POS2021060004',882000.00,0.0,0.00,882000.00,1),
(3,2,3,'POS2021060003',2000000.00,0.0,0.00,2000000.00,1),
(4,3,3,'POS2021060003',1400000.00,0.0,0.00,1400000.00,1);

/*Table structure for table `trn_pr_01` */

DROP TABLE IF EXISTS `trn_pr_01`;

CREATE TABLE `trn_pr_01` (
  `pr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Pr',
  `po_no` varchar(15) DEFAULT NULL COMMENT 'Ref PO',
  `pr_no` varchar(15) NOT NULL COMMENT 'Nomor Pr',
  `pr_date` date NOT NULL COMMENT 'Tanggal Pr',
  `requester` varchar(30) DEFAULT NULL COMMENT 'User Request',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `project_name` varchar(50) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(50) DEFAULT NULL,
  `file_quotation` tinyint(2) DEFAULT NULL COMMENT '1 = Terdapat file 0 = Tidak ada File',
  `delivery_address` varchar(200) DEFAULT NULL COMMENT 'Alamat Pengiriman',
  `pr_info` varchar(50) DEFAULT NULL COMMENT 'Keterangan Pr',
  `currency_id` int(3) DEFAULT NULL COMMENT 'mata uang pr',
  `pr_status` varchar(20) DEFAULT NULL COMMENT 'Status Pr (New, Process, Pending, Terminated, Close)',
  `transport_fee` decimal(12,2) DEFAULT 0.00 COMMENT 'Biaya Transport (Non PPN)',
  `ppn_percent` decimal(12,2) DEFAULT 0.00 COMMENT 'PPN Persen',
  `ppn_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai PPN',
  `top` varchar(30) DEFAULT NULL,
  `pph` int(1) DEFAULT NULL COMMENT '1 = Menggunakan PPH 0 = Tidak ada PPH',
  `pph_value` decimal(12,2) DEFAULT NULL COMMENT 'Value PPH',
  `pbbkb` int(1) DEFAULT NULL COMMENT '1 = Menggunakan pbbkb 0 = Tidak ada pbbkb',
  `pbbkb_value` decimal(12,2) DEFAULT NULL COMMENT 'Value Pbbkb',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  `approve_level` int(1) DEFAULT 0 COMMENT 'Approval Level Untuk PR 0=dept suport, 1=finance, 2=Cost Control, 3=BOD, 4=presdir',
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `trn_pr_01` */

insert  into `trn_pr_01`(`pr_id`,`po_no`,`pr_no`,`pr_date`,`requester`,`project_id`,`project_name`,`company_id`,`company_name`,`supplier_id`,`supplier_name`,`file_quotation`,`delivery_address`,`pr_info`,`currency_id`,`pr_status`,`transport_fee`,`ppn_percent`,`ppn_value`,`top`,`pph`,`pph_value`,`pbbkb`,`pbbkb_value`,`total`,`pic_input`,`input_time`,`pic_edit`,`edit_time`,`approve_level`) values 
(1,'POS2021060005','PRS2021060001','2021-06-01','Test 1',NULL,NULL,1,'Suhanda Souwmpie',NULL,NULL,NULL,NULL,'Test 1',NULL,'Close',0.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,0.00,1,'2021-06-13 11:08:26',1,'2021-06-13 11:08:52',0),
(2,'POS2021060003','PRS2021060002','2021-06-02','Test 2',NULL,NULL,1,'Suhanda Souwmpie',NULL,NULL,NULL,NULL,'Test 2',NULL,'Close',0.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,0.00,1,'2021-06-13 11:10:14',NULL,NULL,0),
(3,'POS2021060003','PRS2021060003','2021-06-02','Test 3',NULL,NULL,1,'Suhanda Souwmpie',NULL,NULL,NULL,NULL,'Test 3',NULL,'Close',0.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,0.00,1,'2021-06-13 11:17:06',NULL,NULL,0),
(4,'POS2021060004','PRS2021060004','2021-06-03','Test 4',NULL,NULL,1,'Suhanda Souwmpie',NULL,NULL,NULL,NULL,'Test 4',NULL,'Close',0.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,0.00,1,'2021-06-13 11:25:27',NULL,NULL,0);

/*Table structure for table `trn_pr_02` */

DROP TABLE IF EXISTS `trn_pr_02`;

CREATE TABLE `trn_pr_02` (
  `pr_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Pr 02',
  `pr_id` int(11) NOT NULL COMMENT 'Id Pr 01',
  `items_id` int(11) NOT NULL COMMENT 'Id Items',
  `items_name` varchar(150) DEFAULT NULL COMMENT 'Nama Items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Items',
  `price` decimal(12,2) DEFAULT NULL COMMENT 'Harga Items',
  `exchange_rate` decimal(10,2) DEFAULT NULL COMMENT 'nilai tukar',
  `subtotal` decimal(12,2) DEFAULT NULL COMMENT '(Harga Items x Qty Items)',
  `remarks` varchar(50) DEFAULT NULL COMMENT 'Info',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pr_02_id`),
  KEY `pr_id` (`pr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `trn_pr_02` */

insert  into `trn_pr_02`(`pr_02_id`,`pr_id`,`items_id`,`items_name`,`qty`,`price`,`exchange_rate`,`subtotal`,`remarks`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(7,1,483,'Overfal',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(8,1,482,'Engsel Dolpin',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(9,1,481,'Siku Besi',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(10,1,480,'Busa 7.5 x 84 x 210',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(11,1,479,'Busa 4 x 100 x 120',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(12,1,478,'Bandul',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(13,1,477,'Bulu Roll Besar (PCS)',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:08:51',NULL,NULL),
(14,2,483,'Overfal',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(15,2,482,'Engsel Dolpin',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(16,2,481,'Siku Besi',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(17,2,480,'Busa 7.5 x 84 x 210',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(18,2,479,'Busa 4 x 100 x 120',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(19,2,478,'Bandul',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(20,2,477,'Bulu Roll Besar (PCS)',3.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:10:14',NULL,NULL),
(21,3,483,'Overfal',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(22,3,482,'Engsel Dolpin',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(23,3,481,'Siku Besi',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(24,3,480,'Busa 7.5 x 84 x 210',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(25,3,479,'Busa 4 x 100 x 120',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(26,3,478,'Bandul',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(27,3,477,'Bulu Roll Besar (PCS)',5.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:17:06',NULL,NULL),
(28,4,483,'Overfal',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL),
(29,4,482,'Engsel Dolpin',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL),
(30,4,481,'Siku Besi',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL),
(31,4,480,'Busa 7.5 x 84 x 210',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL),
(32,4,479,'Busa 4 x 100 x 120',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL),
(33,4,478,'Bandul',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL),
(34,4,477,'Bulu Roll Besar (PCS)',2.0,NULL,NULL,NULL,'',1,1,'2021-06-13 11:25:27',NULL,NULL);

/*Table structure for table `trn_sales_01` */

DROP TABLE IF EXISTS `trn_sales_01`;

CREATE TABLE `trn_sales_01` (
  `sales_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `sales_date` date DEFAULT NULL COMMENT 'Tanggal',
  `sales_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen',
  `warehouse_id` int(11) DEFAULT NULL,
  `warehouse_name` varchar(55) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(10) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(25) DEFAULT NULL COMMENT 'Nama Customer',
  `total_owner_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Owner Price',
  `total_cust_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Cust Price',
  `total_gov_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Government Price',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Keterangan',
  `doc_status` varchar(15) DEFAULT NULL COMMENT 'New, Outstanding, Closed',
  `is_cancel` tinyint(1) DEFAULT 0 COMMENT 'Flag Pembatalan',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`sales_id`),
  UNIQUE KEY `sales_no` (`sales_no`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  KEY `trn_sales_01_ibfk_1` (`warehouse_id`),
  CONSTRAINT `trn_sales_01_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_01_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `db_bumbu_master`.`mst_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_01_ibfk_3` FOREIGN KEY (`fc_id`) REFERENCES `db_bumbu_master`.`mst_customer` (`fc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_01` */

insert  into `trn_sales_01`(`sales_id`,`sales_date`,`sales_no`,`warehouse_id`,`warehouse_name`,`company_id`,`company_name`,`fc_id`,`fc_name`,`total_owner_price`,`total_cust_price`,`total_gov_price`,`remarks`,`doc_status`,`is_cancel`,`disc_value`,`disc_percent`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-06-15','SL2021060001',1,'',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',0.00,144000.00,0.00,'Test','Close',0,0.00,10.00,'1','2021-06-14 08:31:40',NULL,NULL),
(2,'2021-06-17','SL2021060002',1,'',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',0.00,686000.00,0.00,'','Close',0,50000.00,0.00,'1','2021-06-14 08:32:16',NULL,NULL),
(3,'2021-06-14','SL2021060003',1,'',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',0.00,950000.00,0.00,'','Close',0,0.00,10.00,'1','2021-06-14 09:33:29',NULL,NULL),
(4,'2021-06-17','SL2021060004',1,'',1,'Suhanda Souwmpie',2,'PT BALI INDAH (HARTONO)',0.00,1900000.00,0.00,'','Reject',0,300000.00,0.00,'1','2021-06-15 15:55:24','1','2021-06-15 16:00:25');

/*Table structure for table `trn_sales_02` */

DROP TABLE IF EXISTS `trn_sales_02`;

CREATE TABLE `trn_sales_02` (
  `sales_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `items_name` varchar(30) DEFAULT NULL COMMENT 'Nama Ikan',
  `owner_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Owner',
  `cust_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Customer',
  `gov_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Pemprov',
  `qty` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty Masuk',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `subtotal` decimal(14,2) DEFAULT 0.00 COMMENT 'Subtotal',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`sales_02_id`),
  KEY `sales_id` (`sales_id`),
  KEY `trn_sales_02_ibfk_5` (`items_id`),
  CONSTRAINT `trn_sales_02_ibfk_4` FOREIGN KEY (`sales_id`) REFERENCES `trn_sales_01` (`sales_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_02_ibfk_5` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_02` */

insert  into `trn_sales_02`(`sales_02_id`,`sales_id`,`items_id`,`items_name`,`owner_price`,`cust_price`,`gov_price`,`qty`,`disc_value`,`disc_percent`,`subtotal`,`remarks`) values 
(1,1,483,'Overfal',0.00,8000.00,0.00,3.0,2000.00,0.00,24000.00,''),
(2,1,482,'Engsel Dolpin',0.00,40000.00,0.00,3.0,0.00,10.00,120000.00,''),
(3,2,481,'Siku Besi',0.00,12000.00,0.00,3.0,0.00,0.00,36000.00,''),
(4,2,480,'Busa 7.5 x 84 x 210',0.00,325000.00,0.00,2.0,0.00,0.00,650000.00,''),
(5,3,479,'Busa 4 x 100 x 120',0.00,150000.00,0.00,2.0,0.00,10.00,300000.00,''),
(6,3,480,'Busa 7.5 x 84 x 210',0.00,325000.00,0.00,2.0,0.00,10.00,650000.00,''),
(7,4,479,'Busa 4 x 100 x 120',0.00,150000.00,0.00,4.0,0.00,0.00,600000.00,''),
(8,4,480,'Busa 7.5 x 84 x 210',0.00,325000.00,0.00,4.0,0.00,0.00,1300000.00,'');

/*Table structure for table `trn_sales_payment_01` */

DROP TABLE IF EXISTS `trn_sales_payment_01`;

CREATE TABLE `trn_sales_payment_01` (
  `fp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO Payment',
  `fp_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO Payment',
  `fp_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(30) DEFAULT NULL COMMENT 'Nama Customer',
  `payment_type` smallint(2) DEFAULT 0 COMMENT 'Jenis Pembayaran 1=CASH, 2=Transfer, 3=Giro, 4=Check',
  `grand_total` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Keseluruhan',
  `is_active` int(2) DEFAULT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fp_id`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  CONSTRAINT `trn_sales_payment_01_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_master_shp`.`mst_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_payment_01_ibfk_2` FOREIGN KEY (`fc_id`) REFERENCES `db_master_shp`.`mst_fish_customer` (`fc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_payment_01` */

insert  into `trn_sales_payment_01`(`fp_id`,`fp_no`,`fp_date`,`company_id`,`company_name`,`fc_id`,`fc_name`,`payment_type`,`grand_total`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'SPM2021060001','2021-06-15',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',1,113400.00,1,1,'2021-06-15 10:50:34',1,'2021-06-15 10:50:34'),
(2,'SPM2021060002','2021-06-15',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',1,769500.00,1,1,'2021-06-15 10:53:08',1,'2021-06-15 10:53:08'),
(3,'SPM2021060003','2021-06-15',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',1,336000.00,1,1,'2021-06-15 10:53:31',1,'2021-06-15 10:53:31'),
(4,'SPM2021060004','2021-06-15',1,'Suhanda Souwmpie',1,'PT Lautan Makmur Sentosa',2,300000.00,1,1,'2021-06-15 10:53:41',1,'2021-06-15 10:53:41');

/*Table structure for table `trn_sales_payment_02` */

DROP TABLE IF EXISTS `trn_sales_payment_02`;

CREATE TABLE `trn_sales_payment_02` (
  `fp_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '⁯Id Detail Fish Payment',
  `fp_id` int(11) DEFAULT NULL COMMENT 'Id Header Fish Payment',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Id Sales',
  `sales_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Sales',
  `amount` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Bayar',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Persen Diskon',
  `disc_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Diskon',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `is_active` smallint(1) DEFAULT 1,
  PRIMARY KEY (`fp_02_id`),
  KEY `fp_id` (`fp_id`),
  CONSTRAINT `trn_sales_payment_02_ibfk_1` FOREIGN KEY (`fp_id`) REFERENCES `trn_sales_payment_01` (`fp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_payment_02` */

insert  into `trn_sales_payment_02`(`fp_02_id`,`fp_id`,`sales_id`,`sales_no`,`amount`,`disc_percent`,`disc_value`,`total`,`is_active`) values 
(1,1,1,'IN2021060001',113400.00,0.0,0.00,113400.00,1),
(2,2,3,'IN2021060003',769500.00,0.0,0.00,769500.00,1),
(3,3,2,'IN2021060002',336000.00,0.0,0.00,336000.00,1),
(4,4,2,'IN2021060002',300000.00,0.0,0.00,300000.00,1);

/*Table structure for table `trn_so_dp` */

DROP TABLE IF EXISTS `trn_so_dp`;

CREATE TABLE `trn_so_dp` (
  `sd_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `customer_name` varchar(30) DEFAULT NULL COMMENT 'Nama Supplier',
  `sd_no` varchar(15) DEFAULT NULL COMMENT 'No Dokumen',
  `sd_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `so_id` int(11) DEFAULT NULL COMMENT 'Id PO',
  `so_no` varchar(15) DEFAULT NULL COMMENT 'No PO',
  `amount` decimal(10,2) DEFAULT 0.00 COMMENT 'Nilai DP',
  `remarks` varchar(180) DEFAULT NULL COMMENT 'Keterangan Singkat',
  `is_active` tinyint(1) DEFAULT 1 COMMENT 'Status Dokumen',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`sd_id`),
  KEY `supplier_id` (`customer_id`),
  KEY `po_id` (`so_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_so_dp` */

insert  into `trn_so_dp`(`sd_id`,`customer_id`,`customer_name`,`sd_no`,`sd_date`,`so_id`,`so_no`,`amount`,`remarks`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,1,'PT Lautan Makmur Sentosa','SDS2021060001','2021-06-15',3,'IN2021060003',100000.00,'',1,1,'2021-06-15 04:08:00',NULL,NULL),
(2,1,'PT Lautan Makmur Sentosa','SDS2021060002','2021-06-15',3,'IN2021060003',200000.00,'',1,1,'2021-06-15 04:08:10',NULL,NULL),
(3,1,'PT Lautan Makmur Sentosa','SDS2021060003','2021-06-15',1,'IN2021060001',50000.00,'Test',1,1,'2021-06-15 05:46:00',NULL,NULL);

/*Table structure for table `trn_stock` */

DROP TABLE IF EXISTS `trn_stock`;

CREATE TABLE `trn_stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT 'In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `items_out` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `adj_in` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `adj_out` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_id`),
  UNIQUE KEY `items_id` (`items_id`,`warehouse_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `trn_stock_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_ibfk_2` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_bumbu_master`.`mst_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock` */

insert  into `trn_stock`(`stock_id`,`items_id`,`doc_no`,`warehouse_id`,`company_id`,`company_name`,`trn_date`,`trn_year`,`trn_month`,`activity`,`items_in`,`items_out`,`adj_in`,`adj_out`,`old_stock`,`current_stock`) values 
(1,477,'OSS2021060001',5,1,'Suhanda Souwmpie','2021-06-16',2021,'06','Out_tr',0.0,2.0,0.0,0.0,12.0,10.0),
(2,478,'OSS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','Out_tr',0.0,3.0,0.0,0.0,10.0,7.0),
(3,479,'DO2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','DO_Trans',0.0,2.0,0.0,0.0,12.0,10.0),
(4,480,'DO2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','DO_Trans',0.0,2.0,0.0,0.0,10.0,8.0),
(5,481,'DO2021060002',5,1,'Suhanda Souwmpie','2021-06-16',2021,'06','DO_Trans',0.0,3.0,0.0,0.0,12.0,9.0),
(6,482,'DO2021060001',5,1,'Suhanda Souwmpie','2021-06-15',2021,'06','DO_Trans',0.0,3.0,0.0,0.0,14.0,11.0),
(7,483,'DO2021060001',5,1,'Suhanda Souwmpie','2021-06-15',2021,'06','DO_Trans',0.0,3.0,0.0,0.0,14.0,11.0),
(8,477,'ISS2021060006',4,1,'Suhanda Souwmpie','2021-06-16',2021,'06','In_Trans',2.0,0.0,0.0,0.0,0.0,2.0),
(9,478,'ISS2021060006',4,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Trans',3.0,3.0,0.0,0.0,2.0,5.0);

/*Table structure for table `trn_stock_by_doc` */

DROP TABLE IF EXISTS `trn_stock_by_doc`;

CREATE TABLE `trn_stock_by_doc` (
  `stock_by_doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` varchar(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT '1=In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty',
  `items_out` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty',
  `items_remaining` decimal(7,1) NOT NULL DEFAULT 0.0 COMMENT 'Sisa Items Per Dokumen',
  `old_stock` decimal(7,1) DEFAULT 0.0 COMMENT 'Stok Awal',
  `current_stock` decimal(7,1) DEFAULT 0.0 COMMENT 'Stok Akhir',
  `items_price` decimal(10,2) DEFAULT 0.00 COMMENT 'Harga Beli',
  `pic_data` varchar(15) DEFAULT NULL,
  `data_time` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_by_doc_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `trn_stock_by_doc_ibfk_1` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_by_doc_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_by_doc` */

insert  into `trn_stock_by_doc`(`stock_by_doc_id`,`items_id`,`doc_no`,`project_id`,`warehouse_id`,`trn_date`,`trn_year`,`trn_month`,`activity`,`items_in`,`items_out`,`items_remaining`,`old_stock`,`current_stock`,`items_price`,`pic_data`,`data_time`) values 
(1,477,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,10000.00,'1','2021-06-13 14:39:37'),
(2,478,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,7500.00,'1','2021-06-13 14:39:37'),
(3,479,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,100000.00,'1','2021-06-13 14:39:37'),
(4,480,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,275000.00,'1','2021-06-13 14:39:38'),
(5,481,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,8500.00,'1','2021-06-13 14:39:38'),
(6,482,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,35000.00,'1','2021-06-13 14:39:38'),
(7,483,'ISS2021060001',NULL,5,'2021-06-07','2021','06','In_Po',2.0,0.0,0.0,0.0,2.0,5000.00,'1','2021-06-13 14:39:38'),
(8,477,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,2.0,2.0,4.0,20000.00,'1','2021-06-13 14:41:14'),
(9,478,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,0.0,2.0,4.0,17500.00,'1','2021-06-13 14:41:14'),
(10,479,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,2.0,2.0,4.0,110000.00,'1','2021-06-13 14:41:15'),
(11,480,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,0.0,2.0,4.0,285000.00,'1','2021-06-13 14:41:15'),
(12,481,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,1.0,2.0,4.0,18500.00,'1','2021-06-13 14:41:15'),
(13,482,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,1.0,2.0,4.0,45000.00,'1','2021-06-13 14:41:15'),
(14,483,'ISS2021060002',NULL,5,'2021-06-13','2021','06','In_Po',2.0,0.0,1.0,2.0,4.0,15000.00,'1','2021-06-13 14:41:15'),
(15,477,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,11000.00,'1','2021-06-13 14:45:42'),
(16,478,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,8500.00,'1','2021-06-13 14:45:42'),
(17,479,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,101000.00,'1','2021-06-13 14:45:42'),
(18,480,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,276000.00,'1','2021-06-13 14:45:43'),
(19,481,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,9500.00,'1','2021-06-13 14:45:43'),
(20,482,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,36000.00,'1','2021-06-13 14:45:43'),
(21,483,'ISS2021060003',NULL,5,'2021-06-14','2021','06','In_Po',8.0,0.0,0.0,4.0,12.0,2500.00,'1','2021-06-13 14:45:43'),
(22,477,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,11000.00,'1','2021-06-13 15:35:04'),
(23,478,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,8500.00,'1','2021-06-13 15:35:06'),
(24,479,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,101000.00,'1','2021-06-13 15:35:10'),
(25,480,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,276000.00,'1','2021-06-13 15:35:12'),
(26,481,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,9500.00,'1','2021-06-13 15:35:12'),
(27,482,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,36000.00,'1','2021-06-13 15:35:13'),
(28,483,'ISR2021060003',NULL,5,'2021-06-14','2021','06','Rj_InTrans',0.0,8.0,0.0,12.0,4.0,2500.00,'1','2021-06-13 15:35:13'),
(29,477,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,8.0,4.0,12.0,10000.00,'1','2021-06-13 15:39:51'),
(30,478,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,7.0,4.0,12.0,7500.00,'1','2021-06-13 15:39:52'),
(31,479,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,8.0,4.0,12.0,100000.00,'1','2021-06-13 15:39:52'),
(32,480,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,8.0,4.0,12.0,275000.00,'1','2021-06-13 15:39:52'),
(33,481,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,8.0,4.0,12.0,8500.00,'1','2021-06-13 15:39:52'),
(34,482,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,8.0,4.0,12.0,35000.00,'1','2021-06-13 15:39:52'),
(35,483,'ISS2021060004',NULL,5,'2021-06-13','2021','06','In_Po',8.0,0.0,8.0,4.0,12.0,1500.00,'1','2021-06-13 15:39:52'),
(36,483,'IST2021060005',NULL,5,'2021-06-15','2021','06','In_Others',2.0,0.0,2.0,12.0,14.0,5000.00,'1','2021-06-13 16:59:42'),
(37,482,'IST2021060005',NULL,5,'2021-06-15','2021','06','In_Others',2.0,0.0,2.0,12.0,14.0,35000.00,'1','2021-06-13 16:59:42'),
(38,477,'OSS2021060001',NULL,5,'2021-06-16','2021','06','Out_tr',0.0,2.0,0.0,12.0,10.0,10000.00,'1','2021-06-13 17:15:48'),
(39,477,'ISS2021060006',NULL,4,'2021-06-16','2021','06','In_Trans',2.0,0.0,2.0,0.0,2.0,10000.00,'1','2021-06-13 17:15:49'),
(40,478,'OSS2021060001',NULL,5,'2021-06-16','2021','06','Out_tr',2.0,2.0,2.0,12.0,10.0,7500.00,'1','2021-06-13 17:15:49'),
(41,478,'ISS2021060006',NULL,4,'2021-06-16','2021','06','In_Trans',2.0,0.0,2.0,0.0,2.0,7500.00,'1','2021-06-13 17:15:49'),
(42,478,'OSS2021060002',NULL,5,'2021-06-13','2021','06','Out_tr',0.0,3.0,0.0,10.0,7.0,14166.67,'1','2021-06-13 17:17:13'),
(43,478,'ISS2021060006',NULL,4,'2021-06-13','2021','06','In_Trans',3.0,0.0,3.0,2.0,5.0,14166.67,'1','2021-06-13 17:17:13'),
(44,482,'DO2021060001',NULL,5,'2021-06-15','2021','06','DO_Trans',0.0,3.0,0.0,14.0,11.0,38333.33,'1','2021-06-14 09:48:07'),
(45,483,'DO2021060001',NULL,5,'2021-06-15','2021','06','DO_Trans',0.0,3.0,0.0,14.0,11.0,8333.33,'1','2021-06-14 09:48:07'),
(46,480,'DO2021060002',NULL,5,'2021-06-16','2021','06','DO_Trans',0.0,2.0,0.0,12.0,10.0,275000.00,'1','2021-06-14 09:48:30'),
(47,481,'DO2021060002',NULL,5,'2021-06-16','2021','06','DO_Trans',0.0,3.0,0.0,12.0,9.0,11833.33,'1','2021-06-14 09:48:30'),
(48,479,'DO2021060003',NULL,5,'2021-06-14','2021','06','DO_Trans',0.0,2.0,0.0,12.0,10.0,100000.00,'1','2021-06-14 09:48:48'),
(49,480,'DO2021060003',NULL,5,'2021-06-14','2021','06','DO_Trans',0.0,2.0,0.0,10.0,8.0,285000.00,'1','2021-06-14 09:48:48');

/*Table structure for table `trn_stock_by_doc_tmp` */

DROP TABLE IF EXISTS `trn_stock_by_doc_tmp`;

CREATE TABLE `trn_stock_by_doc_tmp` (
  `stock_by_doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` varchar(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT '1=In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(12,2) DEFAULT 0.00 COMMENT 'Qty',
  `items_out` decimal(12,2) DEFAULT 0.00 COMMENT 'Qty',
  `items_remaining` decimal(12,2) NOT NULL DEFAULT 0.00 COMMENT 'Sisa Items Per Dokumen',
  `old_stock` decimal(12,2) DEFAULT 0.00 COMMENT 'Stok Awal',
  `current_stock` decimal(12,2) DEFAULT 0.00 COMMENT 'Stok Akhir',
  `items_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Beli',
  `pic_data` varchar(15) DEFAULT NULL,
  `data_time` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_by_doc_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_by_doc_tmp` */

/*Table structure for table `trn_stock_hist` */

DROP TABLE IF EXISTS `trn_stock_hist`;

CREATE TABLE `trn_stock_hist` (
  `stock_hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Dokumen Transaksi Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Warehouse Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Transaksi',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun Transaksi',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan Transaksi',
  `activity` varchar(15) DEFAULT NULL COMMENT 'Aktifitas Stok',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_hist_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `trn_stock_hist_ibfk_1` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_hist_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_hist_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_bumbu_master`.`mst_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_hist` */

insert  into `trn_stock_hist`(`stock_hist_id`,`items_id`,`doc_no`,`warehouse_id`,`company_id`,`company_name`,`trn_date`,`trn_year`,`trn_month`,`activity`,`qty`,`old_stock`,`current_stock`) values 
(1,477,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(2,478,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(3,479,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(4,480,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(5,481,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(6,482,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(7,483,'ISS2021060001',5,1,'Suhanda Souwmpie','2021-06-07',2021,'06','In_Po',2.0,0.0,2.0),
(8,477,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(9,478,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(10,479,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(11,480,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(12,481,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(13,482,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(14,483,'ISS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',2.0,2.0,4.0),
(15,477,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(16,478,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(17,479,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(18,480,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(19,481,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(20,482,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(21,483,'ISS2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','In_Po',8.0,4.0,12.0),
(22,477,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(23,478,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(24,479,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(25,480,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(26,481,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(27,482,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(28,483,'ISR2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','Rj_InTrans',8.0,12.0,4.0),
(29,477,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(30,478,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(31,479,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(32,480,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(33,481,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(34,482,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(35,483,'ISS2021060004',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Po',8.0,4.0,12.0),
(36,483,'IST2021060005',5,1,'Suhanda Souwmpie','2021-06-15',2021,'06','In_Others',2.0,12.0,14.0),
(37,482,'IST2021060005',5,1,'Suhanda Souwmpie','2021-06-15',2021,'06','In_Others',2.0,12.0,14.0),
(38,477,'OSS2021060001',5,1,'Suhanda Souwmpie','2021-06-16',2021,'06','Out_tr',2.0,12.0,10.0),
(39,477,'ISS2021060006',4,1,'Suhanda Souwmpie','2021-06-16',2021,'06','In_Trans',2.0,0.0,2.0),
(40,478,'OSS2021060001',5,1,'Suhanda Souwmpie','2021-06-16',2021,'06','Out_tr',2.0,12.0,10.0),
(41,478,'ISS2021060006',4,1,'Suhanda Souwmpie','2021-06-16',2021,'06','In_Trans',2.0,0.0,2.0),
(42,478,'OSS2021060002',5,1,'Suhanda Souwmpie','2021-06-13',2021,'06','Out_tr',3.0,10.0,7.0),
(43,478,'ISS2021060006',4,1,'Suhanda Souwmpie','2021-06-13',2021,'06','In_Trans',3.0,2.0,5.0),
(44,482,'DO2021060001',5,1,'Suhanda Souwmpie','2021-06-15',2021,'06','DO_Trans',3.0,14.0,11.0),
(45,483,'DO2021060001',5,1,'Suhanda Souwmpie','2021-06-15',2021,'06','DO_Trans',3.0,14.0,11.0),
(46,480,'DO2021060002',5,1,'Suhanda Souwmpie','2021-06-16',2021,'06','DO_Trans',2.0,12.0,10.0),
(47,481,'DO2021060002',5,1,'Suhanda Souwmpie','2021-06-16',2021,'06','DO_Trans',3.0,12.0,9.0),
(48,479,'DO2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','DO_Trans',2.0,12.0,10.0),
(49,480,'DO2021060003',5,1,'Suhanda Souwmpie','2021-06-14',2021,'06','DO_Trans',2.0,10.0,8.0);

/*Table structure for table `trn_stock_hist_tmp` */

DROP TABLE IF EXISTS `trn_stock_hist_tmp`;

CREATE TABLE `trn_stock_hist_tmp` (
  `stock_hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Dokumen Transaksi Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Warehouse Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Transaksi',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun Transaksi',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan Transaksi',
  `activity` varchar(15) DEFAULT NULL COMMENT 'Aktifitas Stok',
  `qty` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_hist_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_hist_tmp` */

/*Table structure for table `trn_stock_tmp` */

DROP TABLE IF EXISTS `trn_stock_tmp`;

CREATE TABLE `trn_stock_tmp` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT 'In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `items_out` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `adj_in` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `adj_out` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_id`),
  UNIQUE KEY `items_id` (`items_id`,`warehouse_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_tmp` */

/*Table structure for table `trn_unit_conversion` */

DROP TABLE IF EXISTS `trn_unit_conversion`;

CREATE TABLE `trn_unit_conversion` (
  `uv_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Tabel',
  `uv_no` varchar(15) DEFAULT NULL COMMENT 'No Table',
  `warehouse_id` int(11) DEFAULT NULL,
  `items_id_from` int(11) NOT NULL COMMENT 'Id Item Asal',
  `units_id_from` int(11) NOT NULL COMMENT 'Id Stuan Asal',
  `qty_from` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Quantiti Asal',
  `price_from` decimal(11,2) DEFAULT 0.00,
  `items_id_to` int(11) NOT NULL COMMENT 'Id Item Tujuan',
  `units_id_to` int(11) NOT NULL COMMENT 'Id Stuan Tujuan',
  `qty_to` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Quantiti Tujuan',
  `price_to` decimal(11,2) DEFAULT 0.00,
  `remarks` varchar(195) DEFAULT NULL COMMENT 'Catatan',
  `is_active` smallint(1) DEFAULT NULL COMMENT 'Status',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`uv_id`),
  KEY `items_id_from` (`items_id_from`),
  KEY `items_id_to` (`items_id_to`),
  KEY `units_id_from` (`units_id_from`),
  KEY `units_id_to` (`units_id_to`),
  KEY `warehouse_id` (`warehouse_id`),
  CONSTRAINT `trn_unit_conversion_ibfk_1` FOREIGN KEY (`items_id_from`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_2` FOREIGN KEY (`items_id_to`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_3` FOREIGN KEY (`units_id_from`) REFERENCES `db_bumbu_master`.`mst_items_unit` (`items_unit_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_4` FOREIGN KEY (`units_id_to`) REFERENCES `db_bumbu_master`.`mst_items_unit` (`items_unit_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_5` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_unit_conversion` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
