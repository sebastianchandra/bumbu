<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <div>
        <table>
          <tr>
            <td>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                  <li class="breadcrumb-item">Transaction</li>
                  <li class="breadcrumb-item">Pesanan Pembelian</li>
                </ol>
              </nav>
            </td>
          </tr>
          <tr>
            <td>
              <h4 >Input Pesanan Pembelian</h4></td>
            </td>
          </tr>
        </table>
      </div>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb"></nav>
      </div>
    </div>
  </div>
</div>

<form class="form-horizontal" method="post" action="<?php echo base_url().'coba/act'; ?>">    
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
          <div class="card-body-form">            
            <div class="form-group row">
              <div class="col-md-2">
                <p>Remarks</p>
              </div>
              <div class="col-md-7">
                <input type="text" class="form-control" id="remarks" name="remarks" value="">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <button class="btn btn-primary btn-sm" type="submit" id="save">Simpan</button>
                <a class="btn btn-warning btn-sm" href="<?php echo base_url('transaction/purchase_order'); ?>">Batal</a>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</form>
