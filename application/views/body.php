<?php
if($this->current_user['loginuser']==1){
// test($this->current_user['company_id'],1);
// test($this->current_user['company_name'],1);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template">
    <meta name="description"
        content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>POS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/images/'); ?>favicon.png">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/'); ?>multicheck.css">
    <link href="<?= base_url('assets/css/'); ?>dataTables.bootstrap4.css" rel="stylesheet">
    <link href="<?= base_url('assets/css/'); ?>style.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/css/'); ?>select2.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/toastr.min.css" rel="stylesheet">
    
    <script src="<?= base_url('assets/js/'); ?>jquery.min.js"></script>
    <!-- <script src="<?= base_url('assets/js/'); ?>bootstrap.bundle.min.js"></script> -->
    <script src="<?= base_url('assets/js/'); ?>datatable-checkbox-init.js"></script>
    <script src="<?= base_url('assets/js/'); ?>datatables.min.js"></script>
    <script src="<?= base_url('assets/js/'); ?>select2.full.min.js"></script>
    <script src="<?= base_url('assets/js/'); ?>select2.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/toastr.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
    <script>
        var baseUrl = '<?= base_url(); ?>';

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>

    <style type="text/css">
        @media print {
            .no-print {
              visibility: hidden !important;
            }
            .do-print {
              visibility: visible !important;
            }
        }
        
        .dataTable td.col_left {
            text-align: left;
        }
        .dataTable td.col_center {
            text-align: center;
        }
        .dataTable td.col_right {
            text-align: right;
        }

    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar no-print" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b class="logo-icon ps-2">
                            <!-- E. Distributor -->
                            <!-- <img src="<?= base_url('assets/images/'); ?>logo-icon.png" alt="homepage" class="light-logo" /> -->

                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <div style="font-size: 21px;font-weight: bold">POS</div>
                            <!-- <img src="<?= base_url('assets/images/'); ?>logo-text.png" alt="homepage" class="light-logo" /> -->

                        </span>
                    </a>
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item d-none d-lg-block">
                            <!-- <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                                <i class="mdi mdi-menu font-24"></i>
                            </a> -->
                        </li>
                    </ul>
                    <ul class="navbar-nav float-end">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="<?= base_url('logout'); ?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-power-off me-1 ms-1"></i> Logout
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <!-- <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user me-1 ms-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet me-1 ms-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email me-1 ms-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i
                                        class="ti-settings me-1 ms-1"></i> Account Setting</a> -->
                                <!-- <div class="dropdown-divider"></div> -->
                                <a class="dropdown-item" href="<?= base_url('logout'); ?>"><i
                                        class="fa fa-power-off me-1 ms-1"></i> Logout</a>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <?php 
        $amenu    = array();
        $asubmenu = array();


        foreach ($this->current_user['submenu'] as $key => $value) {
          $asubmenu[$value->menu_group][]=$value;
        }

        // test($this->current_user['menu'],0); 
        ?>
        <aside class="left-sidebar no-print" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="pt-4">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="<?= base_url(); ?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <?php
                        foreach ($this->current_user['menu'] as $key => $menu) {
                        ?>
                            <li class="sidebar-item <?php echo $this->session->userdata('ses_menu')['active_menu'] == $menu->menu_group ? 'selected' : ''; ?> "> 
                                <a class="sidebar-link has-arrow waves-effect waves-dark <?php echo $this->session->userdata('ses_menu')['active_menu'] == $menu->menu_group ? 'active' : ''; ?>" href="<?= ($menu->jmlh>=1)? 'javascript:void(0)' : base_url($menu->url); ?>" aria-expanded="false"><i class="<?= $menu->icon; ?>"></i><span class="hide-menu"><?= $menu->menu_group; ?> </span></a>
                                    <ul aria-expanded="false" class="collapse first-level <?php echo $this->session->userdata('ses_menu')['active_menu'] == $menu->menu_group ? 'in' : ''; ?>">
                                        <?php
                                        foreach ($asubmenu[$menu->menu_group] as $key => $submenu) { 
                                        ?>
                                        <li class="sidebar-item <?php echo $submenu->url == $this->session->userdata('ses_menu')['active_submenu'] ? 'active' : '' ?> "><a href="<?php echo base_url($submenu->url); ?>" class="sidebar-link"><i
                                                    class="mdi "></i><?php echo $submenu->menu_name ?></a></li>
                                        <?php 
                                        }
                                        ?>
                                    </ul>
                                </li>
                        <?php 
                        }
                        ?>
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <div class="page-wrapper">
            <?= $contents; ?>
            
            <footer class="footer text-center">
                Copyright &copy; 2021 <a href="https://www.instagram.com/karbo.tech/">karbo.tech</a> - Designed by <a href="https://www.instagram.com/karbo.tech/">karbo.tech</a>
            </footer>
        </div>
    </div>

    <script>

        function tanggal_indonesia(dateString)
        {
            var thisDate = dateString.split('-');
            var newDate = [thisDate[2],thisDate[1],thisDate[0] ].join("-");
            return newDate;
        }

        $(".price").on("keydown", function(e) {
            var keycode = (event.which) ? event.which : event.keyCode;
            if (e.shiftKey == true || e.ctrlKey == true) return false;
            if ([8, 110, 39, 37, 46, 9].indexOf(keycode) >= 0 || // allow tab, dot, left and right arrows, delete keys
                (keycode == 190 && this.value.indexOf('.') === -1) || // allow dot if not exists in the value
                (keycode == 110 && this.value.indexOf('.') === -1) || // allow dot if not exists in the value
                (keycode >= 48 && keycode <= 57) || // allow numbers
                (keycode >= 96 && keycode <= 105)) { // allow numpad numbers
                // check for the decimals after dot and prevent any digits
                var parts = this.value.split('.');
            if (parts.length > 1 && // has decimals
                parts[1].length >= 2 && // should limit this
                (
                    (keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)
                ) // requested key is a digit
            ){
                return false;
            }else {
                if (keycode == 110) {
                    this.value += ".";
                    return false;
                }
                return true;
            }
            } else {
                return false;
            }
        }).on("keyup", function() {
            var parts = this.value.split('.');
            parts[0] = parts[0].replace(/,/g, '').replace(/^0+/g, '');
            if (parts[0] == "") parts[0] = "0";
            var calculated = parts[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            if (parts.length >= 2) calculated += "." + parts[1].substring(0, 2);
            this.value = calculated;
            if (this.value == "NaN" || this.value == "") this.value = 0;
        });
            
    </script>
    
    <!-- Bootstrap tether Core JavaScript -->
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url('assets/js/'); ?>perfect-scrollbar.jquery.min.js"></script>
    <script src="<?= base_url('assets/js/'); ?>sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url('assets/js/'); ?>waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url('assets/js/'); ?>sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url('assets/js/'); ?>custom.min.js"></script>
    <!-- this page js -->
    <script src="<?= base_url('assets/js/'); ?>jquery.multicheck.js"></script>
    

</body>

</html>

<?php 
}else{
$this->session->set_flashdata('msg','<div class="alert alert-danger text-center"><font size="2">Harap Login Kembali.</font></div>');
redirect('login');
}
?>