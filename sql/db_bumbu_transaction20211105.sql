/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.19-MariaDB : Database - db_bumbu_transaction
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_bumbu_transaction` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_bumbu_transaction`;

/*Table structure for table `mst_coa` */

DROP TABLE IF EXISTS `mst_coa`;

CREATE TABLE `mst_coa` (
  `coa_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Id COA',
  `coa_level` int(3) DEFAULT NULL COMMENT 'Level COA',
  `coa_code` varchar(5) DEFAULT NULL COMMENT 'Kode COA',
  `coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA',
  `coa_name` varchar(150) DEFAULT NULL COMMENT 'Nama COA',
  `coa_parent_id` int(3) DEFAULT NULL COMMENT 'Id Induk COA',
  `is_active` int(1) DEFAULT NULL COMMENT 'Status Aktif COA',
  `is_lowest` int(11) DEFAULT NULL COMMENT 'Status Level COA',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`coa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_coa` */

/*Table structure for table `mst_coa_level` */

DROP TABLE IF EXISTS `mst_coa_level`;

CREATE TABLE `mst_coa_level` (
  `coa_level` int(2) NOT NULL AUTO_INCREMENT COMMENT 'Level COA',
  `level_name` varchar(50) DEFAULT NULL COMMENT 'Keterangan',
  PRIMARY KEY (`coa_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_coa_level` */

/*Table structure for table `mst_journal_config` */

DROP TABLE IF EXISTS `mst_journal_config`;

CREATE TABLE `mst_journal_config` (
  `jc_id` varchar(10) NOT NULL COMMENT 'Id Data',
  `trn_code` varchar(10) DEFAULT NULL COMMENT 'Kode Dokumen Transaksi',
  `db_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Debet',
  `db_coa_code` varchar(5) DEFAULT NULL COMMENT 'Kode COA Debet',
  `db_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Debet',
  `db_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Debet',
  `cr_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Kredit',
  `cr_coa_code` varchar(5) DEFAULT NULL COMMENT 'Kode COA Kredit',
  `cr_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Kredit',
  `cr_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Kredit',
  PRIMARY KEY (`jc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_journal_config` */

/*Table structure for table `trn_closing_journal` */

DROP TABLE IF EXISTS `trn_closing_journal`;

CREATE TABLE `trn_closing_journal` (
  `closing_journal_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Closing',
  `month_periode` varchar(2) DEFAULT NULL COMMENT 'Bulan Closing',
  `year_periode` year(4) DEFAULT NULL COMMENT 'Tahun Closing',
  `is_active` int(1) DEFAULT 1 COMMENT '1 = Closing , 0 = Open',
  `pic_closing` int(4) DEFAULT NULL,
  `time_closing` datetime DEFAULT NULL,
  PRIMARY KEY (`closing_journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_closing_journal` */

/*Table structure for table `trn_do_01` */

DROP TABLE IF EXISTS `trn_do_01`;

CREATE TABLE `trn_do_01` (
  `do_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Incoming Stok',
  `do_date` date DEFAULT NULL COMMENT 'Tanggal',
  `do_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen (CCCCYYYYMMNNNN) ex : LMIS2018070001',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Id Gudang',
  `doc_ref` varchar(15) DEFAULT NULL COMMENT 'Referensi Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `do_status` varchar(20) DEFAULT NULL COMMENT '1 = Close, 2 = Void',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `trn_do_01` */

insert  into `trn_do_01`(`do_id`,`do_date`,`do_no`,`warehouse_id`,`doc_ref`,`company_id`,`company_name`,`do_status`,`remarks`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-11-03','DO2021110001',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 04:40:12',NULL,NULL),
(2,'2021-11-03','DO2021110002',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:03:51',NULL,NULL),
(3,'2021-11-03','DO2021110003',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:04:23',NULL,NULL),
(4,'2021-11-03','DO2021110004',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:22:36',NULL,NULL),
(5,'2021-11-03','DO2021110005',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:23:26',NULL,NULL),
(6,'2021-11-03','DO2021110006',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:24:24',NULL,NULL),
(7,'2021-11-03','DO2021110007',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:26:38',NULL,NULL),
(8,'2021-11-03','DO2021110008',6,'SL2021110001',1,'Suhanda Souwmpie','Reject','',1,'2021-11-03 05:27:46',NULL,NULL),
(9,'2021-11-03','DO2021110009',6,'SL2021110001',1,'Suhanda Souwmpie','Close','',1,'2021-11-03 05:29:50',NULL,NULL);

/*Table structure for table `trn_do_02` */

DROP TABLE IF EXISTS `trn_do_02`;

CREATE TABLE `trn_do_02` (
  `do_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `do_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Incoming',
  `do_qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty PO',
  `price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(12,2) DEFAULT 0.00,
  `total` decimal(12,2) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`do_02_id`),
  KEY `is_id` (`do_02_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `trn_do_02` */

insert  into `trn_do_02`(`do_02_id`,`do_id`,`items_id`,`items_name`,`qty`,`do_qty`,`price`,`discount`,`total`,`remarks`) values 
(1,1,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(2,1,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(3,2,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(4,2,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(5,3,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(6,3,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(7,4,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(8,4,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(9,5,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(10,5,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(11,6,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(12,6,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(13,7,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(14,7,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(15,8,1232,'Penggorengan no 16',0.0,10.0,0.00,0.00,0.00,'0'),
(16,8,1233,'test1',0.0,6.0,0.00,0.00,0.00,'0'),
(17,9,1232,'Penggorengan no 16',10.0,10.0,0.00,0.00,0.00,'0'),
(18,9,1233,'test1',6.0,6.0,0.00,0.00,0.00,'0');

/*Table structure for table `trn_do_03` */

DROP TABLE IF EXISTS `trn_do_03`;

CREATE TABLE `trn_do_03` (
  `do_03_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `do_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Incoming',
  `do_qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty PO',
  `price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(12,2) DEFAULT 0.00,
  `total` decimal(12,2) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`do_03_id`),
  KEY `is_id` (`do_03_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_do_03` */

/*Table structure for table `trn_fish_payment_01` */

DROP TABLE IF EXISTS `trn_fish_payment_01`;

CREATE TABLE `trn_fish_payment_01` (
  `fp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO Payment',
  `fp_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO Payment',
  `fp_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(30) DEFAULT NULL COMMENT 'Nama Customer',
  `payment_type` smallint(2) DEFAULT 0 COMMENT 'Jenis Pembayaran 1=CASH, 2=Transfer, 3=Giro, 4=Check',
  `grand_total` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Keseluruhan',
  `is_active` int(2) DEFAULT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fp_id`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  CONSTRAINT `trn_fish_payment_01_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_master_shp`.`mst_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_fish_payment_01_ibfk_2` FOREIGN KEY (`fc_id`) REFERENCES `db_master_shp`.`mst_fish_customer` (`fc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_fish_payment_01` */

/*Table structure for table `trn_fish_payment_02` */

DROP TABLE IF EXISTS `trn_fish_payment_02`;

CREATE TABLE `trn_fish_payment_02` (
  `fp_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '⁯Id Detail Fish Payment',
  `fp_id` int(11) DEFAULT NULL COMMENT 'Id Header Fish Payment',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Id Sales',
  `sales_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Sales',
  `amount` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Bayar',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Persen Diskon',
  `disc_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Diskon',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `is_active` smallint(1) DEFAULT 1,
  PRIMARY KEY (`fp_02_id`),
  KEY `fp_id` (`fp_id`),
  CONSTRAINT `trn_fish_payment_02_ibfk_1` FOREIGN KEY (`fp_id`) REFERENCES `trn_fish_payment_01` (`fp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_fish_payment_02` */

/*Table structure for table `trn_incoming_stock_01` */

DROP TABLE IF EXISTS `trn_incoming_stock_01`;

CREATE TABLE `trn_incoming_stock_01` (
  `is_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Incoming Stok',
  `is_date` date DEFAULT NULL COMMENT 'Tanggal',
  `is_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen (CCCCYYYYMMNNNN) ex : LMIS2018070001',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Id Gudang',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `project_name` varchar(55) DEFAULT NULL COMMENT 'Nama Project',
  `is_kind` int(11) DEFAULT NULL COMMENT '1 = MR PO, 2 = MR Transfer, 3 = MR Other',
  `doc_ref` varchar(15) DEFAULT NULL COMMENT 'Referensi Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `is_status` varchar(20) DEFAULT NULL COMMENT '1 = Close, 2 = Void',
  `sender_pic` varchar(50) DEFAULT NULL COMMENT 'Pengirim barang',
  `receiver_pic` varchar(50) DEFAULT NULL COMMENT 'Penerima barang',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`is_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `trn_incoming_stock_01` */

insert  into `trn_incoming_stock_01`(`is_id`,`is_date`,`is_no`,`warehouse_id`,`project_id`,`project_name`,`is_kind`,`doc_ref`,`company_id`,`company_name`,`is_status`,`sender_pic`,`receiver_pic`,`remarks`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-11-03','IST2021110001',6,0,'',2,'',1,'Suhanda Souwmpie','Closed','','test','',1,'2021-11-03 04:32:27',NULL,NULL),
(2,'2021-11-05','IST2021110002',6,0,'',2,'',1,'Suhanda Souwmpie','Closed','','test','',1,'2021-11-05 09:41:29',NULL,NULL);

/*Table structure for table `trn_incoming_stock_02` */

DROP TABLE IF EXISTS `trn_incoming_stock_02`;

CREATE TABLE `trn_incoming_stock_02` (
  `is_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `is_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Incoming',
  `po_qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty PO',
  `price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(12,2) DEFAULT 0.00,
  `total` decimal(12,2) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`is_02_id`),
  KEY `is_id` (`is_02_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `trn_incoming_stock_02` */

insert  into `trn_incoming_stock_02`(`is_02_id`,`is_id`,`items_id`,`items_name`,`qty`,`po_qty`,`price`,`discount`,`total`,`remarks`) values 
(1,1,1233,'test1',1000.0,0.0,10000.00,0.00,10000000.00,''),
(2,1,1232,'Penggorengan no 16',1000.0,0.0,60000.00,0.00,60000000.00,''),
(3,2,1240,'Bawang Merah',1000.0,0.0,1300.00,0.00,1300000.00,''),
(4,2,1239,'Bawah Putih',1000.0,0.0,900.00,0.00,900000.00,''),
(5,2,1238,'Kemiri',1000.0,0.0,800.00,0.00,800000.00,''),
(6,2,1237,'Garam',1000.0,0.0,500.00,0.00,500000.00,''),
(7,2,1236,'Cabe',1000.0,0.0,1000.00,0.00,1000000.00,'');

/*Table structure for table `trn_incoming_stock_03` */

DROP TABLE IF EXISTS `trn_incoming_stock_03`;

CREATE TABLE `trn_incoming_stock_03` (
  `is_03_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Is 02',
  `is_id` int(11) DEFAULT NULL COMMENT 'Id Is',
  `cost_name` varchar(250) DEFAULT NULL COMMENT 'Nama biaya',
  `cost_value` decimal(10,1) DEFAULT NULL,
  PRIMARY KEY (`is_03_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_incoming_stock_03` */

/*Table structure for table `trn_invoice_01` */

DROP TABLE IF EXISTS `trn_invoice_01`;

CREATE TABLE `trn_invoice_01` (
  `invoice_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `invoice_date` date DEFAULT NULL COMMENT 'Tanggal',
  `invoice_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen',
  `doc_ref` varchar(15) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `warehouse_name` varchar(55) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(10) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(100) DEFAULT NULL COMMENT 'Nama Customer',
  `total_owner_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Owner Price',
  `total_cust_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Cust Price',
  `total_gov_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Government Price',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Keterangan',
  `doc_status` varchar(15) DEFAULT NULL COMMENT 'New, Outstanding, Closed',
  `is_cancel` tinyint(1) DEFAULT 0 COMMENT 'Flag Pembatalan',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  UNIQUE KEY `sales_no` (`invoice_no`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  KEY `trn_sales_01_ibfk_1` (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_invoice_01` */

insert  into `trn_invoice_01`(`invoice_id`,`invoice_date`,`invoice_no`,`doc_ref`,`warehouse_id`,`warehouse_name`,`company_id`,`company_name`,`fc_id`,`fc_name`,`total_owner_price`,`total_cust_price`,`total_gov_price`,`remarks`,`doc_status`,`is_cancel`,`disc_value`,`disc_percent`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-11-03','IN2021110001','SL2021110001',4,'',1,'Suhanda Souwmpie',3,'Putra Bintang Mas I (Suhanda)',0.00,590500.00,0.00,'','Reject',1,0.00,0.00,'1','2021-11-03 04:40:12',NULL,NULL),
(2,'2021-11-03','IN2021110002','SL2021110001',4,'',1,'Suhanda Souwmpie',3,'Putra Bintang Mas I (Suhanda)',0.00,590500.00,0.00,'','Reject',1,0.00,0.00,'1','2021-11-03 05:22:36',NULL,NULL),
(3,'2021-11-03','IN2021110003','SL2021110001',4,'',1,'Suhanda Souwmpie',3,'Putra Bintang Mas I (Suhanda)',0.00,590500.00,0.00,'','Reject',1,0.00,0.00,'1','2021-11-03 05:27:46',NULL,NULL),
(4,'2021-11-03','IN2021110004','SL2021110001',4,'',1,'Suhanda Souwmpie',3,'Putra Bintang Mas I (Suhanda)',0.00,590500.00,0.00,'','New',0,0.00,0.00,'1','2021-11-03 05:29:50',NULL,NULL);

/*Table structure for table `trn_invoice_02` */

DROP TABLE IF EXISTS `trn_invoice_02`;

CREATE TABLE `trn_invoice_02` (
  `invoice_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama Ikan',
  `owner_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Owner',
  `cust_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Customer',
  `gov_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Pemprov',
  `qty` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty Masuk',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `subtotal` decimal(14,2) DEFAULT 0.00 COMMENT 'Subtotal',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`invoice_02_id`),
  KEY `trn_sales_02_ibfk_5` (`items_id`),
  KEY `sales_id` (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `trn_invoice_02` */

insert  into `trn_invoice_02`(`invoice_02_id`,`invoice_id`,`items_id`,`items_name`,`owner_price`,`cust_price`,`gov_price`,`qty`,`disc_value`,`disc_percent`,`subtotal`,`remarks`) values 
(1,1,1232,'Penggorengan no 16',0.00,66000.00,0.00,8.0,0.00,0.00,528000.00,'02'),
(2,1,1232,'Penggorengan no 16',0.00,0.00,0.00,2.0,0.00,0.00,0.00,'03'),
(3,1,1233,'test1',0.00,12500.00,0.00,5.0,0.00,0.00,62500.00,'02'),
(4,1,1233,'test1',0.00,0.00,0.00,1.0,0.00,0.00,0.00,'03'),
(9,2,1232,'Penggorengan no 16',0.00,66000.00,0.00,8.0,0.00,0.00,528000.00,'02'),
(10,2,1232,'Penggorengan no 16',0.00,0.00,0.00,2.0,0.00,0.00,0.00,'03'),
(11,2,1233,'test1',0.00,12500.00,0.00,5.0,0.00,0.00,62500.00,'02'),
(12,2,1233,'test1',0.00,0.00,0.00,1.0,0.00,0.00,0.00,'03'),
(13,3,1232,'Penggorengan no 16',0.00,66000.00,0.00,8.0,0.00,0.00,528000.00,'02'),
(14,3,1232,'Penggorengan no 16',0.00,0.00,0.00,2.0,0.00,0.00,0.00,'03'),
(15,3,1233,'test1',0.00,12500.00,0.00,5.0,0.00,0.00,62500.00,'02'),
(16,3,1233,'test1',0.00,0.00,0.00,1.0,0.00,0.00,0.00,'03'),
(17,4,1232,'Penggorengan no 16',0.00,66000.00,0.00,8.0,0.00,0.00,528000.00,'02'),
(18,4,1232,'Penggorengan no 16',0.00,0.00,0.00,2.0,0.00,0.00,0.00,'03'),
(19,4,1233,'test1',0.00,12500.00,0.00,5.0,0.00,0.00,62500.00,'02'),
(20,4,1233,'test1',0.00,0.00,0.00,1.0,0.00,0.00,0.00,'03');

/*Table structure for table `trn_journal` */

DROP TABLE IF EXISTS `trn_journal`;

CREATE TABLE `trn_journal` (
  `journal_id` varchar(10) NOT NULL COMMENT 'Id Data',
  `trn_code` varchar(10) DEFAULT NULL COMMENT 'Kode Dokumen (IS, OS, PO,..)',
  `journal_date` date DEFAULT NULL COMMENT 'Tanggal Posting',
  `year_period` int(4) DEFAULT NULL COMMENT 'Tahun',
  `month_period` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `journal_dest` varchar(50) DEFAULT NULL COMMENT 'Gudang/ Project',
  `dest_id` int(10) NOT NULL COMMENT 'Id Project/ Id Gudang',
  `trn_id` varchar(16) DEFAULT NULL COMMENT 'Id Dok (ISS2020040001,..)',
  `db_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Debet',
  `db_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Debet',
  `db_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Debet',
  `db_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Debet',
  `db_info` varchar(100) DEFAULT NULL,
  `cr_coa_id` int(10) DEFAULT NULL COMMENT 'Id COA Kredit',
  `cr_coa_detail` varchar(30) DEFAULT NULL COMMENT 'Kode Detil COA Kredit',
  `cr_coa_name` varchar(70) DEFAULT NULL COMMENT 'Nama COA Kredit',
  `cr_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Kredit',
  `cr_info` varchar(100) DEFAULT NULL,
  `pic_posting` int(11) DEFAULT NULL,
  `date_posting` datetime DEFAULT NULL,
  `journal_type` int(1) DEFAULT NULL COMMENT '1 = Auto Journal, 2 = Manual Journal, 3 = Jounal Cash/Bank',
  PRIMARY KEY (`journal_id`),
  KEY `db_coa_id` (`db_coa_id`),
  KEY `cr_coa_id` (`cr_coa_id`),
  CONSTRAINT `trn_journal_ibfk_1` FOREIGN KEY (`db_coa_id`) REFERENCES `db_master_shp`.`mst_coa` (`coa_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_journal_ibfk_2` FOREIGN KEY (`cr_coa_id`) REFERENCES `db_master_shp`.`mst_coa` (`coa_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_journal` */

/*Table structure for table `trn_outgoing_stock_01` */

DROP TABLE IF EXISTS `trn_outgoing_stock_01`;

CREATE TABLE `trn_outgoing_stock_01` (
  `os_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id OS',
  `os_date` date DEFAULT NULL COMMENT 'Tanggal OS',
  `os_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen (CCCCYYYYMMNNNN) ex : LMOS2018070001',
  `company_id` int(11) DEFAULT NULL COMMENT 'Company Id',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Company Name',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Dari mana item itu digunakan',
  `dest_wh_id` int(11) DEFAULT NULL COMMENT 'Tujuan Item',
  `project_id` int(11) DEFAULT NULL,
  `project_name` varchar(55) DEFAULT NULL,
  `dept` int(11) DEFAULT NULL COMMENT 'Departemen Yang Menggunakan Barang',
  `os_kind` varchar(20) DEFAULT NULL COMMENT '1= Transfer, 2= write off, 3= Retur, 4=Others 5=stock Use',
  `os_status` varchar(20) DEFAULT NULL COMMENT '1=Closed (selesai), 2=Outstanding (belum ada tarikan), 3=Void',
  `os_requester` varchar(50) DEFAULT NULL COMMENT 'Requester',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  `pic_input` int(11) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`os_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_outgoing_stock_01` */

/*Table structure for table `trn_outgoing_stock_02` */

DROP TABLE IF EXISTS `trn_outgoing_stock_02`;

CREATE TABLE `trn_outgoing_stock_02` (
  `os_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Os 02',
  `os_id` int(11) DEFAULT NULL COMMENT 'Id Os',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Items',
  `items_name` varchar(250) DEFAULT NULL COMMENT 'Nama Items',
  `qty` decimal(10,1) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Items',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Sub Total',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`os_02_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_outgoing_stock_02` */

/*Table structure for table `trn_po_01` */

DROP TABLE IF EXISTS `trn_po_01`;

CREATE TABLE `trn_po_01` (
  `po_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO',
  `po_no` varchar(15) NOT NULL COMMENT 'Nomor PO',
  `pr_id` int(11) NOT NULL COMMENT 'Id PR',
  `pr_no` varchar(15) NOT NULL COMMENT 'Nomor PR',
  `po_date` date NOT NULL COMMENT 'Tanggal PO',
  `requester` varchar(30) DEFAULT NULL COMMENT 'User Request',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `project_name` varchar(50) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(50) DEFAULT NULL,
  `delivery_address` text DEFAULT NULL COMMENT 'Alamat Pengiriman',
  `po_info` varchar(50) DEFAULT NULL COMMENT 'Keterangan Pr',
  `currency_id` int(3) DEFAULT NULL COMMENT 'mata uang Po',
  `po_status` varchar(20) DEFAULT NULL COMMENT 'Status PO (closed, outstanding, pending)',
  `payment_status` varchar(20) DEFAULT NULL COMMENT 'Status Pembayaran',
  `transport_fee` decimal(12,2) DEFAULT 0.00 COMMENT 'Biaya Transport (Non PPN)',
  `ppn_percent` decimal(12,2) DEFAULT 0.00 COMMENT 'PPN Persen',
  `ppn_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai PPN',
  `pph` int(1) DEFAULT NULL COMMENT '1 = Menggunakan PPH 0 = Tidak ada PPH',
  `pph_value` decimal(12,2) DEFAULT NULL COMMENT 'Value PPH',
  `pbbkb` int(1) DEFAULT NULL COMMENT '1 = Menggunakan pbbkb 0 = Tidak ada pbbkb',
  `pbbkb_value` decimal(12,2) DEFAULT NULL COMMENT 'Value Pbbkb',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Diskon Persen',
  `disc_val` decimal(12,2) DEFAULT 0.00 COMMENT 'Diskon Nilai',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `reamaining` decimal(12,2) DEFAULT 0.00 COMMENT 'Sisa Pembayaran',
  `top` varchar(30) DEFAULT NULL,
  `no_print` int(2) DEFAULT 0 COMMENT 'Berapa Kali Po Dicetak',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`po_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_01` */

/*Table structure for table `trn_po_02` */

DROP TABLE IF EXISTS `trn_po_02`;

CREATE TABLE `trn_po_02` (
  `po_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO 02',
  `po_id` int(11) NOT NULL COMMENT 'Id PO 01',
  `items_id` int(11) NOT NULL COMMENT 'Id Items',
  `items_name` varchar(150) DEFAULT NULL COMMENT 'Nama Items',
  `qty` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty Items',
  `price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Items',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Dikon Persen',
  `disc_val` decimal(12,2) DEFAULT 0.00 COMMENT 'Diskon Nilai',
  `exchange_rate` decimal(10,2) DEFAULT 0.00 COMMENT 'nilai tukar',
  `subtotal` decimal(12,2) DEFAULT 0.00 COMMENT '(Harga Items x Qty Items)',
  `remarks` varchar(50) DEFAULT NULL COMMENT 'Info',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`po_02_id`),
  KEY `po_id` (`po_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_02` */

/*Table structure for table `trn_po_dp` */

DROP TABLE IF EXISTS `trn_po_dp`;

CREATE TABLE `trn_po_dp` (
  `pd_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(30) DEFAULT NULL COMMENT 'Nama Supplier',
  `pd_no` varchar(15) DEFAULT NULL COMMENT 'No Dokumen',
  `pd_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `po_id` int(11) DEFAULT NULL COMMENT 'Id PO',
  `po_no` varchar(15) DEFAULT NULL COMMENT 'No PO',
  `amount` decimal(10,2) DEFAULT 0.00 COMMENT 'Nilai DP',
  `remarks` varchar(180) DEFAULT NULL COMMENT 'Keterangan Singkat',
  `is_active` tinyint(1) DEFAULT 1 COMMENT 'Status Dokumen',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pd_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `po_id` (`po_id`),
  CONSTRAINT `trn_po_dp_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `db_master_shp`.`mst_supplier` (`supplier_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_dp` */

/*Table structure for table `trn_po_payment_01` */

DROP TABLE IF EXISTS `trn_po_payment_01`;

CREATE TABLE `trn_po_payment_01` (
  `pp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO Payment',
  `pp_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO Payment',
  `pp_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(50) DEFAULT NULL,
  `payment_type` varchar(30) DEFAULT '0' COMMENT 'Jenis Pembayaran',
  `grand_total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total Keseluruhan',
  `is_active` int(2) DEFAULT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_payment_01` */

/*Table structure for table `trn_po_payment_02` */

DROP TABLE IF EXISTS `trn_po_payment_02`;

CREATE TABLE `trn_po_payment_02` (
  `pp_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '⁯Id Detail PO Generate',
  `pp_id` int(11) DEFAULT NULL COMMENT 'Id Header PO Generate',
  `po_id` int(11) DEFAULT NULL COMMENT 'Id PO',
  `po_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO',
  `amount` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai PO',
  `disc_percent` decimal(4,1) DEFAULT 0.0,
  `disc_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Diskon',
  `total` decimal(12,2) DEFAULT 0.00,
  `is_active` int(2) DEFAULT NULL,
  PRIMARY KEY (`pp_02_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_po_payment_02` */

/*Table structure for table `trn_pr_01` */

DROP TABLE IF EXISTS `trn_pr_01`;

CREATE TABLE `trn_pr_01` (
  `pr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Pr',
  `po_no` varchar(15) DEFAULT NULL COMMENT 'Ref PO',
  `pr_no` varchar(15) NOT NULL COMMENT 'Nomor Pr',
  `pr_date` date NOT NULL COMMENT 'Tanggal Pr',
  `requester` varchar(30) DEFAULT NULL COMMENT 'User Request',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `project_name` varchar(50) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `supplier_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `supplier_name` varchar(50) DEFAULT NULL,
  `file_quotation` tinyint(2) DEFAULT NULL COMMENT '1 = Terdapat file 0 = Tidak ada File',
  `delivery_address` varchar(200) DEFAULT NULL COMMENT 'Alamat Pengiriman',
  `pr_info` varchar(50) DEFAULT NULL COMMENT 'Keterangan Pr',
  `currency_id` int(3) DEFAULT NULL COMMENT 'mata uang pr',
  `pr_status` varchar(20) DEFAULT NULL COMMENT 'Status Pr (New, Process, Pending, Terminated, Close)',
  `transport_fee` decimal(12,2) DEFAULT 0.00 COMMENT 'Biaya Transport (Non PPN)',
  `ppn_percent` decimal(12,2) DEFAULT 0.00 COMMENT 'PPN Persen',
  `ppn_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai PPN',
  `top` varchar(30) DEFAULT NULL,
  `pph` int(1) DEFAULT NULL COMMENT '1 = Menggunakan PPH 0 = Tidak ada PPH',
  `pph_value` decimal(12,2) DEFAULT NULL COMMENT 'Value PPH',
  `pbbkb` int(1) DEFAULT NULL COMMENT '1 = Menggunakan pbbkb 0 = Tidak ada pbbkb',
  `pbbkb_value` decimal(12,2) DEFAULT NULL COMMENT 'Value Pbbkb',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  `approve_level` int(1) DEFAULT 0 COMMENT 'Approval Level Untuk PR 0=dept suport, 1=finance, 2=Cost Control, 3=BOD, 4=presdir',
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_pr_01` */

/*Table structure for table `trn_pr_02` */

DROP TABLE IF EXISTS `trn_pr_02`;

CREATE TABLE `trn_pr_02` (
  `pr_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Pr 02',
  `pr_id` int(11) NOT NULL COMMENT 'Id Pr 01',
  `items_id` int(11) NOT NULL COMMENT 'Id Items',
  `items_name` varchar(150) DEFAULT NULL COMMENT 'Nama Items',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty Items',
  `price` decimal(12,2) DEFAULT NULL COMMENT 'Harga Items',
  `exchange_rate` decimal(10,2) DEFAULT NULL COMMENT 'nilai tukar',
  `subtotal` decimal(12,2) DEFAULT NULL COMMENT '(Harga Items x Qty Items)',
  `remarks` varchar(50) DEFAULT NULL COMMENT 'Info',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pr_02_id`),
  KEY `pr_id` (`pr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_pr_02` */

/*Table structure for table `trn_production_01` */

DROP TABLE IF EXISTS `trn_production_01`;

CREATE TABLE `trn_production_01` (
  `production_01_id` int(11) NOT NULL,
  `items_id` int(11) DEFAULT NULL,
  `items_name` varchar(100) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `pic_input` varchar(50) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(50) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`production_01_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_production_01` */

/*Table structure for table `trn_production_02` */

DROP TABLE IF EXISTS `trn_production_02`;

CREATE TABLE `trn_production_02` (
  `production_02_id` int(11) NOT NULL AUTO_INCREMENT,
  `production_01_id` int(11) DEFAULT NULL,
  `items_id` int(11) DEFAULT NULL,
  `items_name` varchar(100) DEFAULT NULL,
  `items_unit` varchar(50) DEFAULT NULL,
  `qty` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`production_02_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_production_02` */

/*Table structure for table `trn_sales_01` */

DROP TABLE IF EXISTS `trn_sales_01`;

CREATE TABLE `trn_sales_01` (
  `sales_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `sales_date` date DEFAULT NULL COMMENT 'Tanggal',
  `sales_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Dokumen',
  `warehouse_id` int(11) DEFAULT NULL,
  `warehouse_name` varchar(55) DEFAULT NULL COMMENT 'Nama Project',
  `company_id` int(10) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(100) DEFAULT NULL COMMENT 'Nama Customer',
  `total_owner_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Owner Price',
  `total_cust_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Cust Price',
  `total_gov_price` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Government Price',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Keterangan',
  `doc_status` varchar(15) DEFAULT NULL COMMENT 'New, Outstanding, Closed',
  `is_cancel` tinyint(1) DEFAULT 0 COMMENT 'Flag Pembatalan',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`sales_id`),
  UNIQUE KEY `sales_no` (`sales_no`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  KEY `trn_sales_01_ibfk_1` (`warehouse_id`),
  CONSTRAINT `trn_sales_01_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `db_pos_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_01_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `db_pos_master`.`mst_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_01_ibfk_3` FOREIGN KEY (`fc_id`) REFERENCES `db_pos_master`.`mst_customer` (`fc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_01` */

insert  into `trn_sales_01`(`sales_id`,`sales_date`,`sales_no`,`warehouse_id`,`warehouse_name`,`company_id`,`company_name`,`fc_id`,`fc_name`,`total_owner_price`,`total_cust_price`,`total_gov_price`,`remarks`,`doc_status`,`is_cancel`,`disc_value`,`disc_percent`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'2021-11-03','SL2021110001',4,'',1,'Suhanda Souwmpie',3,'Putra Bintang Mas I (Suhanda)',0.00,590500.00,0.00,'','Close',0,0.00,0.00,'1','2021-11-03 04:33:18',NULL,NULL);

/*Table structure for table `trn_sales_02` */

DROP TABLE IF EXISTS `trn_sales_02`;

CREATE TABLE `trn_sales_02` (
  `sales_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Id header',
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `items_name` varchar(100) DEFAULT NULL COMMENT 'Nama Ikan',
  `owner_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Owner',
  `cust_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Customer',
  `gov_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Pemprov',
  `qty` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty Masuk',
  `disc_value` decimal(12,2) DEFAULT 0.00,
  `disc_percent` decimal(5,2) DEFAULT 0.00,
  `subtotal` decimal(14,2) DEFAULT 0.00 COMMENT 'Subtotal',
  `remarks` varchar(200) DEFAULT NULL COMMENT 'Info',
  PRIMARY KEY (`sales_02_id`),
  KEY `sales_id` (`sales_id`),
  KEY `trn_sales_02_ibfk_5` (`items_id`),
  CONSTRAINT `trn_sales_02_ibfk_4` FOREIGN KEY (`sales_id`) REFERENCES `trn_sales_01` (`sales_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_02_ibfk_5` FOREIGN KEY (`items_id`) REFERENCES `db_pos_master`.`mst_items` (`items_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_02` */

insert  into `trn_sales_02`(`sales_02_id`,`sales_id`,`items_id`,`items_name`,`owner_price`,`cust_price`,`gov_price`,`qty`,`disc_value`,`disc_percent`,`subtotal`,`remarks`) values 
(1,1,1233,'test1',0.00,12500.00,0.00,5.0,0.00,0.00,62500.00,''),
(2,1,1232,'Penggorengan no 16',0.00,66000.00,0.00,8.0,0.00,0.00,528000.00,'');

/*Table structure for table `trn_sales_03` */

DROP TABLE IF EXISTS `trn_sales_03`;

CREATE TABLE `trn_sales_03` (
  `bonus_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Detail',
  `sales_id` int(11) NOT NULL COMMENT 'Id header',
  `items_id` int(11) NOT NULL COMMENT 'Id Barang',
  `items_name` varchar(100) NOT NULL DEFAULT '' COMMENT 'Nama Barang',
  `qty` decimal(7,1) NOT NULL DEFAULT 0.0 COMMENT 'Qty Bonus',
  `remarks` varchar(200) DEFAULT '' COMMENT 'Info',
  PRIMARY KEY (`bonus_id`),
  KEY `sales_id` (`sales_id`),
  KEY `trn_bonus_ibfk_5` (`items_id`),
  CONSTRAINT `trn_bonus_ibfk_4` FOREIGN KEY (`sales_id`) REFERENCES `trn_sales_01` (`sales_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_bonus_ibfk_5` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_03` */

insert  into `trn_sales_03`(`bonus_id`,`sales_id`,`items_id`,`items_name`,`qty`,`remarks`) values 
(1,1,1233,'test1',1.0,''),
(2,1,1232,'Penggorengan no 16',2.0,'');

/*Table structure for table `trn_sales_payment_01` */

DROP TABLE IF EXISTS `trn_sales_payment_01`;

CREATE TABLE `trn_sales_payment_01` (
  `fp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id PO Payment',
  `fp_no` varchar(15) DEFAULT NULL COMMENT 'Nomor PO Payment',
  `fp_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `fc_id` int(11) DEFAULT NULL COMMENT 'Id Customer',
  `fc_name` varchar(150) DEFAULT NULL COMMENT 'Nama Customer',
  `payment_type` smallint(2) DEFAULT 0 COMMENT 'Jenis Pembayaran 1=CASH, 2=Transfer, 3=Giro, 4=Check',
  `grand_total` decimal(14,2) DEFAULT 0.00 COMMENT 'Total Keseluruhan',
  `is_active` int(2) DEFAULT NULL,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fp_id`),
  KEY `company_id` (`company_id`),
  KEY `fc_id` (`fc_id`),
  CONSTRAINT `trn_sales_payment_01_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `db_pos_master`.`mst_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_sales_payment_01_ibfk_2` FOREIGN KEY (`fc_id`) REFERENCES `db_pos_master`.`mst_customer` (`fc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_payment_01` */

/*Table structure for table `trn_sales_payment_02` */

DROP TABLE IF EXISTS `trn_sales_payment_02`;

CREATE TABLE `trn_sales_payment_02` (
  `fp_02_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '⁯Id Detail Fish Payment',
  `fp_id` int(11) DEFAULT NULL COMMENT 'Id Header Fish Payment',
  `sales_id` int(11) DEFAULT NULL COMMENT 'Id Sales',
  `sales_no` varchar(15) DEFAULT NULL COMMENT 'Nomor Sales',
  `amount` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Bayar',
  `disc_percent` decimal(4,1) DEFAULT 0.0 COMMENT 'Persen Diskon',
  `disc_value` decimal(12,2) DEFAULT 0.00 COMMENT 'Nilai Diskon',
  `total` decimal(12,2) DEFAULT 0.00 COMMENT 'Total',
  `is_active` smallint(1) DEFAULT 1,
  PRIMARY KEY (`fp_02_id`),
  KEY `fp_id` (`fp_id`),
  CONSTRAINT `trn_sales_payment_02_ibfk_1` FOREIGN KEY (`fp_id`) REFERENCES `trn_sales_payment_01` (`fp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_sales_payment_02` */

/*Table structure for table `trn_so_dp` */

DROP TABLE IF EXISTS `trn_so_dp`;

CREATE TABLE `trn_so_dp` (
  `sd_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Id Supplier',
  `customer_name` varchar(30) DEFAULT NULL COMMENT 'Nama Supplier',
  `sd_no` varchar(15) DEFAULT NULL COMMENT 'No Dokumen',
  `sd_date` date DEFAULT NULL COMMENT 'Tanggal Dokumen',
  `so_id` int(11) DEFAULT NULL COMMENT 'Id PO',
  `so_no` varchar(15) DEFAULT NULL COMMENT 'No PO',
  `amount` decimal(10,2) DEFAULT 0.00 COMMENT 'Nilai DP',
  `remarks` varchar(180) DEFAULT NULL COMMENT 'Keterangan Singkat',
  `is_active` tinyint(1) DEFAULT 1 COMMENT 'Status Dokumen',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`sd_id`),
  KEY `supplier_id` (`customer_id`),
  KEY `po_id` (`so_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_so_dp` */

/*Table structure for table `trn_stock` */

DROP TABLE IF EXISTS `trn_stock`;

CREATE TABLE `trn_stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT 'In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `items_out` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `adj_in` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `adj_out` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_id`),
  UNIQUE KEY `items_id` (`items_id`,`warehouse_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `trn_stock_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_ibfk_2` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_bumbu_master`.`mst_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock` */

insert  into `trn_stock`(`stock_id`,`items_id`,`doc_no`,`warehouse_id`,`company_id`,`company_name`,`trn_date`,`trn_year`,`trn_month`,`activity`,`items_in`,`items_out`,`adj_in`,`adj_out`,`old_stock`,`current_stock`) values 
(1,1233,'DO2021110009',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',0.0,6.0,0.0,0.0,1000.0,994.0),
(2,1232,'DO2021110009',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',0.0,10.0,0.0,0.0,1000.0,990.0),
(3,1240,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,0.0,0.0,0.0,1000.0),
(4,1239,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,0.0,0.0,0.0,1000.0),
(5,1238,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,0.0,0.0,0.0,1000.0),
(6,1237,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,0.0,0.0,0.0,1000.0),
(7,1236,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,0.0,0.0,0.0,1000.0);

/*Table structure for table `trn_stock_by_doc` */

DROP TABLE IF EXISTS `trn_stock_by_doc`;

CREATE TABLE `trn_stock_by_doc` (
  `stock_by_doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` varchar(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT '1=In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty',
  `items_out` decimal(7,1) DEFAULT 0.0 COMMENT 'Qty',
  `items_remaining` decimal(7,1) NOT NULL DEFAULT 0.0 COMMENT 'Sisa Items Per Dokumen',
  `old_stock` decimal(7,1) DEFAULT 0.0 COMMENT 'Stok Awal',
  `current_stock` decimal(7,1) DEFAULT 0.0 COMMENT 'Stok Akhir',
  `items_price` decimal(10,2) DEFAULT 0.00 COMMENT 'Harga Beli',
  `pic_data` varchar(15) DEFAULT NULL,
  `data_time` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_by_doc_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `trn_stock_by_doc_ibfk_1` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_by_doc_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_by_doc` */

insert  into `trn_stock_by_doc`(`stock_by_doc_id`,`items_id`,`doc_no`,`project_id`,`warehouse_id`,`trn_date`,`trn_year`,`trn_month`,`activity`,`items_in`,`items_out`,`items_remaining`,`old_stock`,`current_stock`,`items_price`,`pic_data`,`data_time`) values 
(1,1233,'IST2021110001',NULL,6,'2021-11-03','2021','11','In_Others',1000.0,0.0,977.0,0.0,1000.0,10000.00,'1','2021-11-03 04:32:27'),
(2,1232,'IST2021110001',NULL,6,'2021-11-03','2021','11','In_Others',1000.0,0.0,965.0,0.0,1000.0,60000.00,'1','2021-11-03 04:32:27'),
(3,1232,'DO2021110001',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,1000.0,999.0,60000.00,'1','2021-11-03 04:40:12'),
(4,1233,'DO2021110001',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,1000.0,999.0,10000.00,'1','2021-11-03 04:40:12'),
(5,1232,'DO2021110002',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,999.0,998.0,60000.00,'1','2021-11-03 05:03:51'),
(6,1233,'DO2021110002',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,999.0,998.0,10000.00,'1','2021-11-03 05:03:51'),
(7,1232,'DO2021110003',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,998.0,997.0,60000.00,'1','2021-11-03 05:04:23'),
(8,1233,'DO2021110003',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,998.0,997.0,10000.00,'1','2021-11-03 05:04:23'),
(9,1232,'DOR2021110001',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,997.0,998.0,60000.00,'1','2021-11-03 05:22:06'),
(10,1233,'DOR2021110001',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,997.0,998.0,10000.00,'1','2021-11-03 05:22:06'),
(11,1232,'DOR2021110002',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,998.0,999.0,60000.00,'1','2021-11-03 05:22:06'),
(12,1233,'DOR2021110002',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,998.0,999.0,10000.00,'1','2021-11-03 05:22:06'),
(13,1232,'DOR2021110003',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,999.0,1000.0,60000.00,'1','2021-11-03 05:22:06'),
(14,1233,'DOR2021110003',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,999.0,1000.0,10000.00,'1','2021-11-03 05:22:06'),
(15,1232,'DO2021110004',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,2.0,0.0,1000.0,998.0,60000.00,'1','2021-11-03 05:22:36'),
(16,1233,'DO2021110004',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,2.0,0.0,1000.0,998.0,10000.00,'1','2021-11-03 05:22:36'),
(17,1232,'DO2021110005',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,3.0,0.0,998.0,995.0,60000.00,'1','2021-11-03 05:23:26'),
(18,1233,'DO2021110005',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,3.0,0.0,998.0,995.0,10000.00,'1','2021-11-03 05:23:26'),
(19,1232,'DO2021110006',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,5.0,0.0,995.0,990.0,60000.00,'1','2021-11-03 05:24:24'),
(20,1233,'DO2021110006',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,1.0,0.0,995.0,994.0,10000.00,'1','2021-11-03 05:24:24'),
(21,1232,'DOR2021110004',NULL,6,'2021-11-03','2021','11','Rj_do',2.0,0.0,2.0,990.0,992.0,60000.00,'1','2021-11-03 05:25:23'),
(22,1233,'DOR2021110004',NULL,6,'2021-11-03','2021','11','Rj_do',2.0,0.0,2.0,994.0,996.0,10000.00,'1','2021-11-03 05:25:23'),
(23,1232,'DO2021110007',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,2.0,0.0,992.0,990.0,60000.00,'1','2021-11-03 05:26:38'),
(24,1233,'DO2021110007',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,2.0,0.0,996.0,994.0,10000.00,'1','2021-11-03 05:26:38'),
(25,1232,'DOR2021110005',NULL,6,'2021-11-03','2021','11','Rj_do',3.0,0.0,3.0,990.0,993.0,60000.00,'1','2021-11-03 05:27:28'),
(26,1233,'DOR2021110005',NULL,6,'2021-11-03','2021','11','Rj_do',3.0,0.0,3.0,994.0,997.0,10000.00,'1','2021-11-03 05:27:28'),
(27,1232,'DOR2021110006',NULL,6,'2021-11-03','2021','11','Rj_do',5.0,0.0,5.0,993.0,998.0,60000.00,'1','2021-11-03 05:27:28'),
(28,1233,'DOR2021110006',NULL,6,'2021-11-03','2021','11','Rj_do',1.0,0.0,1.0,997.0,998.0,10000.00,'1','2021-11-03 05:27:28'),
(29,1232,'DOR2021110007',NULL,6,'2021-11-03','2021','11','Rj_do',2.0,0.0,2.0,998.0,1000.0,60000.00,'1','2021-11-03 05:27:28'),
(30,1233,'DOR2021110007',NULL,6,'2021-11-03','2021','11','Rj_do',2.0,0.0,2.0,998.0,1000.0,10000.00,'1','2021-11-03 05:27:28'),
(31,1232,'DO2021110008',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,10.0,0.0,1000.0,990.0,60000.00,'1','2021-11-03 05:27:46'),
(32,1233,'DO2021110008',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,6.0,0.0,1000.0,994.0,10000.00,'1','2021-11-03 05:27:46'),
(33,1232,'DOR2021110008',NULL,6,'2021-11-03','2021','11','Rj_do',10.0,0.0,10.0,990.0,1000.0,60000.00,'1','2021-11-03 05:29:12'),
(34,1233,'DOR2021110008',NULL,6,'2021-11-03','2021','11','Rj_do',6.0,0.0,6.0,994.0,1000.0,10000.00,'1','2021-11-03 05:29:12'),
(35,1232,'DO2021110009',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,10.0,0.0,1000.0,990.0,60000.00,'1','2021-11-03 05:29:50'),
(36,1233,'DO2021110009',NULL,6,'2021-11-03','2021','11','DO_Trans',0.0,6.0,0.0,1000.0,994.0,10000.00,'1','2021-11-03 05:29:50'),
(37,1240,'IST2021110002',NULL,6,'2021-11-05','2021','11','In_Others',1000.0,0.0,1000.0,0.0,1000.0,1300.00,'1','2021-11-05 09:41:29'),
(38,1239,'IST2021110002',NULL,6,'2021-11-05','2021','11','In_Others',1000.0,0.0,1000.0,0.0,1000.0,900.00,'1','2021-11-05 09:41:29'),
(39,1238,'IST2021110002',NULL,6,'2021-11-05','2021','11','In_Others',1000.0,0.0,1000.0,0.0,1000.0,800.00,'1','2021-11-05 09:41:29'),
(40,1237,'IST2021110002',NULL,6,'2021-11-05','2021','11','In_Others',1000.0,0.0,1000.0,0.0,1000.0,500.00,'1','2021-11-05 09:41:29'),
(41,1236,'IST2021110002',NULL,6,'2021-11-05','2021','11','In_Others',1000.0,0.0,1000.0,0.0,1000.0,1000.00,'1','2021-11-05 09:41:29');

/*Table structure for table `trn_stock_by_doc_tmp` */

DROP TABLE IF EXISTS `trn_stock_by_doc_tmp`;

CREATE TABLE `trn_stock_by_doc_tmp` (
  `stock_by_doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `project_id` int(11) DEFAULT NULL COMMENT 'Id Project',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` varchar(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT '1=In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(12,2) DEFAULT 0.00 COMMENT 'Qty',
  `items_out` decimal(12,2) DEFAULT 0.00 COMMENT 'Qty',
  `items_remaining` decimal(12,2) NOT NULL DEFAULT 0.00 COMMENT 'Sisa Items Per Dokumen',
  `old_stock` decimal(12,2) DEFAULT 0.00 COMMENT 'Stok Awal',
  `current_stock` decimal(12,2) DEFAULT 0.00 COMMENT 'Stok Akhir',
  `items_price` decimal(12,2) DEFAULT 0.00 COMMENT 'Harga Beli',
  `pic_data` varchar(15) DEFAULT NULL,
  `data_time` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_by_doc_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_by_doc_tmp` */

/*Table structure for table `trn_stock_hist` */

DROP TABLE IF EXISTS `trn_stock_hist`;

CREATE TABLE `trn_stock_hist` (
  `stock_hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Dokumen Transaksi Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Warehouse Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Transaksi',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun Transaksi',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan Transaksi',
  `activity` varchar(15) DEFAULT NULL COMMENT 'Aktifitas Stok',
  `qty` decimal(7,1) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(7,1) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_hist_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `trn_stock_hist_ibfk_1` FOREIGN KEY (`items_id`) REFERENCES `db_bumbu_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_hist_ibfk_2` FOREIGN KEY (`warehouse_id`) REFERENCES `db_bumbu_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_stock_hist_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `db_bumbu_master`.`mst_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_hist` */

insert  into `trn_stock_hist`(`stock_hist_id`,`items_id`,`doc_no`,`warehouse_id`,`company_id`,`company_name`,`trn_date`,`trn_year`,`trn_month`,`activity`,`qty`,`old_stock`,`current_stock`) values 
(1,1233,'IST2021110001',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','In_Others',1000.0,0.0,1000.0),
(2,1232,'IST2021110001',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','In_Others',1000.0,0.0,1000.0),
(3,1232,'DO2021110001',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,1000.0,999.0),
(4,1233,'DO2021110001',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,1000.0,999.0),
(5,1232,'DO2021110002',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,999.0,998.0),
(6,1233,'DO2021110002',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,999.0,998.0),
(7,1232,'DO2021110003',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,998.0,997.0),
(8,1233,'DO2021110003',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,998.0,997.0),
(9,1232,'DOR2021110001',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,997.0,998.0),
(10,1233,'DOR2021110001',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,997.0,998.0),
(11,1232,'DOR2021110002',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,998.0,999.0),
(12,1233,'DOR2021110002',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,998.0,999.0),
(13,1232,'DOR2021110003',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,999.0,1000.0),
(14,1233,'DOR2021110003',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,999.0,1000.0),
(15,1232,'DO2021110004',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',2.0,1000.0,998.0),
(16,1233,'DO2021110004',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',2.0,1000.0,998.0),
(17,1232,'DO2021110005',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',3.0,998.0,995.0),
(18,1233,'DO2021110005',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',3.0,998.0,995.0),
(19,1232,'DO2021110006',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',5.0,995.0,990.0),
(20,1233,'DO2021110006',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',1.0,995.0,994.0),
(21,1232,'DOR2021110004',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',2.0,990.0,992.0),
(22,1233,'DOR2021110004',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',2.0,994.0,996.0),
(23,1232,'DO2021110007',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',2.0,992.0,990.0),
(24,1233,'DO2021110007',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',2.0,996.0,994.0),
(25,1232,'DOR2021110005',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',3.0,990.0,993.0),
(26,1233,'DOR2021110005',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',3.0,994.0,997.0),
(27,1232,'DOR2021110006',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',5.0,993.0,998.0),
(28,1233,'DOR2021110006',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',1.0,997.0,998.0),
(29,1232,'DOR2021110007',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',2.0,998.0,1000.0),
(30,1233,'DOR2021110007',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',2.0,998.0,1000.0),
(31,1232,'DO2021110008',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',10.0,1000.0,990.0),
(32,1233,'DO2021110008',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',6.0,1000.0,994.0),
(33,1232,'DOR2021110008',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',10.0,990.0,1000.0),
(34,1233,'DOR2021110008',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','Rj_do',6.0,994.0,1000.0),
(35,1232,'DO2021110009',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',10.0,1000.0,990.0),
(36,1233,'DO2021110009',6,1,'Suhanda Souwmpie','2021-11-03',2021,'11','DO_Trans',6.0,1000.0,994.0),
(37,1240,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,1000.0),
(38,1239,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,1000.0),
(39,1238,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,1000.0),
(40,1237,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,1000.0),
(41,1236,'IST2021110002',6,1,'Suhanda Souwmpie','2021-11-05',2021,'11','In_Others',1000.0,0.0,1000.0);

/*Table structure for table `trn_stock_hist_tmp` */

DROP TABLE IF EXISTS `trn_stock_hist_tmp`;

CREATE TABLE `trn_stock_hist_tmp` (
  `stock_hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Dokumen Transaksi Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Warehouse Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Transaksi',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun Transaksi',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan Transaksi',
  `activity` varchar(15) DEFAULT NULL COMMENT 'Aktifitas Stok',
  `qty` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_hist_id`),
  KEY `items_id` (`items_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_hist_tmp` */

/*Table structure for table `trn_stock_tmp` */

DROP TABLE IF EXISTS `trn_stock_tmp`;

CREATE TABLE `trn_stock_tmp` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `items_id` int(11) DEFAULT NULL COMMENT 'Id Barang',
  `doc_no` varchar(15) DEFAULT NULL COMMENT 'Update Dokumen Terakhir yang berhubungan Dengan Stok',
  `warehouse_id` int(11) DEFAULT NULL COMMENT 'Gudang Id',
  `company_id` int(11) DEFAULT NULL COMMENT 'Id Company',
  `company_name` varchar(50) DEFAULT NULL COMMENT 'Nama Company',
  `trn_date` date DEFAULT NULL COMMENT 'Tanggal Terakhir Yang Berhubungan Dengan Stok',
  `trn_year` year(4) DEFAULT NULL COMMENT 'Tahun',
  `trn_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `activity` varchar(15) DEFAULT NULL COMMENT 'In_Po, In_Trans, In_Others, In_Adj, Out_Trans, Write_Off, Out_Trans, Out_Others, Out_Adj',
  `items_in` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `items_out` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `adj_in` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `adj_out` decimal(12,2) DEFAULT NULL COMMENT 'Qty',
  `old_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Awal',
  `current_stock` decimal(12,2) DEFAULT NULL COMMENT 'Stok Akhir',
  PRIMARY KEY (`stock_id`),
  UNIQUE KEY `items_id` (`items_id`,`warehouse_id`),
  KEY `warehouse_id` (`warehouse_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_stock_tmp` */

/*Table structure for table `trn_unit_conversion` */

DROP TABLE IF EXISTS `trn_unit_conversion`;

CREATE TABLE `trn_unit_conversion` (
  `uv_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id Tabel',
  `uv_no` varchar(15) DEFAULT NULL COMMENT 'No Table',
  `warehouse_id` int(11) DEFAULT NULL,
  `items_id_from` int(11) NOT NULL COMMENT 'Id Item Asal',
  `units_id_from` int(11) NOT NULL COMMENT 'Id Stuan Asal',
  `qty_from` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Quantiti Asal',
  `price_from` decimal(11,2) DEFAULT 0.00,
  `items_id_to` int(11) NOT NULL COMMENT 'Id Item Tujuan',
  `units_id_to` int(11) NOT NULL COMMENT 'Id Stuan Tujuan',
  `qty_to` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Quantiti Tujuan',
  `price_to` decimal(11,2) DEFAULT 0.00,
  `remarks` varchar(195) DEFAULT NULL COMMENT 'Catatan',
  `is_active` smallint(1) DEFAULT NULL COMMENT 'Status',
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`uv_id`),
  KEY `items_id_from` (`items_id_from`),
  KEY `items_id_to` (`items_id_to`),
  KEY `units_id_from` (`units_id_from`),
  KEY `units_id_to` (`units_id_to`),
  KEY `warehouse_id` (`warehouse_id`),
  CONSTRAINT `trn_unit_conversion_ibfk_1` FOREIGN KEY (`items_id_from`) REFERENCES `db_pos_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_2` FOREIGN KEY (`items_id_to`) REFERENCES `db_pos_master`.`mst_items` (`items_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_3` FOREIGN KEY (`units_id_from`) REFERENCES `db_pos_master`.`mst_items_unit` (`items_unit_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_4` FOREIGN KEY (`units_id_to`) REFERENCES `db_pos_master`.`mst_items_unit` (`items_unit_id`) ON UPDATE CASCADE,
  CONSTRAINT `trn_unit_conversion_ibfk_5` FOREIGN KEY (`warehouse_id`) REFERENCES `db_pos_master`.`mst_warehouse` (`warehouse_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_unit_conversion` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
