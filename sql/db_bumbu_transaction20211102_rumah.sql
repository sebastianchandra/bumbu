/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.21-MariaDB : Database - db_bumbu_transaction
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_bumbu_transaction` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_bumbu_transaction`;

/*Table structure for table `trn_production_01` */

DROP TABLE IF EXISTS `trn_production_01`;

CREATE TABLE `trn_production_01` (
  `production_01_id` int(11) NOT NULL,
  `items_id` int(11) DEFAULT NULL,
  `items_name` varchar(100) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `pic_input` varchar(50) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(50) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`production_01_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_production_01` */

/*Table structure for table `trn_production_02` */

DROP TABLE IF EXISTS `trn_production_02`;

CREATE TABLE `trn_production_02` (
  `production_02_id` int(11) NOT NULL AUTO_INCREMENT,
  `production_01_id` int(11) DEFAULT NULL,
  `items_id` int(11) DEFAULT NULL,
  `items_name` varchar(100) DEFAULT NULL,
  `items_unit` varchar(50) DEFAULT NULL,
  `qty` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`production_02_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `trn_production_02` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
